﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Windows.Forms;
using System.Threading;
using System.Net.NetworkInformation;

namespace Input_data_Station_baru
{
    class Control
    {
        DataTable tableInbound = new DataTable();
        DataTable tableMovement;
        DataTable tableOutbound;
        DataTable tableDetail;
        List<string[]> dataOrder = new List<string[]>();
        sqlitecs sq;
        dbaccess db;
        int quantityDetail;

        public List<string[]> datacombo;
        public InputData wr;
        List<string[]> datacomboCold;
        List<string[]> datacomboWare;
        public System.Timers.Timer delaydevice;
        private Thread serverThread;
        
        public Control()
        {
            db = new dbaccess();
            sq = new sqlitecs();
            insialisasitable();
            serverThread = new Thread(new ThreadStart(MulaiServer));
            serverThread.Start();
            //datacomboWare = new List<string[]>();
            //string [] data =  new string[2];
            //data[0] = "Cold Room 1";
            //data[1] = "0";
            //datacomboWare.Add(data); data = new string[2];
            //data[0] = "Cold Room 2";
            //data[1] = "1";
            //datacomboWare.Add(data); data = new string[2];
            //data[0] = "Cold Room 3";
            //data[1] = "2";
            //datacomboWare.Add(data);
        }
        ServerMultiClient srv;
        public void MulaiServer()
        {
            srv = new ServerMultiClient();
            srv.Start(this, sq.config["portserver"]);
        }
        public void insialisasitable()
        {
            tableInbound = new DataTable();
            tableInbound.Columns.Add("Basket ID");
            tableInbound.Columns.Add("Timestamp");
            tableMovement = new DataTable();
            tableMovement.Columns.Add("Basket ID");
            tableMovement.Columns.Add("Timestamp");
            tableOutbound = new DataTable();
            tableOutbound.Columns.Add("Basket ID");
            tableOutbound.Columns.Add("Timestamp");
        }

        public void startstation(string adminid)
        {
            log("Login successfully admin ID = " + adminid);
            log("Server Port : " + sq.config["portserver"]); 
            db.adminid = adminid;
            db.from = "Input Data Station";
            db.eventname = "Start Station";
            db.systemlog();
            delaydevice = new System.Timers.Timer();
            delaydevice.Interval = 10000;
            delaydevice.Elapsed += new System.Timers.ElapsedEventHandler(delaydevice_Elapsed);
            delaydevice.Start();
            //setcomboWare();
           // CekDevice("");
        }

        public void close(string data)
        {
            if (wr.InvokeRequired)
            {
                wr.Invoke(new Action<string>(close), new object[] { data });
                return;
            }
            srv.closeServer("");

        }

        void delaydevice_Elapsed()
        {
            CekDevice("");
        }

        string getProductModelId(string modelName)
        {
            List<string> field = new List<string>();
            field.Add("product_model");
            string where = "dbo.product_model.model_name = '"+modelName+"'";
            List<string[]> ds = db.selectList(field, "product_model", where);
            if (db.num_rows > 0)
                return ds[0][0];
            return "";
        }
        ////RECEIVING
        //public void tambahData(string dataBarcode)
        //{
        //    //cek data di db 
        //    List<string> field = new List<string>();
        //    field.Add("basketid");
        //    string where = "basketid ='" + dataBarcode + "'";
        //    List<string[]> ds = db.selectList(field, "basket", where);
        //    if (db.num_rows > 0)
        //    {
        //        if (cekBasketPickingList(dataBarcode))
        //        {
        //            if (!cekBasketsqlite(dataBarcode))
        //                reciveSqlite(dataBarcode);
        //        }
        //    }
        //}

        //public bool cekBasketPickingList(string databarcode)
        //{
        //    List<string> field = new List<string>();
        //    field.Add("itemid");
        //    string where = "itemid ='" + databarcode + "' and status = '1'";
        //    List<string[]> ds = db.selectList(field, "pickingList", where);
        //    if (db.num_rows > 0)
        //    {
        //        return true;
        //    }
        //    return false;
        //}

        //private void reciveSqlite(string basketid)
        //{
        //    //insert ke sqlite 
            
        //    string dates = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        //    Dictionary<string, string> field = new Dictionary<string, string>();
        //    field.Add("basketid", basketid);
        //    field.Add("timestamp", dates);
        //    sq.insert(field, "warehouse");

        //    // masukan ke table
        //    DataRow row = tableInbound.NewRow();
        //    row[0] = basketid;
        //    row[1] = dates;
        //    tableInbound.Rows.Add(row);
        //    wr.dgvInbound.DataSource = tableInbound;
        //    wr.lblQtyInbound.Text = "" + wr.dgvInbound.Rows.Count;
        //}

        //private bool cekBasketsqlite(string basket)
        //{
        //    List<string> field = new List<string>();
        //    field.Add("basketid");
        //    string where = "basketid ='" + basket + "'";
        //    List<string[]> ds = sq.select(field, "warehouse", where);
        //    if (sq.num_rows > 0)
        //    {
        //        return true;
        //    }
        //    return false;


        //}

        //public void hapusdata(string barcode)
        //{
        //    if (barcode.Length > 0)
        //        barcode = "basketid ='" + barcode + "'";
        //    sq.delete(barcode, "warehouse");
        //}
        
        //public void setcomboWare()
        //{
        //    List<string> da = new List<string>();
        //    List<string> field = new List<string>();
        //    field.Add("warehouse_location.locationName,warehouse_location.locationId");
        //    string from = "warehouse INNER JOIN dbo.warehouse_location ON warehouse_location.warehouseId = warehouse.warehouseId AND warehouse_location.warehouseId = warehouse.warehouseId";
        //    string where = "warehouse.owner = 'Distribution'";
        //    datacomboWare = db.selectList(field, from, where);
        //    if (db.num_rows > 0)
        //    {
        //        List<string> das = new List<string>();
        //        for (int i = 0; i < datacomboWare.Count; i++)
        //        {
        //            das.Add(datacomboWare[i][0]);
        //        }
        //        wr.cmbMoveLocation.DataSource = das;
        //        wr.cmbMoveNewLocation.DataSource = das;
        //        wr.cmbLocation.DataSource = das;
        //    }
        //}

        //public void ClearDgvInbound()
        //{
        //    tableInbound = new DataTable();
        //    tableInbound.Columns.Add("Basket ID");
        //    tableInbound.Columns.Add("Timestamp");
        //    wr.lblQtyInbound.Text = "0";
        //    wr.dgvInbound.DataSource = tableInbound;
        //    hapusdata("");
        //}
       
        //internal void submitReceiving()
        //{
        //    for (int i = 0; i < wr.dgvInbound.RowCount; i++)
        //    {
        //        Dictionary<string, string> field = new Dictionary<string, string>();
        //        field.Add("locationid", datacomboWare[wr.cmbLocation.SelectedIndex][1]);
        //        string where = "basketid ='" + wr.dgvInbound.Rows[i].Cells[0].Value.ToString() + "'";
        //        db.update(field, "basket", where);
        //    }
        //    ClearDgvInbound();
        //}

        string jobnumb;
        char delimit = ',' ;
        List<string []> pro = new List<string[]>();
        void refreshDetail()
        {
            List<string> field = new List<string>();
            field.Add("shippingOrder_detail.shippingOrderDetailNo 'ID',shippingOrder_detail.productModelId 'Product ID',product_model.model_name 'Model Name',shippingOrder_detail.qty 'Qty',dbo.shippingOrder_detail.uom 'Uom',(SELECT COALESCE(SUM(pickingList.qty), 0) FROM pickingList WHERE pickingList.detailNo LIKE shippingOrder_detail.shippingOrderDetailNo + '%') as 'Actual Qty', dbo.shippingOrder_detail.uom 'Act Uom'");
            string from = "shippingOrder INNER JOIN shippingOrder_detail ON shippingOrder_detail.shippingOrderNumber = shippingOrder.shippingOrderNumber AND dbo.shippingOrder.shippingOrderNumber = dbo.shippingOrder_detail.shippingOrderNumber INNER JOIN product_model ON shippingOrder_detail.productModelId = product_model.product_model";
            string where = "shippingOrder.shippingOrderNumber = '" + jobnumb + "'";
            List<string[]> ds = db.selectQty(jobnumb);
            if (db.num_rows > 0)
            {
                //get data
                //get data model
                wr.dgvData.Invoke(new Action(() => { wr.dgvData.DataSource = db.ConvertToDatatable(ds, db.dataHeader); }));

                pro = new List<string[]>();
                int i = 0;
                foreach (string[] da in ds)
                {
                    string[] prod = new string[3];
                    prod[0] = da[0];
                    prod[1] = da[1];
                    prod[2] = da[5];
                    pro.Add(prod);
                }
            }
            else
            {
                wr.dgvData.Invoke(new Action(() => { wr.dgvData.DataSource = db.dataHeader; }));
            }
        }


        void refresha()
        {

        }

        bool isSelected = false;

        internal void SetDetail(string jobNumber,string status)
        {

            jobnumb = jobNumber;
            refreshDetail();
            if (status == "0")
            {
                // wr.btnExecute.Visible = false;
                isSelected = true;
                wr.grpDetailDgv.Visible = true;
            }
            else
            {
                //   wr.btnExecute.Visible = true;
                wr.grpDetailDgv.Visible = false;
            }
            getDataDetail(jobNumber);
            wr.grpJobOrder.Visible = false;
            wr.grpDetail.Visible = true;
            wr.lblJobOrder.Text = jobNumber;
            tampilTableDetail();
        }

        void tampilTableDetail()
        {
           
            List<string> field = new List<string>();
            field.Add("dbo.pickingList.itemId AS [Item ID],dbo.pickingList.qty AS [Qty],dbo.pickingList.uom [Uom], dbo.packaging_order.expired AS [Expiry Date], dbo.product_model.model_name AS [Product Model], dbo.shippingOrder_detail.batchnumber AS [Batch Number]");
            string from = "dbo.shippingOrder INNER JOIN dbo.shippingOrder_detail ON dbo.shippingOrder_detail.shippingOrderNumber = dbo.shippingOrder.shippingOrderNumber INNER JOIN pickingList on shippingOrder_detail.shippingOrderDetailNo = pickingList.detailNo INNER JOIN dbo.packaging_order ON dbo.shippingOrder_detail.batchnumber = dbo.packaging_order.batchNumber INNER JOIN dbo.product_model ON dbo.product_model.product_model = dbo.shippingOrder_detail.productModelId";
            string where = "shippingOrder.shippingOrderNumber = '" + jobnumb + "' and pickingList.status ='1'";
            List<string[]> ds = db.selectList(field, from, where);
            if (db.num_rows > 0)
            {
                DataTable table = db.dataHeader;
                int total=0;
                foreach (string[] Row in ds)
                {
                    DataRow row = table.NewRow();
                    for (int k = 0; k < Row.Length; k++)
                    {
                        row[k] = Row[k];
                        if (k == 1)
                        {
                            total = total + int.Parse(row[k].ToString());
                        }
                    }
                    table.Rows.Add(row);
                }
               // wr.lblActual.Text = "" + total;
                wr.dgv.Invoke(new Action(() => { wr.dgv.DataSource = table; }));
            }
            else
            {
                DataTable table = db.dataHeader;
                wr.dgv.DataSource = table;
            }
        }

        List<string> datacomboDetail;
       
        void getDataDetail(string jobNumber)
        {
            List<string> field = new List<string>();
            field.Add("dbo.pickingList.itemId,dbo.pickingList.qty,dbo.shippingOrder_detail.uom");
            string from = "dbo.shippingOrder INNER JOIN dbo.shippingOrder_detail ON dbo.shippingOrder_detail.shippingOrderNumber = dbo.shippingOrder.shippingOrderNumber INNER JOIN pickingList on shippingOrder_detail.shippingOrderDetailNo = pickingList.detailNo";
            string where = "shippingOrder.shippingOrderNumber = '" + jobNumber + "'";
            dataDetail = db.selectList(field, from, where);
            if (db.num_rows > 0)
            {
                datacomboDetail = new List<string>();
                foreach (string[] da in dataDetail)
                {
                    datacomboDetail.Add(da[1]);
                }
               // wr.cmbDetail.DataSource = datacomboDetail;
            }
        }
       
        List<string[]> dataDetail;

        internal void setProductModel(int selectInd)
        {
           // wr.lblQty.Text = dataDetail[selectInd][2];
           // wr.lblStatus.Text = dataDetail[selectInd][3];
        }

        internal void tampilJobOrder()
        {
            isSelected = false;
            wr.grpJobOrder.Visible = true;
            wr.grpDetail.Visible = false;
        }

        void simpanpickinglist(string itemid, string qty, string detailno,string uom)
        {
            Dictionary<string, string> field = new Dictionary<string, string>();
            field.Add("itemid", itemid);
            field.Add("qty", "1");
            field.Add("detailno", detailno);
            field.Add("uom", uom);
            field.Add("status", "0");
            string dates = DateTime.Now.ToString("mmMMyyssddHH");

            field.Add("pickinglist", dates);
            db.Movement("", "Cold Storage", "Input Data", qty, itemid);
            db.insert(field, "pickinglist");
        }

        void masukKeTable(string barcode, string type, string qty)
        {

        }

        bool cekItem(string barcode)
        {
            List<string> field = new List<string>();
            field.Add("itemid");
            string where = "itemid ='" + barcode + "'";
            List<string[]> ds = db.selectList(field, "pickingList", where);
            if (db.num_rows > 0)
                return false;
            else
                return true;
        }

        private void cekFullShipping(string shippingorder)
        {
            List<string> field = new List<string>();
            field.Add("count(*)");
            string from = "shippingOrder_detail";
            string where = "shippingOrderNumber='" + shippingorder + "' and flag= '1'";
            List<string[]> ds = db.selectList(field, from, where);
            if (db.num_rows > 0)
            {
                int qtyde = int.Parse(ds[0][0]);
                if (qtyde == 0)
                {
                    //tutup shipping order nya 
                    Dictionary<string, string> fieldAct = new Dictionary<string, string>();
                    fieldAct.Add("flag", "2");
                    where = "shippingOrderNumber ='" + shippingorder + "'";
                    db.update(fieldAct, "shippingOrder", where);
                    log("Shipping Order "+shippingorder+" Closed");
                }
            }
        }

        internal void getDataBarcode(string barcode)
        {
            if (!cekItem(barcode))
            {

                log("Proses Data:" + barcode);
                updatepickinglist(barcode);
                int banyaRow = wr.dgvData.Rows.Count;

                // ngecek banyak nya shipping detail
                for (int i = 0; i < banyaRow; i++)
                {
                    cekpenuh(wr.dgvData.Rows[i].Cells[0].Value.ToString(), wr.dgvData.Rows[i].Cells[2].Value.ToString());
                }

                // cek apakah sudah beres semua shipping  detail nya
                cekFullShipping(jobnumb);


                //int i = 0;
                //foreach (string[] da in pro)
                //{
                //    string productmodel = getProductModelId(da[1]);
                //    string batchnumber = da[2];
                //    char cs = (char)29;
                //    barcode = barcode.Replace(cs.ToString(), "");
                //    log("Data : " + barcode);
                //    db.eventname = "receive:" + barcode;
                //    db.systemlog();
                //    if (cekBasket(barcode, productmodel, batchnumber))
                //    {
                //        log("Data : Basket");
                //        int banyakVaccine = cekbanyakBasket(barcode);
                //        quantityDetail = quantityDetail + banyakVaccine;
                //        log("Data : " + banyakVaccine);
                //        simpanpickinglist(barcode, "" + banyakVaccine, da[0], "Basket");
                //    }
                //    else if (cekInfeed(barcode, productmodel, batchnumber))
                //    {
                //        log("Data : Infeed");
                //        int banyakVaccine = cekbanyakInfeed(barcode);
                //        quantityDetail = quantityDetail + banyakVaccine;
                //        log("Data : " + banyakVaccine);
                //        simpanpickinglist(barcode, "" + banyakVaccine, da[0], "Innerbox");
                //    }

                //    else if (cekBlister(barcode, productmodel, batchnumber))
                //    {
                //        log("Data : blister");
                //        int banyakVaccine = cekbanyakblister(barcode);
                //        quantityDetail = quantityDetail + banyakVaccine;
                //        log("Data : " + banyakVaccine);
                //        simpanpickinglist(barcode, "" + banyakVaccine, da[0], "Blister");

                //    }

                //    else if (cekvaccine(barcode, productmodel, batchnumber))
                //    {
                //        log("Data : Vaccine");
                //        quantityDetail = quantityDetail + 1;
                //        log("Data : " + 1);
                //        simpanpickinglist(barcode, "1", da[0], "Vial");
                //    }
                //    i++;

                tampilTableDetail();
                refreshDetail();
            }
            else
                log("Data " + barcode + " not found!");
        }

        private void cekpenuh(string jobnumb, string qtyDetail)
        {
            List<string> field = new List<string>();
            field.Add("COALESCE(SUM (pickingList.qty),0) ");
            string from = "dbo.pickingList INNER JOIN dbo.shippingOrder_detail ON dbo.pickingList.detailNo = dbo.shippingOrder_detail.shippingOrderDetailNo";
            string where = "dbo.pickingList.status = '1' AND dbo.pickingList.detailNo = '" + jobnumb + "'";
            List<string[]> ds = db.selectList(field, from, where);
            if (db.num_rows > 0)
            {
                int qtyde = int.Parse(ds[0][0]);
                int tempQty = int.Parse(qtyDetail);
                if (qtyde >= tempQty)
                {
                    //tutup shipping order detail nya 
                    Dictionary<string, string> fieldAct = new Dictionary<string, string>();
                    fieldAct.Add("flag", "2");
                    where = "shippingOrderDetailNo ='" + jobnumb + "'";
                    db.update(fieldAct, "shippingOrder_detail", where);
                    log("Shipping Order detail " + jobnumb + " Closed");
                }
            }
            //cek shipping order nya 

            
        }

        private void updatepickinglist(string barcode)
        {
            Dictionary<string, string> field = new Dictionary<string, string>();
            field.Add("status", "1");
            string where = "itemid ='" + barcode + "'";
            db.update(field, "pickinglist", where);
        }

        private bool cekBlister(string barcode, string productmodel,string batchnumber)
        {
            List<string> field = new List<string>();
            field.Add("Vaccine.blisterpackId");
            string from = "Vaccine INNER JOIN product_model ON Vaccine.productModelId = product_model.product_model";
            string where = "Vaccine.blisterpackId = '" + barcode + "' AND product_model.product_model = '" + productmodel + "' AND vaccine.batchnumber = '" + batchnumber + "'";
            List<string[]> ds = db.selectList(field, from, where);
            if (db.num_rows > 0)
            {
                return true;
            }
            return false;
        }

        private bool cekvaccine(string barcode, string productmodel, string batchnumber)
        {
            List<string> field = new List<string>();
            field.Add("Vaccine.capId");
            string where = "Vaccine.capId = '" + barcode + "' AND Vaccine.productModelId ='" + productmodel + "' and batchnumber ='" + batchnumber + "'";
            List<string[]> ds = db.selectList(field, "Vaccine", where);
            if (db.num_rows > 0)
            {
                return true;
            }
            return false;
        }

        public bool cekBasket(string barcode, string productmodel,string batchnumber)
        {
            List<string> field = new List<string>();
            field.Add("basketId");
            string from = "vaccine";
            string where = "vaccine.basketId = '" + barcode + "'  AND Vaccine.productModelId = '" + productmodel + "' and Vaccine.batchnumber='" + batchnumber + "'";
            List<string[]> ds = db.selectList(field,from,where);
            if (db.num_rows > 0)
            {
                return true;
            }
            return false;
            
        }
      
        public bool cekInfeed(string barcode, string productmodel, string batchnumber)
        {
            List<string> field = new List<string>();
            field.Add("DISTINCT dbo.innerBox.infeedInnerBoxId");
            string from = "innerBox INNER JOIN blisterPack ON blisterPack.innerBoxId = innerBox.infeedInnerBoxId  INNER JOIN Vaccine ON blisterPack.blisterpackId = Vaccine.blisterpackId";
            string where = "(innerBox.infeedInnerBoxId = '" + barcode + "' OR innerBox.gsOneInnerBoxId = '" + barcode + "') AND Vaccine.productModelId = '" + productmodel + "' AND Vaccine.batchnumber = '" + batchnumber + "'";
            List<string[]> ds = db.selectList(field, from, where);
            if (db.num_rows > 0)
            {
                return true;
            }
            return false;

        }
 
        //public int cekbanyakBasket(string basketid)
        //{
        //    List<string> field = new List<string>();
        //    field.Add("count(Vaccine.capId)");
        //    string tabl = "Vaccine LEFT JOIN dbo.blisterPack ON dbo.Vaccine.blisterpackId = dbo.blisterPack.blisterpackId LEFT JOIN dbo.innerBox ON dbo.blisterPack.innerBoxId = dbo.innerBox.infeedInnerBoxId left JOIN dbo.basket ON dbo.innerBox.basketId = dbo.basket.basketId";
        //    string where = "basket.basketId = '" + basketid + "'";
        //    List<string[]> ds = db.selectList(field, tabl, where);
        //    if (db.num_rows > 0)
        //        return int.Parse(ds[0][0]);
        //    else
        //        return 0;
        //}

        public int cekbanyakBasket(string basketid)
        {
            List<string> field = new List<string>();
            field.Add("count(Vaccine.capId)");
            string where = "basketId = '" + basketid + "'";
            List<string[]> ds = db.selectList(field, "Vaccine", where);
            if (db.num_rows > 0)
                return int.Parse(ds[0][0]);
            else
                return 0;
        }

        public int cekbanyakblister(string blisterpackid)
        {
            List<string> field = new List<string>();
            field.Add("count(Vaccine.capId)");
            string tabl = "vaccine";
            string where = "blisterpackid ='" + blisterpackid + "'";
            List<string[]> ds = db.selectList(field, tabl, where);
            if (db.num_rows > 0)
                return int.Parse(ds[0][0]);
            else
                return 0;
        }

        //private int cekbanyakInfeed(string barcode)
        //{
        //    List<string> field = new List<string>();
        //    field.Add("count(Vaccine.capId)");
        //    string tabl = "Vaccine LEFT JOIN dbo.blisterPack ON dbo.Vaccine.blisterpackId = dbo.blisterPack.blisterpackId LEFT JOIN dbo.innerBox ON dbo.blisterPack.innerBoxId = dbo.innerBox.infeedInnerBoxId";
        //    string where = "innerBox.infeedInnerBoxId = '" + barcode + "'";
        //    List<string[]> ds = db.selectList(field, tabl, where);
        //    if (db.num_rows > 0)
        //        return int.Parse(ds[0][0]);
        //    else
        //        return 0;
        //}

        private int cekbanyakInfeed(string barcode)
        {
            List<string> field = new List<string>();
            field.Add("count(Vaccine.capId)");
            string where = "innerBoxId = '" + barcode + "'";
            List<string[]> ds = db.selectList(field, "Vaccine", where);
            if (db.num_rows > 0)
                return int.Parse(ds[0][0]);
            else
                return 0;
        }

        public void log(string data)
        {
            try
            {
                string dates = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                string simpan = dates + "\t" + data + "\n";
                wr.txtLog.Invoke(new Action(() => { wr.txtLog.AppendText(simpan); }));    
                
            }
            catch
            {

            }
        }

        internal void executeShipping(string joborder)
        {
            Dictionary<string, string> field = new Dictionary<string, string>();
            field.Add("status","1");
            string where = "shippingOrderNumber ='"+joborder+"'";
            db.update(field,"shippingOrder",where);
        }

        internal void tambahData(string data)
        {
            if (wr.InvokeRequired)
            {
                wr.Invoke(new Action<string>(tambahData), new object[] { data });
                return;
            }
            log("Receive Data:" + data);
            if (isSelected)
            {
                data = data.Replace("\r", "");
                data = data.Replace("\n", "");
                data = data.Replace("]d2", "");
                data = data.Replace(" ", "");
                if (!data.Contains("?") && data.Length > 6)
                {
                    string[] hasilSplit = data.Split(delimit);
                    foreach (string da in hasilSplit)
                    {
                        getDataBarcode(da);
                    }
                }else
                    log("Data not found!");
            }
            else
            {
                log("Shipping order not selected!");
            }
        }

        internal void tambahDataCamera(string responseData)
        {
            log("Receive : " + responseData);
            string[] resp = responseData.Split(',');
            foreach (string da in resp)
            {
                getDataBarcode(da);
            }
        }


        public void CekDeviceDelay()
        {
           // CekDevice("");
            delaydevice = new System.Timers.Timer();
            delaydevice.Interval = 10000;
            delaydevice.Elapsed += new System.Timers.ElapsedEventHandler(delaydevice_Elapsed);
            delaydevice.Start();
        }

        void delaydevice_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
          //  CekDevice("");
        }

        public void CekDevice(string data)
        {
            if (wr.InvokeRequired)
            {
                wr.Invoke(new Action<string>(CekDevice), new object[] { data });
                return;
            }
            if (PingHost(sq.config["ipCamera"]))
                wr.lblCamera.Text = "Online";
            else
                wr.lblCamera.Text = "Offline";
        }

        public bool PingHost(string nameOrAddress)
        {
            if (nameOrAddress.Length == 0)
                return false;
            bool pingable = false;
            Ping pinger = new Ping();
            try
            {
                PingReply reply = pinger.Send(nameOrAddress);
                pingable = reply.Status == IPStatus.Success;
            }
            catch (PingException)
            {
                // Discard PingExceptions and return false;
            }
            return pingable;
        }


    }
}
