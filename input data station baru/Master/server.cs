﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Net;
using System.Threading;
using System.Data;

namespace Input_data_Station_baru
{
    class server
    {
        InputData ad;
        int port;
        Control ms;
        public server(InputData bl, string ports, Control mst)
        {
            ms = mst;
            port = int.Parse(ports);
            ad = bl;
            isRunning = true;
            if (!cekPort(ports))
            {
                mThread t = new mThread(StartListening);
                Thread masterThread = new Thread(() => t(ref isRunning));
                masterThread.IsBackground = true; //better to run it as a background thread
                masterThread.Start();
            }

        }
        public static bool isRunning;

        delegate void mThread(ref bool isRunning);
        delegate void AccptTcpClnt(ref TcpClient client, TcpListener listener);


        public static void AccptClnt(ref TcpClient client, TcpListener listener)
        {
            if (client == null)
                client = listener.AcceptTcpClient();
        }

        public void StartListening(ref bool isRunning)
        {
            TcpListener listener = new TcpListener(port);
            Byte[] bytes = new Byte[256];
            
                listener.Start();
                TcpClient handler = null;
                while (isRunning)
                {
                    AccptTcpClnt t = new AccptTcpClnt(AccptClnt);
                    Thread tt = new Thread(() => t(ref handler, listener));
                    tt.IsBackground = true;
                    tt.Start(); //the AcceptTcpClient() is a blocking method, so we are invoking it    in a seperate thread so that we can break the while loop wher isRunning becomes false

                    while (isRunning && tt.IsAlive && handler == null)
                        Thread.Sleep(1); //change the time as you prefer

                    if (handler != null)
                    {
                        NetworkStream stream = null;
                        try
                        {
                            string data = null;
                            stream = handler.GetStream();
                            int i;
                            try
                            {
                                int d =0;
                                String Message = "";
                                while (d < 2)
                                {
                                    Int32 da = stream.Read(bytes, 0, bytes.Length);
                                    Message = Message+ System.Text.Encoding.ASCII.GetString(bytes, 0, da);
                                    d++;
                                }

                                ms.tambahData(Message);
                                stream.Close();
                            }
                            catch (Exception ui)
                            {

                                ms.log(ui.Message);
                                stream.Close();
                            }
                        }
                        catch (Exception ex)
                        {
                            ms.log(ex.Message);
                            stream.Close();
                        }
                    }
                    else if (!isRunning && tt.IsAlive)
                    {
                        tt.Abort();
                    }
                    handler = null;
                }
                
                listener.Stop();
        }

        public bool cekPort(string port)
        {
            using (TcpClient tcpClient = new TcpClient())
            {
                try
                {
                    tcpClient.Connect("127.0.0.1", int.Parse(port));
                    return true;
                }
                catch (Exception)
                {
                    Console.WriteLine("Port closed");
                    return false;
                }
            }
            return false;
        }
    }
}
    
