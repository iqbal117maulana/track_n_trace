﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Net.Sockets;
using System.Net;

namespace Input_data_Station_baru
{
    class ServerMultiClient
    {
      //  static List<Listener> listeners = new List<Listener>();
        Control ctrl;
        bool isRunning = true;

        TcpClient client;
        TcpListener listener;
        public void Start(Control ctrl1, string port)
        {
            ctrl = ctrl1;
            listener = new TcpListener(IPAddress.Any, int.Parse(port));
            
            listener.Start();

            while (isRunning) // Add your exit flag here
            {
                if (!listener.Pending())
                {
                    Thread.Sleep(500); // choose a number (in milliseconds) that makes sense
                    continue; // skip to next iteration of loop
                }
                client = listener.AcceptTcpClient();
                ThreadPool.QueueUserWorkItem(ThreadProc, client);
            }
        }

        public void closeServer(string data)
        {
            try
            {
                isRunning = false;
                listener.Stop();
                client.Close();
            }
            catch(Exception ex)
            {
                
            }
        }

        private void ThreadProc(object obj)
        {
            string bufferincmessage;
            TcpClient client = (TcpClient)obj;
            NetworkStream clientStream = client.GetStream();
            ctrl.log("Connected ");
            byte[] message = new byte[4096];
            int bytesRead;
            while (isRunning)
            {
                bytesRead = 0;
                try
                {
                    //blocks until a client sends a message

                    ctrl.log("Waiting data");
                    bytesRead = clientStream.Read(message, 0, 4096);
                }
                catch
                {
                    //a socket error has occured
                    break;
                }
                if (bytesRead == 0)
                {
                    //the client has disconnected from the server
                    break;
                }

                //message has successfully been received
                ASCIIEncoding encoder = new ASCIIEncoding();
                bufferincmessage = encoder.GetString(message, 0, bytesRead);

                ctrl.log("Receive : " + bufferincmessage);
                ctrl.tambahData(bufferincmessage);
            }
        

        }
    }

    class Listener
    {
        Thread listenThread;
        string bufferincmessage;
        TcpListener tcplistener;
        Control ctrl;
    }
}