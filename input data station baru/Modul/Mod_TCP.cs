﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.IO;
using System.Windows.Forms;

namespace Input_data_Station_baru
{
    class tcp
    {

        private static TcpClient Client;
        private static Byte[] Data = new Byte[256];
        public static Stream ClientStream;
        public static bool IsConnected = false;
        sqlitecs sqlite = new sqlitecs();
        public Control ctr;
        public bool Connect(string ip, string port, string timeout)
        {
            try
            {
                Client = new TcpClient(ip, Int32.Parse(port));
                if (Client.Connected)
                {
                    IsConnected = true;
                    return true;
                }
            }
            catch (SocketException ex)
            {
                new Confirm(ex.ToString());
                if (!ex.NativeErrorCode.Equals(10056))
                    return false;
            } IsConnected = false;
            return false;
        }

        public bool Connect1(string ip, string port, string timeout)
        {
            try
            {
                Client = new TcpClient();
                IAsyncResult result = Client.BeginConnect(ip, Int32.Parse(port), null, null);
                bool success = result.AsyncWaitHandle.WaitOne(Int32.Parse(timeout), true);
                if (success)
                {
                    IsConnected = true;
                    return true;
                }
                else
                {
                    IsConnected = false;
                    return false;
                }
            }
            catch (SocketException ex)
            {
                new Confirm("error +" + ex.ToString());
                if (!ex.NativeErrorCode.Equals(10056))
                    IsConnected = false;
            }
            catch (InvalidOperationException te)
            {
                new Confirm("Printer not connected !");
            }
            return false;
        }

        public void dc()
        {
            if (ClientStream != null)
                ClientStream.Close();

            if (Client != null)
                Client.Close();
        }

        public void kirimSekali()
        {
            if (IsConnected)
            {
                ClientStream = Client.GetStream();
                StreamWriter sw = new StreamWriter(ClientStream);
                sw.WriteLine("");
                sw.Flush();
            }
        }

        public void sendBack()
        {
            try
            {
                byte[] data = new Byte[256];
                while (IsConnected)
                {
                    Int32 bytes = ClientStream.Read(data, 0, data.Length);
                    string responseData = System.Text.Encoding.ASCII.GetString(data, 0, bytes);
                    ctr.tambahDataCamera(responseData);
                }
            }
            catch (InvalidOperationException ex)
            {

            }
        }
    }
}
