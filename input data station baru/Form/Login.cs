﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Input_data_Station_baru
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
            this.Text = this.Text + " V " + System.Windows.Forms.Application.ProductVersion;
        }
        
        private void buttonLogin_Click(object sender, EventArgs e)
        {
            do_login();

        }

        private void Login_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void txtUsername_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (txtUsername.Text.Length > 0)
                {
                    this.ActiveControl = txtPassword;
                }
                else
                {
                    new Confirm("Username cannot empty", "Information", MessageBoxButtons.OK);
                    txtUsername.Focus();
                }
            }
        }

        private void txtPassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (txtUsername.Text.Length > 0)
                {
                    if (txtPassword.Text.Length > 0)
                    {
                        do_login();
                    }
                    else
                    {
                        new Confirm("Password cannot empty", "Information", MessageBoxButtons.OK);
                        txtPassword.Focus();
                    }
                }
                else
                {
                    new Confirm("Username cannot empty", "Information", MessageBoxButtons.OK);
                    txtUsername.Focus();
                }
            }
        }


        private void do_login()
        {
            dbaccess db = new dbaccess();
            try
            { //cek useraname
                if (db.validasi(txtUsername.Text))
                {
                    //cek Password
                    if (db.validasi(txtUsername.Text, txtPassword.Text))
                    {
                        List<string[]> res = db.validasi(txtUsername.Text, txtPassword.Text, "0");
                        if (res != null)
                        {
                            string adminid = res[0][0];
                            if (db.cekPermision(adminid, "input_data", "read"))
                            {
                                InputData bl = new InputData(adminid);
                                bl.Show();
                                this.Hide();
                            }
                            else
                            {
                                new Confirm("Dont have permission", "Information", MessageBoxButtons.OK);
                            }
                        }
                        else
                        {
                            new Confirm("User is inactive", "Information", MessageBoxButtons.OK);
                            db.simpanLog("Error 0010 : Username dan Password Salah !");
                            clearText();
                        }
                    }
                    else
                    {
                        new Confirm("Invalid Password", "Information", MessageBoxButtons.OK);
                        db.simpanLog("Error 0010 : Username dan Password Salah !");
                        txtPassword.Focus();
                        txtPassword.Text = "";
                    }
                }
                else
                {
                    new Confirm("Invalid Username", "Information", MessageBoxButtons.OK);
                    db.simpanLog("Error 0010 : Username Salah !");
                    txtUsername.Focus();
                    clearText();
                }
        
            }
            catch (ArgumentException ae)
            {
                db.simpanLog("Error " + ae.ToString());
            }
        }

        private void clearText()
        {
            txtUsername.Text = "";
            txtPassword.Text = "";
            txtUsername.Focus();
        }
    }
}
