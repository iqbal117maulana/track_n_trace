﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Input_data_Station_baru
{
    public partial class config : Form
    {
        public config()
        {
            InitializeComponent();
            getConfig();
        }

        void getConfig()
        {
            try
            {
                sqlitecs sql = new sqlitecs();
                txtIpCamera.Text = sql.config["ipCamera"];
                txtPortCamera.Text = sql.config["portCamera"];
                txtPortServer.Text = sql.config["portserver"];
                txtIpServer.Text = sql.config["ipServer"];
            }
            catch (KeyNotFoundException k)
            {

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Confirm cf = new Confirm("Are you sure? ", "Confirmation");
            bool hasilDialog = cf.conf();
            if (cekKosong())
            {
                if (hasilDialog)
                {

                    sqlitecs sql = new sqlitecs();
                    Dictionary<string, string> field = new Dictionary<string, string>();
                    field.Add("ipCamera", txtIpCamera.Text);
                    field.Add("portCamera", txtPortCamera.Text);
                    field.Add("portserver", txtPortServer.Text);
                    field.Add("ipServer", txtIpServer.Text);
                    sql.update(field, "config", "");
                    new Confirm("Configuration success saved", "Information", MessageBoxButtons.OK);
                    this.Dispose();

                }
            }
            else
            {
                new Confirm("Invalid configuration data", "Information", MessageBoxButtons.OK);
            }
        }

        private bool cekKosong()
        {
            if (txtIpCamera.Text.Length == 0)
                return false;
            if (txtIpServer.Text.Length == 0)
                return false;
            if (txtIpCamera.Text.Length == 0)
                return false;
            return true;
        }
    }
}
