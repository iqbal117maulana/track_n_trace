﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bottle_Station
{
    class printGs
    {
        tcp_testing tcp;
        sqlitecs sql;
        int hitung = 0;
        public string statusdomino;

        public bool cekKoneksi()
        {
            tcp = new tcp_testing();
            sql = new sqlitecs();
            string ip = sql.config["ipmarker"];
            string port = sql.config["portmarker"];
            bool res = tcp.Connect(ip, port, "5");
            return res;
        }

        public void kirimData(List<string[]> data)
        {
           // tcp.send("BUFFERCLEAR");
            for (int i = 0; i < data.Count; i++)
            {
                string [] datagsone = data[i];
                tcp.send("BUFFERDATA " + hitung + " \"" + datagsone[0] + "\" \"" + datagsone[1] + "\" \"" + datagsone[2] + "\" \"" + datagsone[3] + "\" ");
                //tcp.send(data[i]);
                Console.WriteLine("BUFFERDATA " + hitung + " \" " + datagsone[0] + " \" " + datagsone[1] + " \" " + datagsone[2] + " \" " + datagsone[3] + " \" ");
                hitung++;
            }
            tcp.dc();
        }

        public bool cekStatusPrinter()
        {
            string data = tcp.sendBack("GETMARKMODE");
            bool balik;
            string teksbalikan = "";
            if (data.Length > 0)
            {
                char nozzle = data[5];
                switch (nozzle)
                {
                    case '0':
                        statusdomino = "Printer Not Ready";
                        balik = false;
                        break;
                    default:
                        statusdomino = "Printer Ready";
                        balik = true;
                        break;
                }
                return balik;
            }
            return false;
        }

        public void close()
        {
            tcp.dc();
        }

        public void kirimData(string data)
        {
            tcp.send(data);
        }
    }
}
