﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;
using System.Threading;

namespace Bottle_Station
{

    public class generateCode
    {
        public const int QTY_KIRIM = 10;
        public const int QTY_MAKS_KIRIM_CAP = 20;
        public const int QTY_MAKS_KIRIM_GS1 = 20;
        public string batch;
        public bool audit;

        sqlitecs sql;

        public int lastCount;
        public int lastCountgs;

        public int bufferCount;
        public int bufferCountgs;
        dbaccess db;


        public generateCode()
        {
            sql = new sqlitecs();
            db = new dbaccess();
        }

        public string gsone(string gtin, string batch, string exp, string sn)
        {
            string data = "01" + gtin + "21" + sn.ToString().PadLeft(6, '0') + "10" + batch + "17" + exp;
            return data;
        }
        public string RandomString()
        {
            RandomStringGenerator RSG = new RandomStringGenerator(false, false, true, false);
            //  RSG.UniqueStrings = true;
            // Or we can use the constructor
            // RSG.LowerCaseCharacters = "absndmahjkjdjkhds".ToCharArray();
            return RSG.Generate(1);

        }
        public void generateCap(int banyak)
        {
            //getdata
            banyak = banyak * 3;
            //List<string> datacap = create_Guid(banyak);
            List<string> datacap = generate(banyak);
            datacap = checkNotExistDb(datacap);
            saveCapSqlLite(datacap);
            //for (int i = 0; i < banyak; i++)
            //{
            //    // int j = i + 1;
            //    string guid = Guid.NewGuid().ToString().Substring(0, 5);
            //    string randdat = RandomString();
            //    string capid = randdat + guid;
            //    //string capid = batch + "01" + lastCount.ToString().PadLeft(6, '0');
            //    Dictionary<string, string> field = new Dictionary<string, string>();
            //    field.Add("capid", capid);
            //    field.Add("isused", "0");
            //    if (audit)
            //        field.Add("flag", "1");
            //    else
            //        field.Add("flag", "0");
            //    sql.insertData(field, "tmp_cap_id");
            //    lastCount++;
            //}
            //bufferCount++;
            //sql.Begin();
        }

        private void saveCapSqlLite(List<string> datacap)
        {
            for (int i = 0; i < datacap.Count; i++)
            {
                Dictionary<string, string> field = new Dictionary<string, string>();
                field.Add("capid", datacap[i]);
                field.Add("isused", "0");
                if (audit)
                    field.Add("flag", "1");
                else
                    field.Add("flag", "0");
                sql.insertData(field, "tmp_cap_id");
            } sql.Begin();
        }

        private List<string> checkNotExistDb(List<string> datacap)
        {
            List<string> field = new List<string>();
            field.Add("capid");
            string where = "capid in( ";
            for (int i = 0; i < datacap.Count; i++)
            {
                where += "'" + datacap[i] + "'";
                if (i + 1 != datacap.Count)
                    where += ",";
            }
            where += ") and batchnumber ='" + batch + "'";
            List<string[]> res = db.selectList(field, "vaccine", where);
            if (res.Count > 0)
                ctrl.log("data exist : " + res.Count);
            foreach (string[] data in res)
            {
                string dataCap = data[0];
                datacap.Remove(dataCap);
            }
            return datacap;
        }

        //tambahan flubio
        private List<string> checkNotExistDbFlubio(List<string> datacap)
        {
            List<string> field = new List<string>();
            field.Add("capid");
            string where = "capid in( ";
            for (int i = 0; i < datacap.Count; i++)
            {
                where += "'" + datacap[i] + "'";
                if (i + 1 != datacap.Count)
                    where += ",";
            }
            where += ") and batchnumber ='" + batch + "'";
            List<string[]> res = db.selectList(field, "vaccine", where);
            if (res.Count > 0)
                ctrlf.log("data exist : " + res.Count);
            foreach (string[] data in res)
            {
                string dataCap = data[0];
                datacap.Remove(dataCap);
            }
            return datacap;
        }
        //end tambahan flubio
        public void generateCaptemp(int banyak)
        {
            //getdata
            banyak = banyak * 3;
            List<string> datacap = new List<string>();
            for (int i = 0; i < banyak; i++)
            {
                // int j = i + 1;
                string guid = Guid.NewGuid().ToString().Substring(0, 5);
                string randdat = RandomString();
                string capid = randdat + guid;
                //string capid = batch + "01" + lastCount.ToString().PadLeft(6, '0');
                Dictionary<string, string> field = new Dictionary<string, string>();
                field.Add("capid", capid);
                field.Add("isused", "0");
                if (audit)
                    field.Add("flag", "1");
                else
                    field.Add("flag", "0");
                sql.insertData(field, "tmp_cap_id");
                lastCount++;
            }
            bufferCount++;
            sql.Begin();
        }

        //tambahan
        public static List<string> generate(int banyak)
        {
            List<string> datacap = new List<string>();
            for (int i = 0; i < banyak; i++)
            {
                string code;
                do
                {
                    code = RandomString(2) + RandomString(2) + RandomString(2);
                } while (datacap.Contains(code));
                datacap.Add(code);
            }

            return datacap;
        }

        private static Random random = new Random();
        public static string RandomString(int length)
        {
            const string chars = "abcdefghijklmnopqrstuvwxyz0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
        //end tambahan

        public List<string> create_Guid(int banyak)
        {
            List<string> datatemp = new List<string>();
            for (int i = 0; i < banyak; i++)
            {
                string guid = Guid.NewGuid().ToString().Substring(0, 5);
                string randdat = RandomString();
                datatemp.Add(randdat + guid);
            }
            return datatemp;
        }

        public void generateVial(int banyak)
        {
            string[] data = db.dataPO(batch);
            string gtin = data[0];
            string exp = data[1];
            DateTime dateExpiry = DateTime.ParseExact(exp, "yyMMdd", CultureInfo.InvariantCulture);
            string expiryDate = dateExpiry.ToString("MMM yy").ToUpper();
            for (int i = 0; i < banyak; i++)
            {
                int j = i + 1;
                Dictionary<string, string> field = new Dictionary<string, string>();
                field.Add("batchnumber", batch);
                field.Add("gtin", gtin);
                string guid = "BIOF" + Guid.NewGuid().ToString().Replace("-", "").Substring(0, 8).ToUpper();
                //  field.Add("sn", lastCountgs.ToString().PadLeft(6, '0'));
                field.Add("sn", guid);
                field.Add("expire", exp);
                //string gs = gsone(gtin, batch, exp, lastCountgs.ToString().PadLeft(6, '0'));
                string gs = gsone(gtin, batch, exp, guid);
                field.Add("gsone", gs);
                field.Add("isused", "0");
                if (audit)
                    field.Add("flag", "1");
                else
                    field.Add("flag", "0");
                field.Add("expireDate", expiryDate);
                lastCountgs++;
                sql.insertData(field, "tmp_gsOneVial");
            }

            bufferCountgs++;
            sql.Begin();

        }
        int count = -1;

        public void kirimPrinter(List<string> data)
        {
            count++;
            print pr = new print();
            pr.cekKoneksi();
            pr.kirimData(data);

        }
        int banyakBuffer = -1;
        print PRgs;
        public Control ctrl;
        public ControlFlubio ctrlf;
        Thread GsConnectThread;
        public void kirimPrinter(List<string[]> data)
        {
            PRgs = new print();
            PRgs.cekKoneksigs();
            banyakBuffer = PRgs.kirimData(data, banyakBuffer);
        }


        bool isConnectGS;
        tcp_testing tcpgs;
        public int hitung = 0;
        public int CountBuffer = -1;
        public bool isreceive = false;
        public bool cekKoneksigs()
        {
            if (!isConnectGS)
            {
                tcpgs = new tcp_testing();
                string ip = sql.config["ipmarker"];
                string port = sql.config["portmarker"];
                bool res = tcpgs.Connect(ip, port, sql.config["timeout"]);
                isConnectGS = res;
                return res;
            }
            return true;

        }

        public int kirimData(List<string[]> data, int count)
        {
            string disimpan = "";
            CountBuffer = count;
            for (int i = 0; i < data.Count; i++)
            {
                string[] datagsone = data[i];

                string simpan;
                if (sql.config["debug"].Equals("0"))
                {
                    simpan = "BUFFERDATA " + CountBuffer + " \"" + datagsone[0] + "\" \"" + datagsone[1] + "\" \"" + datagsone[2] + "\" \"" + datagsone[3] + "\" " + "\"" + datagsone[5] + "\"";
                }
                else
                {
                    simpan = datagsone[4] + "|";
                }
                tcpgs.send(simpan);
                disimpan = disimpan + " \n| " + simpan;
                hitung++;
                CountBuffer++;
            }
            if (!isreceive)
            {
                startReceive();
                isreceive = true;
            }
            //simpanLog(disimpan);
            //tcpgs.dc();

            return CountBuffer;
        }

        void startReceive()
        {
            GsConnectThread = new Thread(new ThreadStart(MulaiReceivePrinter));
            GsConnectThread.Start();
        }

        void MulaiReceivePrinter()
        {
            while (isConnectGS)
            {
                //  ctrl.getCallBackDomino(tcpgs.sendBack());
                if (!isConnectGS)
                    break;
            }
        }


        internal void stop()
        {
            isConnectGS = false;
            if (GsConnectThread != null && tcpgs != null)
            {
                GsConnectThread.Abort();
                tcpgs.closeStream();
            }
        }
    }
}
