﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Threading;
using System.ComponentModel;
using System.Net.NetworkInformation;
using System.Windows.Forms;

namespace Bottle_Station
{
    public class Control
    {
        public const string NUMBER_CODE = "01";
        public const string NAMA_FIELD = "capid";
        public const string NAMA_TABLE_TEMP = "tmp_cap_id";
        public const string NAMA_TABLE_TEMP_capid = "tmp_cap_id";
        public const string NAMA_TABLE_TEMP_GS = "tmp_gsOneVial";
        public const string NAMA_TABLE_UTAMA = "Vaccine";
        public const string NAMA_STATION = "Bottle Station";
        public int qtySendToPrinter;
        public int qtyGenerateCode;
        public const string NAMA_SEQ = "bottleseq";
        public char delimit = ',';
        public System.Timers.Timer delaydevice;

        public result rs;
        public sqlitecs sqlite;
        public dbaccess db;
        public BottleStation bl;
        public generateCode Code;
        private Thread serverThread;
        private Thread serverThreadCamera;
        private Thread serverThreadCamera2;

        private Thread serverThreadCapid;
        private Thread serverThreadGsone;

        public string batch;
        public string adminid;
        public string adminName;
        public System.Timers.Timer delayTimer;

        public string linenumber;
        public string linename;
        public List<string[]> datacombo;
        public bool start;
        public bool startDelay;
        public bool auditStarted;
        public bool isBuffer;
        public int templastcountkirim;
        public List<string> dataSendToPrinter;
        public string productmodelid;
        public string[] dataPackagingOrder;
        public int countVaccine = 0;


        DataTable tableProductReceive = new DataTable();
        DataTable tableProductSend = new DataTable();
        DataTable tableProductFail = new DataTable();
        DataTable tableProductGsone = new DataTable();

        int countDataFail = 0;
        List<string> dataVaccine;
        int tempTerima;
        int tempTerimaKirimUlang = 1;
        int qtySend = 0;
        int qtyReceive = 0;
        int dataAggregation;
        string exp;
        public string movementType = "6";
        public int func;

        public ServerMultiClient srvMulti;

        public int hitungCapid = 0;
        public int hitungGsone = 0;
        List<string[]> dataTerimaBaru = new List<string[]>();
        List<bool> dataStatus = new List<bool>();

        private BackgroundWorker worker = new BackgroundWorker();
        private AutoResetEvent resetEvent = new AutoResetEvent(false);

        public Variable Var;

        public Mod_TCP2 prin;
        public tcp_testing prin2;
        bool cekDom = false;
        int count = 0;
        bool Sending = false;

        DataTable tableTerimaCamera = new DataTable();

        public List<string> DataTempCapid = new List<string>();
        public List<string> DataTempGSone = new List<string>();
        int countFail = 0;
        List<string[]> dataKawin = new List<string[]>();
        int countRepeatSend = 0;
        int countRepeat = 0;

        public Log lg = new Log();

//## Set form
        SetForm setform = new SetForm();
        SendData_PLC sendData_plc;

        public bool checkPLCConnected()
        {
            return sendData_plc.CheckConnected();
        }
        public void Thread_StartServer()
        {
            serverThreadCapid = new Thread(new ThreadStart(StartServer));
            serverThreadCapid.Start();
        }

        public void StartServer()
        {
            srvMulti = new ServerMultiClient();
            srvMulti.Start(this, Var.PortServer);
        }

        public void StopServer()
        {
            srvMulti.listener.Stop();
        }

        public void StartServer2()
        {
            srvMulti.listener.Start();
        }

        //Audit
        public void StartPreCheck()
        {
            db.eventname = "Pre-check " + batch;
            db.systemlog();
            // auditStarted = true;
            string[] datapack = db.dataPO(batch);
            productmodelid = datapack[4];
            exp = datapack[1];
            bl.lblBatch.Text = batch;
            bl.lblProductName.Text = datapack[3];
            sqlite.delete("", Variable.NAMA_TABLE_TEMP_Capid);
            sqlite.delete("", Variable.NAMA_TABLE_TEMP_GSOne);
            Code.lastCount = db.getcountProduct(batch) + 1;
            Code.lastCountgs = db.getcountProduct(batch) + 1;
            templastcountkirim = Code.lastCount - 1;
            GenerateCode_Send(qtyGenerateCode);
        }

        //Start
        public void SetParameterStartProduction()
        {
            auditStarted = true;
            db.eventname = "Start Printing GS1 " + batch;
            db.systemlog();
            tempTerima = 1;
            sqlite.delete("", Variable.NAMA_TABLE_TEMP_GSOne);
            string[] dat = db.dataPO(batch);
            exp = dat[1];
            productmodelid = dat[4];
            bl.lblBatch.Text = batch;
            bl.lblProductName.Text = dat[3];
            Code.batch = batch;
            Code.lastCount = db.getcountProduct(batch) + 1;
            templastcountkirim = Code.lastCount;
            Code.lastCountgs = db.getcountProduct(batch) + 1;
            Code.generateVial(Var.QTYGenerate);

            SendToPrinter(getDataSendToPrintergs(Var.QTYSend));
            SendToPrinter(getDataSendToPrintergs(Var.QTYSend));
            fillData();
            cekDom = true;
        }

        public void SendStartPrintercap()
        {
            db.eventname = "Start Printing Capid " + batch;
            db.systemlog();
            tempTerima = 1;
            sqlite.delete("", Variable.NAMA_TABLE_TEMP_Capid);
            string[] dat = db.dataPO(batch);
            exp = dat[1];
            productmodelid = dat[4];
            Code.batch = batch;
            Code.lastCount = db.getcountProduct(batch) + 1;
            templastcountkirim = Code.lastCount;
            Code.generateCap(Var.QTYGenerate);
            startPrinterLeibinger();
        }

        print prin3;

        public void stopPrinterCap()
        {
            prin3.stopListener();
        }
        private void startPrinterLeibinger()
        {
            prin3 = new print();
            if (prin3.cekKoneksi())
            {
                prin3.startCheckStatus(this);
                prin3.clearBuffer();
                prin3.startListener();
                SetDataGrid_Send("0", "");
            }
            else
            {
                new Confirm("Printer not connected", "Information", MessageBoxButtons.OK);

            }
        }

        //Laser
        public void SendStartPrinterGs()
        {
            db.eventname = "Start Printing Gs 1 ID " + batch;
            db.systemlog();
            tempTerima = 1;
            sqlite.delete("", Variable.NAMA_TABLE_TEMP_GSOne);
            string[] dat = db.dataPO(batch);
            exp = dat[1];
            productmodelid = dat[4];
            Code.batch = batch;
            Code.lastCount = db.getcountProduct(batch) + 1;
            templastcountkirim = Code.lastCount;
            Code.generateVial(Var.QTYGenerate);
            startPrinterGS1ID();
        }

        private void startPrinterGS1ID()
        {
            print prin2 = new print();
            if (prin2.cekKoneksigs())
            {
                prin2.startCheckStatus(this);
                prin2.clearBuffergs();
                prin2.startListenerGS();
                SetDataGridSendgs("0", "");
            }
            else
            {
                new Confirm("Laser not connected", "Information", MessageBoxButtons.OK);

            }
        }

        public void SendToPrinter(List<string[]> data)
        {
            Code.ctrl = this;
            Code.kirimPrinter(data);
            sqlite.BeginDelete(data);
            log("Send " + data.Count + " Data");
            getdatasend();
            GetRegenerateData_GS();
        }

        public void log(string data)
        {
            if (lg.txtLog.InvokeRequired)
            {
                lg.txtLog.Invoke(new Action<string>(log), new object[] { data });
                return;
            }

            try
            {
                string dates = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                //string simpan = dates + "\t" + data + "\n";
                string simpan = dates + "\t" + data;
                lg.txtLog.AppendText(simpan + Environment.NewLine);
                //number of lines to remove from the beginning
                //int numOfLines = 1;
                //var lines = lg.txtLog.Lines;
                //var newLines = lines.Skip(numOfLines);

                //lg.txtLog.Lines = newLines.ToArray();
            }
            catch (ObjectDisposedException ob)
            {

            }
        }

        public void startStation()
        {
            //Inisialisasi variabel
            Var = new Variable();
            Var.SetSqliteVariabel(sqlite.config);
            setform = new SetForm();
            setform.Form = bl;


            //Inisialisasi ID Admin
            db.adminid = adminid;
            adminName = GetAdminName(adminid);
            db.adminName = adminName;
            setform.AdminName = adminName;


            //Report System Log
            db.from = "Bottle Station";
            db.eventname = "Start Station";
            db.systemlog();

            //inisialisasi QTY dan Settingan Buffer
            qtyGenerateCode = Var.QTYGenerate;
            qtySend = Var.QTYSend;

            if (sqlite.config["buffer"].Equals("1"))
            {
                //delay();
                isBuffer = true;
            }
            else
            {
                isBuffer = false;
            }

            //Tampil Info Admin dan Port
            log("Server Port : " + sqlite.config["portserver"]);

            //Memulai Server
            Thread_StartServer();

            //Membersihkan Table
            setTableReceive();

            //
            rs = new result();

            //Inisisalisasi data combo box
            //GetComboBoxData();

            //Cek permenit online device
            bg_GetDeviceStatus();

            //init Connecetion to plc
            sendData_plc = new SendData_PLC();
            sendData_plc.IPAddress = sqlite.config["IpPlc"];
            sendData_plc.Port = sqlite.config["PortPlc"];
            sendData_plc.Timeout = "5";
            //sendData_plc.Connect();
            //if (sendData_plc.Is_connect)
            //{
            //    log("\nApplication->PLC : Connected");
            //    sendData_plc.Disconect();
            // //   sendData_plc.Disconect();
            //    //setform.PLC_Online();
            //}
            //else
            //{
            //    log("\nApplication->PLC : Not Connected");
            //  //  setform.PLC_Offline();
            //}


        }

        public void setTableReceive()
        {
            tableProductReceive = new DataTable();
            tableProductReceive.Columns.Add("CapID");
            tableProductReceive.Columns.Add("GS1ID");
            tableProductReceive.Columns.Add("Timestamp");
            tableProductReceive.Columns.Add("Status");
            bl.dgvReceive.DataSource = tableProductReceive;

            tableProductSend = new DataTable();
            tableProductSend.Columns.Add("Cap ID");
            bl.dgvCapId.DataSource = tableProductSend;

            tableProductGsone = new DataTable();
            tableProductGsone.Columns.Add("GS1 ID");
            bl.dgvGsone.DataSource = tableProductGsone;
        }

        // untuk generate data pass/fail dari plc
        int qtypass= 0;
        int qtyfail=0;
        public string GetStatus(string data)
        {
            if (data.Equals("0"))
            {
                qtypass++;
                return "Pass";
            }
            else if (data.Equals("2"))
            {
                qtyfail++;
                return "Capid Already Exist";
            }
            else if (data.Equals("3"))
            {
                qtyfail++;
                return "GSOne Already Exist";
            }
            else if (data.Equals("4"))
            {
                qtyfail++;
                return "Fail XXX";
            }
            else
            {
                qtyfail++;
                return "Fail";
            }
        }

        private void GenerateCode_Send(int qty)
        {
            db.eventname = "Generate : " + qty;
            db.uom = "Vial";
            db.systemlog();
            db.uom = "";
            Code.batch = batch;
            Code.audit = true;
            Code.generateCap(qty);
            Code.generateVial(qty);
            Code.audit = false;
        }

        public void SetDataGrid_Send(string status, string banyak)
        {
            if (bl.InvokeRequired)
            {
                bl.Invoke(new Action<string, string>(SetDataGrid_Send), new object[] { status, banyak });
                return;
            }
            List<string> field = new List<string>();
            field.Add(NAMA_FIELD + " 'Cap ID'");
            string limit = "";
            if (banyak.Length > 0)
                limit = "limit " + banyak;
            List<string[]> ds = sqlite.select(field, NAMA_TABLE_TEMP, "isused = '" + status + "' " + limit);
            if (sqlite.num_rows > 0)
            {
                tableProductSend = sqlite.dataHeader;
                bl.dgvCapId.DataSource = tableProductSend;
                foreach (string[] Row in ds)
                {
                    DataRow row = tableProductSend.NewRow();
                    row[0] = Row[0];
                    tableProductSend.Rows.Add(row);
                }

                bl.dgvCapId.DataSource = tableProductSend;
            }
        }

        public bool GetBatchnumber(string batch)
        {
            List<string> field = new List<string>();
            field.Add("batchnumber");
            string where = "batchnumber ='" + batch + "' and status ='1'";
            List<string[]> ds = db.selectList(field, "packaging_order", where);
            if (db.num_rows > 0)
                return true;
            else
                return false;
        }

        public void SetClearDataGridReceive()
        {
            firstPass = false;
            //qtypass = 0;
            //qtyfail = 0;
            tableProductReceive = new DataTable();
            tableProductReceive.Columns.Add("Cap ID");
            tableProductReceive.Columns.Add("GS1 ID");
            tableProductReceive.Columns.Add("Timestamp");
            tableProductReceive.Columns.Add("Status");
            bl.dgvReceive.DataSource = tableProductReceive;
            //bl.lblQuantityReceive.Text = "0";
            //quantityReceive = 0;
            bl.lblQtyNow.Text = "0";
            receiveNow = 0;
            setPassFail();
        }

        public void SetClearDataGridCap()
        {
            tableProductReceive = new DataTable();
            tableProductReceive.Columns.Add("Cap ID");
            bl.dgvCapId.DataSource = tableProductReceive;
            bl.lblQtyCapid.Text = "0";
        }

        public void SetClearDataGridGS()
        {
            tableProductReceive = new DataTable();
            tableProductReceive.Columns.Add("GS1 ID");
            bl.dgvGsone.DataSource = tableProductReceive;
            bl.lblQtyGs.Text = "0";
        }

        void setPassFail()
        {
            bl.lblQtyPass.Text = "" + qtypass;
            bl.lblQtyFail.Text = "" + qtyfail; 
        }

        public void SendToPrinter(List<string> data)
        {
            Code.kirimPrinter(data);
            Dictionary<string, string> field = new Dictionary<string, string>();
            field.Add("isused", "1");
            field.Add("count", "" + countRepeatSend);
            foreach (string da in data)
            {
               // log("Send To Printer : " + da);
                sqlite.update(field, NAMA_TABLE_TEMP, NAMA_FIELD + " = '" + da + "'");
            }
            getdatasend();
            templastcountkirim = templastcountkirim + data.Count;
            db.updatesequence(batch, "" + templastcountkirim);
            CheckRegenerateData();
            db.eventname = "Send To Printer";
            db.systemlog();
            countRepeatSend++;
        }

        private void getdatasend()
        {
            dataSendToPrinter = new List<string>();
            List<string> field = new List<string>();
            field.Add(NAMA_FIELD);
            string where = "isused = 1";
            List<string[]> ds = sqlite.select(field, NAMA_TABLE_TEMP, where);

            foreach (string[] da in ds)
            {
                dataSendToPrinter.Add(da[0]);
            }
        }

        public List<string> getDataSendToPrinter(int count)
        {
            List<string> field = new List<string>();
            field.Add(NAMA_FIELD);
            string where = "isused = '0' order by " + NAMA_FIELD + " asc limit " + count;
            List<string[]> ds = sqlite.select(field, NAMA_TABLE_TEMP, where);
            List<string> temp = new List<string>();
            // ambil id nya saja 
            int i = 0;
            foreach (string[] data in ds)
            {
                temp.Add(ds[i][0]);
                i++;
            }
            return temp;
        }

        public List<string[]> getDataSendToPrintergs(int count)
        {
            List<string> field = new List<string>();
            field.Add("gtin,batchnumber,expire,sn,gsone,expireDate");
            string where = "isused = '0' order by gsone asc limit " + count;
            List<string[]> ds = sqlite.select(field, "tmp_gsOneVial", where);
            return ds;
        }

        //list array yang akan dikirim ke database;
        public List<string> dataCap;
        public List<string[]> dataGs;

        //Flow thread
        public void fillData()
        {
            //  resetSendData();
            //  setDataCap();
            setDataGs();
            CheckRegenerateData();

        }

        //isi data yang akan dikirim
        public void setDataCap()
        {
            dataCap = getDataSendToPrinter(int.Parse(sqlite.config["qtysend"]));
        }

        public void setDataGs()
        {
            int count = 30 * int.Parse(sqlite.config["again"]);
            //log("Send to Domino " + count);
            dataGs = getDataSendToPrintergs(count);
        }

        //reset data
        public void resetSendData()
        {
            dataCap = new List<string>();
            dataGs = new List<string[]>();
        }

        public void ReGenerateData()
        {
            //Code.lastCount = db.getcountProduct(batch) + 1;
            GenerateCode_Send(qtyGenerateCode);
        }

        public void GetRegenerateData_Capid()
        {
            int setengahgenerate = Var.QTYGenerate / 2;
            if (GetDataNotSend(NAMA_TABLE_TEMP_capid) < setengahgenerate)
            {
                log("Generate Code CapID:" + qtyGenerateCode);
                db.eventname = "Generate : " + qtyGenerateCode;
                db.uom = "Vial";
                db.systemlog();
                db.uom = "";
                Code.batch = batch;
                Code.audit = true;
                Code.generateCap(qtyGenerateCode);
            }
        }

        public void GetRegenerateData_GS()
        {
            //int setengahgenerate = qtyGenerateCode / 2;
            if (GetDataNotSend(NAMA_TABLE_TEMP_GS) < qtyGenerateCode)
            {
                log("Generate Code GS1 ID:" + qtyGenerateCode);
                db.eventname = "Generate : " + qtyGenerateCode;
                db.uom = "Vial";
                db.systemlog();
                db.uom = "";
                Code.batch = batch;
                Code.audit = true;
                Code.generateVial(qtyGenerateCode);
            }
        }

        public void CheckRegenerateData()
        {
            int setengahgenerate = Var.QTYGenerate / 2;
            if (GetDataNotSend() < setengahgenerate)
            {
                log("Generate Code :" + Var.QTYGenerate);
                ReGenerateData();
                tempTerima = 0;
                SetDataGridSendgs("0", "");
            }
        }

        public int GetDataNotSend()
        {
            List<string> field = new List<string>();
            field.Add("count(*)");
            string where = "isused = '0'";
            List<string[]> ds = sqlite.select(field, NAMA_TABLE_TEMP, where);
            if (sqlite.num_rows > 0)
            {
                return int.Parse(ds[0][0]);
            }
            else
            {
                return 0;
            }
        }

        public int GetDataNotSend(string tabel)
        {
            List<string> field = new List<string>();
            field.Add("count(*)");
            string where = "isused = '0'";
            List<string[]> ds = sqlite.select(field, tabel, where);
            if (sqlite.num_rows > 0)
            {
                return int.Parse(ds[0][0]);
            }
            else
            {
                return 0;
            }
        }

        void bg_GetDeviceStatus()
        {
            worker.DoWork += new DoWorkEventHandler(worker_DoWork);
            worker.WorkerReportsProgress = true;
            worker.RunWorkerAsync();
            resetEvent.WaitOne();
        }

        void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            GetDeviceStatus();
            resetEvent.Set();
        }

        public void Start_Form()
        {
            bl.btnAudit.Enabled = false;
            bl.btnDeco.Enabled = false;
            bl.btnLogout.Enabled = false;
            bl.btnConfig.Enabled = false;
            bl.btnSample.Enabled = false;

            bl.btnCheckIndicator.Enabled = false;
            bl.pictureBox1.Enabled = false;
        }

        public void Stop_Form()
        {
            try
            {
                if (!bl.startCap && !bl.startGs)
                {
                    bl.btnAudit.Enabled = true;
                    //bl.cmbBatch.Enabled = true;
                    bl.txtBatchNumber.Enabled = true;
                    bl.btnAudit.Enabled = true;
                    bl.btnDeco.Enabled = true;
                    bl.btnLogout.Enabled = true;
                    bl.btnConfig.Enabled = true;
                    bl.btnSample.Enabled = true;
                    //GetComboBoxData();
                    db.eventname = "Finish Print";
                    db.systemlog();
                    sqlite.updatestatusconfig("");

                    bl.btnCheckIndicator.Enabled = true;
                    bl.pictureBox1.Enabled = true;
                }
            }
            catch (NullReferenceException nf)
            {

            }
        }

        //public void GetComboBoxData()
        //{
        //    List<string> field = new List<string>();
        //    field.Add("batchNumber,productModelId");
        //    string where = "status = '1' and linepackagingid = '" + linenumber + "'";
        //    datacombo = db.selectList(field, "[packaging_order]", where);
        //    List<string> da = new List<string>();
        //    for (int i = 0; i < datacombo.Count; i++)
        //    {
        //        da.Add(datacombo[i][0]);
        //    }
        //    bl.cmbBatch.DataSource = da;
        //}

        private List<string> GetSplitReceiveData(string data)
        {
            char[] delimiters = new char[] { '|' };
            List<string> hasilSplit = new List<string>(data.Split(delimiters, StringSplitOptions.RemoveEmptyEntries));
            return hasilSplit;
        }

        public void SetDataGridSendgs(string status, string banyak)
        {
            if (bl.InvokeRequired)
            {
                bl.Invoke(new Action<string, string>(SetDataGridSendgs), new object[] { status, banyak });
                return;
            }

            List<string> field = new List<string>();
            field.Add("gsone 'GS1 ID '");
            string limit = "";
            if (banyak.Length > 0)
                limit = "limit " + banyak;
            List<string[]> ds = sqlite.select(field, "tmp_gsOneVial", "isused = '" + status + "' " + limit);
            if (sqlite.num_rows > 0)
            {
                tableProductSend = sqlite.dataHeader;
                bl.dgvGsone.DataSource = tableProductSend;
                foreach (string[] Row in ds)
                {
                    DataRow row = tableProductSend.NewRow();
                    row[0] = Row[0];
                    tableProductSend.Rows.Add(row);
                }

                bl.dgvGsone.DataSource = tableProductSend;
            }
        }
        public void cleanDataTemp()
        {
            DataTempCapid = new List<string>();
        }
        public void cleanDataTempgs()
        {
            DataTempGSone = new List<string>();
        }

        public void ReceiveData(string data)
        {
            try
            {
                if (bl.InvokeRequired)
                {
                    bl.Invoke(new Action<string>(ReceiveData), new object[] { data });
                    return;
                }

                if (data.Length > 0)
                {
                    data = Function_Set_Data.RemoveCharFromCamera(data);
                    List<string> dataMas = GetSplitReceiveData(data);
                    for (int i = 0; i < dataMas.Count; i++)
                    {
                       // log("Proses Data:" + dataMas[i]);
                        string[] hasilSplit = dataMas[i].Split(delimit);

                        if (hasilSplit.Length == 3)
                        {
                            if (bl.cekDB())
                            {
                                if (hasilSplit[0].Equals("0"))
                                {
                                    if (!hasilSplit[1].Equals("XXXXXX") || !hasilSplit[1].Equals("XXXXX") || !hasilSplit[1].Equals("XXXXXXX") || !hasilSplit[1].Equals("xxxxx") || !hasilSplit[1].Equals("xxxxxx") || !hasilSplit[1].Equals("xxxxxxx"))
                                    {
                                        InsertToDatabase(hasilSplit[1], hasilSplit[2], hasilSplit[0]);
                                    }
                                    else
                                    {
                                        //UpdateDataGrid_Receive("XXXXXX", "0", "4");
                                        UpdateDataGrid_Receive(hasilSplit[1], hasilSplit[2], "4");
                                        InsertToDatabaseReject(hasilSplit[1], hasilSplit[2], "4");
                                        SendData_PLC("1");
                                        if (auditStarted)
                                            CheckReSendData();
                                    }
                                }
                                else
                                {
                                    //UpdateDataGrid_Receive("0", "0", "1");
                                    InsertToDatabaseReject(hasilSplit[1], hasilSplit[2], hasilSplit[0]);
                                    if (auditStarted)
                                        CheckReSendData();
                                }
                                db.closeConnection();
                            }
                            else
                            {
                                SendData_PLC("1");
                                if (db.fcon < 2)
                                {
                                    new Confirm("Can not open connection to Database Server.", "Information", MessageBoxButtons.OK);
                                    config cfg = new config(null, null, 0, "");
                                    cfg.ShowDialog();
                                }
                            }
                        }
                        else 
                        {
                            UpdateDataGrid_Receive(hasilSplit[1], hasilSplit[2], "");
                            log("Data :" + dataMas[i] + " Not Recognized");
                            SendData_PLC("1");
                        }
                        
                    }
                }
                else
                {
                    UpdateDataGrid_Receive("", "", "");
                    log("Data Not Recognized");
                }
            }
            catch (Exception ex)
            {
                //log("Error :" + ex.Message);
            }
        }

        private bool cekCapidWithBatch(string datacapid)
        {
            List<string> field = new List<string>();
            field.Add("capid");
            string where = "batchnumber ='" + batch + "' and capid ='"+datacapid+"'";
            List<string[]> ds = db.selectList(field, "vaccine", where);
            if (db.num_rows > 0)
                return false;
            else
                return true;
        }

        private bool cekExistCapid()
        {
            return true;
        }

        public void SendData_PLC(string data)
        {
            try
            {
                sendData_plc.TextData = data;
                sendData_plc.SendText();
              //  sendData_plc.Disconect();
            }
            catch (Exception)
            {
                new Confirm("Something wrong with PLC", "Information", MessageBoxButtons.OK);
            }
        }

        private void InsertToDatabase(string dataCapid, string dataGsone, string status)
        {
            if (batch != null && exp != null && productmodelid != null)
            {
                if (batch.Length > 0 && exp.Length > 0 && productmodelid.Length > 0)
                {
                    Dictionary<string, string> field = new Dictionary<string, string>();
                    field.Add("capid", dataCapid);
                    field.Add("gsonevialid", dataGsone);
                    field.Add("flag", "0");
                    field.Add("batchnumber", batch);
                    field.Add("productModelId", productmodelid);
                    field.Add("expdate", exp);
                    field.Add("createdtime", db.getUnixString());
                    field.Add("isreject", status);
                    field.Add("linenumber", linenumber);
                    if (!db.insert(field, "vaccine"))
                    {
                        if (db.errorMessage.Contains("duplicate"))
                        {
                            if (db.errorMessage.Contains("PRIMARY"))
                            {
                                status = "2";
                                field.Remove("isreject");
                                field.Add("isreject", "5");
                                db.insert(field, "vaccine_reject");
                            }
                            else
                            {
                                status = "3";
                                field.Remove("isreject");
                                field.Add("isreject", "6");
                                db.insert(field, "vaccine_reject");
                            }
                            SendData_PLC("1");
                        }
                        else if (db.errorMessage.Contains("connection"))
                        {
                            InsertToDatabaseTemp(dataCapid, dataGsone, status);
                            SendData_PLC("1");
                        }
                        else 
                        {
                            status = "1";
                        }
                    }
                    db.Movement(batch, dataCapid, dataGsone, "1", "");
                }
                else
                {
                    status = "1";
                    log("Data not saved. Start production GS1");
                }
            }
            else
            {
                status = "1";
                log("Data not saved. Start production GS1");
            }
                
            UpdateDataGrid_Receive(dataCapid, dataGsone, status);
            if (status == "2" || status == "3")
            {
                new Confirm("\tAlready Exist\nCapid " + dataCapid + "\nGS1ID: " + dataGsone, "Information", MessageBoxButtons.OK);
                                                        
            }
            if (auditStarted)
                CheckReSendData();

        }

        //tambahan
        private void InsertToDatabaseTemp(string dataCapid, string dataGsone, string status)
        {
            if (batch != null && exp != null && productmodelid != null)
            {
                if (batch.Length > 0 && exp.Length > 0 && productmodelid.Length > 0)
                {
                    Dictionary<string, string> field = new Dictionary<string, string>();
                    field.Add("capid", dataCapid);
                    field.Add("gsonevialid", dataGsone);
                    field.Add("flag", "0");
                    field.Add("batchnumber", batch);
                    field.Add("productModelId", productmodelid);
                    field.Add("expdate", exp);
                    field.Add("createdtime", db.getUnixString());
                    field.Add("isreject", status);
                    field.Add("linenumber", linenumber);
                    if (!sqlite.insert(field, "vaccine"))
                    {
                        if (sqlite.errorMessage.Contains("duplicate"))
                        {
                            if (sqlite.errorMessage.Contains("PRIMARY"))
                            {
                                status = "2";
                                field.Remove("isreject");
                                field.Add("isreject", "5");
                                sqlite.insert(field, "vaccine_reject");
                            }
                            else
                            {
                                status = "3";
                                field.Remove("isreject");
                                field.Add("isreject", "6");
                                sqlite.insert(field, "vaccine_reject");
                            }
                            SendData_PLC("1");
                        }
                        else
                        {
                            status = "1";
                        }
                    }
                }
                else
                {
                    status = "1";
                    log("Data not saved. Start production GS1");
                }
            }
            else
            {
                status = "1";
                log("Data not saved. Start production GS1");
            }

            UpdateDataGrid_Receive(dataCapid, dataGsone, status);
            if (status == "2" || status == "3")
            {
                new Confirm("\tAlready Exist\nCapid " + dataCapid + "\nGS1ID: " + dataGsone, "Information", MessageBoxButtons.OK);

            }
            if (auditStarted)
                CheckReSendData();

        }

        private void InsertToDatabaseReject(string dataCapid, string dataGsone, string status)
        {
            if (batch != null && exp != null && productmodelid != null)
            {
                if (batch.Length > 0 && exp.Length > 0 && productmodelid.Length > 0)
                {
                    string table = null;
                    Dictionary<string, string> field = new Dictionary<string, string>();
                    field.Add("capid", dataCapid);
                    field.Add("gsonevialid", dataGsone);
                    field.Add("flag", "0");
                    field.Add("batchnumber", batch);
                    field.Add("productModelId", productmodelid);
                    field.Add("expdate", exp);
                    field.Add("createdtime", db.getUnixString());
                    field.Add("isreject", status);
                    field.Add("linenumber", linenumber);
                    if (!db.insert(field, "vaccine_reject"))
                    {
                        if (db.errorMessage.Contains("connection"))
                        {
                            InsertToDatabaseRejectTemp(dataCapid, dataGsone, status);
                            SendData_PLC("1");
                        }
                    }
                }
                else
                {
                    status = "1";
                    log("Data not saved. Start production GS1");
                }
            }
            else
            {
                status = "1";
                log("Data not saved. Start production GS1");
            }

            UpdateDataGrid_Receive(dataCapid, dataGsone, status);
            if (auditStarted)
                CheckReSendData();

        }

        private void InsertToDatabaseRejectTemp(string dataCapid, string dataGsone, string status)
        {
            if (batch != null && exp != null && productmodelid != null)
            {
                if (batch.Length > 0 && exp.Length > 0 && productmodelid.Length > 0)
                {
                    string table = null;
                    Dictionary<string, string> field = new Dictionary<string, string>();
                    field.Add("capid", dataCapid);
                    field.Add("gsonevialid", dataGsone);
                    field.Add("flag", "0");
                    field.Add("batchnumber", batch);
                    field.Add("productModelId", productmodelid);
                    field.Add("expdate", exp);
                    field.Add("createdtime", db.getUnixString());
                    field.Add("isreject", status);
                    field.Add("linenumber", linenumber);
                    sqlite.insert(field, "vaccine_reject");
                }
                else
                {
                    status = "1";
                    log("Data not saved. Start production GS1");
                }
            }
            else
            {
                status = "1";
                log("Data not saved. Start production GS1");
            }

            UpdateDataGrid_Receive(dataCapid, dataGsone, status);
            if (auditStarted)
                CheckReSendData();

        }
        //end tambahan

        int quantityReceive = 0;
        int receiveNow = 0;
        bool firstPass;
        public void UpdateDataGrid_Receive(string capid, string gsone, string status)
        {
            
            string dates = DateTime.Now.ToString("HH:mm:ss");
            DataRow row = tableProductReceive.NewRow();
            row[0] = capid;
            row[1] = gsone;
            row[2] = dates;
            row[3] = GetStatus(status);
            tableProductReceive.Rows.InsertAt(row, 0);
            bl.dgvReceive.DataSource = tableProductReceive;
            
            setPassFail();
            if (!firstPass)
            {
                if (status == "0")
                {
                    firstPass = true;
                    //set autosize mode
                    bl.dgvReceive.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                    bl.dgvReceive.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                    bl.dgvReceive.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                    bl.dgvReceive.Columns[3].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;

                    //datagrid has calculated it's widths so we can store them

                    //asalnya ga di comment
                    //for (int i = 0; i <= bl.dgvReceive.Columns.Count - 1; i++)
                    //{
                    //    //store autosized widths
                    //    int colw = bl.dgvReceive.Columns[i].Width;
                    //    //remove autosizing
                    //    bl.dgvReceive.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
                    //    //set width to calculated by autosize
                    //    bl.dgvReceive.Columns[i].Width = colw;
                    //}
                }
            }
            

            bl.dgvReceive.ClearSelection();
            bl.dgvReceive.Rows[0].Selected = true;
            quantityReceive++;
            receiveNow++;
            bl.lblQuantityReceive.Text = "" + quantityReceive;
            bl.lblQtyNow.Text = "" + receiveNow;
            if(bl.dgvReceive.Rows.Count>100){
                bl.dgvReceive.Rows.RemoveAt(99);
                tableProductReceive.Rows.RemoveAt(99);

            }
            bl.dgvReceive.FirstDisplayedScrollingRowIndex = 1;
            //quantityReceive++;
            //bl.lblQuantityReceive.Text = ""+quantityReceive;
            
        }

        private bool CheckData_Sample(string capid)
        {
            List<string> field = new List<string>();
            field.Add("capid");
            string where = "flag = 1 And capid ='" + capid + "'";
            List<string[]> ds = sqlite.select(field, "tmp_cap_id", where);
            if (sqlite.num_rows > 0)
            {
                return true;
            }
            return false;
        }

        public bool CheckData_GS(string blister)
        {
            List<string> field = new List<string>();
            field.Add("gsone");
            string where = "gsone = '" + blister + "'";
            sqlite.select(field, "tmp_gsOneVial", where);
            if (sqlite.num_rows > 0)
                return true;
            else
                return false;
        }

        public bool CheckData_Capid(string bottle)
        {
            List<string> field = new List<string>();
            field.Add(NAMA_FIELD);
            string where = NAMA_FIELD + " = '" + bottle + "' and isused = '1'";
            sqlite.select(field, NAMA_TABLE_TEMP, where);
            if (sqlite.num_rows > 0)
                return true;
            else
                return false;
        }

        public void CheckReSendData()
        {
            if (tempTerimaKirimUlang >= 10)
            {
                SendToPrinter(dataGs);
                fillData();
                tempTerimaKirimUlang = 1;
                log("Send to Printer : 30");
               // Code.generateVial(5);
                count++;
            }
            else
            {
                tempTerimaKirimUlang++;
            }

        }

        public void GetDeviceStatus()
        {
            if (PingHost(sqlite.config["ipCamera"]))
                setform.Camera1_Online();
            else
                setform.Camera1_Offline();

            if (PingHost(sqlite.config["ipcamera2"]))
                setform.Camera2_Online();
            else
                setform.Camera2_Offline();
        }

        public bool PingHost(string nameOrAddress)
        {
            if (nameOrAddress.Length == 0)
                return false;
            bool pingable = false;
            Ping pinger = new Ping();
            try
            {
                PingReply reply = pinger.Send(nameOrAddress);
                pingable = reply.Status == IPStatus.Success;
            }
            catch (PingException)
            {
                // Discard PingExceptions and return false;
            }
            return pingable;
        }

        internal void clearBuffer_Cap()
        {
            print pr = new print();
            pr.cekKoneksi();
            pr.clearBuffer();
            pr.closeCap();
        }

        internal void clearBuffer_Gs()
        {
            print pr2 = new print();
            pr2.cekKoneksigs();
            pr2.clearBuffergs();
            pr2.closeGs();
        }

        internal void refresh()
        {

            //GetComboBoxData();
            tableProductReceive = new DataTable();
            tableProductReceive.Columns.Add("Cap ID");
            tableProductReceive.Columns.Add("GS1 ID");
            tableProductReceive.Columns.Add("Timestamp");
            tableProductReceive.Columns.Add("Status");
            bl.dgvReceive.DataSource = tableProductReceive;
            bl.lblQuantityReceive.Text = "0";
            quantityReceive = 0;
            //Inisialisasi variabel Sqlite
            Var.SetSqliteVariabel(sqlite.config);

            if (sqlite.config["buffer"].Equals("1"))
            {
                //delay();
                isBuffer = true;
            }
            else
            {
                isBuffer = false;
            }
            //tableProductFail = new DataTable();
            //tableProductFail.Columns.Add("Cap ID");
            //tableProductFail.Columns.Add("GS1 ID");
            //bl.dgvFail.DataSource = tableProductFail;
            bl.lblQtyFail.Text = "0";
            GetDeviceStatus();
        }

        internal void cekBufferPrinter(string Message)
        {
            SendToPrinter(getDataSendToPrintergs(int.Parse(sqlite.config["qtysend"])));
        }

        internal string GetAdminName(string admin)
        {
            List<string> field = new List<string>();
            field.Add("first_name   ");
            string where = "id ='" + admin + "'";
            List<string[]> ds = db.selectList(field, "[USERS]", where);
            if (db.num_rows > 0)
                return ds[0][0];
            else
                return "";
        }

        internal void SetForm_PLC_Online()
        {
          //  bl.lblPLC.Text = "Online";
        }

        internal void SetForm_PLC_Offline()
        {
           // bl.lblPLC.Text = "Offline";
        }

        internal void startListenerPLC()
        {

        }

        public string getBatchSqlite()
        {
            return sqlite.selectBatch();
        }

        public int[] getCount(string btch)
        {
            int[] cnt = new int[3];
            qtypass = db.selectCountPass(btch);
            qtyfail = db.selectCountReject(btch);
            quantityReceive = qtypass + qtyfail;
            cnt[0] = qtypass;
            cnt[1] = qtyfail;
            cnt[2] = quantityReceive;
            return cnt;
        }

    }
}
