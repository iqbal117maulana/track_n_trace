﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Net.Sockets;
using System.Net;

namespace Bottle_Station
{
    public class ServerMultiClientFlubio
    {

        //  static List<Listener> listeners = new List<Listener>();
        ControlFlubio ctrl;
        bool isRunning = true;

        TcpClient client;
        TcpListener listener;
        public void Start(ControlFlubio ctrl1, string port)
        {
            try
            {
                ctrl = ctrl1;
                listener = new TcpListener(IPAddress.Any, int.Parse(port));

                listener.Start();

                while (isRunning) // Add your exit flag here
                {
                    if (!listener.Pending())
                    {
                        Thread.Sleep(500); // choose a number (in milliseconds) that makes sense
                        continue; // skip to next iteration of loop
                    }
                    client = listener.AcceptTcpClient();
                    ThreadPool.QueueUserWorkItem(ThreadProc, client);
                }
            }
            catch (Exception ex)
            {
                new Confirm("Port aleady used:" + port + ", Restart Application", "Information", System.Windows.Forms.MessageBoxButtons.OK);
                Environment.Exit(Environment.ExitCode);
            }
        }

        public void closeServer(string data)
        {
            isRunning = false;
            if (client != null)
            {
                client.Close();
            }
            listener.Stop();

        }
        //## untuk applikasi langsung dari kamera
        //        private void ThreadProc(object obj)
        //        {
        //            string bufferincmessage;
        //            TcpClient client = (TcpClient)obj;
        //            NetworkStream clientStream = client.GetStream();
        //            ctrl.log("Connected ");
        //            byte[] message = new byte[4096];
        //            int bytesRead;
        //            while (isRunning)
        //            {
        //                bytesRead = 0;
        //                try
        //                {
        //                    //blocks until a client sends a message

        //                    ctrl.log("Waiting data");
        //                    bytesRead = clientStream.Read(message, 0, 4096);
        //                }
        //                catch
        //                {
        //                    //a socket error has occured
        //                    break;
        //                }
        //                if (bytesRead == 0)
        //                {
        //                    //the client has disconnected from the server
        //                    break;
        //                }

        //                //message has successfully been received
        //                ASCIIEncoding encoder = new ASCIIEncoding();
        //                bufferincmessage = encoder.GetString(message, 0, bytesRead);

        //                ctrl.log("Receive : " + bufferincmessage);
        //                ctrl.tambahDataBaru(bufferincmessage);
        //            }


        //        }

        string tampung = "";
        public void clearCache()
        {
            tampung = "";
        }
        private void ThreadProc(object obj)
        {
            string bufferincmessage;
            TcpClient client = (TcpClient)obj;
            NetworkStream clientStream = client.GetStream();
            ctrl.log("Connected ");
            ctrl.SetForm_PLC_Online();
            byte[] message = new byte[4096];
            int bytesRead;
            int Counting = 0;
            int countSucces = 0;
            while (isRunning)
            {
                bytesRead = 0;
                try
                {
                    ctrl.log("Waiting data");
                    bytesRead = clientStream.Read(message, 0, 4096);
                }
                catch
                {
                    break;
                }
                if (bytesRead == 0)
                {
                    break;
                }

                //message has successfully been received
                ASCIIEncoding encoder = new ASCIIEncoding();
                bufferincmessage = encoder.GetString(message, 0, bytesRead);

                ctrl.log("Receive : " + bufferincmessage);
                //tampung = tampung + bufferincmessage;
                if (bufferincmessage.Contains('|'))
                {
                    string dataKirim = tampung;
                    if (bufferincmessage.Contains('$'))
                    {
                        string[] temp = bufferincmessage.Split(new char[] { '$' }, StringSplitOptions.RemoveEmptyEntries);
                        if (temp.Length > 1)
                        {
                            dataKirim = temp[0];
                            bool hapusTampung = false;
                            for (int i = 0; i < temp.Length; i++)
                            {
                                if (temp[i].Contains('|'))
                                {
                                    ctrl.ReceiveData(temp[i]);
                                }
                                //else
                                //{
                                    //    tampung = "$" + temp[i];
                                    // hapusTampung = true;
                                    //    ctrl.log(tampung);
                                //}
                            }
                            //if (!hapusTampung)
                            //{
                            //   tampung = "";
                            //}
                        }
                        else
                        {

                            ctrl.ReceiveData(bufferincmessage);
                            //ctrl.ReceiveData(tampung);
                            tampung = "";
                        }
                    }
                    else
                    {
                        ctrl.log("Not Contains Dollar [ $ ]");
                    }
                    Counting = 0;
                }
                else
                {
                    ctrl.log("Not Contains Pipe [ | ]");
                    Counting++;
                }
            }


        }

    }

    class ListenerFlubio
    {
        Thread listenThread;
        string bufferincmessage;
        TcpListener tcplistener;
        ControlFlubio ctrl;
    }
}