﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Finisar.SQLite;
using System.IO;
using System.Windows.Forms;

namespace Bottle_Station
{
    public class sqlitecs
    {
        private SQLiteConnection sql_con;
        private SQLiteCommand sql_cmd;
        private SQLiteDataAdapter DB;
        private SQLiteDataReader datareader;
        public  DataTable dataHeader;
        private DataSet DS = new DataSet();
        private DataTable DT = new DataTable();
        public int num_rows = 0;
        public List<string> dataInsert = new List<string>();
        public List<string> dataUpdate = new List<string>();
        public Dictionary<string, string> config = new Dictionary<string, string>();
        bool isConnect = false;
        public string errorMessage;

        public sqlitecs()
        {
            SetConnection();
            getConfig();
        }

        private void SetConnection()
        {
            if (!isConnect)
            {
                try
                {
                    sql_con = new SQLiteConnection
                    ("Data Source=config.db;Version=3;New=False;Compress=True;");
                    sql_con.Open();
                    isConnect = true;
                }
                catch(Exception e)
                {
                    errorMessage = e.Message;
                }
                
            }
        }
        
        public void ExecuteQuery(string txtQuery)
        {
            try
            {
                SetConnection();
                sql_cmd = sql_con.CreateCommand();
                sql_cmd.CommandText = txtQuery;
                sql_cmd.ExecuteNonQuery();
            }
            catch (Exception sq)
            {
                try
                {
                    isConnect = false;
                    sql_con.Close();
                    SetConnection();
                    simpanLog("Error : " + sq.ToString());
                }catch(Exception ex)
                {
                    new Confirm("Error SQLLite : " + ex.ToString(), "Error");
                }
            }
        }
        
        public void Add(string data)
        {
            string txtSQLQuery = "insert into  innerbox (innerbox) values ('" + data + "')";
            ExecuteQuery(txtSQLQuery);
        }

        public bool insert(Dictionary<string,string> field, string table)
        {
            try
            {
                string sql = "INSERT INTO " + table + " (";
                int i = 0;
                foreach (string key in field.Keys)
                {
                    sql += "'" + key + "'";
                    if (i + 1 < field.Count)
                    {
                        sql += ",";
                        i++;
                    }
                }

                sql += ") values (";

                i = 0;
                foreach (string key in field.Values)
                {
                    sql += "'" + key + "'";
                    if (i + 1 < field.Count)
                    {
                        sql += ",";
                        i++;
                    }
                }
                sql += ")";
                simpanLog(sql);
                ExecuteQuery(sql);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public void simpanLog(String line)
        {
            DateTime now = DateTime.Now;
            string tahun = now.ToString("yyyy");
            string bulan = now.ToString("MM");
            string hari = now.ToString("dd");
            string dates = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            line = dates + "\t" + line;
            // cek file 
            bool cekfile = false;
            string namaFolder = "eventlog\\Sqlite";
            try
            {
                while (!cekfile)
                {
                    if (Directory.Exists(namaFolder))
                    {
                        if (Directory.Exists(namaFolder + @"\" + tahun))
                        {
                            if (Directory.Exists(namaFolder + @"\" + tahun + @"\" + bulan))
                            {

                                using (StreamWriter outputFile = new StreamWriter(namaFolder + @"\" + tahun + @"\" + bulan + @"\" + hari + ".txt", true))
                                {
                                    outputFile.WriteLine(line);
                                }
                                cekfile = true;
                            }
                            else
                            {

                                Directory.CreateDirectory(namaFolder + @"\" + tahun + @"\" + bulan);
                            }
                        }
                        else
                        {
                            Directory.CreateDirectory(namaFolder + @"\" + tahun);
                        }
                    }
                    else
                    {

                        Directory.CreateDirectory(namaFolder);
                    }
                }
            }
            catch (Exception ex)
            {
                simpanLog(ex.Message);
            }
        }

        private void getConfig()
        {
            try
            {
                string sql = "Select config.portServer," +
                                "config.Servername," +
                                "config.ipprintercap," +
                                "config.portprintercap," +
                                "config.ipCamera," +
                                "config.portCamera," +
                                "config.ipDB," +
                                "config.portDB," +
                                "config.usernameDB," +
                                "config.passDB," +
                                "config.ipServer," +
                                "config.lineNumber," +
                                "config.portlisten," +
                                "config.timeout," +
                                "config.flag," +
                                "config.batchnumber," +
                                "config.ipprintermarker," +
                                "config.portprintermarker," +
                                "config.ipCamera2," +
                                "config.portCamera2," +
                                "config.audit," +
                                "config.namadb," +
                                "config.qtysend," +
                                "config.qtygenerate," +
                                "config.delay," +
                                "config.debug," +
                                "config.buffer," +
                                "config.again," +
                                "config.second," +
                                "config.ipPLC," +
                                "config.PortPLC" +
                                " From config";
                SetConnection();
                sql_cmd = new SQLiteCommand(sql, sql_con);
                datareader = sql_cmd.ExecuteReader();
                while (datareader.Read())
                {
                    config.Add("portserver", datareader.GetValue(0).ToString());
                    config.Add("Servername", datareader.GetValue(1).ToString());
                    config.Add("ipprinter", datareader.GetValue(2).ToString());
                    config.Add("portprinter", datareader.GetValue(3).ToString());
                    config.Add("ipCamera", datareader.GetValue(4).ToString());
                    config.Add("portCamera", datareader.GetValue(5).ToString());
                    config.Add("ipDB", datareader.GetValue(6).ToString());
                    config.Add("portDB", datareader.GetValue(7).ToString());
                    config.Add("usernameDB", datareader.GetValue(8).ToString());
                    config.Add("passDB", datareader.GetValue(9).ToString());
                    config.Add("ipServer", datareader.GetValue(10).ToString());
                    config.Add("lineNumber", datareader.GetValue(11).ToString());
                    config.Add("portlisten", datareader.GetValue(12).ToString());
                    config.Add("timeout", datareader.GetValue(13).ToString());
                    config.Add("flag", datareader.GetValue(14).ToString());
                    config.Add("batchnumber", datareader.GetValue(15).ToString());
                    config.Add("ipmarker", datareader.GetValue(16).ToString());
                    config.Add("portmarker", datareader.GetValue(17).ToString());
                    config.Add("ipcamera2", datareader.GetValue(18).ToString());
                    config.Add("portcamera2", datareader.GetValue(19).ToString());
                    config.Add("audit", datareader.GetValue(20).ToString());
                    config.Add("namaDB", datareader.GetValue(21).ToString());
                    config.Add("qtysend", datareader.GetValue(22).ToString());
                    config.Add("qtygenerate", datareader.GetValue(23).ToString());
                    config.Add("delay", datareader.GetValue(24).ToString());
                    config.Add("debug", datareader.GetValue(25).ToString());
                    config.Add("buffer", datareader.GetValue(26).ToString());
                    config.Add("again", datareader.GetValue(27).ToString());
                    config.Add("second", datareader.GetValue(28).ToString());
                    config.Add("IpPlc", datareader.GetValue(29).ToString());
                    config.Add("PortPlc", datareader.GetValue(30).ToString());
                }
            }
            catch (Exception)
            {
                sql_con.Close();
                isConnect = false;
                SetConnection();
            }
        }

        public void update(Dictionary<string, string> field, string table, string where)
        {

            string sql = "UPDATE " + table + " SET ";
            int i = 0;
            foreach (KeyValuePair<string, string> key in field)
            {
                sql += ""+key.Key + " = " + "'" + key.Value + "'";
                if (i + 1 < field.Count)
                {
                    sql += ",";
                    i++;
                }
            }
            if (where.Length > 0)
                sql += " WHERE " + where + "";
            simpanLog(sql);
            ExecuteQuery(sql);
        }

        public void delete(string where, string table)
        {
            string sql = "DELETE FROM " + table ;

            if (where.Length > 0)
                sql += " WHERE "+where;

            simpanLog(sql);
            ExecuteQuery(sql);
        }

        public List<string[]> select(List<string> field, string table , string where)
        {
            try
            {
                string sql = "SELECT ";
                for (int j = 0; j < field.Count; j++)
                {
                    sql += field[j];
                    if (j + 1 < field.Count)
                    {
                        sql += ",";
                        j++;
                    }
                }
                sql += " From " + table;

                if (where.Length > 0)
                    sql += " WHERE " + where;

                int num = 0;
                simpanLog(sql);
                SetConnection();
                sql_cmd = new SQLiteCommand(sql, sql_con);
                datareader = sql_cmd.ExecuteReader();
                List<String[]> Results = new List<String[]>();
                while (datareader.Read())
                {
                    string[] data = new string[datareader.FieldCount];
                    for (int u = 0; u < datareader.FieldCount; u++)
                    {
                        data[u] = datareader.GetValue(u).ToString();
                    }

                    Results.Add(data);
                    num++;
                }
                num_rows = num;
                dataHeader = new DataTable();
                for (int i = 0; i < datareader.FieldCount; i++)
                {
                    dataHeader.Columns.Add(datareader.GetName(i));
                }
                return Results;
            }
            catch (Exception IO)
            {
                isConnect = false;
                sql_con.Close();
                SetConnection();
                num_rows = 0;
                return null;
               // return select(field, table, where);
            }
        }

        public void insertData(Dictionary<string, string> field, string table)
        {
            string sql = "INSERT INTO " + table + " (";
            int i = 0;
            foreach (string key in field.Keys)
            {
                sql += "'" + key + "'";
                if (i + 1 < field.Count)
                {
                    sql += ",";
                    i++;
                }
            }

            sql += ") values (";

            i = 0;
            foreach (string key in field.Values)
            {
                sql += "'" + key + "'";
                if (i + 1 < field.Count)
                {
                    sql += ",";
                    i++;
                }
            }
            sql += ");";
            dataInsert.Add(sql);
        }

        public void Begin()
        {
            string sql = "BEGIN TRANSACTION;\n ";

            for (int i = 0; i < dataInsert.Count; i++)
            {
                sql += dataInsert[i] + "\n";
            }
            sql += "COMMIT;";
           // simpanLog(sql);
            ExecuteQuery(sql);
            dataInsert = new List<string>();
        }

        public void BeginUpdate()
        {
            string sql = "BEGIN TRANSACTION;\n ";

            for (int i = 0; i < dataUpdate.Count; i++)
            {
                sql += dataUpdate[i] + "\n";
            }
            sql += "COMMIT;";
            simpanLog(sql);
            ExecuteQuery(sql);
            dataUpdate = new List<string>();
        }

        public void updateData(Dictionary<string, string> field, string table, string where)
        {

            string sql = "UPDATE " + table + " SET";
            int i = 0;
            foreach (KeyValuePair<string, string> key in field)
            {
                sql += "'" + key.Key + "' = " + "'" + key.Value + "'";
                if (i + 1 < field.Count)
                {
                    sql += ",";
                    i++;
                }
            }
            if (where.Length > 0)
                sql += " WHERE  " + where + ";";
            dataUpdate.Add(sql);
        }

        public string[] getDatacap()
        {
            List<string> field = new List<string>();
            string [] temp;
            field.Add("capid");
            string where = "isused = '0' or isused = '1'";
            List<string[]> ds = select(field, "tmp_cap_id", where);
            temp = new string[num_rows];
            int i=0;
            foreach (string[] datac in ds)
            {
                temp[i] = datac[0];
                i++;
            }
            return temp;
        }

        public List<string[]> getDatagsone()
        {
            List<string> field = new List<string>();
            List<string[]> temp;
            field.Add("gtin,batchnumber,expire,sn");
            string where = "isused = '0'";
            List<string[]> ds = select(field, "tmp_gsOneVial", where);
            return ds;
        }

        public int getDatacapAll()
        {
            List<string> field = new List<string>();
            string[] temp;
            field.Add("capid");
            string where = "isused = '0' or isused = '1'";
            List<string[]> ds = select(field, "tmp_cap_id", "");
            temp = new string[num_rows];
            int i = 0;
            foreach (string[] datac in ds)
            {
                temp[i] = datac[0];
                i++;
            }
            return temp.Length;
        }

        public int getDatagsoneAll()
        {
            List<string> field = new List<string>();
            List<string[]> temp;
            field.Add("gtin,batchnumber,expire,sn");
            string where = "isused = '0'";
            List<string[]> ds = select(field, "tmp_gsOneVial", "");
            return ds.Count;
        }

        public List<string> getDatagsonedetail()
        {
            List<string> field = new List<string>();
            List<string> temp = new List<string>() ;
            field.Add("gsone");
            string where = "isused = '0' or isused = '1'";
            List<string[]> ds = select(field, "tmp_gsOneVial", where);

            foreach (string[] da in ds)
            {
                temp.Add(da[0]);
            }
            return temp;
        }

        public void updatestatusconfig(string batch)
        {
            Dictionary<string, string> field = new Dictionary<string, string>();
            field.Add("batchnumber", batch);
            update(field, "config", "");
        }

        public List<string[]> getcaptable()
        {
            List<string> field = new List<string>();
            field.Add("capid 'Cap ID'");
            List<string[]> ds = select(field, "tmp_cap_id", "isused = '0'");
            return ds;
        }

        public int getCountCap(string status)
        {
            List<string> field = new List<string>();
            field.Add("count(*)");
            string where = "isused " + status + "";
            List<string[]> ds = select(field, "tmp_cap_id", where);
            int temp = int.Parse(ds[0][0]);
            return temp;
        }

        public int getCountGs(string status)
        {
            List<string> field = new List<string>();
            field.Add("count(*)");
            string where = "isused " + status + "";
            List<string[]> ds = select(field, "tmp_gsOneVials", where);
            int temp = int.Parse(ds[0][0]);
            return temp;
        }

        public void RefreshData()
        {
            string sql = "DELETE FROM tmp_cap_id";
            ExecuteQuery(sql);
            sql = "Delete from tmp_gsOneVial";
            ExecuteQuery(sql);
            Dictionary<string, string> field = new Dictionary<string, string>();
            field.Add("batchnumber", "");
            update(field, "config", "");
        }

        public int getDatacapAllNotReciv()
        {
            List<string> field = new List<string>();
            string[] temp;
            field.Add("capid");
            string where = "isused <> '2' ";
            List<string[]> ds = select(field, "tmp_cap_id", where);
            temp = new string[num_rows];
            int i = 0;
            foreach (string[] datac in ds)
            {
                temp[i] = datac[0];
                i++;
            }
            return temp.Length;
        }

        public int getDatagsoneNotReciv()
        {
            List<string> field = new List<string>();
            List<string[]> temp;
            field.Add("gtin,batchnumber,expire,sn");
            string where = "isused <> '2'";
            List<string[]> ds = select(field, "tmp_gsOneVial", where);
            return ds.Count;
        }

        public int getCountResult()
        {
            List<string> field = new List<string>();
            field.Add("count(*)");
            List<string[]> ds = select(field, "result", "");
            int temp = int.Parse(ds[0][0]);
            return temp;
        }

        public void BeginDelete(List<string[]> data)
        {
            string sql = "BEGIN TRANSACTION;\n ";

            for (int i = 0; i < data.Count; i++)
            {
                sql += "DELETE FROM tmp_gsOneVial Where gsone = '" + data[i][4] + "';\n";
            }
            sql += "COMMIT;";
            // simpanLog(sql);
            ExecuteQuery(sql);
        }



        internal void BeginDeleteCapid(List<string> data)
        {
            string sql = "BEGIN TRANSACTION;\n ";

            for (int i = 0; i < data.Count; i++)
            {
                sql += "DELETE FROM tmp_cap_id Where capid = '" + data[i] + "';\n";
            }
            sql += "COMMIT;";
            ExecuteQuery(sql);
        }

        //tambahan
        public string selectBatch()
        {
            try
            {
                string sql = "SELECT batchNumber from tmp_gsOneVial limit 1";
                
                simpanLog(sql);
                SetConnection();
                sql_cmd = new SQLiteCommand(sql, sql_con);
                datareader = sql_cmd.ExecuteReader();
                string Results = "";
                while (datareader.Read())
                {
                    string[] data = new string[datareader.FieldCount];
                    for (int u = 0; u < datareader.FieldCount; u++)
                    {
                        data[u] = datareader.GetValue(u).ToString();
                    }

                    Results = data[0];
                }
                
                return Results;
            }
            catch (Exception IO)
            {
                isConnect = false;
                sql_con.Close();
                SetConnection();
                return "";
            }
        }
        //end of tambahan
    }
}
