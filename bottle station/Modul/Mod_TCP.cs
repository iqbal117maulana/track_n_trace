﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Windows.Forms;
using System.IO;
using System.Threading;

namespace Bottle_Station
{
    public class tcp_testingFlubio
    {
        private static TcpClient Client;
        private static Byte[] Data = new Byte[256];
        private static Stream ClientStream;
        public static bool IsConnected = false;
        public print pr;
        public bool startTerimaDomino;
        public string IPAddress;
        public string Port;
        public string Timeout;
        public int timer;
        //public bool IsConnected = false;
        public ControlFlubio ctrl;
        private Thread serverThread;
        private Thread serverThread2;
        private Thread serverThreadGs;
        public bool conneect = true;

        internal bool Connect()
        {
            try
            {
                // if (!checkConnected())
                //{
                Client = new TcpClient();
                IAsyncResult result = Client.BeginConnect(IPAddress, Int32.Parse(Port), null, null);
                bool success = result.AsyncWaitHandle.WaitOne(Int32.Parse(Timeout) * 100, true);
                if (success)
                {
                    IsConnected = true;
                    return true;
                }
                else
                {
                    IsConnected = false;
                    return false;
                }

                //}
                //else
                // {
                //   IsConnected = true;
                // return true;
                //}
            }
            catch (SocketException ex)
            {
                new Confirm("error +" + ex.ToString());
                if (!ex.NativeErrorCode.Equals(10056))
                    IsConnected = false;
            }
            catch (InvalidOperationException te)
            {
                new Confirm("Printer not connected !");
                IsConnected = false;
            }
            return false;

        }

        public bool Connect(string ip, string port, string timeout)
        {
            try
            {
                Client = new TcpClient();
                IAsyncResult result = Client.BeginConnect(ip, Int32.Parse(port), null, null);
                bool success = result.AsyncWaitHandle.WaitOne(Int32.Parse(timeout), true);
                if (success)
                {
                    IsConnected = true;
                    return true;
                }
                else
                {
                    IsConnected = false;
                    return false;
                }
            }
            catch (SocketException ex)
            {
                new Confirm("error +" + ex.ToString());
                if (!ex.NativeErrorCode.Equals(10056))
                    IsConnected = false;
            }
            catch (InvalidOperationException te)
            {
                new Confirm("Printer not connected !");
            }
            return false;
        }

        public bool checkConnected()
        {
            try
            {
                return Connect();
            }
            catch (Exception)
            {
                return false;
            }
        }

        public void sendText(string dataTerima)
        {
            try
            {
                if (IsConnected)
                {
                    ClientStream = Client.GetStream();
                    StreamWriter sw = new StreamWriter(ClientStream);
                    sw.WriteLine(dataTerima);
                    sw.Flush();
                }
            }
            catch (Exception ex)
            {
                IsConnected = false;
                int cekNotConnect = 0;
                while (!IsConnected && conneect)
                {
                    if (cekNotConnect < 5)
                    {
                        if (Connect())
                        {
                            IsConnected = true;
                            sendText(dataTerima);
                        }
                        cekNotConnect++;
                    }
                    else
                    {
                        cekNotConnect = 0;
                        MessageBox.Show("PLC not connected", "Information", MessageBoxButtons.OK);
                    }
                    Thread.Sleep(1000);
                }
            }
        }

        public void send(string dataTerima)
        {
            try
            {
                if (IsConnected)
                {
                    ClientStream = Client.GetStream();
                    StreamWriter sw = new StreamWriter(ClientStream);
                    sw.WriteLine(dataTerima);
                    sw.Flush();
                }
            }
            catch (Exception ex)
            {
                IsConnected = false;
                int cekNotConnect = 0;
                while (!IsConnected && conneect)
                {
                    if (cekNotConnect < 5)
                    {
                        ctrl.log("Try to reconect [SEND]");
                        if (Connect())
                        {
                            IsConnected = true;
                            ctrl.log("Connected to " + IPAddress + ":" + Port);
                            send(dataTerima);
                        }
                        else
                        {
                            ctrl.log("Not connected " + IPAddress + ":" + Port);
                        }
                        cekNotConnect++;
                    }
                    else
                    {
                        cekNotConnect = 0;
                        MessageBox.Show("Inkjet not connected", "Information", MessageBoxButtons.OK);
                    }
                    Thread.Sleep(1000);
                }
            }
        }

        public string sendBack()
        {
            try
            {
                byte[] data = new Byte[256];
                if (IsConnected)
                {
                    ClientStream = Client.GetStream();
                    Int32 bytes = ClientStream.Read(data, 0, data.Length);
                    string responseData = System.Text.Encoding.ASCII.GetString(data, 0, bytes);
                    startTerimaDomino = true;
                    startTerimaDomino = true;
                    return responseData;
                }
                return "";
            }
            catch (InvalidOperationException ex)
            {

            }
            return "";
        }

        public string sendBack(string dataTerima)
        {
            try
            {
                byte[] data = new Byte[256];
                if (IsConnected)
                {
                    ClientStream = Client.GetStream();
                    StreamWriter sw = new StreamWriter(ClientStream);
                    sw.WriteLine(dataTerima);
                    sw.Flush();
                    Int32 bytes = ClientStream.Read(data, 0, data.Length);
                    string responseData = System.Text.Encoding.ASCII.GetString(data, 0, bytes);
                    return responseData;
                }
                return "";
            }
            catch (InvalidOperationException ex)
            {

            }
            return "";
        }

        public void disconnect()
        {
            try
            {
                Client.GetStream().Close();
                Client.Close();
            }
            catch (Exception)
            {

            }
        }

        public void dc()
        {
            try
            {
                Client.Close();
            }
            catch (Exception)
            {

            }
        }

        public void updatestatus(string data)
        {
            string dates = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            //pr.txtStatus.AppendText(dates+"\t"+data+"\n");
        }

        internal void closeStream()
        {
            Client.GetStream().Close();
            Client.Close();
        }

        public void startListener()
        {
            serverThread = new Thread(new ThreadStart(MulaiServerCapid));
            serverThread.Start();
        }

        public void startCheckStatus(ControlFlubio ct)
        {
            ctrl = ct;
            serverThread2 = new Thread(new ThreadStart(MulaiCheker));
            serverThread2.Start();
        }

        public void MulaiCheker()
        {
            //while (conneect)
            //{
            //    // kirim ceker terus
            //    send("^0?SM");
            //    Thread.Sleep(5000);
            //}
        }

        public void MulaiServerCapid()
        {
            timer = Int32.Parse(ctrl.sqlite.config["second"]);
            while (conneect)
            {
                try
                {
                    if (GetStatusPrinter())
                    {
                        int QtySend = int.Parse(ctrl.sqlite.config["qtysend"]);
                        send("^0?SM");
                        string data = listenerCekBuffer();
                        if (data.Length > 0 && data.Contains("SM"))
                        {
                            string[] dataSplit = data.Split('\t');
                            int dataIndat = int.Parse(dataSplit[1]);
                            ctrl.log("Data Buffer: " + dataIndat);
                            if (dataIndat < QtySend * 2)
                            {
                                ctrl.log("Send to printer :" + QtySend);
                                List<string> dataKirim = ctrl.getDataSendToPrinter(QtySend);
                                for (int i = 0; i < dataKirim.Count; i++)
                                {
                                    //ctrl.log("Send to printer :" + dataKirim[i]);
                                    send("^0=MR" + i + " " + dataKirim[i]);
                                }
                                ctrl.sqlite.BeginDeleteCapid(dataKirim);

                                // ctrl.SetDataGrid_Send("0", "");
                                ctrl.CheckRegenerateData();
                            }
                        }
                    }
                    Thread.Sleep(timer * 1000);
                }
                catch (Exception ex)
                {
                    ctrl.log("Error data from printer");
                }

            }
        }
        bool isChecking = false;
        private bool GetStatusPrinter()
        {
            try
            {
                string data = "";
                if (!isChecking && conneect)
                {
                    isChecking = true;
                    for (int i = 0; i < 5; i++)
                    {
                        send("^0?RS");
                        data = listenerCekBuffer();
                        if (data.Contains("RS"))
                        {
                            break;
                        }
                    }
                    string nozzle = data.Substring(5, 1);
                    string[] split = data.Split('\t');
                    if (nozzle.Equals("2") && split[1].Equals("6"))
                    {
                        isChecking = false;
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                isChecking = false;
                return false;
            }
            isChecking = false;
            return false;

        }

        private bool cekStatusPrinter()
        {
            try
            {
                send("^0?RS");
                string data = listenerCekBuffer();
                ctrl.log("Receive Status: " + data);
                string nozzle = data.Substring(5, 1);
                string[] split = data.Split('\t');
                if (nozzle.Equals("2") && split[1].Equals("6"))
                {
                    ctrl.log("Status printer : On printing");
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return false;

        }

        internal string listenerCekBuffer()
        {
            try
            {
                byte[] data = new Byte[256];
                if (IsConnected)
                {
                    if (ClientStream == null)
                    {
                        send("cek");
                    }
                    Int32 bytes = ClientStream.Read(data, 0, data.Length);
                    string responseData = System.Text.Encoding.ASCII.GetString(data, 0, bytes);
                    simpanLog(responseData);
                    return responseData;
                }
                else
                {
                    Connect();
                }
            }
            catch (Exception ex)
            {
                IsConnected = false;
                int cekNotConnect = 0;
                while (!IsConnected && conneect)
                {
                    if (cekNotConnect < 3)
                    {
                        ctrl.log("Try to reconect [Buffer]");
                        if (Connect())
                        {
                            IsConnected = true;
                            ctrl.log("Connected to " + IPAddress + ":" + Port);
                            send("^0?SM");
                        }
                        else
                        {
                            ctrl.log("Not connected " + IPAddress + ":" + Port);
                        }
                        cekNotConnect++;
                    }
                    else
                    {
                        cekNotConnect = 0;
                        MessageBox.Show("Inkjet not connected", "Information", MessageBoxButtons.OK);
                    }
                    Thread.Sleep(1000);
                }
                return "Reconected";
            }
            return "";
        }

        internal bool listenerCekStatus()
        {
            try
            {
                byte[] data = new Byte[256];
                if (IsConnected)
                {
                    send("^0?RS");
                    Int32 bytes = ClientStream.Read(data, 0, data.Length);
                    string responseData = System.Text.Encoding.ASCII.GetString(data, 0, bytes);
                    char nozzle = responseData[5];
                    if (nozzle == '2')
                    {
                        return true;
                    }
                    return false;
                    //return responseData;
                }
                else
                {
                    Connect();
                }
            }
            catch (Exception ex)
            {
                IsConnected = false;
                int cekNotConnect = 0;
                while (!IsConnected)
                {
                    if (cekNotConnect < 5)
                    {
                        ctrl.log("Try to reconect [STATUS]");
                        if (Connect())
                        {
                            IsConnected = true;
                            ctrl.log("Connected to " + IPAddress + ":" + Port);
                            send("^0?SM");
                        }
                        else
                        {
                            ctrl.log("Not connected " + IPAddress + ":" + Port);
                        }
                        cekNotConnect++;
                    }
                    else
                    {
                        cekNotConnect = 0;
                        MessageBox.Show("Inkjet not connected", "Information", MessageBoxButtons.OK);
                    }
                    Thread.Sleep(1000);
                }
                return false;
            }
            return false;
        }

        public void simpanLog(String line)
        {
            DateTime now = DateTime.Now;
            string tahun = now.ToString("yyyy");
            string bulan = now.ToString("MM");
            string hari = now.ToString("dd");
            string dates = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            line = dates + "\t" + line;
            // cek file 
            bool cekfile = false;
            string namaFolder = "eventlog\\Printer";
            try
            {
                while (!cekfile)
                {
                    if (Directory.Exists(namaFolder))
                    {
                        if (Directory.Exists(namaFolder + @"\" + tahun))
                        {
                            if (Directory.Exists(namaFolder + @"\" + tahun + @"\" + bulan))
                            {

                                using (StreamWriter outputFile = new StreamWriter(namaFolder + @"\" + tahun + @"\" + bulan + @"\" + hari + ".txt", true))
                                {
                                    outputFile.WriteLine(line);
                                }
                                cekfile = true;
                            }
                            else
                            {

                                Directory.CreateDirectory(namaFolder + @"\" + tahun + @"\" + bulan);
                            }
                        }
                        else
                        {
                            Directory.CreateDirectory(namaFolder + @"\" + tahun);
                        }
                    }
                    else
                    {

                        Directory.CreateDirectory(namaFolder);
                    }
                }
            }
            catch (Exception ex)
            {
                simpanLog(ex.Message);
            }
        }



        // domino laser
        public int CountBuffer = -1;
        public void startListenerLaser()
        {
            serverThreadGs = new Thread(new ThreadStart(MulaiServerGS1ID));
            serverThreadGs.Start();
        }

        private bool cekStatusLaser()
        {
            try
            {
                send("GETMARKMODE");
                ctrl.log("Send Laser: GETMARKMODE");

                string data = listenerCekBuffer();
                ctrl.log("Receive Status: " + data);

                string[] hasil = data.Split(' ');
                if (hasil[2].Equals("1"))
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return false;

        }

        public void MulaiServerGS1ID()
        {
            while (conneect)
            {
                try
                {
                    if (cekStatusLaser())
                    {
                        int QtySend = int.Parse(ctrl.sqlite.config["qtysend"]);
                        send("GETBUFFERSTATUS");
                        string data = listenerCekBuffer();
                        if (data.Length > 0)
                        {
                            string[] dataSplit = data.Split(' ');
                            int dataIndat = int.Parse(dataSplit[2]);
                            ctrl.log("Data Buffer Laser: " + dataIndat);
                            if (dataIndat < QtySend * 2)
                            {
                                ctrl.log("Send to laser");
                                List<string[]> dataKirim = ctrl.getDataSendToPrintergs(QtySend);
                                //Dictionary<string, string> field = new Dictionary<string, string>();
                                //field.Add("isused", "1");
                                for (int i = 0; i < QtySend; i++)
                                {
                                    ctrl.log("Send to printer :" + dataKirim[i][4]);
                                    ctrl.sqlite.delete("gsone = '" + dataKirim[i][4] + "'", Variable.NAMA_TABLE_TEMP_GSOne);
                                    string simpan = "BUFFERDATA " + CountBuffer + " \"" + dataKirim[i][0] + "\" \"" + dataKirim[i][1] + "\" \"" + dataKirim[i][2] + "\" \"" + dataKirim[i][3] + "\" " + "\"" + dataKirim[i][5] + "\"";
                                    send(simpan);
                                    CountBuffer++;
                                }

                                if (CountBuffer >= 10000)
                                    CountBuffer = -1;
                                ctrl.SetDataGridSendgs("0", "");
                                ctrl.GetRegenerateData_GS();
                            }
                        }
                    }
                    Thread.Sleep(1000);
                }
                catch (Exception ex)
                {
                    ctrl.log("Error data from printer");
                }
            }
        }
    }
}
