﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bottle_Station
{
    class SetForm
    {
        public BottleStation Form;

        string Batchnumber_ = "";
        string Line_Number_ = "";
        string ProductName_ = "";
        string AdminName_ = "";

        public string Batchnumber
        {
            get { return Batchnumber_; }
            set { Batchnumber_ = value; Form.lblBatch.Text = value; }
        }

        public string Line_Number
        {
            get { return Line_Number_; }
            set { Line_Number_ = value; Form.lblLine.Text = value; }
        }

        public string ProductName
        {
            get { return ProductName_; }
            set { ProductName_ = value; Form.lblProductName.Text = value; }
        }

        public string AdminName
        {
            get { return AdminName_; }
            set { AdminName_ = value; Form.lblAdmin.Text = value; }
        }

        internal void PLC_Online()
        {
        //    Form.lblPLC.Text = "Online";
        }

        internal void PLC_Offline()
        {
          //  Form.lblPLC.Text = "Offline";
        }

        internal void Printer_Online()
        {
            //Form.lblPLC.Text = "Online";
        }

        internal void Printer_Offline()
        {
            Form.lblPrinter.Text = "Offline";
        }

        internal void Marker_Online()
        {
            Form.lblMarker.Text = "Online";
        }

        internal void Marker_Offline()
        {
            Form.lblMarker.Text = "Offline";
        }

        internal void Camera1_Online()
        {
            Form.lblCamera1.Text = "Online";
        }

        internal void Camera1_Offline()
        {
            Form.lblCamera1.Text = "Offline";
        }

        internal void Camera2_Online()
        {
            Form.lblCamera2.Text = "Online";
        }

        internal void Camera2_Offline()
        {
            Form.lblCamera2.Text = "Offline";
        }

        //tambahan flubio
        public BottleStationFlubio FormFlubio;

        string Batchnumber_flubio = "";
        string Line_Number_flubio = "";
        string ProductName_flubio = "";
        string AdminName_flubio = "";

        public string Batchnumber_F
        {
            get { return Batchnumber_flubio; }
            set { Batchnumber_flubio = value; FormFlubio.lblBatch.Text = value; }
        }

        public string Line_Number_F
        {
            get { return Line_Number_flubio; }
            set { Line_Number_flubio = value; FormFlubio.lblLine.Text = value; }
        }

        public string ProductName_F
        {
            get { return ProductName_flubio; }
            set { ProductName_flubio = value; FormFlubio.lblProductName.Text = value; }
        }

        public string AdminName_F
        {
            get { return AdminName_flubio; }
            set { AdminName_flubio = value; FormFlubio.lblAdmin.Text = value; }
        }

        internal void Printer_Offline_F()
        {
            FormFlubio.lblPrinter.Text = "Offline";
        }

        internal void Marker_Online_F()
        {
            FormFlubio.lblMarker.Text = "Online";
        }

        internal void Marker_Offline_F()
        {
            FormFlubio.lblMarker.Text = "Offline";
        }

        internal void Camera1_Online_F()
        {
            FormFlubio.lblCamera1.Text = "Online";
        }

        internal void Camera1_Offline_F()
        {
            FormFlubio.lblCamera1.Text = "Offline";
        }

        internal void Camera2_Online_F()
        {
            FormFlubio.lblCamera2.Text = "Online";
        }

        internal void Camera2_Offline_F()
        {
            FormFlubio.lblCamera2.Text = "Offline";
        }

        //end tambahan flubio
    }
}
