﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Configuration;
using System.Windows.Forms;
using System.Data;
using System.IO;
using System.Diagnostics;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using System.Net.NetworkInformation;
using System.Net;
using System.Net.Sockets;

namespace Bottle_Station
{
    public class dbaccess
    {
        public static SqlConnection Connection;
        public static SqlCommand Command;
        public static SqlDataReader DataReader;
        public static SqlDataAdapter dataadapter;
        //data 
        public List<string> dataInsert = new List<string>();
        public List<string> dataUpdate = new List<string>();
        //header
        public DataTable dataHeader ;
        public int num_rows=0;
        sqlitecs sqlite;
        public string valid;
        //system log
        public string eventname;
        string eventtype;
        public bool sysLog = true;
        public string adminid;
        public string from;
        public string to;
        //line
        public string linecode;
        public string linename;
        public string uom = "Vial";

        public string ipAddress;
        public string eventType = "12";
        public string movementType = "6";

        public string errorMessage;
        public string adminName;
        public int fcon = 0 ;

        public dbaccess()
        {
            sqlite = new sqlitecs();
            //Connect();
        }

        public bool Connect()
        {
            try
            {
                string connec = "Data Source=" + sqlite.config["ipDB"] + ";" +
                                "Initial Catalog=" + sqlite.config["namaDB"] + ";" +
                                "User id=" + sqlite.config["usernameDB"] + ";" +
                                "Password=" + sqlite.config["passDB"] + ";" +
                                //"Password=docotronics;" + 
                                "Connection Timeout=" + sqlite.config["timeout"]+";";
                                //"Connection Timeout=1;";
                Connection = new SqlConnection(connec);
                Connection.Open();
                fcon = 0;
                return true;
            }
            catch (TimeoutException t)
            {
                Console.WriteLine("MASUK CATCH 1");
                fcon++;
                errorMessage = t.Message;
                if (fcon < 2)
                {
                    Console.WriteLine("MASUK CATCH 1 IF");
                    new Confirm("Can not open connection to Database Server.", "Information", MessageBoxButtons.OK);
                    config cfg = new config(null, null, 0, "");
                    cfg.ShowDialog();
                }
                
            }
            catch (SqlException se)
            {
                Console.WriteLine("MASUK CATCH 2 : " + se.Message);
                fcon++;
                Console.WriteLine("FCON : " + fcon);
                errorMessage = se.Message;
                if (fcon < 2)
                {
                    Console.WriteLine("MASUK CATCH 2 IF");
                    new Confirm("Can not open connection to Database Server.", "Information", MessageBoxButtons.OK);
                    config cfg = new config(null, null, 0, "");
                    cfg.ShowDialog();
                    return false;
                }
                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine("MASUK CATCH 3");
                fcon++;
                errorMessage = ex.Message;
                if (fcon < 2)
                {
                    Console.WriteLine("MASUK CATCH 3 IF");
                    new Confirm("Can not open connection to Database Server.", "Information", MessageBoxButtons.OK);
                    simpanLog("Error SQLSERVER : " + ex.ToString());
                    config cfg = new config(null, null, 0, "");
                    cfg.ShowDialog();
                }
            }
            return false;
        }

        public void closeConnection()
        {
            Connection.Close();
        }

        public static List<String[]> LoadProductionOrders()
        {
            List<String[]> Results = new List<String[]>();
            String Query = "SELECT production_order_number, created_date, status FROM transaction_production_order";
            Command = new SqlCommand(Query, Connection);
            DataReader = Command.ExecuteReader();

            while (DataReader.Read())
            {
                String[] Data = new String[] { DataReader.GetValue(0).ToString(), DataReader.GetValue(1).ToString(), DataReader.GetValue(2).ToString() };
                Results.Add(Data);
            }

            DataReader.Close();
            Command.Dispose();

            return Results;
        }

        public bool insert(Dictionary<string, string> field, string table)
        {
            try
            {
                string sql = "INSERT INTO " + table + " (";
                int i = 0;
                foreach (string key in field.Keys)
                {
                    sql += "" + key + "";
                    if (i + 1 < field.Count)
                    {
                        sql += ",";
                        i++;
                    }
                }

                sql += ") values (";

                i = 0;
                foreach (string key in field.Values)
                {
                    sql += "'" + key + "'";
                    if (i + 1 < field.Count)
                    {
                        sql += ",";
                        i++;
                    }
                }
                sql += ")";
                simpanLog(sql);
                if (Connect())
                {
                    //Console.WriteLine("MASUK ELSE INSERT");
                    Command = new SqlCommand(sql, Connection);
                    Command.ExecuteNonQuery();
                    return true;
                }
            }
            catch (SqlException see)
            {
                //Console.WriteLine("MASUK CATCH INSERT 2 : " + see.Message);
            }
            catch (Exception sq)
            {
                //Console.WriteLine("MASUK CATCH INSERT");
                try
                {
                    //Console.WriteLine("MASUK CATCH TRY INSERT");
                    simpanLog("Error SQl : " + sq.ToString());
                    errorMessage = sq.Message;
                    Connection.Close();
                }
                catch (Exception ex)
                {
                    //Console.WriteLine("MASUK CATCH CATCH INSERT");
                }
            }
            
            return false;
        }

        public void update(Dictionary<string, string> field, string table, string where)
        {
            try
            {
                string sql = "UPDATE " + table + " SET ";
                int i = 0;
                foreach (KeyValuePair<string, string> key in field)
                {
                    sql += key.Key+" = "+"'" + key.Value + "'";
                    if (i + 1 < field.Count)
                    {
                        sql += ",";
                        i++;
                    }
                }

                if (where.Length > 0)
                    sql += " WHERE " + where;

                simpanLog(sql);
                if (Connect())
                {
                    Command = new SqlCommand(sql, Connection);
                    Command.ExecuteNonQuery();
                    Connection.Close();
                }
            }
            catch (SqlException sq)
            {
                try
                {
                    simpanLog("Error SQl : " + sq.ToString());
                    Connection.Close();
                }
                catch (Exception ex)
                {

                }
            }
        }

        public DataSet select(List<string> field, string table, string where)
        {
            try
            {
                string sql = "SELECT ";
                for (int j = 0; j < field.Count; j++)
                {
                    sql += "'" + field[j] + "'";
                    if (j + 1 < field.Count)
                    {
                        sql += ",";
                        j++;
                    }
                }

                sql += " From " + table;

                if (where.Length > 0)
                    sql += " WHERE " + where;

                simpanLog(sql);
                dataadapter = new SqlDataAdapter(sql, Connection);
                DataSet Results = new DataSet();
                dataadapter.Fill(Results);
                return Results;
            }
            catch (SqlException sq)
            {
                simpanLog("Error SQLSERVER : " + sq.ToString());
            }
            finally
            {
                try
                {
                    DataReader.Close();
                    Command.Dispose();
                }
                catch (SqlException sq)
                {
                    simpanLog("Error SQLSERVER : " + sq.ToString());
                }
                catch (NullReferenceException nl)
                {
                    simpanLog("Error SQLSERVER : " + nl.ToString());
                }

            }
            return null;
        }
        
        public List<String[]> selectList(List<string> field, string table, string where)
        {
            try
            {
                string sql = "SELECT ";
                for (int j = 0; j < field.Count; j++)
                {
                    sql +=  field[j] ;
                    if (j + 1 < field.Count)
                    {
                        sql += ",";
                        j++;
                    }
                }
                sql += " From " + table;

                if (where.Length > 0)
                    sql += " WHERE " + where;

                simpanLog(sql);
                List<String[]> Results = new List<String[]>();
                if (Connect())
                {
                    Command = new SqlCommand(sql, Connection);
                    DataReader = Command.ExecuteReader();
                    int num = 0;
                    while (DataReader.Read())
                    {
                        string[] data = new string[DataReader.FieldCount];
                        for (int u = 0; u < DataReader.FieldCount; u++)
                        {
                            data[u] = DataReader.GetValue(u).ToString();
                        }

                        Results.Add(data);
                        num++;
                    }
                    num_rows = num;
                    dataHeader = new DataTable();
                    for (int i = 0; i < DataReader.FieldCount; i++)
                    {
                        dataHeader.Columns.Add(DataReader.GetName(i));
                    }
                    Connection.Close();
                    return Results;
                }
            }
            catch (SqlException sq)
            {
                try
                {
                    var st = new StackTrace(sq, true);
                    var frame = st.GetFrame(0);
                    string line = frame.ToString();
                    simpanLog("Error  00100: " + line);
                    Connection.Close();
                }
                catch (Exception ex)
                {
                    Connection.Close();
                }
            }
            catch (NullReferenceException nl)
            {

                var st = new StackTrace(nl, true);
                var frame = st.GetFrame(0);
                string line = frame.ToString();
                simpanLog("Error  00100: " + line);
                Connection.Close();
            }
            catch (InvalidOperationException ide)
            {
                Application.Exit();
            }
            finally
            {
                try
                {
                    DataReader.Close();
                    Command.Dispose();
                }
                catch (SqlException sq)
                {
                    simpanLog("Error SQLSERVER : " + sq.ToString());
                }
                catch (NullReferenceException nl)
                {
                    simpanLog("Error SQLSERVER : " + nl.ToString());
                }

            }
            return null;
        }

        public static List<String> GetPODetails(String PO)
        {
            List<String> Results = new List<String>();
            String Query = String.Format("SELECT production_order_number, product_code, product_name, product_quantity, status FROM transaction_production_order WHERE production_order_number = '{0}'", PO);
            Command = new SqlCommand(Query, Connection);
            DataReader = Command.ExecuteReader();

            while (DataReader.Read())
            {
                for (int i = 0; i < 5; i++)
                {
                    Results.Add(DataReader.GetValue(i).ToString());
                }
            }

            DataReader.Close();
            Command.Dispose();

            return Results;
        }

        public void simpanLog(String line)
        {
            DateTime now = DateTime.Now;
            string tahun = now.ToString("yyyy");
            string bulan = now.ToString("MM");
            string hari = now.ToString("dd");
            string dates = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            line = dates + "\t" + line;
            // cek file 
            bool cekfile = false;
            string namaFolder = "eventlog\\Database";
            try
            {
                while (!cekfile)
                {
                    if (Directory.Exists(namaFolder))
                    {
                        if (Directory.Exists(namaFolder + @"\" + tahun))
                        {
                            if (Directory.Exists(namaFolder + @"\" + tahun + @"\" + bulan))
                            {

                                using (StreamWriter outputFile = new StreamWriter(namaFolder + @"\" + tahun + @"\" + bulan + @"\" + hari + ".txt", true))
                                {
                                    outputFile.WriteLine(line);
                                }
                                cekfile = true;
                            }
                            else
                            {

                                Directory.CreateDirectory(namaFolder + @"\" + tahun + @"\" + bulan);
                            }
                        }
                        else
                        {
                            Directory.CreateDirectory(namaFolder + @"\" + tahun);
                        }
                    }
                    else
                    {

                        Directory.CreateDirectory(namaFolder);
                    }
                }
            }
            catch (Exception ex)
            {
                simpanLog(ex.Message);
            }
        }

        public bool validasi(string username)
        {
            try
            {
                if (username.Length > 0)
                {
                    List<string> field = new List<string>();
                    field.Add("id");
                    string where = "username = '" + username+"'";
                    List<string[]> result = selectList(field, "[users]", where);
                    if (result.Count > 0)
                    {
                        return true;
                    }
                    else
                        return false;
                }
                else
                {
                    return false;
                }
            }
            catch (NullReferenceException nl)
            {
                simpanLog("Error  00100: " + nl.Message);
                return false;
            }

        }

        public bool validasi(string username, string password)
        {
            try
            {
                if (username.Length > 0 && password.Length > 0)
                {
                    List<string> field = new List<string>();
                    field.Add("id");
                    string where = "username = '" + username + "' AND password = '" + md5hash(password) + "'";
                    List<string[]> result = selectList(field, "[users]", where);
                    if (result.Count > 0)
                    {
                        return true;
                    }
                    else
                        return false;
                }
                else
                {
                    string code = "0010";
                    simpanLog(code + " : USERNAME DAN PASSWORD SALAH");
                    return false;
                }
            }
            catch (NullReferenceException nl)
            {
                var st = new StackTrace(nl, true);
                var frame = st.GetFrame(0);
                string line = frame.ToString();
                simpanLog("Error  00100: " + line);
                return false;
            }

        }

        public List<string[]> validasi(string username, string password,string status)
        {
            try
            {
                if (username.Length > 0 && password.Length > 0)
                {
                    List<string> field = new List<string>();
                    field.Add("id");
                    string where = "username = '" + username + "' AND password = '" + md5hash(password) + "' AND Active = "+status;
                    List<string[]> result = selectList(field, "[users]", where);
                    if (result.Count > 0)
                    {
                        from = "Bottle Station";
                        adminid = result[0][0];
                        eventtype = "1";
                        eventname = "Login";
                        systemlog();
                        return result;
                    }
                    else
                        return null;
                }
                else
                {
                    string code = "0010";
                    simpanLog(code + " : USERNAME DAN PASSWORD SALAH");
                    return null;
                }
            }
            catch (NullReferenceException nl)
            {
                var st = new StackTrace(nl, true);
                var frame = st.GetFrame(0);
                string line = frame.ToString();
                simpanLog("Error  00100: " + line);
                return null;
            }

        }
       
        public string md5hash(string source)
        {
            using (MD5 md5Hash = MD5.Create())
            {
                string hash = GetMd5Hash(md5Hash, source);
                return hash;
            }
            return null;
        }

        static string GetMd5Hash(MD5 md5Hash, string input)
        {

            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }

        static bool VerifyMd5Hash(MD5 md5Hash, string input, string hash)
        {
            // Hash the input.
            string hashOfInput = GetMd5Hash(md5Hash, input);

            // Create a StringComparer an compare the hashes.
            StringComparer comparer = StringComparer.OrdinalIgnoreCase;

            if (0 == comparer.Compare(hashOfInput, hash))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void Movement(Dictionary<string, string> field)
        {
            string from = "movement_history";
            DateTime now = DateTime.Now;
            string dates = DateTime.Now.ToString("yyMMddHHmmss");
            field.Add("movementId",dates);
            field.Add("eventTime",""+(Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1)).TotalSeconds));
            insert(field, from);
        }

        private static readonly DateTime UnixEpoch = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);

        public string getUnixString()
        {
            string date = "" + (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1)).TotalSeconds); 
            return date;
            
        }

        public string getUnixString(double data)
        {
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(data).ToLocalTime();
            return dtDateTime.ToShortDateString();
        }

        public void insertData(Dictionary<string, string> field, string table)
        {
            try
            {
                string sql = "INSERT INTO " + table + " (";
                int i = 0;
                foreach (string key in field.Keys)
                {
                    sql += "" + key + "";
                    if (i + 1 < field.Count)
                    {
                        sql += ",";
                        i++;
                    }
                }

                sql += ") values (";

                i = 0;
                foreach (string key in field.Values)
                {
                    sql += "'" + key + "'";
                    if (i + 1 < field.Count)
                    {
                        sql += ",";
                        i++;
                    }
                }
                sql += ")";
                simpanLog(sql);
                dataInsert.Add(sql);
            }
            catch (SqlException sq)
            {
                simpanLog("Error SQl : " + sq.ToString());
            }
        }

        public void Begin()
        {
            try
            {
                string sql = "BEGIN TRANSACTION;\n ";

                for (int i = 0; i < dataInsert.Count; i++)
                {
                    sql += dataInsert[i] + "\n";
                }
                sql += "COMMIT;";
                simpanLog("BEGIN TRANSACTION");

                if (Connect())
                {
                    Command = new SqlCommand(sql, Connection);
                    Command.ExecuteNonQuery();
                    Connection.Close();
                }
            }
            catch (SqlException sq)
            {
                simpanLog("Error "+sq.Message);
            }

            dataInsert = new List<string>();
        }

        string iden = "BOT_";

        public void Movement(string order, string from, string to, string qty, string itemid)
        {
            string dates = DateTime.Now.ToString("ssMMddmmHHssyy");
            Dictionary<string, string> field = new Dictionary<string, string>();
            string guid = Guid.NewGuid().ToString().Substring(0, 10);
            field.Add("movementId", iden + dates + "_" + guid);
            field.Add("orderNumber", order);
            field.Add("movementType", movementType);
            field.Add("[from]", from);
            field.Add("[to]", to);
            field.Add("qty", qty);
            field.Add("uom", uom);
            field.Add("userid", adminid);
            field.Add("username", adminName);
            field.Add("itemid", "CapID -> GS1ID");
            field.Add("eventTime", getUnixString());
            insert(field, "movement_history");
        }
      
        public bool delayStat = false;
       
        public void addVaccine(string batch,string line,string productmodel, string expdate,result sq)
        {
            Dictionary<string, string> field;
            List<string> fieldlist = new List<string>();
            fieldlist.Add("capid,gsonevial,createdtime,flag,batchnumber,sample");
            List<string[]> res = sq.select(fieldlist, "result", "");
            if (sq.num_rows > 0)
            {
                foreach (string[] data in res)
                {
                    string [] datap = dataPO(data[4]);
                    field = new Dictionary<string, string>();
                    field.Add("capid", data[0]);
                    field.Add("gsonevialid", data[1]);
                    field.Add("createdtime", data[2]);
                    field.Add("batchnumber", data[4]);
                    if (data[3].Equals("0"))
                    {
                        field.Add("isreject", "0");
                    }
                    else
                    {
                        field.Add("isreject", "1");
                    }


                    field.Add("flag", data[5]);
                    field.Add("productmodelid", datap[4]);
                    field.Add("LineNumber", line);
                    field.Add("expdate", datap[1]);
                    insertData(field, "vaccine");
                    updatestatus(data[0], sq);
                    Movement(batch, data[0], data[1], "1", "");
                }
                Begin();
            }
            delayStat = false;
        }

        public void addVaccineNew(string capid,string gsone,string batch, string line,string flag, string status)
        {
            Dictionary<string, string> field = new Dictionary<string, string>();
            string[] datap = dataPO(batch);
            field = new Dictionary<string, string>();
            field.Add("capid", capid);
            field.Add("gsonevialid", gsone);
            field.Add("createdtime", getUnixString());
            field.Add("productmodelid", datap[4]);
            field.Add("LineNumber", line);
            field.Add("batchnumber", batch);
            field.Add("expdate", datap[1]);
            field.Add("isreject", status);
            field.Add("flag", flag);
            insert(field, "vaccine");
            Movement(batch, capid, gsone, "1", "");
        }


        void updatestatus(string capid, result sq)
        {
            //Dictionary<string, string> field = new Dictionary<string, string>();
            //field.Add("flag", "3");
            string where = "capid ='" + capid + "'";
            sq.delete(where, "result");
        }

        public bool validasi(string data,int val)
        {
            //text ip val = 0
            if(val ==0)
                return cekIP(data);
            else if (val == 1)
            {
                //text tidak kosong val =1
                if (data.Length == 0)
                {
                    valid = "Data kosong";
                    return false;
                }
            }
            else if (val == 2)
            {
                //text tidak ada angka
                  int errorCounter = Regex.Matches(data, @"[a-zA-Z]").Count;
                  if (errorCounter > 0)
                  {
                      valid = "data mengandung angka";
                      return false;
                  }
            }
            else if (val ==3)
            {
                //text tidak ada angka
                
                if (data.Length > 6)
                    return false; 
                bool rege = Regex.Match(data, @"^[0-9]+$").Success;
                if (!rege)
                {
                    valid = "data mengandung angka";
                    return false;
                }
            }
            return true;

        }

        bool cekIP(string data)
        {
            if (data.Length < 3 & data.Length > 0)
                return false;
            int errorCounter = Regex.Matches(data, @"[a-zA-Z]").Count;
            if (errorCounter == 0)
            {
                string[] dapat = data.Split('.');
                if (dapat.Length != 4)
                {
                    valid = "Error : IP must 4 point";
                    return false;
                }
                if (dapat[3].Equals("0"))
                {
                     valid = "Error : the fourth digit of ip cannot be 0";
                    return false;
                }
               
                bool kosong = false;
                if (dapat[0].Equals("000"))
                    kosong = true;
                for (int i = 0; i < dapat.Length; i++)
                {
                    //if (dapat[i] == "" || dapat[i].Length <= 0)
                    //{
                    //    new Confirm("Error : IP must 4 point");
                    //    return false;
                    //}
                    if (dapat[i][0].Equals("0"))
                        return false;
                    if (dapat[i].Equals("00"))
                    {
                        valid = "Error : IP cannot fill 00";
                        return false;
                    }

                    bool rege = Regex.Match(dapat[i], @"^[0-9]+$").Success;
                    if (!rege)
                    {
                         valid = "Error : IP cannot contain alphabeth and symbols " + data;
                        return false;
                    }
                    if (dapat[i].Length == 0)
                    {
                         valid = "Error : Cannot empty";
                        return false;
                    }
                    int temp = int.Parse(dapat[i]);
                    if (temp >= 255)
                    {
                        valid = "Error : IP must be less then 255";
                        return false;
                    }
                    if (temp == 0 && kosong)
                    {
                         valid = "Error : First digit of ip cannot be 0";
                        return false;
                    }
                    if (dapat[i].Equals("000") && !kosong)
                    {
                         valid = "Error : IP cannot fill 000";
                        return false;
                    }
                    if (dapat[i][0].Equals('0') && temp != 0 && dapat[i].Length > 1)
                    {
                         valid = "Error : Ip cannot fill '0'";
                        return false;
                    }
                }
                return true;
            }
            else
            {
                 valid = "Error : IP cannot contain alphabeth " + errorCounter;
                return false;
            }
            return true;
        }

        public void systemlog()
        {
            Dictionary<string, string> field = new Dictionary<string, string>();
            field.Add("eventtime", getUnixString());
            field.Add("eventname", eventname);
            field.Add("userid", adminid);
            field.Add("eventtype", eventType);
            field.Add("uom", uom);
            field.Add("[from]", from);
            field.Add("[to]", to);
            insert(field, "system_log");
        }

        public bool PingHost(string nameOrAddress)
        {
            bool pingable = false;
            Ping pinger = new Ping();
            try
            {
                PingReply reply = pinger.Send(nameOrAddress);
                pingable = reply.Status == IPStatus.Success;
            }
            catch (PingException)
            {
                // Discard PingExceptions and return false;
            }
            return pingable;
        }
       
        public int PackagingqtyOnprogress;
       
        public bool onprogress(string batch)
        {
            List<string> field = new List<string>();
            field.Add("status,packagingqty");
            string where = "batchnumber = '"+batch+"'";
            List<string[]> ds = selectList(field, "packaging_order", where);
            if (num_rows > 0)
            {
                string status = ds[0][0];
                if (status.Equals("99"))
                    return false;
                else
                    return true;
                PackagingqtyOnprogress = int.Parse(ds[0][1]);
            }
            else
            {
                return false;
            }
        }
   
        public static string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            throw new Exception("Local IP Address Not Found!");
        }

        public string [] cekLine(string where)
        {
            ipAddress = GetLocalIPAddress();
            List<string> field = new List<string>();
            string[] temp = new string[0];
             field.Add("lineName,linePackaging.linePackagingId");
            string from = "linePackagingDetail INNER JOIN linePackaging ON linePackagingDetail.linePackagingId = linePackaging.linePackagingId";
            //string where1 = where + "= '" + GetLocalIPAddress() + "'";
           // string where1 = where + "= '172.16.160.103'";
            string where1 = where + "= '192.168.11.100'";
            List<string[]> ds = selectList(field, from, where1);
            if (num_rows > 0)
                return ds[0];
            else 
                return temp;
        }

        public bool decomission(string capid)
        {
            List<string> fieldlist = new List<string>();
            fieldlist.Add("capid");
            selectList(fieldlist, "Vaccine", "Capid like '%" + capid + "%' or gsOneVialId='"+capid+"'");
            if (num_rows > 0)
            {
                Dictionary<string, string> field = new Dictionary<string, string>();
                field.Add("isreject", "2");
                string where = "Capid like '%" + capid + "%' or gsOneVialId='" + capid + "'";
                update(field, "Vaccine", where);
                return true;
            }
            else
            {
                new Confirm("Data Not Found!", "Information", MessageBoxButtons.OK);
                return false;
            }
        }

        public int getcountVaccine(string batch)
        {
            List<string> field = new List<string>();
            field.Add("count(*)");
            string where = "batchnumber ='"+batch+"'";
            List<string[]> ds = selectList(field, "Vaccine", where);
            if (num_rows > 0)
                return int.Parse(ds[0][0]);
            else
                return 0;
        }

        public string[] dataPO(string batch)
        {
            List<string> field = new List<string>();
            field.Add("gtin,expired,packagingqty,model_name,product_model");
            string from = "packaging_order INNER JOIN product_model ON packaging_order.productModelId = product_model.product_model";
            string where = "batchnumber = '" + batch + "'";
            List<string[]> ds = selectList(field, from, where);
            return ds[0];
        }

        internal int getcountProduct(string batch)
        {
            List<string> field = new List<string>();
            field.Add(Control.NAMA_SEQ);
            string where = "batchnumber = '" + batch + "'";
            List<string[]> ds = selectList(field, "packaging_order", where);
            if (num_rows > 0)
                return int.Parse(ds[0][0]);
            else
                return 0;
        }
      
        public void updatesequence(string batch, string status)
        {
            Dictionary<string, string> field = new Dictionary<string, string>();
            field.Add(Control.NAMA_SEQ, status);
            string where = "batchnumber = '" + batch + "'";
            update(field, "packaging_order", where);
        }

        public bool sampleData(string productid)
        {
            Dictionary<string, string> field = new Dictionary<string, string>();
            //status sample
            field.Add("isReject", "3");
            string where = "(capid like '%" + productid + "%' OR gsonevialid ='" + productid + "' )";
            update(field, Control.NAMA_TABLE_UTAMA, where);
            return true;
        }

        internal bool cekPermision(string adminid, string modul_name, string permission)
        {
            List<string> field = new List<string>();
            field.Add("permissions_name");
            string where = "dbo.users.id = '" + adminid + "' AND dbo.permissions.module_name = '" + modul_name + "' AND dbo.permissions.permissions_name = '" + permission + "'";
            string from = "dbo.groups INNER JOIN dbo.permissions ON dbo.groups.id = dbo.permissions.role_id INNER JOIN dbo.users_groups ON dbo.users_groups.group_id = dbo.groups.id  INNER JOIN dbo.users ON dbo.users_groups.user_id = dbo.users.id";
            selectList(field, from, where);

            Connection.Close();
            if (num_rows > 0)
                return true;
            return false;
        }

        //tambahan
        internal List<string[]> getRole(string adminid)
        {
            List<string> field = new List<string>();
            field.Add("first_name, company");
            string where = "id = '" + adminid + "';";
            string from = "dbo.[users]";
            List<string[]> result = selectList(field, from, where);
            Connection.Close();
            if (result.Count > 0)
            {
                return result;
            }
            else
                return null;
        }

        public int selectCountPass(string batchNumber)
        {
            try
            {
                string sql = "SELECT Count(*) from [Vaccine] where batchNumber = '" + batchNumber + "' and isReject = 0 ";

                simpanLog(sql);
                if (Connect())
                {
                    Command = new SqlCommand(sql, Connection);
                    DataReader = Command.ExecuteReader();
                    int Results = 0;
                    while (DataReader.Read())
                    {
                        string[] data = new string[DataReader.FieldCount];
                        for (int u = 0; u < DataReader.FieldCount; u++)
                        {
                            data[u] = DataReader.GetValue(u).ToString();
                        }

                        Results = Int32.Parse(data[0]);
                    }
                    Connection.Close();
                    return Results;
                }
            }
            catch (SqlException sq)
            {
                simpanLog("Error SQLSERVER : " + sq.ToString());
            }
            finally
            {
                try
                {
                    DataReader.Close();
                    Command.Dispose();
                }
                catch (SqlException sq)
                {
                    simpanLog("Error SQLSERVER : " + sq.ToString());
                }
                catch (NullReferenceException nl)
                {
                    simpanLog("Error SQLSERVER : " + nl.ToString());
                }

            }
            return 0;
        }

        public int selectCountReject(string batchNumber)
        {
            try
            {
                string sql = "SELECT Count(*) from [Vaccine_Reject] where batchNumber = '" + batchNumber + "' and blisterpackId is null";

                simpanLog(sql);
                if (Connect())
                {
                    Command = new SqlCommand(sql, Connection);
                    DataReader = Command.ExecuteReader();
                    int Results = 0;
                    while (DataReader.Read())
                    {
                        string[] data = new string[DataReader.FieldCount];
                        for (int u = 0; u < DataReader.FieldCount; u++)
                        {
                            data[u] = DataReader.GetValue(u).ToString();
                        }

                        Results = Int32.Parse(data[0]);
                    }
                    Connection.Close();
                    return Results;
                }
            }
            catch (SqlException sq)
            {
                simpanLog("Error SQLSERVER : " + sq.ToString());
            }
            finally
            {
                try
                {
                    DataReader.Close();
                    Command.Dispose();
                }
                catch (SqlException sq)
                {
                    simpanLog("Error SQLSERVER : " + sq.ToString());
                }
                catch (NullReferenceException nl)
                {
                    simpanLog("Error SQLSERVER : " + nl.ToString());
                }

            }
            return 0;
        }
        //end of tambahan
    }
}