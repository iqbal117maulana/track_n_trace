﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bottle_Station
{
    public class Variable
    {
        //Master
        public const string NAMA_FIELD_Capid = "capid";
        public const string NAMA_FIELD_GSOne = "Gsonevialid";
        public const string NAMA_TABLE_TEMP_Capid = "tmp_cap_id";
        public const string NAMA_TABLE_TEMP_GSOne = "tmp_gsOneVial";
        public const string NAMA_TABLE_UTAMA = "Vaccine";
        public const string NAMA_SEQ = "bottleseq";
        public const string movementType = "6";

        // configuration
        string config;

        // Get and Set data
        List<string> DataTempCapid = new List<string>();
        List<string> DataTempGSone = new List<string>();

        //Receive from camera
        string Receive;

        //Status Status 
        bool camera1;
        bool camera2;
        bool camera3;
        bool printer1;
        bool printer2;
        bool server;

        //counter
        int countGenerate;
        int countToSend_Capid;
        int countToSend_GSOne;
        int countSend_Capid;
        int countSend_Gsone;
        int countSendDataAudit;
        int countReceive;

        //Sqlite
        public int QTYGenerate;
        public int QTYSend;
        public string PortServer;
        public string IpPrinter;
        public string PortPrinter;
        public string IpCamera;
        public string PortCamera;
        public string IpDatabase;
        public string portDatabase;
        public string UsernameDatabase;
        public string PasswordDatabase;
        public string timeout;
        public string IPPrinter2;
        public string PortPrinter2;

        public string IPCamera2;
        public string PortCamera2;

        public string delay;
        public string NameDatabase;

        internal void SetSqliteVariabel(Dictionary<string, string> dictionary)
        {
            PortServer = dictionary["portserver"];
            IpPrinter = dictionary["ipprinter"];
            PortPrinter = dictionary["portprinter"];
            IpCamera = dictionary["ipCamera"];
            PortCamera = dictionary["portCamera"];
            IpDatabase = dictionary["ipDB"];
            portDatabase = dictionary["portDB"];
            UsernameDatabase = dictionary["usernameDB"];
            PasswordDatabase = dictionary["passDB"];
            NameDatabase = dictionary["namaDB"];
            timeout = dictionary["timeout"];
            IPPrinter2 = dictionary["ipmarker"];
            PortPrinter2 = dictionary["portmarker"];
            IPCamera2 = dictionary["ipcamera2"];
            PortCamera2 = dictionary["portcamera2"];
            QTYGenerate = int.Parse(dictionary["qtygenerate"]);
            QTYSend = int.Parse(dictionary["qtysend"]);
            delay = dictionary["delay"];
            IPPrinter2 = dictionary["ipmarker"];
        }
    }
}
