﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.IO;
using System.Windows.Forms;
using System.Threading;

namespace Bottle_Station
{
    public class Mod_TCP2
    {
        private static TcpClient Client;
        private static Byte[] Data = new Byte[256];
        private static Stream ClientStream;

        public string IPAddress="";
        public string port="";
        public string timeout="";
        public string statusFail;
        public string dataPrint;
        internal static bool IsConnected = false;
        Control ctrl;
        private Thread serverThread;
        private Thread serverThread2;
        public bool conneect = true;
        
        internal bool checkConnecteion()
        {
            bool temp = OpenConnection();
            if (temp)
                CloseConnection();
            return temp;
        }

        internal bool OpenConnection()
        {
            if (checkParameter())
            {
                try
                {
                    Log("Try Open Connection IP "+IPAddress+" Port "+port);
                    Client = new TcpClient();
                   // IAsyncResult result = Client.BeginConnect(IPAddress, Int32.Parse(port), null, null);
                    //bool success = result.AsyncWaitHandle.WaitOne(Int32.Parse(timeout), true);
                    Client.Connect(IPAddress,int.Parse(port));
                    
                    if (Client.Connected)
                    {
                        ClientStream = Client.GetStream();
                        Log("Open Connection Success");
                        IsConnected = true;
                        return true;
                    }
                    else
                    {
                        
                        Log("Open Connection Failed [1] ");
                        IsConnected = false;
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    Log("Open Connection Failed "+ex.Message);
                    statusFail = ex.Message;
                }
            }
            else
            {
                Log("Configuration Parameter Not Good");
                statusFail = "Configuration Parameter Not Good";
                return false;
            }
            return false;
        }

        internal void CloseConnection()
        {
            try
            {
                ClientStream.Close();
                Client.Close();
            }
            catch (Exception ex)
            {
            }
        }

        internal bool CheckConnection()
        {
            return Client.Connected;
        }

        private bool checkParameter()
        {
            if (IPAddress.Length == 0 || port.Length == 0 || timeout.Length == 0)
            {
                return false;
            }
            return true;
        }

        internal string listener()
        {
            try
            {
                byte[] data = new Byte[256];
                if (IsConnected)
                {
                    if (ClientStream == null)
                    {
                        send("cek");
                    }
                    // ClientStream.ReadTimeout = 8000;
                    Int32 bytes = ClientStream.Read(data, 0, data.Length);
                    string responseData = System.Text.Encoding.ASCII.GetString(data, 0, bytes);
                    return responseData;
                }
                else
                {
                    OpenConnection();
                }
            }
            catch (Exception ex)
            {
                CloseConnection();
                IsConnected = false;
                if (OpenConnection())
                {
                    send("^0?SM");
                }
                return "err "+ex.Message;
            }
            return "";
        }
        
        internal void send(string dataSend)
        {
            try
            {
                //wait printer connected 
                // jika sudah ke send 
                bool send = false;
                // count retry try to connect
                int count = 0;
                while (!send&& count < 5)
                {
                    if (IsConnected)
                    {
                        StreamWriter sw = new StreamWriter(ClientStream);
                        sw.WriteLine(dataSend);
                        sw.Flush();
                        send = true;
                    }
                    else
                    {
                        count++;
                        int second = int.Parse(ctrl.sqlite.config["second"]);
                        Thread.Sleep(second * 1000);
                        Log("Try to reconnected "+count);
                        OpenConnection();
                    }
                }
            }
            catch (Exception ex)
            {
                CloseConnection();
                IsConnected = false;
                if (OpenConnection())
                {
                    send("^0?SM");
                }
            }
        }
     
        public void Log(String line)
        {
            DateTime now = DateTime.Now;
            string tahun = now.ToString("yyyy");
            string bulan = now.ToString("MM");
            string hari = now.ToString("dd");
            string dates = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            line = dates + "\t" + line;
            // cek file 
            bool cekfile = false;
            string namaFolder = "eventlog\\TCP";
            try
            {
                while (!cekfile)
                {
                    if (Directory.Exists(namaFolder))
                    {
                        if (Directory.Exists(namaFolder + @"\" + tahun))
                        {
                            if (Directory.Exists(namaFolder + @"\" + tahun + @"\" + bulan))
                            {

                                using (StreamWriter outputFile = new StreamWriter(namaFolder + @"\" + tahun + @"\" + bulan + @"\" + hari + ".txt", true))
                                {
                                    outputFile.WriteLine(line);
                                }
                                cekfile = true;
                            }
                            else
                            {

                                Directory.CreateDirectory(namaFolder + @"\" + tahun + @"\" + bulan);
                            }
                        }
                        else
                        {
                            Directory.CreateDirectory(namaFolder + @"\" + tahun);
                        }
                    }
                    else
                    {

                        Directory.CreateDirectory(namaFolder);
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }
        
        public void startListener()
        {
            serverThread = new Thread(new ThreadStart(MulaiServerCapid));
            serverThread.Start();
        }
        
        public void startCheckStatus(Control ct)
        {
            ctrl = ct;
            serverThread2 = new Thread(new ThreadStart(MulaiCheker));
            serverThread2.Start();
        }
       
        public void MulaiCheker()
        {
            while (conneect)
            {
                // kirim ceker terus
                send("^0?SM");
                Thread.Sleep(5000);
            }
        }

        public void MulaiServerCapid()
        {
            while (conneect)
            {
                int QtySend = int.Parse(ctrl.sqlite.config["qtysend"]);
                string data = listener();
                ctrl.log(data);
                string[] dataSplit = data.Split('\t');
                int dataInt = int.Parse(dataSplit[1]);
                if (dataInt < QtySend*2)
                {
                    List<string> dataKirim= ctrl.getDataSendToPrinter(QtySend);
                    //kirim ulang
                    ctrl.log("Send to printer"); 
                    Dictionary<string, string> field = new Dictionary<string, string>();
                    field.Add("isused", "1");
                    for(int i = 0; i <QtySend;i++)
                    {
                        ctrl.log("Send to printer :" + dataKirim[i]);
                        ctrl.sqlite.update(field, "tmp_cap_id", "capid = '" + dataKirim[i] + "'");
                        send("^0=MR" + i + " " + dataKirim[i]);
                    }

                    ctrl.SetDataGrid_Send("0", "");
                    ctrl.CheckRegenerateData();
                }
            }
        }

    }
}
