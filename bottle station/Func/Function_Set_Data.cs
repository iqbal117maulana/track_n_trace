﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bottle_Station
{
    class Function_Set_Data
    {

        public static string RemoveCharFromCamera(string data)
        {
            data = data.Replace("\r", "");
            data = data.Replace("\n", "");
            data = data.Replace("]d2", "");
            data = data.Replace("$", "");
            data = data.Replace("?", "");
            data = data.Replace(" ", "");
            data = data.Replace("<GS>", "");
            return data;
        }

    }
}
