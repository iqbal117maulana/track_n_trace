﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace Bottle_Station
{
    public partial class Decomission : Form
    {
        dbaccess db;
        string linenumber;
        Model.Vaccine vaccine;

        public Decomission(string admin, string line, string parameter)
        {
            InitializeComponent();
            db = new dbaccess();
            db.adminid = admin;
            db.from = "Bottle Station";
            linenumber = line;
            SetBatchNumber(parameter);
            vaccine = new Model.Vaccine();
            loadData();
        }

        private void SetBatchNumber(string parameter)
        {
            List<string> field = new List<string>();
            field.Add("batchNumber,productModelId");
            string where = "status = '1' and linepackagingid = '" + linenumber + "' and productModelId IN " + parameter + "";
            List<string[]> datacombo = db.selectList(field, "[packaging_order]", where);
            List<string> da = new List<string>();
            //for (int i = 0; i < datacombo.Count; i++)
            for (int i = datacombo.Count-1; i > -1; i--)
            {
                da.Add(datacombo[i][0]);
            }
            cmbBatch.DataSource = da;
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                decomision();
            }
        }

        private void decomision()
        {
            vaccine = new Model.Vaccine();
            txtProduct.Text = Util.Common.REMOVE_UNUSED_CHAR(txtProduct.Text);
            vaccine.CAPID = txtProduct.Text;
            if (!Util.Common.CHECK_EMPTY(txtProduct))
            {
                vaccine.CAPID = txtProduct.Text;
                vaccine.GS1VIALID = txtProduct.Text;
                if (vaccine.CHECK_VACCINE())
                {
                    vaccine.BATCHNUMBER = cmbBatch.Text;
                    if (vaccine.CHECK_VACCINE())
                    {
                        vaccine.ISREJECT = "0";
                        if (vaccine.CHECK_VACCINE())
                        {
                            vaccine.BLISTERID = "kosong";
                            if (vaccine.CHECK_VACCINE())
                            {
                                if (vaccine.DECOMMISSION())
                                {
                                    db.eventname = "Decomission =" + txtProduct.Text + " success";
                                    db.to = txtProduct.Text;
                                    db.systemlog();
                                    log(txtProduct.Text + " decommission success");
                                    loadData();
                                }
                                else
                                {
                                    db.eventname = "Decommission failed," + txtProduct.Text + " Something wrong with Database";
                                    db.to = txtProduct.Text;
                                    db.systemlog();
                                    log("Decommission failed," + txtProduct.Text + " Something wrong with Database");
                                }
                            }
                            else
                            {
                                db.eventname = "Decommission failed," + txtProduct.Text + " is register with blister";
                                db.to = txtProduct.Text;
                                db.systemlog();
                                log("Decommission failed," + txtProduct.Text + " is register with blister");

                            }
                        }
                        else
                        {
                            db.eventname = "Decommission failed," + txtProduct.Text + " is not a good product";
                            db.to = txtProduct.Text;
                            db.systemlog();
                            log("Decommission failed," + txtProduct.Text + " is not a good product ");
                        }
                    }
                    else
                    {
                        db.eventname = "Decommission failed," + txtProduct.Text + " Wrong BatchNumber";
                        db.to = txtProduct.Text;
                        db.systemlog();
                        log("Decommission failed," + txtProduct.Text + " Wrong BatchNumber");

                    }
                }
                else
                {
                    db.eventname = "Decomission =" + txtProduct.Text + " Not Found";
                    db.to = txtProduct.Text;
                    db.systemlog();
                    log("Decommission failed," + txtProduct.Text + " Not Found");
                }
            }
            else
            {
                log("Decommission failed\n");
                new Confirm("Vaccine ID is empty", "Information", MessageBoxButtons.OK);
            }

            txtProduct.Text = "";
            txtProduct.Focus();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            decomision();
        }

        private void Decomission_Load(object sender, EventArgs e)
        {
            txtProduct.Text = "";
            txtProduct.Focus();
        }

        private void log(string line)
        {
            line = Util.Common.SetWithDate(line);
            txtLog.AppendText(line+"\n");
            txtLog.AppendText(Environment.NewLine);
        }

        private void loadData()
        {
            List<string> field = new List<string>();
            field.Add("gsOneVialId, SUBSTRING(gsOneVialId, CHARINDEX('BIOF', gsOneVialId), 14)");
            string where = "batchNumber = '" + cmbBatch.Text + "' and isReject = 0 ";
            List<string[]> data = db.selectList(field, "[Vaccine]", where);
            AutoCompleteStringCollection namesCollection = new AutoCompleteStringCollection();
            for (int i = 0; i < data.Count; i++)
            {
                namesCollection.Add(data[i][0]);
            }
            txtProduct.AutoCompleteMode = AutoCompleteMode.None;
            txtProduct.AutoCompleteSource = AutoCompleteSource.CustomSource;
            txtProduct.AutoCompleteCustomSource = namesCollection;
        }

        private void cmbBatch_SelectedIndexChanged(object sender, EventArgs e)
        {
            loadData();
        }

        private void txtProduct_TextChanged(object sender, EventArgs e)
        {
            listBox1.Items.Clear();
            if (txtProduct.Text.Length == 0)
            {
                hideResults();
                return;
            }

            foreach (String ss in txtProduct.AutoCompleteCustomSource)
            {
                if (ss.Contains(txtProduct.Text))
                {
                    listBox1.Items.Add(ss);
                    listBox1.Visible = true;
                }
            }
        }

        void hideResults()
        {
            listBox1.Visible = false;
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtProduct.Text = listBox1.Items[listBox1.SelectedIndex].ToString();
            hideResults();
        }

        void listBox1_LostFocus(object sender, System.EventArgs e)
        {
            hideResults();
        }

        private void txtProduct_KeyPress(object sender, KeyPressEventArgs e)
        {
            var regex = new Regex(@"[^a-zA-Z0-9\b]");
            if (regex.IsMatch(e.KeyChar.ToString()))
            {
                e.Handled = true;
            }
        }
    }
}
