﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Globalization;
using System.Text.RegularExpressions;
using Excel = Microsoft.Office.Interop.Excel;
using System.IO;

namespace Bottle_Station
{
    public partial class ReportVaccine : Form
    {

        public Dashboard dbss;
        string product;
        ListBatchNumber lbn;
        dbaccess db = new dbaccess();
        string linenumber;
        DataTable table1 = new DataTable();
        DataTable table2 = new DataTable();
        DataTable table3 = new DataTable();
        Boolean pass = false;
        Boolean fail = false;
        Boolean sample = false;
        string batch;
        List<string[]> data;

        public ReportVaccine(Dashboard dsb, string line)
        {
            InitializeComponent();
            this.Text = this.Text + " V " + System.Windows.Forms.Application.ProductVersion;
            dbss = dsb;
            linenumber = line;
            GetComboBoxData();
            btnGetReport.Enabled = false;
            btnExport.Enabled = false;
            refresh();
        }

        private void ReportVaccine_Load(object sender, EventArgs e)
        {
            timer1.Start();
            lblTime.Text = DateTime.Now.ToLongTimeString();
            lblDate.Text = DateTime.Now.ToShortDateString();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            lblTime.Text = DateTime.Now.ToLongTimeString();
            timer1.Start();
        }

        internal void refresh()
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new Action(refresh));
                return;
            }
            table1 = new DataTable();
            table1.Columns.Add("No");
            table1.Columns.Add("Cap ID");
            table1.Columns.Add("GS1 ID");
            table1.Columns.Add("Created Time");
            table1.Columns.Add("Expire Date");
            dataGridView1.DataSource = table1;
            this.dataGridView1.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridView1.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridView1.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridView1.Columns[3].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridView1.Columns[4].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;

            table2 = new DataTable();
            table2.Columns.Add("No");
            table2.Columns.Add("Cap ID");
            table2.Columns.Add("GS1 ID");
            table2.Columns.Add("Created Time");
            table2.Columns.Add("Expire Date");
            table2.Columns.Add("Description");
            dataGridView2.DataSource = table2;
            this.dataGridView2.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridView2.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridView2.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridView2.Columns[3].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridView2.Columns[4].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridView2.Columns[5].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;

            table3 = new DataTable();
            table3.Columns.Add("No");
            table3.Columns.Add("Cap ID");
            table3.Columns.Add("GS1 ID");
            table3.Columns.Add("Created Time");
            table3.Columns.Add("Expire Date");
            dataGridView3.DataSource = table3;
            this.dataGridView3.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridView3.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridView3.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridView3.Columns[3].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridView3.Columns[4].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
        }

        private void pictureBox1_DoubleClick(object sender, EventArgs e)
        {
            dbss.Show();
            this.Hide();
        }

        private void pictureBox1_MouseEnter(object sender, EventArgs e)
        {
            this.pictureBox1.BackColor = ColorTranslator.FromHtml("#00505E");
        }

        private void pictureBox1_MouseHover(object sender, EventArgs e)
        {
            this.pictureBox1.BackColor = ColorTranslator.FromHtml("#002F36");
        }

        private void ReportVaccine_FormClosed(object sender, FormClosedEventArgs e)
        {
            dbss.Show();
            this.Hide();
        }

        public void GetComboBoxData()
        {
            List<string> field = new List<string>();
            field.Add("model_name,product_model");
            List<string[]> datacombo = db.selectList(field, "[product_model]", "");
            List<ComboboxItem> da = new List<ComboboxItem>();
            for (int i = 0; i < datacombo.Count; i++)
            {
                da.Add(new ComboboxItem() { Text = datacombo[i][0], Value = datacombo[i][1] });
            }
            cmbProduct.DataSource = da;
            cmbProduct.DisplayMember = "Text";
            cmbProduct.ValueMember = "Value";
        }

        public class ComboboxItem
        {
            public string Text { get; set; }
            public object Value { get; set; }
        }

        private void cmbProduct_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboboxItem ci1 = cmbProduct.SelectedItem as ComboboxItem;
            product = ci1.Value.ToString();
        }

        private void txtBatch_Click(object sender, EventArgs e)
        {
            lbn = new ListBatchNumber(null, linenumber, "(" + product + ")", "");
            lbn.ShowDialog();
            txtBatch.Text = lbn.batch;
            btnGetReport.Focus();
            if (txtBatch.Text.Length > 0)
            {
                btnGetReport.Enabled = true;
            }
            else
            {
                btnGetReport.Enabled = false;
            }
        }

        public void SetDataGridPass()
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new Action(SetDataGridPass));
                return;
            }
            List<string> field = new List<string>();
            field.Add("capId 'Cap ID', gsOneVialId 'GS1 ID', createdTime 'Created Time', expDate 'Expire Date'");
            string where = "batchNumber = '" + txtBatch.Text + "' and isReject = 0;";
            List<string[]> dss = db.selectList(field, "[Vaccine]", where);
            if (db.num_rows > 0)
            {
                lblPas.Text = dss.Count.ToString();
                table1 = db.dataHeader;
                table1.Columns.Add("No").SetOrdinal(0);
                int i = 0;
                foreach (string[] Row in dss.Reverse<string[]>())
                {
                    DateTime dateExpiry = DateTime.ParseExact(Row[3], "yyMMdd", CultureInfo.InvariantCulture);
                    var a = new DateTime(1970, 1, 1, 0, 0, 0).AddSeconds(long.Parse(Row[2])).ToLocalTime();
                    i++;
                    DataRow row = table1.NewRow();
                    row[0] =  i;
                    row[1] = "'" + Row[0];
                    row[2] = Row[1];
                    row[3] = a;
                    row[4] = dateExpiry.ToShortDateString();
                    table1.Rows.Add(row);
                }

                //this.dataGridView1.DataSource = table;
                //this.dataGridView1.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                //this.dataGridView1.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                //this.dataGridView1.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                //this.dataGridView1.Columns[3].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                //this.dataGridView1.Columns[4].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                pass = true;
            }
            else
            {
                lblPas.Text = "0";
                pass = false;
                //Confirm cf = new Confirm("Cannot find vaccine with batch number " + txtBatch.Text, "Information", MessageBoxButtons.OK);
            }
        }

        public void SetDataGridFail()
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new Action(SetDataGridFail));
                return;
            }
            
            List<string> field2 = new List<string>();
            field2.Add("capId 'Cap ID', gsOneVialId 'GS1 ID', createdTime 'Created Time', expDate 'Expire Date', isReject");
            string where2 = "batchNumber = '" + batch +"' and isReject in (5,6,1);";
            List<string[]> dss2 = db.selectList(field2, "[Vaccine_Reject]", where2);

            List<string> field = new List<string>();
            field.Add("capId 'Cap ID', gsOneVialId 'GS1 ID', createdTime 'Created Time', expDate 'Expire Date', isReject");
            string where = "batchNumber = '" + batch + "' and isReject = 2;";
            List<string[]> dss = db.selectList(field, "[Vaccine]", where);
            if (dss2.Count > 0 && dss.Count > 0)
            {
                dss.AddRange(dss2);
                data = dss;
            }
            else
            {
                if (dss.Count <= 0 && dss2.Count > 0) {
                    data = dss2;
                }
                else
                {
                    data = dss;
                }
            }

            //if (db.num_rows > 0)
            //{
                lblFail.Text = data.Count.ToString();
                table2 = db.dataHeader;
                table2.Columns.Add("No").SetOrdinal(0);
                table2.Columns.Add("Description").SetOrdinal(5);
                table2.Columns.Remove("isReject");
                int i = 0;
                foreach (string[] Row in data.Reverse<string[]>())
                {
                    DateTime dateExpiry = DateTime.ParseExact(Row[3], "yyMMdd", CultureInfo.InvariantCulture);
                    var a = new DateTime(1970, 1, 1, 0, 0, 0).AddSeconds(long.Parse(Row[2])).ToLocalTime();
                    i++;
                    DataRow row = table2.NewRow();
                    row[0] = i;
                    row[1] = "'" + Row[0];
                    row[2] = Row[1];
                    row[3] = a;
                    row[4] = dateExpiry.ToShortDateString();
                    if (Row[0].Equals("XXXXXX") || Row[0].Equals("XXXXX") || Row[0].Equals("XXXXXXX") || Row[0].Equals("xxxxx") || Row[0].Equals("xxxxxx") || Row[0].Equals("xxxxxxx") || !Regex.IsMatch(Row[0], @"^[a-zA-Z0-9]"))
                    {
                        row[5] = "Cap ID can not be read";
                    }
                    else if(!Regex.IsMatch(Row[1], @"^[a-zA-Z0-9]"))
                    {
                        row[5] = "GS1 ID can not be read";
                    }
                    else if (Row[4].Equals("2"))
                    {
                        row[5] = "Decommission";
                    }
                    else if (Row[4].Equals("5"))
                    {
                        row[5] = "Cap ID already exist";
                    }
                    else if (Row[4].Equals("6"))
                    {
                        row[5] = "GS1 ID already exist";
                    }
                    else if (!Regex.IsMatch(Row[1], @"^[a-zA-Z0-9]") && !Regex.IsMatch(Row[0], @"^[a-zA-Z0-9]"))
                    {
                        row[5] = "Can not be read";
                    }
                    table2.Rows.Add(row);
                }

                //this.dataGridView2.DataSource = table2;
                //this.dataGridView2.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                //this.dataGridView2.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                //this.dataGridView2.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                //this.dataGridView2.Columns[3].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                //this.dataGridView2.Columns[4].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                //this.dataGridView2.Columns[5].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                fail = true;
            //}
            //else
            //{
            //    lblFail.Text = "0";
            //    fail = false;
            //    //Confirm cf = new Confirm("Cannot find fail vaccine with batch number " + txtBatch.Text, "Information", MessageBoxButtons.OK);
            //}
        }

        public void SetDataGridSample()
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new Action(SetDataGridSample));
                return;
            }

            List<string> field = new List<string>();
            field.Add("capId 'Cap ID', gsOneVialId 'GS1 ID', createdTime 'Created Time', expDate 'Expire Date'");
            string where = "batchNumber = '" + txtBatch.Text + "' and isReject = 3;";
            List<string[]> dss = db.selectList(field, "[Vaccine]", where);
            if (db.num_rows > 0)
            {
                lblSample.Text = dss.Count.ToString();
                table3 = db.dataHeader;
                table3.Columns.Add("No").SetOrdinal(0);
                int i = 0;
                foreach (string[] Row in dss.Reverse<string[]>())
                {
                    DateTime dateExpiry = DateTime.ParseExact(Row[3], "yyMMdd", CultureInfo.InvariantCulture);
                    var a = new DateTime(1970, 1, 1, 0, 0, 0).AddSeconds(long.Parse(Row[2])).ToLocalTime();
                    i++;
                    DataRow row = table3.NewRow();
                    row[0] = i;
                    row[1] = "'" + Row[0];
                    row[2] = Row[1];
                    row[3] = a;
                    row[4] = dateExpiry.ToShortDateString();
                    table3.Rows.Add(row);
                }

                //this.dataGridView3.DataSource = table3;
                //this.dataGridView3.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                //this.dataGridView3.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                //this.dataGridView3.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                //this.dataGridView3.Columns[3].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                //this.dataGridView3.Columns[4].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                sample = true;
            }
            else
            {
                lblSample.Text = "0";
                sample = false;
                //Confirm cf = new Confirm("Cannot find sample vaccine with batch number " + txtBatch.Text, "Information", MessageBoxButtons.OK);
            }
        }

        private void btnGetReport_Click(object sender, EventArgs e)
        {
            refresh();
            batch = txtBatch.Text;
            backgroundWorker1 = new BackgroundWorker();
            backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork_1);
            backgroundWorker1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker1_RunWorkerCompleted_1);
            backgroundWorker1.RunWorkerAsync();
            loadingGif.Visible = true;
            btnGetReport.Enabled = false;
            btnExport.Enabled = false;
        }

        private void Task() { 
            //if (this.InvokeRequired)
            //{
            //    this.Invoke(new Action(Task));
            //    return;
            //}
            //refresh();
            SetDataGridPass();
            SetDataGridFail();
            SetDataGridSample();
            
            //Thread.Sleep(2000);
        }

        private void backgroundWorker1_DoWork_1(object sender, DoWorkEventArgs e)
        {
            
            Task();
        }

        private void backgroundWorker1_RunWorkerCompleted_1(object sender, RunWorkerCompletedEventArgs e)
        {
            showDataToGrid();
            if (pass == true || fail == true || sample == true)
            {
                btnExport.Enabled = true;
            }
            else if (pass == false || fail == false || sample == false)
            {
                btnExport.Enabled = false;
            }
            btnGetReport.Enabled = true;
            loadingGif.Visible = false;
        }

        private void showDataToGrid() {
            this.dataGridView1.DataSource = table1;
            this.dataGridView1.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridView1.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridView1.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridView1.Columns[3].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridView1.Columns[4].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;

            this.dataGridView2.DataSource = table2;
            this.dataGridView2.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridView2.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridView2.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridView2.Columns[3].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridView2.Columns[4].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridView2.Columns[5].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;

            this.dataGridView3.DataSource = table3;
            this.dataGridView3.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridView3.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridView3.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridView3.Columns[3].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridView3.Columns[4].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            cmbProduct.Enabled = false;
            btnGetReport.Enabled = false;
            btnExport.Enabled = false;
            exportWithPaste();
            cmbProduct.Enabled = true;
            btnGetReport.Enabled = true;
            btnExport.Enabled = true;
        }

        private void copyAlltoClipboard()
        {
            dataGridView1.SelectAll();
            DataObject dataObj = dataGridView1.GetClipboardContent();
            if (dataObj != null)
                Clipboard.SetDataObject(dataObj);
        }

        private void copyAlltoClipboard1()
        {
            dataGridView2.SelectAll();
            DataObject dataObj = dataGridView2.GetClipboardContent();
            if (dataObj != null)
                Clipboard.SetDataObject(dataObj);
        }

        private void copyAlltoClipboard2()
        {
            dataGridView3.SelectAll();
            DataObject dataObj = dataGridView3.GetClipboardContent();
            if (dataObj != null)
                Clipboard.SetDataObject(dataObj);
        }

        //private void exportWithoutSaveAs()
        //{
        //    string filelocation = "d:\\" + cmbProduct.Text + " Batch Number " + txtBatch.Text + ".xls";
        //    Microsoft.Office.Interop.Excel.Application xlexcel;
        //    Microsoft.Office.Interop.Excel.Workbook xlWorkBook;
        //    Microsoft.Office.Interop.Excel.Worksheet xlWorkSheet;
        //    object misValue = System.Reflection.Missing.Value;
        //    xlexcel = new Excel.Application();
        //    xlexcel.Visible = true;
        //    xlWorkBook = xlexcel.Workbooks.Add(misValue);
        //    xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);

        //    for (int i = 1; i < dataGridView1.Columns.Count + 1; i++)
        //    {
        //        xlWorkSheet.Cells[1, i] = dataGridView1.Columns[i - 1].HeaderCell.Value;
        //    }

        //    Excel.Range CR = (Excel.Range)xlWorkSheet.Cells[2, 1];
        //    CR.Select();
        //    xlWorkSheet.PasteSpecial(CR, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, true);
        //    xlexcel.Columns.AutoFit();
        //    xlWorkBook.SaveAs(filelocation, Excel.XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue, Excel.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);
        //    xlWorkBook.Close(true, misValue, misValue);
        //    xlexcel.Quit();

        //    releaseObject(xlWorkSheet);
        //    releaseObject(xlWorkBook);
        //    releaseObject(xlexcel);

        //    MessageBox.Show("Excel file created!");
        //}

        private void exportWithPaste()
        {
            string dir = "c:\\Report TnT\\Bottle Station";
            Directory.CreateDirectory(dir);
            string filelocation = dir + "\\" + cmbProduct.Text + " Batch Number " + txtBatch.Text + ".xls";
            Microsoft.Office.Interop.Excel.Application xlexcel;
            Microsoft.Office.Interop.Excel.Workbook xlWorkBook;
            Microsoft.Office.Interop.Excel.Sheets worksheet;
            object misValue = System.Reflection.Missing.Value;
            xlexcel = new Excel.Application();
            xlexcel.Visible = true;
            xlWorkBook = xlexcel.Workbooks.Add(misValue);
            worksheet = (Excel.Sheets)xlWorkBook.Worksheets;

            //PASS
            copyAlltoClipboard();
            var worksheet1 = (Excel.Worksheet)worksheet.Add(worksheet[1], Type.Missing, Type.Missing, Type.Missing);
            worksheet1.Name = "Pass Vaccine";
            for (int i = 1; i < dataGridView1.Columns.Count + 1; i++)
            {
                worksheet1.Cells[1, i] = dataGridView1.Columns[i - 1].HeaderCell.Value;
                Excel.Range crg = (Excel.Range)worksheet1.Cells[1, i];
                crg.Font.Bold = true;
            }
            Excel.Range CR = (Excel.Range)worksheet1.Cells[2, 1];
            CR.Select();
            worksheet1.PasteSpecial(CR, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, true);
            xlexcel.Columns.AutoFit();

            //FAIL
            copyAlltoClipboard1();
            var worksheet2 = (Excel.Worksheet)worksheet.Add(worksheet[2], Type.Missing, Type.Missing, Type.Missing);
            worksheet2.Name = "Fail Vaccine";
            for (int i = 1; i < dataGridView2.Columns.Count + 1; i++)
            {
                worksheet2.Cells[1, i] = dataGridView2.Columns[i - 1].HeaderCell.Value;
                Excel.Range crg = (Excel.Range)worksheet2.Cells[1, i];
                crg.Font.Bold = true;
            }
            Excel.Range CR1 = (Excel.Range)worksheet2.Cells[2, 1];
            CR1.Select();
            worksheet2.PasteSpecial(CR1, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, true);
            xlexcel.Columns.AutoFit();

            //SAMPLE
            copyAlltoClipboard2();
            var worksheet3 = (Excel.Worksheet)worksheet.Add(worksheet[3], Type.Missing, Type.Missing, Type.Missing);
            worksheet3.Name = "Sample Vaccine";
            for (int i = 1; i < dataGridView3.Columns.Count + 1; i++)
            {
                worksheet3.Cells[1, i] = dataGridView3.Columns[i - 1].HeaderCell.Value;
                Excel.Range crg = (Excel.Range)worksheet3.Cells[1, i];
                crg.Font.Bold = true;
            }
            Excel.Range CR2 = (Excel.Range)worksheet3.Cells[2, 1];
            CR2.Select();
            worksheet3.PasteSpecial(CR1, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, true);
            xlexcel.Columns.AutoFit();


            xlWorkBook.SaveAs(filelocation, Excel.XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue, Excel.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);
            xlWorkBook.Close(true, misValue, misValue);
            xlexcel.Quit();

            releaseObject(worksheet);
            releaseObject(xlWorkBook);
            releaseObject(xlexcel);

            MessageBox.Show("Excel file created " + filelocation);
        }

        //private void exportToExcel()
        //{
        //    Excel.Application xlApp;
        //    Excel.Workbook xlWorkBook;

        //    //worksheet pass, fail, sample
        //    Excel.Worksheet xlWorkSheet;
        //    object misValue = System.Reflection.Missing.Value;

        //    xlApp = new Excel.Application();
        //    xlWorkBook = xlApp.Workbooks.Add(misValue);
        //    xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
        //    int i = 0;
        //    int j = 0;

        //    for (i = 0; i <= dataGridView1.RowCount - 1; i++)
        //    {
        //        for (j = 0; j <= dataGridView1.ColumnCount - 1; j++)
        //        {
        //            DataGridViewCell cell = dataGridView1[j, i];
        //            xlWorkSheet.Cells[i + 1, j + 1] = cell.Value;
        //        }
        //    }
        //    xlApp.Columns.AutoFit();
        //    xlWorkBook.SaveAs("csharp.net-informations.xls", Excel.XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue, Excel.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);
        //    xlWorkBook.Close(true, misValue, misValue);
        //    xlApp.Quit();

        //    releaseObject(xlWorkSheet);
        //    releaseObject(xlWorkBook);
        //    releaseObject(xlApp);

        //    MessageBox.Show("Excel file created , you can find the file c:\\csharp.net-informations.xls");
        //}

        private void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
                MessageBox.Show("Exception Occured while releasing object " + ex.ToString());
            }
            finally
            {
                GC.Collect();
            }
        }

        //private void exportMultipleDatagrid()
        //{
        //    Excel.Application app = new Excel.Application();
        //    Excel.Workbook workbook = app.Workbooks.Add(Type.Missing);
        //    string filelocation = "d:\\"+"Batch Number " + txtBatch.Text + ".xls";
        //    Excel.Sheets worksheet = (Excel.Sheets)workbook.Worksheets;

        //    var worksheet1 = (Excel.Worksheet)worksheet.Add(worksheet[1], Type.Missing, Type.Missing, Type.Missing);
        //    worksheet1.Name = "Pass Vaccine";

        //    //for (int i = 1; i < dataGridView1.Columns.Count + 1; i++)
        //    //{
        //    //    worksheet1.Cells[1, i + 1] = dataGridView1.Columns[i - 1].HeaderCell.Value;

        //    //}

        //    for (int i = 1; i < dataGridView1.Rows.Count + 1; i++)
        //    {
        //        worksheet1.Cells[i + 1, 1] = dataGridView1.Rows[i - 1].HeaderCell.Value;
        //    }

        //    for (int i = 0; i < dataGridView1.Rows.Count; i++)
        //    {
        //        for (int j = 0; j < dataGridView1.Columns.Count; j++)
        //        {
        //            worksheet1.Cells[i + 2, j + 2] = dataGridView1.Rows[i].Cells[j].Value.ToString();
        //        }
        //    }

        //    var worksheet2 = (Excel.Worksheet)worksheet.Add(worksheet[2], Type.Missing, Type.Missing, Type.Missing);
        //    worksheet2.Name = "Fail Vaccine";

        //    for (int i = 1; i < dataGridView2.Columns.Count + 1; i++)
        //    {
        //        worksheet2.Cells[1, i + 1] = dataGridView2.Columns[i - 1].HeaderCell.Value;
        //    }

        //    for (int i = 0; i < dataGridView2.Rows.Count; i++)
        //    {
        //        for (int j = 0; j < dataGridView2.Columns.Count; j++)
        //        {

        //            worksheet2.Cells[i + 2, j + 2] = dataGridView2.Rows[i].Cells[j].Value.ToString();

        //        }
        //    }

        //    var worksheet3 = (Excel.Worksheet)worksheet.Add(worksheet[3], Type.Missing, Type.Missing, Type.Missing);
        //    worksheet3.Name = "Sample Vaccine";

        //    for (int i = 1; i < dataGridView3.Columns.Count + 1; i++)
        //    {
        //        worksheet3.Cells[1, i + 1] = dataGridView3.Columns[i - 1].HeaderCell.Value;
        //    }

        //    for (int i = 0; i < dataGridView3.Rows.Count; i++)
        //    {
        //        for (int j = 0; j < dataGridView3.Columns.Count; j++)
        //        {

        //            worksheet3.Cells[i + 2, j + 2] = dataGridView3.Rows[i].Cells[j].Value.ToString();

        //        }
        //    }


        //        // save the application
        //        //int count = workbook.Worksheets.Count;
        //        //Excel.Worksheet addedSheet = (Microsoft.Office.Interop.Excel.Worksheet)workbook.Worksheets.Add(Type.Missing, workbook.Worksheets[count], Type.Missing, Type.Missing);
        //        workbook.SaveAs(filelocation, Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookNormal, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlExclusive, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
        //        workbook.Close(true, Type.Missing, Type.Missing);
        //        app.Quit();

        //        //releaseObject(xlWorkSheet);
        //        //releaseObject(xlWorkBook);
        //        //releaseObject(xlApp);
        //    //}
        //}

    }
}

