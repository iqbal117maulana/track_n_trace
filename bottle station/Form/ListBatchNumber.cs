﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace Bottle_Station
{
    public partial class ListBatchNumber : Form
    {
        BottleStation bt;
        BottleStationFlubio btf;
        dbaccess database = new dbaccess();
        string parameter;
        DataTable table = new DataTable();
        public string batch;
        Dashboard dsbb;
        string linenumber;
        string frm;
        Control ctrlCap = new Control();

        public ListBatchNumber(Dashboard dsb, string line, string param, string from)
        {
            parameter = param;
            linenumber = line;
            InitializeComponent();
            refresh();
            SetDataGridListBatch("0", "");
            dsbb = dsb;
            frm = from;
        }

        public void SetDataGridListBatch(string status, string banyak)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new Action<string, string>(SetDataGridListBatch), new object[] { status, banyak });
                return;
            }

            List<string> field = new List<string>();
            field.Add("a.batchNumber 'Batch Number', b.model_name 'Product', a.packagingQty 'Qty', COUNT(c.batchNumber) as 'Actual Qty', a.startdate 'Start Date', a.enddate 'End Date'");

            List<string[]> dss = database.selectList(field, "[packaging_order] a inner join [product_model] b on a.productModelId = b.product_model left outer join (select * from Vaccine where isReject = '0') as c on c.batchNumber = a.batchNumber ", "a.status = 1 and linepackagingid = '" + linenumber + "' and a.productModelId IN " + parameter + " group by a.batchNumber, b.model_name, a.packagingQty, a.startdate, a.enddate, a.packagingOrderNumber order by a.packagingOrderNumber desc");
            if (database.num_rows > 0)
            {
                table = database.dataHeader;
                table.Columns.Add("No").SetOrdinal(0);
                int i = 0;
                foreach (string[] Row in dss)
                {
                    i++;
                    DataRow row = table.NewRow();
                    row[0] = i;
                    row[1] = Row[0];
                    row[2] = Row[1];
                    row[3] = Row[2];
                    row[4] = Row[3];
                    string a = new DateTime(1970, 1, 1, 0, 0, 0).AddSeconds(long.Parse(Row[4])).ToShortDateString();
                    string b = new DateTime(1970, 1, 1, 0, 0, 0).AddSeconds(long.Parse(Row[5])).ToShortDateString();
                    row[5] = a;
                    row[6] = b;
                    table.Rows.Add(row);
                }

                this.dataGridView1.DataSource = table;
                this.dataGridView1.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                this.dataGridView1.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                this.dataGridView1.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                this.dataGridView1.Columns[3].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                this.dataGridView1.Columns[4].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                this.dataGridView1.Columns[5].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                this.dataGridView1.Columns[6].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            }
            else
            {
                refresh();
            }
        }

        internal void refresh()
        {
            table = new DataTable();
            table.Columns.Add("No");
            table.Columns.Add("Batch Number");
            table.Columns.Add("Product");
            table.Columns.Add("Status");
            table.Columns.Add("Qty");
            table.Columns.Add("Start Date");
            table.Columns.Add("End Date");
            dataGridView1.DataSource = table;
            this.dataGridView1.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridView1.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridView1.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridView1.Columns[3].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridView1.Columns[4].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridView1.Columns[5].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridView1.Columns[6].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
        }

        private void txtBatchNumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            var regex = new Regex(@"[^a-zA-Z0-9\b]");
            if (regex.IsMatch(e.KeyChar.ToString()))
            {
                e.Handled = true;
            }
        }

        private void txtBatchNumber_TextChanged(object sender, EventArgs e)
        {
            (dataGridView1.DataSource as DataTable).DefaultView.RowFilter = string.Format("[Batch Number] LIKE '%{0}%'", txtBatchNumber.Text);
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                if (dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex].Value != null)
                {
                    batch = this.dataGridView1.Rows[e.RowIndex].Cells["Batch Number"].FormattedValue.ToString();
                    //string ds = ctrlCap.getBatchSqlite();

                    if (frm.Equals("dashboard pentabio"))
                    {
                        bt = new BottleStation(dsbb, dsbb.admin, dsbb.linenumber, parameter, ctrlCap);
                        string ds = ctrlCap.getBatchSqlite();
                        if (ds != "")
                        {
                           if(ds == batch)
                           {
                               ctrlCap.SetDataGrid_Send("0", "");
                               ctrlCap.SetDataGridSendgs("0", "");
                           }
                        }
                        int[] cnt = ctrlCap.getCount(batch);
                        bt.lblQtyPass.Text = cnt[0].ToString();
                        bt.lblQtyFail.Text = cnt[1].ToString();
                        bt.lblQuantityReceive.Text = cnt[2].ToString();
                        bt.txtBatchNumber.Text = batch;
                        bt.lblUserId.Text = dsbb.lblUserId.Text;
                        bt.lblRole.Text = dsbb.lblRole.Text;
                        bt.Show();
                        Close();
                        dsbb.Hide();
                    }
                    else if (frm.Equals("dashboard flubio"))
                    {
                        btf = new BottleStationFlubio(dsbb, dsbb.admin, dsbb.linenumber, parameter);
                        btf.txtBatchNumber.Text = batch;
                        btf.lblUserId.Text = dsbb.lblUserId.Text;
                        btf.lblRole.Text = dsbb.lblRole.Text;
                        btf.Show();
                        Close();
                        dsbb.Hide();
                    }
                }
                Close();
            }
        }

    }
}
