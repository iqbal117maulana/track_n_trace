﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Bottle_Station
{
    public partial class Sample : Form
    {
        dbaccess db;
        public Sample(string admin)
        {
            InitializeComponent();
            db = new dbaccess();
            db.adminid = admin;
            db.from = "Bottle Station";

        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Enter)
            {
                sample();
            }
        }

        private bool cekVaccine(string databarcode, int status)
        {
            List<string> field = new List<string>();
            field.Add("*");
            string where = "(capid like '%" + databarcode + "%' OR gsonevialid ='" + databarcode + "' ) and isreject='" + status + "'";
            db.selectList(field, "vaccine", where);
            if (db.num_rows > 0)
                return true;
            return false;
        }

        private void sample()
        {
            if (db.validasi(txtBlisterid.Text, 1))
            {
                if (cekVaccine(txtBlisterid.Text, 0))
                {
                    if (db.sampleData(txtBlisterid.Text))
                    {
                        db.eventname = "Sample =" + txtBlisterid.Text + " success";
                        db.to = txtBlisterid.Text;
                        db.systemlog();
                        txtLog.AppendText(txtBlisterid.Text + " set sample success\n");
                    }
                    else
                    {
                        db.eventname = "Sample =" + txtBlisterid.Text + " failed";
                        db.to = txtBlisterid.Text;
                        db.systemlog();
                        txtLog.AppendText(txtBlisterid.Text + " set sample failed\n");
                    }
                }
                else
                {
                    db.eventname = "Sample =" + txtBlisterid.Text + " success";
                    db.to = txtBlisterid.Text;
                    db.systemlog();
                    Confirm cf = new Confirm("Vaccine ID is empty", "Information", MessageBoxButtons.OK);
                }
            }
            else
            {
                db.eventname = "Sample =" + txtBlisterid.Text + " success";
                db.to = txtBlisterid.Text;
                db.systemlog();
                Confirm cf = new Confirm("Vaccine ID is empty", "Information", MessageBoxButtons.OK);
            }

            txtBlisterid.Text = "";
            txtBlisterid.Focus();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            sample();
            txtBlisterid.Focus();
        }

        private void Sample_Load(object sender, EventArgs e)
        {
            txtBlisterid.Text = "";
            txtBlisterid.Focus();
        }
    }
}
