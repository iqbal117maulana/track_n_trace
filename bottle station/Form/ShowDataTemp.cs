﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Bottle_Station
{
    public partial class ShowDataTemp : Form
    {
        public Control ctrl;
        public ControlFlubio ctrlf;
        public ShowDataTemp(Control ct, ControlFlubio ctf)
        {
            InitializeComponent();
            ctrl = ct;
            ctrlf = ctf;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (ctrlf != null)
            {
                ShowData();
            }
            else
            {
                ShowDataFlubio();
            }
        }

        void ShowData()
        {
            DataTable _myDataTable = new DataTable();
            string column = "";
            List<string> data;
            if (ctrl.DataTempCapid.Count > ctrl.DataTempGSone.Count)
            {
                column = "CapID";
                data = ctrl.DataTempCapid;
            }
            else
            {
                column = "GS1 ID";
                data = ctrl.DataTempGSone;
            }

            // create columns
            _myDataTable.Columns.Add("No");
            _myDataTable.Columns.Add(column);

            for (int j = 0; j < data.Count(); j++)
            {
                // create a DataRow using .NewRow()
                DataRow row = _myDataTable.NewRow();
                int i = j + 1;
                row[0] = "" + i;
                row[1] = data[j];
                // iterate over all columns to fill the row
                //for (int i = 0; i < ele; i++)
                //{
                //    row[i] = data[i, j];
                //}

                // add the current row to the DataTable
                _myDataTable.Rows.Add(row);
            }
            dgvReceive.DataSource = _myDataTable;
            lblTotal.Text = "" + data.Count;
        }

        void ShowDataFlubio()
        {
            DataTable _myDataTable = new DataTable();
            string column = "";
            List<string> data;
            if (ctrlf.DataTempCapid.Count > ctrlf.DataTempGSone.Count)
            {
                column = "CapID";
                data = ctrlf.DataTempCapid;
            }
            else
            {
                column = "GS1 ID";
                data = ctrlf.DataTempGSone;
            }

            // create columns
            _myDataTable.Columns.Add("No");
            _myDataTable.Columns.Add(column);

            for (int j = 0; j < data.Count(); j++)
            {
                // create a DataRow using .NewRow()
                DataRow row = _myDataTable.NewRow();
                int i = j + 1;
                row[0] = "" + i;
                row[1] = data[j];
                // iterate over all columns to fill the row
                //for (int i = 0; i < ele; i++)
                //{
                //    row[i] = data[i, j];
                //}

                // add the current row to the DataTable
                _myDataTable.Rows.Add(row);
            }
            dgvReceive.DataSource = _myDataTable;
            lblTotal.Text = "" + data.Count;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (ctrlf != null)
            {
                ctrl.cleanDataTemp();
                ShowData();
            }
            else
            {
                ctrlf.cleanDataTemp();
                ShowDataFlubio();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
