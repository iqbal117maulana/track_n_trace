﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace Bottle_Station
{
    public partial class config : Form
    {
        BottleStation bot;
        BottleStationFlubio botf;
        int status;
        string adminid;
        dbaccess db;
        public config(BottleStation bt, BottleStationFlubio btf, int stat, string admin)
        {
            InitializeComponent();
            bot = bt;
            getConfig();
            if (stat == 1)
            {
                db = new dbaccess();
                db.from = "Bottle Station";
                status = stat;
                adminid = admin;
            }
        }

        void getConfig()
        {
            sqlitecs sql = new sqlitecs();
            try
            {
                txtIpCamera.Text = sql.config["ipCamera"];
                txtPortCamera.Text = sql.config["portCamera"];
                txtIpPrinter.Text = sql.config["ipprinter"];
                txtPortPrinter.Text = sql.config["portprinter"];
                txtPortServer.Text = sql.config["portserver"];
                txtIpServer.Text = sql.config["ipServer"];
                txtIpDB.Text = sql.config["ipDB"];
                txtPortDb.Text = sql.config["portDB"];
                txtTimeoutDb.Text = sql.config["timeout"];
                txtIpMarker.Text = sql.config["ipmarker"];
                txtPortMarker.Text = sql.config["portmarker"];
                txtIpCamera2.Text = sql.config["ipcamera2"];
                txtPortCamera2.Text = sql.config["portcamera2"];
                txtAudit.Text = sql.config["audit"];
                txtNamaDB.Text = sql.config["namaDB"];
                txtSendPrinter.Text = sql.config["qtysend"];
                txtGenerate.Text = sql.config["qtygenerate"];
                txtDelay.Text = sql.config["delay"];
                txtAgain.Text = sql.config["again"];
                txtSecond.Text = sql.config["second"];
                txtIP_PLC.Text = sql.config["IpPlc"];
                txtPort_PLC.Text = sql.config["PortPlc"];
                if (sql.config["debug"].Equals("1"))
                {
                    chkDebug.Checked = true;
                }
                else
                {
                    chkDebug.Checked = false;
                }

                if (sql.config["buffer"].Equals("1"))
                {
                    chkBuffer.Checked = true;
                }
                else
                {
                    chkBuffer.Checked = false;
                }
            }
            catch (KeyNotFoundException k)
            {
                sql.simpanLog("Data Tidak Ditemukan : " + k.ToString());
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            simpan();
        }

        bool cekBesarGenerate(string generate, string send)
        {
            int generatebalik = int.Parse(generate);
            int sendbalik = int.Parse(send);
            if (generatebalik < sendbalik)
                return true;
            return false;
        }

        private void simpan()
        {
          //  if (!bot.start)
            {
                Confirm cf = new Confirm("Are you sure? ", "Confirmation");
                bool hasilDialog = cf.conf();
                if (cekKosong())
                {
                    if (hasilDialog)
                    {
                        if (validate())
                        {
                            if (cekBesarGenerate(txtSendPrinter.Text, txtGenerate.Text))
                            {
                                sqlitecs sql = new sqlitecs();
                                Dictionary<string, string> field = new Dictionary<string, string>();
                                field.Add("ipCamera", txtIpCamera.Text);
                                field.Add("portCamera", "" + int.Parse(txtPortCamera.Text));
                                field.Add("ipPrinterCap", txtIpPrinter.Text);
                                field.Add("portPrinterCap", "" + int.Parse(txtPortPrinter.Text));
                                field.Add("ipPrinterMarker", txtIpMarker.Text);
                                field.Add("portPrinterMarker", "" + int.Parse(txtPortMarker.Text));
                                field.Add("portserver", "" + int.Parse(txtPortServer.Text));
                                field.Add("ipServer", txtIpServer.Text);
                                field.Add("ipDB", txtIpDB.Text);
                                field.Add("portDB", "" + int.Parse(txtPortDb.Text));
                                field.Add("timeout", txtTimeoutDb.Text);
                                field.Add("ipcamera2", txtIpCamera2.Text);
                                field.Add("portcamera2", "" + int.Parse(txtPortCamera2.Text));
                                field.Add("audit", txtAudit.Text);
                                field.Add("namadb", txtNamaDB.Text);
                                field.Add("qtysend", txtSendPrinter.Text);
                                field.Add("qtygenerate", txtGenerate.Text);
                                field.Add("delay", txtDelay.Text);
                                field.Add("again", txtAgain.Text);
                                field.Add("second", txtSecond.Text);
                                field.Add("IpPlc", txtIP_PLC.Text);
                                field.Add("PortPlc", txtPort_PLC.Text);
                                if (chkDebug.Checked)
                                    field.Add("debug", "1");
                                else
                                    field.Add("debug", "0");


                                if (chkBuffer.Checked)
                                    field.Add("buffer", "1");
                                else
                                    field.Add("buffer", "0");

                                sql.update(field, "config", "");
                                new Confirm("Configuration is successfully saved,\nRestart application.", "Information", MessageBoxButtons.OK);
                                this.Dispose();
                                if (status == 1)
                                {
                                    db.eventname = "Set Config susccess";
                                    db.systemlog();
                                }
                                if (bot != null)
                                    bot.refresh();
                            }
                            else
                            {
                                Confirm cf2 = new Confirm("Qty generate must be more than Qty send", "Information", MessageBoxButtons.OK);
                            }
                        }
                        else
                        {
                            Confirm cf3 = new Confirm("Configuration wrong format", "Information", MessageBoxButtons.OK);
                        }
                    }
                }
                else
                {
                    Confirm cf4 = new Confirm("Configuration can not be empty", "Information", MessageBoxButtons.OK);
                }
            }
            //else
            //{
           //     Confirm cf5 = new Confirm("Printer started ", "Information", MessageBoxButtons.OK);
           // }
        }

        private bool cekKosong()
        {
            if (!cek(txtIpServer.Text))
            {
                return false;
            }
            else if (!cek(txtPortServer.Text))
            {
                return false;
            }
            else if (!cek(txtIpPrinter.Text))
            {
                return false;
            }
            else if (!cek(txtPortPrinter.Text))
            {
                return false;
            }
            else if (!cek(txtIpMarker.Text))
            {
                return false;
            }
            else if (!cek(txtPortMarker.Text))
            {
                return false;
            }
            else if (!cek(txtIpCamera2.Text))
            {
                return false;
            }
            else if (!cek(txtPortCamera2.Text))
            {
                return false;
            }
            else if (!cek(txtAudit.Text))
            {
                return false;
            }
            else if (!cek(txtIpCamera.Text))
            {
                return false;
            }
            else if (!cek(txtPortCamera.Text))
            {
                return false;
            }
            else if (!cek(txtIpDB.Text))
            {
                return false;
            }
            else if (!cek(txtPortDb.Text))
            {
                return false;
            }
            else if (!cek(txtTimeoutDb.Text))
            {
                return false;
            }
            else if (!cek(txtNamaDB.Text))
            {
                return false;
            }
            else if (!cek(txtDelay.Text))
            {
                return false;
            }
            return true;
        }

        private bool cek(string p)
        {
            if (p.Length > 0) return true; else return false;
        }

        private bool validate()
        {
            string kesalahan = "Error Input from : ";
            bool kembali = true;
            if (!validasi(txtIpCamera.Text, 0))
            {
                kesalahan = kesalahan + txtIpCamera.Text+"\n";
                kembali = false;
            }

            if (!validasi(txtIpCamera2.Text, 0))
            {
                kesalahan = kesalahan + txtIpCamera2.Text + "\n";
                kembali = false;
            }

            if (!validasi(txtIpDB.Text, 0))
            {
                kesalahan = kesalahan + txtIpCamera2.Text + "\n";
                kembali = false;
            }

            if (!validasi(txtIpDB.Text, 0))
            {
                kesalahan = kesalahan + txtIpDB.Text + "\n";
                kembali = false;
            }

            if (!validasi(txtIpMarker.Text, 0))
            {
                kesalahan = kesalahan + txtIpMarker.Text + "\n";
                kembali = false;
            }

            if (!validasi(txtIpPrinter.Text, 0))
            {
                kesalahan = kesalahan + txtIpPrinter.Text + "\n";
                kembali = false;
            }

            if (!validasi(txtIpServer.Text, 0))
            {
                kesalahan = kesalahan + txtIpServer.Text + "\n";
                kembali = false;
            }

            if (!validasi(txtPortCamera.Text, 3))
            {
                kesalahan = kesalahan + txtPortCamera.Text + "\n";
                kembali = false;
            }

            if (!validasi(txtPortCamera2.Text, 3))
            {
                kesalahan = kesalahan + txtPortCamera2.Text + "\n";
                kembali = false;
            }

            if (!validasi(txtPortDb.Text, 3))
            {
                kesalahan = kesalahan + txtPortDb.Text + "\n";
                kembali = false;
            }

            if (!validasi(txtPortMarker.Text, 3))
            {
                kesalahan = kesalahan + txtPortMarker.Text + "\n";
                kembali = false;
            }

            if (!validasi(txtPortPrinter.Text, 3))
            {
                kesalahan = kesalahan + txtPortPrinter.Text + "\n";
                kembali = false;
            }

            if (!validasi(txtPortServer.Text, 3))
            {
                kesalahan = kesalahan + txtPortServer.Text + "\n";
                kembali = false;
            }

            if (!kembali)
                new Confirm(kesalahan);
            return kembali;
        }
        string valid;

        bool cekIP(string data)
        {
            if (data.Length < 3 & data.Length > 0)
                return false;
            int errorCounter = Regex.Matches(data, @"[a-zA-Z]").Count;
            if (errorCounter == 0)
            {
                string[] dapat = data.Split('.');
                if (dapat.Length != 4)
                {
                    valid = "Error : IP must 4 point";
                    return false;
                }
                if (dapat[3].Equals("0"))
                {
                    valid = "Error : the fourth digit of ip cannot be 0";
                    return false;
                }

                bool kosong = false;
                if (dapat[0].Equals("000"))
                    kosong = true;
                for (int i = 0; i < dapat.Length; i++)
                {
                    //if (dapat[i] == "" || dapat[i].Length <= 0)
                    //{
                    //    new Confirm("Error : IP must 4 point");
                    //    return false;
                    //}
                    if (dapat[i][0].Equals("0"))
                        return false;
                    if (dapat[i].Equals("00"))
                    {
                        valid = "Error : IP cannot fill 00";
                        return false;
                    }

                    bool rege = Regex.Match(dapat[i], @"^[0-9]+$").Success;
                    if (!rege)
                    {
                        valid = "Error : IP cannot contain alphabeth and symbols " + data;
                        return false;
                    }
                    if (dapat[i].Length == 0)
                    {
                        valid = "Error : Cannot empty";
                        return false;
                    }
                    int temp = int.Parse(dapat[i]);
                    if (temp >= 255)
                    {
                        valid = "Error : IP must be less then 255";
                        return false;
                    }
                    if (temp == 0 && kosong)
                    {
                        valid = "Error : First digit of ip cannot be 0";
                        return false;
                    }
                    if (dapat[i].Equals("000") && !kosong)
                    {
                        valid = "Error : IP cannot fill 000";
                        return false;
                    }
                    if (dapat[i][0].Equals('0') && temp != 0 && dapat[i].Length > 1)
                    {
                        valid = "Error : Ip cannot fill '0'";
                        return false;
                    }
                }
                return true;
            }
            else
            {
                valid = "Error : IP cannot contain alphabeth " + errorCounter;
                return false;
            }
            return true;
        }

        public bool validasi(string data, int val)
        {
            //text ip val = 0
            if (val == 0)
                return cekIP(data);
            else if (val == 1)
            {
                //text tidak kosong val =1
                if (data.Length == 0)
                {
                    valid = "Data empty";
                    return false;
                }
            }
            else if (val == 2)
            {
                //text tidak ada angka
                int errorCounter = Regex.Matches(data, @"[a-zA-Z]").Count;
                if (errorCounter > 0)
                {
                    valid = "Data cannot contain numeric";
                    return false;
                }
            }
            else if (val == 3)
            {
                //text tidak ada angka

                if (data.Length > 6)
                    return false;
                bool rege = Regex.Match(data, @"^[0-9]+$").Success;
                if (!rege)
                {
                    valid = "Data cannot contain numeric";
                    return false;
                }
            }
            return true;
        }
       
        private void config_Load(object sender, EventArgs e)
        {

        }

        private void txtAudit_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtNamaDB_TextChanged(object sender, EventArgs e)
        {

        }

        private void groupBox3_Enter(object sender, EventArgs e)
        {

        }

        private void label45_Click(object sender, EventArgs e)
        {

        }
    }
}
