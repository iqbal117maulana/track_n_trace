﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Threading;

namespace Bottle_Station
{
    public partial class printf : Form
    {
        tcp_testingFlubio tcp;
        Mod_TCP2Flubio tcp1;
        tcp_testingFlubio tcpgs;
        List<string> dataCap = new List<string>();
        List<string> dataVial = new List<string>();
        sqlitecs sql;

        public int hitung = 0;
        public int CountBuffer = -1;
        public string statusLeibinger;
        private Thread serverThread;
        private Thread serverThread2;
        public bool isConnectGS;


        public printf()
        {

            InitializeComponent();
            tcp = new tcp_testingFlubio();
            tcp1 = new Mod_TCP2Flubio();
            // tcp.pr = this;
        }

        public printf(List<string> data, List<string> dataVi)
        {
            dataCap = data;
            //kirimData();
            dataVial = dataVi;
        }

        public bool cekKoneksi()
        {
            sql = new sqlitecs();
            tcp.IPAddress = sql.config["ipprinter"];
            tcp.Port = sql.config["portprinter"];
            tcp.Timeout = sql.config["timeout"];
            bool res = tcp.Connect();
            return res;
        }

        public bool cekKoneksigs()
        {
            tcpgs = new tcp_testingFlubio();
            sql = new sqlitecs();
            string ip = sql.config["ipmarker"];
            string port = sql.config["portmarker"];
            bool res = tcpgs.Connect(ip, port, sql.config["timeout"]);
            isConnectGS = res;
            return res;

        }

        public void closeCap()
        {
            try
            {
                tcp1.CloseConnection();
            }
            catch (Exception ex)
            {
            }
        }

        public void closeGs()
        {
            try
            {
                tcpgs.dc();
            }
            catch (Exception ex)
            {
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            tcp1.IPAddress = txtIp.Text;
            tcp1.port = txtPort.Text;
            tcp1.timeout = "1";
            tcp1.OpenConnection();
            tcp1.startCheckStatus(null);
            tcp1.startListener();
            //   tcp.Connect(txtIp.Text, txtPort.Text,"10");
            //tcp.updatestatus("Connected");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            tcp.send("^0!PO");
            tcp.updatestatus("Power ON");
        }

        private void button4_Click(object sender, EventArgs e)
        {
            tcp.send("^0!NO");
            tcp.updatestatus("Noozle ON");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            tcp.send("^0!PF");
            tcp.updatestatus("Power OFF");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            tcp.send("^0!NC");
            tcp.updatestatus("Nozzle Close");
        }

        private void button6_Click(object sender, EventArgs e)
        {
            tcp1.send(txtExternal.Text);
            //tcp.send("^0=ET " + txtExternal.Text);
            //tcp.updatestatus("EXTERNAL TEXT : "+txtExternal.Text);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            tcp.send("^0!GO");
            hitung = 0;
            tcp.updatestatus("PRINT...");
        }

        private void button8_Click(object sender, EventArgs e)
        {
            tcp.send("^0=MR" + hitung + " " + txtMailing.Text);
            //tcp.updatestatus("MAILING RECORD " + hitung + " " + txtMailing.Text);
            hitung++;
        }

        private void label3_Click(object sender, EventArgs e)
        {
            tcp.send("^0?SM");
            tcp.updatestatus("Mail Status");
        }

        private void button9_Click(object sender, EventArgs e)
        {
            tcp = new tcp_testingFlubio();
            if (tcp.Connect(txtIp.Text, txtPort.Text, "2"))
            {
                string data = tcp.sendBack("^0?RS");
                txtStatus.AppendText(data + "\n");
            }
            else
                new Confirm("Gak konek");
        }

        public void kirimData(List<string> data)
        {
            string disimpan = "";
            for (int i = 0; i < data.Count; i++)
            {
                string Send;
                if (sql.config["debug"].Equals("0"))
                {
                    Send = "^0=MR" + hitung + " " + data[i];
                }
                else
                {
                    Send = data[i] + "|";
                }
                tcp.send(Send);
                disimpan = disimpan + " \n| " + Send;
                hitung++;
            }
            int akhir = hitung - 1;
            disimpan = disimpan + " \n| " + "^0=CM " + akhir;
            if (sql.config["debug"].Equals("0"))
            {
                tcp.send("^0=CM " + akhir);
            }
            simpanLog(disimpan);
            tcp.dc();
        }

        public int kirimData(List<string[]> data, int count)
        {
            string disimpan = "";
            CountBuffer = count;
            for (int i = 0; i < data.Count; i++)
            {
                string[] datagsone = data[i];

                string simpan;
                if (sql.config["debug"].Equals("0"))
                {
                    simpan = "BUFFERDATA " + CountBuffer + " \"" + datagsone[0] + "\" \"" + datagsone[1] + "\" \"" + datagsone[2] + "\" \"" + datagsone[3] + "\" " + "\"" + datagsone[5] + "\"";
                }
                else
                {
                    simpan = datagsone[4] + "|";
                }
                tcpgs.send(simpan);
                disimpan = disimpan + " \n| " + simpan;
                hitung++;
                CountBuffer++;
                if (CountBuffer >= 10000)
                    CountBuffer = -1;
            }
            simpanLog(disimpan);
            tcpgs.dc();
            return CountBuffer;
        }

        internal void clearBuffer()
        {
            tcp.send("^0!RC");
            simpanLog("^0!RC");
            //tcp.dc();
        }

        internal void clearBuffergs()
        {
            simpanLog("BUFFERCLEAR");
            tcp.send("BUFFERCLEAR");
            // tcp.dc();
        }

        public void simpanLog(String line)
        {
            DateTime now = DateTime.Now;
            string tahun = now.ToString("yyyy");
            string bulan = now.ToString("MM");
            string hari = now.ToString("dd");
            string dates = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            line = dates + "\t" + line;
            // cek file 
            bool cekfile = false;
            try
            {
                while (!cekfile)
                {
                    if (Directory.Exists("eventlog\\Printer"))
                    {
                        if (Directory.Exists("eventlog\\Printer" + @"\" + tahun))
                        {
                            if (Directory.Exists("eventlog\\Printer" + @"\" + tahun + @"\" + bulan))
                            {

                                using (StreamWriter outputFile = new StreamWriter("eventlog\\Printer" + @"\" + tahun + @"\" + bulan + @"\" + hari + ".txt", true))
                                {
                                    outputFile.WriteLine(line);
                                }
                                cekfile = true;
                            }
                            else
                            {

                                Directory.CreateDirectory("eventlog\\Printer" + @"\" + tahun + @"\" + bulan);
                            }
                        }
                        else
                        {
                            Directory.CreateDirectory("eventlog\\Printer" + @"\" + tahun);
                        }
                    }
                    else
                    {

                        Directory.CreateDirectory("eventlog\\Printer");
                    }
                }
            }
            catch (Exception ex)
            {
                // simpanLog(ex.Message);
            }
        }

        internal void startCheckStatus(ControlFlubio control)
        {
            tcp.ctrl = control;
            serverThread2 = new Thread(new ThreadStart(tcp.MulaiCheker));
            serverThread2.Start();
        }

        internal void startListener()
        {
            tcp.conneect = true;
            serverThread = new Thread(new ThreadStart(tcp.MulaiServerCapid));
            serverThread.Start();
        }

        internal void stopListener()
        {
            tcp.conneect = false;
        }

        internal void startListenerGS()
        {
            serverThread = new Thread(new ThreadStart(tcp.MulaiServerGS1ID));
            serverThread.Start();
        }

        //## used

        //private Thread GsprintThread;
        //        internal void send(string data)
        //        {
        //            simpanLog(data);
        //            tcp.send(data);
        //            string dataReceive = tcp.listener();
        //            tcp.dc();
        //        }




        //## Not used
        //        public void kirimData(string[] data, string[] dataVi)
        //        {
        //            for (int i = 0; i < data.Length; i++)
        //            {
        //                tcp.send("" + hitung + " " + data[i]);
        //                hitung++;
        //            }

        //            for (int i = 0; i < dataVi.Length; i++)
        //            {
        //                tcp.send("" + hitung + " " + dataVi[i]);
        //                hitung++;
        //            }
        //            tcp.dc();
        //        }

        //public void kirimData(string[] data)
        //{
        //    for (int i = 0; i < data.Length; i++)
        //    {
        //        tcp.send("^0=MR " + hitung + " " + data[i]);
        //        //tcp.send(data[i]);
        //        Console.WriteLine("" + hitung + " " + data[i]);
        //        hitung++;
        //    }
        //    tcp.dc();
        //}

        //public void kirimDatags(string[] data)
        //{
        //    for (int i = 0; i < data.Length; i++)
        //    {
        //        // tcp.send("^0=MR " + hitung + " " + data[i]);
        //        tcp.send(data[i]);
        //        Console.WriteLine("" + hitung + " " + data[i]);
        //        hitung++;
        //    }
        //    tcp.dc();
        //}


        //public void kirimData(List<string> data, List<string> dataVi)
        //{
        //    for (int i = 0; i < data.Count; i++)
        //    {
        //        tcp.send("" + hitung + " " + data[i]);
        //        hitung++;
        //    }

        //    for (int i = 0; i < dataVi.Count; i++)
        //    {
        //        tcp.send("" + hitung + " " + dataVi[i]);
        //        hitung++;
        //    }
        //    tcp.dc();
        //}

        //private void kirimDataVial()
        //{
        //    sql = new sqlitecs();
        //    tcp = new tcp_testing();
        //    tcp.Connect(sql.config["ipLibringer"], sql.config["portLibringer"], sql.config["timeout"]);
        //    for (int i = 0; i < dataVial.Count; i++)
        //    {
        //        tcp.send("^0=MR" + hitung + " " + dataVial[i]);
        //        hitung++;
        //    }
        //    tcp.send("^0=MR" + hitung + " " + "End Of");
        //}

        //public void kirimData(string data)
        //{
        //    tcp = new tcp_testing();
        //    tcp.Connect("192.168.0.2", "3000", "30");
        //    tcp.send("^0=MR" + data);
        //}
        //public string cekprinterStatus()
        //{
        //    return tcp.sendBack("^0?RS");
        //}
        //public bool cekStatusPrinter()
        //{
        //    string data = tcp.sendBack("^0?RS");
        //    bool balikan = false;
        //    bool balikan1 = false;
        //    string teksbalikan = "";
        //    if (data.Length > 0)
        //    {
        //        char nozzle = data[5];
        //        switch (nozzle)
        //        {
        //            case '1':
        //                statusLeibinger = "Nozzle Opens";
        //                break;
        //            case '2':
        //                statusLeibinger = "Nozzle Is Open";
        //                balikan = true;
        //                break;
        //            case '3':
        //                statusLeibinger = "Nozzle Closes";
        //                break;
        //            case '4':
        //                statusLeibinger = "Nozzle Is Closed";
        //                break;
        //            case '5':
        //                statusLeibinger = "Nozzle In Between";
        //                break;
        //            default:
        //                statusLeibinger = "Nozzle Is Invalid";
        //                break;
        //        }

        //        char state = data[7];
        //        switch (state)
        //        {
        //            case '1':
        //                statusLeibinger = statusLeibinger + ", State : Standby";
        //                break;
        //            case '2':
        //                statusLeibinger = statusLeibinger + ", State : Initialization";
        //                break;
        //            case '3':
        //                statusLeibinger = statusLeibinger + ", State : Interval";
        //                break;
        //            case '4':
        //                statusLeibinger = statusLeibinger + ", State : Ready For Action";
        //                break;
        //            case '5':
        //                statusLeibinger = statusLeibinger + ", State : Ready For PrintStart";

        //                break;
        //            default:
        //                statusLeibinger = statusLeibinger + ", State : Printing";
        //                balikan1 = true;
        //                break;
        //        }
        //        if (balikan && balikan1)
        //            return true;
        //        else
        //            return false;
        //    }
        //    else
        //    {
        //        return false;
        //    }
        //}

        //internal bool cekStatusPrinter2()
        //{
        //    if (cekKoneksi())
        //    {
        //        if (cekKoneksigs())
        //        {
        //            return true;
        //        }
        //    }
        //    return false;

        //}

    }

}
