﻿namespace Bottle_Station
{
    partial class BottleStation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BottleStation));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblQtyGs = new System.Windows.Forms.Label();
            this.dgvGsone = new System.Windows.Forms.DataGridView();
            this.dgvCapId = new System.Windows.Forms.DataGridView();
            this.label7 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.lblQtyCapid = new System.Windows.Forms.Label();
            this.dgvReceive = new System.Windows.Forms.DataGridView();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lblQtyNow = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.lblQtyFail = new System.Windows.Forms.Label();
            this.lblQtyPass = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.lblQuantityReceive = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblPrinter = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.lblCamera2 = new System.Windows.Forms.Label();
            this.lblQuan2 = new System.Windows.Forms.Label();
            this.lblCamera1 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.lblAdmin = new System.Windows.Forms.Label();
            this.lblMarker = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.lblProductName = new System.Windows.Forms.Label();
            this.lblLine = new System.Windows.Forms.Label();
            this.lblBatch = new System.Windows.Forms.Label();
            this.lblRole = new System.Windows.Forms.Label();
            this.lblUserId = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.lblDate = new System.Windows.Forms.Label();
            this.lblTime = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.txtBatchNumber = new System.Windows.Forms.TextBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.lblDb = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.offDb = new System.Windows.Forms.PictureBox();
            this.onDb = new System.Windows.Forms.PictureBox();
            this.offMarker = new System.Windows.Forms.PictureBox();
            this.offPrinter = new System.Windows.Forms.PictureBox();
            this.offCamera2 = new System.Windows.Forms.PictureBox();
            this.offCamera1 = new System.Windows.Forms.PictureBox();
            this.onMarker = new System.Windows.Forms.PictureBox();
            this.onPrinter = new System.Windows.Forms.PictureBox();
            this.onCamera2 = new System.Windows.Forms.PictureBox();
            this.onCamera1 = new System.Windows.Forms.PictureBox();
            this.btnCheckIndicator = new System.Windows.Forms.Button();
            this.btnLog = new System.Windows.Forms.Button();
            this.btnStartGs = new System.Windows.Forms.Button();
            this.btnSample = new System.Windows.Forms.Button();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBoxCamera = new System.Windows.Forms.PictureBox();
            this.btnDeco = new System.Windows.Forms.Button();
            this.btnAudit = new System.Windows.Forms.Button();
            this.btnConfig = new System.Windows.Forms.Button();
            this.btnLogout = new System.Windows.Forms.Button();
            this.btnStart = new System.Windows.Forms.Button();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvGsone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCapId)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvReceive)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.offDb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.onDb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.offMarker)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.offPrinter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.offCamera2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.offCamera1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.onMarker)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.onPrinter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.onCamera2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.onCamera1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCamera)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox1.Controls.Add(this.dgvGsone);
            this.groupBox1.Controls.Add(this.dgvCapId);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(12, 160);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(277, 649);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Ready";
            // 
            // lblQtyGs
            // 
            this.lblQtyGs.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblQtyGs.AutoSize = true;
            this.lblQtyGs.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQtyGs.Location = new System.Drawing.Point(779, 145);
            this.lblQtyGs.Name = "lblQtyGs";
            this.lblQtyGs.Size = new System.Drawing.Size(18, 20);
            this.lblQtyGs.TabIndex = 97;
            this.lblQtyGs.Text = "0";
            this.lblQtyGs.Visible = false;
            // 
            // dgvGsone
            // 
            this.dgvGsone.AllowUserToAddRows = false;
            this.dgvGsone.AllowUserToDeleteRows = false;
            this.dgvGsone.AllowUserToResizeColumns = false;
            this.dgvGsone.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dgvGsone.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvGsone.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvGsone.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvGsone.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvGsone.Location = new System.Drawing.Point(5, 284);
            this.dgvGsone.Name = "dgvGsone";
            this.dgvGsone.ReadOnly = true;
            this.dgvGsone.RowHeadersVisible = false;
            this.dgvGsone.Size = new System.Drawing.Size(266, 357);
            this.dgvGsone.TabIndex = 10;
            this.dgvGsone.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dgvGsone_RowsAdded);
            // 
            // dgvCapId
            // 
            this.dgvCapId.AllowUserToAddRows = false;
            this.dgvCapId.AllowUserToDeleteRows = false;
            this.dgvCapId.AllowUserToResizeColumns = false;
            this.dgvCapId.AllowUserToResizeRows = false;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dgvCapId.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvCapId.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvCapId.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvCapId.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCapId.Location = new System.Drawing.Point(5, 19);
            this.dgvCapId.Name = "dgvCapId";
            this.dgvCapId.ReadOnly = true;
            this.dgvCapId.RowHeadersVisible = false;
            this.dgvCapId.Size = new System.Drawing.Size(266, 259);
            this.dgvCapId.TabIndex = 9;
            this.dgvCapId.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dgvCapId_RowsAdded);
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(695, 145);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(78, 20);
            this.label7.TabIndex = 96;
            this.label7.Text = "Qty GS1 :";
            this.label7.Visible = false;
            // 
            // label18
            // 
            this.label18.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(556, 145);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(91, 20);
            this.label18.TabIndex = 94;
            this.label18.Text = "Qty CapID :";
            this.label18.Visible = false;
            // 
            // lblQtyCapid
            // 
            this.lblQtyCapid.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblQtyCapid.AutoSize = true;
            this.lblQtyCapid.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQtyCapid.Location = new System.Drawing.Point(653, 145);
            this.lblQtyCapid.Name = "lblQtyCapid";
            this.lblQtyCapid.Size = new System.Drawing.Size(18, 20);
            this.lblQtyCapid.TabIndex = 95;
            this.lblQtyCapid.Text = "0";
            this.lblQtyCapid.Visible = false;
            // 
            // dgvReceive
            // 
            this.dgvReceive.AllowUserToAddRows = false;
            this.dgvReceive.AllowUserToDeleteRows = false;
            this.dgvReceive.AllowUserToResizeColumns = false;
            this.dgvReceive.AllowUserToResizeRows = false;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dgvReceive.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvReceive.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvReceive.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvReceive.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvReceive.Location = new System.Drawing.Point(6, 19);
            this.dgvReceive.Name = "dgvReceive";
            this.dgvReceive.ReadOnly = true;
            this.dgvReceive.RowHeadersVisible = false;
            this.dgvReceive.Size = new System.Drawing.Size(538, 599);
            this.dgvReceive.TabIndex = 7;
            this.dgvReceive.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dgvReceive_RowsAdded);
            this.dgvReceive.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvReceive_CellClick);
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.lblQtyNow);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.lblQtyFail);
            this.groupBox2.Controls.Add(this.lblQtyPass);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.lblQuantityReceive);
            this.groupBox2.Controls.Add(this.dgvReceive);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(296, 160);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(550, 649);
            this.groupBox2.TabIndex = 8;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Pass";
            // 
            // lblQtyNow
            // 
            this.lblQtyNow.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblQtyNow.AutoSize = true;
            this.lblQtyNow.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQtyNow.Location = new System.Drawing.Point(495, 624);
            this.lblQtyNow.Name = "lblQtyNow";
            this.lblQtyNow.Size = new System.Drawing.Size(18, 20);
            this.lblQtyNow.TabIndex = 101;
            this.lblQtyNow.Text = "0";
            // 
            // label15
            // 
            this.label15.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(416, 624);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(76, 20);
            this.label15.TabIndex = 100;
            this.label15.Text = "Qty Now :";
            // 
            // lblQtyFail
            // 
            this.lblQtyFail.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblQtyFail.AutoSize = true;
            this.lblQtyFail.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQtyFail.Location = new System.Drawing.Point(198, 624);
            this.lblQtyFail.Name = "lblQtyFail";
            this.lblQtyFail.Size = new System.Drawing.Size(18, 20);
            this.lblQtyFail.TabIndex = 97;
            this.lblQtyFail.Text = "0";
            // 
            // lblQtyPass
            // 
            this.lblQtyPass.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblQtyPass.AutoSize = true;
            this.lblQtyPass.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQtyPass.Location = new System.Drawing.Point(64, 624);
            this.lblQtyPass.Name = "lblQtyPass";
            this.lblQtyPass.Size = new System.Drawing.Size(18, 20);
            this.lblQtyPass.TabIndex = 99;
            this.lblQtyPass.Text = "0";
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(143, 624);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(42, 20);
            this.label10.TabIndex = 96;
            this.label10.Text = "Fail :";
            // 
            // label13
            // 
            this.label13.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(6, 624);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(52, 20);
            this.label13.TabIndex = 98;
            this.label13.Text = "Pass :";
            // 
            // lblQuantityReceive
            // 
            this.lblQuantityReceive.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblQuantityReceive.AutoSize = true;
            this.lblQuantityReceive.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQuantityReceive.Location = new System.Drawing.Point(333, 624);
            this.lblQuantityReceive.Name = "lblQuantityReceive";
            this.lblQuantityReceive.Size = new System.Drawing.Size(18, 20);
            this.lblQuantityReceive.TabIndex = 97;
            this.lblQuantityReceive.Text = "0";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(267, 624);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 20);
            this.label3.TabIndex = 96;
            this.label3.Text = "All Qty :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(15, 117);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(119, 20);
            this.label1.TabIndex = 38;
            this.label1.Text = "Batch Number :";
            // 
            // lblPrinter
            // 
            this.lblPrinter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblPrinter.AutoSize = true;
            this.lblPrinter.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrinter.Location = new System.Drawing.Point(955, 544);
            this.lblPrinter.Name = "lblPrinter";
            this.lblPrinter.Size = new System.Drawing.Size(50, 18);
            this.lblPrinter.TabIndex = 91;
            this.lblPrinter.Text = "Offline";
            // 
            // label16
            // 
            this.label16.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(889, 566);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(59, 20);
            this.label16.TabIndex = 90;
            this.label16.Text = "Printer ";
            // 
            // lblCamera2
            // 
            this.lblCamera2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCamera2.AutoSize = true;
            this.lblCamera2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCamera2.Location = new System.Drawing.Point(955, 460);
            this.lblCamera2.Name = "lblCamera2";
            this.lblCamera2.Size = new System.Drawing.Size(50, 18);
            this.lblCamera2.TabIndex = 87;
            this.lblCamera2.Text = "Offline";
            // 
            // lblQuan2
            // 
            this.lblQuan2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblQuan2.AutoSize = true;
            this.lblQuan2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQuan2.Location = new System.Drawing.Point(887, 484);
            this.lblQuan2.Name = "lblQuan2";
            this.lblQuan2.Size = new System.Drawing.Size(82, 20);
            this.lblQuan2.TabIndex = 86;
            this.lblQuan2.Text = "Camera 2 ";
            // 
            // lblCamera1
            // 
            this.lblCamera1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCamera1.AutoSize = true;
            this.lblCamera1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCamera1.Location = new System.Drawing.Point(955, 382);
            this.lblCamera1.Name = "lblCamera1";
            this.lblCamera1.Size = new System.Drawing.Size(50, 18);
            this.lblCamera1.TabIndex = 85;
            this.lblCamera1.Text = "Offline";
            // 
            // label17
            // 
            this.label17.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.White;
            this.label17.Location = new System.Drawing.Point(328, 10);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(221, 37);
            this.label17.TabIndex = 94;
            this.label17.Text = "Bottle Station";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(602, 33);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(111, 20);
            this.label2.TabIndex = 96;
            this.label2.Text = "Batch Number";
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(602, 11);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(117, 20);
            this.label8.TabIndex = 97;
            this.label8.Text = "Line Packaging";
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(602, 53);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(110, 20);
            this.label9.TabIndex = 98;
            this.label9.Text = "Product Name";
            // 
            // lblAdmin
            // 
            this.lblAdmin.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblAdmin.AutoSize = true;
            this.lblAdmin.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAdmin.Location = new System.Drawing.Point(967, 333);
            this.lblAdmin.Name = "lblAdmin";
            this.lblAdmin.Size = new System.Drawing.Size(57, 20);
            this.lblAdmin.TabIndex = 102;
            this.lblAdmin.Text = "admin";
            this.lblAdmin.Visible = false;
            this.lblAdmin.Click += new System.EventHandler(this.lblAdmin_Click);
            // 
            // lblMarker
            // 
            this.lblMarker.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMarker.AutoSize = true;
            this.lblMarker.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMarker.Location = new System.Drawing.Point(955, 625);
            this.lblMarker.Name = "lblMarker";
            this.lblMarker.Size = new System.Drawing.Size(50, 18);
            this.lblMarker.TabIndex = 105;
            this.lblMarker.Text = "Offline";
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(887, 654);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(55, 20);
            this.label11.TabIndex = 104;
            this.label11.Text = "Printer";
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(887, 405);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(82, 20);
            this.label5.TabIndex = 84;
            this.label5.Text = "Camera 1 ";
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(729, 33);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(13, 20);
            this.label4.TabIndex = 111;
            this.label4.Text = ":";
            // 
            // label12
            // 
            this.label12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.White;
            this.label12.Location = new System.Drawing.Point(729, 11);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(13, 20);
            this.label12.TabIndex = 112;
            this.label12.Text = ":";
            // 
            // label19
            // 
            this.label19.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.White;
            this.label19.Location = new System.Drawing.Point(729, 53);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(13, 20);
            this.label19.TabIndex = 113;
            this.label19.Text = ":";
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(860, 102);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(150, 40);
            this.button1.TabIndex = 126;
            this.button1.Text = "Refresh";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(148)))), ((int)(((byte)(172)))));
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.lblProductName);
            this.panel1.Controls.Add(this.lblLine);
            this.panel1.Controls.Add(this.lblBatch);
            this.panel1.Controls.Add(this.lblRole);
            this.panel1.Controls.Add(this.lblUserId);
            this.panel1.Controls.Add(this.pictureBox3);
            this.panel1.Controls.Add(this.lblDate);
            this.panel1.Controls.Add(this.lblTime);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.label17);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label19);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1024, 86);
            this.panel1.TabIndex = 127;
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(360, 40);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(151, 37);
            this.label6.TabIndex = 133;
            this.label6.Text = "Pentabio";
            // 
            // lblProductName
            // 
            this.lblProductName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblProductName.AutoSize = true;
            this.lblProductName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProductName.ForeColor = System.Drawing.Color.White;
            this.lblProductName.Location = new System.Drawing.Point(738, 53);
            this.lblProductName.Name = "lblProductName";
            this.lblProductName.Size = new System.Drawing.Size(110, 20);
            this.lblProductName.TabIndex = 132;
            this.lblProductName.Text = "Product Name";
            // 
            // lblLine
            // 
            this.lblLine.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblLine.AutoSize = true;
            this.lblLine.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLine.ForeColor = System.Drawing.Color.White;
            this.lblLine.Location = new System.Drawing.Point(738, 11);
            this.lblLine.Name = "lblLine";
            this.lblLine.Size = new System.Drawing.Size(39, 20);
            this.lblLine.TabIndex = 131;
            this.lblLine.Text = "Line";
            // 
            // lblBatch
            // 
            this.lblBatch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblBatch.AutoSize = true;
            this.lblBatch.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBatch.ForeColor = System.Drawing.Color.White;
            this.lblBatch.Location = new System.Drawing.Point(738, 33);
            this.lblBatch.Name = "lblBatch";
            this.lblBatch.Size = new System.Drawing.Size(111, 20);
            this.lblBatch.TabIndex = 130;
            this.lblBatch.Text = "Batch Number";
            // 
            // lblRole
            // 
            this.lblRole.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblRole.AutoSize = true;
            this.lblRole.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRole.ForeColor = System.Drawing.Color.White;
            this.lblRole.Location = new System.Drawing.Point(931, 44);
            this.lblRole.Name = "lblRole";
            this.lblRole.Size = new System.Drawing.Size(52, 20);
            this.lblRole.TabIndex = 129;
            this.lblRole.Text = "admin";
            // 
            // lblUserId
            // 
            this.lblUserId.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblUserId.AutoSize = true;
            this.lblUserId.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUserId.ForeColor = System.Drawing.Color.White;
            this.lblUserId.Location = new System.Drawing.Point(930, 24);
            this.lblUserId.Name = "lblUserId";
            this.lblUserId.Size = new System.Drawing.Size(57, 20);
            this.lblUserId.TabIndex = 128;
            this.lblUserId.Text = "admin";
            // 
            // pictureBox3
            // 
            this.pictureBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox3.Image = global::Bottle_Station.Properties.Resources.user;
            this.pictureBox3.Location = new System.Drawing.Point(879, 20);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(45, 45);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox3.TabIndex = 116;
            this.pictureBox3.TabStop = false;
            // 
            // lblDate
            // 
            this.lblDate.AutoSize = true;
            this.lblDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDate.ForeColor = System.Drawing.Color.White;
            this.lblDate.Location = new System.Drawing.Point(216, 38);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(33, 15);
            this.lblDate.TabIndex = 115;
            this.lblDate.Text = "Date";
            // 
            // lblTime
            // 
            this.lblTime.AutoSize = true;
            this.lblTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTime.ForeColor = System.Drawing.Color.White;
            this.lblTime.Location = new System.Drawing.Point(216, 20);
            this.lblTime.Name = "lblTime";
            this.lblTime.Size = new System.Drawing.Size(39, 15);
            this.lblTime.TabIndex = 114;
            this.lblTime.Text = "Time";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(47)))), ((int)(((byte)(54)))));
            this.panel2.Controls.Add(this.pictureBox1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(210, 86);
            this.panel2.TabIndex = 0;
            // 
            // pictureBox1
            // 
            this.pictureBox1.ImageLocation = "logo_biofarma.png";
            this.pictureBox1.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.InitialImage")));
            this.pictureBox1.Location = new System.Drawing.Point(29, 4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(150, 75);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 35;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.DoubleClick += new System.EventHandler(this.pictureBox1_DoubleClick);
            this.pictureBox1.MouseLeave += new System.EventHandler(this.pictureBox1_MouseLeave);
            this.pictureBox1.MouseEnter += new System.EventHandler(this.pictureBox1_MouseEnter);
            // 
            // txtBatchNumber
            // 
            this.txtBatchNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBatchNumber.Location = new System.Drawing.Point(140, 114);
            this.txtBatchNumber.Name = "txtBatchNumber";
            this.txtBatchNumber.ReadOnly = true;
            this.txtBatchNumber.Size = new System.Drawing.Size(192, 26);
            this.txtBatchNumber.TabIndex = 129;
            this.txtBatchNumber.Click += new System.EventHandler(this.txtBatchNumber_Click);
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // lblDb
            // 
            this.lblDb.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblDb.AutoSize = true;
            this.lblDb.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDb.Location = new System.Drawing.Point(955, 711);
            this.lblDb.Name = "lblDb";
            this.lblDb.Size = new System.Drawing.Size(50, 18);
            this.lblDb.TabIndex = 140;
            this.lblDb.Text = "Offline";
            // 
            // label20
            // 
            this.label20.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(887, 740);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(79, 20);
            this.label20.TabIndex = 139;
            this.label20.Text = "Database";
            // 
            // offDb
            // 
            this.offDb.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.offDb.Image = global::Bottle_Station.Properties.Resources.off1;
            this.offDb.Location = new System.Drawing.Point(959, 694);
            this.offDb.Name = "offDb";
            this.offDb.Size = new System.Drawing.Size(14, 14);
            this.offDb.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.offDb.TabIndex = 143;
            this.offDb.TabStop = false;
            // 
            // onDb
            // 
            this.onDb.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.onDb.Image = global::Bottle_Station.Properties.Resources.on1;
            this.onDb.Location = new System.Drawing.Point(959, 694);
            this.onDb.Name = "onDb";
            this.onDb.Size = new System.Drawing.Size(14, 14);
            this.onDb.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.onDb.TabIndex = 142;
            this.onDb.TabStop = false;
            this.onDb.Visible = false;
            // 
            // offMarker
            // 
            this.offMarker.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.offMarker.Image = global::Bottle_Station.Properties.Resources.off1;
            this.offMarker.Location = new System.Drawing.Point(959, 608);
            this.offMarker.Name = "offMarker";
            this.offMarker.Size = new System.Drawing.Size(14, 14);
            this.offMarker.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.offMarker.TabIndex = 138;
            this.offMarker.TabStop = false;
            // 
            // offPrinter
            // 
            this.offPrinter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.offPrinter.Image = global::Bottle_Station.Properties.Resources.off1;
            this.offPrinter.Location = new System.Drawing.Point(959, 527);
            this.offPrinter.Name = "offPrinter";
            this.offPrinter.Size = new System.Drawing.Size(14, 14);
            this.offPrinter.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.offPrinter.TabIndex = 137;
            this.offPrinter.TabStop = false;
            // 
            // offCamera2
            // 
            this.offCamera2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.offCamera2.Image = global::Bottle_Station.Properties.Resources.off1;
            this.offCamera2.Location = new System.Drawing.Point(959, 443);
            this.offCamera2.Name = "offCamera2";
            this.offCamera2.Size = new System.Drawing.Size(14, 14);
            this.offCamera2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.offCamera2.TabIndex = 136;
            this.offCamera2.TabStop = false;
            // 
            // offCamera1
            // 
            this.offCamera1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.offCamera1.Image = global::Bottle_Station.Properties.Resources.off1;
            this.offCamera1.Location = new System.Drawing.Point(959, 365);
            this.offCamera1.Name = "offCamera1";
            this.offCamera1.Size = new System.Drawing.Size(14, 14);
            this.offCamera1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.offCamera1.TabIndex = 135;
            this.offCamera1.TabStop = false;
            // 
            // onMarker
            // 
            this.onMarker.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.onMarker.Image = global::Bottle_Station.Properties.Resources.on1;
            this.onMarker.Location = new System.Drawing.Point(959, 608);
            this.onMarker.Name = "onMarker";
            this.onMarker.Size = new System.Drawing.Size(14, 14);
            this.onMarker.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.onMarker.TabIndex = 134;
            this.onMarker.TabStop = false;
            this.onMarker.Visible = false;
            // 
            // onPrinter
            // 
            this.onPrinter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.onPrinter.Image = global::Bottle_Station.Properties.Resources.on1;
            this.onPrinter.Location = new System.Drawing.Point(959, 527);
            this.onPrinter.Name = "onPrinter";
            this.onPrinter.Size = new System.Drawing.Size(14, 14);
            this.onPrinter.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.onPrinter.TabIndex = 133;
            this.onPrinter.TabStop = false;
            this.onPrinter.Visible = false;
            // 
            // onCamera2
            // 
            this.onCamera2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.onCamera2.Image = global::Bottle_Station.Properties.Resources.on1;
            this.onCamera2.Location = new System.Drawing.Point(959, 443);
            this.onCamera2.Name = "onCamera2";
            this.onCamera2.Size = new System.Drawing.Size(14, 14);
            this.onCamera2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.onCamera2.TabIndex = 132;
            this.onCamera2.TabStop = false;
            this.onCamera2.Visible = false;
            // 
            // onCamera1
            // 
            this.onCamera1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.onCamera1.Image = global::Bottle_Station.Properties.Resources.on1;
            this.onCamera1.Location = new System.Drawing.Point(959, 365);
            this.onCamera1.Name = "onCamera1";
            this.onCamera1.Size = new System.Drawing.Size(14, 14);
            this.onCamera1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.onCamera1.TabIndex = 131;
            this.onCamera1.TabStop = false;
            this.onCamera1.Visible = false;
            // 
            // btnCheckIndicator
            // 
            this.btnCheckIndicator.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCheckIndicator.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCheckIndicator.Image = global::Bottle_Station.Properties.Resources.indicator;
            this.btnCheckIndicator.Location = new System.Drawing.Point(861, 769);
            this.btnCheckIndicator.Name = "btnCheckIndicator";
            this.btnCheckIndicator.Size = new System.Drawing.Size(150, 40);
            this.btnCheckIndicator.TabIndex = 130;
            this.btnCheckIndicator.Text = "Indicator";
            this.btnCheckIndicator.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCheckIndicator.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCheckIndicator.UseVisualStyleBackColor = true;
            this.btnCheckIndicator.Click += new System.EventHandler(this.btnCheckIndicator_Click);
            // 
            // btnLog
            // 
            this.btnLog.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLog.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLog.Image = global::Bottle_Station.Properties.Resources.log;
            this.btnLog.Location = new System.Drawing.Point(861, 234);
            this.btnLog.Name = "btnLog";
            this.btnLog.Size = new System.Drawing.Size(150, 40);
            this.btnLog.TabIndex = 128;
            this.btnLog.Text = "Log";
            this.btnLog.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnLog.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnLog.UseVisualStyleBackColor = true;
            this.btnLog.Click += new System.EventHandler(this.btnLog_Click);
            // 
            // btnStartGs
            // 
            this.btnStartGs.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStartGs.Image = global::Bottle_Station.Properties.Resources.start;
            this.btnStartGs.Location = new System.Drawing.Point(650, 102);
            this.btnStartGs.Name = "btnStartGs";
            this.btnStartGs.Size = new System.Drawing.Size(150, 40);
            this.btnStartGs.TabIndex = 125;
            this.btnStartGs.Text = "Start GS1";
            this.btnStartGs.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnStartGs.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnStartGs.UseVisualStyleBackColor = true;
            this.btnStartGs.Click += new System.EventHandler(this.btnStartGs_Click);
            // 
            // btnSample
            // 
            this.btnSample.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSample.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSample.Image = global::Bottle_Station.Properties.Resources.sample;
            this.btnSample.Location = new System.Drawing.Point(861, 93);
            this.btnSample.Name = "btnSample";
            this.btnSample.Size = new System.Drawing.Size(150, 40);
            this.btnSample.TabIndex = 122;
            this.btnSample.Text = "Sample";
            this.btnSample.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSample.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSample.UseVisualStyleBackColor = true;
            this.btnSample.Click += new System.EventHandler(this.btnSample_Click);
            // 
            // pictureBox5
            // 
            this.pictureBox5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox5.ImageLocation = "domino.jpg";
            this.pictureBox5.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox5.InitialImage")));
            this.pictureBox5.Location = new System.Drawing.Point(890, 598);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(55, 50);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox5.TabIndex = 110;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox4.ImageLocation = "libringer.jpg";
            this.pictureBox4.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox4.InitialImage")));
            this.pictureBox4.Location = new System.Drawing.Point(891, 513);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(55, 50);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox4.TabIndex = 109;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox2.ImageLocation = "genie-nano-camera.png";
            this.pictureBox2.Location = new System.Drawing.Point(891, 431);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(55, 50);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 107;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBoxCamera
            // 
            this.pictureBoxCamera.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBoxCamera.ImageLocation = "genie-nano-camera.png";
            this.pictureBoxCamera.Location = new System.Drawing.Point(890, 352);
            this.pictureBoxCamera.Name = "pictureBoxCamera";
            this.pictureBoxCamera.Size = new System.Drawing.Size(55, 50);
            this.pictureBoxCamera.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxCamera.TabIndex = 106;
            this.pictureBoxCamera.TabStop = false;
            // 
            // btnDeco
            // 
            this.btnDeco.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDeco.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeco.Image = global::Bottle_Station.Properties.Resources.deco;
            this.btnDeco.Location = new System.Drawing.Point(861, 140);
            this.btnDeco.Name = "btnDeco";
            this.btnDeco.Size = new System.Drawing.Size(150, 40);
            this.btnDeco.TabIndex = 103;
            this.btnDeco.Text = "Decommission";
            this.btnDeco.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDeco.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnDeco.UseVisualStyleBackColor = true;
            this.btnDeco.Click += new System.EventHandler(this.btnDecommission_Click_1);
            // 
            // btnAudit
            // 
            this.btnAudit.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAudit.Image = global::Bottle_Station.Properties.Resources.pre_check;
            this.btnAudit.Location = new System.Drawing.Point(338, 102);
            this.btnAudit.Name = "btnAudit";
            this.btnAudit.Size = new System.Drawing.Size(150, 40);
            this.btnAudit.TabIndex = 95;
            this.btnAudit.Text = "Pre-Check";
            this.btnAudit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAudit.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnAudit.UseVisualStyleBackColor = true;
            this.btnAudit.Click += new System.EventHandler(this.btnPrecheck_Click);
            // 
            // btnConfig
            // 
            this.btnConfig.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnConfig.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConfig.Image = global::Bottle_Station.Properties.Resources.config;
            this.btnConfig.Location = new System.Drawing.Point(860, 187);
            this.btnConfig.Name = "btnConfig";
            this.btnConfig.Size = new System.Drawing.Size(150, 40);
            this.btnConfig.TabIndex = 93;
            this.btnConfig.Text = "Config";
            this.btnConfig.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnConfig.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnConfig.UseVisualStyleBackColor = true;
            this.btnConfig.Click += new System.EventHandler(this.btnConfig_Click);
            // 
            // btnLogout
            // 
            this.btnLogout.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLogout.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLogout.Image = global::Bottle_Station.Properties.Resources.logout;
            this.btnLogout.Location = new System.Drawing.Point(861, 281);
            this.btnLogout.Name = "btnLogout";
            this.btnLogout.Size = new System.Drawing.Size(150, 40);
            this.btnLogout.TabIndex = 92;
            this.btnLogout.Text = "Logout";
            this.btnLogout.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnLogout.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnLogout.UseVisualStyleBackColor = true;
            this.btnLogout.Click += new System.EventHandler(this.btnLogout_Click);
            // 
            // btnStart
            // 
            this.btnStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStart.Image = global::Bottle_Station.Properties.Resources.start;
            this.btnStart.Location = new System.Drawing.Point(494, 102);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(150, 40);
            this.btnStart.TabIndex = 39;
            this.btnStart.Text = "Start CapID";
            this.btnStart.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnStart.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStartCap_Click);
            // 
            // pictureBox6
            // 
            this.pictureBox6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox6.Image = global::Bottle_Station.Properties.Resources.dtbs;
            this.pictureBox6.Location = new System.Drawing.Point(894, 687);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(55, 50);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox6.TabIndex = 144;
            this.pictureBox6.TabStop = false;
            // 
            // BottleStation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1024, 821);
            this.Controls.Add(this.lblQtyGs);
            this.Controls.Add(this.pictureBox6);
            this.Controls.Add(this.offDb);
            this.Controls.Add(this.onDb);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.lblDb);
            this.Controls.Add(this.lblQtyCapid);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.offMarker);
            this.Controls.Add(this.offPrinter);
            this.Controls.Add(this.offCamera2);
            this.Controls.Add(this.offCamera1);
            this.Controls.Add(this.onMarker);
            this.Controls.Add(this.onPrinter);
            this.Controls.Add(this.onCamera2);
            this.Controls.Add(this.onCamera1);
            this.Controls.Add(this.btnCheckIndicator);
            this.Controls.Add(this.txtBatchNumber);
            this.Controls.Add(this.btnLog);
            this.Controls.Add(this.btnStartGs);
            this.Controls.Add(this.btnSample);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBoxCamera);
            this.Controls.Add(this.lblMarker);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.btnDeco);
            this.Controls.Add(this.lblAdmin);
            this.Controls.Add(this.btnAudit);
            this.Controls.Add(this.btnConfig);
            this.Controls.Add(this.btnLogout);
            this.Controls.Add(this.lblPrinter);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.lblCamera2);
            this.Controls.Add(this.lblQuan2);
            this.Controls.Add(this.lblCamera1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.button1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "BottleStation";
            this.Text = "Bottle Station";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.BottleStation_Load);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.Click += new System.EventHandler(this.BottleStation_Click);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvGsone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCapId)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvReceive)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.offDb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.onDb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.offMarker)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.offPrinter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.offCamera2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.offCamera1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.onMarker)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.onPrinter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.onCamera2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.onCamera1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCamera)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.Label lblQtyGs;
        public System.Windows.Forms.Label lblQtyCapid;
        public System.Windows.Forms.GroupBox groupBox1;
        public System.Windows.Forms.DataGridView dgvReceive;
        public System.Windows.Forms.GroupBox groupBox2;
        public System.Windows.Forms.PictureBox pictureBox1;
        public System.Windows.Forms.Label label1;
        public System.Windows.Forms.Button btnStart;
        public System.Windows.Forms.Button btnConfig;
        public System.Windows.Forms.Label lblPrinter;
        public System.Windows.Forms.Label label16;
        public System.Windows.Forms.Label lblCamera2;
        public System.Windows.Forms.Label lblQuan2;
        public System.Windows.Forms.Label lblCamera1;
        public System.Windows.Forms.DataGridView dgvGsone;
        public System.Windows.Forms.DataGridView dgvCapId;
        public System.Windows.Forms.Label label17;
        public System.Windows.Forms.Label lblQuantityReceive;
        public System.Windows.Forms.Label label3;
        public System.Windows.Forms.Button btnAudit;
        public System.Windows.Forms.Label label2;
        public System.Windows.Forms.Label label8;
        public System.Windows.Forms.Label label9;
        public System.Windows.Forms.Label lblAdmin;
        public System.Windows.Forms.Button btnDeco;
        public System.Windows.Forms.Label label7;
        public System.Windows.Forms.Label label18;
        public System.Windows.Forms.Label lblMarker;
        public System.Windows.Forms.Label label11;
        public System.Windows.Forms.PictureBox pictureBoxCamera;
        public System.Windows.Forms.PictureBox pictureBox2;
        public System.Windows.Forms.Label label5;
        public System.Windows.Forms.PictureBox pictureBox4;
        public System.Windows.Forms.PictureBox pictureBox5;
        public System.Windows.Forms.Label label4;
        public System.Windows.Forms.Label label12;
        public System.Windows.Forms.Label label19;
        public System.Windows.Forms.Button btnSample;
        public System.Windows.Forms.Label lblQtyFail;
        public System.Windows.Forms.Label label10;
        public System.Windows.Forms.Label lblQtyPass;
        public System.Windows.Forms.Label label13;
        public System.Windows.Forms.Button btnStartGs;
        public System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label lblTime;
        private System.Windows.Forms.Label lblDate;
        private System.Windows.Forms.PictureBox pictureBox3;
        public System.Windows.Forms.Label lblRole;
        public System.Windows.Forms.Label lblUserId;
        public System.Windows.Forms.Button btnLog;
        public System.Windows.Forms.Label lblProductName;
        public System.Windows.Forms.Label lblLine;
        public System.Windows.Forms.Label lblBatch;
        private System.Windows.Forms.Timer timer1;
        public System.Windows.Forms.TextBox txtBatchNumber;
        public System.Windows.Forms.Button btnLogout;
        public System.Windows.Forms.Button btnCheckIndicator;
        private System.Windows.Forms.PictureBox onCamera1;
        private System.Windows.Forms.PictureBox onCamera2;
        private System.Windows.Forms.PictureBox onPrinter;
        private System.Windows.Forms.PictureBox onMarker;
        public System.Windows.Forms.Label label6;
        private System.Windows.Forms.PictureBox offCamera1;
        private System.Windows.Forms.PictureBox offCamera2;
        private System.Windows.Forms.PictureBox offPrinter;
        private System.Windows.Forms.PictureBox offMarker;
        public System.Windows.Forms.Label lblQtyNow;
        public System.Windows.Forms.Label label15;
        private System.Windows.Forms.PictureBox offDb;
        private System.Windows.Forms.PictureBox onDb;
        public System.Windows.Forms.Label lblDb;
        public System.Windows.Forms.Label label20;
        private System.Windows.Forms.PictureBox pictureBox6;
    }
}

