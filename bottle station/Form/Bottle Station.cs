﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.Net.NetworkInformation;

namespace Bottle_Station
{
    public partial class BottleStation : Form
    {

//## not Used
//        List<string[]> datagsoneVial;
//        string[] datacapTrial;
//        int banyakCap;
//        string[] dataVialTrial;
//        int banyakVial;
//        int post = 0;
//        string batch;
//        string[] tempcap;
//        string productmodelid;
//        string[] tempvial;
//        int postv = 0;
//        int tempterima = 1;
//        List<string[]> datavial = new List<string[]>();
//        string[] tempCap;
//        string[] tempVial;
//        private Thread serverThread;
//        List<string[]> datacombo;
//        int qtyReceive;
//        bool onprogress = false;
//        string jumlah;
//        int startke = 0;
//        string ordernumber;
//        int quantysend;
      
        sqlitecs sqlite;
        List<int> quantysendke = new List<int>();
        List<string[]> datacap = new List<string[]>();
        DataTable table = new DataTable();
        dbaccess db;
        generateCode Code;
        DataTable tableCap;
        DataTable tableVial;
        List<string> capid = new List<string>();
        Control ctrlCap;
        System.Timers.Timer delayTimer;
        public bool startCap = false;
        public bool startGs = false;
        public bool audit;
        string admin;
        public string adminName;

        public Dashboard dbss;
        string parameter;
        public ListBatchNumber lbn;
        string[] linenumber;

        //tambahan disable close button
        private const int CP_NOCLOSE_BUTTON = 0x200;
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams myCp = base.CreateParams;
                myCp.ClassStyle = myCp.ClassStyle | CP_NOCLOSE_BUTTON;
                return myCp;
            }
        }
        //end of disable close button

        public BottleStation(Dashboard dbs, string adminid, string[] line, string param, Control ctrl)
        {
            InitializeComponent();
            this.Text = this.Text + " V " + System.Windows.Forms.Application.ProductVersion;
            sqlite = new sqlitecs();
            Code = new generateCode();
            db = new dbaccess();
            admin = adminid;
            initializeControl(adminid, line, ctrl);
            lblLine.Text = line[0];
            db.from = "Bottle Station";
            cekDeviceCap();
            cekDeviceGS();
            cekCamera();
            cekDB();
            //InitTimer();

            dbss = dbs;
            parameter = param;
            linenumber = line;
            lblBatch.Text = "";
            lblProductName.Text = "";
        }

        void initializeControl(string adminid, string[] line, Control ctr)
        {
            //ctrlCap = new Control();
            ctrlCap = ctr;
            ctrlCap.sqlite = sqlite;
            ctrlCap.bl = this;
            ctrlCap.adminid = adminid;
            ctrlCap.db = db;
            ctrlCap.Code = Code;
            ctrlCap.adminid = adminid;
            ctrlCap.linename = line[0];
            ctrlCap.linenumber = line[1];
            ctrlCap.startStation();
        }

        private void btnLogout_Click(object sender, EventArgs e)
        {
            Confirm cf = new Confirm("Are you sure? ", "Confirmation");
            bool result = cf.conf();
            if (result)
            {
                //close2("");
                if (db.Connect())
                {
                    db.eventname = "Logout Bottle station " + admin;
                    db.systemlog();
                }
                //close("");
                Application.Exit();
                Environment.Exit(Environment.ExitCode);
                //Login lg = new Login();
               //lg.Show();
            }
        }

        private void btnConfig_Click(object sender, EventArgs e)
        {
            if (startCap || startGs)
            {
                new Confirm("Printer On Progress");
            }
            else
            {
                config cfg = new config(this,null,1,admin);
                cfg.ShowDialog();
            }
        }

        public void refresh()
        {
            if (startCap || startGs)
            {
                sqlite = new sqlitecs();
                table = new DataTable();
                table.Columns.Add("Cap ID");
                table.Columns.Add("GS1 Vial ID");
                table.Columns.Add("Timestamp");
                table.Columns.Add("Status");

                tableCap = new DataTable();
                tableVial = new DataTable();
                //getJumlah();
                //if (sqlite.config["flag"].Equals("0"))
                //    onprogress = false;
                //else
                //    onprogress = true;
                //     cmb();
                dgvReceive.DataSource = table;
                dgvCapId.DataSource = tableCap;
                dgvGsone.DataSource = tableVial;
                ctrlCap.refresh();
            }
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            close("");
        }

        public void close(string ti)
        {
            try
            {
                if (InvokeRequired)
                {
                    this.Invoke(new Action<string>(close), new object[] { ti });
                    return;
                }
                Application.Exit();
              //  ctrlCap.srv.StreamClose();
                ctrlCap.srvMulti.closeServer();
                ctrlCap.Code.stop();
                ctrlCap.prin.conneect = false;
            }
            catch (Exception ex)
            {
            }
        }

        public void close2(string ti)
        {
            try
            {
                if (InvokeRequired)
                {
                    this.Invoke(new Action<string>(close2), new object[] { ti });
                    return;
                }
                //ctrlCap.srv.StreamClose();
                ctrlCap.srvMulti.closeServer();
            }
            catch (Exception ex_a)
            {
            }
        }

        private void btnPrecheck_Click(object sender, EventArgs e)
        {
            Confirm cf = new Confirm("Are you sure? ", "Confirmation");
            bool hasilDialog = cf.conf();
            print pr = new print();
            if (hasilDialog)
            {
                //if (cmbBatch.SelectedIndex > -1)
                //{
                    if (cekDeviceCap())
                    {
                        if (cekDeviceGS())
                        {
                            //ctrlCap.batch = ctrlCap.datacombo[cmbBatch.SelectedIndex][0];
                            ctrlCap.batch = txtBatchNumber.Text;
                            ctrlCap.SetClearDataGridReceive();
                            ctrlCap.StartPreCheck();
                            audit = true;
                            ctrlCap.SendToPrinter(ctrlCap.getDataSendToPrinter(int.Parse(sqlite.config["audit"])));
                            ctrlCap.SendToPrinter(ctrlCap.getDataSendToPrintergs(int.Parse(sqlite.config["audit"])));
                            ctrlCap.SetDataGrid_Send("0", "");
                            ctrlCap.SetDataGridSendgs("0", "");
                        }
                        else
                        {
                            new Confirm("Laser marker not connect", "Information", MessageBoxButtons.OK);
                        }
                    }
                    else
                    {
                        new Confirm("Inkjet not connected", "Information", MessageBoxButtons.OK);
                    }
                //}
                //else
                //{
                //    new Confirm("Batchnumber not recognition", "Information", MessageBoxButtons.OK);
                //}
            }
        }

        private void btnSample_Click(object sender, EventArgs e)
        {
            Sample sam = new Sample(admin);
            sam.ShowDialog();
        }

        private void btnDecommission_Click_1(object sender, EventArgs e)
        {
            Decomission deco = new Decomission(admin, ctrlCap.linenumber, parameter);
            deco.ShowDialog();
        }

        private void delay()
        {
            delayTimer = new System.Timers.Timer();
            delayTimer.Interval = 60000;
            delayTimer.Elapsed += delayTimer_Elapsed;
            delayTimer.Start();
        }

        private void delayTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
         //    db.addVaccine(batch, sqlite.config["lineNumber"], productmodelid, sqlite);
        }

        private void dgvGsone_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            lblQtyGs.Text = dgvGsone.Rows.Count.ToString();
        }

        private void dgvCapId_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {

            lblQtyCapid.Text = dgvCapId.Rows.Count.ToString();
        }

        private void dgvReceive_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            //lblQuantityReceive.Text = dgvReceive.Rows.Count.ToString();
        }

        private void btnStartCap_Click(object sender, EventArgs e)
        {
            Confirm cf = new Confirm("Are you sure? ", "Confirmation");
            bool hasilDialog = cf.conf();
            if (hasilDialog)
            {
                if (!startCap)
                {
                    if (cekDeviceCap())
                    {
                        if (cekDB())
                        {
                            //if (cmbBatch.SelectedIndex > -1)
                            //{
                            //ctrlCap.batch = ctrlCap.datacombo[cmbBatch.SelectedIndex][0];
                            ctrlCap.batch = txtBatchNumber.Text;
                            txtBatchNumber.Enabled = false;
                            lblBatch.Text = txtBatchNumber.Text;

                            ctrlCap.clearBuffer_Cap();
                            ctrlCap.log("Start print CapID");
                            ctrlCap.SendStartPrintercap();
                            ctrlCap.Start_Form();
                            btnAudit.Enabled = false;
                            //cmbBatch.Enabled = false;
                            startCap = true;
                            btnStart.Text = "Stop Cap ID";
                            btnStart.Image = global::Bottle_Station.Properties.Resources.stop;
                            ctrlCap.SetDataGrid_Send("0", "");
                            //}
                            //else
                            //{
                            //    new Confirm("Batchnumber not recognition", "Information", MessageBoxButtons.OK);
                            //}
                        }
                    }
                    else
                    {
                        new Confirm("Inkjet not connected", "Information", MessageBoxButtons.OK);
                    }

                }
                else
                {
                    startCap = false;
                    ctrlCap.Stop_Form();
                    ctrlCap.stopPrinterCap();
                    btnStart.Text = "Start CapID";
                    btnStart.Image = global::Bottle_Station.Properties.Resources.start;
                }
            }
        }
       
        private void btnStartGs_Click(object sender, EventArgs e)
        {
            Confirm cf = new Confirm("Are you sure? ", "Confirmation");
            bool hasilDialog = cf.conf();
            if (hasilDialog)
            {
                if (!startGs)
                {
                    if (cekCamera())
                    {
                        if (cekDeviceGS())
                        {
                            if (cekDB())
                            {
                                if (cekPLCConnect())
                                {
                                    //if (cmbBatch.SelectedIndex > -1)
                                    //{

                                    //ctrlCap.batch = ctrlCap.datacombo[cmbBatch.SelectedIndex][0];
                                    ctrlCap.batch = txtBatchNumber.Text;
                                    txtBatchNumber.Enabled = false;

                                    if (sqlite.config["debug"].Equals("0"))
                                    {
                                        ctrlCap.clearBuffer_Gs();
                                    }
                                    ctrlCap.clearBuffer_Gs();
                                    ctrlCap.SetClearDataGridReceive();
                                    ctrlCap.SetParameterStartProduction();
                                    ctrlCap.Start_Form();
                                    ctrlCap.startListenerPLC();
                                    btnAudit.Enabled = false;
                                    //cmbBatch.Enabled = false;
                                    startGs = true;
                                    btnStartGs.Text = "Stop GS1";
                                    btnStartGs.Image = global::Bottle_Station.Properties.Resources.stop;
                                    ctrlCap.SetDataGridSendgs("0", "");
                                    //}
                                    //else
                                    //{
                                    //    new Confirm("Batchnumber not recognition", "Information", MessageBoxButtons.OK);
                                    //}
                                }
                                else
                                {
                                    new Confirm("PLC Not Connected", "Information", MessageBoxButtons.OK);
                                }
                            }
                        }
                        else
                        {
                            new Confirm("Laser marker not ready", "Information", MessageBoxButtons.OK);
                        }
                    }

                }
                else
                {
                    ctrlCap.cleanDataTempgs();
                    startGs = false;
                    ctrlCap.Stop_Form();
                    btnStartGs.Text = "Start GS1";
                    btnStartGs.Image = global::Bottle_Station.Properties.Resources.start;
                    ctrlCap.auditStarted = false;
                }
            }
        }
        private bool Is_FirstConnect = false;
        private bool cekPLCConnect()
        {
            if (!Is_FirstConnect)
            {
                Is_FirstConnect =ctrlCap.checkPLCConnected();
                return Is_FirstConnect;
            }
            else
                return true;
        }

        private bool cekDeviceCap()
        {

            print pr = new print();
            if (pr.cekKoneksi())
            {
                lblPrinter.Text = "Online";
                onPrinter.Visible = true;
                offPrinter.Visible = false;
                pr.closeCap();
                return true;
            }
            else
            {
                lblPrinter.Text = "Offline";
                onPrinter.Visible = false;
                offPrinter.Visible = true;
                return false;
            }
        }

        private bool cekCamera()
        {
            if (ctrlCap.PingHost(sqlite.config["ipCamera"]))
            {
                lblCamera1.Text = "Online";
                onCamera1.Visible = true;
                offCamera1.Visible = false;

                if (ctrlCap.PingHost(sqlite.config["ipcamera2"]))
                {
                    lblCamera2.Text = "Online";
                    onCamera2.Visible = true;
                    offCamera2.Visible = false;
                    return true;
                }
                else
                {
                    lblCamera2.Text = "Offline";
                    onCamera2.Visible = false;
                    offCamera2.Visible = true;
                    new Confirm("Camera Laser not connected");
                }
            }
            else
            {
                lblCamera1.Text = "Offline";
                onCamera1.Visible = false;
                offCamera1.Visible = true;
                new Confirm("Camera Inkjet not connected");
            }
            return false;
        }
      
        private bool cekDeviceGS()
        {
            print pr = new print();
            if (pr.cekKoneksigs())
            {
                lblMarker.Text = "Online";
                onMarker.Visible = true;
                offMarker.Visible = false;
                pr.closeGs();
                return true;
            }
            else
            {
                lblMarker.Text = "Offline";
                onMarker.Visible = false;
                offMarker.Visible = true;
                return false;
            }
        }

        public bool cekDB()
        {
            if (db.Connect())
            {
                //Console.WriteLine("MASUK ONLINE " + DateTime.Now.ToLongTimeString());
                lblDb.Text = "Online";
                onDb.Visible = true;
                offDb.Visible = false;
                ctrlCap.StopServer();
                ctrlCap.StartServer2();
                db.closeConnection();
                return true;
            }
            else
            {
                //Console.WriteLine("MASUK OFFLINE " + DateTime.Now.ToLongTimeString());
                lblDb.Text = "Offline";
                onDb.Visible = false;
                offDb.Visible = true;
                ctrlCap.StopServer();
                return false;
            }
        }

        ShowDataTemp fd;
        private void button1_Click(object sender, EventArgs e)
        {
            if (fd == null)
            {
                fd = new ShowDataTemp(ctrlCap, null);
                //fd.ctrl = ctrlCap;
                fd.FormClosing += new FormClosingEventHandler(fd_FormClosing);
            }
            fd.Show(this);
        }

        void fd_FormClosing(object sender, FormClosingEventArgs e)
        {
            fd = null;
        }

        private void lblAdmin_Click(object sender, EventArgs e)
        {
            ctrlCap.batch = txtBatchNumber.Text;
           // ctrlCap.batch = ctrlCap.datacombo[cmbBatch.SelectedIndex][0];
           // ctrlCap.clearBufferGs();
           // ctrlCap.SendStartPrinterGs();
        }

        private void BottleStation_Load(object sender, EventArgs e)
        {
            timer1.Start();
            lblTime.Text = DateTime.Now.ToLongTimeString();
            lblDate.Text = DateTime.Now.ToShortDateString();
        }

        private void BottleStation_Click(object sender, EventArgs e)
        {
            DataGridViewSelectionMode oldmode = dgvReceive.SelectionMode;

            dgvReceive.SelectionMode = DataGridViewSelectionMode.CellSelect;

            dgvReceive.ClearSelection();

            dgvReceive.SelectionMode = oldmode;
        }

        private void txtBatchNumber_Click(object sender, EventArgs e)
        {
            lbn = new ListBatchNumber(null, linenumber[1], parameter, "");
            lbn.ShowDialog();
            txtBatchNumber.Text = lbn.batch;
            ctrlCap.SetClearDataGridCap();
            ctrlCap.SetClearDataGridGS();
            int[] cnt = ctrlCap.getCount(lbn.batch);
            lblQtyPass.Text = cnt[0].ToString();
            lblQtyFail.Text = cnt[1].ToString();
            lblQuantityReceive.Text = cnt[2].ToString();
            string ds = ctrlCap.getBatchSqlite();
            if (ds != "")
            {
                if (ds == lbn.batch)
                {
                    ctrlCap.SetDataGrid_Send("0", "");
                    ctrlCap.SetDataGridSendgs("0", "");
                }
            }
                
            btnAudit.Focus();
            if (txtBatchNumber.Text.Length > 0)
            {
                btnAudit.Enabled = true;
                btnStart.Enabled = true;
                btnStartGs.Enabled = true;
            }
            else
            {
                btnAudit.Enabled = false;
                btnStart.Enabled = false;
                btnStartGs.Enabled = false;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            lblTime.Text = DateTime.Now.ToLongTimeString();
            timer1.Start();
        }

        private void pictureBox1_MouseEnter(object sender, EventArgs e)
        {
            this.pictureBox1.BackColor = ColorTranslator.FromHtml("#00505E");
        }

        private void pictureBox1_MouseLeave(object sender, EventArgs e)
        {
            this.pictureBox1.BackColor = ColorTranslator.FromHtml("#002F36");
        }

        private void pictureBox1_DoubleClick(object sender, EventArgs e)
        {
            close2("");
            dbss.Show();
            this.Hide();
        }

        private void btnLog_Click(object sender, EventArgs e)
        {
            Log lg = ctrlCap.lg;
            lg.Show();
        }

        private void dgvReceive_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0) 
            { 
                if (dgvReceive.Rows[e.RowIndex].Cells[e.ColumnIndex].Value != null)
                {
                    DetailVaccine dv = new DetailVaccine();
                    dv.lblBatchNumber.Text = txtBatchNumber.Text;
                    dv.lblCapID.Text = this.dgvReceive.Rows[e.RowIndex].Cells[0].FormattedValue.ToString();
                    dv.lblGsOne.Text = this.dgvReceive.Rows[e.RowIndex].Cells[1].FormattedValue.ToString();
                    dv.lblTime.Text = this.dgvReceive.Rows[e.RowIndex].Cells[2].FormattedValue.ToString();
                    dv.lblStatus.Text = this.dgvReceive.Rows[e.RowIndex].Cells[3].FormattedValue.ToString();
                    dv.ShowDialog();
                    
                }
            }
            
        }

        private void btnCheckIndicator_Click(object sender, EventArgs e)
        {
            cekDeviceCap();
            cekDeviceGS();
            cekDB();
            cekCamera();
        }

        //timer for check online/offline
        //public void InitTimer()
        //{
        //    timer2 = new System.Windows.Forms.Timer();
        //    timer2.Tick += new EventHandler(timer2_Tick);
        //    timer2.Interval = 1000; // in miliseconds
        //    timer2.Start();
        //}

        //private void timer2_Tick(object sender, EventArgs e)
        //{
        //    //if(lblPrinter.Text.Equals("Offline"))
        //        cekDeviceCap();
            
        //    //if(lblMarker.Text.Equals("Offline"))
        //        cekDeviceGS();
        //}


        ////## not used
        //        public bool cekPrinterReady()
        //        {
        //            print pr = new print();
        //            if (pr.cekKoneksi())
        //            {
        //                if (pr.cekStatusPrinter())
        //                {
        //                    printGs prg = new printGs();
        //                    if (prg.cekKoneksi())
        //                    {
        //                        if (prg.cekStatusPrinter())
        //                        {
        //                            ctrlCap.log(prg.statusdomino);
        //                            return true;
        //                            pr.close();
        //                            prg.close();
        //                        }
        //                        else
        //                        {
        //                            new Confirm("Laser: " + prg.cekStatusPrinter());
        //                        }
        //                    }
        //                    else
        //                    {
        //                        new Confirm("Laser Not connected");
        //                    }
        //                }
        //                else
        //                {
        //                    new Confirm("Printer " + pr.statusLeibinger);
        //                }
        //            }
        //            else
        //            {
        //                new Confirm("Printer Not connected");
        //            }
        //            return false;
        //        }
        //## Not used
        //        private bool cekPrinter()
        //        {
        //            print pr = new print();
        //            bool cek;
        //            bool cekgs;
        //            if (pr.cekKoneksi())
        //            {
        //                ctrlCap.log("Inkjet Printer Conected");
        //                cek = true;
        //            }
        //            else
        //            {
        //                ctrlCap.log("Inkjet Printer Not Conected");
        //                cek = false;
        //            }

        //            printGs prg = new printGs();
        //            if (prg.cekKoneksi())
        //            {
        //                ctrlCap.log("Laser Marker Conected");
        //                cekgs = true;
        //            }
        //            else
        //            {
        //                ctrlCap.log("Laser Marker Not Conected");
        //                cekgs = false;
        //            }
        //            if (cek && cekgs)
        //                return true;
        //            else
        //                return false;
        //        }

        //## Not Used
        //        private void stopPrinting()
        //        {
        //            start = false;
        //            refresh();
        //            delayTimer.Stop();
        //            printGs prg = new printGs();
        //            prg.cekKoneksi();
        //            prg.kirimData("BUFFERCLEAR");
        //        }
    }
}

