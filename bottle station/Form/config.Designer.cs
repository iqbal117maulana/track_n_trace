﻿namespace Bottle_Station
{
    partial class config
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtIpServer = new System.Windows.Forms.TextBox();
            this.txtPortServer = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtIpPrinter = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtPortPrinter = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtIpCamera = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtPortCamera = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.txtPortDb = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtIpDB = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtTimeoutDb = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtPortMarker = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtIpMarker = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtPortCamera2 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtIpCamera2 = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtAudit = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtNamaDB = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtSendPrinter = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtGenerate = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.txtBagi = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.txtDelay = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.chkDebug = new System.Windows.Forms.CheckBox();
            this.label39 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.chkBuffer = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label45 = new System.Windows.Forms.Label();
            this.txtSecond = new System.Windows.Forms.TextBox();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.txtAgain = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.txtIP_PLC = new System.Windows.Forms.TextBox();
            this.label48 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.txtPort_PLC = new System.Windows.Forms.TextBox();
            this.label46 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.txtQtytable = new System.Windows.Forms.TextBox();
            this.label51 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Enabled = false;
            this.label1.Location = new System.Drawing.Point(201, 458);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Ip Server";
            this.label1.Visible = false;
            // 
            // txtIpServer
            // 
            this.txtIpServer.Enabled = false;
            this.txtIpServer.Location = new System.Drawing.Point(290, 455);
            this.txtIpServer.Name = "txtIpServer";
            this.txtIpServer.Size = new System.Drawing.Size(100, 20);
            this.txtIpServer.TabIndex = 1;
            this.txtIpServer.Visible = false;
            // 
            // txtPortServer
            // 
            this.txtPortServer.Location = new System.Drawing.Point(111, 22);
            this.txtPortServer.Name = "txtPortServer";
            this.txtPortServer.Size = new System.Drawing.Size(100, 20);
            this.txtPortServer.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Port Server";
            // 
            // txtIpPrinter
            // 
            this.txtIpPrinter.Location = new System.Drawing.Point(109, 22);
            this.txtIpPrinter.Name = "txtIpPrinter";
            this.txtIpPrinter.Size = new System.Drawing.Size(100, 20);
            this.txtIpPrinter.TabIndex = 10;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 22);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(50, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "IP Printer";
            // 
            // txtPortPrinter
            // 
            this.txtPortPrinter.Location = new System.Drawing.Point(109, 48);
            this.txtPortPrinter.Name = "txtPortPrinter";
            this.txtPortPrinter.Size = new System.Drawing.Size(100, 20);
            this.txtPortPrinter.TabIndex = 11;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 48);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(59, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Port Printer";
            // 
            // txtIpCamera
            // 
            this.txtIpCamera.Location = new System.Drawing.Point(102, 25);
            this.txtIpCamera.Name = "txtIpCamera";
            this.txtIpCamera.Size = new System.Drawing.Size(100, 20);
            this.txtIpCamera.TabIndex = 8;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 57);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "IP Camera 2";
            // 
            // txtPortCamera
            // 
            this.txtPortCamera.Location = new System.Drawing.Point(618, 83);
            this.txtPortCamera.Name = "txtPortCamera";
            this.txtPortCamera.Size = new System.Drawing.Size(100, 20);
            this.txtPortCamera.TabIndex = 14;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(514, 112);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(74, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "Port Camera 2";
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.button1.Image = global::Bottle_Station.Properties.Resources.submit;
            this.button1.Location = new System.Drawing.Point(317, 369);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(150, 50);
            this.button1.TabIndex = 17;
            this.button1.Text = "Submit";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtPortDb
            // 
            this.txtPortDb.Location = new System.Drawing.Point(111, 49);
            this.txtPortDb.Name = "txtPortDb";
            this.txtPortDb.ReadOnly = true;
            this.txtPortDb.Size = new System.Drawing.Size(100, 20);
            this.txtPortDb.TabIndex = 15;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(7, 49);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(44, 13);
            this.label8.TabIndex = 27;
            this.label8.Text = "Port DB";
            // 
            // txtIpDB
            // 
            this.txtIpDB.Location = new System.Drawing.Point(111, 23);
            this.txtIpDB.Name = "txtIpDB";
            this.txtIpDB.Size = new System.Drawing.Size(100, 20);
            this.txtIpDB.TabIndex = 14;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(7, 23);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(35, 13);
            this.label9.TabIndex = 25;
            this.label9.Text = "IP DB";
            // 
            // txtTimeoutDb
            // 
            this.txtTimeoutDb.Location = new System.Drawing.Point(111, 75);
            this.txtTimeoutDb.Name = "txtTimeoutDb";
            this.txtTimeoutDb.ReadOnly = true;
            this.txtTimeoutDb.Size = new System.Drawing.Size(100, 20);
            this.txtTimeoutDb.TabIndex = 16;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(7, 75);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(63, 13);
            this.label10.TabIndex = 23;
            this.label10.Text = "Timeout DB";
            // 
            // txtPortMarker
            // 
            this.txtPortMarker.Location = new System.Drawing.Point(109, 100);
            this.txtPortMarker.Name = "txtPortMarker";
            this.txtPortMarker.Size = new System.Drawing.Size(100, 20);
            this.txtPortMarker.TabIndex = 13;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(7, 100);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(62, 13);
            this.label12.TabIndex = 31;
            this.label12.Text = "Port Marker";
            // 
            // txtIpMarker
            // 
            this.txtIpMarker.Location = new System.Drawing.Point(109, 74);
            this.txtIpMarker.Name = "txtIpMarker";
            this.txtIpMarker.Size = new System.Drawing.Size(100, 20);
            this.txtIpMarker.TabIndex = 12;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(7, 74);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(53, 13);
            this.label13.TabIndex = 29;
            this.label13.Text = "IP Marker";
            // 
            // txtPortCamera2
            // 
            this.txtPortCamera2.Location = new System.Drawing.Point(618, 109);
            this.txtPortCamera2.Name = "txtPortCamera2";
            this.txtPortCamera2.Size = new System.Drawing.Size(100, 20);
            this.txtPortCamera2.TabIndex = 16;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(514, 86);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(74, 13);
            this.label11.TabIndex = 35;
            this.label11.Text = "Port Camera 1";
            // 
            // txtIpCamera2
            // 
            this.txtIpCamera2.Location = new System.Drawing.Point(102, 54);
            this.txtIpCamera2.Name = "txtIpCamera2";
            this.txtIpCamera2.Size = new System.Drawing.Size(100, 20);
            this.txtIpCamera2.TabIndex = 9;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(6, 28);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(65, 13);
            this.label14.TabIndex = 33;
            this.label14.Text = "IP Camera 1";
            // 
            // txtAudit
            // 
            this.txtAudit.Location = new System.Drawing.Point(111, 19);
            this.txtAudit.Name = "txtAudit";
            this.txtAudit.Size = new System.Drawing.Size(100, 20);
            this.txtAudit.TabIndex = 2;
            this.txtAudit.TextChanged += new System.EventHandler(this.txtAudit_TextChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(9, 22);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(75, 13);
            this.label15.TabIndex = 37;
            this.label15.Text = "Qty Pre-check";
            // 
            // txtNamaDB
            // 
            this.txtNamaDB.Location = new System.Drawing.Point(111, 102);
            this.txtNamaDB.Name = "txtNamaDB";
            this.txtNamaDB.Size = new System.Drawing.Size(100, 20);
            this.txtNamaDB.TabIndex = 20;
            this.txtNamaDB.TextChanged += new System.EventHandler(this.txtNamaDB_TextChanged);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(7, 102);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(53, 13);
            this.label16.TabIndex = 39;
            this.label16.Text = "DB Name";
            // 
            // txtSendPrinter
            // 
            this.txtSendPrinter.Location = new System.Drawing.Point(111, 70);
            this.txtSendPrinter.Name = "txtSendPrinter";
            this.txtSendPrinter.Size = new System.Drawing.Size(100, 20);
            this.txtSendPrinter.TabIndex = 4;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(9, 73);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(51, 13);
            this.label17.TabIndex = 41;
            this.label17.Text = "Qty Send";
            // 
            // txtGenerate
            // 
            this.txtGenerate.Location = new System.Drawing.Point(111, 44);
            this.txtGenerate.Name = "txtGenerate";
            this.txtGenerate.Size = new System.Drawing.Size(100, 20);
            this.txtGenerate.TabIndex = 3;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(9, 47);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(70, 13);
            this.label18.TabIndex = 43;
            this.label18.Text = "Qty Generate";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Enabled = false;
            this.label19.Location = new System.Drawing.Point(276, 458);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(10, 13);
            this.label19.TabIndex = 44;
            this.label19.Text = ":";
            this.label19.Visible = false;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(95, 25);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(10, 13);
            this.label20.TabIndex = 45;
            this.label20.Text = ":";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(93, 22);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(10, 13);
            this.label21.TabIndex = 46;
            this.label21.Text = ":";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(93, 48);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(10, 13);
            this.label22.TabIndex = 47;
            this.label22.Text = ":";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(93, 74);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(10, 13);
            this.label23.TabIndex = 48;
            this.label23.Text = ":";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(93, 100);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(10, 13);
            this.label24.TabIndex = 49;
            this.label24.Text = ":";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(88, 28);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(10, 13);
            this.label25.TabIndex = 50;
            this.label25.Text = ":";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(602, 86);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(10, 13);
            this.label26.TabIndex = 51;
            this.label26.Text = ":";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(95, 22);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(10, 13);
            this.label27.TabIndex = 52;
            this.label27.Text = ":";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(95, 47);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(10, 13);
            this.label28.TabIndex = 53;
            this.label28.Text = ":";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(88, 57);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(10, 13);
            this.label29.TabIndex = 54;
            this.label29.Text = ":";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(602, 112);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(10, 13);
            this.label30.TabIndex = 55;
            this.label30.Text = ":";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(95, 23);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(10, 13);
            this.label32.TabIndex = 57;
            this.label32.Text = ":";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(95, 49);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(10, 13);
            this.label33.TabIndex = 58;
            this.label33.Text = ":";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(95, 75);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(10, 13);
            this.label34.TabIndex = 59;
            this.label34.Text = ":";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(95, 102);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(10, 13);
            this.label35.TabIndex = 60;
            this.label35.Text = ":";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(95, 73);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(10, 13);
            this.label36.TabIndex = 61;
            this.label36.Text = ":";
            // 
            // txtBagi
            // 
            this.txtBagi.Location = new System.Drawing.Point(101, 458);
            this.txtBagi.Name = "txtBagi";
            this.txtBagi.Size = new System.Drawing.Size(100, 20);
            this.txtBagi.TabIndex = 12;
            this.txtBagi.Visible = false;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(12, 461);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(73, 13);
            this.label37.TabIndex = 63;
            this.label37.Text = "Qty Threshold";
            this.label37.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(87, 461);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(10, 13);
            this.label3.TabIndex = 64;
            this.label3.Text = ":";
            this.label3.Visible = false;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(87, 438);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(10, 13);
            this.label31.TabIndex = 67;
            this.label31.Text = ":";
            // 
            // txtDelay
            // 
            this.txtDelay.Location = new System.Drawing.Point(101, 435);
            this.txtDelay.Name = "txtDelay";
            this.txtDelay.Size = new System.Drawing.Size(100, 20);
            this.txtDelay.TabIndex = 65;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(12, 438);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(75, 13);
            this.label38.TabIndex = 66;
            this.label38.Text = "Delay (Minute)";
            // 
            // chkDebug
            // 
            this.chkDebug.AutoSize = true;
            this.chkDebug.Location = new System.Drawing.Point(621, 173);
            this.chkDebug.Name = "chkDebug";
            this.chkDebug.Size = new System.Drawing.Size(40, 17);
            this.chkDebug.TabIndex = 5;
            this.chkDebug.Text = "On";
            this.chkDebug.UseVisualStyleBackColor = true;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(605, 173);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(10, 13);
            this.label39.TabIndex = 70;
            this.label39.Text = ":";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(519, 173);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(69, 13);
            this.label40.TabIndex = 69;
            this.label40.Text = "Debug Mode";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(95, 97);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(10, 13);
            this.label41.TabIndex = 73;
            this.label41.Text = ":";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(9, 97);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(80, 13);
            this.label42.TabIndex = 72;
            this.label42.Text = "Qty Send again";
            // 
            // chkBuffer
            // 
            this.chkBuffer.AutoSize = true;
            this.chkBuffer.Location = new System.Drawing.Point(593, 148);
            this.chkBuffer.Name = "chkBuffer";
            this.chkBuffer.Size = new System.Drawing.Size(40, 17);
            this.chkBuffer.TabIndex = 71;
            this.chkBuffer.Text = "On";
            this.chkBuffer.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtPortServer);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label20);
            this.groupBox1.Location = new System.Drawing.Point(15, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(226, 61);
            this.groupBox1.TabIndex = 74;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Port";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtIpPrinter);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.txtPortPrinter);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.txtIpMarker);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.txtPortMarker);
            this.groupBox2.Controls.Add(this.label21);
            this.groupBox2.Controls.Add(this.label22);
            this.groupBox2.Controls.Add(this.label23);
            this.groupBox2.Controls.Add(this.label24);
            this.groupBox2.Location = new System.Drawing.Point(258, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(226, 132);
            this.groupBox2.TabIndex = 75;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Printer";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label45);
            this.groupBox3.Controls.Add(this.txtSecond);
            this.groupBox3.Controls.Add(this.label43);
            this.groupBox3.Controls.Add(this.label44);
            this.groupBox3.Controls.Add(this.txtAgain);
            this.groupBox3.Controls.Add(this.txtAudit);
            this.groupBox3.Controls.Add(this.label15);
            this.groupBox3.Controls.Add(this.label17);
            this.groupBox3.Controls.Add(this.txtSendPrinter);
            this.groupBox3.Controls.Add(this.label41);
            this.groupBox3.Controls.Add(this.label18);
            this.groupBox3.Controls.Add(this.label42);
            this.groupBox3.Controls.Add(this.txtGenerate);
            this.groupBox3.Controls.Add(this.label27);
            this.groupBox3.Controls.Add(this.label28);
            this.groupBox3.Controls.Add(this.label36);
            this.groupBox3.Location = new System.Drawing.Point(15, 79);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(226, 159);
            this.groupBox3.TabIndex = 76;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "System";
            this.groupBox3.Enter += new System.EventHandler(this.groupBox3_Enter);
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(166, 125);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(44, 13);
            this.label45.TabIndex = 78;
            this.label45.Text = "Second";
            this.label45.Click += new System.EventHandler(this.label45_Click);
            // 
            // txtSecond
            // 
            this.txtSecond.Location = new System.Drawing.Point(110, 120);
            this.txtSecond.Name = "txtSecond";
            this.txtSecond.Size = new System.Drawing.Size(50, 20);
            this.txtSecond.TabIndex = 7;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(95, 123);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(10, 13);
            this.label43.TabIndex = 76;
            this.label43.Text = ":";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(9, 123);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(71, 13);
            this.label44.TabIndex = 75;
            this.label44.Text = "Check Printer";
            // 
            // txtAgain
            // 
            this.txtAgain.Location = new System.Drawing.Point(110, 94);
            this.txtAgain.Name = "txtAgain";
            this.txtAgain.Size = new System.Drawing.Size(100, 20);
            this.txtAgain.TabIndex = 6;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.txtNamaDB);
            this.groupBox4.Controls.Add(this.label10);
            this.groupBox4.Controls.Add(this.txtTimeoutDb);
            this.groupBox4.Controls.Add(this.label9);
            this.groupBox4.Controls.Add(this.txtIpDB);
            this.groupBox4.Controls.Add(this.label8);
            this.groupBox4.Controls.Add(this.txtPortDb);
            this.groupBox4.Controls.Add(this.label16);
            this.groupBox4.Controls.Add(this.label32);
            this.groupBox4.Controls.Add(this.label33);
            this.groupBox4.Controls.Add(this.label34);
            this.groupBox4.Controls.Add(this.label35);
            this.groupBox4.Location = new System.Drawing.Point(258, 150);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(226, 134);
            this.groupBox4.TabIndex = 77;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Database";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label14);
            this.groupBox5.Controls.Add(this.txtIpCamera);
            this.groupBox5.Controls.Add(this.label25);
            this.groupBox5.Controls.Add(this.label6);
            this.groupBox5.Controls.Add(this.txtIpCamera2);
            this.groupBox5.Controls.Add(this.label29);
            this.groupBox5.Location = new System.Drawing.Point(12, 244);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(229, 100);
            this.groupBox5.TabIndex = 78;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Camera";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.txtIP_PLC);
            this.groupBox6.Controls.Add(this.label48);
            this.groupBox6.Controls.Add(this.label49);
            this.groupBox6.Controls.Add(this.txtPort_PLC);
            this.groupBox6.Controls.Add(this.label46);
            this.groupBox6.Controls.Add(this.label47);
            this.groupBox6.Location = new System.Drawing.Point(15, 350);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(226, 85);
            this.groupBox6.TabIndex = 75;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Port";
            // 
            // txtIP_PLC
            // 
            this.txtIP_PLC.Location = new System.Drawing.Point(99, 19);
            this.txtIP_PLC.Name = "txtIP_PLC";
            this.txtIP_PLC.Size = new System.Drawing.Size(100, 20);
            this.txtIP_PLC.TabIndex = 46;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(7, 22);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(40, 13);
            this.label48.TabIndex = 47;
            this.label48.Text = "IP PLC";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(79, 22);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(10, 13);
            this.label49.TabIndex = 48;
            this.label49.Text = ":";
            // 
            // txtPort_PLC
            // 
            this.txtPort_PLC.Location = new System.Drawing.Point(99, 49);
            this.txtPort_PLC.Name = "txtPort_PLC";
            this.txtPort_PLC.Size = new System.Drawing.Size(100, 20);
            this.txtPort_PLC.TabIndex = 1;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(7, 48);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(49, 13);
            this.label46.TabIndex = 2;
            this.label46.Text = "Port PLC";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(79, 52);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(10, 13);
            this.label47.TabIndex = 45;
            this.label47.Text = ":";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(7, 25);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(53, 13);
            this.label50.TabIndex = 2;
            this.label50.Text = "Qty Table";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.txtQtytable);
            this.groupBox7.Controls.Add(this.label50);
            this.groupBox7.Controls.Add(this.label51);
            this.groupBox7.Location = new System.Drawing.Point(258, 298);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(226, 61);
            this.groupBox7.TabIndex = 75;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Receive Table";
            // 
            // txtQtytable
            // 
            this.txtQtytable.Location = new System.Drawing.Point(111, 22);
            this.txtQtytable.Name = "txtQtytable";
            this.txtQtytable.Size = new System.Drawing.Size(100, 20);
            this.txtQtytable.TabIndex = 1;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(95, 25);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(10, 13);
            this.label51.TabIndex = 45;
            this.label51.Text = ":";
            // 
            // config
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(490, 435);
            this.Controls.Add(this.groupBox7);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.txtPortCamera);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.chkBuffer);
            this.Controls.Add(this.txtPortCamera2);
            this.Controls.Add(this.label31);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtDelay);
            this.Controls.Add(this.label30);
            this.Controls.Add(this.label39);
            this.Controls.Add(this.label38);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label40);
            this.Controls.Add(this.chkDebug);
            this.Controls.Add(this.txtBagi);
            this.Controls.Add(this.label37);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.txtIpServer);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "config";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Configuration";
            this.Load += new System.EventHandler(this.config_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtIpServer;
        private System.Windows.Forms.TextBox txtPortServer;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtIpPrinter;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtPortPrinter;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtIpCamera;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtPortCamera;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtPortDb;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtIpDB;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtTimeoutDb;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtPortMarker;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtIpMarker;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtPortCamera2;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtIpCamera2;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtAudit;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtNamaDB;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtSendPrinter;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtGenerate;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox txtBagi;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox txtDelay;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.CheckBox chkDebug;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.CheckBox chkBuffer;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox txtAgain;
        private System.Windows.Forms.TextBox txtSecond;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TextBox txtIP_PLC;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.TextBox txtPort_PLC;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.TextBox txtQtytable;
        private System.Windows.Forms.Label label51;
    }
}