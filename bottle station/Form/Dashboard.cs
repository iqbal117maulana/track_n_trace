﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.Net.NetworkInformation;

namespace Bottle_Station
{
    public partial class Dashboard : Form
    {
        dbaccess db;
        public string[] linenumber;
        public string admin;
        public DataTable table = new DataTable();
        ListBatchNumber lbn;
        public string param;
        Control ctrl = new Control();

        public Dashboard(string adminid, string[] line)
        {
            InitializeComponent();
            this.Text = this.Text + " V " + System.Windows.Forms.Application.ProductVersion;
            db = new dbaccess();
            db.adminid = adminid;
            admin = adminid;
            db.from = "Bottle Station";
            linenumber = line;
            lblCountBatchPentabio.Text = GetOpenBatch(linenumber[1], "('03.003', '03.004')").Count.ToString();
            lblCountBatchFlubio.Text = GetOpenBatch(linenumber[1], "('03.005')").Count.ToString();    
        }

        private void Dashboard_Load(object sender, EventArgs e)
        {
            timer1.Start();
            lblTime.Text = DateTime.Now.ToLongTimeString();
            lblDate.Text = DateTime.Now.ToShortDateString();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            lblTime.Text = DateTime.Now.ToLongTimeString();
            timer1.Start();
        }

        private void Dashboard_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        //tambahan
        public List<string[]> GetOpenBatch(string line, string product)
        {
            List<string> field = new List<string>();
            field.Add("batchNumber,productModelId");
            string where = "status = '1' and linepackagingid = '" + line + "' and productModelId IN " + product + "";
            return db.selectList(field, "[packaging_order]", where);
        }
        //end tambahan

        private void pnlBatchPentabio_MouseEnter(object sender, EventArgs e)
        {
            this.pnlBatchPentabio.BackColor = ColorTranslator.FromHtml("#cccccc");
        }

        private void pnlBatchPentabio_MouseLeave(object sender, EventArgs e)
        {
            this.pnlBatchPentabio.BackColor = ColorTranslator.FromHtml("#ffffff");
        }

        private void pnlBatchPentabio_DoubleClick(object sender, EventArgs e)
        {
            lbn = new ListBatchNumber(this, linenumber[1], "('03.003', '03.004')", "dashboard pentabio");
            lbn.ShowDialog();
        }

        private void btnPentabio_Click(object sender, EventArgs e)
        {
            
            BottleStation bl = new BottleStation(this, admin, linenumber, "('03.003', '03.004')", ctrl);
            bl.lblUserId.Text = lblUserId.Text;
            bl.lblRole.Text = lblRole.Text;
            bl.btnAudit.Enabled = false;
            bl.btnStart.Enabled = false;
            bl.btnStartGs.Enabled = false;
            bl.Show();
            this.Hide();
        }

        private void btnReport_Click(object sender, EventArgs e)
        {
            ReportVaccine rp = new ReportVaccine(this, linenumber[1]);
            rp.lblUserId.Text = lblUserId.Text;
            rp.lblRole.Text = lblRole.Text;
            rp.Show();
            this.Hide();
        }

        private void pbPentabio_DoubleClick(object sender, EventArgs e)
        {
            lbn = new ListBatchNumber(this, linenumber[1], "('03.003', '03.004')", "dashboard pentabio");
            lbn.ShowDialog();
        }

        private void lblPentabio_DoubleClick(object sender, EventArgs e)
        {
            lbn = new ListBatchNumber(this, linenumber[1], "('03.003', '03.004')", "dashboard pentabio");
            lbn.ShowDialog();
        }

        private void lblCountBatchPentabio_DoubleClick(object sender, EventArgs e)
        {
            lbn = new ListBatchNumber(this, linenumber[1], "('03.003', '03.004')", "dashboard pentabio");
            lbn.ShowDialog();
        }

        private void pbPentabio_MouseEnter(object sender, EventArgs e)
        {
            this.pnlBatchPentabio.BackColor = ColorTranslator.FromHtml("#cccccc");
        }

        private void lblPentabio_MouseEnter(object sender, EventArgs e)
        {
            this.pnlBatchPentabio.BackColor = ColorTranslator.FromHtml("#cccccc");
        }

        private void lblCountBatchPentabio_MouseEnter(object sender, EventArgs e)
        {
            this.pnlBatchPentabio.BackColor = ColorTranslator.FromHtml("#cccccc");
        }

        private void pbPentabio_MouseLeave(object sender, EventArgs e)
        {
            this.pnlBatchPentabio.BackColor = ColorTranslator.FromHtml("#ffffff");
        }

        private void lblPentabio_MouseLeave(object sender, EventArgs e)
        {
            this.pnlBatchPentabio.BackColor = ColorTranslator.FromHtml("#ffffff");
        }

        private void lblCountBatchPentabio_MouseLeave(object sender, EventArgs e)
        {
            this.pnlBatchPentabio.BackColor = ColorTranslator.FromHtml("#ffffff");
        }

        private void pnlBatchFlubio_DoubleClick(object sender, EventArgs e)
        {
            lbn = new ListBatchNumber(this, linenumber[1], "('03.005')", "dashboard flubio");
            lbn.ShowDialog();
        }

        private void pbFlubio_DoubleClick(object sender, EventArgs e)
        {
            lbn = new ListBatchNumber(this, linenumber[1], "('03.005')", "dashboard flubio");
            lbn.ShowDialog();
        }

        private void lblFlubio_DoubleClick(object sender, EventArgs e)
        {
            lbn = new ListBatchNumber(this, linenumber[1], "('03.005')", "dashboard flubio");
            lbn.ShowDialog();
        }

        private void lblCountBatchFlubio_DoubleClick(object sender, EventArgs e)
        {
            lbn = new ListBatchNumber(this, linenumber[1], "('03.005')", "dashboard flubio");
            lbn.ShowDialog();
        }

        private void pnlBatchFlubio_MouseEnter(object sender, EventArgs e)
        {
            this.pnlBatchFlubio.BackColor = ColorTranslator.FromHtml("#cccccc");
        }

        private void pbFlubio_MouseEnter(object sender, EventArgs e)
        {
            this.pnlBatchFlubio.BackColor = ColorTranslator.FromHtml("#cccccc");
        }

        private void lblFlubio_MouseEnter(object sender, EventArgs e)
        {
            this.pnlBatchFlubio.BackColor = ColorTranslator.FromHtml("#cccccc");
        }

        private void lblCountBatchFlubio_MouseEnter(object sender, EventArgs e)
        {
            this.pnlBatchFlubio.BackColor = ColorTranslator.FromHtml("#cccccc");
        }

        private void pnlBatchFlubio_MouseLeave(object sender, EventArgs e)
        {
            this.pnlBatchFlubio.BackColor = ColorTranslator.FromHtml("#ffffff");
        }

        private void pbFlubio_MouseLeave(object sender, EventArgs e)
        {
            this.pnlBatchFlubio.BackColor = ColorTranslator.FromHtml("#ffffff");
        }

        private void lblFlubio_MouseLeave(object sender, EventArgs e)
        {
            this.pnlBatchFlubio.BackColor = ColorTranslator.FromHtml("#ffffff");
        }

        private void lblCountBatchFlubio_MouseLeave(object sender, EventArgs e)
        {
            this.pnlBatchFlubio.BackColor = ColorTranslator.FromHtml("#ffffff");
        }

        private void btnFlubio_Click(object sender, EventArgs e)
        {
            BottleStationFlubio bl = new BottleStationFlubio(this, admin, linenumber, "('03.005')");
            bl.lblUserId.Text = lblUserId.Text;
            bl.lblRole.Text = lblRole.Text;
            bl.btnAudit.Enabled = false;
            bl.btnStart.Enabled = false;
            bl.btnStartGs.Enabled = false;
            bl.Show();
            this.Hide();
        }
    }
}

