﻿namespace Bottle_Station
{
    partial class Dashboard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Dashboard));
            this.label17 = new System.Windows.Forms.Label();
            this.lblUserId = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblDate = new System.Windows.Forms.Label();
            this.lblTime = new System.Windows.Forms.Label();
            this.lblRole = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pnlBatchFlubio = new System.Windows.Forms.Panel();
            this.lblCountBatchFlubio = new System.Windows.Forms.Label();
            this.lblFlubio = new System.Windows.Forms.Label();
            this.pbFlubio = new System.Windows.Forms.PictureBox();
            this.pnlBatchPentabio = new System.Windows.Forms.Panel();
            this.lblCountBatchPentabio = new System.Windows.Forms.Label();
            this.lblPentabio = new System.Windows.Forms.Label();
            this.pbPentabio = new System.Windows.Forms.PictureBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btnFlubio = new System.Windows.Forms.Button();
            this.btnReport = new System.Windows.Forms.Button();
            this.btnPentabio = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel2.SuspendLayout();
            this.pnlBatchFlubio.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbFlubio)).BeginInit();
            this.pnlBatchPentabio.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbPentabio)).BeginInit();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // label17
            // 
            this.label17.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.White;
            this.label17.Location = new System.Drawing.Point(494, 20);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(184, 37);
            this.label17.TabIndex = 94;
            this.label17.Text = "Dashboard";
            // 
            // lblUserId
            // 
            this.lblUserId.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblUserId.AutoSize = true;
            this.lblUserId.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUserId.ForeColor = System.Drawing.Color.White;
            this.lblUserId.Location = new System.Drawing.Point(930, 24);
            this.lblUserId.Name = "lblUserId";
            this.lblUserId.Size = new System.Drawing.Size(57, 20);
            this.lblUserId.TabIndex = 102;
            this.lblUserId.Text = "admin";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(148)))), ((int)(((byte)(172)))));
            this.panel1.Controls.Add(this.lblDate);
            this.panel1.Controls.Add(this.lblTime);
            this.panel1.Controls.Add(this.lblRole);
            this.panel1.Controls.Add(this.pictureBox2);
            this.panel1.Controls.Add(this.lblUserId);
            this.panel1.Controls.Add(this.label17);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1024, 86);
            this.panel1.TabIndex = 114;
            // 
            // lblDate
            // 
            this.lblDate.AutoSize = true;
            this.lblDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDate.ForeColor = System.Drawing.Color.White;
            this.lblDate.Location = new System.Drawing.Point(216, 38);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(33, 15);
            this.lblDate.TabIndex = 105;
            this.lblDate.Text = "Date";
            // 
            // lblTime
            // 
            this.lblTime.AutoSize = true;
            this.lblTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTime.ForeColor = System.Drawing.Color.White;
            this.lblTime.Location = new System.Drawing.Point(216, 20);
            this.lblTime.Name = "lblTime";
            this.lblTime.Size = new System.Drawing.Size(39, 15);
            this.lblTime.TabIndex = 104;
            this.lblTime.Text = "Time";
            // 
            // lblRole
            // 
            this.lblRole.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblRole.AutoSize = true;
            this.lblRole.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRole.ForeColor = System.Drawing.Color.White;
            this.lblRole.Location = new System.Drawing.Point(931, 44);
            this.lblRole.Name = "lblRole";
            this.lblRole.Size = new System.Drawing.Size(48, 18);
            this.lblRole.TabIndex = 103;
            this.lblRole.Text = "admin";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::Bottle_Station.Properties.Resources.user;
            this.pictureBox2.Location = new System.Drawing.Point(879, 20);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(45, 45);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox2.TabIndex = 96;
            this.pictureBox2.TabStop = false;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(47)))), ((int)(((byte)(54)))));
            this.panel3.Controls.Add(this.pictureBox1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(210, 86);
            this.panel3.TabIndex = 95;
            // 
            // pictureBox1
            // 
            this.pictureBox1.ImageLocation = "logo_biofarma.png";
            this.pictureBox1.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.InitialImage")));
            this.pictureBox1.Location = new System.Drawing.Point(30, 5);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(150, 75);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 35;
            this.pictureBox1.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.pnlBatchFlubio);
            this.panel2.Controls.Add(this.pnlBatchPentabio);
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 86);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1024, 654);
            this.panel2.TabIndex = 115;
            // 
            // pnlBatchFlubio
            // 
            this.pnlBatchFlubio.BackColor = System.Drawing.Color.White;
            this.pnlBatchFlubio.Controls.Add(this.lblCountBatchFlubio);
            this.pnlBatchFlubio.Controls.Add(this.lblFlubio);
            this.pnlBatchFlubio.Controls.Add(this.pbFlubio);
            this.pnlBatchFlubio.Enabled = false;
            this.pnlBatchFlubio.Location = new System.Drawing.Point(634, 33);
            this.pnlBatchFlubio.Name = "pnlBatchFlubio";
            this.pnlBatchFlubio.Size = new System.Drawing.Size(363, 186);
            this.pnlBatchFlubio.TabIndex = 108;
            this.pnlBatchFlubio.DoubleClick += new System.EventHandler(this.pnlBatchFlubio_DoubleClick);
            this.pnlBatchFlubio.MouseLeave += new System.EventHandler(this.pnlBatchFlubio_MouseLeave);
            this.pnlBatchFlubio.MouseEnter += new System.EventHandler(this.pnlBatchFlubio_MouseEnter);
            // 
            // lblCountBatchFlubio
            // 
            this.lblCountBatchFlubio.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblCountBatchFlubio.AutoSize = true;
            this.lblCountBatchFlubio.Enabled = false;
            this.lblCountBatchFlubio.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCountBatchFlubio.ForeColor = System.Drawing.Color.Black;
            this.lblCountBatchFlubio.Location = new System.Drawing.Point(110, 63);
            this.lblCountBatchFlubio.Name = "lblCountBatchFlubio";
            this.lblCountBatchFlubio.Size = new System.Drawing.Size(36, 37);
            this.lblCountBatchFlubio.TabIndex = 107;
            this.lblCountBatchFlubio.Text = "0";
            this.lblCountBatchFlubio.MouseLeave += new System.EventHandler(this.lblCountBatchFlubio_MouseLeave);
            this.lblCountBatchFlubio.DoubleClick += new System.EventHandler(this.lblCountBatchFlubio_DoubleClick);
            this.lblCountBatchFlubio.MouseEnter += new System.EventHandler(this.lblCountBatchFlubio_MouseEnter);
            // 
            // lblFlubio
            // 
            this.lblFlubio.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblFlubio.AutoSize = true;
            this.lblFlubio.Enabled = false;
            this.lblFlubio.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFlubio.ForeColor = System.Drawing.Color.Black;
            this.lblFlubio.Location = new System.Drawing.Point(20, 115);
            this.lblFlubio.Name = "lblFlubio";
            this.lblFlubio.Size = new System.Drawing.Size(209, 24);
            this.lblFlubio.TabIndex = 106;
            this.lblFlubio.Text = "Opened Batch Flubio";
            this.lblFlubio.MouseLeave += new System.EventHandler(this.lblFlubio_MouseLeave);
            this.lblFlubio.DoubleClick += new System.EventHandler(this.lblFlubio_DoubleClick);
            this.lblFlubio.MouseEnter += new System.EventHandler(this.lblFlubio_MouseEnter);
            // 
            // pbFlubio
            // 
            this.pbFlubio.Enabled = false;
            this.pbFlubio.Image = global::Bottle_Station.Properties.Resources.batch_flbio;
            this.pbFlubio.Location = new System.Drawing.Point(241, 40);
            this.pbFlubio.Name = "pbFlubio";
            this.pbFlubio.Size = new System.Drawing.Size(113, 99);
            this.pbFlubio.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pbFlubio.TabIndex = 0;
            this.pbFlubio.TabStop = false;
            this.pbFlubio.DoubleClick += new System.EventHandler(this.pbFlubio_DoubleClick);
            this.pbFlubio.MouseLeave += new System.EventHandler(this.pbFlubio_MouseLeave);
            this.pbFlubio.MouseEnter += new System.EventHandler(this.pbFlubio_MouseEnter);
            // 
            // pnlBatchPentabio
            // 
            this.pnlBatchPentabio.BackColor = System.Drawing.Color.White;
            this.pnlBatchPentabio.Controls.Add(this.lblCountBatchPentabio);
            this.pnlBatchPentabio.Controls.Add(this.lblPentabio);
            this.pnlBatchPentabio.Controls.Add(this.pbPentabio);
            this.pnlBatchPentabio.Location = new System.Drawing.Point(239, 33);
            this.pnlBatchPentabio.Name = "pnlBatchPentabio";
            this.pnlBatchPentabio.Size = new System.Drawing.Size(363, 186);
            this.pnlBatchPentabio.TabIndex = 1;
            this.pnlBatchPentabio.DoubleClick += new System.EventHandler(this.pnlBatchPentabio_DoubleClick);
            this.pnlBatchPentabio.MouseLeave += new System.EventHandler(this.pnlBatchPentabio_MouseLeave);
            this.pnlBatchPentabio.MouseEnter += new System.EventHandler(this.pnlBatchPentabio_MouseEnter);
            // 
            // lblCountBatchPentabio
            // 
            this.lblCountBatchPentabio.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblCountBatchPentabio.AutoSize = true;
            this.lblCountBatchPentabio.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCountBatchPentabio.ForeColor = System.Drawing.Color.Black;
            this.lblCountBatchPentabio.Location = new System.Drawing.Point(110, 63);
            this.lblCountBatchPentabio.Name = "lblCountBatchPentabio";
            this.lblCountBatchPentabio.Size = new System.Drawing.Size(36, 37);
            this.lblCountBatchPentabio.TabIndex = 107;
            this.lblCountBatchPentabio.Text = "0";
            this.lblCountBatchPentabio.MouseLeave += new System.EventHandler(this.lblCountBatchPentabio_MouseLeave);
            this.lblCountBatchPentabio.DoubleClick += new System.EventHandler(this.lblCountBatchPentabio_DoubleClick);
            this.lblCountBatchPentabio.MouseEnter += new System.EventHandler(this.lblCountBatchPentabio_MouseEnter);
            // 
            // lblPentabio
            // 
            this.lblPentabio.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblPentabio.AutoSize = true;
            this.lblPentabio.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPentabio.ForeColor = System.Drawing.Color.Black;
            this.lblPentabio.Location = new System.Drawing.Point(3, 115);
            this.lblPentabio.Name = "lblPentabio";
            this.lblPentabio.Size = new System.Drawing.Size(232, 24);
            this.lblPentabio.TabIndex = 106;
            this.lblPentabio.Text = "Opened Batch Pentabio";
            this.lblPentabio.MouseLeave += new System.EventHandler(this.lblPentabio_MouseLeave);
            this.lblPentabio.DoubleClick += new System.EventHandler(this.lblPentabio_DoubleClick);
            this.lblPentabio.MouseEnter += new System.EventHandler(this.lblPentabio_MouseEnter);
            // 
            // pbPentabio
            // 
            this.pbPentabio.Image = global::Bottle_Station.Properties.Resources.batch_pentabio;
            this.pbPentabio.Location = new System.Drawing.Point(241, 40);
            this.pbPentabio.Name = "pbPentabio";
            this.pbPentabio.Size = new System.Drawing.Size(113, 99);
            this.pbPentabio.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pbPentabio.TabIndex = 0;
            this.pbPentabio.TabStop = false;
            this.pbPentabio.DoubleClick += new System.EventHandler(this.pbPentabio_DoubleClick);
            this.pbPentabio.MouseLeave += new System.EventHandler(this.pbPentabio_MouseLeave);
            this.pbPentabio.MouseEnter += new System.EventHandler(this.pbPentabio_MouseEnter);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(47)))), ((int)(((byte)(54)))));
            this.panel4.Controls.Add(this.btnFlubio);
            this.panel4.Controls.Add(this.btnReport);
            this.panel4.Controls.Add(this.btnPentabio);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(210, 654);
            this.panel4.TabIndex = 0;
            // 
            // btnFlubio
            // 
            this.btnFlubio.Enabled = false;
            this.btnFlubio.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(47)))), ((int)(((byte)(54)))));
            this.btnFlubio.FlatAppearance.BorderSize = 0;
            this.btnFlubio.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(80)))), ((int)(((byte)(94)))));
            this.btnFlubio.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(80)))), ((int)(((byte)(94)))));
            this.btnFlubio.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFlubio.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFlubio.ForeColor = System.Drawing.Color.White;
            this.btnFlubio.Image = global::Bottle_Station.Properties.Resources.vial;
            this.btnFlubio.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnFlubio.Location = new System.Drawing.Point(0, 129);
            this.btnFlubio.Name = "btnFlubio";
            this.btnFlubio.Size = new System.Drawing.Size(210, 123);
            this.btnFlubio.TabIndex = 2;
            this.btnFlubio.Text = "Flubio";
            this.btnFlubio.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnFlubio.UseVisualStyleBackColor = true;
            this.btnFlubio.Click += new System.EventHandler(this.btnFlubio_Click);
            // 
            // btnReport
            // 
            this.btnReport.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(47)))), ((int)(((byte)(54)))));
            this.btnReport.FlatAppearance.BorderSize = 0;
            this.btnReport.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(80)))), ((int)(((byte)(94)))));
            this.btnReport.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(80)))), ((int)(((byte)(94)))));
            this.btnReport.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReport.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReport.ForeColor = System.Drawing.Color.White;
            this.btnReport.Image = global::Bottle_Station.Properties.Resources.report_vial;
            this.btnReport.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnReport.Location = new System.Drawing.Point(0, 258);
            this.btnReport.Name = "btnReport";
            this.btnReport.Size = new System.Drawing.Size(210, 123);
            this.btnReport.TabIndex = 1;
            this.btnReport.Text = "Report";
            this.btnReport.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnReport.UseVisualStyleBackColor = true;
            this.btnReport.Click += new System.EventHandler(this.btnReport_Click);
            // 
            // btnPentabio
            // 
            this.btnPentabio.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(47)))), ((int)(((byte)(54)))));
            this.btnPentabio.FlatAppearance.BorderSize = 0;
            this.btnPentabio.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(80)))), ((int)(((byte)(94)))));
            this.btnPentabio.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(80)))), ((int)(((byte)(94)))));
            this.btnPentabio.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPentabio.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPentabio.ForeColor = System.Drawing.Color.White;
            this.btnPentabio.Image = global::Bottle_Station.Properties.Resources.vial;
            this.btnPentabio.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnPentabio.Location = new System.Drawing.Point(0, 0);
            this.btnPentabio.Name = "btnPentabio";
            this.btnPentabio.Size = new System.Drawing.Size(210, 123);
            this.btnPentabio.TabIndex = 0;
            this.btnPentabio.Text = "Pentabio";
            this.btnPentabio.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnPentabio.UseVisualStyleBackColor = true;
            this.btnPentabio.Click += new System.EventHandler(this.btnPentabio_Click);
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // Dashboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1024, 740);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Dashboard";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Bottle Station";
            this.Load += new System.EventHandler(this.Dashboard_Load);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Dashboard_FormClosed);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel2.ResumeLayout(false);
            this.pnlBatchFlubio.ResumeLayout(false);
            this.pnlBatchFlubio.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbFlubio)).EndInit();
            this.pnlBatchPentabio.ResumeLayout(false);
            this.pnlBatchPentabio.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbPentabio)).EndInit();
            this.panel4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.PictureBox pictureBox1;
        public System.Windows.Forms.Label label17;
        public System.Windows.Forms.Label lblUserId;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.PictureBox pictureBox2;
        public System.Windows.Forms.Label lblRole;
        private System.Windows.Forms.Label lblDate;
        private System.Windows.Forms.Label lblTime;
        private System.Windows.Forms.Button btnPentabio;
        private System.Windows.Forms.Button btnReport;
        private System.Windows.Forms.Panel pnlBatchPentabio;
        private System.Windows.Forms.PictureBox pbPentabio;
        private System.Windows.Forms.Timer timer1;
        public System.Windows.Forms.Label lblCountBatchPentabio;
        public System.Windows.Forms.Label lblPentabio;
        private System.Windows.Forms.Button btnFlubio;
        private System.Windows.Forms.Panel pnlBatchFlubio;
        public System.Windows.Forms.Label lblCountBatchFlubio;
        public System.Windows.Forms.Label lblFlubio;
        private System.Windows.Forms.PictureBox pbFlubio;
    }
}

