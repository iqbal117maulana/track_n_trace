﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Cold_Storage
{
    public partial class config : Form
    {
        public config()
        {
            InitializeComponent();
            getConfig();
        }

        void getConfig()
        {
            try
            {
                sqlitecs sql = new sqlitecs();
                txtIpDb.Text = sql.config["ipDB"];
                txtPortDb.Text = sql.config["portDB"];
                txtNamaDb.Text = sql.config["namadb"];
                txtNamaDb2.Text = sql.config["namadb2"];
            }
            catch (KeyNotFoundException k)
            {

            }
        }
        private bool validasi()
        {
            if (txtIpDb.Text.Length > 0 && dbaccess.cekIP(txtIpDb.Text))
            {
                return true;
            }
            return false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Confirm cf = new Confirm("Are you sure? ", "Confirmation");
            bool hasilDialog = cf.conf();
            if (hasilDialog)
            {
                if (validasi())
                {
                    sqlitecs sql = new sqlitecs();
                    Dictionary<string, string> field = new Dictionary<string, string>();
                    field.Add("ipDB", txtIpDb.Text);
                    field.Add("portDB", txtPortDb.Text);
                    field.Add("namadb", txtNamaDb.Text);
                    field.Add("namadb2", txtNamaDb2.Text);
                    sql.update(field, "config", "");
                    Confirm cf4 = new Confirm("Configuration success saved", "Information", MessageBoxButtons.OK);
                    this.Dispose();
                }
                else
                {
                    new Confirm("Invalid configuration data", "Information", MessageBoxButtons.OK);
                }
            }
        }
        private bool cekKosong()
        {
            if (txtIpDb.Text.Length == 0)
                return false;
            if (txtNamaDb.Text.Length == 0)
                return false;
            if (txtNamaDb2.Text.Length == 0)
                return false;
            if (txtPortDb.Text.Length == 0)
                return false;
            return true;
        }

        private void config_Load(object sender, EventArgs e)
        {

        }
    }
}
