﻿namespace Cold_Storage
{
    partial class Cold
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Cold));
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.dgvBatch = new System.Windows.Forms.DataGridView();
            this.BATCH = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PRODUCT_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QTY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UOM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DETAIL_NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QTY_SCANNED = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lblInnerBox = new System.Windows.Forms.Label();
            this.lblVial = new System.Windows.Forms.Label();
            this.dgvOutbound = new System.Windows.Forms.DataGridView();
            this.TYPE_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PRODUCT_NAME_SC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EXPIRE_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QTY_SC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UOM_SC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DETAIL_NO_SC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BATCH_SC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PICKINGLIST_SC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BTN_DELETE = new System.Windows.Forms.DataGridViewButtonColumn();
            this.lblBasket = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtPickingList = new System.Windows.Forms.TextBox();
            this.txtOutbound = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.lbladmin = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txtLog1 = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblRole = new System.Windows.Forms.Label();
            this.lblUserId = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.btnPending = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.btnOpenLog = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.btnConfig = new System.Windows.Forms.Button();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBatch)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvOutbound)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.Location = new System.Drawing.Point(15, 48);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.groupBox6);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.groupBox2);
            this.splitContainer1.Size = new System.Drawing.Size(1202, 505);
            this.splitContainer1.SplitterDistance = 588;
            this.splitContainer1.SplitterWidth = 8;
            this.splitContainer1.TabIndex = 9;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.dgvBatch);
            this.groupBox6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox6.Location = new System.Drawing.Point(0, 0);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(588, 505);
            this.groupBox6.TabIndex = 9;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Batch";
            // 
            // dgvBatch
            // 
            this.dgvBatch.AllowUserToAddRows = false;
            this.dgvBatch.AllowUserToDeleteRows = false;
            this.dgvBatch.AllowUserToResizeColumns = false;
            this.dgvBatch.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dgvBatch.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvBatch.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvBatch.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvBatch.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvBatch.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.BATCH,
            this.PRODUCT_NAME,
            this.QTY,
            this.UOM,
            this.DETAIL_NO,
            this.QTY_SCANNED});
            this.dgvBatch.Location = new System.Drawing.Point(10, 26);
            this.dgvBatch.Name = "dgvBatch";
            this.dgvBatch.ReadOnly = true;
            this.dgvBatch.RowHeadersVisible = false;
            this.dgvBatch.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvBatch.Size = new System.Drawing.Size(567, 439);
            this.dgvBatch.TabIndex = 8;
            this.dgvBatch.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvBatch_CellClick);
            this.dgvBatch.SelectionChanged += new System.EventHandler(this.dgvBatch_SelectionChanged);
            // 
            // BATCH
            // 
            this.BATCH.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.BATCH.DataPropertyName = "BATCH";
            this.BATCH.HeaderText = "Batch";
            this.BATCH.Name = "BATCH";
            this.BATCH.ReadOnly = true;
            this.BATCH.Width = 76;
            // 
            // PRODUCT_NAME
            // 
            this.PRODUCT_NAME.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.PRODUCT_NAME.DataPropertyName = "PRODUCT_NAME";
            this.PRODUCT_NAME.HeaderText = "Product Name";
            this.PRODUCT_NAME.Name = "PRODUCT_NAME";
            this.PRODUCT_NAME.ReadOnly = true;
            this.PRODUCT_NAME.Width = 135;
            // 
            // QTY
            // 
            this.QTY.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.QTY.DataPropertyName = "QTY";
            this.QTY.HeaderText = "Qty";
            this.QTY.Name = "QTY";
            this.QTY.ReadOnly = true;
            this.QTY.Width = 58;
            // 
            // UOM
            // 
            this.UOM.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.UOM.DataPropertyName = "UOM";
            this.UOM.HeaderText = "Uom";
            this.UOM.Name = "UOM";
            this.UOM.ReadOnly = true;
            this.UOM.Width = 68;
            // 
            // DETAIL_NO
            // 
            this.DETAIL_NO.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.DETAIL_NO.DataPropertyName = "DETAIL_NO";
            this.DETAIL_NO.HeaderText = "DETAIL_NO";
            this.DETAIL_NO.Name = "DETAIL_NO";
            this.DETAIL_NO.ReadOnly = true;
            this.DETAIL_NO.Visible = false;
            // 
            // QTY_SCANNED
            // 
            this.QTY_SCANNED.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.QTY_SCANNED.DataPropertyName = "QTY_SCANNED";
            this.QTY_SCANNED.HeaderText = "Qty Scanned";
            this.QTY_SCANNED.Name = "QTY_SCANNED";
            this.QTY_SCANNED.ReadOnly = true;
            this.QTY_SCANNED.Width = 126;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lblInnerBox);
            this.groupBox2.Controls.Add(this.lblVial);
            this.groupBox2.Controls.Add(this.dgvOutbound);
            this.groupBox2.Controls.Add(this.lblBasket);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(0, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(606, 505);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Scanned";
            // 
            // lblInnerBox
            // 
            this.lblInnerBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblInnerBox.AutoSize = true;
            this.lblInnerBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInnerBox.Location = new System.Drawing.Point(205, 476);
            this.lblInnerBox.Name = "lblInnerBox";
            this.lblInnerBox.Size = new System.Drawing.Size(125, 20);
            this.lblInnerBox.TabIndex = 14;
            this.lblInnerBox.Text = "Inner Box : 1000";
            // 
            // lblVial
            // 
            this.lblVial.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblVial.AutoSize = true;
            this.lblVial.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVial.Location = new System.Drawing.Point(410, 476);
            this.lblVial.Name = "lblVial";
            this.lblVial.Size = new System.Drawing.Size(83, 20);
            this.lblVial.TabIndex = 13;
            this.lblVial.Text = "Vial : 1000";
            // 
            // dgvOutbound
            // 
            this.dgvOutbound.AllowUserToAddRows = false;
            this.dgvOutbound.AllowUserToDeleteRows = false;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dgvOutbound.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvOutbound.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvOutbound.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvOutbound.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvOutbound.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.TYPE_ID,
            this.PRODUCT_NAME_SC,
            this.EXPIRE_DATE,
            this.QTY_SC,
            this.UOM_SC,
            this.ID,
            this.DETAIL_NO_SC,
            this.BATCH_SC,
            this.PICKINGLIST_SC,
            this.BTN_DELETE});
            this.dgvOutbound.Location = new System.Drawing.Point(11, 26);
            this.dgvOutbound.Name = "dgvOutbound";
            this.dgvOutbound.ReadOnly = true;
            this.dgvOutbound.RowHeadersVisible = false;
            this.dgvOutbound.Size = new System.Drawing.Size(583, 439);
            this.dgvOutbound.TabIndex = 7;
            this.dgvOutbound.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgvOutbound_CellFormatting);
            this.dgvOutbound.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dgvOutbound_RowsAdded);
            this.dgvOutbound.SelectionChanged += new System.EventHandler(this.dataGridView2_SelectionChanged);
            this.dgvOutbound.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvOutbound_CellContentClick);
            // 
            // TYPE_ID
            // 
            this.TYPE_ID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.TYPE_ID.DataPropertyName = "TYPE_ID";
            this.TYPE_ID.HeaderText = "Type ID";
            this.TYPE_ID.Name = "TYPE_ID";
            this.TYPE_ID.ReadOnly = true;
            this.TYPE_ID.Width = 89;
            // 
            // PRODUCT_NAME_SC
            // 
            this.PRODUCT_NAME_SC.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.PRODUCT_NAME_SC.DataPropertyName = "PRODUCT_NAME_SC";
            this.PRODUCT_NAME_SC.HeaderText = "Product Name";
            this.PRODUCT_NAME_SC.Name = "PRODUCT_NAME_SC";
            this.PRODUCT_NAME_SC.ReadOnly = true;
            this.PRODUCT_NAME_SC.Width = 135;
            // 
            // EXPIRE_DATE
            // 
            this.EXPIRE_DATE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.EXPIRE_DATE.DataPropertyName = "EXPIRE_DATE";
            this.EXPIRE_DATE.HeaderText = "Expire Date";
            this.EXPIRE_DATE.Name = "EXPIRE_DATE";
            this.EXPIRE_DATE.ReadOnly = true;
            this.EXPIRE_DATE.Width = 117;
            // 
            // QTY_SC
            // 
            this.QTY_SC.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.QTY_SC.DataPropertyName = "QTY_SC";
            this.QTY_SC.HeaderText = "Qty";
            this.QTY_SC.Name = "QTY_SC";
            this.QTY_SC.ReadOnly = true;
            this.QTY_SC.Width = 58;
            // 
            // UOM_SC
            // 
            this.UOM_SC.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.UOM_SC.DataPropertyName = "UOM_SC";
            this.UOM_SC.HeaderText = "Uom";
            this.UOM_SC.Name = "UOM_SC";
            this.UOM_SC.ReadOnly = true;
            this.UOM_SC.Width = 68;
            // 
            // ID
            // 
            this.ID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.ID.DataPropertyName = "ID";
            this.ID.HeaderText = "ID";
            this.ID.Name = "ID";
            this.ID.ReadOnly = true;
            this.ID.Width = 51;
            // 
            // DETAIL_NO_SC
            // 
            this.DETAIL_NO_SC.DataPropertyName = "DETAIL_NO_SC";
            this.DETAIL_NO_SC.HeaderText = "DETAIL_NO_SC";
            this.DETAIL_NO_SC.Name = "DETAIL_NO_SC";
            this.DETAIL_NO_SC.ReadOnly = true;
            this.DETAIL_NO_SC.Visible = false;
            // 
            // BATCH_SC
            // 
            this.BATCH_SC.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.BATCH_SC.DataPropertyName = "BATCH_SC";
            this.BATCH_SC.HeaderText = "BATCH_SC";
            this.BATCH_SC.Name = "BATCH_SC";
            this.BATCH_SC.ReadOnly = true;
            this.BATCH_SC.Visible = false;
            // 
            // PICKINGLIST_SC
            // 
            this.PICKINGLIST_SC.DataPropertyName = "PICKINGLIST_SC";
            this.PICKINGLIST_SC.HeaderText = "PICKINGLIST_SC";
            this.PICKINGLIST_SC.Name = "PICKINGLIST_SC";
            this.PICKINGLIST_SC.ReadOnly = true;
            this.PICKINGLIST_SC.Visible = false;
            // 
            // BTN_DELETE
            // 
            this.BTN_DELETE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.BTN_DELETE.HeaderText = "Action";
            this.BTN_DELETE.Name = "BTN_DELETE";
            this.BTN_DELETE.ReadOnly = true;
            this.BTN_DELETE.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.BTN_DELETE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.BTN_DELETE.Width = 79;
            // 
            // lblBasket
            // 
            this.lblBasket.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblBasket.AutoSize = true;
            this.lblBasket.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBasket.Location = new System.Drawing.Point(10, 476);
            this.lblBasket.Name = "lblBasket";
            this.lblBasket.Size = new System.Drawing.Size(107, 20);
            this.lblBasket.TabIndex = 11;
            this.lblBasket.Text = "Basket : 1000";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(148, 20);
            this.label2.TabIndex = 26;
            this.label2.Text = "Picking List Number";
            // 
            // txtPickingList
            // 
            this.txtPickingList.Location = new System.Drawing.Point(165, 16);
            this.txtPickingList.Name = "txtPickingList";
            this.txtPickingList.ReadOnly = true;
            this.txtPickingList.Size = new System.Drawing.Size(171, 26);
            this.txtPickingList.TabIndex = 24;
            this.txtPickingList.Click += new System.EventHandler(this.txtPickingList_Click);
            // 
            // txtOutbound
            // 
            this.txtOutbound.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtOutbound.Location = new System.Drawing.Point(128, 573);
            this.txtOutbound.Name = "txtOutbound";
            this.txtOutbound.Size = new System.Drawing.Size(1089, 26);
            this.txtOutbound.TabIndex = 20;
            this.txtOutbound.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtOutbound_KeyDown);
            this.txtOutbound.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtOutbound_KeyPress);
            // 
            // label14
            // 
            this.label14.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.White;
            this.label14.Location = new System.Drawing.Point(549, 41);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(315, 29);
            this.label14.TabIndex = 35;
            this.label14.Text = "DISTRIBUTION STATION";
            // 
            // label21
            // 
            this.label21.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.White;
            this.label21.Location = new System.Drawing.Point(541, 12);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(331, 29);
            this.label21.TabIndex = 36;
            this.label21.Text = "FINISH GOODS CONTROL";
            // 
            // lbladmin
            // 
            this.lbladmin.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbladmin.AutoSize = true;
            this.lbladmin.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbladmin.Location = new System.Drawing.Point(1115, 39);
            this.lbladmin.Name = "lbladmin";
            this.lbladmin.Size = new System.Drawing.Size(84, 29);
            this.lbladmin.TabIndex = 37;
            this.lbladmin.Text = "admin";
            this.lbladmin.Visible = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.txtLog1);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(1254, 229);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(149, 114);
            this.groupBox3.TabIndex = 122;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Log";
            this.groupBox3.Visible = false;
            // 
            // txtLog1
            // 
            this.txtLog1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtLog1.Location = new System.Drawing.Point(6, 19);
            this.txtLog1.Multiline = true;
            this.txtLog1.Name = "txtLog1";
            this.txtLog1.ReadOnly = true;
            this.txtLog1.Size = new System.Drawing.Size(137, 84);
            this.txtLog1.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(148)))), ((int)(((byte)(172)))));
            this.panel1.Controls.Add(this.lblRole);
            this.panel1.Controls.Add(this.lblUserId);
            this.panel1.Controls.Add(this.pictureBox3);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.lbladmin);
            this.panel1.Controls.Add(this.label21);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1413, 86);
            this.panel1.TabIndex = 123;
            // 
            // lblRole
            // 
            this.lblRole.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblRole.AutoSize = true;
            this.lblRole.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRole.ForeColor = System.Drawing.Color.White;
            this.lblRole.Location = new System.Drawing.Point(1313, 41);
            this.lblRole.Name = "lblRole";
            this.lblRole.Size = new System.Drawing.Size(52, 20);
            this.lblRole.TabIndex = 150;
            this.lblRole.Text = "admin";
            // 
            // lblUserId
            // 
            this.lblUserId.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblUserId.AutoSize = true;
            this.lblUserId.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUserId.ForeColor = System.Drawing.Color.White;
            this.lblUserId.Location = new System.Drawing.Point(1312, 21);
            this.lblUserId.Name = "lblUserId";
            this.lblUserId.Size = new System.Drawing.Size(57, 20);
            this.lblUserId.TabIndex = 149;
            this.lblUserId.Text = "admin";
            // 
            // pictureBox3
            // 
            this.pictureBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox3.Image = global::Cold_Storage.Properties.Resources.user;
            this.pictureBox3.Location = new System.Drawing.Point(1261, 17);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(45, 45);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox3.TabIndex = 148;
            this.pictureBox3.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(47)))), ((int)(((byte)(54)))));
            this.panel2.Controls.Add(this.pictureBox1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(210, 86);
            this.panel2.TabIndex = 38;
            // 
            // pictureBox1
            // 
            this.pictureBox1.ImageLocation = "logo_biofarma.png";
            this.pictureBox1.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.InitialImage")));
            this.pictureBox1.Location = new System.Drawing.Point(30, 5);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(150, 75);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 34;
            this.pictureBox1.TabStop = false;
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl1.Location = new System.Drawing.Point(7, 96);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1241, 642);
            this.tabControl1.TabIndex = 1;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.btnPending);
            this.tabPage2.Controls.Add(this.splitContainer1);
            this.tabPage2.Controls.Add(this.label2);
            this.tabPage2.Controls.Add(this.txtPickingList);
            this.tabPage2.Controls.Add(this.txtOutbound);
            this.tabPage2.Controls.Add(this.label3);
            this.tabPage2.Location = new System.Drawing.Point(4, 29);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1233, 609);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Outbound";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // btnPending
            // 
            this.btnPending.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPending.Image = global::Cold_Storage.Properties.Resources.pending;
            this.btnPending.Location = new System.Drawing.Point(346, 14);
            this.btnPending.Name = "btnPending";
            this.btnPending.Size = new System.Drawing.Size(125, 30);
            this.btnPending.TabIndex = 27;
            this.btnPending.Text = "Pending";
            this.btnPending.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnPending.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnPending.UseVisualStyleBackColor = true;
            this.btnPending.Click += new System.EventHandler(this.btnPending_Click);
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(5, 576);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(118, 20);
            this.label3.TabIndex = 19;
            this.label3.Text = "Scan Barcode :";
            // 
            // btnOpenLog
            // 
            this.btnOpenLog.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOpenLog.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOpenLog.Image = global::Cold_Storage.Properties.Resources.log1;
            this.btnOpenLog.Location = new System.Drawing.Point(1254, 694);
            this.btnOpenLog.Name = "btnOpenLog";
            this.btnOpenLog.Size = new System.Drawing.Size(150, 40);
            this.btnOpenLog.TabIndex = 153;
            this.btnOpenLog.Text = "Log File";
            this.btnOpenLog.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnOpenLog.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnOpenLog.UseVisualStyleBackColor = true;
            this.btnOpenLog.Click += new System.EventHandler(this.btnOpenLog_Click);
            // 
            // button8
            // 
            this.button8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button8.Image = global::Cold_Storage.Properties.Resources.logout;
            this.button8.Location = new System.Drawing.Point(1253, 125);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(150, 40);
            this.button8.TabIndex = 21;
            this.button8.Text = "Logout";
            this.button8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button8.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // btnConfig
            // 
            this.btnConfig.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnConfig.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConfig.Image = global::Cold_Storage.Properties.Resources.config;
            this.btnConfig.Location = new System.Drawing.Point(1253, 171);
            this.btnConfig.Name = "btnConfig";
            this.btnConfig.Size = new System.Drawing.Size(150, 40);
            this.btnConfig.TabIndex = 20;
            this.btnConfig.Text = "Config";
            this.btnConfig.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnConfig.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnConfig.UseVisualStyleBackColor = true;
            this.btnConfig.Click += new System.EventHandler(this.btnConfig_Click);
            // 
            // Cold
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1413, 746);
            this.Controls.Add(this.btnOpenLog);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.btnConfig);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Cold";
            this.Text = "Finish Goods Control Distribution Station";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Warehouse_FormClosed);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvBatch)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvOutbound)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button btnConfig;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label lbladmin;
        public System.Windows.Forms.DataGridView dgvOutbound;
        public System.Windows.Forms.TextBox txtOutbound;
        private System.Windows.Forms.GroupBox groupBox3;
        public System.Windows.Forms.TextBox txtLog1;
        public System.Windows.Forms.DataGridView dgvBatch;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.PictureBox pictureBox1;
        public System.Windows.Forms.Label lblRole;
        public System.Windows.Forms.Label lblUserId;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Button btnOpenLog;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.TextBox txtPickingList;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label label3;
        public System.Windows.Forms.Label lblBasket;
        public System.Windows.Forms.Label lblVial;
        public System.Windows.Forms.Label lblInnerBox;
        private System.Windows.Forms.Button btnPending;
        private System.Windows.Forms.DataGridViewTextBoxColumn BATCH;
        private System.Windows.Forms.DataGridViewTextBoxColumn PRODUCT_NAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn QTY;
        private System.Windows.Forms.DataGridViewTextBoxColumn UOM;
        private System.Windows.Forms.DataGridViewTextBoxColumn DETAIL_NO;
        private System.Windows.Forms.DataGridViewTextBoxColumn QTY_SCANNED;
        private System.Windows.Forms.DataGridViewTextBoxColumn TYPE_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn PRODUCT_NAME_SC;
        private System.Windows.Forms.DataGridViewTextBoxColumn EXPIRE_DATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn QTY_SC;
        private System.Windows.Forms.DataGridViewTextBoxColumn UOM_SC;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn DETAIL_NO_SC;
        private System.Windows.Forms.DataGridViewTextBoxColumn BATCH_SC;
        private System.Windows.Forms.DataGridViewTextBoxColumn PICKINGLIST_SC;
        private System.Windows.Forms.DataGridViewButtonColumn BTN_DELETE;
    }
}

