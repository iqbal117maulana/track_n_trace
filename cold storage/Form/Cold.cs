﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace Cold_Storage
{
    public partial class Cold : Form
    {
        Control ms;
        int ordersItemIndex;
        int ordersItemIndex2; 
        sqlitecs sq;
        dbaccess db;
        int orderItemBatch; 
        int orderItemBatch2;
        bool selectedBatch = false;        
        public string LogFile = "";
        public DataTable DtBatch = new DataTable();
        public DataTable DtScanned = new DataTable();
        private int SumQtyBatch = 0;
        public String role;

        //tambahan disable close button
        private const int CP_NOCLOSE_BUTTON = 0x200;
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams myCp = base.CreateParams;
                myCp.ClassStyle = myCp.ClassStyle | CP_NOCLOSE_BUTTON;
                return myCp;
            }
        }

        public Cold(string admin)
        {
            InitializeComponent();
            sq = new sqlitecs();
            db = new dbaccess();
            ms = new Control();
            ms.wr = this;
            ms.adminid = admin;
            ms.startstation();
            setAdminName(admin);
            txtPickingList.Text = ms.GetLastOutbond();
            btnPending.Enabled = false;
            if (!txtPickingList.Text.Equals(""))
            {
                LoadData();
            }
            ms.SetCountScanned();
            txtOutbound.Focus();
        }

        private void setAdminName(string admin)
        {
            //List<string> field = new List<string>();            
            //field.Add("first_name, company ");
            //string where = "id ='" + admin + "'";
            //List<string[]> ds = db.selectList(field, "[USERS]", where);
            //if (db.num_rows > 0)
            //{
            //    lblUserId.Text = ds[0][0];
            //    lblRole.Text = ds[0][1];
            //}

            List<string> field = new List<string>();
            field.Add("a.first_name, c.name, c.id as role ");
            string where = "a.id ='" + admin + "'";
            string from = "users a inner join users_groups b on a.id = b.user_id inner join groups c on b.group_id = c.id";            
            List<string[]> ds = db.selectList(field, from, where);
            if (db.num_rows > 0)
            {
                lblUserId.Text = ds[0][0];
                lblRole.Text = ds[0][1];
                role = ds[0][2];
                btnConfig.Visible = role == "1";
            }
        }

       
        private void Warehouse_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void ClearData() {            
            dgvBatch.AutoGenerateColumns = false;
            dgvBatch.DataSource = null;
            dgvOutbound.AutoGenerateColumns = false;
            dgvOutbound.DataSource = null;
            ms.insialisasitable();
        }
     
        private void dataGridView2_SelectionChanged(object sender, EventArgs e)
        {            
            if (dgvOutbound.SelectedRows.Count > 0)
            {
                ordersItemIndex2 = dgvOutbound.SelectedRows[0].Index;
            }
        }
      
        private void addPickingList(string itemid, string qty, string detailno)
        {
            Dictionary<string, string> field = new Dictionary<string, string>();
            field.Add("itemid", itemid);
            field.Add("qty", qty);
            field.Add("detailno", detailno);
            field.Add("status", "1");
            db.insert(field, "pickinglist");

        }        
        
        private void txtOutbound_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (txtOutbound.Text == "")
                {
                    new Confirm("Barcode empty", "Information", MessageBoxButtons.OK);
                    ms.log("Barcode empty");
                    return;
                }                

                if (selectedBatch)
                {
                    orderItemBatch2 = orderItemBatch;
                    ms.setOutbound(txtOutbound.Text);
                }
                else
                {
                    new Confirm("Batch Not Selected", "Information", MessageBoxButtons.OK);
                    ms.log("Batch Not Selected");
                }
                txtOutbound.Text = "";
                txtPickingList.Enabled = DtScanned.Rows.Count <= 0;
                CekScannFinished();

                if (ms.LastIdxSelectedBatch > -1)
                {
                    dgvBatch.CurrentCell = dgvBatch.Rows[ms.LastIdxSelectedBatch].Cells[0];
                }                         

                if (dgvBatch.SelectedRows.Count > 0)
                {
                    ms.qtyDetailSisa = Convert.ToString(db.StrToInt(dgvBatch.Rows[orderItemBatch].Cells["QTY"].Value.ToString(), 0)
                                      - db.StrToInt(dgvBatch.Rows[orderItemBatch].Cells["QTY_SCANNED"].Value.ToString(),0));
                }
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            Confirm cf = new Confirm("Are you sure? ", "Confirmation");
            bool result = cf.conf();
            if (result)
            {
                ms.InsertSystemLog("Logout User : " + lblUserId.Text);
                this.Dispose();
                Login lg = new Login();
                lg.Show();
            }
            else {
                txtOutbound.Focus();
            }
        }

        private void txtBarcode_ImeModeChanged(object sender, EventArgs e)
        {

        }        

        public void setDgvBatch()
        {            
            if (dgvBatch.RowCount > 0){
                dgvBatch_CellClick(this.dgvBatch, new DataGridViewCellEventArgs(0, 0));
            }
        }        

        private void dgvBatch_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            selectedBatch = false;
            ms.LastIdxSelectedBatch = -1;
            if (dgvBatch.SelectedRows.Count > 0)
            {
                orderItemBatch = dgvBatch.SelectedRows[0].Index;
                ms.detailNo = dgvBatch.Rows[orderItemBatch].Cells["DETAIL_NO"].Value.ToString();
                ms.batchSelect = dgvBatch.Rows[orderItemBatch].Cells["BATCH"].Value.ToString();
                ms.qtyDetail = dgvBatch.Rows[orderItemBatch].Cells["QTY"].Value.ToString();
                ms.qtyDetailSisa = Convert.ToString(db.StrToInt(dgvBatch.Rows[orderItemBatch].Cells["QTY"].Value.ToString(), 0)
                                   - db.StrToInt(dgvBatch.Rows[orderItemBatch].Cells["QTY_SCANNED"].Value.ToString(), 0));                
                ms.uomShhiping = dgvBatch.Rows[orderItemBatch].Cells["UOM"].Value.ToString();
                ms.namaProduct = dgvBatch.Rows[orderItemBatch].Cells["PRODUCT_NAME"].Value.ToString();
                selectedBatch = true;
                ms.LastIdxSelectedBatch = e.RowIndex;
            }            
        }

        private void dgvBatch_SelectionChanged(object sender, EventArgs e)
        {
            selectedBatch = false;
            ms.LastIdxSelectedBatch = -1;
            if (dgvBatch.SelectedRows.Count > 0)
            {
                orderItemBatch = dgvBatch.SelectedRows[0].Index;
                ms.detailNo = dgvBatch.Rows[orderItemBatch].Cells["DETAIL_NO"].Value.ToString();
                ms.batchSelect = dgvBatch.Rows[orderItemBatch].Cells["BATCH"].Value.ToString();
                ms.qtyDetail = dgvBatch.Rows[orderItemBatch].Cells["QTY"].Value.ToString();
                ms.qtyDetailSisa = Convert.ToString(db.StrToInt(dgvBatch.Rows[orderItemBatch].Cells["QTY"].Value.ToString(), 0)
                                   - db.StrToInt(dgvBatch.Rows[orderItemBatch].Cells["QTY_SCANNED"].Value.ToString(), 0));                
                ms.uomShhiping = dgvBatch.Rows[orderItemBatch].Cells["UOM"].Value.ToString();
                ms.namaProduct = dgvBatch.Rows[orderItemBatch].Cells["PRODUCT_NAME"].Value.ToString();                
                selectedBatch = true;
                ms.LastIdxSelectedBatch = dgvBatch.SelectedRows[0].Index;                 
            }         
        }

        private void dgvOutbound_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {            
            ms.SetCountScanned();
            btnPending.Enabled = DtScanned.Rows.Count > 0;
        }

        private void btnOpenLog_Click(object sender, EventArgs e)
        {
            Log frm;
            frm = new Log(LogFile);
            frm.ShowDialog();
            if (frm.isClear == true)
                LogFile = "";
            txtOutbound.Focus();
        }

        private void txtPickingList_Click(object sender, EventArgs e)
        {
            ClearData();
            PickingList frm = new PickingList();
            frm.ShowDialog();
            txtPickingList.Text = frm.PickingListNumber;
            ms.log("Picking List Number : "+txtPickingList.Text+ " Selected");
            ms.InsertSystemLog("Picking List Number : " + txtPickingList.Text + " Selected");
            LoadData();
            ms.dataDelivery = txtPickingList.Text;
            SetSizeAcolumnBatch();
            btnPending.Enabled = DtScanned.Rows.Count > 0;
            txtOutbound.Focus();
        }

        private void LoadData(){
            dgvBatch.AutoGenerateColumns = false;
            dgvOutbound.AutoGenerateColumns = false;
            if (!txtPickingList.Text.Equals(""))
            {
                DtBatch = ms.GetBatch(txtPickingList.Text);
                DtScanned = ms.GetScenned(txtPickingList.Text);
            }
            else {
                DtBatch.Clear();
                DtScanned.Clear();
            }            
            ms.dataDelivery = txtPickingList.Text;
            dgvBatch.DataSource = DtBatch;
            dgvOutbound.DataSource = DtScanned;
            setDgvBatch();
            ms.SetCountScanned();
            txtPickingList.Enabled = DtScanned.Rows.Count <= 0;
            
            if (DtBatch.Rows.Count > 0){
                SumQtyBatch = int.Parse(DtBatch.Compute("SUM(QTY)", string.Empty).ToString());
            }
            CekScannFinished();
            SetSizeAcolumnBatch();
        }

        private void CekScannFinished() {
            int SumQtySccaned = 0;
            if (DtScanned.Rows.Count > 0)
            {
                object SumObject;
                SumObject = DtScanned.Compute("SUM(QTY_SC)", string.Empty);
                SumQtySccaned = int.Parse(SumObject.ToString());
            }

            
            if ((SumQtySccaned >= SumQtyBatch) || (SumQtySccaned == 0))
            {
                txtPickingList.Enabled = true;
                if ((SumQtySccaned >= SumQtyBatch) && (SumQtySccaned > 0) && (SumQtyBatch > 0))
                {
                    new Confirm("Scanned full", "Information", MessageBoxButtons.OK);
                    ms.InsertSystemLog("Scanned full " + SumQtySccaned.ToString() + "for Picking List Number : " + txtPickingList.Text);
                    txtPickingList.Text = "";
                    txtPickingList.Enabled = true;
                    DtBatch.Clear();
                    DtScanned.Clear();
                    dgvBatch.DataSource = DtBatch;
                    dgvOutbound.DataSource = DtScanned;
                    txtOutbound.Text = "";
                    ms.SetCountScanned();                
                    btnPending.Enabled = DtScanned.Rows.Count > 0;
                    ms.LastIdxSelectedBatch = -1;
                }
            }            
            
        }

        public void SetSizeAcolumnBatch()
        {
            if (DtBatch.Rows.Count > 0)
            {
                for (int i = 0; i < dgvBatch.Columns.Count; i++)
                {
                    dgvBatch.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                }
                dgvBatch.Columns[dgvBatch.Columns.Count - 1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }
            else
            {
                for (int i = 0; i < dgvBatch.Columns.Count; i++)
                {
                    dgvBatch.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                }
            }
        }

        private void dgvOutbound_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            dgvOutbound.Rows[e.RowIndex].Cells["BTN_DELETE"].Value = "Delete";
        }

        

        private void dgvOutbound_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0){
                return;
            }            

            if (dgvOutbound.Columns[e.ColumnIndex].Name == "BTN_DELETE")
            {

                bool IsAdmin = false;
                if (DtBatch.Rows.Count > 0) {
                    if (role != "1")
                    {
                        LoginAdmin frm = new LoginAdmin();
                        frm.ShowDialog();
                        IsAdmin = frm.IsAdmin;
                    }
                    else {
                        IsAdmin = true;
                    }

                    if (IsAdmin)
                    {
                        Confirm cf = new Confirm("Are you sure? ", "Confirmation");
                        bool result = cf.conf();
                        if (result)
                        {
                            int idxSelected = ms.LastIdxSelectedBatch;
                            String shippingOrderNumber = txtPickingList.Text;
                            String itemId = DtScanned.Rows[e.RowIndex]["ID"].ToString();
                            String detailNo = DtScanned.Rows[e.RowIndex]["DETAIL_NO_SC"].ToString();
                            String Pickinglist = DtScanned.Rows[e.RowIndex]["PICKINGLIST_SC"].ToString();
                            String Batch = DtScanned.Rows[e.RowIndex]["BATCH_SC"].ToString();
                            ms.IsDeleteBatchScanned(shippingOrderNumber, itemId, detailNo, Pickinglist, Batch);
                            LoadData();
                            if ((DtBatch.Rows.Count > 0) && (idxSelected > -1))
                            {
                                dgvBatch.CurrentCell = dgvBatch.Rows[idxSelected].Cells[0];
                            }
                        }
                    }
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void btnPending_Click(object sender, EventArgs e)
        {
            bool IsAdmin = false;            
            if (role != "1")
            {
                LoginAdmin frm = new LoginAdmin();
                frm.ShowDialog();
                IsAdmin = frm.IsAdmin;
            }
            else
            {
                IsAdmin = true;
            }

            if (IsAdmin)
            {
                Confirm cf = new Confirm("Are you sure? ", "Confirmation");
                bool result = cf.conf();
                if (result)
                {
                    ms.InsertSystemLog("Authorized pending Picking List Number : "+txtPickingList.Text);
                    txtPickingList.Text = "";
                    txtPickingList.Enabled = true;
                    DtBatch.Clear();
                    DtScanned.Clear();
                    dgvBatch.DataSource = DtBatch;
                    dgvOutbound.DataSource = DtScanned;
                    txtOutbound.Text = "";
                    ms.SetCountScanned();
                }
            }
            btnPending.Enabled = DtScanned.Rows.Count > 0;
            txtOutbound.Focus();
        }

        private void btnConfig_Click(object sender, EventArgs e)
        {
            config cfg = new config();
            cfg.ShowDialog();
            txtOutbound.Focus();
        }

        private void txtOutbound_KeyPress(object sender, KeyPressEventArgs e)
        {
            var regex = new Regex(@"[^a-zA-Z0-9\b]");
            if (regex.IsMatch(e.KeyChar.ToString()))
            {
                e.Handled = true;
            }
        }
    }
}

