﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Cold_Storage
{
    public partial class LoginAdmin : Form
    {
        public bool IsAdmin = false;
        dbaccess db;
        public LoginAdmin()
        {
            InitializeComponent();
            this.Text = this.Text + " V " + System.Windows.Forms.Application.ProductVersion;
            db = new dbaccess();
        }

        private void Login_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void txtUsername_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (txtUsername.Text.Length > 0)
                {
                    this.ActiveControl = txtPassword;
                }
                else
                {
                    new Confirm("Username cannot empty", "Information", MessageBoxButtons.OK);
                    txtUsername.Focus();
                }
            }
        }

        private void txtPassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (txtUsername.Text.Length > 0)
                {
                    if (txtPassword.Text.Length > 0)
                    {
                        do_login();
                    }
                    else
                    {
                        new Confirm("Password cannot empty", "Information", MessageBoxButtons.OK);
                        txtPassword.Focus();
                    }
                }
                else
                {
                    new Confirm("Username cannot empty", "Information", MessageBoxButtons.OK);
                    txtUsername.Focus();
                }
            }
        }

        private void do_login()
        {
            try
            { //cek useraname
                if (db.validasi(txtUsername.Text))
                {
                    //cek Password
                    if (db.validasi(txtUsername.Text, txtPassword.Text))
                    {
                        List<string[]> res = db.validasi(txtUsername.Text, txtPassword.Text, "0");
                        if (res != null)
                        {
                            string adminid = res[0][0];
                            if (db.cekPermision(adminid, "cold_storage", "read"))
                            {
                                IsAdmin = CekAdmin(adminid);
                                if (IsAdmin == false)
                                {
                                    new Confirm("Dont have permission", "Information", MessageBoxButtons.OK);
                                }
                                else {
                                    this.DialogResult = DialogResult.Cancel;
                                }
                                
                            }
                            else
                            {
                                new Confirm("Dont have permission", "Information", MessageBoxButtons.OK);
                            }
                        }
                        else
                        {
                            new Confirm("User is inactive", "Information", MessageBoxButtons.OK);
                            db.simpanLog("Error 0010 : Username dan Password Salah !");
                            clearText();
                        }
                    }
                    else
                    {
                        new Confirm("Invalid Username/Password", "Information", MessageBoxButtons.OK);
                        db.simpanLog("Error 0010 : Username dan Password Salah !");
                        txtPassword.Focus();
                        txtPassword.Text = "";
                    }
                }
                else
                {
                    new Confirm("Invalid Username/Password", "Information", MessageBoxButtons.OK);
                    db.simpanLog("Error 0010 : Username Salah !");
                    txtUsername.Focus();
                    clearText();
                }
            }
            catch (ArgumentException ae)
            {
                db.simpanLog("Error " + ae.ToString());
            }
        }

        private void clearText()
        {
            txtUsername.Text = "";
            txtPassword.Text = "";
            txtUsername.Focus();
        }

        private void buttonLogin_Click(object sender, EventArgs e)
        {
            do_login();
        }

        private bool CekAdmin(string admin)
        {
            bool hasil = false;
            List<string> field = new List<string>();
            field.Add("c.id as role ");
            string where = "a.id ='" + admin + "'";
            string from = "users a inner join users_groups b on a.id = b.user_id inner join groups c on b.group_id = c.id";
            List<string[]> ds = db.selectList(field, from, where);
            if (db.num_rows > 0)
            {
                String role = ds[0][0];
                hasil = role == "1";
            }
            return hasil;
        }
     
    }
}
