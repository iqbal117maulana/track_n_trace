﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Windows.Forms;

namespace Cold_Storage
{
    class Control
    {   
        DataTable tableOutbound;                
        sqlitecs sq;
        dbaccess db;
        int quantityDetail;
        public string adminid;
        
        public Cold wr;        

        public string batchSelect;
        public string dataDelivery;
        public int LastIdxSelectedBatch = -1;
        String sql;      

        private const String UOM_BASKET = "Basket";
        private const String UOM_INNER_BOX = "Innerbox";
        private const String UOM_VIAL = "Vial";

        private const String UOM_BASKET_TEXT = "Basket";
        private const String UOM_INNER_BOX_TEXT = "Inner Box";
        private const String UOM_VIAL_TEXT = "Vial";       
        private bool IsSas = false;

        public Control()
        {
            db = new dbaccess();
            sq = new sqlitecs();
            insialisasitable();
        }

        public void insialisasitable()
        {
            tableOutbound = new DataTable();
            tableOutbound.Columns.Add("ID");
            tableOutbound.Columns.Add("PRODUCT_NAME_SC");
            tableOutbound.Columns.Add("UOM_SC");
            tableOutbound.Columns.Add("QTY_SC");
        }

        public void startstation()
        {
            db.adminid = adminid;
            db.from = "Finish Good Station";
            db.eventname = "Start Station";
            db.systemlog();            
        }

        public void log(string data)
        {
            try
            {
                string dates = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                string simpan = dates + "\t" + data + "\n";
                //wr.txtLog1.AppendText(simpan);
                wr.LogFile = wr.LogFile + simpan + Environment.NewLine;
                db.simpanLog(simpan);
            }
            catch (ObjectDisposedException ob)
            {

            }
        }
        

        public bool cekBasketPickingList(string databarcode, String Status)
        {
            List<string> field = new List<string>();
            field.Add("itemid");
            string where = "itemid ='" + databarcode + " AND status = '"+Status+"' "; //3 = status dari decomision di manual packer
            List<string[]> ds = db.selectList(field, "pickingList", where);
            if (db.num_rows > 0)
            {
                return true;
            }
            return false;
        }
        
        private bool cekBasketsqlite(string basket)
        {
            List<string> field = new List<string>();
            field.Add("id");
            string where = "id ='" + basket + "'";
            List<string[]> ds = sq.select(field, "warehouse", where);
            if (sq.num_rows > 0)
            {
                return true;
            }
            return false;
        }

        string uom;
        public void hapusdata(string barcode)
        {
            if (barcode.Length > 0)
                barcode = "basketid ='" + barcode + "'";
            sq.delete(barcode, "warehouse");
        }

        bool CekIsSas(string product_model)
        {
            List<string> field = new List<string>();
            field.Add("isSAS");
            string from = "product_model";
            string where = "product_model = '" + product_model + "'";
            List<string[]> ds = db.selectList(field, from, where);
            if (db.num_rows > 0)
            {
                return ds[0][0] == "True";
            }
            return false;
        }

        string uomId = "";
        string ProductModel = "";
        bool cekproduct(string databarcode, string Batch)
        {
            ProductModel = "";
            IsSas = false;
            if (cekbasket(databarcode, Batch, out ProductModel))
            {
                uomId = "basketid='"+databarcode+"'";
                uom = UOM_BASKET;
                IsSas = CekIsSas(ProductModel);
                return true;
            }
            if (cekInnerBox(databarcode, Batch, out ProductModel))
            {
                uomId = "innerBoxId='" + databarcode + "' or innerBoxGsOneId='" + databarcode + "'";
                uom = UOM_INNER_BOX;           
                IsSas = CekIsSas(ProductModel);
                return true;
            }

            //if (cekblister(databarcode,locat))
            //{
            //    uomId = "blisterpackid='" + databarcode + "'";
            //    uom = "Blister";            
            //    return true;
            //}

            if (cekVial(databarcode, Batch, out ProductModel))
            {
                uomId = "capid='" + databarcode + "' or gsonevialid='" + databarcode + "'";
                uom = UOM_VIAL;
                IsSas = CekIsSas(ProductModel);
                return true;
            }
            uom = "";
            return false;
        }

        private bool getProductModel(string dataBarcode, String batchnumber)
        {
            List<string> field = new List<string>();
            field.Add("v.basketId");
            String from = "dbo.vaccine v ";
            string where = "v.basketId = '" + dataBarcode + "' AND v.batchnumber='" + batchnumber + "' AND v.isReject = '0'";
            db.selectList(field, from, where);
            if (db.num_rows > 0)
            {
                return true;
            }
            return false;
        }

        private bool cekbasket(string dataBarcode, String batchnumber, out string productModel)
        {
            List<string> field = new List<string>();
            field.Add("v.basketId, v.productModelId ");
            String from = "dbo.vaccine v ";            
            string where = "v.basketId = '" + dataBarcode + "' AND v.batchnumber='" + batchnumber + "' AND v.isReject = '0'";
            List<string[]> ds = db.selectList(field, from, where);
            if (db.num_rows > 0)
            {
                productModel = ds[0][1];
                return true;
            }
            productModel = ""; 
            return false;
        }

        //private bool cekblister(string dataBarcode, string batchnumber)
        //{
        //    List<string> field = new List<string>();
        //    field.Add("v.basketId");
        //    String from = "dbo.vaccine v ";
        //    string where = "(v.blisterpackid = '" + dataBarcode + "' OR " + "innerBoxGsOneId ='" + dataBarcode + "') AND v.batchnumber='" + batchnumber + "' AND v.isReject = '0'";
        //    db.selectList(field, from + TableLocation, where);
        //    if (db.num_rows > 0) {
        //        return true;
        //    }                
        //    return false;
        //}

        private bool cekInnerBox(string dataBarcode, string batchnumber, out string productModel)
        {
            List<string> field = new List<string>();
            field.Add("v.innerBoxId, v.productModelId ");
            String from = "dbo.vaccine v ";
            string where = "(v.innerBoxId = '" + dataBarcode + "' OR " + "innerBoxGsOneId ='" + dataBarcode + "') AND v.batchnumber='" + batchnumber + "' AND v.isReject = '0'";
            List<string[]> ds = db.selectList(field, from, where);            
            if (db.num_rows > 0) {
                productModel = ds[0][1];
                return true;
            }
            productModel = ""; 
            return false;
        }

        private bool cekVial(string dataBarcode, String batchnumber, out string productModel)
        {
            List<string> field = new List<string>();
            field.Add("v.capid, v.productModelId ");
            String from = "dbo.vaccine v ";            
            string where = "(v.capid = '" + dataBarcode + "' OR " + "gsonevialid ='" + dataBarcode + "') AND v.batchnumber='" + batchnumber + "' AND v.isReject = '0'";
            List<string[]> ds = db.selectList(field, from, where);
            if (db.num_rows > 0)
            {
                productModel = ds[0][1];
                return true;
            }
            productModel = ""; 
            return false;
        }        

        void updateVaccine(string databarcode, string lastlocat, String pickinglist)
        {
            string cari="";
            if (uom.Equals(UOM_BASKET))
            {
                cari = "basketid ='"+databarcode+"'";
            }            
            //else if (uom.Equals("Blister"))
            //{
            //    cari = "blisterpackid='" + databarcode + "'";
            //}
            else if (uom.Equals(UOM_VIAL))
            {
                cari = "capid ='"+databarcode+"' OR gsonevialid ='"+databarcode+"'";
            }
            else if (uom.Equals(UOM_INNER_BOX) || IsSas)
            {
                cari = "innerboxid ='" + databarcode + "' OR innerBoxGsOneId ='" + databarcode + "'";
            }
            
            cari = cari + " AND pickinglist IS null ";
            Dictionary<string, string> field = new Dictionary<string, string>();            
            field.Add("pickinglist", pickinglist);
            field.Add("outbound_time", "GETDATE()");
            db.update(field, "vaccine", cari);
        }      
   
       
        void deleteVialSqlite(string delete)
        {
            if (delete.Length > 0)
            {
                string where = "id ='"+delete+"'";
                sq.delete(where, "warehouse");
            }
        }

        public bool cekItemIdPickinglist(string databarcode, string detailNo)
        {
            List<string> field = new List<string>();
            field.Add("itemid");
            string where = "itemid ='" + databarcode + "' and detailNo = '" + detailNo + "'";
            List<string[]> ds = db.selectList(field, "pickinglist", where);
            if (db.num_rows > 0)
            {
                return true;
            }
            return false;
        }


        public String simpanpickinglist(string itemid, String qty, string detailno, string uom)
        {
            Dictionary<string, string> field = new Dictionary<string, string>();            
            field.Add("itemid", itemid);
            field.Add("qty", qty);
            field.Add("detailno", detailno);
            field.Add("uom", uom);
            field.Add("status", "1");

            //Jadinya di generate otomatis
            //string dates = DateTime.Now.ToString("mmMMyyssddHH");
            //field.Add("pickinglist", dates);
            //db.Movement("", "Cold Storage", "Input Data", qty, itemid);
            if (db.insert(field, "pickinglist")) {
                sql = "SELECT pickinglist " +
                      "FROM pickinglist " +
                      "WHERE itemid = '" + itemid + "' AND detailno = '" + detailno + "' AND uom = '" + uom + "' AND status = 1 ";
                DataTable dt = new DataTable();
                db.OpenQuery(out dt, sql);
                if (dt.Rows.Count > 0)
                {
                    return dt.Rows[0]["pickinglist"].ToString();
                }                
            }            
            return "";
        }

        
        public string detailNo;
        public string qtyDetail;
        public string qtyDetailSisa;
        public string uomShhiping;
        public string namaProduct;
        internal void setOutbound(string barcode)
        {
            // if (cekDalamCold(barcode))
            {
                if (cekproduct(barcode, batchSelect))
                {
                    if (!cekItemIdPickinglist(barcode, detailNo))// Cek apakah item barcode sudah pernah masuk ke pickinglist
                    {
                        String qty = Convert.ToString(getproduct(barcode)); //ambil count qty dari vaccine                        
                        if (int.Parse(qty) <= 0)
                        {
                            log(barcode + " is empty");
                            new Confirm(barcode + " is empty", "Information", MessageBoxButtons.OK);
                            return;
                        }


                        if (int.Parse(qty) > int.Parse(qtyDetailSisa))
                        {
                            log(barcode + " Qty scanned more than qty request");
                            new Confirm(" Qty scanned more than qty request", "Information", MessageBoxButtons.OK);
                            return;
                        }
                        
                        String IdPickingList = simpanpickinglist(barcode, qty, detailNo, uom);
                        if (!IdPickingList.Equals("")) //insert ke table packinglist){                        
                        {
                            updateVaccine(barcode, "null", IdPickingList); //update lokasi di vaccine jadi null
                            deleteVialSqlite(barcode);
                            cekKurangQtyDetail(detailNo, dataDelivery, batchSelect); //jika qty sudah sesuai di pickinglist update ke shippingOrder_detail flag menjadi 2
                            cekDetailShipping(dataDelivery); // jika semua flag shippingOrder_detail = 2 maka update flag di shippingOrder jadi 2
                            AddBatchScanned(detailNo, barcode);
                            int idxSelected = LastIdxSelectedBatch;
                            wr.DtBatch = GetBatch(wr.txtPickingList.Text);
                            wr.dgvBatch.DataSource = wr.DtBatch;
                            //wr.setDgvBatch();               
                            if (wr.DtBatch.Rows.Count > 0)
                            {
                                wr.dgvBatch.CurrentCell = wr.dgvBatch.Rows[idxSelected].Cells[0];
                            }
                            log(barcode + " scanned");
                            if (wr.DtScanned.Rows.Count > 0)
                            {
                                for (int i = 0; i < wr.dgvOutbound.Columns.Count; i++)
                                {
                                    wr.dgvOutbound.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                                }
                            }
                            else {
                                for (int i = 0; i < wr.dgvOutbound.Columns.Count; i++)
                                {
                                    wr.dgvOutbound.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                                }
                            }
                        }                        
                    }
                    else {
                        log(barcode + " already exist !");
                        new Confirm("already exist !", "Information", MessageBoxButtons.OK);
                    }
                }
                else
                {
                    log(barcode + " Not Found");
                    new Confirm(" Not Found", "Information", MessageBoxButtons.OK);
                }
            }            
        }
                                
        private int getproduct(string basket)
        {
            String FieldGroup = "";
            String Uom = uomShhiping;            
            Uom.ToLower();
            if (Uom.Equals(UOM_BASKET.ToLower()))
            {
                FieldGroup = "basketid";
            }
            else if (Uom.Equals(UOM_INNER_BOX.ToLower()))
            {
                FieldGroup = "innerBoxId";
            }
            else if (Uom.Equals("inner box"))
            {
                FieldGroup = "innerBoxId";
            }
            //else if (Uom == "blister")
            //{
            //    FieldGroup = "blisterpackid";
            //}
            else if (Uom.Equals(UOM_VIAL.ToLower()))
            {
                FieldGroup = "capid";
            }

            int outbox_qty = 1;
            string sql = "";
            if (IsSas)
            {
                sql = "SELECT ISNULL(outbox_qty,0) " +
                             "FROM product_model " +
                             "WHERE product_model = '" + ProductModel + "'";
                outbox_qty = db.selectCount(sql);
            }
            List<string> field = new List<string>();
            sql = "SELECT COUNT(jml) FROM( " +
                    " SELECT COUNT(capid) AS jml " +
                    " FROM vaccine " +
                    " WHERE " + uomId + " And isreject = 0 AND pickinglist IS NULL " +
                    " GROUP BY " + FieldGroup +
                 " ) AS sql";
            return (db.selectCount(sql) * outbox_qty);
        }
        

        private bool cekBlisterBatch(string barcode, string batchnumber)
        {
            List<string> field = new List<string>();
            field.Add("Vaccine.blisterpackId");
            string from = "Vaccine INNER JOIN product_model ON Vaccine.productModelId = product_model.product_model";
            string where = "Vaccine.blisterpackId = '" + barcode + "' AND vaccine.batchnumber = '" + batchnumber + "'";
            List<string[]> ds = db.selectList(field, from, where);
            if (db.num_rows > 0)
            {
                return true;
            }
            return false;
        }

        private bool cekvaccineBatch(string barcode, string batchnumber)
        {
            List<string> field = new List<string>();
            field.Add("Vaccine.capId");
            string where = "Vaccine.capId = '" + barcode + "' and batchnumber ='" + batchnumber + "'";
            List<string[]> ds = db.selectList(field, "Vaccine", where);
            if (db.num_rows > 0)
            {
                return true;
            }
            return false;
        }

        public bool cekBasketBatch(string barcode, string batchnumber)
        {
            List<string> field = new List<string>();
            field.Add("basketId");
            string from = "vaccine";
            string where = "vaccine.basketId = '" + barcode + "' and Vaccine.batchnumber='" + batchnumber + "'";
            List<string[]> ds = db.selectList(field, from, where);
            if (db.num_rows > 0)
            {
                return true;
            }
            return false;

        }

        public bool cekInnerBoxBatch(string barcode, string batchnumber)
        {
            List<string> field = new List<string>();
            field.Add("innerBoxId");
            string from = "Vaccine";
            string where = "(innerBoxId = '" + barcode + "' OR innerboxGsOneId = '" + barcode + "') AND batchnumber = '" + batchnumber + "'";
            List<string[]> ds = db.selectList(field, from, where);
            if (db.num_rows > 0)
            {
                return true;
            }
            return false;
        }

        public bool cekSheepingOrder(string databarcode)
        {
            List<string> field = new List<string>();
            field.Add("itemid");
            string where = "itemid ='" + databarcode + "' and status = '1'";
            List<string[]> ds = db.selectList(field, "pickinglist", where);
            if (db.num_rows > 0)
            {
                return true;
            }
            return false;
        }

        private void cekDetailShipping(string shippingorder)
        {
            List<string> field = new List<string>();
            field.Add("count(shippingOrderNumber)");
            string from = "shippingOrder_detail";
            string where = "shippingOrderNumber='" + shippingorder + "' and flag ='0'";
            List<string[]> ds = db.selectList(field, from, where);
            if (db.num_rows > 0)
            {
                int qtyde = int.Parse(ds[0][0]);
                if (qtyde == 0)
                {
                    //tutup shipping order nya 
                    Dictionary<string, string> fieldAct = new Dictionary<string, string>();
                    fieldAct.Add("flag", "2");
                    where = "shippingOrderNumber ='" + shippingorder + "'";
                    db.update(fieldAct, "shippingOrder", where);

                    //tutup pickingList_ERP di db TNT
                    Dictionary<string, string> fieldAct2 = new Dictionary<string, string>();
                    fieldAct2.Add("flag", "2");
                    where = "pickingListNumber ='" + shippingorder + "'";
                    db.update2(fieldAct2, "pickingList_ERP", where);
                }
            }
        }

        private void cekKurangQtyDetail(string shippingDetail, string shippingOrdernumber, string BatchNumber)
        {
            List<string> field = new List<string>();
            field.Add("sum(pickingList.qty)");
            string from = "dbo.pickingList INNER JOIN dbo.shippingOrder_detail ON dbo.pickingList.detailNo = dbo.shippingOrder_detail.shippingOrderDetailNo";
            string where = "dbo.pickingList.status = '1' AND dbo.pickingList.detailNo = '" + shippingDetail + "'";
            List<string[]> ds = db.selectList(field, from, where);
            if (db.num_rows > 0)
            {
                if (ds[0][0].Length > 0)
                {
                    int qtyde = int.Parse(ds[0][0]);
                    int tempQty = int.Parse(qtyDetail);
                    if (qtyde >= tempQty)
                    {
                        //tutup shipping order detail nya 
                        Dictionary<string, string> fieldAct = new Dictionary<string, string>();
                        fieldAct.Add("flag", "2");
                        where = "shippingOrderDetailNo ='" + shippingDetail + "'";
                        db.update(fieldAct, "shippingOrder_detail", where);

                        //tutup pickingList_ERP_detail db TNT
                        Dictionary<string, string> fieldAct2 = new Dictionary<string, string>();
                        fieldAct2.Add("flag", "2");
                        where = "pickingList_ERP ='" + shippingOrdernumber + "' AND batchNumber = '" + BatchNumber + "'";
                        db.update2(fieldAct2, "pickingList_ERP_detail", where);
                    }
                }
            }
        }


        internal void AddBatchScanned(string DetailNo, String Barcode)
        {
            DataTable dt = new DataTable();            
            dt = GetBatchScenned(detailNo, Barcode);

            for (int i = 0; i < dt.Rows.Count; i++){
                DataRow Row = wr.DtScanned.NewRow();
                Row["TYPE_ID"] = dt.Rows[i]["TYPE_ID"];
                Row["PRODUCT_NAME_SC"] = dt.Rows[i]["PRODUCT_NAME_SC"];
                Row["UOM_SC"] = dt.Rows[i]["UOM_SC"];
                Row["QTY_SC"] = dt.Rows[i]["QTY_SC"];
                Row["ID"] = dt.Rows[i]["ID"];
                Row["EXPIRE_DATE"] = dt.Rows[i]["EXPIRE_DATE"];
                Row["DETAIL_NO_SC"] = dt.Rows[i]["DETAIL_NO_SC"];
                Row["PICKINGLIST_SC"] = dt.Rows[i]["PICKINGLIST_SC"];
                Row["BATCH_SC"] = dt.Rows[i]["BATCH_SC"];
                wr.DtScanned.Rows.Add(Row);
            }
            wr.dgvOutbound.DataSource = wr.DtScanned;
        }

        internal void SetCountScanned() {
            int CountBasket = 0;
            int CountInnerBox = 0;
            int CountVial = 0;
            
            for (int i = 0; i < wr.DtScanned.Rows.Count; i++)
            {
                String Uom = wr.DtScanned.Rows[i]["TYPE_ID"].ToString();
                switch (Uom)
                {
                    case UOM_BASKET:
                        CountBasket ++;
                        break;
                    case UOM_INNER_BOX:
                        CountInnerBox++;
                        break;
                    case UOM_VIAL:
                        CountVial++;
                        break;
                }
            }
            wr.lblBasket.Text = UOM_BASKET_TEXT + " : " + CountBasket.ToString();
            wr.lblInnerBox.Text = UOM_INNER_BOX_TEXT + " : " + CountInnerBox.ToString();
            wr.lblVial.Text = UOM_VIAL_TEXT + " : " + CountVial.ToString();
        }

        internal void IsDeleteBatchScanned(String shippingOrderNumber, String ItemId, String DetailNo, String Pickinglist, String BatchNumber)
        {
            if (DeleteBatchScanned(shippingOrderNumber, ItemId, DetailNo, Pickinglist, BatchNumber))
            {
                log(ItemId + " deleted");
                new Confirm(ItemId + " deleted", "Information", MessageBoxButtons.OK);
            }
        }

        internal DataTable GetBatch(string NoPickingList)
        {
            sql = "SELECT d.batchnumber AS BATCH, d.qty AS QTY, d.location AS LOCATION, d.uom AS UOM, d.shippingOrderDetailNo AS DETAIL_NO, p.model_name AS PRODUCT_NAME, SUM(pl.qty) AS QTY_SCANNED " +
                  "FROM dbo.shippingOrder m, product_model p, " +
                  "dbo.shippingOrder_detail d LEFT JOIN dbo.pickingList pl ON pl.detailNo = d.shippingOrderDetailNo " +
                  "WHERE m.shippingOrderNumber = d.shippingOrderNumber " +
                  "AND m.status = '1' AND d.status = '0' AND d.productModelId = p.product_model AND m.shippingOrderNumber = d.shippingOrderNumber " +
                  "AND m.shippingOrderNumber = '" + NoPickingList + "' " +
                  "GROUP BY d.batchnumber, d.qty, d.location, d.uom, d.shippingOrderDetailNo, p.model_name";
            DataTable dt = new DataTable();
            db.OpenQuery(out dt, sql);
            return dt;
        }

        internal DataTable GetScenned(string NoPickingList)
        {
            sql = "SELECT p.uom AS TYPE_ID, m.model_name AS PRODUCT_NAME_SC, d.uom AS UOM_SC, p.qty AS QTY_SC, p.itemId AS ID, pac.expired AS EXPIRE_DATE, " +
                  "p.detailNo AS DETAIL_NO_SC, p.pickingList AS PICKINGLIST_SC, d.batchnumber AS BATCH_SC " +
                  "FROM pickingList p, shippingOrder_detail d, product_model m, packaging_order pac " +
                  "WHERE p.detailNo = d.shippingOrderDetailNo AND d.productModelId = m.product_model " +                  
                  //"AND ((p.itemId = v.basketid) OR (p.itemId = v.innerBoxId) OR (p.itemId = v.innerBoxGsOneId) OR (p.itemId = v.capid) OR (p.itemId = v.gsonevialid)) " +
                  "AND d.batchnumber = pac.batchnumber AND d.shippingOrderNumber = '" + NoPickingList + "' " +
                  "GROUP BY p.uom, m.model_name, d.uom, p.qty, p.itemId, pac.expired, p.detailNo, p.pickingList, d.batchnumber ";
            DataTable dt = new DataTable();
            db.OpenQuery(out dt, sql);
            return dt;
        }

        internal DataTable GetBatchScenned(string detailNo, String Barcode)
        {
            sql = "SELECT p.uom AS TYPE_ID, m.model_name AS PRODUCT_NAME_SC, d.uom AS UOM_SC, p.qty AS QTY_SC, p.itemId AS ID, pac.expired AS EXPIRE_DATE, "+
                  "p.detailNo AS DETAIL_NO_SC, p.pickingList AS PICKINGLIST_SC, d.batchnumber AS BATCH_SC " +
                  "FROM pickingList p, shippingOrder_detail d, product_model m, packaging_order pac " +
                  "WHERE p.detailNo = d.shippingOrderDetailNo AND d.productModelId = m.product_model " +
                  //"AND ((p.itemId = v.basketid) OR (p.itemId = v.innerBoxId) OR (p.itemId = v.innerBoxGsOneId) OR (p.itemId = v.capid) OR (p.itemId = v.gsonevialid)) " +
                  "AND d.batchnumber = pac.batchnumber AND p.detailNo = '" + detailNo + "' AND p.itemId = '" + Barcode + "' " +
                  "GROUP BY p.uom, m.model_name, d.uom, p.qty, p.itemId, pac.expired, p.detailNo, p.pickingList, d.batchnumber ";                   
            DataTable dt = new DataTable();
            db.OpenQuery(out dt, sql);
            return dt;
        }

        internal String GetLastOutbond()
        {
            sql = "SELECT TOP 1 m.shippingOrderNumber AS SHIPPING_ORDER_NUMBER, p.id AS ID_PIC " +
                  "FROM shippingOrder m, shippingOrder_detail d, pickingList p " +
                  "WHERE m.shippingtype ='2' AND m.flag=0 and m.status = 1 " +
                  "AND m.shippingOrderNumber = d.shippingOrderNumber AND d.shippingOrderDetailNo = p.detailNo " +
                  "ORDER BY p.id DESC ";
            DataTable dt = new DataTable();
            db.OpenQuery(out dt, sql);
            String ShippingOrderNo = "";
            if (dt.Rows.Count > 0){
                ShippingOrderNo = dt.Rows[0]["SHIPPING_ORDER_NUMBER"].ToString();
            }
            return ShippingOrderNo;
        }

        internal bool DeleteBatchScanned(String shippingOrderNumber, String ItemId, String DetailNo, String Pickinglist, String BatchNumber)
        {
            sql = "DELETE FROM pickingList " +
                  "WHERE itemId = '" + ItemId + "' AND detailNo = '" + DetailNo + "' AND pickinglist = '" + Pickinglist + "'";
            db.ExecuteSql(sql);

            sql = "UPDATE vaccine " +
                  "SET pickinglist = null, " +
                  "outbound_time = null " +
                  "WHERE pickinglist = '" + Pickinglist + "'";
            db.ExecuteSql(sql);

            sql = "UPDATE shippingOrder " +
                  "SET flag = 0 " +
                  "WHERE shippingOrderNumber = '" + shippingOrderNumber + "'";
            db.ExecuteSql(sql);

            sql = "UPDATE shippingOrder_detail " +
                  "SET flag = 0 " +
                  "WHERE shippingOrderDetailNo = '" + DetailNo + "'";
            db.ExecuteSql(sql);

            sql = "UPDATE pickingList_ERP_detail "+
                  "SET flag = 0 " +
                  "WHERE pickingList_ERP = '" + shippingOrderNumber + "' AND batchNumber = '" + BatchNumber + "'";
            return db.ExecuteSql2(sql);
        }

        public void InsertSystemLog(string EventName){
            db.eventname = EventName;
            db.systemlog();
        }
    }
}
