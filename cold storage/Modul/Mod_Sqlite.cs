﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Finisar.SQLite;
using System.IO;
using System.Windows.Forms;

namespace Cold_Storage
{
    class sqlitecs
    {
        private SQLiteConnection sql_con;
        private SQLiteCommand sql_cmd;
        private SQLiteDataAdapter DB;
        private SQLiteDataReader datareader;
        public DataTable dataHeader;
        private DataSet DS = new DataSet();
        private DataTable DT = new DataTable();
        public Dictionary<string, string> config = new Dictionary<string, string>();
        public int num_rows;
        public sqlitecs()
        {
            SetConnection();
            getConfig();
        }


        private void SetConnection()
        {
            sql_con = new SQLiteConnection
                ("Data Source=config.db;Version=3;New=False;Compress=True;");
        }

        private void ExecuteQuery(string txtQuery)
        {
            try
            {
                SetConnection();
                sql_con.Open();
                sql_cmd = sql_con.CreateCommand();
                sql_cmd.CommandText = txtQuery;
                sql_cmd.ExecuteNonQuery();
                sql_con.Close();
            }
            catch (SQLiteException sq)
            {
                simpanLog("Error : "+ sq.ToString());
                new Confirm("Error SQLLite : " +sq.ToString(), "Error");
            }
        }
        
        public void Add(string data)
        {
            string txtSQLQuery = "insert into  innerbox (innerbox) values ('" + data + "')";
            ExecuteQuery(txtSQLQuery);
        }

        public void insert(Dictionary<string,string> field, string table)
        {
            string sql = "INSERT INTO "+table+" (";
            int i = 0;
            foreach (string key in field.Keys)
            {
                sql += "'" + key + "'";
                if (i + 1 < field.Count)
                {
                    sql += ",";
                    i++;
                }
            }

            sql += ") values (";

            i = 0;
            foreach (string key in field.Values)
            {
                sql += "'" + key + "'";
                if (i + 1 < field.Count)
                {
                    sql += ",";
                    i++;
                }
            }
            sql += ")";
            simpanLog(sql);
            ExecuteQuery(sql);
        }

        private static void simpanLog(String line)
        {
            DateTime now = DateTime.Now;
            string tahun = now.ToString("yyyy");
            string bulan = now.ToString("MM");
            string hari = now.ToString("dd");
            string dates = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            line = dates + "\t" + line;
            // cek file 
            bool cekfile = false;
            while (!cekfile)
            {
                if (Directory.Exists("eventlog"))
                {
                    if (Directory.Exists("eventlog" + @"\" + tahun))
                    {
                        if (Directory.Exists("eventlog" + @"\" + tahun + @"\" + bulan))
                        {

                            using (StreamWriter outputFile = new StreamWriter("eventlog" + @"\" + tahun + @"\" + bulan + @"\" + hari + ".txt", true))
                            {
                                outputFile.WriteLine(line);
                            }
                            cekfile = true;
                        }
                        else
                        {

                            Directory.CreateDirectory("eventlog" + @"\" + tahun + @"\" + bulan);
                        }
                    }
                    else
                    {
                        Directory.CreateDirectory("eventlog" + @"\" + tahun);
                    }
                }
                else
                {

                    Directory.CreateDirectory("eventlog");
                }
            }
        }

        private void getConfig()
        {
            try
            {
                string sql = "Select config.portServer," +
                                "config.Servername," +
                                "config.ipLibringer," +
                                "config.portLibringer," +
                                "config.ipCamera," +
                                "config.portCamera," +
                                "config.ipDB," +
                                "config.portDB," +
                                "config.usernameDB," +
                                "config.passDB," +
                                "config.ipServer," +
                                "config.namadb," +
                                "config.namadb2," +
                                "config.ipdb2" +
                                " From config";
                sql_con.Open();
                sql_cmd = new SQLiteCommand(sql, sql_con);
                datareader = sql_cmd.ExecuteReader();
                while (datareader.Read())
                {
                    config.Add("portserver", datareader.GetValue(0).ToString());
                    config.Add("Servername", datareader.GetValue(1).ToString());
                    config.Add("ipLibringer", datareader.GetValue(2).ToString());
                    config.Add("portLibringer", datareader.GetValue(3).ToString());
                    config.Add("ipCamera", datareader.GetValue(4).ToString());
                    config.Add("portCamera", datareader.GetValue(5).ToString());
                    config.Add("ipDB", datareader.GetValue(6).ToString());
                    config.Add("portDB", datareader.GetValue(7).ToString());
                    config.Add("usernameDB", datareader.GetValue(8).ToString());
                    config.Add("passDB", datareader.GetValue(9).ToString());
                    config.Add("ipServer", datareader.GetValue(10).ToString());
                    config.Add("namadb", datareader.GetValue(11).ToString());
                    config.Add("namadb2", datareader.GetValue(12).ToString());
                    config.Add("ipdb2", datareader.GetValue(13).ToString());
                }
                sql_con.Close();
            }
            catch (SQLiteException sq)
            {
                new Confirm("Data Config Tidak Ditemukan");
            }
        }

        public List<string[]> getData(string data, string locat)
        {
            List<string> field = new List<string>();
            field.Add("*");
            string where = "basketid = '" + data + "' "; 
            if (locat.Length > 0)
                where = where + "AND warehouseid='" + locat + "'";
            List<string[]> Result = select(field, "warehouse", where);
            return Result;
        }

        public List<string[]> select(List<string> field, string table, string where)
        {
            try
            {
                string sql = "SELECT ";
                for (int j = 0; j < field.Count; j++)
                {
                    sql += field[j];
                    if (j + 1 < field.Count)
                    {
                        sql += ",";
                        j++;
                    }
                }
                sql += " From " + table;

                if (where.Length > 0)
                    sql += " WHERE " + where;

                int num = 0;
                simpanLog(sql);
                sql_con.Open();
                sql_cmd = new SQLiteCommand(sql, sql_con);
                datareader = sql_cmd.ExecuteReader();
                List<String[]> Results = new List<String[]>();

                while (datareader.Read())
                {
                    string[] data = new string[datareader.FieldCount];
                    for (int u = 0; u < datareader.FieldCount; u++)
                    {
                        data[u] = datareader.GetValue(u).ToString();
                    }

                    Results.Add(data);
                    num++;
                }
                num_rows = num;
                dataHeader = new DataTable();
                for (int i = 0; i < datareader.FieldCount; i++)
                {
                    dataHeader.Columns.Add(datareader.GetName(i));
                }
                sql_con.Close();
                return Results;
            }
            catch (InvalidOperationException ioe)
            {
                simpanLog("SQLITE LOCKED " + ioe.ToString());
                return null;
            }
        }

        public void update(Dictionary<string, string> field, string table, string where)
        {

            string sql = "UPDATE " + table + " SET";
            int i = 0;
            foreach (KeyValuePair<string, string> key in field)
            {
                sql += "'"+key.Key + "' = " + "'" + key.Value + "'";
                if (i + 1 < field.Count)
                {
                    sql += ",";
                    i++;
                }
            }
            if (where.Length > 0)
                sql += " WHERE " + where ;
            simpanLog(sql);
            ExecuteQuery(sql);
        }

        public void delete(string where, string table)
        {
            string sql = "DELETE FROM " + table;

            if (where.Length > 0)
                sql += " WHERE " + where;

            simpanLog(sql);
            ExecuteQuery(sql);
        }

    }
}
