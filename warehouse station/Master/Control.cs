﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Windows.Forms;

namespace Warehouse_Station
{
    public class Control
    {
        public DataTable tableReceiving;
        DataTable tableMovement;
        DataTable tableTransfer;
        DataTable tableTransferDetail;
        public List<string[]> datadetail;
        List<string[]> dataOrder = new List<string[]>();
        sqlitecs sq;
        dbaccess db;
        bool isSas;
        int quantityDetail,outboxQty;
        public string adminid;
        public List<string[]> datacombo;
        public Warehouse wr;
        List<string[]> datacomboCold;
        List<string[]> datacomboWare;
        List<string[]> datacomboOLD;
        List<string[]> datacomboMove;
        public string ActualQty;
        public string batchNumber;

        public Control()
        {
            db = new dbaccess();
            sq = new sqlitecs();
        }

        public void insialisasitable()
        {
            db.adminid = adminid;
            db.from = "Warehouse Station";
            db.eventname = "Start Station";
            db.systemlog();
            tableReceiving = new DataTable();
            tableReceiving.Columns.Add("ID");
            tableReceiving.Columns.Add("Timestamp");
            tableReceiving.Columns.Add("UOM");
            tableMovement = new DataTable();
            tableMovement.Columns.Add("ID");
            tableMovement.Columns.Add("Last Status");
            tableMovement.Columns.Add("Last Location");
            tableMovement.Columns.Add("UOM");
            tableTransferDetail = new DataTable();
            tableTransferDetail.Columns.Add("ID");
            tableTransferDetail.Columns.Add("Last Status");
            tableTransferDetail.Columns.Add("Last Location");
            tableTransferDetail.Columns.Add("UOM");
            tableTransferDetail.Columns.Add("Quantity");
            tableTransfer = new DataTable();
        }

        public void tblMovement()
        {
            tableMovement = new DataTable();
            tableMovement.Columns.Add("ID");
            tableMovement.Columns.Add("Last Status");
            tableMovement.Columns.Add("Last Location");
            tableMovement.Columns.Add("UOM");
            wr.lblQtyMovement.Text = "0";
            wr.dgvMovement.DataSource = tableMovement;
        }

        public void insialisasiReceiving()
        {
            tableReceiving = new DataTable();
            tableReceiving.Columns.Add("ID");
            tableReceiving.Columns.Add("Timestamp");
            tableReceiving.Columns.Add("UOM");
            wr.dgvReceiving.DataSource = tableReceiving;
            wr.lblQtyReceiving.Text = "" + wr.dgvReceiving.Rows.Count;
            // sq.delete("", "warehouse");
        }

        // ambil data Shipping Order
        public void getdataShippingOrder()
        {
            tableTransfer = new DataTable();
            List<string> field = new List<string>();
            field.Add("shippingOrder.shippingOrderNumber," +
                        "shippingOrder.customerName," +
                        "shippingOrder.shippingType," +
                        "shippingOrder.createdBy," +
                        "shippingOrder.status," +
                        "shippingOrder.createTime");
            string tab = "shippingOrder";

            List<string[]> ds = db.selectList(field, tab, "shippingtype ='1' and status ='1'");
            dataOrder = ds;
            if (db.num_rows > 0)
            {
                tableTransfer.Columns.Add("Warehouse Transfer Order Number");
                foreach (string[] Row in ds)
                {
                    DataRow row = tableTransfer.NewRow();
                    for (int k = 0; k < Row.Length; k++)
                    {
                        if (k == 0)
                            row[k] = Row[k];
                    }
                    tableTransfer.Rows.Add(row);
                }
                wr.dgvTransferOrder.DataSource = tableTransfer;
            }
            else
            {
                tableTransfer.Columns.Add("Shipping Order Number");
                wr.dgvTransferOrder.DataSource = tableTransfer;

            }
        }

        void closeTransfer()
        {
            wr.cmbDetail.Text = "";
            //wr.cmbDetail.Enabled = false;
            wr.lblOrder.Text = "";
            wr.lblQuantity.Text = "0";
            wr.lblActualQuantity.Text = "0";
            wr.lblProduct.Text = "";
        }

        //set combobox status Receiving
        public void cmb()
        {
            List<string> da = new List<string>();
            List<string> field = new List<string>();
            field.Add("warehouse.warehouseName,warehouse.warehouseId");
            datacombo = db.selectList(field, "[warehouse]", "owner = 'Packaging'");
            datacomboOLD = datacombo;
            List<string> das = new List<string>();
            List<string> das1 = new List<string>();
            for (int i = 0; i < datacombo.Count; i++)
            {
                das.Add(datacombo[i][0]);
                das1.Add(datacombo[i][0]);
            }
            wr.cmbPilih.DataSource = das;
            wr.cmbStatusOld.DataSource = das1;
        }

        //set combobox location receiving
        public void setLocationReceiving(int seletedindex)
        {
            List<string> da = new List<string>();
            List<string> field = new List<string>();
            field.Add("warehouse_location.locationName,warehouse_location.locationId");
            string where = "warehouse_location.warehouseId = '" + datacombo[seletedindex][1] + "' AND warehouse_location.status = '0' AND warehouse_location.status = '0'";
            datacomboWare = db.selectList(field, "[warehouse_location]", where);
            List<string> das = new List<string>();
            for (int i = 0; i < datacomboWare.Count; i++)
            {
                das.Add(datacomboWare[i][0]);
            }
            wr.cmbLocation.DataSource = das;
        }

        //set combobox new location
        public void setNewLocation(int seletedindex)
        {
            List<string> da = new List<string>();
            List<string> field = new List<string>();
            field.Add("warehouse_location.locationName,warehouse_location.locationId");
            string where = "warehouse_location.warehouseId = '" + datacombo[seletedindex][1] + "' AND warehouse_location.status = '0' AND warehouse_location.status = '0'";
            datacomboMove = db.selectList(field, "[warehouse_location]", where);
            List<string> das = new List<string>();
            for (int i = 0; i < datacomboMove.Count; i++)
            {
                das.Add(datacomboMove[i][0]);
            }
            wr.cmbMove.DataSource = das;
        }

        bool cekdgvReceiving(string data)
        {
            for (int i = 0; i < wr.dgvReceiving.Rows.Count; i++)
            {
                if (data.Equals(wr.dgvReceiving.Rows[i].Cells[0].Value.ToString()))
                {
                    return true;
                }
            }
            return false;

        }
        string uom;
        //add data to receiving 
        public void tambahData(string dataBarcode)
        {
            if (ceklocation(dataBarcode))
            {
                if (!cekdgvReceiving(dataBarcode))
                {
                    if (cekbasketSqlite(dataBarcode))
                    {
                        if (cekproduct(dataBarcode))
                        {
                            if (cekTransfer(dataBarcode))
                            {
                                db.Movement("", "", "Warehouse Packaging", "1", dataBarcode);
                                string dates = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                                ReceiveToDgv(dataBarcode, dates);
                            }
                            else
                            {
                                Confirm cf = new Confirm("The Basket Has Been Transfer.", "Information", MessageBoxButtons.OK);
                            }

                        }
                        else
                        {
                            Confirm cf = new Confirm("Data Not Found.", "Information", MessageBoxButtons.OK);
                        }
                    }
                    else
                    {
                        Confirm cf = new Confirm("Data Already Exist.", "Information", MessageBoxButtons.OK);
                    }
                }
                else
                {
                    Confirm cf = new Confirm("Data Already Exist.", "Information", MessageBoxButtons.OK);
                }
            }
            else
            {
                Confirm cf = new Confirm("The Basket Already Enter To The Cold Room.", "Information", MessageBoxButtons.OK);
            }
        }

        public void ReceiveToDgv(string databarcode, string dates)
        {
            DataRow row = tableReceiving.NewRow();
            row[0] = databarcode;
            row[1] = dates;
            row[2] = uom;
            tableReceiving.Rows.Add(row);

            wr.dgvReceiving.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            wr.dgvReceiving.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            wr.dgvReceiving.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;

            wr.dgvReceiving.DataSource = tableReceiving;
            wr.lblQtyReceiving.Text = "" + wr.dgvReceiving.Rows.Count;
        }

        public void ReceiveToSqlite(string databarcode, string dates)
        {
            DataRow row = tableReceiving.NewRow();
            Dictionary<string, string> field = new Dictionary<string, string>();
            field.Add("id", databarcode);
            field.Add("timestamp", dates);
            field.Add("locationid", datacomboWare[wr.cmbLocation.SelectedIndex][1]);
            field.Add("warehouseid", datacombo[wr.cmbPilih.SelectedIndex][1]);
            field.Add("uom", uom);
            sq.insert(field, "warehouse");

        }

        bool cekproduct(string databarcode)
        {
            if (cekbasket(databarcode))
            {
                uom = "Basket";
                return true;
            }
            if (cekInfeed(databarcode))
            {
                uom = "Infeed";
                return true;
            }
            if (cekblister(databarcode))
            {
                uom = "Blister";
                return true;
            }
            if (cekVial(databarcode))
            {
                uom = "Vaccine";
                return true;
            }
            uom = "";
            return false;

        }

        private bool ceklocation(string dataBarcode)
        {
            List<string> field = new List<string>();
            field.Add("basketid");
            string where = "basketid ='" + dataBarcode + "' and lastLocationId is null group by basketid";
            List<string[]> ds = db.selectList(field, "[Vaccine]", where);
            if (db.num_rows > 0)
                return true;

            return false;
        }

        private bool cekTransfer(string dataBarcode)
        {
            List<string> field = new List<string>();
            field.Add("itemId");
            string where = "itemId ='" + dataBarcode + "'";
            List<string[]> ds = db.selectList(field, "[pickingList]", where);
            if (db.num_rows < 1)
                return true;

            return false;
        }

        private bool cekbasket(string dataBarcode)
        {
            List<string> field = new List<string>();
            field.Add("basketid");
            string where = "basketid ='" + dataBarcode + "'";
            List<string[]> ds = db.selectList(field, "basket", where);
            if (db.num_rows > 0)
                return true;

            return false;
        }

        private bool cekblister(string dataBarcode)
        {
            List<string> field = new List<string>();
            field.Add("blisterid");
            string where = "blisterid ='" + dataBarcode + "'";
            List<string[]> ds = db.selectList(field, "blisterpack", where);
            if (db.num_rows > 0)
                return true;

            return false;
        }

        private bool cekVial(string dataBarcode)
        {
            List<string> field = new List<string>();
            field.Add("capid");
            string where = "capid ='" + dataBarcode + "' OR " + "gsonevialid ='" + dataBarcode + "'";
            List<string[]> ds = db.selectList(field, "vaccine", where);
            if (db.num_rows > 0)
                return true;

            return false;
        }

        private bool cekInfeed(string dataBarcode)
        {
            List<string> field = new List<string>();
            field.Add("infeedInnerBoxId");
            string where = "infeedInnerBoxId ='" + dataBarcode + "' OR " + "gsOneInnerBoxId ='" + dataBarcode + "'";
            List<string[]> ds = db.selectList(field, "innerbox", where);
            if (db.num_rows > 0)
                return true;

            return false;
        }

        private bool cekbasketSqlite(string dataBarcode)
        {
            List<string> field = new List<string>();
            field.Add("id");
            string where = "id ='" + dataBarcode + "'";
            List<string[]> ds = sq.select(field, "warehouse", where);
            db.fieldList = new List<string>();
            if (sq.num_rows > 0)
                return false;
            return true;
        }

        //insert tabel to database
        public void submitReceiving()
        {
            //List<string> fieldList = new List<string>();
            //fieldList.Add("warehouse.basketid,warehouse.warehouseid,warehouse.locationid,warehouse.timestamp");
            //List<string[]> ds = sq.select(fieldList, "warehouse", "");
            //if (sq.num_rows > 0)
            //{
            //    Dictionary<string, string> field;
            //    string loc="";
            //    foreach(string [] da in ds)
            //    {
            //        db.adminid = adminid;
            //        db.from = "Basket Station";
            //        db.to = "warehouse Station";
            //        db.eventname = "Receive " + da[0];
            //        db.systemlog();
            //        updateStatusLastLocation(da[0], da[2]);
            //        loc = da[2];
            //        log("Data : " + da[0]);
            //    }
            //    log("Set Location : " + loc);
            //    Confirm cf = new Confirm("Data success saved!", "Information", MessageBoxButtons.OK);
            Confirm cf = new Confirm("Are you sure? ", "Confirmation");
            bool hasilDialog = cf.conf();
            if (hasilDialog)
            {
                for (int i = 0; i < wr.dgvReceiving.Rows.Count; i++)
                {
                    Dictionary<string, string> field = new Dictionary<string, string>();
                    field.Add("id", wr.dgvReceiving.Rows[i].Cells[0].Value.ToString());
                    field.Add("timestamp", wr.dgvReceiving.Rows[i].Cells[1].Value.ToString());
                    field.Add("uom", wr.dgvReceiving.Rows[i].Cells[2].Value.ToString());
                    field.Add("locationid", datacomboWare[wr.cmbLocation.SelectedIndex][1]);
                    field.Add("warehouseid", datacombo[wr.cmbPilih.SelectedIndex][1]);
                    sq.insert(field, "warehouse");
                    updateVaccine(wr.dgvReceiving.Rows[i].Cells[2].Value.ToString(), wr.dgvReceiving.Rows[i].Cells[0].Value.ToString());
                }
                //sq.clearWarehouse();
                insialisasiReceiving();
                cf = new Confirm("Success!", "Information", MessageBoxButtons.OK);
            }

        }


        void updateVaccine(string uom, string databarcode)
        {
            string cari = "";
            if (uom.Equals("Basket"))
            {
                cari = "basketid ='" + databarcode + "'";
            }
            else if (uom.Equals("Infeed"))
            {
                cari = "innerboxid ='" + databarcode + "' OR innerBoxGsOneId ='" + databarcode + "'";
            }
            else if (uom.Equals("Blister"))
            {
                cari = "blisterpackid='" + databarcode + "'";
            }
            else if (uom.Equals("Vaccine"))
            {
                cari = "capid ='" + databarcode + " OR gsonevialid ='" + databarcode + "'";
            }
            Dictionary<string, string> field = new Dictionary<string, string>();
            field.Add("lastLocationId", datacomboWare[wr.cmbLocation.SelectedIndex][1]);
            db.update(field, "vaccine", cari);
        }

        void updateVaccineMov(string uom, string databarcode)
        {
            string cari = "";
            if (uom.Equals("Basket"))
            {
                cari = "basketid ='" + databarcode + "'";
            }
            else if (uom.Equals("Infeed"))
            {
                cari = "innerboxid ='" + databarcode + "' OR innerBoxGsOneId ='" + databarcode + "'";
            }
            else if (uom.Equals("Blister"))
            {
                cari = "blisterpackid='" + databarcode + "'";
            }
            else if (uom.Equals("Vaccine"))
            {
                cari = "capid ='" + databarcode + " OR gsonevialid ='" + databarcode + "'";
            }
            Dictionary<string, string> field = new Dictionary<string, string>();
            field.Add("lastLocationId", datacomboMove[wr.cmbMove.SelectedIndex][1]);
            db.update(field, "vaccine", cari);
        }
        public void updateStatusLastLocation(string id, string status)
        {
            Dictionary<string, string> field = new Dictionary<string, string>();
            field.Add("lastlocationid", status);
            string where = "basketid ='" + id + "'";
            db.update(field, "vaccine", where);
            field = new Dictionary<string, string>();

            field.Add("locationid", status);
            db.update(field, "basket", where);
        }
        // get selected shipping set detail
        public void getOrderDetail(int ordersItem)
        {
            wr.lblOrder.Text = dataOrder[ordersItem][0];
            List<string> field = new List<string>();
            field.Add("shippingOrder_detail.productModelId,product_model.model_name,shippingOrder_detail.qty,shippingOrder_detail.shippingOrderDetailNo,dbo.shippingOrder_detail.batchnumber,dbo.product_model.isSas,dbo.product_model.outbox_qty");
            string table = "shippingOrder_detail INNER JOIN product_model ON shippingOrder_detail.productModelId = product_model.product_model";
            string where = "shippingOrder_detail.shippingOrderNumber = '" + dataOrder[ordersItem][0] + "' AND shippingOrder_detail.status = '0'";
            datadetail = db.selectList(field, table, where);
            if (db.num_rows > 0)
            {
                List<string> productDetail = new List<string>();

                for (int i = 0; i < datadetail.Count; i++)
                {
                    productDetail.Add(datadetail[i][4]);
                }
                wr.cmbDetail.Enabled = true;
                wr.txtTransfer.Enabled = true;
                wr.cmbDetail.DataSource = productDetail;
                wr.lblProduct.Text = datadetail[0][1];
                if (datadetail[0][6] != null)
                {
                    outboxQty = int.Parse(datadetail[0][6]);

                }
                else {
                    outboxQty = 0;

                }
                if (datadetail[0][5] == "True") {
                    isSas = true;
                    wr.isSas = true;
                    wr.lblActualQuantity.Text = cekSasQty(datadetail[0][4], wr.txtTransfer.Text.Replace("\n", "")).ToString();
                }else{
                    wr.isSas = false;
                    isSas = false;
                }
            }
        }

        // get selected shipping set detail parameter string
        public void getOrderDetail(string ordersItemIndex)
        {
            wr.lblOrder.Text = ordersItemIndex;
            List<string> field = new List<string>();
            field.Add("shippingOrder_detail.productModelId,product_model.model_name,shippingOrder_detail.qty,shippingOrder_detail.shippingOrderDetailNo,dbo.shippingOrder_detail.batchnumber,dbo.product_model.isSas,dbo.product_model.outbox_qty");
            string table = "shippingOrder_detail INNER JOIN product_model ON shippingOrder_detail.productModelId = product_model.product_model";
            string where = "shippingOrder_detail.shippingOrderNumber = '" + ordersItemIndex + "' AND shippingOrder_detail.status = '0'";
            datadetail = db.selectList(field, table, where);
            if (db.num_rows > 0)
            {
                List<string> productDetail = new List<string>();

                for (int i = 0; i < datadetail.Count; i++)
                {
                    productDetail.Add(datadetail[i][4]);
                }
                wr.cmbDetail.Enabled = true;
                wr.txtTransfer.Enabled = true;
                wr.cmbDetail.DataSource = productDetail;
                wr.lblProduct.Text = datadetail[0][1];
                
                if (datadetail[0][6] != null)
                {
                    outboxQty = int.Parse(datadetail[0][6]);

                }
                else
                {
                    outboxQty = 0;

                }
                if (datadetail[0][5] == "True")
                {
                    isSas = true;
                    wr.isSas = true;
                    wr.lblActualQuantity.Text = cekSasQty(datadetail[0][4], wr.txtTransfer.Text.Replace("\n", "")).ToString();
                }
                else
                {
                    isSas = false;
                    wr.isSas = false;
                    wr.lblActualQuantity.Text = cekPickingList(ordersItemIndex).ToString();

                }
            }
        }

        public int cekSasQty(string batchnumber,string barcode) {

            List<string> field = new List<string>();
            field.Add("sum(qty)");
            string where = "detailno='"+wr.lblDetailNumber.Text+"'";
            List<string[]> ds = db.selectList(field, "pickinglist", where);
            if (db.num_rows > 0)
            {
                try
                {
                    return int.Parse(ds[0][0]) ;
                }
                catch (Exception ex)
                {
                    return 0;
                }
            }
            return 0;
        }

        //add and get data to table movment
        public void getdataMovement(string datamove)
        {
            if (cekproduct(datamove))
            {
                string status = datacomboOLD[wr.cmbStatusOld.SelectedIndex][1];
                List<string[]> res = getData(datamove, status);
                if (res.Count > 0)
                {
                    string dataLocation = getNamaLokasi(res[0][1]);
                    string stat = getStatus(res[0][2]);
                    //wr.lblBasketId.Text = res[0][3];
                    wr.lblBasketId.Text = "Basket";
                    wr.lblLastLocation.Text = dataLocation;
                    wr.lblLastWarehouse.Text = res[0][2];
                    string dates = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                    // ReceiveToSqlite(res[0][0], dates);
                    //MovetToDGV(datamove, dataLocation, dates, res[0][3]);
                    MovetToDGV(datamove, dataLocation, dates, "Basket", stat);
                }
                else
                {
                    log("Data basket not yet received!");
                    Confirm cf = new Confirm("Data basket not yet received!", "Information", MessageBoxButtons.OK);
                }
            }
            else
            {
                Confirm cf = new Confirm("Data not found", "Information", MessageBoxButtons.OK);
            }
            wr.txtBasketMove.Text = "";
        }

        private string getNamaLokasi(string p)
        {
            List<string> field = new List<string>();
            field.Add("locationName");
            string where = "locationId = '" + p + "'";
            List<string[]> ds = db.selectList(field, "warehouse_location", where);
            if (db.num_rows > 0)
                return ds[0][0];
            return "";
        }

        private string getStatus(string p)
        {
            List<string> field = new List<string>();
            field.Add("warehouseName");
            string where = "warehouseId = '" + p + "'";
            List<string[]> ds = db.selectList(field, "warehouse", where);
            if (db.num_rows > 0)
                return ds[0][0];
            return "";
        }

        public void MovetToDGV(string databarcode, string dataloc, string dates, string dataUom, string stat)
        {
            bool found = false;
            foreach (DataGridViewRow row in wr.dgvMovement.Rows)
            {
                if (row.Cells[0].Value.ToString().Equals(databarcode))
                {
                    found = true;
                    Confirm cf = new Confirm("Data Already Exist", "Information", MessageBoxButtons.OK);
                    break;
                }
            }

            if (!found)
            {
                DataRow rows = tableMovement.NewRow();
                rows[0] = databarcode;
                rows[1] = stat;
                rows[2] = dataloc;
                rows[3] = dataUom;
                tableMovement.Rows.Add(rows);

                wr.dgvMovement.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                wr.dgvMovement.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                wr.dgvMovement.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                wr.dgvMovement.Columns[3].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;



                wr.dgvMovement.DataSource = tableMovement;
            }

            
        }

        public List<string[]> getData(string data, string locat)
        {
            List<string> field = new List<string>();
            field.Add("warehouse.id,warehouse.locationid,warehouse.warehouseid,warehouse.uom");
            string where = "id = '" + data + "' ";
            //if (locat.Length > 0)
            //    where = where + "AND warehouse_location.warehouseid='" + locat + "'";
            List<string[]> Result = sq.select(field, "warehouse", where);
            return Result;
        }

        //public List<string[]> getData(string data, string locat)
        //{
        //    List<string> field = new List<string>();
        //    field.Add("a.basketId,  a.lastLocationId, c.warehouseId");
        //    string where = "a.basketId = '" + data + "' and lastLocationId is not null";
        //    List<string[]> Result = db.selectList(field, "[Development_Packaging].[dbo].[Vaccine] a inner join warehouse_location b on a.lastLocationId = b.locationId inner join warehouse c on b.warehouseId = c.warehouseId", where);
        //    return Result;
        //}


        //get product model code from gtin
        private string cekGtin(string gtin)
        {
            List<string> field = new List<string>();
            field.Add("product_model.product_model");
            string where = "product_model.gtin = '" + gtin + "'";
            List<string[]> ds = db.selectList(field, "product_model", where);
            if (db.num_rows > 0)
                return ds[0][0];
            return "";
        }

        //add transfer detail
        public void tambahdetail(string basketid)
        {
            //tutup ganti gak usah cek data nya ke sqlite, cukup cek laslocationid nya keisi apa enggak aja
            //List<string[]> res = sq.getData(basketid, "");
            //if (res.Count > 0)
            List<string> field = new List<string>();
            field.Add("distinct basketid, lastlocationid");
            string where = "basketid = '" + basketid + "' and lastlocationid is not null";
            List<string[]> ds = db.selectList(field, "vaccine", where);
            if (db.num_rows > 0 && ds != null)

            //if(db.getDataWarehouse(basketid))
            {
                //cek model 
                //  string gtin = basketid.Substring(2, 14);
                // string productCek = datadetail[wr.cmbDetail.SelectedIndex][0];
                if (cekProductModel("", basketid))
                {
                    Confirm cf = new Confirm("Send " + basketid + " \nAre you sure?", "Confirmation");
                    bool hasilDialog = cf.conf();
                    if (hasilDialog)
                    {
                        bool found = false;
                        foreach (DataGridViewRow row in wr.dgv.Rows)
                        {
                            if (row.Cells[0].Value.ToString().Equals(basketid))
                            {
                                found = true;
                                cf = new Confirm("Data Already Exist", "Information", MessageBoxButtons.OK);
                                break;
                            }
                        }

                        if (!found)
                        {
                            //updateTableDetail(res[0][0], res[0][1], "" + banyakVaccine, res[0][2]);
                            updateTableDetail(ds[0][0], "", "" + banyakVaccine, ds[0][1]);

                            //cek actual qty ke picking list
                            //addPickingList(basketid, "" + banyakVaccine, wr.lblDetailNumber.Text, res[0][4]);
                            addPickingList(basketid, "" + banyakVaccine, wr.lblDetailNumber.Text, uomProduct);

                            wr.lblActualQuantity.Text = "" + banyakVaccine;
                            cekMemenuhiDetailOrder(wr.lblDetailNumber.Text, wr.cmbDetail.Text);
                            cekMemenuhiOrder(wr.lblOrder.Text);
                            getOrderDetail(wr.lblOrder.Text);
                            getdataShippingOrder();
                            sq.delete("id ='" + basketid + "'", "warehouse");

                        }
                    }
                    else
                    {
                        new Confirm("Data " + basketid + " not send", "Information", MessageBoxButtons.OK);
                    }
                }
                else
                {
                    new Confirm("Data Not Found", "Information", MessageBoxButtons.OK);
                }
            }
            else
            {
                Confirm cf = new Confirm("Data Not Found at Warehouse", "Information", MessageBoxButtons.OK);
                wr.txtTransfer.Text = "";
            }
        }
        int banyakVaccine;
        string uomProduct;

        public void SetForceClose()
        {
            setQtyWTO();
            cekMemenuhiDetailOrder(wr.lblDetailNumber.Text, wr.cmbDetail.Text);
            cekMemenuhiOrder(wr.lblOrder.Text);
            getOrderDetail(wr.lblOrder.Text);
            getdataShippingOrder();

        }

        private void setQtyWTO()
        {
            Dictionary<string, string> fieldAct = new Dictionary<string, string>();
            fieldAct.Add("qty", ActualQty);
            string where = "dbo.shippingOrder_detail.shippingOrderDetailNo = '" + wr.lblDetailNumber.Text + "' AND dbo.shippingOrder_detail.batchnumber = '" + batchNumber + "'";
            db.update(fieldAct, "shippingOrder_detail", where);

            where = "receivingOrderDetailNo = '" + wr.lblDetailNumber.Text + "' AND batchnumber = '" + batchNumber + "'";
            db.update(fieldAct, "receivingOrder_detail", where);
        }


        public void updateTableDetail(string a, string b,string c, string d)
        {
            string dataLocation = getNamaLokasi(d);
            DataRow row = tableTransferDetail.NewRow();
            row[0] = a;
            string loc;
            if (b.Equals("0"))
                row[1] = "Quarantine";
            else
                row[1] = "Release";
            row[2] = dataLocation;
            row[3] = uomProduct;
            row[4] = c;
            tableTransferDetail.Rows.Add(row);
            wr.dgv.DataSource = tableTransferDetail;

            wr.dgv.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            wr.dgv.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            wr.dgv.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            wr.dgv.Columns[3].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            wr.dgv.Columns[4].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;

            //datagrid has calculated it's widths so we can store them
            for (int i = 0; i <= wr.dgv.Columns.Count - 1; i++)
            {
                //store autosized widths
                int colw = wr.dgv.Columns[i].Width;
                //remove autosizing
                wr.dgv.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
                //set width to calculated by autosize
                wr.dgv.Columns[i].Width = colw;
            }

        }
        public int cekPickingList(string shippingorderDetail)
        {
            List<string> field = new List<string>();
            field.Add("sum(qty)");
            string where = "dbo.pickingList.detailNo ='" + shippingorderDetail + "'";
            List<string[]> ds = db.selectList(field, "pickinglist", where);
            if (db.num_rows > 0)
            {
                try
                {
                    return int.Parse(ds[0][0]);
                }
                catch (Exception ex)
                {
                    return 0;
                }
            }
            return 0;
        }

        private void cekMemenuhiDetailOrder(string shippingorder, string batch)
        {
            List<string> field = new List<string>();
            field.Add("shippingOrder_detail.qty");
            string where = "dbo.shippingOrder_detail.shippingOrderDetailNo = '" + shippingorder + "' AND dbo.shippingOrder_detail.batchnumber = '" + batch + "'";
            List<string[]> ds = db.selectList(field, "shippingOrder_detail", where);
            if (db.num_rows > 0)
            {
                try
                {
                    int qtyDetailShipping = int.Parse(ds[0][0]);
                    int cekQtyPickinglist = cekPickingList(shippingorder);
                    if (cekQtyPickinglist >= qtyDetailShipping)
                    {
                        Dictionary<string, string> fieldAct = new Dictionary<string, string>();
                        fieldAct.Add("status", "1");
                        db.update(fieldAct, "shippingOrder_detail", where);
                        db.deleteTempVial(batch);
                        log("Batcnumber " + batch + " Transfered");
                        
                    }
                }
                catch (Exception ex)
                {
                    db.simpanLog(ex.Message);
                    // log("Error :" + ex.Message);
                }

            }
        }

        private void cekMemenuhiOrder(string shippingorder)
        {
            List<string> field = new List<string>();
            field.Add("count(*)");
            string where = "dbo.shippingOrder_detail.shippingOrderNumber = '" + shippingorder + "' AND dbo.shippingOrder_detail.status = '0'";
            List<string[]> ds = db.selectList(field, "shippingOrder_detail", where);
            if (db.num_rows > 0)
            {
                int banyakDetailNotFull = int.Parse(ds[0][0]);
                if (banyakDetailNotFull == 0)
                {
                    Dictionary<string, string> fieldAct = new Dictionary<string, string>();
                    fieldAct.Add("status", "2");
                    where = "shippingOrderNumber = '" + shippingorder + "'";
                    db.update(fieldAct, "shippingOrder", where);
                    log("Shipping Order " + shippingorder + " Transfered");

                    closeTransfer();
                    tambahDataReceiving(shippingorder);
                }
                else
                {
                    wr.SetDetailBatch();
                }
            }
        }

        private void tambahDataReceiving(string shippingorder)
        {
            //ambil data shipping order
            List<string> field = new List<string>();
            field.Add("dbo.shippingOrder.shippingOrderNumber,dbo.shippingOrder.customerId,dbo.shippingOrder.customerName,dbo.shippingOrder.shippingType,dbo.shippingOrder.createTime,dbo.shippingOrder.createdBy");
            string where = "dbo.shippingOrder.shippingOrderNumber = '" + shippingorder + "'";
            List<string[]> ds1 = db.selectList(field, "shippingOrder", where);
            if (db.num_rows > 0)
            {
                //tambah data shipping order ke receiving
                Dictionary<string, string> fieldInsert = new Dictionary<string, string>();
                fieldInsert.Add("receivingOrderNumber", ds1[0][0]);
                fieldInsert.Add("supplierId", ds1[0][1]);
                fieldInsert.Add("supplierName", ds1[0][2]);
                fieldInsert.Add("receivingType", ds1[0][3]);
                fieldInsert.Add("createdTime", ds1[0][4]);
                fieldInsert.Add("createdBy", ds1[0][5]);
                fieldInsert.Add("status", "0");
                db.insert(fieldInsert, "receiving_order");

                //ambil data shipping order detail
                field = new List<string>();
                field.Add("dbo.shippingOrder_detail.shippingOrderDetailNo,dbo.shippingOrder_detail.shippingOrderNumber,dbo.shippingOrder_detail.productModelId,dbo.shippingOrder_detail.qty,dbo.shippingOrder_detail.uom,dbo.shippingOrder_detail.batchnumber");
                where = "dbo.shippingOrder_detail.shippingOrderNumber = '" + shippingorder + "'"; List<string[]> ds = db.selectList(field, "shippingOrder", where);
                ds = db.selectList(field, "shippingOrder_detail", where);
                if (db.num_rows > 0)
                {
                    //tambah data shipping order detail ke receving detail
                    for (int i = 0; i < ds.Count; i++)
                    {
                        fieldInsert = new Dictionary<string, string>();
                        fieldInsert.Add("receivingOrderDetailNo", ds[i][0]);
                        fieldInsert.Add("receivingOrderNumber", ds[i][1]);
                        fieldInsert.Add("productModelId", ds[i][2]);
                        fieldInsert.Add("qty", ds[i][3]);
                        fieldInsert.Add("uom", ds[i][4]);
                        fieldInsert.Add("batchnumber", ds[i][5]);
                        fieldInsert.Add("status", "0");
                        db.insert(fieldInsert, "receivingOrder_detail");
                        cleanDetail();
                    }
                }
            }



        }

        private void cleanDetail()
        {
            wr.lblOrder.Text = "";
            List<string> productDetail = new List<string>();
            wr.cmbDetail.DataSource = productDetail;
            wr.lblDetailNumber.Text = "";
            wr.lblProduct.Text = "";
            wr.lblQuantity.Text = "";
            wr.lblActualQuantity.Text = "";
        }

        private bool cekProductModel(string productmodel, string databarcode)
        {
            string cari = "";
            cekproduct(databarcode);
            List<string> field = new List<string>();
            field.Add("productmodelid,substraction");            
            if (uom.Equals("Basket"))
            {
                uomProduct = "Basket";
                cari = "basketid ='" + databarcode + "'";
            }
            else if (uom.Equals("Infeed"))
            {
                uomProduct = "Infeed";
                cari = "innerboxid ='" + databarcode + "' OR innerBoxGsOneId ='" + databarcode + "'";
            }
            else if (uom.Equals("Blister"))
            {
                uomProduct = "Blister";
                cari = "blisterpackid='" + databarcode + "'";
            }
            else if (uom.Equals("Vaccine"))
            {

                uomProduct = "Vaccine";
                cari = "capid ='" + databarcode + " OR gsonevialid ='" + databarcode + "'";
            }
            cari = cari + " AND batchnumber ='" + datadetail[wr.cmbDetail.SelectedIndex][4] + "' and isReject = 0";
            updateVaccineLocation(cari);
            List<string[]> ds = db.selectList(field, "vaccine", cari);
            if (db.num_rows > 0)
            {
                banyakVaccine = 0;
                if (isSas)
                {                    
                    for (int i = 0; i < ds.Count; i++)
                    {                        
                        banyakVaccine += (1 * outboxQty) - int.Parse(ds[i][1]);
                    }
                }
                else 
                {
                    banyakVaccine = ds.Count;

                }
                return true;
            }
            return false;
        }

        private void updateVaccineLocation(string cari)
        {
            Dictionary<string, string> field = new Dictionary<string, string>();
            field.Add("lastLocationId", "null");
            db.update(field, "vaccine", cari);
        }

        //get count vaccine
        public int cekbanyakInfeed(string basketid)
        {
            List<string> field = new List<string>();
            field.Add("count(Vaccine.capId)");
            string tabl = "Vaccine LEFT JOIN dbo.blisterPack ON dbo.Vaccine.blisterpackId = dbo.blisterPack.blisterpackId LEFT JOIN dbo.innerBox ON dbo.blisterPack.innerBoxId = dbo.innerBox.infeedInnerBoxId left JOIN dbo.basket ON dbo.innerBox.basketId = dbo.basket.basketId";
            string where = "basket.basketId = '" + basketid + "'";
            List<string[]> ds = db.selectList(field, tabl, where);
            if (db.num_rows > 0)
                return int.Parse(ds[0][0]);
            else
                return 0;
        }

        //add pickinglist
        private void addPickingList(string itemid, string qty, string detailno, string uom)
        {
            Dictionary<string, string> field = new Dictionary<string, string>();
            string dates = DateTime.Now.ToString("ssyyMMHHmm");
            field.Add("itemid", itemid);
            field.Add("qty", qty);
            field.Add("detailno", detailno);
            field.Add("status", "0");
            field.Add("pickinglist", dates);
            field.Add("uom", uom);
            db.Movement("", "", "Warehouse Transfer", "1", itemid);
            db.insert(field, "pickinglist");

        }

        // set status old 
        //public void cmbstatusold()
        //{
        //    if (wr.cmbStatusOld.Text.Equals("Release"))
        //    {
        //        List<string> das = new List<string>();
        //        das.Add("Release");
        //        wr.cmbWare.DataSource = das;
        //    }
        //    else
        //    {
        //        List<string> das = new List<string>();
        //        das.Add("Quarantine");
        //        das.Add("Release");
        //        wr.cmbWare.DataSource = das;
        //    }
        //}

        //set combo box detail
        public void setdetail()
        {
            wr.lblQuantity.Text = datadetail[wr.cmbDetail.SelectedIndex][2];
            wr.lblDetailNumber.Text = datadetail[wr.cmbDetail.SelectedIndex][1];
        }

        //set combobox movement Location
        public void setcomboWare(int selectedindex)
        {
            List<string> da = new List<string>();
            List<string> field = new List<string>();
            field.Add("warehouse_location.locationName,warehouse_location.locationId");
            string where = "warehouse_location.warehouseId = '" + datacombo[selectedindex][1] + "' AND warehouse_location.status = '0'";
            datacomboOLD = db.selectList(field, "[warehouse_location]", where);
            List<string> das = new List<string>();
            for (int i = 0; i < datacomboOLD.Count; i++)
            {
                das.Add(datacomboOLD[i][0]);
            }
            wr.cmbMove.DataSource = das;
        }

        public void log(string data)
        {
            try
            {
                string dates = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                string simpan = dates + "\t" + data + Environment.NewLine;
                wr.txtLog.AppendText(simpan);
            }
            catch (ObjectDisposedException ob)
            {

            }
        }
        internal string getAdminName(string admin)
        {
            List<string> field = new List<string>();
            field.Add("first_name   ");
            string where = "id ='" + admin + "'";
            List<string[]> ds = db.selectList(field, "[USERS]", where);
            if (db.num_rows > 0)
                return ds[0][0];
            else
                return "";
        }
        public void simpanMove()
        {
            // Confirm cf = new Confirm("Are you sure? ", "Confirmation");
            //bool hasilDialog = cf.conf();
            //if (hasilDialog)
            //{

            Confirm cf = new Confirm("Are you sure? ", "Confirmation");
            bool hasilDialog = cf.conf();
            if (hasilDialog)
            {
                for (int i = 0; i < wr.dgvMovement.Rows.Count; i++)
                {
                    Dictionary<string, string> field = new Dictionary<string, string>();
                    field.Add("locationid", datacomboMove[wr.cmbMove.SelectedIndex][1]);
                    field.Add("warehouseid", datacombo[wr.cmbStatusOld.SelectedIndex][1]);
                    string where = "id ='"+wr.dgvMovement.Rows[i].Cells[0].Value.ToString()+"'";
                    sq.update(field, "warehouse",where);
                    updateVaccineMov(wr.dgvMovement.Rows[i].Cells[3].Value.ToString(), wr.dgvMovement.Rows[i].Cells[0].Value.ToString());
                    
                }
                tblMovement();
                cf = new Confirm("Success!", "Information", MessageBoxButtons.OK);
            }
            //List<string> fieldList = new List<string>();
            //fieldList.Add("warehouse.id,warehouse.warehouseid,warehouse.locationid,warehouse.timestamp");
            //List<string[]> ds = sq.select(fieldList, "warehouse", "");
            //if (sq.num_rows > 0)
            //{
            //    Dictionary<string, string> field;
            //    string loc = "";
            //    foreach (string[] da in ds)
            //    {
            //        updateStatusLastLocation(da[0], datacomboWare[wr.cmbMove.SelectedIndex][1]);
            //        loc = da[2];
            //        log("Data : " + da[0]);
            //    }
            //    log("Set Location : " + loc);
            //    new Confirm("Data success saved!", "Information", MessageBoxButtons.OK);
            //    insialisasiReceiving();
            //}
            //for (int i = 0; i < wr.dgvMovement.Rows.Count; i++)
            //{
            //    Dictionary<string, string> field = new Dictionary<string, string>();
            //    //field.Add("id", wr.dgvMovement.Rows[i].Cells[0].Value.ToString());
            //    string where = "ID ='" + wr.dgvMovement.Rows[i].Cells[0].Value.ToString() + "'";
            //    //field.Add("timestamp", wr.dgvMovement.Rows[i].Cells[1].Value.ToString());
            //    //field.Add("uom", wr.dgvMovement.Rows[i].Cells[2].Value.ToString());
            //    field.Add("locationid", datacomboWare[wr.cmbMove.SelectedIndex][1]);
            //    field.Add("warehouseid", datacombo[wr.cmbWare.SelectedIndex][1]);
            //    sq.update(field, "warehouse", where);
            //    updateVaccine(wr.dgvMovement.Rows[i].Cells[2].Value.ToString(), wr.dgvMovement.Rows[i].Cells[0].Value.ToString());
            //}
            //tblMovement();
        }

        
    }
}