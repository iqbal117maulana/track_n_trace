﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Warehouse_Station
{
    public partial class config : Form
    {
        public config()
        {
            InitializeComponent();
            getConfig();
        }

        void getConfig()
        {
            try
            {
                sqlitecs sql = new sqlitecs();
                txtNamaDB.Text = sql.config["namaDB"];
                txtPortDB.Text = sql.config["portDB"];
                txtIpDB.Text = sql.config["ipDB"];
            }
            catch (KeyNotFoundException k)
            {

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Confirm cf = new Confirm("Are you sure? ", "Confirmation");
            bool hasilDialog = cf.conf();
            if (hasilDialog)
            {
                if (cekKosong())
                {
                    sqlitecs sql = new sqlitecs();
                    Dictionary<string, string> field = new Dictionary<string, string>();
                    field.Add("namadb", txtNamaDB.Text);
                    field.Add("portdb", txtPortDB.Text);
                    field.Add("ipDB", txtIpDB.Text);
                    sql.update(field, "config", "");
                    cf = new Confirm("Data success saved", "Information",MessageBoxButtons.OK);
                    this.Dispose();
                }
                else
                {
                    cf = new Confirm("Configuration can not be empty", "Information",MessageBoxButtons.OK);
                }
            }
        }

        private bool cekKosong()
        {
            if (!cek(txtNamaDB.Text))
            {
                return false;
            }
            else if (!cek(txtPortDB.Text))
            {
                return false;
            }
            else if (!cek(txtIpDB.Text))
            {
                return false;
            }
            return true;
        }

        private bool cek(string p)
        {
            if (p.Length > 0) return true; else return false;
        }
    }
}
