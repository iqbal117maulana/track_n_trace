﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Warehouse_Station
{
    public partial class Warehouse : Form
    {
        DataTable table;
        DataTable table2;
        DataTable table3;
        Control ms;
        int ordersItemIndex;
        int ordersItemIndex2; 
        int qu = 0;
        public bool isSas = false;
        string cek;
        string cekProduct;
        sqlitecs sq;
        dbaccess db;
        List<string[]> datadetail;
        List<string[]> dataOrder = new List<string[]>();
        List<string[]> datacomboCold;
        List<string[]> datacomboWare;
        public List<string[]> datacombo;

        //tambahan disable close button
        private const int CP_NOCLOSE_BUTTON = 0x200;
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams myCp = base.CreateParams;
                myCp.ClassStyle = myCp.ClassStyle | CP_NOCLOSE_BUTTON;
                return myCp;
            }
        }
        //end of disable close button

        public Warehouse(string admin)
        {
            InitializeComponent();
            db = new dbaccess();
            sq = new sqlitecs();
            ms = new Control();
            ms.wr = this;
            ms.adminid = getAdminName(admin);
            ms.log("Logged as : " + ms.adminid);
            ms.insialisasitable();
            ms.cmb();
            lbladmin.Text = ms.getAdminName(admin);
            ms.getdataShippingOrder();
            //unused
            //table = new DataTable();
            //table.Columns.Add("Basket ID");
            //table.Columns.Add("Timestamp");
            //table2 = new DataTable();
            //table2.Columns.Add("Basket ID");
            //table2.Columns.Add("Last Location");
            //table3 = new DataTable();
            //table3.Columns.Add("Basket ID");
            //table3.Columns.Add("Last Location");
            //cmb();
            //getdataShippingOrder();

            refreshReceiving();
            refreshMovement();
            refreshDetail();
        }

        internal string getAdminName(string admin)
        {
            List<string> field = new List<string>();
            field.Add("first_name   ");
            string where = "id ='" + admin + "'";
            List<string[]> ds = db.selectList(field, "[USERS]", where);
            if (db.num_rows > 0)
                return ds[0][0];
            else
                return "";
        }
          
        private void Warehouse_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (txtBarcode.Text.Length > 0)
                {
                    string data = txtBarcode.Text.Replace(" ", "").Replace("\n", "");
                    ms.tambahData(data);
                    ms.log("Receive Data : " + txtBarcode.Text);
                    txtBarcode.Text = "";
                }
                else
                {
                    new Confirm("Data empty", "Information", MessageBoxButtons.OK);
                }
            }
        }

        //unused
        //private void tambahData()
        //{
        //    DataRow row = table.NewRow();
        //    row[0] = txtBarcode.Text;
        //    string dates = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        //    row[1] = dates;
        //    table.Rows.Add(row);
        //    dgvReceiving.DataSource = table;
        //    lblQtyReceiving.Text = ""+dgvReceiving.Rows.Count;
        //}

        private void button1_Click(object sender, EventArgs e)
        {
            ms.submitReceiving();
        }

        private void button9_Click(object sender, EventArgs e)
        {
            config cfg = new config();
            cfg.Show();
        }

        //unused
        //void getdataShippingOrder()
        //{
        //    List<string> field = new List<string>();
        //    field.Add("shippingOrder.shippingOrderNumber," +
        //                "shippingOrder.customerName," +
        //                "shippingOrder.shippingType," +
        //                "shippingOrder.createdBy," +
        //                "shippingOrder.status," +
        //                "shippingOrder.createTime");
        //    string tab = "shippingOrder";

        //    List<string[]> ds = db.selectList(field, tab, "");
        //    dataOrder = ds;
        //    if (db.num_rows > 0)
        //    {
        //        DataTable table = new DataTable();
        //        table.Columns.Add("Shipping Order Number");
        //        foreach (string[] Row in ds)
        //        {
        //            DataRow row = table.NewRow();
        //            for (int k = 0; k < Row.Length; k++)
        //            {
        //                if (k == 0)
        //                    row[k] = Row[k];
        //            }
        //            table.Rows.Add(row);
        //        }
        //        dgvTransferOrder.DataSource = table;
        //    }
        //}
     
        private void dataGridView2_SelectionChanged(object sender, EventArgs e)
        {
            if (dgvTransferOrder.SelectedRows.Count > 0)
            {
                ordersItemIndex2 = dgvTransferOrder.SelectedRows[0].Index;
                ms.getOrderDetail(ordersItemIndex2);
            }
        }
      
        private void addPickingList(string itemid, string qty, string detailno)
        {
            Dictionary<string, string> field = new Dictionary<string, string>();
            field.Add("itemid", itemid);
            field.Add("qty", qty);
            field.Add("detailno", detailno);
            field.Add("status", "1");
            db.insert(field, "pickinglist");

        }
 
        //unused
        //private void update()
        //{
        //    lblOrder.Text = dataOrder[ordersItemIndex][0];
        //    getOrderDetail(dataOrder[ordersItemIndex][0]);

        //}

        private void getOrderDetail(string shippingorder)
        {
            List<string> field = new List<string>();
            field.Add("shippingOrder_detail.productModel,product_model.model_name,shippingOrder_detail.qty,shippingOrder_detail.shippingOrderDetailNo");
            string table = "shippingOrder_detail INNER JOIN product_model ON shippingOrder_detail.productModel = product_model.product_model";
            string where = "shippingOrder_detail.shippingOrderNumber = '" + shippingorder + "' AND shippingOrder_detail.status = '0'";
            datadetail = db.selectList(field, table, where);
            if (db.num_rows > 0)
            {
                List<string> productDetail = new List<string>();

                for (int i = 0; i < datadetail.Count; i++)
                {
                    productDetail.Add(datadetail[i][1]);
                }
                cmbDetail.Enabled = true;
                txtTransfer.Enabled = true;
                cmbDetail.DataSource = productDetail;
            }
            else
            {
                Dictionary<string, string> field2 = new Dictionary<string, string>();
                field2.Add("status", "1");
                where = "shippingOrderNumber ='" + lblOrder.Text + "'";
                db.update(field2, "shippingOrder", where);
            }
        }

        private void dataGridView2_CellClick(object sender, DataGridViewCellEventArgs e)
        {

        }

       


        private void button3_Click(object sender, EventArgs e)
        {
            Confirm cf = new Confirm("Are you sure? ", "Confirmation");
            bool hasilDialog = cf.conf();
            if (hasilDialog)
            {
                ms.tableReceiving = new DataTable();
                ms.tableReceiving = new DataTable();
                ms.tableReceiving.Columns.Add("ID");
                ms.tableReceiving.Columns.Add("Timestamp");
                ms.tableReceiving.Columns.Add("UOM");
                //lblQtyReceiving.Text = "" + dgvReceiving.Rows.Count;
                lblQtyReceiving.Text = "0";
                dgvReceiving.DataSource = ms.tableReceiving;
                //sq.delete("", "warehouse");
            }
        }

        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            if (dgvReceiving.SelectedRows.Count > 0)
                ordersItemIndex = dgvReceiving.SelectedRows[0].Index;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Confirm cf = new Confirm("Are you sure? ", "Confirmation");
            bool hasilDialog = cf.conf();
            if (hasilDialog)
            {
                if (dgvReceiving.Rows.Count > 0)
                {
                    dgvReceiving.Rows.RemoveAt(ordersItemIndex);
                    lblQtyReceiving.Text = "" + dgvReceiving.Rows.Count;
                }
            }
        }

        private void textBox2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (cmbStatusOld.SelectedIndex > -1)
                {
                    if (txtBasketMove.Text.Length > 0)
                    {
                        string data = txtBasketMove.Text.Replace(" ", "").Replace("\n", "");
                        ms.getdataMovement(data);
                    }
                    else
                    {
                        new Confirm("Data cannot empty.", "Information", MessageBoxButtons.OK);
                    }
                }
                else
                {
                     new Confirm("Please select status.", "Information", MessageBoxButtons.OK);
                }
            }
            
        }

        private void button5_Click(object sender, EventArgs e)
        {
            dgvMovement.DataSource = "";
            table3 = new DataTable();
            table3.Columns.Add("Basket ID");
            table3.Columns.Add("Last Location");
        }

      


        private void button4_Click(object sender, EventArgs e)
        {
            ms.simpanMove();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            qu = 0;
            lblActualQuantity.Text = "" + qu;
        }
        
        private void textBox4_KeyDown(object sender, KeyEventArgs e)
        {
            Console.WriteLine("txt");
            if (e.KeyCode == Keys.Enter)
             {
                if (qu < int.Parse(lblQuantity.Text))
                {
                    ms.tambahdetail(txtTransfer.Text.Replace("\n", ""));
                    
                }
                else
                {
                   new Confirm("Can not exceed the requested item size.", "Information", MessageBoxButtons.OK);
                }
                //SetDetailBatch();
                if (!isSas)
                {
                    lblActualQuantity.Text = ms.cekPickingList(lblDetailNumber.Text).ToString();
                }
                txtTransfer.Text = "";
                txtTransfer.Focus();
            }
        }

        public int cekbanyakInfeed(string basketid)
        {
            List<string> field = new List<string>();
            field.Add("count(Vaccine.capId)");
            string tabl = "Vaccine LEFT JOIN dbo.blisterPack ON dbo.Vaccine.blisterpackId = dbo.blisterPack.blisterpackId LEFT JOIN dbo.innerBox ON dbo.blisterPack.innerBoxId = dbo.innerBox.infeedInnerBoxId left JOIN dbo.basket ON dbo.innerBox.basketId = dbo.basket.basketId";
            string where = "basket.basketId = '" + basketid + "'";
            List<string[]> ds = db.selectList(field, tabl, where);
            if (db.num_rows > 0)
                return int.Parse(ds[0][0]);
            else
                return 0;
        }
        
        private void cmbPilih_SelectedIndexChanged(object sender, EventArgs e)
        {
            ms.setLocationReceiving(cmbPilih.SelectedIndex);
            //List<string> da = new List<string>();
            //List<string> field = new List<string>();
            //field.Add("warehouse_location.locationName,warehouse_location.locationId");
            //string where = "warehouse_location.warehouseId = '"+datacombo[cmbPilih.SelectedIndex][1]+"' AND warehouse_location.status = '0' AND warehouse_location.status = '0'";
            //datacomboCold = db.selectList(field, "[warehouse_location]", where);
            //List<string> das = new List<string>();
            //for (int i = 0; i < datacomboCold.Count; i++)
            //{
            //    das.Add(datacomboCold[i][0]);
            //}
            //cmbLocation.DataSource = das;
        }

        //private void cmbWare_SelectedIndexChanged(object sender, EventArgs e)
        //{

        //    ms.setcomboWare(cmbWare.SelectedIndex);
            //List<string> da = new List<string>();
            //List<string> field = new List<string>();
            //field.Add("warehouse_location.locationName,warehouse_location.locationId");
            //string where = "warehouse_location.warehouseId = '" + datacombo[cmbWare.SelectedIndex][1] + "' AND warehouse_location.status = '0'";
            //datacomboWare = db.selectList(field, "[warehouse_location]", where);
            //List<string> das = new List<string>();
            //for (int i = 0; i < datacomboWare.Count; i++)
            //{
            //    das.Add(datacomboWare[i][0]);
            //}
            //cmbMove.DataSource = das;
        //}

        private void dataGridView3_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            lblQtyMovement.Text = "" + dgvMovement.Rows.Count;
        }

        private void dataGridView1_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            lblQtyReceiving.Text = "" + dgvReceiving.Rows.Count;
        }

        //private void cmbStatusOld_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    ms.cmbstatusold();
            //if (cmbStatusOld.Text.Equals("Release"))
            //{
            //    List<string> das = new List<string>();
            //    das.Add("Release");
            //    cmbWare.DataSource = das;
            //}
            //else
            //{
            //    List<string> das = new List<string>();
            //    das.Add("Quarantine");
            //    das.Add("Release");
            //    cmbWare.DataSource = das;
            //}
        //}

        private void button8_Click(object sender, EventArgs e)
        {
            Confirm cf = new Confirm("Are you sure? ", "Confirmation");
            bool result = cf.conf();
            if (result)
            {
                this.Dispose();
                Application.Exit();
                //Login lg = new Login();
                //lg.Show();
            }
        }

        private void groupBox3_Enter(object sender, EventArgs e)
        {

        }

        private void cmbDetail_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetDetailBatch();
        }

        public void SetDetailBatch()
        {
            lblQuantity.Text = ms.datadetail[cmbDetail.SelectedIndex][2];
            lblDetailNumber.Text = ms.datadetail[cmbDetail.SelectedIndex][3];
            lblActualQuantity.Text = ms.cekPickingList(lblDetailNumber.Text).ToString();
        }

        private void groupBox4_Enter(object sender, EventArgs e)
        {

        }

        private void button6_Click_1(object sender, EventArgs e)
        {
            Confirm cf = new Confirm("Are you sure? ", "Confirmation");
            bool hasilDialog = cf.conf();
            if (hasilDialog)
            {
                if (dgvMovement.Rows.Count > 0)
                {
                    if (ordersItemIndexMove > -1)
                    {
                        dgvMovement.Rows.RemoveAt(ordersItemIndexMove);
                        lblQtyMovement.Text = "" + dgvMovement.Rows.Count;
                    }
                }
            }
        }

        private void button5_Click_1(object sender, EventArgs e)
        {
            Confirm cf = new Confirm("Are you sure? ", "Confirmation");
            bool hasilDialog = cf.conf();
            if (hasilDialog)
            {
                ms.tblMovement();
            }
        }
        int ordersItemIndexMove;
        private void dgvMovement_SelectionChanged(object sender, EventArgs e)
        {
            if (dgvMovement.SelectedRows.Count > 0)
                ordersItemIndexMove = dgvMovement.SelectedRows[0].Index;
            else
            {
                ordersItemIndexMove = 0;
            }
        }

        private void dgvTransferOrder_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {

            ms.getdataShippingOrder();
        }

        private void dgvTransferOrder_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int a = dgvTransferOrder.Rows.Count;
            if (dgvTransferOrder.CurrentCell.RowIndex > -1)
            {
                ordersItemIndex2 = dgvTransferOrder.CurrentCell.RowIndex;
                ms.getOrderDetail(ordersItemIndex2);
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            ms.getdataShippingOrder();
        }

        private void groupBox6_Enter(object sender, EventArgs e)
        {

        }

        private void groupBox5_Enter(object sender, EventArgs e)
        {

        }

        private void Warehouse_Load(object sender, EventArgs e)
        {
            timer1.Start();
            lblTime.Text = DateTime.Now.ToLongTimeString();
            lblDate.Text = DateTime.Now.ToShortDateString();
            txtBarcode.Focus();
        }

        private void dgvTransferOrder_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button10_Click(object sender, EventArgs e)
        {
            Confirm cf = new Confirm("Are you sure? ", "Confirmation");
            bool hasilDialog = cf.conf();
            if (hasilDialog)
            {
                ms.ActualQty = lblActualQuantity.Text;
                ms.batchNumber = cmbDetail.Text;
                ms.SetForceClose();
                refreshDetail();
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            lblTime.Text = DateTime.Now.ToLongTimeString();
            timer1.Start();
        }

        public void refreshReceiving()
        {
            DataTable table = new DataTable();
            table.Columns.Add("ID");
            table.Columns.Add("TimeStamp");
            table.Columns.Add("UOM");
            dgvReceiving.DataSource = table;
        }

        public void refreshMovement()
        {
            DataTable table = new DataTable();
            table.Columns.Add("ID");
            table.Columns.Add("Last Status");
            table.Columns.Add("Last Location");
            table.Columns.Add("UOM");
            dgvMovement.DataSource = table;
        }

        public void refreshDetail()
        {
            DataTable table = new DataTable();
            table.Columns.Add("ID");
            table.Columns.Add("Last Status");
            table.Columns.Add("Last Location");
            table.Columns.Add("UOM");
            table.Columns.Add("Quantity");
            dgv.DataSource = table;
        }

        private void cmbStatusOld_SelectedIndexChanged(object sender, EventArgs e)
        {
            ms.setNewLocation(cmbStatusOld.SelectedIndex);
        }

        private void txtTransfer_TextChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void lblDetailNumber_Click(object sender, EventArgs e)
        {

        }

    }
}
