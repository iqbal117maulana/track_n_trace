﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Configuration;
using System.Windows.Forms;
using System.Data;
using System.IO;
using System.Diagnostics;
using System.Security.Cryptography;
using System.Net.Sockets;
using System.Net;

namespace Label_Weigher_Station 
{
   public class dbaccess
    {
        public static SqlConnection Connection;
        public static SqlCommand Command;
        public static SqlDataReader DataReader;
        public static SqlDataAdapter dataadapter;
        public DataTable dataHeader ;
        public int num_rows=0;
        sqlitecs sqlite;

        public string eventname;
        public string eventType = "12";
        public string movementType = "4";
        public string from;
        public string uom;
        public string adminid;
        public dbaccess()
        {
            sqlite = new sqlitecs();
            Connect();
        }

        public void Connect()
        {
            try
            {
                string connec = "Data Source=" + sqlite.config["ipDB"] + ";" +
                                "Initial Catalog=" + sqlite.config["namadb"] + ";" +
                                "User id=" + sqlite.config["usernameDB"] + ";" +
                                "Password=" + sqlite.config["passDB"] + ";";
                Connection = new SqlConnection(connec);
                try
                {
                    Connection.Open();
                }
                catch (Exception ex)
                {
                    Confirm csf = new Confirm("Can not open connection to Database Server.", "Information", MessageBoxButtons.OK);
                    simpanLog("Error SQLSERVER : " + ex.ToString());
                    config cfg = new config();
                    cfg.ShowDialog();
                }
                finally
                {
                    Connection.Close();
                }
            }
            catch (KeyNotFoundException key)
            {

            }
        }

        public static List<String[]> LoadProductionOrders()
        {
            List<String[]> Results = new List<String[]>();
            String Query = "SELECT production_order_number, created_date, status FROM transaction_production_order";

            Connection.Open();
            Command = new SqlCommand(Query, Connection);
            DataReader = Command.ExecuteReader();

            while (DataReader.Read())
            {
                String[] Data = new String[] { DataReader.GetValue(0).ToString(), DataReader.GetValue(1).ToString(), DataReader.GetValue(2).ToString() };
                Results.Add(Data);
            }

            DataReader.Close();
            Command.Dispose();
            Connection.Close();

            return Results;
        }

        public void insert(Dictionary<string, string> field, string table)
        {
            try
            {
                string sql = "INSERT INTO " + table + " (";
                int i = 0;
                foreach (string key in field.Keys)
                {
                    sql += "" + key + "";
                    if (i + 1 < field.Count)
                    {
                        sql += ",";
                        i++;
                    }
                }

                sql += ") values (";

                i = 0;
                foreach (string key in field.Values)
                {
                    sql += "'" + key + "'";
                    if (i + 1 < field.Count)
                    {
                        sql += ",";
                        i++;
                    }
                }
                sql += ")";
                simpanLog(sql);
                Connection.Open();
                Command = new SqlCommand(sql, Connection);
                Command.ExecuteNonQuery();
                Connection.Close();
            }
            catch (SqlException sq)
            {
                simpanLog("Error SQl : " + sq.ToString());
            }
            finally
            {
                Connection.Close();
            }
        }

        public void insertManual(string masterboxid)
        {
            try
            {
                string sql = "insert into Distribution.dbo.vaccine "+
                                "(dbo.Vaccine.capId, " +
                                "dbo.Vaccine.gsOneVialId, "+
                                "dbo.Vaccine.blisterpackId, "+
                                "dbo.Vaccine.createdTime, "+
                                "dbo.Vaccine.lineNumber, "+
                                "dbo.Vaccine.batchNumber, "+
                                "dbo.Vaccine.isReject, "+
                                "dbo.Vaccine.lastLocationId, "+
                                "dbo.Vaccine.shippingStatus, "+
                                "dbo.Vaccine.productModelId, "+
                                "dbo.Vaccine.innerBoxId, "+
                                "dbo.Vaccine.innerBoxGsOneId, "+
                                "dbo.Vaccine.masterBoxId, "+
                                "dbo.Vaccine.masterBoxGsOneId, "+
                                "dbo.Vaccine.basketId, "+
                                "dbo.Vaccine.usageTime, "+
                                "dbo.Vaccine.usageCount, "+
                                "dbo.Vaccine.isEmpty, "+
                                "dbo.Vaccine.remarks, "+
                                "dbo.Vaccine.serialNumber, "+
                                "dbo.Vaccine.expDate) "+
                                "SELECT "+
                                "dbo.Vaccine.capId, "+
                                "dbo.Vaccine.gsOneVialId, "+
                                "dbo.Vaccine.blisterpackId, "+
                                "dbo.Vaccine.createdTime, "+
                                "dbo.Vaccine.lineNumber, "+
                                "dbo.Vaccine.batchNumber, "+
                                "dbo.Vaccine.isReject, "+
                                "0, "+
                                "1, "+
                                "dbo.Vaccine.productModelId, "+
                                "dbo.Vaccine.innerBoxId, "+
                                "dbo.Vaccine.innerBoxGsOneId, "+
                                "dbo.Vaccine.masterBoxId, "+
                                "dbo.Vaccine.masterBoxGsOneId, "+
                                "dbo.Vaccine.basketId, "+
                                "0, "+
                                "0, "+
                                "0, "+
                                "dbo.Vaccine.remarks, "+
                                "dbo.Vaccine.serialNumber, "+
                                "dbo.Vaccine.expDate "+
                                "FROM "+
                                "dbo.Vaccine "+
                                "where masterBoxGsOneId ='" + masterboxid + "'";
                simpanLog(sql);
                Connection.Open();
                Command = new SqlCommand(sql, Connection);
                Command.ExecuteNonQuery();
                Connection.Close();
            }
            catch (SqlException sq)
            {
                simpanLog("Error SQl : " + sq.ToString());
            }
            finally
            {
                Connection.Close();
            }
        }

        public void update(Dictionary<string, string> field, string table, string where)
        {
            try
            {
                string sql = "UPDATE " + table + " SET ";
                int i = 0;
                foreach (KeyValuePair<string, string> key in field)
                {
                    sql += key.Key + " = " + "'" + key.Value + "'";
                    if (i + 1 < field.Count)
                    {
                        sql += ",";
                        i++;
                    }
                }

                if (where.Length > 0)
                    sql += " WHERE " + where;

                simpanLog(sql);
                Connection.Open();
                Command = new SqlCommand(sql, Connection);
                Command.ExecuteNonQuery();
            }
            catch (SqlException sq)
            {
                simpanLog("Error SQl : " + sq.ToString());
            }
            finally
            {
                Connection.Close();
            }
        }

        public DataSet select(List<string> field, string table, string where)
        {
            try
            {
                string sql = "SELECT ";
                for (int j = 0; j < field.Count; j++)
                {
                    sql += "'" + field[j] + "'";
                    if (j + 1 < field.Count)
                    {
                        sql += ",";
                        j++;
                    }
                }

                sql += " From " + table;

                if (where.Length > 0)
                    sql += " WHERE " + where;

                simpanLog(sql);
                Connection.Open();
                dataadapter = new SqlDataAdapter(sql, Connection);
                DataSet Results = new DataSet();
                dataadapter.Fill(Results);
                return Results;
            }
            catch (SqlException sq)
            {
                simpanLog("Error SQLSERVER : " + sq.ToString());
            }
            finally
            {
                try
                {
                    DataReader.Close();
                    Command.Dispose();
                    Connection.Close();
                }
                catch (SqlException sq)
                {
                    simpanLog("Error SQLSERVER : " + sq.ToString());
                }
                catch (NullReferenceException nl)
                {
                    simpanLog("Error SQLSERVER : " + nl.ToString());
                }

            }
            return null;
        }
        
        public List<String[]> selectList(List<string> field, string table, string where)
        {
            try
            {
                string sql = "SELECT ";
                for (int j = 0; j < field.Count; j++)
                {
                    sql +=  field[j] ;
                    if (j + 1 < field.Count)
                    {
                        sql += ",";
                        j++;
                    }
                }
                sql += " From " + table;

                if (where.Length > 0)
                    sql += " WHERE " + where;

                simpanLog(sql);
                Connection.Open();
                List<String[]> Results = new List<String[]>();
                Command = new SqlCommand(sql, Connection);
                DataReader = Command.ExecuteReader();
                int num = 0;
                while (DataReader.Read())
                {
                    string[] data = new string[DataReader.FieldCount];
                    for (int u = 0; u < DataReader.FieldCount; u++)
                    {
                        data[u] = DataReader.GetValue(u).ToString();
                    }

                    Results.Add(data);
                    num++;
                }
                num_rows = num;
                dataHeader = new DataTable();
                for (int i = 0; i < DataReader.FieldCount; i++)
                {
                    dataHeader.Columns.Add(DataReader.GetName(i));
                }

                return Results;
            }
            catch (SqlException sq)
            {
                var st = new StackTrace(sq, true);
                var frame = st.GetFrame(0);
                string line = frame.ToString();
                simpanLog("Error  00100: " + line);
            }
            catch (NullReferenceException nl)
            {

                var st = new StackTrace(nl, true);
                var frame = st.GetFrame(0);
                string line = frame.ToString();
                simpanLog("Error  00100: " + line);
            }
            finally
            {
                try
                {
                    DataReader.Close();
                    Command.Dispose();
                    Connection.Close();
                }
                catch (SqlException sq)
                {
                    simpanLog("Error SQLSERVER : " + sq.ToString());
                }
                catch (NullReferenceException nl)
                {
                    simpanLog("Error SQLSERVER : " + nl.ToString());
                }

            }
            return null;
        }
       
        
        public List<String[]> selectListManual(string sql)
        {
            try
            {
                simpanLog(sql);
                Connection.Open();
                List<String[]> Results = new List<String[]>();
                Command = new SqlCommand(sql, Connection);
                DataReader = Command.ExecuteReader();
                int num = 0;
                while (DataReader.Read())
                {
                    string[] data = new string[DataReader.FieldCount];
                    for (int u = 0; u < DataReader.FieldCount; u++)
                    {
                        data[u] = DataReader.GetValue(u).ToString();
                    }

                    Results.Add(data);
                    num++;
                }
                num_rows = num;
                dataHeader = new DataTable();
                for (int i = 0; i < DataReader.FieldCount; i++)
                {
                    dataHeader.Columns.Add(DataReader.GetName(i));
                }

                return Results;
            }
            catch (SqlException sq)
            {
                var st = new StackTrace(sq, true);
                var frame = st.GetFrame(0);
                string line = frame.ToString();
                simpanLog("Error  00100: " + line);
            }
            catch (NullReferenceException nl)
            {

                var st = new StackTrace(nl, true);
                var frame = st.GetFrame(0);
                string line = frame.ToString();
                simpanLog("Error  00100: " + line);
            }
            finally
            {
                try
                {
                    DataReader.Close();
                    Command.Dispose();
                    Connection.Close();
                }
                catch (SqlException sq)
                {
                    simpanLog("Error SQLSERVER : " + sq.ToString());
                }
                catch (NullReferenceException nl)
                {
                    simpanLog("Error SQLSERVER : " + nl.ToString());
                }

            }
            return null;
        }

 
        public static List<String> GetPODetails(String PO)
        {
            List<String> Results = new List<String>();
            String Query = String.Format("SELECT production_order_number, product_code, product_name, product_quantity, status FROM transaction_production_order WHERE production_order_number = '{0}'", PO);

            Connection.Open();
            Command = new SqlCommand(Query, Connection);
            DataReader = Command.ExecuteReader();

            while (DataReader.Read())
            {
                for (int i = 0; i < 5; i++)
                {
                    Results.Add(DataReader.GetValue(i).ToString());
                }
            }

            DataReader.Close();
            Command.Dispose();
            Connection.Close();

            return Results;
        }

        public void simpanLog(String line)
        {
            DateTime now = DateTime.Now;
            string tahun = now.ToString("yyyy");
            string bulan = now.ToString("MM");
            string hari = now.ToString("dd");
            string dates = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            line = dates + "\t" + line;
            // cek file 
            bool cekfile = false;
            while (!cekfile)
            {
                if (Directory.Exists("eventlog"))
                {
                    if (Directory.Exists("eventlog" + @"\" + tahun))
                    {
                        if (Directory.Exists("eventlog" + @"\" + tahun + @"\" + bulan))
                        {

                            using (StreamWriter outputFile = new StreamWriter("eventlog" + @"\" + tahun + @"\" + bulan + @"\" + hari + ".txt", true))
                            {
                                outputFile.WriteLine(line);
                            }
                            cekfile = true;
                        }
                        else
                        {

                            Directory.CreateDirectory("eventlog" + @"\" + tahun + @"\" + bulan);
                        }
                    }
                    else
                    {
                        Directory.CreateDirectory("eventlog" + @"\" + tahun);
                    }
                }
                else
                {

                    Directory.CreateDirectory("eventlog");
                }
            }
        }

        internal bool cekPermision(string adminid, string modul_name, string permission)
        {
            List<string> field = new List<string>();
            field.Add("permissions_name");
            string where = "dbo.users.id = '" + adminid + "' AND dbo.permissions.module_name = '" + modul_name + "' AND dbo.permissions.permissions_name = '" + permission + "'";
            string from = "dbo.groups INNER JOIN dbo.permissions ON dbo.groups.id = dbo.permissions.role_id INNER JOIN dbo.users_groups ON dbo.users_groups.group_id = dbo.groups.id  INNER JOIN dbo.users ON dbo.users_groups.user_id = dbo.users.id";
            selectList(field, from, where);
            if (num_rows > 0)
                return true;
            return false;
        }

        public string md5hash(string source)
        {
            using (MD5 md5Hash = MD5.Create())
            {
                string hash = GetMd5Hash(md5Hash, source);
                return hash;
            }
            return null;
        }

        static string GetMd5Hash(MD5 md5Hash, string input)
        {

            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }

        static bool VerifyMd5Hash(MD5 md5Hash, string input, string hash)
        {
            // Hash the input.
            string hashOfInput = GetMd5Hash(md5Hash, input);

            // Create a StringComparer an compare the hashes.
            StringComparer comparer = StringComparer.OrdinalIgnoreCase;

            if (0 == comparer.Compare(hashOfInput, hash))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public string[] cekLine(string where)
        {
            List<string> field = new List<string>();
            string[] temp = new string[0];
            field.Add("lineName,linePackaging.linePackagingId");
            string from = "linePackagingDetail INNER JOIN linePackaging ON linePackagingDetail.linePackagingId = linePackaging.linePackagingId";
            string where1 = where + "= '" + GetLocalIPAddress() + "'";
            List<string[]> ds = selectList(field, from, where1);
            if (num_rows > 0)
                return ds[0];
            else
                return temp;
        }

        public static string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            throw new Exception("Local IP Address Not Found!");
        }

        public bool cekRole(string adminid, string module)
        {
            List<string> field = new List<string>();
            field.Add("permission.[module]");
            string from = "[user] INNER JOIN user_role ON [user].role = user_role.role_id INNER JOIN permission ON permission.roleId = user_role.role_id";
            string where = "permission.[read] = '0' AND [user].userId = '" + adminid + "' AND permission.[module] = '" + module + "'";
            List<string[]> ds = selectList(field, from, where);
            if (num_rows > 0)
                return true;
            else
                return false;
        }
  
       public string getUnixString()
        {
            string date = "" + (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1)).TotalSeconds);
            return date;
        }

        public DataTable ConvertToDatatable(List<string[]> field, DataTable dataHeader2)
        {
            DataTable table = dataHeader2;
            foreach (string[] Row in field)
            {
                DataRow row = table.NewRow();
                for (int k = 0; k < Row.Length; k++)
                {
                    row[k] = Row[k];
                }
                table.Rows.Add(row);
            }
            return table;
        }

        public void systemlog()
        {
            Dictionary<string, string> field = new Dictionary<string, string>();
            field.Add("eventtime", getUnixString());
            field.Add("eventname", eventname);
            field.Add("eventtype", eventType);
            field.Add("userid", adminid);
            field.Add("[from]", from);
            insert(field, "system_log");
        }

        public void Movement(string order, string from, string to, string qty, string itemid)
        {
            string dates = DateTime.Now.ToString("ssMMddmmHHssyy");
            Dictionary<string, string> field = new Dictionary<string, string>();
            field.Add("movementId", dates);
            field.Add("orderNumber", order);
            field.Add("[from]", from);
            field.Add("movementType", movementType);
            field.Add("[to]", to);
            field.Add("qty", qty);
            field.Add("uom", uom);
            field.Add("userid", adminid);
            field.Add("itemid", itemid);
            field.Add("eventTime", getUnixString());
            insert(field, "movement_history");
        }

        internal void insertReceivingDinkes(string shipping,string supplierid)
        {
            //string qty = getQty(Masterbox);
            string id = getUnixString();
            Dictionary<string, string> field = new Dictionary<string, string>();
            field.Add("receivingOrderNumber", shipping);
            field.Add("supplierName", "Biofarma");
            field.Add("supplierid", "191");
            field.Add("receivingType", "1");
            field.Add("createtime", getUnixString());
            field.Add("createby", "Administrator");
            field.Add("status", "1");
            field.Add("uom", "MasterBox");
            insert(field, "Distribution.dbo.pickingList");
        }

        internal void insertPickilist(string Masterbox,string shipping)
        {
            string qty = getQty(Masterbox);
            string id = getUnixString();
            Dictionary<string, string> field = new Dictionary<string, string>();
            field.Add("pickinglistid", id);
            field.Add("itemid", Masterbox);
            field.Add("type", "masterBoxGsOneId");
            field.Add("qty", qty);
            field.Add("status", "1");
            field.Add("detailno", shipping);
            insert(field, "Distribution.dbo.pickingList");

        }

        private string getQty(string masterbox)
        {
            List<string> field = new List<string>();
            field.Add("COUNT(capId)");
            string where = "dbo.Vaccine.masterBoxId = '000204000053' OR masterBoxGsOneId='"+masterbox+"'";
            List<string[]> ds = selectList(field, "Vaccine", where);
            if (num_rows > 0)
                return ds[0][0];
            else
                return "0";
        }

        public string getUnixString(double data)
        {
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(data).ToLocalTime();
            return dtDateTime.ToShortDateString();
        }

        public bool validasi(string username)
        {
            try
            {
                if (username.Length > 0)
                {
                    List<string> field = new List<string>();
                    field.Add("id");
                    string where = "username = '" + username + "'";
                    List<string[]> result = selectList(field, "[users]", where);
                    if (result.Count > 0)
                    {
                        return true;
                    }
                    else
                        return false;
                }
                else
                {
                    return false;
                }
            }
            catch (NullReferenceException nl)
            {
                simpanLog("Error  00100: " + nl.Message);
                return false;
            }

        }

        public List<string[]> validasi(string username, string password, string status)
        {
            try
            {
                if (username.Length > 0 && password.Length > 0)
                {
                    List<string> field = new List<string>();
                    field.Add("id");
                    string where = "username = '" + username + "' AND password = '" + md5hash(password) + "' AND Active = " + status;
                    List<string[]> result = selectList(field, "[users]", where);
                    if (result.Count > 0)
                    {
                        from = "Bottle Station";
                        adminid = result[0][0];
                        eventType = "1";
                        eventname = "Login";
                        systemlog();
                        eventType = "12";
                        return result;
                    }
                    else
                        return null;
                }
                else
                {
                    string code = "0010";
                    simpanLog(code + " : USERNAME DAN PASSWORD SALAH");
                    return null;
                }
            }
            catch (NullReferenceException nl)
            {
                var st = new StackTrace(nl, true);
                var frame = st.GetFrame(0);
                string line = frame.ToString();
                simpanLog("Error  00100: " + line);
                return null;
            }

        }

        public bool validasi(string username, string password)
        {
            try
            {
                if (username.Length > 0 && password.Length > 0)
                {
                    List<string> field = new List<string>();
                    field.Add("id");
                    string where = "username = '" + username + "' AND password = '" + md5hash(password) + "'";
                    List<string[]> result = selectList(field, "[users]", where);
                    if (result.Count > 0)
                    {
                        return true;
                    }
                    else
                        return false;
                }
                else
                {
                    string code = "0010";
                    simpanLog(code + " : USERNAME DAN PASSWORD SALAH");
                    return false;
                }
            }
            catch (NullReferenceException nl)
            {
                var st = new StackTrace(nl, true);
                var frame = st.GetFrame(0);
                string line = frame.ToString();
                simpanLog("Error  00100: " + line);
                return false;
            }

        }


     
    }
}