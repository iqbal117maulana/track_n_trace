﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Windows.Forms;
using System.IO;

namespace Label_Weigher_Station
{
    class tcp_testing
    {
        private static TcpClient Client;
        private static TcpClient Client2;
        private static TcpClient Client3;
        private static Byte[] Data = new Byte[256];
        private static Stream ClientStream;
        public static bool IsConnected = false;
        public print pr;
        public string ipa;
        public string ports;
        int printer;
        public bool Connect(string ip , string port, string timeout ,int printers)
        {
            ipa = ip;
            ports = port;
            try
            {
                IAsyncResult result = null; 
                printer = printers;
                if (printer == 1)
                {
                    Client = new TcpClient();
                    result = Client.BeginConnect(ip, Int32.Parse(port), null, null);
                }
                if (printer == 2)
                {
                    Client2 = new TcpClient();
                    result = Client2.BeginConnect(ip, Int32.Parse(port), null, null);
                }
                if (printer == 3)
                {
                    Client3 = new TcpClient();
                    result = Client3.BeginConnect(ip, Int32.Parse(port), null, null);
                }
                bool success = result.AsyncWaitHandle.WaitOne(Int32.Parse(timeout), true);
                if (success)
                {
                    IsConnected = true;
                    return true;
                }
                else
                {
                    IsConnected = false;
                    return false;
                }
            }
            catch (SocketException ex)
            {
                new Confirm("error +" + ex.ToString());
                if (!ex.NativeErrorCode.Equals(10056))
                    IsConnected = false;
            }
            catch (InvalidOperationException te)
            {
                new Confirm("Printer not connected !");
            }
            return false;
        }
            
        public void send(string dataTerima)
        {
            try
            {
                if (IsConnected)
                {
                    if (printer == 1)
                        ClientStream = Client.GetStream();
                    if (printer == 2)
                        ClientStream = Client2.GetStream();
                    if (printer == 3)
                        ClientStream = Client3.GetStream();
                    StreamWriter sw = new StreamWriter(ClientStream);
                    sw.WriteLine(dataTerima);
                    sw.Flush();
                }
            }
            catch (InvalidOperationException ex)
            {
                
            }
        }

        public string sendBack(string dataTerima)
        {
            try
            {
                byte [] data = new Byte[256];
                if (IsConnected)
                {
                    ClientStream = Client.GetStream();
                    StreamWriter sw = new StreamWriter(ClientStream);
                    sw.WriteLine(dataTerima);
                    sw.Flush();
                    Int32 bytes = ClientStream.Read(data, 0, data.Length);
                    string responseData = System.Text.Encoding.ASCII.GetString(data, 0, bytes);
                    return responseData;
                }
                return "";
            }
            catch (InvalidOperationException ex)
            {

            }
            return "";
        }

        public void dc()
        {
            Client.Close();
        }

        public void updatestatus(string data)
        {
            string dates = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            //pr.txtStatus.AppendText(dates+"\t"+data+"\n");
        }
    }
}
