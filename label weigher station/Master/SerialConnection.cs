﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO.Ports;

namespace Label_Weigher_Station.Master
{
    class SerialConnection
    {
        // default values
        private String port = "COM1";
        private Int32 baudRate = 9600;
        private Int16 dataBits = Convert.ToInt16(7);
        private StopBits stopBits = (StopBits)Enum.Parse(typeof(StopBits), "One"); //"One", "OnePointFive", "Two"
        private Handshake handShake = (Handshake)Enum.Parse(typeof(Handshake), "None"); //"None", "XonXoff", "RequestToSend", "RequestToSendXonXoff"
        private Parity parity = (Parity)Enum.Parse(typeof(Parity), "Even"); //"None", "Even", "Mark", "Odd", "Space"
        private SerialPort serialPort;
        private String receivedData;

        public SerialConnection()
        {
            serialPort = new SerialPort();
         //   serialPort.DataReceived += new SerialDataReceivedEventHandler(receiveData);
        }

        public bool open()
        {
            if (serialPort.IsOpen)
                return false;

            serialPort.PortName = port;
            serialPort.BaudRate = baudRate;
            serialPort.DataBits = dataBits;
            serialPort.StopBits = stopBits;
            serialPort.Handshake = handShake;
            serialPort.Parity = parity;
            serialPort.Open();
            return true;
        }

        public bool close()
        {
            if (!serialPort.IsOpen)
                return false;

            serialPort.Close();
            return true;
        }

        public void setPort(string port)
        {
            this.port = port;
        }

        public void setBaudRate(int baudRate)
        {
            this.baudRate = baudRate;
        }

        public void setDataBits(int dataBits)
        {
            this.dataBits = Convert.ToInt16(dataBits);
        }

        public void setStopBits(string stopBits)
        {
            //if (String.Compare(stopBits, "One", StringComparison.OrdinalIgnoreCase))
            //{
            //    this.stopBits = (StopBits)Enum.Parse(typeof(StopBits), "One");
            //}
            //else if (String.Compare(stopBits, "OnePointFive", StringComparison.OrdinalIgnoreCase))
            //{
            //    this.stopBits = (StopBits)Enum.Parse(typeof(StopBits), "OnePointFive");
            //}
            //else if (String.Compare(stopBits, "Two", StringComparison.OrdinalIgnoreCase))
            //{
            //    this.stopBits = (StopBits)Enum.Parse(typeof(StopBits), "Two");
            //}
        }

        public void setParity(string parity)
        {
            //if (String.Compare(parity, "None", StringComparison.OrdinalIgnoreCase))
            //{
            //    this.parity = (Parity)Enum.Parse(typeof(Parity), "None");
            //}
            //else if (String.Compare(parity, "Even", StringComparison.OrdinalIgnoreCase))
            //{
            //    this.parity = (Parity)Enum.Parse(typeof(Parity), "Even");
            //}
            //else if (String.Compare(parity, "Mark", StringComparison.OrdinalIgnoreCase))
            //{
            //    this.parity = (Parity)Enum.Parse(typeof(Parity), "Mark");
            //}
            //else if (String.Compare(parity, "Odd", StringComparison.OrdinalIgnoreCase))
            //{
            //    this.parity = (Parity)Enum.Parse(typeof(Parity), "Odd");
            //}
            //else if (String.Compare(parity, "Space", StringComparison.OrdinalIgnoreCase))
            //{
            //    this.parity = (Parity)Enum.Parse(typeof(Parity), "Space");
            //}
        }

        public void setHandshake(string handshake)
        {
            //if (String.Compare(handshake, "None", StringComparison.OrdinalIgnoreCase))
            //{
            //    handShake = (Handshake)Enum.Parse(typeof(Handshake), "None");
            //}
            //else if (String.Compare(handshake, "XonXoff", StringComparison.OrdinalIgnoreCase))
            //{
            //    handShake = (Handshake)Enum.Parse(typeof(Handshake), "XonXoff");
            //}
            //else if (String.Compare(handshake, "RequestToSend", StringComparison.OrdinalIgnoreCase))
            //{
            //    handShake = (Handshake)Enum.Parse(typeof(Handshake), "RequestToSend");
            //}
            //else if (String.Compare(handshake, "RequestToSendXonXoff", StringComparison.OrdinalIgnoreCase))
            //{
            //    handShake = (Handshake)Enum.Parse(typeof(Handshake), "RequestToSendXonXoff");
            //}
        }

        private float receiveData(object sender, SerialDataReceivedEventArgs e)
        {
            receivedData = serialPort.ReadExisting();
            if (receivedData != String.Empty)
            {
                // get weight
         //       float weight = float.Parse(receivedData.Split("\t")[1]);

           //     return weight;
            }

            return -1;
        }
    }
}
