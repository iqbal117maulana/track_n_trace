﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO.Ports;
using System.Text.RegularExpressions;

namespace Label_Weigher_Station
{
    public partial class config : Form
    {
        FormWigher wr;
        public config()
        {
            InitializeComponent();
            getConfig();
        }

        public void setWr(FormWigher a)
        {
            wr = a;
        }
        void getConfig()
        {
            try
            {
                sqlitecs sql = new sqlitecs();
                txtIpCamera.Text = sql.config["ipCamera"];
                txtPortCamera.Text = sql.config["portCamera"];
                txtIpServer.Text = sql.config["ipServer"];
                txtPortServer.Text = sql.config["portserver"];
                txtIpDb.Text = sql.config["ipDB"];
                txtPortDb.Text = sql.config["portDB"];
                txtNamaDb.Text = sql.config["namadb"];
                txtIpPrinter.Text = sql.config["ipprinter"];
                txtPortPrinter.Text = sql.config["portprinter"];
                txtIpPrinter2.Text = sql.config["ipprinter2"];
                txtPortPrinter2.Text = sql.config["portprinter2"];
                txtIpPrinter3.Text = sql.config["ipprinter3"];
                txtPortPrinter3.Text = sql.config["portprinter3"];
                txtIpCamera2.Text = sql.config["ipCamera2"];
                txtPortCamera2.Text = sql.config["portCamera2"];
                txtIpCamera3.Text = sql.config["ipCamera3"];
                txtPortCamera3.Text = sql.config["portCamera3"];
                txtIpCamera4.Text = sql.config["ipCamera4"];
                txtPortCamera4.Text = sql.config["portCamera4"];

                cmbPort.Text = sql.config["port"];
                txtBaudrate.Text = sql.config["baudrate"];
                txtDatabits.Text = sql.config["databits"];
                txtStopBits.Text = sql.config["stopbits"];
                txtHandshake.Text = sql.config["handshake"];
                txtParity.Text = sql.config["parity"];
            }
            catch (KeyNotFoundException k)
            {

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Confirm cf = new Confirm("Are you sure? ", "Confirmation");
            bool hasilDialog = cf.conf();
            if (cekKosong())
            {
                if (hasilDialog)
                {
                    sqlitecs sql = new sqlitecs();
                    Dictionary<string, string> field = new Dictionary<string, string>();

                    field.Add("ipServer", txtIpServer.Text);
                    field.Add("portserver", txtPortServer.Text);
                    field.Add("ipDB", txtIpDb.Text);
                    field.Add("portDB", txtPortDb.Text);
                    field.Add("namadb", txtNamaDb.Text);
                    field.Add("ipprinter", txtIpPrinter.Text);
                    field.Add("portprinter", txtPortPrinter.Text);
                    field.Add("ipprinter2", txtIpPrinter2.Text);
                    field.Add("portprinter2", txtPortPrinter2.Text);
                    field.Add("ipprinter3", txtIpPrinter3.Text);
                    field.Add("portprinter3", txtPortPrinter3.Text);
                    field.Add("ipCamera", txtIpCamera.Text);
                    field.Add("portCamera", txtPortCamera.Text);
                    field.Add("ipCamera2", txtIpCamera2.Text);
                    field.Add("portCamera2", txtPortCamera2.Text);
                    field.Add("ipCamera3", txtIpCamera3.Text);
                    field.Add("portCamera3", txtPortCamera3.Text);
                    field.Add("ipCamera4", txtIpCamera4.Text);
                    field.Add("portCamera4", txtPortCamera4.Text);


                    field.Add("port", cmbPort.Text);
                    field.Add("baudrate", txtBaudrate.Text);
                    field.Add("databits", txtDatabits.Text);
                    field.Add("stopbits", txtStopBits.Text);
                    field.Add("handshake", txtHandshake.Text);
                    field.Add("parity", txtParity.Text);
                    sql.update(field, "config", "");
                    new Confirm("Configuration success saved", "Information", MessageBoxButtons.OK);
                    this.Dispose();
                }
            }
            else
            {
                new Confirm("Invalid configuration data", "Information", MessageBoxButtons.OK);
            }
        }
        internal static bool cekIP(string data)
        {
            if (data.Length < 3 & data.Length > 0)
                return false;
            int errorCounter = Regex.Matches(data, @"[a-zA-Z]").Count;
            if (errorCounter == 0)
            {
                string[] dapat = data.Split('.');
                if (dapat.Length != 4)
                {
                    valid1 = "Error : IP must 4 point";
                    return false;
                }
                if (dapat[3].Equals("0"))
                {
                    valid1 = "Error : the fourth digit of ip cannot be 0";
                    return false;
                }

                bool kosong = false;
                if (dapat[0].Equals("000"))
                    kosong = true;
                for (int i = 0; i < dapat.Length; i++)
                {
                    //if (dapat[i] == "" || dapat[i].Length <= 0)
                    //{
                    //    new Confirm("Error : IP must 4 point");
                    //    return false;
                    //}
                    if (dapat[i][0].Equals("0"))
                        return false;
                    if (dapat[i].Equals("00"))
                    {
                        valid1 = "Error : IP cannot fill 00";
                        return false;
                    }

                    bool rege = Regex.Match(dapat[i], @"^[0-9]+$").Success;
                    if (!rege)
                    {
                        valid1 = "Error : IP cannot contain alphabeth and symbols " + data;
                        return false;
                    }
                    if (dapat[i].Length == 0)
                    {
                        valid1 = "Error : Cannot empty";
                        return false;
                    }
                    int temp = int.Parse(dapat[i]);
                    if (temp >= 255)
                    {
                        valid1 = "Error : IP must be less then 255";
                        return false;
                    }
                    if (temp == 0 && kosong)
                    {
                        valid1 = "Error : First digit of ip cannot be 0";
                        return false;
                    }
                    if (dapat[i].Equals("000") && !kosong)
                    {
                        valid1 = "Error : IP cannot fill 000";
                        return false;
                    }
                    if (dapat[i][0].Equals('0') && temp != 0 && dapat[i].Length > 1)
                    {
                        valid1 = "Error : Ip cannot fill '0'";
                        return false;
                    }
                }
                return true;
            }
            else
            {
                valid1 = "Error : IP cannot contain alphabeth " + errorCounter;
                return false;
            }
            return true;
        }
        static string valid1;


        private bool cekKosong()
        {
            if (txtIpCamera.Text.Length == 0) return false;
            if (!cekIP(txtIpCamera.Text)) return false;
            if (txtPortCamera.Text.Length == 0) return false;
            if (txtIpServer.Text.Length == 0) return false;
            if (txtPortServer.Text.Length == 0) return false;
            if (txtIpDb.Text.Length == 0) return false;
            if (!cekIP(txtIpDb.Text)) return false;
            if (txtPortDb.Text.Length == 0) return false;
            if (txtNamaDb.Text.Length == 0) return false;
            if (txtIpPrinter.Text.Length == 0) return false;
            if (!cekIP(txtIpPrinter.Text)) return false;
            if (txtPortPrinter.Text.Length == 0) return false;
            if (txtIpPrinter2.Text.Length == 0) return false;
            if (txtPortPrinter2.Text.Length == 0) return false;
            if (txtIpPrinter3.Text.Length == 0) return false;
            if (txtPortPrinter3.Text.Length == 0) return false;
            if (txtIpCamera2.Text.Length == 0) return false;
            if (!cekIP(txtIpCamera2.Text)) return false;
            if (txtPortCamera2.Text.Length == 0) return false;

            if (!cekIP(txtIpCamera3.Text)) return false;
            if (txtIpCamera3.Text.Length == 0) return false;
            if (txtPortCamera3.Text.Length == 0) return false;
            if (txtIpCamera4.Text.Length == 0) return false;

            if (!cekIP(txtIpCamera4.Text)) return false;
            if (txtPortCamera4.Text.Length == 0) return false;
            if (txtBaudrate.Text.Length == 0) return false;
            if (txtDatabits.Text.Length == 0) return false;
            if (txtStopBits.Text.Length == 0) return false;
            if (txtHandshake.Text.Length == 0) return false;
            if (txtParity.Text.Length == 0) return false;
            return true;
        }

        private void txtPortCamera_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            cmbPort.Items.Clear();
            string[] ArrayComPortsNames = null;
            int index = -1;
            string ComPortName = null;
           
            ArrayComPortsNames = SerialPort.GetPortNames();
            if (ArrayComPortsNames.Length > 0)
            {
                do
                {
                    index += 1;
                    cmbPort.Items.Add(ArrayComPortsNames[index]);


                } while (!((ArrayComPortsNames[index] == ComPortName) || (index == ArrayComPortsNames.GetUpperBound(0))));
                Array.Sort(ArrayComPortsNames);

                if (index == ArrayComPortsNames.GetUpperBound(0))
                {
                    ComPortName = ArrayComPortsNames[0];
                }

                cmbPort.Text = ArrayComPortsNames[0];
            }
            else
            {
                new Confirm("Com port not found", "Information", MessageBoxButtons.OK);
            }
        }

        private void groupBox5_Enter(object sender, EventArgs e)
        {

        }
    }
}
