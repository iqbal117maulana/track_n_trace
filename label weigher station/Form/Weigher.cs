﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Label_Weigher_Station
{
    public partial class FormWigher : Form
    {
        DataTable table;
        DataTable table2;
        DataTable table3;
        Control ms;
        int ordersItemIndex;
        int ordersItemIndex2; 
        int qu = 0;
        string cek;
        string cekProduct;
     //   sqlitecs sq;
     //   dbaccess db;
        public string LogFile = "";
        List<string[]> datadetail;
        List<string[]> dataOrder = new List<string[]>();
        List<string[]> datacomboCold;
        List<string[]> datacomboWare;
        public List<string[]> datacombo;

        //tambahan disable close button
        private const int CP_NOCLOSE_BUTTON = 0x200;
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams myCp = base.CreateParams;
                myCp.ClassStyle = myCp.ClassStyle | CP_NOCLOSE_BUTTON;
                return myCp;
            }
        }
        
        public FormWigher(string admin)
        {
            InitializeComponent();
            ms = new Control();
            ms.wr = this;
            ms.startstation(admin);
            setAdminName(admin);
        }

        private void setAdminName(string admin)
        {
            List<string> field = new List<string>();
            field.Add("first_name, company ");
            string where = "id ='" + admin + "'";
            List<string[]> ds = ms.db.selectList(field, "[USERS]", where);
            if (ms.db.num_rows > 0)
            {
                lblUserId.Text = ds[0][0];
                lblRole.Text = ds[0][1];
            }
        }

        internal string getAdminName(string admin)
        {
            List<string> field = new List<string>();
            field.Add("first_name   ");
            string where = "id ='" + admin + "'";
            List<string[]> ds = ms.db.selectList(field, "[USERS]", where);
            if (ms.db.num_rows > 0)
                return ds[0][0];
            else
                return "";
        }

        private void Warehouse_FormClosed(object sender, FormClosedEventArgs e)
        {
            ms.close("");
            Application.Exit();
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                textBox1.Text = textBox1.Text.Replace("\n", "");
                if (textBox1.Text.Length > 0)
                    ms.tambahData("0,"+textBox1.Text);
                textBox1.Text = "";
            }
        }

      
        private void button1_Click(object sender, EventArgs e)
        {
           
        }

        private void button9_Click(object sender, EventArgs e)
        {
            config cfg = new config();
            cfg.setWr(this);
            cfg.ShowDialog();
        }


        private void dataGridView2_SelectionChanged(object sender, EventArgs e)
        {
           
        }
      
        private void dataGridView2_CellClick(object sender, DataGridViewCellEventArgs e)
        {
        }

        private void button3_Click(object sender, EventArgs e)
        {
            dgvReceiving.DataSource = "";
            table = new DataTable();
            table.Columns.Add("Basket ID");
            table.Columns.Add("Timestamp");
            lblQtyReceiving.Text = "" + dgvReceiving.Rows.Count;
        }

        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            if (dgvReceiving.SelectedRows.Count > 0)
                ordersItemIndex = dgvReceiving.SelectedRows[0].Index;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            dgvReceiving.Rows.RemoveAt(ordersItemIndex);
            lblQtyReceiving.Text = "" + dgvReceiving.Rows.Count;
        }

        private void textBox2_KeyDown(object sender, KeyEventArgs e)
        {
           
            
        }

        private void button5_Click(object sender, EventArgs e)
        {
           
        }

        private void button4_Click(object sender, EventArgs e)
        {
           

        }

        private void button6_Click(object sender, EventArgs e)
        {
           
        }
        
        private void textBox4_KeyDown(object sender, KeyEventArgs e)
        {
           
        }

        private void Warehouse_Load(object sender, EventArgs e)
        {
        }
        
        private void cmbPilih_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        private void cmbWare_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        private void dataGridView3_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
          
        }

        private void dataGridView1_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            lblQtyReceiving.Text = "" + dgvReceiving.Rows.Count;
        }

        private void cmbStatusOld_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        private void button8_Click(object sender, EventArgs e)
        {
            Confirm cf = new Confirm("Are you sure? ", "Confirmation");
            bool result = cf.conf();
            if (result)
            {
               // this.Dispose();
                //Login lg = new Login();
               // lg.Show();
                ms.close("");
                Application.Exit();
            }
        }

        private void groupBox3_Enter(object sender, EventArgs e)
        {

        }

        private void cmbDetail_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        private void dgvReceiving_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            lblQtyReceiving.Text = dgvReceiving.Rows.Count.ToString();
        }

        private void label14_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {

        }
        private void button2_Click_1(object sender, EventArgs e)
        {
           
        }

        private void button1_Click_2(object sender, EventArgs e)
        {
            ms.tambahData("2,0");
        }

        private void textBox2_KeyDown_1(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                ms.tambahData("2," + textBox2.Text);
                textBox2.Text = "";
            }

            
        }

        private void lbladmin_Click(object sender, EventArgs e)
        {
            ms.tambahData("1,10.0");
        }

        private void btnOpenLog_Click(object sender, EventArgs e)
        {
            Log frm;
            frm = new Log(LogFile);
            frm.ShowDialog();
            if (frm.isClear == true)
                LogFile = "";

        }

        private void btnIndicator_Click(object sender, EventArgs e)
        {
            ms.CekDevice();
        }

        }
    }
