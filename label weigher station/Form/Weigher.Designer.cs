﻿namespace Label_Weigher_Station
{
    partial class FormWigher
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormWigher));
            this.lblQtyReceiving = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblOnProgress = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.dgvReceiving = new System.Windows.Forms.DataGridView();
            this.label6 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.lblCamera = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblCamera2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblCamera3 = new System.Windows.Forms.Label();
            this.lblCamera4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label21 = new System.Windows.Forms.Label();
            this.lblDate = new System.Windows.Forms.Label();
            this.lblTime = new System.Windows.Forms.Label();
            this.lblRole = new System.Windows.Forms.Label();
            this.lblUserId = new System.Windows.Forms.Label();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.label7 = new System.Windows.Forms.Label();
            this.lblCamera1 = new System.Windows.Forms.Label();
            this.lblApplicator1 = new System.Windows.Forms.Label();
            this.lblApplicator2 = new System.Windows.Forms.Label();
            this.lblApplicator3 = new System.Windows.Forms.Label();
            this.btnIndicator = new System.Windows.Forms.Button();
            this.onApplicator3 = new System.Windows.Forms.PictureBox();
            this.offApplicator3 = new System.Windows.Forms.PictureBox();
            this.onApplicator2 = new System.Windows.Forms.PictureBox();
            this.offApplicator2 = new System.Windows.Forms.PictureBox();
            this.onApplicator1 = new System.Windows.Forms.PictureBox();
            this.offApplicator1 = new System.Windows.Forms.PictureBox();
            this.onCamera4 = new System.Windows.Forms.PictureBox();
            this.offCamera4 = new System.Windows.Forms.PictureBox();
            this.onCamera3 = new System.Windows.Forms.PictureBox();
            this.offCamera3 = new System.Windows.Forms.PictureBox();
            this.onCamera2 = new System.Windows.Forms.PictureBox();
            this.offCamera2 = new System.Windows.Forms.PictureBox();
            this.onCamera1 = new System.Windows.Forms.PictureBox();
            this.offCamera1 = new System.Windows.Forms.PictureBox();
            this.btnOpenLog = new System.Windows.Forms.Button();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.lblPrinter = new System.Windows.Forms.Label();
            this.lblPrinter2 = new System.Windows.Forms.Label();
            this.lblPrinter3 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvReceiving)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.onApplicator3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.offApplicator3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.onApplicator2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.offApplicator2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.onApplicator1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.offApplicator1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.onCamera4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.offCamera4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.onCamera3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.offCamera3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.onCamera2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.offCamera2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.onCamera1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.offCamera1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.SuspendLayout();
            // 
            // lblQtyReceiving
            // 
            this.lblQtyReceiving.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblQtyReceiving.AutoSize = true;
            this.lblQtyReceiving.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQtyReceiving.Location = new System.Drawing.Point(948, 569);
            this.lblQtyReceiving.Name = "lblQtyReceiving";
            this.lblQtyReceiving.Size = new System.Drawing.Size(19, 21);
            this.lblQtyReceiving.TabIndex = 10;
            this.lblQtyReceiving.Text = "0";
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.lblOnProgress);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.lblQtyReceiving);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.dgvReceiving);
            this.groupBox1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(12, 92);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1010, 599);
            this.groupBox1.TabIndex = 128;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Scanned";
            // 
            // lblOnProgress
            // 
            this.lblOnProgress.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblOnProgress.AutoSize = true;
            this.lblOnProgress.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOnProgress.Location = new System.Drawing.Point(754, 569);
            this.lblOnProgress.Name = "lblOnProgress";
            this.lblOnProgress.Size = new System.Drawing.Size(19, 21);
            this.lblOnProgress.TabIndex = 12;
            this.lblOnProgress.Text = "0";
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(623, 569);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(103, 21);
            this.label10.TabIndex = 11;
            this.label10.Text = "On Progress :";
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(838, 569);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(77, 21);
            this.label8.TabIndex = 9;
            this.label8.Text = "Quantity :";
            // 
            // dgvReceiving
            // 
            this.dgvReceiving.AllowUserToAddRows = false;
            this.dgvReceiving.AllowUserToDeleteRows = false;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dgvReceiving.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvReceiving.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvReceiving.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvReceiving.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvReceiving.Location = new System.Drawing.Point(13, 26);
            this.dgvReceiving.Name = "dgvReceiving";
            this.dgvReceiving.ReadOnly = true;
            this.dgvReceiving.RowHeadersVisible = false;
            this.dgvReceiving.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvReceiving.Size = new System.Drawing.Size(985, 537);
            this.dgvReceiving.TabIndex = 7;
            this.dgvReceiving.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dgvReceiving_RowsAdded);
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(1035, 799);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(101, 21);
            this.label6.TabIndex = 142;
            this.label6.Text = "Applicator 3 :";
            // 
            // label9
            // 
            this.label9.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(1035, 711);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(101, 21);
            this.label9.TabIndex = 139;
            this.label9.Text = "Applicator 2 :";
            // 
            // label11
            // 
            this.label11.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(1035, 621);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(101, 21);
            this.label11.TabIndex = 136;
            this.label11.Text = "Applicator 1 :";
            // 
            // label18
            // 
            this.label18.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(1035, 273);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(84, 21);
            this.label18.TabIndex = 20;
            this.label18.Text = "Camera 1 :";
            // 
            // lblCamera
            // 
            this.lblCamera.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblCamera.AutoSize = true;
            this.lblCamera.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCamera.Location = new System.Drawing.Point(1154, 229);
            this.lblCamera.Name = "lblCamera";
            this.lblCamera.Size = new System.Drawing.Size(0, 21);
            this.lblCamera.TabIndex = 21;
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(1035, 363);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 21);
            this.label2.TabIndex = 130;
            this.label2.Text = "Camera 2 :";
            // 
            // lblCamera2
            // 
            this.lblCamera2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblCamera2.AutoSize = true;
            this.lblCamera2.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCamera2.Location = new System.Drawing.Point(1114, 314);
            this.lblCamera2.Name = "lblCamera2";
            this.lblCamera2.Size = new System.Drawing.Size(56, 21);
            this.lblCamera2.TabIndex = 131;
            this.lblCamera2.Text = "Online";
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(1035, 451);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(84, 21);
            this.label4.TabIndex = 133;
            this.label4.Text = "Camera 3 :";
            // 
            // lblCamera3
            // 
            this.lblCamera3.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblCamera3.AutoSize = true;
            this.lblCamera3.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCamera3.Location = new System.Drawing.Point(1114, 403);
            this.lblCamera3.Name = "lblCamera3";
            this.lblCamera3.Size = new System.Drawing.Size(56, 21);
            this.lblCamera3.TabIndex = 134;
            this.lblCamera3.Text = "Online";
            // 
            // lblCamera4
            // 
            this.lblCamera4.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblCamera4.AutoSize = true;
            this.lblCamera4.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCamera4.Location = new System.Drawing.Point(1114, 491);
            this.lblCamera4.Name = "lblCamera4";
            this.lblCamera4.Size = new System.Drawing.Size(56, 21);
            this.lblCamera4.TabIndex = 146;
            this.lblCamera4.Text = "Online";
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(1035, 539);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(84, 21);
            this.label3.TabIndex = 145;
            this.label3.Text = "Camera 4 :";
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.textBox2);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.textBox1);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(12, 697);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1010, 71);
            this.groupBox2.TabIndex = 148;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Action";
            // 
            // textBox2
            // 
            this.textBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox2.Location = new System.Drawing.Point(762, 27);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(238, 29);
            this.textBox2.TabIndex = 3;
            this.textBox2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox2_KeyDown_1);
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(625, 30);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(125, 21);
            this.label5.TabIndex = 2;
            this.label5.Text = "Gs1 Master Box :";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(138, 27);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(238, 29);
            this.textBox1.TabIndex = 1;
            this.textBox1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox1_KeyDown);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(14, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(113, 21);
            this.label1.TabIndex = 0;
            this.label1.Text = "ID Master Box :";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(148)))), ((int)(((byte)(172)))));
            this.panel1.Controls.Add(this.label21);
            this.panel1.Controls.Add(this.lblDate);
            this.panel1.Controls.Add(this.lblTime);
            this.panel1.Controls.Add(this.lblRole);
            this.panel1.Controls.Add(this.lblUserId);
            this.panel1.Controls.Add(this.pictureBox9);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1202, 86);
            this.panel1.TabIndex = 151;
            // 
            // label21
            // 
            this.label21.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.White;
            this.label21.Location = new System.Drawing.Point(384, 17);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(405, 32);
            this.label21.TabIndex = 153;
            this.label21.Text = "SCALING AND LABEL APPLICATOR";
            // 
            // lblDate
            // 
            this.lblDate.AutoSize = true;
            this.lblDate.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDate.ForeColor = System.Drawing.Color.White;
            this.lblDate.Location = new System.Drawing.Point(216, 35);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(31, 15);
            this.lblDate.TabIndex = 152;
            this.lblDate.Text = "Date";
            this.lblDate.Visible = false;
            // 
            // lblTime
            // 
            this.lblTime.AutoSize = true;
            this.lblTime.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTime.ForeColor = System.Drawing.Color.White;
            this.lblTime.Location = new System.Drawing.Point(216, 17);
            this.lblTime.Name = "lblTime";
            this.lblTime.Size = new System.Drawing.Size(35, 15);
            this.lblTime.TabIndex = 151;
            this.lblTime.Text = "Time";
            this.lblTime.Visible = false;
            // 
            // lblRole
            // 
            this.lblRole.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblRole.AutoSize = true;
            this.lblRole.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRole.ForeColor = System.Drawing.Color.White;
            this.lblRole.Location = new System.Drawing.Point(1102, 41);
            this.lblRole.Name = "lblRole";
            this.lblRole.Size = new System.Drawing.Size(54, 21);
            this.lblRole.TabIndex = 150;
            this.lblRole.Text = "admin";
            // 
            // lblUserId
            // 
            this.lblUserId.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblUserId.AutoSize = true;
            this.lblUserId.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUserId.ForeColor = System.Drawing.Color.White;
            this.lblUserId.Location = new System.Drawing.Point(1101, 21);
            this.lblUserId.Name = "lblUserId";
            this.lblUserId.Size = new System.Drawing.Size(59, 21);
            this.lblUserId.TabIndex = 149;
            this.lblUserId.Text = "admin";
            // 
            // pictureBox9
            // 
            this.pictureBox9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox9.Image = global::Label_Weigher_Station.Properties.Resources.user;
            this.pictureBox9.Location = new System.Drawing.Point(1050, 17);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(45, 45);
            this.pictureBox9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox9.TabIndex = 148;
            this.pictureBox9.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(47)))), ((int)(((byte)(54)))));
            this.panel2.Controls.Add(this.pictureBox10);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(210, 86);
            this.panel2.TabIndex = 38;
            // 
            // pictureBox10
            // 
            this.pictureBox10.ImageLocation = "logo_biofarma.png";
            this.pictureBox10.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox10.InitialImage")));
            this.pictureBox10.Location = new System.Drawing.Point(30, 5);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(150, 75);
            this.pictureBox10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox10.TabIndex = 129;
            this.pictureBox10.TabStop = false;
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label7.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(437, 41);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(320, 29);
            this.label7.TabIndex = 35;
            this.label7.Text = "STATION";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblCamera1
            // 
            this.lblCamera1.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblCamera1.AutoSize = true;
            this.lblCamera1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCamera1.Location = new System.Drawing.Point(1114, 228);
            this.lblCamera1.Name = "lblCamera1";
            this.lblCamera1.Size = new System.Drawing.Size(56, 21);
            this.lblCamera1.TabIndex = 158;
            this.lblCamera1.Text = "Online";
            // 
            // lblApplicator1
            // 
            this.lblApplicator1.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblApplicator1.AutoSize = true;
            this.lblApplicator1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblApplicator1.Location = new System.Drawing.Point(1114, 576);
            this.lblApplicator1.Name = "lblApplicator1";
            this.lblApplicator1.Size = new System.Drawing.Size(56, 21);
            this.lblApplicator1.TabIndex = 170;
            this.lblApplicator1.Text = "Online";
            // 
            // lblApplicator2
            // 
            this.lblApplicator2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblApplicator2.AutoSize = true;
            this.lblApplicator2.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblApplicator2.Location = new System.Drawing.Point(1114, 662);
            this.lblApplicator2.Name = "lblApplicator2";
            this.lblApplicator2.Size = new System.Drawing.Size(56, 21);
            this.lblApplicator2.TabIndex = 173;
            this.lblApplicator2.Text = "Online";
            // 
            // lblApplicator3
            // 
            this.lblApplicator3.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblApplicator3.AutoSize = true;
            this.lblApplicator3.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblApplicator3.Location = new System.Drawing.Point(1114, 751);
            this.lblApplicator3.Name = "lblApplicator3";
            this.lblApplicator3.Size = new System.Drawing.Size(56, 21);
            this.lblApplicator3.TabIndex = 176;
            this.lblApplicator3.Text = "Online";
            // 
            // btnIndicator
            // 
            this.btnIndicator.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnIndicator.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnIndicator.Image = global::Label_Weigher_Station.Properties.Resources.indicator;
            this.btnIndicator.Location = new System.Drawing.Point(168, 774);
            this.btnIndicator.Name = "btnIndicator";
            this.btnIndicator.Size = new System.Drawing.Size(150, 40);
            this.btnIndicator.TabIndex = 179;
            this.btnIndicator.Text = "Indicator";
            this.btnIndicator.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnIndicator.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnIndicator.UseVisualStyleBackColor = true;
            this.btnIndicator.Click += new System.EventHandler(this.btnIndicator_Click);
            // 
            // onApplicator3
            // 
            this.onApplicator3.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.onApplicator3.ErrorImage = global::Label_Weigher_Station.Properties.Resources.on1;
            this.onApplicator3.Image = global::Label_Weigher_Station.Properties.Resources.on1;
            this.onApplicator3.ImageLocation = "";
            this.onApplicator3.Location = new System.Drawing.Point(1119, 734);
            this.onApplicator3.Name = "onApplicator3";
            this.onApplicator3.Size = new System.Drawing.Size(14, 14);
            this.onApplicator3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.onApplicator3.TabIndex = 178;
            this.onApplicator3.TabStop = false;
            this.onApplicator3.Visible = false;
            // 
            // offApplicator3
            // 
            this.offApplicator3.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.offApplicator3.ErrorImage = global::Label_Weigher_Station.Properties.Resources.off1;
            this.offApplicator3.Image = global::Label_Weigher_Station.Properties.Resources.off1;
            this.offApplicator3.ImageLocation = "";
            this.offApplicator3.Location = new System.Drawing.Point(1143, 734);
            this.offApplicator3.Name = "offApplicator3";
            this.offApplicator3.Size = new System.Drawing.Size(14, 14);
            this.offApplicator3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.offApplicator3.TabIndex = 177;
            this.offApplicator3.TabStop = false;
            this.offApplicator3.Visible = false;
            // 
            // onApplicator2
            // 
            this.onApplicator2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.onApplicator2.ErrorImage = global::Label_Weigher_Station.Properties.Resources.on1;
            this.onApplicator2.Image = global::Label_Weigher_Station.Properties.Resources.on1;
            this.onApplicator2.ImageLocation = "";
            this.onApplicator2.Location = new System.Drawing.Point(1119, 645);
            this.onApplicator2.Name = "onApplicator2";
            this.onApplicator2.Size = new System.Drawing.Size(14, 14);
            this.onApplicator2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.onApplicator2.TabIndex = 175;
            this.onApplicator2.TabStop = false;
            this.onApplicator2.Visible = false;
            // 
            // offApplicator2
            // 
            this.offApplicator2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.offApplicator2.ErrorImage = global::Label_Weigher_Station.Properties.Resources.off1;
            this.offApplicator2.Image = global::Label_Weigher_Station.Properties.Resources.off1;
            this.offApplicator2.ImageLocation = "";
            this.offApplicator2.Location = new System.Drawing.Point(1143, 645);
            this.offApplicator2.Name = "offApplicator2";
            this.offApplicator2.Size = new System.Drawing.Size(14, 14);
            this.offApplicator2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.offApplicator2.TabIndex = 174;
            this.offApplicator2.TabStop = false;
            this.offApplicator2.Visible = false;
            // 
            // onApplicator1
            // 
            this.onApplicator1.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.onApplicator1.ErrorImage = global::Label_Weigher_Station.Properties.Resources.on1;
            this.onApplicator1.Image = global::Label_Weigher_Station.Properties.Resources.on1;
            this.onApplicator1.ImageLocation = "";
            this.onApplicator1.Location = new System.Drawing.Point(1119, 559);
            this.onApplicator1.Name = "onApplicator1";
            this.onApplicator1.Size = new System.Drawing.Size(14, 14);
            this.onApplicator1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.onApplicator1.TabIndex = 172;
            this.onApplicator1.TabStop = false;
            this.onApplicator1.Visible = false;
            // 
            // offApplicator1
            // 
            this.offApplicator1.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.offApplicator1.ErrorImage = global::Label_Weigher_Station.Properties.Resources.off1;
            this.offApplicator1.Image = global::Label_Weigher_Station.Properties.Resources.off1;
            this.offApplicator1.ImageLocation = "";
            this.offApplicator1.Location = new System.Drawing.Point(1142, 559);
            this.offApplicator1.Name = "offApplicator1";
            this.offApplicator1.Size = new System.Drawing.Size(14, 14);
            this.offApplicator1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.offApplicator1.TabIndex = 171;
            this.offApplicator1.TabStop = false;
            this.offApplicator1.Visible = false;
            // 
            // onCamera4
            // 
            this.onCamera4.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.onCamera4.ErrorImage = global::Label_Weigher_Station.Properties.Resources.on1;
            this.onCamera4.Image = global::Label_Weigher_Station.Properties.Resources.on1;
            this.onCamera4.ImageLocation = "";
            this.onCamera4.Location = new System.Drawing.Point(1118, 474);
            this.onCamera4.Name = "onCamera4";
            this.onCamera4.Size = new System.Drawing.Size(14, 14);
            this.onCamera4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.onCamera4.TabIndex = 169;
            this.onCamera4.TabStop = false;
            this.onCamera4.Visible = false;
            // 
            // offCamera4
            // 
            this.offCamera4.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.offCamera4.ErrorImage = global::Label_Weigher_Station.Properties.Resources.off1;
            this.offCamera4.Image = global::Label_Weigher_Station.Properties.Resources.off1;
            this.offCamera4.ImageLocation = "";
            this.offCamera4.Location = new System.Drawing.Point(1142, 474);
            this.offCamera4.Name = "offCamera4";
            this.offCamera4.Size = new System.Drawing.Size(14, 14);
            this.offCamera4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.offCamera4.TabIndex = 168;
            this.offCamera4.TabStop = false;
            this.offCamera4.Visible = false;
            // 
            // onCamera3
            // 
            this.onCamera3.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.onCamera3.ErrorImage = global::Label_Weigher_Station.Properties.Resources.on1;
            this.onCamera3.Image = global::Label_Weigher_Station.Properties.Resources.on1;
            this.onCamera3.ImageLocation = "";
            this.onCamera3.Location = new System.Drawing.Point(1118, 386);
            this.onCamera3.Name = "onCamera3";
            this.onCamera3.Size = new System.Drawing.Size(14, 14);
            this.onCamera3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.onCamera3.TabIndex = 166;
            this.onCamera3.TabStop = false;
            this.onCamera3.Visible = false;
            // 
            // offCamera3
            // 
            this.offCamera3.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.offCamera3.ErrorImage = global::Label_Weigher_Station.Properties.Resources.off1;
            this.offCamera3.Image = global::Label_Weigher_Station.Properties.Resources.off1;
            this.offCamera3.ImageLocation = "";
            this.offCamera3.Location = new System.Drawing.Point(1142, 386);
            this.offCamera3.Name = "offCamera3";
            this.offCamera3.Size = new System.Drawing.Size(14, 14);
            this.offCamera3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.offCamera3.TabIndex = 165;
            this.offCamera3.TabStop = false;
            this.offCamera3.Visible = false;
            // 
            // onCamera2
            // 
            this.onCamera2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.onCamera2.ErrorImage = global::Label_Weigher_Station.Properties.Resources.on1;
            this.onCamera2.Image = global::Label_Weigher_Station.Properties.Resources.on1;
            this.onCamera2.ImageLocation = "";
            this.onCamera2.Location = new System.Drawing.Point(1118, 297);
            this.onCamera2.Name = "onCamera2";
            this.onCamera2.Size = new System.Drawing.Size(14, 14);
            this.onCamera2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.onCamera2.TabIndex = 163;
            this.onCamera2.TabStop = false;
            this.onCamera2.Visible = false;
            // 
            // offCamera2
            // 
            this.offCamera2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.offCamera2.ErrorImage = global::Label_Weigher_Station.Properties.Resources.off1;
            this.offCamera2.Image = global::Label_Weigher_Station.Properties.Resources.off1;
            this.offCamera2.ImageLocation = "";
            this.offCamera2.Location = new System.Drawing.Point(1142, 297);
            this.offCamera2.Name = "offCamera2";
            this.offCamera2.Size = new System.Drawing.Size(14, 14);
            this.offCamera2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.offCamera2.TabIndex = 162;
            this.offCamera2.TabStop = false;
            this.offCamera2.Visible = false;
            // 
            // onCamera1
            // 
            this.onCamera1.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.onCamera1.ErrorImage = global::Label_Weigher_Station.Properties.Resources.on1;
            this.onCamera1.Image = global::Label_Weigher_Station.Properties.Resources.on1;
            this.onCamera1.ImageLocation = "";
            this.onCamera1.Location = new System.Drawing.Point(1118, 211);
            this.onCamera1.Name = "onCamera1";
            this.onCamera1.Size = new System.Drawing.Size(14, 14);
            this.onCamera1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.onCamera1.TabIndex = 160;
            this.onCamera1.TabStop = false;
            this.onCamera1.Visible = false;
            // 
            // offCamera1
            // 
            this.offCamera1.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.offCamera1.ErrorImage = global::Label_Weigher_Station.Properties.Resources.off1;
            this.offCamera1.Image = global::Label_Weigher_Station.Properties.Resources.off1;
            this.offCamera1.ImageLocation = "";
            this.offCamera1.Location = new System.Drawing.Point(1142, 211);
            this.offCamera1.Name = "offCamera1";
            this.offCamera1.Size = new System.Drawing.Size(14, 14);
            this.offCamera1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.offCamera1.TabIndex = 159;
            this.offCamera1.TabStop = false;
            this.offCamera1.Visible = false;
            // 
            // btnOpenLog
            // 
            this.btnOpenLog.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnOpenLog.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOpenLog.Image = global::Label_Weigher_Station.Properties.Resources.log;
            this.btnOpenLog.Location = new System.Drawing.Point(12, 774);
            this.btnOpenLog.Name = "btnOpenLog";
            this.btnOpenLog.Size = new System.Drawing.Size(150, 40);
            this.btnOpenLog.TabIndex = 157;
            this.btnOpenLog.Text = "Log File";
            this.btnOpenLog.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnOpenLog.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnOpenLog.UseVisualStyleBackColor = true;
            this.btnOpenLog.Click += new System.EventHandler(this.btnOpenLog_Click);
            // 
            // pictureBox8
            // 
            this.pictureBox8.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.pictureBox8.ErrorImage = ((System.Drawing.Image)(resources.GetObject("pictureBox8.ErrorImage")));
            this.pictureBox8.ImageLocation = "camera.png";
            this.pictureBox8.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox8.InitialImage")));
            this.pictureBox8.Location = new System.Drawing.Point(1040, 474);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(73, 62);
            this.pictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox8.TabIndex = 147;
            this.pictureBox8.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.pictureBox5.ErrorImage = ((System.Drawing.Image)(resources.GetObject("pictureBox5.ErrorImage")));
            this.pictureBox5.ImageLocation = "applicator.png";
            this.pictureBox5.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox5.InitialImage")));
            this.pictureBox5.Location = new System.Drawing.Point(1039, 734);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(73, 62);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox5.TabIndex = 144;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.pictureBox6.ErrorImage = ((System.Drawing.Image)(resources.GetObject("pictureBox6.ErrorImage")));
            this.pictureBox6.ImageLocation = "applicator.png";
            this.pictureBox6.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox6.InitialImage")));
            this.pictureBox6.Location = new System.Drawing.Point(1039, 645);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(73, 62);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox6.TabIndex = 141;
            this.pictureBox6.TabStop = false;
            // 
            // pictureBox7
            // 
            this.pictureBox7.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.pictureBox7.ErrorImage = ((System.Drawing.Image)(resources.GetObject("pictureBox7.ErrorImage")));
            this.pictureBox7.ImageLocation = "applicator.png";
            this.pictureBox7.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox7.InitialImage")));
            this.pictureBox7.Location = new System.Drawing.Point(1040, 557);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(73, 62);
            this.pictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox7.TabIndex = 138;
            this.pictureBox7.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.pictureBox4.ErrorImage = ((System.Drawing.Image)(resources.GetObject("pictureBox4.ErrorImage")));
            this.pictureBox4.ImageLocation = "camera.png";
            this.pictureBox4.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox4.InitialImage")));
            this.pictureBox4.Location = new System.Drawing.Point(1039, 386);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(73, 62);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox4.TabIndex = 135;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.pictureBox2.ErrorImage = ((System.Drawing.Image)(resources.GetObject("pictureBox2.ErrorImage")));
            this.pictureBox2.ImageLocation = "camera.png";
            this.pictureBox2.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox2.InitialImage")));
            this.pictureBox2.Location = new System.Drawing.Point(1039, 297);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(73, 62);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 132;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.pictureBox3.ErrorImage = ((System.Drawing.Image)(resources.GetObject("pictureBox3.ErrorImage")));
            this.pictureBox3.ImageLocation = "camera.png";
            this.pictureBox3.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox3.InitialImage")));
            this.pictureBox3.Location = new System.Drawing.Point(1040, 209);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(73, 62);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 120;
            this.pictureBox3.TabStop = false;
            // 
            // button8
            // 
            this.button8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button8.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button8.Image = global::Label_Weigher_Station.Properties.Resources.logout;
            this.button8.Location = new System.Drawing.Point(1039, 104);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(150, 40);
            this.button8.TabIndex = 21;
            this.button8.Text = "Logout";
            this.button8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button8.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button9
            // 
            this.button9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button9.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button9.Image = global::Label_Weigher_Station.Properties.Resources.config;
            this.button9.Location = new System.Drawing.Point(1039, 150);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(150, 40);
            this.button9.TabIndex = 20;
            this.button9.Text = "Config";
            this.button9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button9.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // lblPrinter
            // 
            this.lblPrinter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblPrinter.AutoSize = true;
            this.lblPrinter.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrinter.Location = new System.Drawing.Point(1136, 431);
            this.lblPrinter.Name = "lblPrinter";
            this.lblPrinter.Size = new System.Drawing.Size(0, 20);
            this.lblPrinter.TabIndex = 180;
            this.lblPrinter.Visible = false;
            // 
            // lblPrinter2
            // 
            this.lblPrinter2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblPrinter2.AutoSize = true;
            this.lblPrinter2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrinter2.Location = new System.Drawing.Point(1139, 519);
            this.lblPrinter2.Name = "lblPrinter2";
            this.lblPrinter2.Size = new System.Drawing.Size(0, 20);
            this.lblPrinter2.TabIndex = 181;
            this.lblPrinter2.Visible = false;
            // 
            // lblPrinter3
            // 
            this.lblPrinter3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblPrinter3.AutoSize = true;
            this.lblPrinter3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrinter3.Location = new System.Drawing.Point(1176, 731);
            this.lblPrinter3.Name = "lblPrinter3";
            this.lblPrinter3.Size = new System.Drawing.Size(0, 20);
            this.lblPrinter3.TabIndex = 182;
            this.lblPrinter3.Visible = false;
            // 
            // FormWigher
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1202, 826);
            this.Controls.Add(this.lblPrinter3);
            this.Controls.Add(this.lblPrinter2);
            this.Controls.Add(this.lblPrinter);
            this.Controls.Add(this.btnIndicator);
            this.Controls.Add(this.onApplicator3);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.offApplicator3);
            this.Controls.Add(this.lblApplicator3);
            this.Controls.Add(this.onApplicator2);
            this.Controls.Add(this.offApplicator2);
            this.Controls.Add(this.lblApplicator2);
            this.Controls.Add(this.onApplicator1);
            this.Controls.Add(this.offApplicator1);
            this.Controls.Add(this.lblApplicator1);
            this.Controls.Add(this.onCamera4);
            this.Controls.Add(this.offCamera4);
            this.Controls.Add(this.onCamera3);
            this.Controls.Add(this.offCamera3);
            this.Controls.Add(this.onCamera2);
            this.Controls.Add(this.offCamera2);
            this.Controls.Add(this.onCamera1);
            this.Controls.Add(this.offCamera1);
            this.Controls.Add(this.lblCamera1);
            this.Controls.Add(this.btnOpenLog);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.pictureBox8);
            this.Controls.Add(this.lblCamera4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.pictureBox6);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.pictureBox7);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.lblCamera3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.lblCamera2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.lblCamera);
            this.Controls.Add(this.label18);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormWigher";
            this.Text = " ";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Warehouse_Load);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Warehouse_FormClosed);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvReceiving)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.onApplicator3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.offApplicator3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.onApplicator2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.offApplicator2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.onApplicator1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.offApplicator1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.onCamera4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.offCamera4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.onCamera3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.offCamera3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.onCamera2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.offCamera2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.onCamera1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.offCamera1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        public System.Windows.Forms.Label lblQtyReceiving;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label8;
        public System.Windows.Forms.DataGridView dgvReceiving;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.Label label18;
        public System.Windows.Forms.Label lblCamera;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.Label lblCamera2;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label4;
        public System.Windows.Forms.Label lblCamera3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox8;
        public System.Windows.Forms.Label lblCamera4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label5;
        public System.Windows.Forms.Label lblOnProgress;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label lblDate;
        private System.Windows.Forms.Label lblTime;
        public System.Windows.Forms.Label lblRole;
        public System.Windows.Forms.Label lblUserId;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnOpenLog;
        public System.Windows.Forms.Label lblCamera1;
        public System.Windows.Forms.Label lblApplicator1;
        public System.Windows.Forms.Label lblApplicator2;
        public System.Windows.Forms.Label lblApplicator3;
        private System.Windows.Forms.Button btnIndicator;
        public System.Windows.Forms.Label label9;
        public System.Windows.Forms.Label label11;
        public System.Windows.Forms.PictureBox onCamera1;
        public System.Windows.Forms.PictureBox offCamera1;
        public System.Windows.Forms.PictureBox onCamera2;
        public System.Windows.Forms.PictureBox offCamera2;
        public System.Windows.Forms.PictureBox onCamera3;
        public System.Windows.Forms.PictureBox offCamera3;
        public System.Windows.Forms.PictureBox onCamera4;
        public System.Windows.Forms.PictureBox offCamera4;
        public System.Windows.Forms.PictureBox onApplicator1;
        public System.Windows.Forms.PictureBox offApplicator1;
        public System.Windows.Forms.PictureBox onApplicator2;
        public System.Windows.Forms.PictureBox offApplicator2;
        public System.Windows.Forms.PictureBox onApplicator3;
        public System.Windows.Forms.PictureBox offApplicator3;
        public System.Windows.Forms.Label lblPrinter;
        public System.Windows.Forms.Label lblPrinter2;
        public System.Windows.Forms.Label lblPrinter3;
    }
}

