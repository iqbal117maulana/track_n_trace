﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Outfeed_Station.Model
{
    class Blister
    {
        public string BLISTERPACKID { get; set; }
        public string INNERBOXID { get; set; }

        public string CREATEDTIME { get; set; }
        public string ISREJECT { get; set; }
        public string REJECTTIME { get; set; }

        public string FLAG { get; set; }
        public string BATCHNUMBER { get; set; }

        Modul.DBACCESS db = new Modul.DBACCESS();

        public bool CHECK_BLISTER()
        {
            try
            {
                db.FIELD.Add("blisterpackId");
                db.WHERE = "(blisterpackId = '" + BLISTERPACKID + "')";

                if (BATCHNUMBER != null)
                    db.WHERE += " AND BATCHNUMBER = '" + BATCHNUMBER + "'";

                if (ISREJECT != null)
                    db.WHERE += " AND isreject = '" + ISREJECT + "'";

                db.TABLE = "BLISTERPACK";
                db.SELECT();
                if (db.num_rows > 0)
                {
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                Util.Common.simpanLog("Error Blister : " + ex.Message);
                return false;
            }
        }

        internal bool DECOMMISSION()
        {
            db.PARAMETER.Add("isreject", "2");
            db.WHERE = "(blisterpackId = '" + BLISTERPACKID + "')";
            db.TABLE = "BLISTERPACK";
            if (BATCHNUMBER.Length > 0)
                db.WHERE += "AND BATCHNUMBER = '" + BATCHNUMBER + "'";
            return db.UPDATE();
        }

        internal bool UPDATE_EMPTY_INNERBOX()
        {
            db.PARAMETER.Add("INNERBOXID", "null");
            db.WHERE = "INNERBOXID =(select infeedInnerBoxId from innerBox where infeedInnerBoxId = '" + INNERBOXID + "' or gsOneInnerBoxId = '" + INNERBOXID + "')";
            db.TABLE = "blisterPack";
            if (BATCHNUMBER.Length > 0)
                db.WHERE += " AND BATCHNUMBER = '" + BATCHNUMBER + "'";
            return db.UPDATE();
        }


    }
}
