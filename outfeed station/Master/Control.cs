﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Data;
using System.Net.NetworkInformation;
using System.ComponentModel;

namespace Outfeed_Station
{
    public class Control
    {
        public const string NUMBER_CODE = "03";
        public const string NAMA_FIELD = "infeedInnerBoxId";
        public const string NAMA_TABLE_TEMP = "tmp_gsOneVial";
        public const string NAMA_TABLE_UTAMA = "innerBox";
        public const string NAMA_STATION = "OUTFEED";
        public int qtySendToPrinter;
        public int qtyGenerateCode;
        public const string NAMA_SEQ = "outfeedseq";
        public char delimit = ',';

        public result rs;
        public sqlitecs sqlite;
        public dbaccess db;
        public Outfeed bl;
        public generateCode Code;
        private Thread serverThread;
        private Thread serverThreadCamera;
        public string batch;
        public string adminid;
        public System.Timers.Timer delayTimer;
        public System.Timers.Timer delaydevice;
        public string linenumber;
        public string linename;
        public List<string[]> datacombo;
        public bool start;
        public bool startDelay;
        public int templastcountkirim;
        public List<string> dataSendToPrinter;
        public string productmodelid;
        public string[] dataPackagingOrder;
        public int countVaccine = 0;

        DataTable tableProductReceive = new DataTable();
        DataTable tableProductSend = new DataTable();
        List<string> dataVaccine;
        int tempTerima;
        int tempTerimaKirimUlang = 1;
        int qtySend = 0;
        int qtyReceive = 0;

        private BackgroundWorker worker = new BackgroundWorker();
        private AutoResetEvent resetEvent = new AutoResetEvent(false);


        public ServerMultiClient srv;

        public Log lg = new Log();
        public int allQty = 0;
        public server srv2;

        public void MulaiServer()
        {
            srv = new ServerMultiClient();
            srv.Start(this, sqlite.config["portserver"]);
        }

        public void MulaiServer2()
        {
            srv2 = new server(bl, sqlite.config["portCamera"], this, 1);
        }

        public void StopServer()
        {
            srv.listener.Stop();
            //srv2.StreamClose();
        }

        public void StartServer2()
        {
            srv.listener.Start();
            //srv2.listener.Start();
        }


        public void disable()
        {
            bl.btnStart.Text = "Stop";
            bl.btnAudit.Enabled = false;
            bl.btnDeco.Enabled = false;
            bl.btnLogout.Enabled = false;
            bl.btnConfig.Enabled = false;
            bl.btnSample.Enabled = false;
            bl.btnIndicator.Enabled = false;
        }

        public void enable()
        {
            bl.btnStart.Text = "Start";
            bl.btnAudit.Enabled = true;
            bl.btnDeco.Enabled = true;
            bl.btnLogout.Enabled = true;
            bl.btnConfig.Enabled = true;
            bl.btnSample.Enabled = true;
            bl.btnIndicator.Enabled = true;
        }

        //public void cmb()
        //{
        //    List<string> field = new List<string>();
        //    field.Add("batchNumber,productModelId");
        //    string where = "status = '1' and linepackagingid = '" + linenumber + "'";
        //    datacombo = db.selectList(field, "[packaging_order]", where);
        //    List<string> da = new List<string>();
        //    if (datacombo.Count > 0)
        //    {
        //        for (int i = 0; i < datacombo.Count; i++)
        //        {
        //            da.Add(datacombo[i][0]);
        //        }
        //        bl.cmbBatch.DataSource = da;
        //    }
        //}

        public void log(string data)
        {

            if (lg.txtLog.InvokeRequired)
            {
                lg.txtLog.Invoke(new Action<string>(log), new object[] { data });
                return;
            }

            try
            {
                string dates = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                string simpan = dates + "\t" + data + "\n";
                lg.txtLog.AppendText(simpan + Environment.NewLine);
            }
            catch (ObjectDisposedException ob)
            {

            }
        }

        public string movementType = "6";
        public void move(string To)
        {
            Dictionary<string, string> field = new Dictionary<string, string>();
            field.Add("userid", adminid);
            field.Add("[from]", Master.NAMA_TABLE_UTAMA);
            field.Add("[to]", To);
            field.Add("qty", "" + Code.PackagingOrder);
            field.Add("movementType", movementType);
            field.Add("orderNumber", batch);
            db.Movement(field);
        }

        public bool cekPrinterready()
        {
            print pr = new print();
            if (pr.cekKoneksi())
            {
                if (pr.cekStatusPrinter())
                {
                    bl.lblCamera.Text = pr.statusLeibinger;
                    return true;
                }
            }
            new Confirm("Printer Not Connected " + pr.statusLeibinger);
            bl.lblCamera.Text = pr.statusLeibinger;
            return false;
        }

        public void startStation()
        {
            db.adminid = adminid;
            db.from = "Outfeed Station";
            db.eventname = "Start Station";
            db.systemlog();

            rs = new result();
            bl.lblAdmin.Text = getAdminName(adminid);
            log("Server Port : " + sqlite.config["portserver"]);

            //cmb();
            clearDataReceive();
            cekQty();
            //SetDataGridSend("1", "");

            clearDataSend();
            quantityReceive = 0;
            sendData_plc = new SendData_PLC();
            sendData_plc.IPAddress = sqlite.config["IpPLC"];
            sendData_plc.Port = sqlite.config["PortPLC"];
            sendData_plc.Timeout = "5";

            serverThread = new Thread(new ThreadStart(MulaiServer));
            serverThread.Start();
            serverThreadCamera = new Thread(new ThreadStart(MulaiServer2));
            serverThreadCamera.Start();
            //backgroundwo();
        }
        internal string getAdminName(string admin)
        {
            List<string> field = new List<string>();
            field.Add("first_name   ");
            string where = "id ='" + admin + "'";
            List<string[]> ds = db.selectList(field, "[USERS]", where);
            if (db.num_rows > 0)
                return ds[0][0];
            else
                return "";
        }

        public void cekQty()
        {
            qtyGenerateCode = int.Parse(sqlite.config["qtygenerate"]);
            qtySend = int.Parse(sqlite.config["qtysend"]);
            qtySendToPrinter = int.Parse(sqlite.config["qtysend"]); 
        }
        public void sudahditerimaProduct(string[] hasilsplit)
        {
            // tambah di table result
            Dictionary<string, string> field;
            field = new Dictionary<string, string>();
            field.Add("gsone", hasilsplit[1]);
            field.Add("flag", hasilsplit[0]);
            field.Add("createdtime", db.getUnixString());
            field.Add("infeed", hasilsplit[2]);
            rs.insert(field, "result");
            setlog(hasilsplit);
            //hapus yang ada di tmp
            string where = "gsone ='" + hasilsplit[1] + "'";
            sqlite.delete(where, NAMA_TABLE_TEMP);
        }

        //cek yang sudah dikirim ke printer
        public bool CekData(string blister)
        {
            List<string> field = new List<string>();
            field.Add("gsone");
            string where = "gsone = '" + blister + "' and isused = '1'";
            sqlite.select(field, NAMA_TABLE_TEMP, where);
            if (sqlite.num_rows > 0)
                return true;
            else
                return false;
        }


        SendData_PLC sendData_plc;
        public void SendData_PLC(string data)
        {
            try
            {
                sendData_plc.TextData = data;
                sendData_plc.SendText();
                //  sendData_plc.Disconect();
            }
            catch (Exception)
            {
                new Confirm("Something wrong witg PLC", "Information", MessageBoxButtons.OK);
            }
        }

        //cek status
        public string cek(string data)
        {
            if (data.Equals("0"))
                return "Pass";
            else if (data.Equals("2"))
            {
                return "Data infeed not found";
            }
            else if (data.Equals("3"))
            {
                return "Data infeed on vaccine not found";
            }
            else
            {
                return "Fail";
            }
        }
        public string cekStatus(string data)
        {
            if (data.Equals("0"))
            {
                allQty++;
                return "Pass";
            }
            else if (data.Equals("2"))
            {
                SendData_PLC("1");
                return "Data infeed not found";
            }
            else if (data.Equals("3"))
            {
                SendData_PLC("1");
                return "Data infeed on vaccine not found";
            }
            else if (data.Equals("4"))
            {
                SendData_PLC("1");
                return "Data infeed wrong batchnumber";
            }
            else if (data.Equals("5"))
            {
                SendData_PLC("1");
                return "Data Vaccine not full";
            }
            else if (data.Equals("6"))
            {
                SendData_PLC("1");
                return "Data innerbox already register";
            }
            else if (data.Equals("7"))
            {
                SendData_PLC("1");
                return "Data outfeed is null";
            }
            else if (data.Equals("1"))
            {
                return "Fail";
            }
            else
            {
                SendData_PLC("1");
                return "Fail unrecognized";
            }
        }


        private List<string> dataMassInfeed(string data)
        {
            char[] delimiters = new char[] { '|' };
            List<string> hasilSplit = new List<string>(data.Split(delimiters, StringSplitOptions.RemoveEmptyEntries));
            return hasilSplit;
        }

        public void AddNewVaccine(string[] hasilsplit)
        {
            // Update table infeed
            Dictionary<string, string> field;
            List<string> fieldList = new List<string>();
            fieldList.Add("infeedInnerBoxId");
            // cek data yang ada di database
            string where1 = "infeedInnerBoxId ='" + hasilsplit[2] + "'";
            List<string[]> dataCapid = db.selectList(fieldList, "innerBox", where1);
            if (db.num_rows > 0)
            {
                //masukan datanya
                field = new Dictionary<string, string>();
                field.Add("gsOneInnerBoxId", hasilsplit[1]);
                field.Add("isreject", hasilsplit[0]);
                db.update(field, "innerBox", "infeedInnerBoxId ='" + hasilsplit[2] + "'");

                // update data yang ada di Vaccine
                field = new Dictionary<string, string>();
                field.Add("innerBoxGsOneId", hasilsplit[1]);
                field.Add("isreject", hasilsplit[0]);
                db.update(field, "Vaccine", "innerBoxId ='" + hasilsplit[2] + "'");
            }
            else
            {
                log("data Infeed: " + hasilsplit[2] + " Not Found");
            }
            
            //hapus yang ada di tmp
            string where = NAMA_FIELD + " ='" + hasilsplit[1] + "'";
            sqlite.delete(where, NAMA_TABLE_TEMP);
        }
        bool auditStarted = false;
        // Terima data dari PLC
        public void tambahData(string data)
        {
            if (bl.InvokeRequired)
            {
                bl.Invoke(new Action<string>(tambahData), new object[] { data });
                return;
            }
            if (data.Length > 0)
                log("Receive Data:" + data);
            data = data.Replace("\r", "");
            data = data.Replace("\n", "");
            data = data.Replace("\\7E\\7E", "\\7E");
            data = data.Replace("]d2", "");
            data = data.Replace(" ", "");
            List<string> dataMas = dataMassInfeed(data);
            for (int i = 0; i < dataMas.Count; i++)
            {
                log("Proses Data:" + dataMas[i]);
                string[] hasilSplit = dataMas[i].Split(delimit);
                if (data[0].Equals('0'))
                {
                    AddNewVaccine(hasilSplit);
                    updatetableReveive(hasilSplit);
                    sudahditerimaProduct(hasilSplit);

                }
                else
                {
                    updatetableReveive(hasilSplit);
                }
                if (auditStarted)
                    cekKirimUlang();
            }
        }

        private List<string> dataMass(string data)
        {
            char[] delimiters = new char[] { '|' };
            List<string> hasilSplit = new List<string>(data.Split(delimiters, StringSplitOptions.RemoveEmptyEntries));
            return hasilSplit;
        }

        List<string[]> dataTerimaBaru = new List<string[]>();

        private bool cekDataSample(string capid)
        {
            List<string> field = new List<string>();
            field.Add("capid");
            string where = "flag = 1 And capid ='" + capid + "'";
            List<string[]> ds = sqlite.select(field, "tmp_cap_id", where);
            if (sqlite.num_rows > 0)
            {
                return true;
            }
            return false;
        }

        int hitungInfeed = 0;
        int hitungOutfeed = 0;
        int countDataFail = 0;
        List<bool> dataStatus = new List<bool>();

        public void tambahDataBaru(string data, int func)
        {
            try
            {
                if (bl.InvokeRequired)
                {
                    bl.Invoke(new Action<string, int>(tambahDataBaru), new object[] { data, func });
                    return;
                }
                data = data.Replace("\r", "");
                data = data.Replace("\n", "");
                data = data.Replace("]d2", "");
                if (data.Length > 0)
                {
                    if (func == 3)
                    {
                        List<string> dataMas = dataMass(data);
                        for (int i = 0; i < dataMas.Count; i++)
                        {
                            string[] temp = new string[2];
                            log("Process data :" + dataMas[i]);
                            if (!data.Equals("0"))
                            {
                                temp[0] = dataMas[i];
                                dataTerimaBaru.Add(temp);
                                Dictionary<string, string> field1;
                                field1 = new Dictionary<string, string>();
                                field1.Add("capid", dataMas[i]);
                                if (cekDataSample(dataMas[i]))
                                {
                                    field1.Add("flag", "1");
                                }
                                field1.Add("batchnumber", batch);
                                field1.Add("productModelId", productmodelid);
                                //field1.Add("expdate", exp);
                                field1.Add("createdtime", db.getUnixString());
                                field1.Add("isreject", "1");
                                field1.Add("linenumber", linenumber);
                                db.insert(field1, "vaccine");
                                hitungInfeed++;
                                dataStatus.Add(true);
                                string where1 = "capid ='" + dataMas[i] + "'";
                                sqlite.delete(where1, NAMA_TABLE_TEMP);
                                updatetableReveive(dataMas[i], 3, "0");
                            }
                            else
                            {
                                hitungInfeed++;
                                dataStatus.Add(false);
                                temp[0] = dataMas[i];
                                dataTerimaBaru.Add(temp);
                                countDataFail++;
                                log("Data Capid Fail");
                                updatetableReveive(dataMas[i], 3, "1");
                            }
                        }
                        //SetDataGridSend("1", "");
                    }
                    else
                    {
                        List<string> dataMas = dataMass(data);
                        for (int i = 0; i < dataMas.Count; i++)
                        {
                            Dictionary<string, string> field1;
                            field1 = new Dictionary<string, string>();
                            field1.Add("gsonevialid", dataMas[i]);
                            string stat = "0";
                            if (dataStatus[hitungOutfeed])
                            {
                                log("Receive Gsone:" + dataMas[i]);
                                dataTerimaBaru[hitungOutfeed][1] = dataMas[i];
                                if (dataMas[i].Equals("0"))
                                {
                                    log("Data GS1 Fail");
                                    field1.Add("isreject", "1");
                                    stat = "1";

                                }
                                db.update(field1, "Vaccine", "capid='" + dataTerimaBaru[hitungOutfeed][0] + "'");
                                string where = "gsone ='" + dataMas[i] + "'";
                                sqlite.delete(where, "tmp_gsOneVial");
                                updatetableReveive(dataMas[i], 4, stat);
                            }
                            else
                            {
                                updatetableReveive(dataMas[i], 4, "1");
                            }
                            //       SetDataGridSendgs("1", "");
                            hitungOutfeed++;
                            if (auditStarted)
                                cekKirimUlang();
                        }
                    }
                    //  kirimCekLaser();
                }
                else
                {
                    log("Data :" + data + " Not Recognized");
                }
            }
            catch (Exception ex)
            {
                db.simpanLog(ex.Message);
            }
        }

        string RemoveCharacter(string data)
        {
            data = data.Replace(" ", "");
            data = data.Replace("\0", "");
            data = data.Replace("\n", "");
            data = data.Replace("\t", "");
            return data;
        }

        int countFail=0;

        private void sendToserver(string dataGs1ID, string dataInfeedID, string status)
        {
            dataGs1ID = RemoveCharacter(dataGs1ID);

            if (CheckDataInfeed(dataInfeedID))
            {
                if (CheckDataInfeedVaccine(dataInfeedID))
                {
                    if (CheckDataInfeedInnerboxGsOne(dataInfeedID))
                    {
                        if (CheckDataVaccineFull(dataInfeedID))
                        {
                            if (CheckDataInfeedBatchNumber(dataInfeedID))
                            {
                                if (dataGs1ID.Equals("") || dataGs1ID.Length < 2)
                                {
                                    status = "7";
                                }
                                else
                                {
                                    Dictionary<string, string> field = new Dictionary<string, string>();
                                    field.Add("gsOneInnerBoxId", dataGs1ID);
                                    if (cekDataSample(dataGs1ID))
                                    {
                                        field.Add("flag", "1");
                                    }
                                    else
                                    {
                                        field.Add("flag", "0");
                                    }
                                    field.Add("createdtime", db.getUnixString());
                                    field.Add("isreject", status);
                                    if (status.Equals("1"))
                                    {
                                        field.Add("rejecttime", db.getUnixString());
                                    }
                                    db.update(field, "innerBox", "infeedInnerBoxId like '%" + dataInfeedID + "%' and batchnumber ='" + batch + "'");

                                    field = new Dictionary<string, string>();
                                    field.Add("innerBoxGsOneId", dataGs1ID);

                                    db.update(field, "vaccine", "innerBoxId like '%" + dataInfeedID + "%' and batchnumber ='" + batch + "'");
                                }
                            }
                            else
                            {
                                status = "4";
                            }
                        }
                        else
                        {
                            status = "5";
                        }
                    }
                    else
                    {
                        status = "6";
                    }
                }
                else
                {
                    status = "3";
                }
            }
            else
            {
                status = "2";
            }
            updateDataTable(dataGs1ID, dataInfeedID, status);
        }

        private bool CheckDataVaccineFull(string dataInfeedID)
        {
            List<string> field = new List<string>();
            field.Add("capid");
            string where = "innerBoxId ='" + dataInfeedID + "' ";
            List<string[]> ds = db.selectList(field, "vaccine", where);
            dataVaccine = new List<string>();
            if (db.num_rows == 10)
            {
                return true;
            }
            return false;
        }

        private bool CheckDataInfeedInnerboxGsOne(string dataInfeedID)
        {
            List<string> field = new List<string>();
            field.Add("innerBoxId");
            string where = "innerBoxId ='" + dataInfeedID + "' and innerBoxGsOneId is null";
            List<string[]> ds = db.selectList(field, "vaccine", where);
            dataVaccine = new List<string>();
            if (db.num_rows > 0)
            {
                return true;
            }
            return false;
        }

        private bool CheckDataInfeedBatchNumber(string dataInfeedID)
        {
            List<string> field = new List<string>();
            field.Add("innerBoxId");
            string where = "innerBoxId ='" + dataInfeedID + "' and batchnumber = '"+batch+"'";
            List<string[]> ds = db.selectList(field, "vaccine", where);
            dataVaccine = new List<string>();
            if (db.num_rows > 0)
            {
                return true;
            }
            return false;
        }

        private bool CheckDataInfeedVaccine(string dataInfeedID)
        {
            List<string> field = new List<string>();
            field.Add("innerBoxId");
            string where = "innerBoxId ='" + dataInfeedID + "' ";
            List<string[]> ds = db.selectList(field, "vaccine", where);
            dataVaccine = new List<string>();
            if (db.num_rows > 0)
            {
                return true;
            }
            return false;
        }

        private bool CheckDataInfeed(string dataInfeedID)
        {
            List<string> field = new List<string>();
            field.Add("infeedInnerBoxId");
            string where = "infeedInnerBoxId ='" + dataInfeedID+ "'";
            List<string[]> ds = db.selectList(field, "innerbox", where);
            dataVaccine = new List<string>();
            if (db.num_rows > 0)
            {
                return true;
            }
            return false;
        }
        int quantityReceive= 0;
        public void updateDataTable(string capid, string gsone, string status)
        {
            string dates = DateTime.Now.ToString("HH:mm:ss");
            DataRow row = tableProductReceive.NewRow();
            row[0] = capid;
            row[1] = gsone;
            row[2] = dates;
            row[3] = cekStatus(status);
            tableProductReceive.Rows.InsertAt(row, 0);
            bl.dgvReceive.DataSource = tableProductReceive;
            if(!firstPass)
            {
                if (status == "0")
                {
                    firstPass = true;
                    bl.dgvReceive.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                    bl.dgvReceive.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                    bl.dgvReceive.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                    bl.dgvReceive.Columns[3].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;

                    //datagrid has calculated it's widths so we can store them
                    for (int i = 0; i <= bl.dgvReceive.Columns.Count - 1; i++)
                    {
                        //store autosized widths
                        int colw = bl.dgvReceive.Columns[i].Width;
                        //remove autosizing
                        bl.dgvReceive.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
                        //set width to calculated by autosize
                        bl.dgvReceive.Columns[i].Width = colw;
                    }
                }
            }
            bl.dgvReceive.ClearSelection();
            bl.dgvReceive.Rows[0].Selected = true;

            quantityReceive++;
            bl.lblQuantityReceive.Text = "" + quantityReceive;
            bl.lblQuantityReceiveAll.Text = "" + allQty;
            if (bl.dgvReceive.Rows.Count == 100)
            {
                bl.dgvReceive.Rows.RemoveAt(100);
                tableProductReceive.Rows.RemoveAt(100);
            }
            bl.dgvReceive.FirstDisplayedScrollingRowIndex = 1;


            log("Data Infeed: " + gsone);
            log("Data Outfeed: " + capid);
            log("Status: " + cek(status));
            if (status.Equals("0"))
                log("Send To Server");
            log("-------------------------");
        }

        List<string> DataTempCapid = new List<string>();
        List<string> DataTempGSone = new List<string>();

        public void tambahDataBaru(string data)
        {
            try
            {
                if (bl.InvokeRequired)
                {
                    bl.Invoke(new Action<string>(tambahDataBaru), new object[] { data });
                    return;
                }
                //format 0,[outfeed]|
                //format 1,[infeed]|
                data = data.Replace("\r", "");
                data = data.Replace("\n", "");
                data = data.Replace("]d2", "");
                data = data.Replace("$", "");
                data = data.Replace("?", "");
                data = data.Replace(" ", "");
                char da = (char)29;
                data = data.Replace("" + da, "");
                if (data.Length > 0)
                {
                    List<string> dataMas = dataMass(data);
                    for (int i = 0; i < dataMas.Count; i++)
                    {
                        Dictionary<string, string> field = new Dictionary<string, string>();
                        string[] dataSplit = dataMas[i].Split(',');
                        if (dataSplit.Length == 3)
                        {
                            if (dataSplit[0].Equals("0"))
                            {
                                sendToserver(dataSplit[2], dataSplit[1], dataSplit[0]);
                            }
                            else if (dataSplit[0].Equals("1"))
                            {
                                updateDataTable(dataSplit[2], dataSplit[1], dataSplit[0]);
                                log("Data Infeed: " + dataSplit[1]);
                                log("Data Outfeed: " + dataSplit[2]);
                                log("Status: " + cek(dataSplit[0]));
                                log("Send To Server");
                                log("-------------------------");

                            }
                            else
                            {
                                updateDataTable(dataSplit[2], dataSplit[1], dataSplit[0]);
                                log("Data Infeed: " + dataSplit[1]);
                                log("Data Outfeed: " + dataSplit[2]);
                                log("Status: " + cek(dataSplit[0]));
                                log("Send To Server");
                                log("-------------------------");
                                log("Status not recognize");
                            }
                        }
                        else
                        {
                            log("Data: "+dataMas[i]+" Not recognize");
                        }
                        if (auditStarted)
                            cekKirimUlang();

                    }
                }
                else
                {
                    log("Data :" + data + " Not Recognized");
                }
            }
            catch (Exception ex)
            {
                log("Error :" + ex.Message);
            }
        }

        
        List<string[]> dataKawin = new List<string[]>();
        // masukan ke tabel recive()

        DataTable ConvertListToDataTable(List<string[]> list)
        {
            // Add rows. 
            tableProductReceive = new DataTable();
            tableProductReceive.Columns.Add("Capid");
            tableProductReceive.Columns.Add("GS1 Vial ID");
            tableProductReceive.Columns.Add("Timestamp");
            tableProductReceive.Columns.Add("Status");
            for (int i = list.Count; i > 0; i--)
            {
                tableProductReceive.Rows.Add(list[i - 1]);
            }

            return tableProductReceive;
        }

        public void updatetableReveive(string data, int func, string status)
        {
            if (func == 3)
            {
                string dates = DateTime.Now.ToString("HH:mm:ss");
                string[] dataTemp = new string[4];
                dataTemp[0] = data;
                dataTemp[2] = dates;
                //      dataTemp[3] = cek(status);
                dataKawin.Add(dataTemp);
            }
            else
            {
                dataKawin[hitungOutfeed][1] = data;
                dataKawin[hitungOutfeed][3] = cek(status);
            }

            bl.dgvReceive.DataSource = ConvertListToDataTable(dataKawin);
        }

        public void updatetableReveive(string[] hasilSplit)
        {
            string dates = DateTime.Now.ToString("HH:mm:ss");
            DataRow row = tableProductReceive.NewRow();
            string productinside = "";
            row[0] = hasilSplit[1];
            row[1] = hasilSplit[2];
            row[2] = dates;
            row[3] = cek(hasilSplit[0]);
            tableProductReceive.Rows.InsertAt(row, 0);
        }
        // cek banyak yang belum dikirim
        public int cekDataNotsend()
        {
            List<string> field = new List<string>();
            field.Add("count(*)");
            string where = "isused = '0'";
            List<string[]> ds = sqlite.select(field, NAMA_TABLE_TEMP, where);
            if (sqlite.num_rows > 0)
            {
                return int.Parse(ds[0][0]);
            }
            else
            {
                return 0;
            }
        }
        //Check Untuk generate ulang
        public void cekgenerateUlang()
        {
           // int setengahgenerate = qtyGenerateCode / 2;
            if (cekDataNotsend() < qtyGenerateCode)
            {
                generateAgain();
                tempTerima = 0;
            }
        }


        public void cekKirimUlang()
        {
            if (tempTerimaKirimUlang >= int.Parse(sqlite.config["qtysend"]))
            {
                SendToPrinter(getDataSendToPrinter(int.Parse(sqlite.config["qtysend"])));
                tempTerimaKirimUlang = 1;
                SetDataGridSend("0", "");
                DeleteUsedcode();
            }
            else
            {
                tempTerimaKirimUlang++;
            }
        }

        private void DeleteUsedcode()
        {
            sqlite.delete("isused='1'", NAMA_TABLE_TEMP);
        }
        DataTable tableProductFail = new DataTable();

        private void tampilDataFail()
        {
            try
            {
                //get data capid
                List<string> field = new List<string>();
                field.Add("gsone");
                string where = "isused ='1' and flag ='0' and count = '" + countRepeatDelete + "'";
                List<string[]> ds = sqlite.select(field, "tmp_gsOneVial", where);

                //tampil ke dgvFail
                tableProductFail = new DataTable();
                tableProductFail.Columns.Add("GS1 ID");
                for (int i = 0; i < ds.Count; i++)
                {
                    DataRow row = tableProductFail.NewRow();
                    row[0] = ds[i][0];
                    tableProductFail.Rows.Add(row);

                    Dictionary<string, string> field1;
                    field1 = new Dictionary<string, string>();
                    field1.Add("gsOneInnerBoxId", ds[i][0]);
                    field1.Add("isReject", "1");
                    field1.Add("createdtime", db.getUnixString());
                    db.insert(field1, "innerBox");
                }
                countRepeatDelete++;
            }
            catch (Exception ex)
            {
                db.simpanLog(ex.Message);
            }
        }


        // ambil data yang mau dikirim
        public List<string[]> getDataSendToPrinter(int count)
        {
            List<string> field = new List<string>();
            field.Add("gtin,batchnumber,expire,sn,gsone,manufacturingDate,expireDate,mfgDate");
            string where = "isused = '0' order by gsone asc limit " + count;
            List<string[]> ds = sqlite.select(field, NAMA_TABLE_TEMP, where);
            return ds;
        }

        // untuk mengirim ke printer dengan parameter
        int countRepeatSend = 0;
        int countRepeatDelete = 0;
        public void SendToPrinter(List<string[]> data)
        {
            Code.kirimPrinter(data);
            Dictionary<string, string> field = new Dictionary<string, string>();
            field.Add("isused", "1");
            log("Send To Printer : " + data.Count+" data");
            sqlite.BeginDelete(data);
            //foreach (string[] da in data)
            //{
            //   // log("Send To Printer : " + da[4]);
            //    sqlite.update(field, NAMA_TABLE_TEMP, "gsone = '" + da[4] + "'");
            //}
            string[] datapack = db.dataPO(batch);
            bl.labelBatch.Text = batch;
            bl.labelProduct.Text = datapack[3];
            getdatasend();
            templastcountkirim = templastcountkirim + data.Count;
            db.updatesequence(batch, "" + templastcountkirim);
            countRepeatSend++;
            cekgenerateUlang();
        }

        // ambil data yang sudah di kirim 
        private void getdatasend()
        {
            dataSendToPrinter = new List<string>();
            List<string> field = new List<string>();
            field.Add("gsone");
            string where = "isused = 1";
            List<string[]> ds = sqlite.select(field, NAMA_TABLE_TEMP, where);

            foreach (string[] da in ds)
            {
                dataSendToPrinter.Add(da[0]);
            }
        }

        //untuk generate code
        private void generateCodeSend(int qty)
        {
            //cek lastcount
            db.eventname = "Generate Code:" + qty;
            db.systemlog();
            //Code.getLastcount();
            //set batch
            Code.batch = batch;
            //buat code
            Code.generateProduct(qty);
        }

        //stop proses
        public void Stop()
        {
            try
            {
                auditStarted = false;
                start = false;
                //bl.cmbBatch.Enabled = true;
                bl.txtBatchNumber.Enabled = true;
                enable();
                //cmb();
                sqlite.delete("", NAMA_TABLE_TEMP);
                sqlite.delete("", "result");
                delayTimer.Stop();
                delaydevice.Stop();
                move("infeed");
                db.eventname = "Finish Print";
                db.systemlog();
                sqlite.updatestatusconfig("");
                db.addVaccine(batch, linenumber, productmodelid, rs);
            }
            catch (NullReferenceException nf)
            {

            }
        }

        // hapus data datagridview receive
        public void clearDataReceive()
        {
            tableProductReceive = new DataTable();
            tableProductReceive.Columns.Add("Outfeed ID");
            tableProductReceive.Columns.Add("Infeed ID");
            tableProductReceive.Columns.Add("Timestamp");
            tableProductReceive.Columns.Add("Status");
            bl.dgvReceive.DataSource = tableProductReceive;
            bl.lblQuantityReceive.Text = "0";
        }

        // hapus data datagridview Send
        public void clearDataSend()
        {
            sqlite.delete("", NAMA_TABLE_TEMP);
        }

        // mengecek batchnumber masih aktif atau tidak
        public bool cekBatchNumber(string batch)
        {
            List<string> field = new List<string>();
            field.Add("batchnumber");
            string where = "batchnumber ='" + batch + "' and status ='1'";
            List<string[]> ds = db.selectList(field, "packaging_order", where);
            if (db.num_rows > 0)
                return true;
            else
                return false;
        }

        // memulai audit
        public void startAudit()
        {
            db.eventname = "Start precheck " + batch;
            db.systemlog();
            setIdAudit();
        }

        //memulai printing dengan sebelumnya memulai audit
        public void startPrinting()
        {
            db.eventname = "Start Printing";
            db.systemlog();
            dataPackagingOrder = db.dataPO(batch);
            //getVaccine();
            generateCodeSend(qtyGenerateCode);
            SetDataGridSend("0", "");
        }

        bool firstPass;

        public void sendStartPrinter()
        {
            firstPass = false;
            auditStarted = true;
            db.eventname = "Start printing " + batch + " Outfeed Station";
            db.systemlog();
            tempTerima = 1;
            string[] datapack = db.dataPO(batch);
            bl.labelBatch.Text = batch;
            bl.labelProduct.Text = datapack[3];
            sqlite.delete("", NAMA_TABLE_TEMP);
            Code.lastCount = db.getcountProduct(batch) + 1;
            templastcountkirim = Code.lastCount - 1;
            generateCodeSend(int.Parse(sqlite.config["qtysend"]) * 2);
            SendToPrinter(getDataSendToPrinter(int.Parse(sqlite.config["qtysend"])));
            SendToPrinter(getDataSendToPrinter(int.Parse(sqlite.config["qtysend"])));
        }

        // unused
        public void setIdAudit()
        {
            db.eventname = "Start Printing " + batch;
            db.systemlog();

            sqlite.delete("", NAMA_TABLE_TEMP);
            Code.lastCount = db.getcountProduct(batch) + 1;
            templastcountkirim = Code.lastCount - 1;
            Code.audit = true;
            generateCodeSend(int.Parse(sqlite.config["qtygenerate"]));
            Code.audit = false;
            db.eventname = "Start Audit";
            db.systemlog();
            //SetDataGridSend("0", "" + qtyGenerateCode);
        }

        // setelah kirim printer apabila kurang maka generate ulang
        public void generateAgain()
        {
            //  Code.lastCount = db.getcountProduct(batch) + 1;
            generateCodeSend(qtyGenerateCode);
        }

        // unused
        public void getVaccine()
        {
            List<string> field = new List<string>();
            field.Add("capid");
            string where = "batchnumber ='" + batch + "'";
            List<string[]> ds = db.selectList(field, "vaccine", where);
            dataVaccine = new List<string>();
            if (db.num_rows > 0)
            {
                foreach (string[] da in ds)
                {
                    dataVaccine.Add(da[0]);
                }
            }
            else
            {

            }

        }

        //unused
        public void setIdstartprint()
        {

        }

        // untuk tampil data di grid send
        public void SetDataGridSend(string status, string banyak)
        {
            List<string> field = new List<string>();
            field.Add("gsone 'GS1 ID Outfeed'");
            string limit = "";
            if (banyak.Length > 0)
                limit = "limit " + banyak;
            List<string[]> ds = sqlite.select(field, NAMA_TABLE_TEMP, "isused = '" + status + "' " + limit);
            if (sqlite.num_rows > 0)
            {
                tableProductSend = sqlite.dataHeader;
                bl.dgvSend.DataSource = tableProductSend;
                foreach (string[] Row in ds)
                {
                    DataRow row = tableProductSend.NewRow();
                    row[0] = Row[0];
                    tableProductSend.Rows.Add(row);
                }
            }else
                tableProductSend = sqlite.dataHeader;

            bl.dgvSend.DataSource = tableProductSend;
        }

        //update data sample
        public void sampleData(string blisterid)
        {
            Dictionary<string, string> field = new Dictionary<string, string>();
            //status sample
            field.Add("isused", "4");
            string where = NAMA_FIELD + "='" + blisterid + "'";
            sqlite.update(field, NAMA_TABLE_TEMP, where);
        }

        // untuk mengirim ke database server
        public void delay()
        {
            startDelay = true;
            delayTimer = new System.Timers.Timer();
            int delaySql = int.Parse(sqlite.config["delay"]);
            delaySql = delaySql * 60000;
            delayTimer.Interval = delaySql;
            delayTimer.Elapsed += new System.Timers.ElapsedEventHandler(delayTimer_Elapsed);
            delayTimer.Start();
        }

        // yang dilakukan pada saat delay
        void delayTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            db.addVaccine(batch, linenumber, productmodelid, rs);
        }

        public void CekDeviceDelay()
        {
            delaydevice = new System.Timers.Timer();
            delaydevice.Interval = 10000;
            delaydevice.Elapsed += new System.Timers.ElapsedEventHandler(delaydevice_Elapsed);
            delaydevice.Start();
        }

        void delaydevice_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            CekDevice();
        }

        public void CekDevice()
        {
            if (PingHost(sqlite.config["ipprinter"]))
                bl.lblPrinter.Text = "Online";
            else
                bl.lblPrinter.Text = "Offline";

            if (PingHost(sqlite.config["ipCamera"]))
                bl.lblCamera.Text = "Online";
            else
                bl.lblCamera.Text = "Offline";


            if (PingHost(sqlite.config["ipCamera2"]))
                bl.lblCamera2.Text = "Online";
            else
                bl.lblCamera2.Text = "Offline";


            if (PingHost(sqlite.config["ipCamera3"]))
                bl.lblCamera3.Text = "Online";
            else
                bl.lblCamera3.Text = "Offline";


            if (PingHost(sqlite.config["ipCamera4"]))
                bl.lblCamera4.Text = "Online";
            else
                bl.lblCamera4.Text = "Offline";

        }

        //untuk ngeping ke ip parameter
        public bool PingHost(string nameOrAddress)
        {
            if (nameOrAddress.Length == 0)
                return false;
            bool pingable = false;
            Ping pinger = new Ping();
            try
            {
                PingReply reply = pinger.Send(nameOrAddress);
                pingable = reply.Status == IPStatus.Success;
            }
            catch (PingException)
            {
                // Discard PingExceptions and return false;
            }
            return pingable;
        }


        //void backgroundwo()
        //{
        //    worker.DoWork += new DoWorkEventHandler(worker_DoWork);
        //    worker.WorkerReportsProgress = true;
        //    worker.RunWorkerAsync();
        //    resetEvent.WaitOne();
        //}

        //void worker_DoWork(object sender, DoWorkEventArgs e)
        //{
        //    CekDevice();
        //    resetEvent.Set();
        //}

        void setlog(string[] hasilsplit)
        {
            log("----------------------------------------");
            log("Outfeed ID : " + hasilsplit[1]);
            log("Infeed ID  : " + hasilsplit[2]);
            log("Status     : " + cek(hasilsplit[0]));
            log("----------------------------------------");
        }

        internal void test()
        {
            db.addVaccine(batch, sqlite.config["lineNumber"], productmodelid, rs);

        }

        internal int cekKamera(string Message)
        {
            Message = Message.Replace("\r", "");
            Message = Message.Replace("\n", "");
            // log("Receive Data : " + Message);
            List<string> field = new List<string>();
            field.Add("*");
            string where = "gsone = '" + Message + "'";
            sqlite.select(field, "tmp_gsOneVial", where);
            if (sqlite.num_rows > 0)
            {
                return 0;
            }
            else
            {
                return 1;
            }
        }

        public void clearBuffer()
        {
            print pr = new print();
            pr.cekKoneksi();
            pr.clearBuffer();
            srv.clearCache();
        }

        internal bool checkPLCConnected()
        {
            return sendData_plc.CheckConnected();
        }

        //tambahan
        public int[] getCount(string btch)
        {
            int[] cnt = new int[1];
            allQty = db.selectCountPass(btch);
            //qtyFail = db.selectCountReject(btch);
            //quantityReceive = qtyGood + qtyFail;
            //cnt[0] = qtyGood;
            //cnt[1] = qtyFail;
            cnt[0] = allQty;
            return cnt;
        }
    }
}
