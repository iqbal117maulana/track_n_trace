﻿using System;
using System.Collections.Generic;
using System.Linq;
using Finisar.SQLite;
using System.Text;
using System.Data;
using System.IO;
using System.Windows.Forms;

namespace Outfeed_Station
{
    public class result
    {
        private SQLiteConnection sql_con;
        private SQLiteCommand sql_cmd;
        private SQLiteDataAdapter DB;
        private SQLiteDataReader datareader;
        public DataTable dataHeader;
        private DataSet DS = new DataSet();
        private DataTable DT = new DataTable();
        public int num_rows = 0;
        public List<string> dataInsert = new List<string>();
        public List<string> dataUpdate = new List<string>();
        public Dictionary<string, string> config = new Dictionary<string, string>();

        private void SetConnection()
        {
            sql_con = new SQLiteConnection
                ("Data Source=result.db;Version=3;New=False;Compress=True;");
        }

        public result()
        {
            SetConnection();
         }

        public void ExecuteQuery(string txtQuery)
        {
            try
            {
                SetConnection();
                sql_con.Open();
                sql_cmd = sql_con.CreateCommand();
                sql_cmd.CommandText = txtQuery;
                sql_cmd.ExecuteNonQuery();
                sql_con.Close();
            }
            catch (SQLiteException sq)
            {
                simpanLog("Error : " + sq.ToString());
              //  new Confirm("Error SQLLite : " + sq.ToString(), "Error");
            }
            finally
            {
                sql_con.Close();
            }
        }

        public void insert(Dictionary<string, string> field, string table)
        {
            string sql = "INSERT INTO " + table + " (";
            int i = 0;
            foreach (string key in field.Keys)
            {
                sql += "'" + key + "'";
                if (i + 1 < field.Count)
                {
                    sql += ",";
                    i++;
                }
            }

            sql += ") values (";

            i = 0;
            foreach (string key in field.Values)
            {
                sql += "'" + key + "'";
                if (i + 1 < field.Count)
                {
                    sql += ",";
                    i++;
                }
            }
            sql += ")";
            simpanLog(sql);
            ExecuteQuery(sql);
        }

        public void simpanLog(String line)
        {
            DateTime now = DateTime.Now;
            string tahun = now.ToString("yyyy");
            string bulan = now.ToString("MM");
            string hari = now.ToString("dd");
            string dates = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            line = dates + "\t" + line;
            // cek file 
            bool cekfile = false;
            string namaFolder = "eventlog\\Result";
            try
            {
                while (!cekfile)
                {
                    if (Directory.Exists(namaFolder))
                    {
                        if (Directory.Exists(namaFolder + @"\" + tahun))
                        {
                            if (Directory.Exists(namaFolder + @"\" + tahun + @"\" + bulan))
                            {

                                using (StreamWriter outputFile = new StreamWriter(namaFolder + @"\" + tahun + @"\" + bulan + @"\" + hari + ".txt", true))
                                {
                                    outputFile.WriteLine(line);
                                }
                                cekfile = true;
                            }
                            else
                            {

                                Directory.CreateDirectory(namaFolder + @"\" + tahun + @"\" + bulan);
                            }
                        }
                        else
                        {
                            Directory.CreateDirectory(namaFolder + @"\" + tahun);
                        }
                    }
                    else
                    {

                        Directory.CreateDirectory(namaFolder);
                    }
                }
            }
            catch (Exception ex)
            {
                //simpanLog(ex.Message);
            }
        }

        public void update(Dictionary<string, string> field, string table, string where)
        {

            string sql = "UPDATE " + table + " SET";
            int i = 0;
            foreach (KeyValuePair<string, string> key in field)
            {
                sql += "'" + key.Key + "' = " + "'" + key.Value + "'";
                if (i + 1 < field.Count)
                {
                    sql += ",";
                    i++;
                }
            }
            if (where.Length > 0)
                sql += " WHERE " + where + "";
            simpanLog(sql);
            ExecuteQuery(sql);
        }

        public void delete(string where, string table)
        {
            string sql = "DELETE FROM " + table;

            if (where.Length > 0)
                sql += " WHERE " + where;

            simpanLog(sql);
            ExecuteQuery(sql);
        }

        public List<string[]> select(List<string> field, string table, string where)
        {
            try
            {
                string sql = "SELECT ";
                for (int j = 0; j < field.Count; j++)
                {
                    sql += field[j];
                    if (j + 1 < field.Count)
                    {
                        sql += ",";
                        j++;
                    }
                }
                sql += " From " + table;

                if (where.Length > 0)
                    sql += " WHERE " + where;

                int num = 0;
                simpanLog(sql);
                sql_con.Open();
                sql_cmd = new SQLiteCommand(sql, sql_con);
                datareader = sql_cmd.ExecuteReader();
                List<String[]> Results = new List<String[]>();

                while (datareader.Read())
                {
                    string[] data = new string[datareader.FieldCount];
                    for (int u = 0; u < datareader.FieldCount; u++)
                    {
                        data[u] = datareader.GetValue(u).ToString();
                    }

                    Results.Add(data);
                    num++;
                }
                num_rows = num;
                dataHeader = new DataTable();
                for (int i = 0; i < datareader.FieldCount; i++)
                {
                    dataHeader.Columns.Add(datareader.GetName(i));
                }
                sql_con.Close();
                return Results;
            }
            catch (InvalidOperationException IO)
            {
                num_rows = 0;
                MessageBox.Show(IO.Message);
                return null;
            }
            catch (Exception ex)
            {

                return select(field, table, where);
            }
            finally
            {
                sql_con.Close();
            }
        }

        public void insertData(Dictionary<string, string> field, string table)
        {
            string sql = "INSERT INTO " + table + " (";
            int i = 0;
            foreach (string key in field.Keys)
            {
                sql += "'" + key + "'";
                if (i + 1 < field.Count)
                {
                    sql += ",";
                    i++;
                }
            }

            sql += ") values (";

            i = 0;
            foreach (string key in field.Values)
            {
                sql += "'" + key + "'";
                if (i + 1 < field.Count)
                {
                    sql += ",";
                    i++;
                }
            }
            sql += ");";
            dataInsert.Add(sql);
        }

        public void updateData(Dictionary<string, string> field, string table, string where)
        {

            string sql = "UPDATE " + table + " SET";
            int i = 0;
            foreach (KeyValuePair<string, string> key in field)
            {
                sql += "'" + key.Key + "' = " + "'" + key.Value + "'";
                if (i + 1 < field.Count)
                {
                    sql += ",";
                    i++;
                }
            }
            if (where.Length > 0)
                sql += " WHERE  " + where + ";";
            dataUpdate.Add(sql);
        }

        public void Begin()
        {
            string sql = "BEGIN TRANSACTION;\n ";

            for (int i = 0; i < dataInsert.Count; i++)
            {
                sql += dataInsert[i] + "\n";
            }
            sql += "COMMIT;";
            // simpanLog(sql);
            ExecuteQuery(sql);
            dataInsert = new List<string>();
        }

        public void BeginUpdate()
        {
            string sql = "BEGIN TRANSACTION;\n ";

            for (int i = 0; i < dataUpdate.Count; i++)
            {
                sql += dataUpdate[i] + "\n";
            }
            sql += "COMMIT;";
            simpanLog(sql);
            ExecuteQuery(sql);
            dataUpdate = new List<string>();
        }
       
    }
}
