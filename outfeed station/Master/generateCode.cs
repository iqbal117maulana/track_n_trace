﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;

namespace Outfeed_Station
{
    
    public class generateCode
    {

        public const int QTY_KIRIM = 5;
        public const int QTY_MAKS_KIRIM_PRODUCT = 10;

        public int PackagingOrder;
        public int postCodeProduct;
        public int tempBanyak;
        public string batch;
        public string gtin;
        public string exp;
        sqlitecs sql;
        public int tambahProduct = 1;
        public List<string> dataProduct;
        public bool masih;

        public int penomoran;
        public int penomorangs;
        public int lastCount;
        public bool audit;
        public generateCode()
        {
            sql = new sqlitecs();
            dataProduct = new List<string>();
        }

        //public List<string[]> generateProduct()
        //{
        //    cekTemp();
        //    string[] temp;
        //    List<string[]> tempkirim = new List<string[]>();
        //    for (int i = 0; i < tempBanyak; i++)
        //    {
        //        temp = new string[4];
        //        Dictionary<string, string> field = new Dictionary<string, string>();
        //        field.Add("batchnumber", batch);
        //        field.Add("gtin", gtin);
        //        field.Add("sn", tempBanyak.ToString().PadLeft(5, '0'));
        //        field.Add("expire", exp);
        //        temp[0] = gtin;
        //        temp[1] = batch;
        //        temp[2] = exp;
        //        temp[3] = tempBanyak.ToString().PadLeft(5, '0');
        //        string gesonedata = gsone(gtin, batch, exp, "" + p);
        //        field.Add("gsone", gesonedata);
        //        field.Add("isused", "0");
        //        sql.insertData(field, Master.NAMA_TABLE_TEMP);
        //        tempBanyak++;
        //        tempkirim.Add(temp);
        //    }
        //    sql.Begin();
        //    return tempkirim;
        //}
        int hitung = 0;
        public void kirimPrinter(List<string[]> data)
        {
            print pr = new print();
            pr.cekKoneksi();
            hitung = pr.kirimData(data, hitung);
            if (hitung > 90)
            {
                hitung = 0;
            }
        }

        public void cekTemp()
        {
            int qtygenerate = 0;
            if (postCodeProduct == 0)
                qtygenerate = QTY_KIRIM * 2;
            else
                qtygenerate = QTY_KIRIM;
            int temp = PackagingOrder - postCodeProduct;
            if (temp > qtygenerate)
            {
                tempBanyak = qtygenerate;
                postCodeProduct = postCodeProduct + tempBanyak;
            }
            else
            {
                if (temp <= qtygenerate)
                {
                    tempBanyak = temp;
                    masih = true;
                }
                else
                {
                    tempBanyak = qtygenerate;
                    postCodeProduct = postCodeProduct + tempBanyak;
                }
            }
        }

        public List<string[]> genarateProductAudit(int banyak)
        {
            cekTemp();
            string[] temp;
            List<string[]> tempkirim = new List<string[]>();
            for (int i = 0; i < banyak; i++)
            {
                temp = new string[4];
                Dictionary<string, string> field = new Dictionary<string, string>();
                field.Add("batchnumber", batch);
                field.Add("gtin", gtin);
                field.Add("sn", penomoran.ToString().PadLeft(5, '0'));
                field.Add("expire", exp);
                temp[0] = gtin;
                temp[1] = batch;
                temp[2] = exp;
                temp[3] = tambahProduct.ToString().PadLeft(5, '0');
                string gesonedata = gsone(gtin, batch, exp, "" + penomoran);
                field.Add("gsone", gesonedata);
                field.Add("isused", "0");
                sql.insertData(field, "tmp_Outfeed");
                penomoran++;
                tempkirim.Add(temp);
            }
            sql.Begin();
            return tempkirim;
        }
         
        public string gsone(string gtin, string batch, string exp, string sn)
        {
            string data = "01" + gtin + "10" + batch + "17" + exp + "21" + sn.ToString().PadLeft(6, '0');
            return data;
        }

        public void genarateProductAuditNew(int banyak)
        {
            dbaccess db = new dbaccess();
            if (tambahProduct == 1)
            {
                string[] data = db.dataPO(batch,Master.AGGREGATION);
                string gtin = data[0];
                string exp = data[1];
                for (int i = 0; i < banyak; i++)
                {
                    int j = i + 1;
                    Dictionary<string, string> field = new Dictionary<string, string>();
                    field.Add("batchnumber", batch);
                    field.Add("gtin", gtin);
                    field.Add("sn", j.ToString().PadLeft(5, '0'));
                    field.Add("expire", exp);
                    tambahProduct++;
                    string gs = gsone(gtin, batch, exp, "" + j);
                    field.Add("gsone", gs);
                    field.Add("isused", "0");
                    sql.insertData(field, Master.NAMA_TABLE_TEMP);
                }
                sql.Begin();
            }
        }

        public void generateProductAuditContinue(int banyak)
        {
            dbaccess db = new dbaccess();
            string[] data = db.dataPO(batch);
            string gtin = data[0];
            string exp = data[1];
            for (int i = 0; i < banyak; i++)
            {
                int j = i + 1;
                Dictionary<string, string> field = new Dictionary<string, string>();
                field.Add("batchnumber", batch);
                field.Add("gtin", gtin);
                field.Add("sn", tambahProduct.ToString().PadLeft(5, '0'));
                field.Add("expire", exp);
                string gs = gsone(gtin, batch, exp, "" + tambahProduct);
                field.Add("gsone", gs);
                field.Add("isused", "0");
                sql.insertData(field, Master.NAMA_TABLE_TEMP);
                tambahProduct++;
            }
            sql.Begin();
        }

        public void generateProduct(int banyak)
        {
            dbaccess db = new dbaccess();
            string[] data = db.dataPO(batch);
            string gtin = data[0];
            string exp = data[1];
            string manu = data[5];

            DateTime dateExpiry = DateTime.ParseExact(exp, "yyMMdd", CultureInfo.InvariantCulture);
            DateTime dateManu = DateTime.ParseExact(manu, "yyMMdd", CultureInfo.InvariantCulture);

            string expiryDate = dateExpiry.ToString("MMM yy").ToUpper();
            string mfgDate = dateManu.ToString("MMM dd, yy").ToUpper();
            for (int i = 0; i < banyak; i++)
            {
                int j = i + 1;
                Dictionary<string, string> field = new Dictionary<string, string>();
                field.Add("batchnumber", batch);
                field.Add("gtin", gtin);
                field.Add("manufacturingDate", manu);
                string guid = Guid.NewGuid().ToString().Replace("-", "").Substring(0, 12).ToUpper();
                //field.Add("sn", tambahProduct.ToString().PadLeft(6, '0'));
                field.Add("expireDate", expiryDate);
                field.Add("mfgDate", mfgDate);
                //string gs = gsone(gtin, batch, exp, "" + tambahProduct.ToString().PadLeft(6, '0'));
                string gs = gsone(gtin, batch, exp, guid);
                field.Add("sn", guid);
                field.Add("expire", exp);
                if (audit)
                {
                    field.Add("flag", "1");
                }
                else
                {
                    field.Add("flag", "0");
                }
                tambahProduct++;
                field.Add("gsone", gs);
                field.Add("isused", "0");
                sql.insertData(field, "tmp_gsOneVial");
            }
            sql.Begin();
        }

        public List<string[]> generateProduct()
        {
            string[] temp;
            cekTemp();
            List<string[]> tempkirim = new List<string[]>();
            for (int i = 0; i < tempBanyak; i++)
            {
                temp = new string[4];
                Dictionary<string, string> field = new Dictionary<string, string>();
                field.Add("batchnumber", batch);
                field.Add("gtin", gtin);
                field.Add("sn", penomorangs.ToString().PadLeft(5, '0'));
                field.Add("expire", exp);
                temp[0] = gtin;
                temp[1] = batch;
                temp[2] = exp;
                temp[3] = penomorangs.ToString().PadLeft(5, '0');
                string gesonedata = gsone(gtin, batch, exp, "" + penomorangs);
                field.Add("gsone", gesonedata);
                field.Add("isused", "0");
                sql.insertData(field, "tmp_gsOneVial");
                tambahProduct++;
                penomorangs++;
                tempkirim.Add(temp);
            }
            sql.Begin();
            return tempkirim;
        }

    }
}
