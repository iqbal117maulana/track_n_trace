﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Windows.Forms;
using System.Threading;
using System.Net.NetworkInformation;

namespace Outfeed_Station
{
    class Master
    {
        public const string NUMBER_CODE = "04";
        public const string NAMA_FIELD = "gsOneInnerBoxId";
        public const string NAMA_TABLE_TEMP = "tmp_Outfeed";
        public const string NAMA_TABLE_UTAMA = "innerBox";
        public const string AGGREGATION = "innerbox";
        public const string NAMA_STATION = "OUTFEED";
        public const string STATUS_PO = "1";
        List<string[]> tempkirim = new List<string[]>();
        List<string> tempGsone = new List<string>();

        public string productmodelid;
        public string batch;
        public string adminid;
        public sqlitecs sqlite;
        public dbaccess db;
        public Outfeed bl;
        public generateCode Code;
        public bool onprogress;
        public List<string[]> datacombo;
        List<string[]> dataProductTrial;
        int post;
        int quantySend = 0;
        int quantyReceive = 0;
        int quantyProduct=0;
        int tempTerima=1;
        int tempTerimaTrial=0;
        bool audit;
        public bool start;
        DataTable tableProductSend = new DataTable();
        DataTable tableProductReceive = new DataTable();
        private Thread serverThread;
        public System.Timers.Timer delayTimer;
        System.Timers.Timer delayTimerDevice;

        public string linenumber;
        public string linename;

        Log lg = new Log();

        public void DelayDevice()
        {
            delayTimerDevice = new System.Timers.Timer();
            delayTimerDevice.Interval = 10000;
            delayTimerDevice.Elapsed += new System.Timers.ElapsedEventHandler(delayTimerDevice_Elapsed);
            delayTimerDevice.Start();
        }

        void delayTimerDevice_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            cekDevice("");
            //if(!start)
            //cmb();
        }

        public void cekDevice(string data)
        {
            if (bl.InvokeRequired)
            {
                bl.Invoke(new Action<string>(cekDevice), new object[] { data });
                return;
            }
            string online = "Online";
            string Offline = "Offline";
            //if (PingHost(sqlite.config["ipCamera"]))
            //    bl.lblCamera.Text = online;
            //else
            //    bl.lblCamera.Text = Offline;

            //if (PingHost(sqlite.config["ipCamera2"]))
            //    bl.lblCamera2.Text = online;
            //else
            //    bl.lblCamera2.Text = Offline;

            //if (PingHost(sqlite.config["ipCamera3"]))
            //    bl.lblCamera3.Text = online;
            //else
            //    bl.lblCamera3.Text = Offline;


            //if (PingHost(sqlite.config["ipCamera4"]))
            //    bl.lblCamera4.Text = online;
            //else
            //    bl.lblCamera4.Text = Offline;


            //if (PingHost(sqlite.config["ipprinter"]))
            //    bl.lblPrinter.Text = online;
            //else
            //    bl.lblPrinter.Text = Offline;
        }

        public void delay()
        {
            delayTimer = new System.Timers.Timer();
            delayTimer.Interval = 60000;
            delayTimer.Elapsed += delayTimer_Elapsed;
            delayTimer.Start();
        }

        public void delayTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
           // db.addVaccine(batch, sqlite.config["lineNumber"], productmodelid, rs);
        }

        private void kirimPrinterGsone(int banyak)
        {
            print pr = new print();
            pr.cekKoneksi();
            List<string[]> tempdetail = new List<string[]>(); ;
            for (int i = 0; i < banyak; i++)
            {
                tempdetail.Add(dataProductTrial[i]);
                //log("Vial Gs1 ID : " + tempvialdetail[i] + " Sent To Printer");
                post++;
            }
           // pr.kirimData(tempdetail);
        }

        public void updateStatusProduct(string[] Product)
        {
            Dictionary<string, string> field = new Dictionary<string, string>();
            field.Add("isused", "1");
            string where;
            for (int i = 0; i < Product.Length; i++)
            {
                where = "gsone = '" + Product[i] + "'";
                sqlite.updateData(field, NAMA_TABLE_TEMP, where);
            }
            sqlite.BeginUpdate();
        }

        public void finish()
        {
            Dictionary<string, string> field = new Dictionary<string, string>();
            field.Add("status", "5");
            string where = "batchnumber = '" + batch + "'";
            db.update(field, "packaging_order", where);
            start = false;
            //bl.cmbBatch.Enabled = true;
            bl.txtBatchNumber.Enabled = true;
            enable();
            //cmb();
            sqlite.delete("", NAMA_TABLE_TEMP);
            sqlite.updatestatusconfig("");
            sqlite.delete("", "result");
            delayTimer.Stop();
            //move("infeed");
            db.eventname = "Finish Print";
            db.systemlog();
            sqlite.updatestatusconfig("");
            tableProductReceive = new DataTable();
            tableProductReceive.Columns.Add("Gs1 ID");
            tableProductReceive.Columns.Add("Timestamp");
            tableProductReceive.Columns.Add("Status");
            bl.dgvReceive.DataSource = tableProductReceive;

        }

        public void tampilDataProduct(string limit, string status)
        {
            List<string> field = new List<string>();
            field.Add("gtin,batchNumber,expire,sn,gsone");
            if(limit.Length >0)
                limit = "limit "+limit;
            List<string[]> ds = sqlite.select(field, NAMA_TABLE_TEMP, status+" "+limit);
            if (sqlite.num_rows > 0)
            {
                quantySend = sqlite.num_rows;
                tableProductSend = new DataTable();
                tableProductSend.Columns.Add("GS1 ID");
                bl.dgvSend.DataSource = tableProductSend;
                int j = 0;
                dataProductTrial = new List<string[]>();

                foreach (string[] Row in ds)
                {
                    DataRow row = tableProductSend.NewRow();
                    row[0] = Row[4];
                    dataProductTrial.Add(Row);
                    tableProductSend.Rows.Add(row);
                    j++;
                }

                bl.dgvSend.DataSource = tableProductSend;
                bl.lblQtySend.Text = "" + bl.dgvSend.Rows.Count;
            }
        }

        public bool hapusGrid(string capid)
        {
            int rowIndex = -1;
            foreach (DataGridViewRow row in bl.dgvSend.Rows)
            {
                if (row.Cells[0].Value.ToString().Equals(capid))
                {
                    rowIndex = row.Index;
                    break;
                }
            }
            if (rowIndex > -1)
            {
                bl.dgvSend.Rows.RemoveAt(rowIndex);
                return true;
            }
            else
                return false;
        }

        public void sudahditerimaProduct(string product)
        {
            Dictionary<string, string> field = new Dictionary<string, string>();
            field.Add("isused", "2");
            string where = "gsone ='" + product + "'";
            sqlite.update(field, NAMA_TABLE_TEMP, where);
        }
        
        public void updateKirimPrinter(string [] product)
        {
            foreach (string data in product)
            {
                Dictionary<string, string> field = new Dictionary<string, string>();
                field.Add("isused", "1");
                string where = "gsone ='" + product + "'";
                sqlite.update(field, NAMA_TABLE_TEMP, where);
            }
        }

        public void kirimprinterLagi()
        {
            List<string[]> dataTempBlister = Code.generateProduct();
            tampilDataProduct("","isused ='0' or isused = '1'");
            Code.kirimPrinter(dataTempBlister);
        }

        public void MulaiServer()
        {
          //  server srv = new server(bl, sqlite.config["portserver"],this);
        }

        public string cek(string data)
        {
            if (data.Equals("0"))
                return "Pass";
            else
                return "Fail";
        }

        public void startprinting()
        {
            Confirm cf = new Confirm("Are you sure? ", "Confirmation");
            bool hasilDialog = cf.conf();
            if (hasilDialog)
            {
                delay();
                start = true;
                disable();
                string[] data = db.dataPO(batch, AGGREGATION);
                quantyProduct = int.Parse(data[2]);
                int aggre = int.Parse(data[4]);
                double qtybl = quantyProduct / aggre;
                int aggreInfeed = int.Parse(data[6]);
                qtybl = qtybl / aggreInfeed;
                Code.PackagingOrder = (int)Math.Ceiling(qtybl);
                Code.batch = batch;
                quantySend = quantySend + (int)Math.Ceiling(qtybl);
                bl.lblQtySend.Text = "" + quantySend;
                Code.PackagingOrder = (int)Math.Ceiling(qtybl);
                bl.labelBatch.Text = batch;
                bl.labelProduct.Text = data[3];
                productmodelid = data[5];
                if (!onprogress)
                {
                    sqlite.updatestatusconfig(batch);
                    int countOnprogress = int.Parse(sqlite.getCountProductProgress());
                    Code.tambahProduct = countOnprogress + 1;
                    List<string[]> dataOnProgress = sqlite.getProductProgress();
                    liststring(dataOnProgress);
                    List<string[]> dataTempBlister = Code.generateProduct();
                    liststring(dataTempBlister);
                    updateKirimPrinter(tempGsone.ToArray());
                    Code.kirimPrinter(tempkirim);
                    tampilDataProduct("", "isused ='0' or isused = '1'");
                }
                else
                {
                    //count yang sudah ada
                    int countOnprogress = int.Parse(sqlite.getCountProductProgress());
                    if (countOnprogress >= Code.PackagingOrder)
                        Code.masih = true;
                    Code.tambahProduct = countOnprogress + 1;
                    //count sisa
                    List<string[]> dataOnProgress = sqlite.getProductProgress();
                    //count yang sudah dikirim 
                    int countDoneprogress = int.Parse(sqlite.getCountOnProgress());
                    quantyReceive = countDoneprogress;
                    liststring(dataOnProgress);
                    int makskirim = generateCode.QTY_KIRIM*2;
                    List<string[]>dataTempBlister;
                    if (dataOnProgress.Count < makskirim)
                    {
                        Code.postCodeProduct = countOnprogress;
                        dataTempBlister = Code.generateProduct();
                        liststring(dataTempBlister);
                    }
                    updateKirimPrinter(tempGsone.ToArray());
                    Code.kirimPrinter(tempkirim);
                    tampilDataProduct("", "isused ='0' or isused = '1'");
                }
                db.eventname = "Start Print";
                db.systemlog();
                tempkirim = new List<string[]>();
                tempGsone = new List<string>();

            }
        }

        void liststring(List<string[]> data)
        {
            foreach (string[] dat in data)
            {
                tempkirim.Add(dat);
                tempGsone.Add(dat[4]);
            }
        }

        public void disable()
        {
            bl.btnStart.Text = "Stop";
            bl.btnAudit.Enabled = false;
            bl.btnDeco.Enabled = false;
            bl.btnLogout.Enabled = false;
            bl.btnConfig.Enabled = false;
        }

        public void enable()
        {
            bl.btnStart.Text = "Start";
            bl.btnAudit.Enabled = true;
            bl.btnDeco.Enabled = true;
            bl.btnLogout.Enabled = true;
            bl.btnConfig.Enabled = true;
            bl.btnStart.Enabled = false;
        }

        public void tambahData(string data)
        {
            if (bl.InvokeRequired)
            {
                bl.Invoke(new Action<string>(tambahData), new object[] { data });
                return;
            }
            if (start)
            {
                log("Receive Data:" + data);
                data = data.Replace("\r", "");
                data = data.Replace("\n", "");
                if (!data.Contains("?") && data.Length > 45)
                {
                    string dates = DateTime.Now.ToString("HH:mm:ss");
                    string status = data.Substring(0, 1);
                    string product = data.Substring(1, 45);
                    if (hapusGrid(product))
                    {
                        DataRow row = tableProductReceive.NewRow();
                        log(status);
                        row[0] = product;
                        row[1] = dates;
                        row[2] = cek(status);
                        tableProductReceive.Rows.Add(row);
                        Dictionary<string, string> field = new Dictionary<string, string>();
                        field.Add("gsone", product);
                        field.Add("createdtime", db.getUnixString());
                        field.Add("flag", status);
                        sqlite.insert(field, "result");
                        sudahditerimaProduct(product);
                        bl.dgvReceive.DataSource = tableProductReceive;
                        if (status.Equals("0"))
                        {
                            quantyReceive++;
                        }
                        else
                        {
                            quantyReceive++;
                            Code.PackagingOrder++;
                        }
                        quantySend--;
                        bl.lblQuantityReceive.Text = "" + bl.dgvReceive.Rows.Count;
                        bl.lblQtySend.Text = "" + quantySend;
                        if (!Code.masih)
                        {
                            if (tempTerima > generateCode.QTY_KIRIM-1)
                            {
                                kirimprinterLagi();
                                tempTerima = 0;
                            }
                            tempTerima++;
                        }
                        else
                        {
                            if (quantyReceive == Code.PackagingOrder)
                            {
                                finish();
                            }

                        }
                    }
                }
            }
        }

        //public void cmb()
        //{
        //    if (onprogress)
        //    {
        //        List<string> da = new List<string>();
        //        da.Add(sqlite.config["batchnumber"]);
        //        bl.cmbBatch.DataSource = da;
        //    }
        //    else
        //    {
        //        List<string> field = new List<string>();
        //        field.Add("batchNumber,productModelId");
        //        string where = "status = '" + STATUS_PO + "' and linepackagingid = '" + linenumber+ "'";
        //        datacombo = db.selectList(field, "[packaging_order]", where);
        //        List<string> da = new List<string>();
        //        for (int i = 0; i < datacombo.Count; i++)
        //        {
        //            da.Add(datacombo[i][0]);
        //        }
        //        bl.cmbBatch.DataSource = da;
        //    }
        //}

        public void log(string data)
        {
            string dates = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            string simpan = dates + "\t" + data + "\n";
            lg.txtLog.AppendText(simpan + Environment.NewLine);
        }

  
        public void startAudit()
        {
           // if (cekPrinterready())
          //  {
                if (batch.Length > 0)
                {
                    bool cekOnProgress = db.onprogress(sqlite.config["batchnumber"]);
                    if (cekOnProgress)
                    {
                        onprogress = true;
                        batch = sqlite.config["batchnumber"];
                        log("Pre-check On Progress");
                        string[] data = db.dataPO(batch, AGGREGATION);
                        int aggre = int.Parse(data[4]);
                        quantyProduct = int.Parse(data[2]);
                        int blister = int.Parse(data[6]);
                        double qtybl = quantyProduct / blister;
                        int aggreInfeed = int.Parse(data[6]);
                        qtybl = qtybl / aggreInfeed;
                        blister = (int)Math.Ceiling(qtybl);
                        double qtyinfeed = blister / aggre;
                        aggre = (int)Math.Ceiling(qtyinfeed);

                        tampilDataProduct("5", "isused ='0' or isused = '1'");
                        Code.kirimPrinter(dataProductTrial);
                        Confirm cf = new Confirm("Pre-check success? ", "Confirmation");
                        bool hasilDialog = cf.conf();
                        if (hasilDialog)
                        {
                            bl.btnStart.Enabled = true;
                        }
                    }
                    else
                    {
                        auditNew();
                    }
                }
                else
                {
                    auditNew();
                }

                db.eventname = "Start Audit";
                db.systemlog();
            }
        //}
       
        private void auditNew()
        {
            string[] data = db.dataPO(batch, AGGREGATION);
            log("Pre-check Started");
            int aggre = int.Parse(data[4]);
            quantyProduct = int.Parse(data[2]);
            int blister = int.Parse(data[6]);
            double qtybl = quantyProduct / blister;
            blister = (int)Math.Ceiling(qtybl);
            double qtyinfeed =(double) blister / (double) aggre;
            if (qtybl > 5)
                aggre = 5;
            else
                aggre = (int)Math.Ceiling(qtyinfeed);
            Code.postCodeProduct = aggre;
            audit = false;
            Code.gtin = data[0];
            Code.exp = data[1];
            datattemp = sqlite.getProductProgress();
            if (!start)
            {
                List<string[]> datakirim = null;
                if (datattemp.Count < 5 && datattemp.Count < aggre)
                {
                  //  datakirim = Code.genarateProductAudit(aggre, batch);
                    gabungData(datakirim);
                }
                tampilDataProduct("5", "isused ='0' or isused = '1'");
                Code.kirimPrinter(datattemp);
                datattemp = new List<string[]>();
            }
            bool sukses = false;
            bl.lblQtySend.Text = "" + bl.dgvSend.Rows.Count;
           // DialogResult result = new Confirm("Pre-check success?", "Confirmation", MessageBoxButtons.YesNo);
           // if (result == DialogResult.Yes)
            {
                bl.btnStart.Enabled = true;
            //    foreach (string[] cap in dataProductTrial)
            //    {
            //        hapusGrid(cap[4]);
            //    }
            //    bl.lblQuan.Text = "" + Math.Ceiling(qtyinfeed);
            //    bl.lblQtySend.Text = "" + tempProduct.Count;
            }
          //  else
            {
                //foreach (string[] cap in dataProductTrial)
                //{
                //    hapusGrid(cap[4]);
                //}
                //bl.lblQtySend.Text = "5";
            }
            bl.lblQtySend.Text = "0";
            post = 0;
            audit = true;
        }
      
        List<string[]> datattemp;

        public void gabungData(List<string[]> data)
        {
            if (data== null)
            {
                foreach (string[] da in data)
                {
                    datattemp.Add(da);
                }
            }
        }

        public void startStation()
        {
            db.eventname = "Start Station";
            db.systemlog();
            log("Login successfully admin ID = " + adminid);
            bl.lblAdmin.Text = adminid;
            log("Server Port : " + sqlite.config["portserver"]);
            //cmb();
            bl.labelLine.Text = "Line " + sqlite.config["lineNumber"];
            tableProductReceive.Columns.Add("Gs1 ID");
            tableProductReceive.Columns.Add("Timestamp");
            tableProductReceive.Columns.Add("Status");
            DelayDevice();
            serverThread = new Thread(new ThreadStart(MulaiServer));
            serverThread.Start();
            bl.dgvReceive.DataSource = tableProductReceive;
        }

        public bool PingHost(string nameOrAddress)
        {
            if (nameOrAddress.Length == 0)
                return false;
            bool pingable = false;
            Ping pinger = new Ping();
            try
            {
                PingReply reply = pinger.Send(nameOrAddress);
                pingable = reply.Status == IPStatus.Success;
            }
            catch (PingException)
            {
                // Discard PingExceptions and return false;
            }
            return pingable;
        }

        public bool cekPrinterready()
        {
             print pr = new print();
             if (pr.cekKoneksi())
             {
                 if (pr.cekStatusPrinter())
                 {
                     bl.lblCamera.Text = pr.statusLeibinger;
                     return true;
                 }
             }
             new Confirm("Printer Not Connected"+pr.statusLeibinger);
             bl.lblCamera.Text = pr.statusLeibinger;
             return false;
        }

        public void refresh()
        {
            if (!start)
            {
                sqlite = new sqlitecs();
                tableProductReceive = new DataTable();
                tableProductReceive.Columns.Add("Gs1 ID");
                tableProductReceive.Columns.Add("Timestamp");
                tableProductReceive.Columns.Add("Status");

                tableProductReceive = new DataTable();
                //getJumlah();
                if (sqlite.config["flag"].Equals("0"))
                    onprogress = false;
                else
                    onprogress = true;
                //cmb();
                bl.dgvReceive.DataSource = tableProductReceive;
                bl.dgvSend.DataSource = tableProductSend;
            }
        }
    
    // yang baru 
        int quantysend;
        int banyakCap;
        DataTable tableProduct;
        string[] tempProduct;
        private void tampilDataProductNew(string status, string banyak)
        {
            List<string> field = new List<string>();
            field.Add("gtin,batchNumber,expire,sn,gsone");
            string limit = "";
            if (banyak.Length > 0)
                limit = "limit " + limit;
            List<string[]> ds = sqlite.select(field, NAMA_TABLE_TEMP, status + " " + limit);
            if (sqlite.num_rows > 0)
            {
                quantysend = sqlite.num_rows;
                banyakCap = sqlite.num_rows;
                tableProduct = new DataTable();
                tableProduct.Columns.Add("GS1 ID");
                bl.dgvSend.DataSource = tableProduct;
                int j = 0;
                tempProduct = new string[sqlite.num_rows];
                dataProductTrial = new List<string[]>();
                foreach (string[] Row in ds)
                {
                    DataRow row = tableProduct.NewRow();
                    dataProductTrial.Add(Row);
                    tableProductSend.Rows.Add(row);
                    row[0] = Row[4];
                    tableProduct.Rows.Add(row);
                    j++;
                }

                bl.dgvSend.DataSource = tableProduct;
            }
        }

        public void updatetoServer()
        {
            string[] data = db.dataPO(batch, AGGREGATION);
          //  db.addVaccine(sqlite.config["batchnumber"], linenumber, data[4], sqlite);
        }

        public void startAuditNew()
        {
            int auditProduct = sqlite.getDataProductAll();


            int countResult = sqlite.getCountResult();
            if (countResult > 0)
            {
                updatetoServer();
            }
            if (sqlite.config["batchnumber"].Length > 0)
            {
                if (db.onprogress(sqlite.config["batchnumber"]))
                {
                    if (batch.Equals(sqlite.config["batchnumber"]))
                    {
                        //cek dulu di sql server 
                        int maxVaccine = db.getcountProduct(batch);
                        if (maxVaccine == 0)
                        {
                            if (sqlite.getDatacapAllNotReciv() > 0)
                            {
                                batch = sqlite.config["batchnumber"];
                                onprogress = true;
                                log("Pre-check On progress");
                                tampilDataProductNew("isused = '0' or isused ='1'", "5");
                                kirimPrinterGsone(5);

                                //DialogResult resultd = new Confirm("Audit Success?", "Confirmation", MessageBoxButtons.YesNo);
                                //if (resultd == DialogResult.Yes)
                                {
                                    bl.btnStart.Enabled = true;
                                    post = 0;
                                }
                               // else
                                {
                                    post = 0;
                                    bl.btnStart.Enabled = false;
                                }
                            }
                            else
                            {
                                auditnew();
                            }
                        }
                        else
                        {
                            continueAudit(maxVaccine + 1);
                        }
                    }
                    else
                    {
                        auditnew();
                    }
                }
                else
                {
                    deleteData();
                    new Confirm("BatchNumber " + sqlite.config["batchnumber"] + " Close From admin");
                    // auditnew();
                }
            }
            else
            {
                //cek dulu di sql server 
                int maxVaccine = db.getcountProduct(batch);
                if (maxVaccine == 0)
                {
                    deleteData();
                    Code.tambahProduct = 1;
                    auditnew();
                }
                else
                {
                    deleteData();
                    continueAudit(maxVaccine + 1);
                }
            }
        }

        public void continueAudit(int lastCreate)
        {
            log("Pre-check continue from :" + lastCreate);
            deleteData();
            audit = false;
            Code.batch = batch;
            Code.tambahProduct = lastCreate;
            Code.generateProductAuditContinue(5);
            tampilDataProductNew("isused = '0' or isused ='1'", "");
            kirimPrinterGsone(5);
            post = post + 5;
            //DialogResult resultd = new Confirm("Audit Success?", "Confirmation", MessageBoxButtons.YesNo);
            //if (resultd == DialogResult.Yes)
            {
                bl.btnStart.Enabled = true;
            }
         //   else
            {
                //startAudit();
            }
            post = 0;
        }

        public void auditnew()
        {
            log("Audit New Trial Started");
            audit = false;
            Code.batch = batch;
            string[] CountTemp = sqlite.getDataProduct();
            if (CountTemp.Length < 5)
            {
                int banyakkirim = 5 - CountTemp.Length;
                Code.genarateProductAuditNew(banyakkirim);
            }

            tampilDataProductNew("isused = '0' or isused ='1'", "");
            bool sukses = false;
            kirimPrinterGsone(5);
            post = post + 5;
            //DialogResult resultd = new Confirm("Audit Success?", "Confirmation", MessageBoxButtons.YesNo);
            //if (resultd == DialogResult.Yes)
            {
                bl.btnStart.Enabled = true;
            }
           // else
            {
                //startAudit();
            }
            post = 0;
        }
     
        public void deleteData()
        {

            sqlite.delete("", NAMA_TABLE_TEMP);
            sqlite.delete("", "result");
        }

        int hitungInfeed()
        {
            string[] data = db.dataPO(batch, AGGREGATION);
            quantyProduct = int.Parse(data[2]);
            int aggre = int.Parse(data[4]);
            double qtybl = quantyProduct / aggre;
            int aggreInfeed = int.Parse(data[6]);
            qtybl = qtybl / aggreInfeed;
            return (int)Math.Ceiling(qtybl);
        }


        public void startprintingNew()
        {
            Confirm cf = new Confirm("Are you sure? ", "Confirmation");
            bool hasilDialog = cf.conf();
            if (hasilDialog)
            {
                start = true;
                disable();
                delay();
                if (onprogress)
                {
                    //Ambil Data yang diperlukan
                    batch = sqlite.config["batchnumber"];
                    Code.batch = batch;
                    string[] data = db.dataPO(batch, AGGREGATION);

                    //ambil data Cap
                    string[] dataProductOnProgress = sqlite.getDataProduct();
                    List<string> tem = new List<string>(dataProductOnProgress);
                    Code.dataProduct = tem;

                    //ambil banyaknya data
                    int countCap = sqlite.getDataProductAll();

                    //ambil banyaknya data yang belum diterima
                    int countCapNotReciv = sqlite.getDatacapAllNotReciv();


                    //inisialisasi banyak datanya
                    Code.PackagingOrder = hitungInfeed();
                    Code.tambahProduct = countCap + 1;

                    //update tampilan
                    bl.labelBatch.Text = batch;
                    bl.labelProduct.Text = data[3];
                    productmodelid = data[4];
                    bl.labelProduct.Text = productmodelid;
                    string[] dataTempcap;
                    List<string[]> dataTempvial;
                    //jika masih kurang maka generate cap
                    if (countCapNotReciv < generateCode.QTY_MAKS_KIRIM_PRODUCT)
                    {
                        //hitung kurang dari maks
                        int hitungCap = generateCode.QTY_MAKS_KIRIM_PRODUCT - countCapNotReciv;
                        Code.generateProduct(hitungCap);
                    }
                    //tampil data
                    tampilDataProductNew("isused = '0' or isused ='1'", "");

                    //kirim ke printer
                    Code.kirimPrinter(dataProductTrial);
                }
                else
                {
                    //inisialisasi batch
                    sqlite.updatestatusconfig(batch);
                    Code.batch = batch;
                    //inisialisasi data GS1
                    string[] data = db.dataPO(batch, AGGREGATION);
                    Code.PackagingOrder = hitungInfeed();

                    //update tampilan
                    bl.labelBatch.Text = batch;
                    bl.labelProduct.Text = data[3];

                    //ambil 

                    int maxVaccine = db.getcountProduct(batch);
                    if (maxVaccine == 0)
                    {
                        Code.penomoran = 1;
                    }
                    else
                    {
                        Code.penomoran = maxVaccine + 1;

                    }
                    //generate data
                    deleteData();
                    List<string[]> dataTempcap = Code.generateProduct();
                    //tampil kan di table
                    tampilDataProductNew("isused = '0' or isused ='1'", "");

                    //kirim ke printer
                    Code.kirimPrinter(dataTempcap);
                    //productmodelid = datacombo[bl.cmbBatch.SelectedIndex][1];
                }
                bl.lblQtySend.Text = "" + bl.dgvSend.Rows.Count;
            }
        }

    }
}
