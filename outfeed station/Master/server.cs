﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Net;
using System.Threading;
using System.Data;
using System.Windows.Forms;

namespace Outfeed_Station
{
    public class server
    {
        Outfeed ad;
        int port;
        Control ms;
        int fun;
        public bool alredyOpened;

        public server(Outfeed bl, string ports, Control mst, int func)
        {
            ms = mst;
            port = int.Parse(ports);
            ad = bl;
            isRunning = true;
            fun = func;
            mThread t;
            if (!cekPort(ports))
            {
                alredyOpened = false;
                t = new mThread(StartListening);
                Thread masterThread = new Thread(() => t(ref isRunning));
                masterThread.IsBackground = true; //better to run it as a background thread
                masterThread.Start();
            }
            else
            {
                alredyOpened = true;
                t = new mThread(StartListening);
                Thread masterThread = new Thread(() => t(ref isRunning));
                masterThread.IsBackground = true; //better to run it as a background thread
                masterThread.Start();
                //   new Confirm("Port already opened", "Information", MessageBoxButtons.OK);
                mst.db.simpanLog("Port :" + ports + " Used");
            }
        }

        public bool isRunning;

        delegate void mThread(ref bool isRunning);
        delegate void AccptTcpClnt(ref TcpClient client, TcpListener listener);

        public void StreamClose()
        {
            try
            {
                stream.Close();
                listener.Stop();
                tt.Abort();
                handler.Close();
                handler = null;
            }
            catch (Exception ex)
            {
                //ms.db.simpanLog(ex.Message);
            }
        }

        TcpClient handler = null;

        public TcpListener listener;
        Thread tt;
        NetworkStream stream;
        public static void AccptClnt(ref TcpClient client, TcpListener listener)
        {
            if (client == null)
                client = listener.AcceptTcpClient();
        }

        public void StartListening(ref bool isRunning)
        {
            listener = new TcpListener(port);
            Byte[] bytes = new Byte[256];
            
                listener.Start();
                handler = null;
                while (isRunning)
                {
                    AccptTcpClnt t = new AccptTcpClnt(AccptClnt);
                    tt = new Thread(() => t(ref handler, listener));
                    tt.IsBackground = true;
                    tt.Start(); //the AcceptTcpClient() is a blocking method, so we are invoking it    in a seperate thread so that we can break the while loop wher isRunning becomes false

                    while (isRunning && tt.IsAlive && handler == null)
                        Thread.Sleep(0); //change the time as you prefer

                    if (handler != null)
                    {
                        stream = null;
                        if (fun == 0)
                        {
                            stream = handler.GetStream();
                            ms.log("Connected " + handler.Client.RemoteEndPoint);
                            String Message = "";
                            bool konek = true;
                            while (konek)
                            {
                                try
                                {
                                    string data = null;
                                    int i;
                                    ms.log("Waiting Data");
                                    Int32 da = stream.Read(bytes, 0, bytes.Length);

                                    Message = "";
                                    while (true)
                                    {
                                        if (da == 0)
                                            break;
                                        Message = Message + System.Text.Encoding.ASCII.GetString(bytes, 0, da);
                                        if (da < 256)
                                            break;
                                        da = stream.Read(bytes, 0, bytes.Length);
                                    }
                                    if (Message.Length > 0)
                                    {
                                        ms.tambahData(Message);
                                    }
                                    else
                                    {

                                        konek = false;
                                        stream.Close();
                                        ms.log("Close Connection");
                                    }
                                }
                                catch (Exception ex)
                                {
                                    konek = false;
                                    stream.Close();
                                    ms.db.simpanLog(ex.Message);
                                    ms.log("Close Connection");
                                }
                            }
                        }
                        else
                        {
                            try
                            {
                                string data = null;
                                int i;
                                Int32 da = stream.Read(bytes, 0, bytes.Length);
                                String Message = System.Text.Encoding.ASCII.GetString(bytes, 0, da);
                                stream = handler.GetStream();
                                string databalik = "" + ms.cekKamera(Message) + "\n";
                                byte[] msg = System.Text.Encoding.ASCII.GetBytes(databalik);
                                stream.Write(msg, 0, msg.Length);
                                stream.Flush();
                                stream.Close();
                            }
                            catch (Exception nu)
                            {
                                if(stream != null)
                                    stream.Close();
                            }
                        }
                    }
                    else if (!isRunning && tt.IsAlive)
                    {
                        tt.Abort();
                    }
                    handler = null;
                }
                listener.Stop();
        }

        public bool cekPort(string port)
        {
            using (TcpClient tcpClient = new TcpClient())
            {
                try
                {
                    tcpClient.Connect("127.0.0.1", int.Parse(port));
                    return true;
                }
                catch (Exception)
                {
                    Console.WriteLine("Port closed");
                    return false;
                }
            }
            return false;
        }
    }
}
    
