﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Configuration;
using System.Windows.Forms;
using System.Data;
using System.IO;
using System.Diagnostics;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using System.Net;
using System.Net.Sockets;

namespace Outfeed_Station
{
    public class dbaccess
    {
        public static SqlConnection Connection;
        public static SqlCommand Command;
        public static SqlDataReader DataReader;
        public static SqlDataAdapter dataadapter;
        public List<string> dataInsert = new List<string>();
        public List<string> dataUpdate = new List<string>();
        public DataTable dataHeader;
        public int num_rows = 0;
        sqlitecs sqlite;
        public string valid;
        public string eventname;
        string eventtype;
        public bool sysLog = true;
        public string adminid;
        public string uom;
        public string to;
        public string from;
        public int PackagingqtyOnprogress;
        public string movementType = "6";
        public bool isConnect = false;

        public int fcon = 0;
        public string errorMessage;
     
        public dbaccess()
        {
            sqlite = new sqlitecs();
            Connect();
        }

        public bool Connect()
        {
            //if (!isConnect)
            //{
            try
            {
                string connec = "Data Source=" + sqlite.config["ipDB"] + ";" +
                                "Initial Catalog=" + sqlite.config["namaDB"] + ";" +
                                "User id=" + sqlite.config["usernameDB"] + ";" +
                                "Password=" + sqlite.config["passDB"] + ";" +
                                //"Password=docotronics;" +
                                 "Connection Timeout=" + sqlite.config["timeout"] + ";";
                Connection = new SqlConnection(connec);
                Connection.Open();
                fcon = 0;
                errorMessage = "";
                return true;
            }
            catch (TimeoutException t)
            {
                Console.WriteLine("MASUK CATCH 1");
                fcon++;
                errorMessage = t.Message;
                if (fcon < 2)
                {
                    Console.WriteLine("MASUK CATCH 1 IF");
                    new Confirm("Can not open connection to Database Server.", "Information", MessageBoxButtons.OK);
                    config cfg = new config(null, 0, "");
                    cfg.ShowDialog();
                }
            }
            catch (SqlException se)
            {
                Console.WriteLine("MASUK CATCH 2 : " + se.Message);
                fcon++;
                Console.WriteLine("FCON : " + fcon);
                errorMessage = se.Message;
                if (fcon < 2)
                {
                    Console.WriteLine("MASUK CATCH 2 IF : " + se.Message);
                    errorMessage = se.Message;
                    new Confirm("Can not open connection to Database Server.", "Information", MessageBoxButtons.OK);
                    config cfg = new config(null, 0, "");
                    cfg.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("MASUK CATCH 3");
                fcon++;
                errorMessage = ex.Message;
                if (fcon < 2)
                {
                    Console.WriteLine("MASUK CATCH 3 IF");
                    new Confirm("Can not open connection to Database Server.", "Information", MessageBoxButtons.OK);
                    simpanLog("Error SQLSERVER : " + ex.ToString());
                    config cfg = new config(null, 0, "");
                    cfg.ShowDialog();
                }
            }
            return false;
            //}
        }

        public void closeConnection()
        {
            Connection.Close();
        }

        public static List<String[]> LoadProductionOrders()
        {
            List<String[]> Results = new List<String[]>();
            String Query = "SELECT production_order_number, created_date, status FROM transaction_production_order";

            Connection.Open();
            Command = new SqlCommand(Query, Connection);
            DataReader = Command.ExecuteReader();

            while (DataReader.Read())
            {
                String[] Data = new String[] { DataReader.GetValue(0).ToString(), DataReader.GetValue(1).ToString(), DataReader.GetValue(2).ToString() };
                Results.Add(Data);
            }

            DataReader.Close();
            Command.Dispose();
            Connection.Close();

            return Results;
        }

        public void insert(Dictionary<string, string> field, string table)
        {
            try
            {
                string sql = "INSERT INTO " + table + " (";
                int i = 0;
                foreach (string key in field.Keys)
                {
                    sql += "" + key + "";
                    if (i + 1 < field.Count)
                    {
                        sql += ",";
                        i++;
                    }
                }

                sql += ") values (";

                i = 0;
                foreach (string key in field.Values)
                {
                    sql += "'" + key + "'";
                    if (i + 1 < field.Count)
                    {
                        sql += ",";
                        i++;
                    }
                }
                sql += ")";
                simpanLog(sql);
                eventtype = "0";
                Connect();
                Command = new SqlCommand(sql, Connection);
                Command.ExecuteNonQuery();
            }
            catch (Exception sq)
            {
                isConnect = false;
                Connection.Close();
                simpanLog("Error SQl : " + sq.ToString());
            }
        }

        public void update(Dictionary<string, string> field, string table, string where)
        {
            try
            {
                string sql = "UPDATE " + table + " SET ";
                int i = 0;
                foreach (KeyValuePair<string, string> key in field)
                {
                    sql += key.Key + " = " + "'" + key.Value + "'";
                    if (i + 1 < field.Count)
                    {
                        sql += ",";
                        i++;
                    }
                }

                if (where.Length > 0)
                    sql += " WHERE " + where;

                simpanLog(sql);
                eventtype = "1";
                Connect();
                Command = new SqlCommand(sql, Connection);
                Command.ExecuteNonQuery();
            }
            catch (Exception sq)
            {
                isConnect = false;
                Connection.Close();
                simpanLog("Error SQl : " + sq.ToString());
            }
        }

        public DataSet select(List<string> field, string table, string where)
        {
            try
            {
                string sql = "SELECT ";
                for (int j = 0; j < field.Count; j++)
                {
                    sql += "'" + field[j] + "'";
                    if (j + 1 < field.Count)
                    {
                        sql += ",";
                        j++;
                    }
                }

                sql += " From " + table;

                if (where.Length > 0)
                    sql += " WHERE " + where;

                simpanLog(sql);
                Connect();
                dataadapter = new SqlDataAdapter(sql, Connection);
                DataSet Results = new DataSet();
                dataadapter.Fill(Results);
                return Results;
            }
            catch (Exception sq)
            {
                isConnect = false;
                Connection.Close();
                simpanLog("Error SQLSERVER : " + sq.ToString());
            }
            finally
            {
                try
                {
                    DataReader.Close();
                    Command.Dispose();
                }
                catch (Exception sq)
                {
                    isConnect = false;
                    Connection.Close();
                    simpanLog("Error SQLSERVER : " + sq.ToString());
                }

            }
            return null;
        }

        public List<String[]> selectList(List<string> field, string table, string where)
        {
            try
            {
                string sql = "SELECT ";
                for (int j = 0; j < field.Count; j++)
                {
                    sql += field[j];
                    if (j + 1 < field.Count)
                    {
                        sql += ",";
                        j++;
                    }
                }
                sql += " From " + table;

                if (where.Length > 0)
                    sql += " WHERE " + where;

                simpanLog(sql);
                Connect();
                eventtype = "1";
                List<String[]> Results = new List<String[]>();
                Command = new SqlCommand(sql, Connection);
                DataReader = Command.ExecuteReader();
                int num = 0;
                while (DataReader.Read())
                {
                    string[] data = new string[DataReader.FieldCount];
                    for (int u = 0; u < DataReader.FieldCount; u++)
                    {
                        data[u] = DataReader.GetValue(u).ToString();
                    }

                    Results.Add(data);
                    num++;
                }
                num_rows = num;
                dataHeader = new DataTable();
                for (int i = 0; i < DataReader.FieldCount; i++)
                {
                    dataHeader.Columns.Add(DataReader.GetName(i));
                }

                return Results;
            }
            catch (Exception sq)
            {
                isConnect = false;
                Connection.Close();
                simpanLog("Error  00100: " + sq.Message);
            }
            finally
            {
                try
                {
                    DataReader.Close();
                    Command.Dispose();
                }
                catch (Exception sq)
                {
                    isConnect = false;
                    Connection.Close();
                    simpanLog("Error SQLSERVER : " + sq.Message);
                }

            }
            return null;
        }

        public static List<String> GetPODetails(String PO)
        {
            List<String> Results = new List<String>();
            String Query = String.Format("SELECT production_order_number, product_code, product_name, product_quantity, status FROM transaction_production_order WHERE production_order_number = '{0}'", PO);

            Connection.Open();
            Command = new SqlCommand(Query, Connection);
            DataReader = Command.ExecuteReader();

            while (DataReader.Read())
            {
                for (int i = 0; i < 5; i++)
                {
                    Results.Add(DataReader.GetValue(i).ToString());
                }
            }

            DataReader.Close();
            Command.Dispose();
            Connection.Close();

            return Results;
        }

        public void simpanLog(String line)
        {
            DateTime now = DateTime.Now;
            string tahun = now.ToString("yyyy");
            string bulan = now.ToString("MM");
            string hari = now.ToString("dd");
            string dates = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            line = dates + "\t" + line;
            // cek file 
            bool cekfile = false;
            string namaFolder = "eventlog\\Database";
            try
            {
                while (!cekfile)
                {
                    if (Directory.Exists(namaFolder))
                    {
                        if (Directory.Exists(namaFolder + @"\" + tahun))
                        {
                            if (Directory.Exists(namaFolder + @"\" + tahun + @"\" + bulan))
                            {

                                using (StreamWriter outputFile = new StreamWriter(namaFolder + @"\" + tahun + @"\" + bulan + @"\" + hari + ".txt", true))
                                {
                                    outputFile.WriteLine(line);
                                }
                                cekfile = true;
                            }
                            else
                            {

                                Directory.CreateDirectory(namaFolder + @"\" + tahun + @"\" + bulan);
                            }
                        }
                        else
                        {
                            Directory.CreateDirectory(namaFolder + @"\" + tahun);
                        }
                    }
                    else
                    {

                        Directory.CreateDirectory(namaFolder);
                    }
                }
            }
            catch (Exception ex)
            {
                //simpanLog(ex.Message);
            }
        }
     
        internal bool cekPermision(string adminid, string modul_name, string permission)
        {
            List<string> field = new List<string>();
            field.Add("permissions_name");
            string where = "dbo.users.id = '" + adminid + "' AND dbo.permissions.module_name = '" + modul_name + "' AND dbo.permissions.permissions_name = '" + permission + "'";
            string from = "dbo.groups INNER JOIN dbo.permissions ON dbo.groups.id = dbo.permissions.role_id INNER JOIN dbo.users_groups ON dbo.users_groups.group_id = dbo.groups.id  INNER JOIN dbo.users ON dbo.users_groups.user_id = dbo.users.id";
            selectList(field, from, where);
            if (num_rows > 0)
                return true;
            return false;
        }
 
 
        public string md5hash(string source)
        {
            using (MD5 md5Hash = MD5.Create())
            {
                string hash = GetMd5Hash(md5Hash, source);
                return hash;
            }
            return null;
        }

        static string GetMd5Hash(MD5 md5Hash, string input)
        {

            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }

        static bool VerifyMd5Hash(MD5 md5Hash, string input, string hash)
        {
            // Hash the input.
            string hashOfInput = GetMd5Hash(md5Hash, input);

            // Create a StringComparer an compare the hashes.
            StringComparer comparer = StringComparer.OrdinalIgnoreCase;

            if (0 == comparer.Compare(hashOfInput, hash))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void Movement(Dictionary<string, string> field)
        {
            string from = "movement_history";
            DateTime now = DateTime.Now;
            string dates = DateTime.Now.ToString("yyMMddHHmmss");
            field.Add("movementId", dates);
            field.Add("eventTime", "" + (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1)).TotalSeconds));
            insert(field, from);
        }

        public string getUnixString()
        {
            string date = "" + (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1)).TotalSeconds);
            return date;
        }

        public string getUnixString(double data)
        {
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(data).ToLocalTime();
            return dtDateTime.ToShortDateString();
        }

        public void insertData(Dictionary<string, string> field, string table)
        {
            try
            {
                string sql = "INSERT INTO " + table + " (";
                int i = 0;
                foreach (string key in field.Keys)
                {
                    sql += "" + key + "";
                    if (i + 1 < field.Count)
                    {
                        sql += ",";
                        i++;
                    }
                }

                sql += ") values (";

                i = 0;
                foreach (string key in field.Values)
                {
                    sql += "'" + key + "'";
                    if (i + 1 < field.Count)
                    {
                        sql += ",";
                        i++;
                    }
                }
                sql += ")";
                simpanLog(sql);
                dataInsert.Add(sql);
            }
            catch (SqlException sq)
            {
                simpanLog("Error SQl : " + sq.ToString());
            }
            finally
            {
                Connection.Close();
            }
        }

        public void updateData(Dictionary<string, string> field, string table, string where)
        {
            try
            {
                string sql = "UPDATE " + table + " SET ";
                int i = 0;
                foreach (KeyValuePair<string, string> key in field)
                {
                    sql += key.Key + " = " + "'" + key.Value + "'";
                    if (i + 1 < field.Count)
                    {
                        sql += ",";
                        i++;
                    }
                }

                if (where.Length > 0)
                    sql += " WHERE " + where;

                simpanLog(sql);
                dataInsert.Add(sql);
            }
            catch (SqlException sq)
            {
                simpanLog("Error SQl : " + sq.ToString());
            }
            finally
            {
                Connection.Close();
            }
        }

        public void Begin()
        {
            try
            {
                if (dataInsert.Count > 0)
                {
                    string sql = "BEGIN TRANSACTION;\n ";

                    for (int i = 0; i < dataInsert.Count; i++)
                    {
                        sql += dataInsert[i] + "\n";
                    }
                    sql += "COMMIT;";
                    simpanLog("BEGIN TRANSACTION");
                    Connect();
                    Command = new SqlCommand(sql, Connection);
                    Command.ExecuteNonQuery();
                }
                dataInsert = new List<string>();
            }
            catch (Exception sq)
            {
                isConnect = false;
                Connection.Close();
                simpanLog("Error " + sq.ToString());

            }
        }     
        
        public Dictionary<string, string> field = new Dictionary<string, string>();
        public List<string> fieldGet = new List<string>();

        string iden = "OUT_";
        public void Movement(string order, string from, string to, string qty, string itemid)
        {
            string dates = DateTime.Now.ToString("ssMMddmmHHssyy");
            Dictionary<string, string> field = new Dictionary<string, string>();
            string guid = Guid.NewGuid().ToString().Substring(0, 10);
            field.Add("movementId", iden + dates + "_" + guid);
            field.Add("orderNumber", order);
            field.Add("movementType", movementType);
            field.Add("[from]", from);
            field.Add("[to]", to);
            field.Add("qty", qty);
            field.Add("uom", uom);
            field.Add("userid", adminid);
            field.Add("itemid", itemid);
            field.Add("eventTime", getUnixString());
            insert(field, "movement_history");
        }

        public void updateVaccine(string infeed, string gsone)
        {
            field = new Dictionary<string, string>();
            field.Add("innerBoxGsOneId", gsone);
            string where = "innerBoxId='" + infeed + "'";
            update(field, "Vaccine", where);
        }

        public void addVaccine(string batch, string line, string productmodel, result sq)
        {
            Dictionary<string, string> field;
            List<string> fieldlist = new List<string>();
            fieldlist.Add("gsone,createdtime,flag,infeed");
            List<string[]> res = sq.select(fieldlist, "result", "");
            int i= 0;
            if (sq.num_rows > 0)
            {

                foreach (string[] data in res)
                {
                    field = new Dictionary<string, string>();
                    field.Add("gsOneInnerBoxId", data[0]);
                    field.Add("createdtime", data[1]);
                    if (data[2].Equals("0"))
                    {
                        // vaccine sukses
                        field.Add("isreject", "0");
                    }
                    else
                    {
                        field.Add("isreject", "1");
                        field.Add("rejectTime", data[1]);
                    }
                    string where = "infeedInnerBoxId = '" + data[3] + "'";
                    updateData(field, "innerBox", where);

                    Movement(batch, data[3], data[1], "1", "");
                    updateVaccine(data[3], data[0]);
                }
                Begin();

                foreach (string[] data in res)
                {
                    updatestatus(data[0], sq);
                }
            }
            else
            {

            }
        }

        string[] datainnerbox()
        {
            List<string> field = new List<string>();
            field.Add("infeedInnerBoxId");
            string where = "innerBox.gsOneInnerBoxId IS NULL";
            List<string[]> ds = selectList(field,"innerBox",where);
            string[] temp;
            if(num_rows>0)
            {
                int i = 0;
                temp = new string[num_rows];
                foreach(string [] data in ds)
                {
                    temp[i] = data[0];
                }
                return temp;
            }
            return null;

        }

        void updatestatus(string capid, result sq)
        {
            string where = "gsone ='" + capid + "'";
            sq.delete(where,"result");
        }

        public bool validasi(string data, int val)
        {
            //text ip val = 0
            if (val == 0)
                return cekIP(data);
            else if (val == 1)
            {
                //text tidak kosong val =1
                if (data.Length == 0)
                {
                    valid = "Data kosong";
                    return false;
                }
            }
            else if (val == 2)
            {
                //text tidak ada angka
                int errorCounter = Regex.Matches(data, @"[a-zA-Z]").Count;
                if (errorCounter > 0)
                {
                    valid = "data mengandung angka";
                    return false;
                }
            }
            return true;

        }

        bool cekIP(string data)
        {
            if (data.Length < 3 & data.Length > 0)
                return false;
            int errorCounter = Regex.Matches(data, @"[a-zA-Z]").Count;
            if (errorCounter == 0)
            {
                string[] dapat = data.Split('.');
                if (dapat.Length != 4)
                {
                    valid = "Error : IP must 4 point";
                    return false;
                }
                if (dapat[3].Equals("0"))
                {
                    valid = "Error : the fourth digit of ip cannot be 0";
                    return false;
                }

                bool kosong = false;
                if (dapat[0].Equals("000"))
                    kosong = true;
                for (int i = 0; i < dapat.Length; i++)
                {
                    //if (dapat[i] == "" || dapat[i].Length <= 0)
                    //{
                    //    new Confirm("Error : IP must 4 point");
                    //    return false;
                    //}
                    if (dapat[i][0].Equals("0"))
                        return false;
                    if (dapat[i].Equals("00"))
                    {
                        valid = "Error : IP cannot fill 00";
                        return false;
                    }

                    bool rege = Regex.Match(dapat[i], @"^[0-9]+$").Success;
                    if (!rege)
                    {
                        valid = "Error : IP cannot contain alphabeth and symbols " + data;
                        return false;
                    }
                    if (dapat[i].Length == 0)
                    {
                        valid = "Error : Cannot empty";
                        return false;
                    }
                    int temp = int.Parse(dapat[i]);
                    if (temp >= 255)
                    {
                        valid = "Error : IP must be less then 255";
                        return false;
                    }
                    if (temp == 0 && kosong)
                    {
                        valid = "Error : First digit of ip cannot be 0";
                        return false;
                    }
                    if (dapat[i].Equals("000") && !kosong)
                    {
                        valid = "Error : IP cannot fill 000";
                        return false;
                    }
                    if (dapat[i][0].Equals('0') && temp != 0 && dapat[i].Length > 1)
                    {
                        valid = "Error : Ip cannot fill '0'";
                        return false;
                    }
                }
                return true;
            }
            else
            {
                valid = "Error : IP cannot contain alphabeth " + errorCounter;
                return false;
            }
            return true;
        }

        public string eventType = "12";
      
        public void systemlog()
        {
            try
            {
                Dictionary<string, string> field = new Dictionary<string, string>();
                field.Add("eventtime", getUnixString());
                field.Add("eventname", eventname);
                field.Add("eventtype", eventType);
                field.Add("userid", adminid);
                field.Add("[from]", from);
                field.Add("[to]", to);
                insert(field, "system_log");
            }
            catch (Exception)
            {

            }
        }
        
        public string[] dataPO(string batch)
        {
            List<string> field = new List<string>();
            field.Add("gtinoutbox,expired,packagingqty,model_name,product_model,manufacturingdate");
            string from = "packaging_order INNER JOIN product_model ON packaging_order.productModelId = product_model.product_model ";
            string where = "batchnumber = '" + batch + "'";
            List<string[]> ds = selectList(field, from, where);
            return ds[0];
        }

        public void updateStatus(string batch)
        {
            Dictionary<string, string> field = new Dictionary<string, string>();
            field.Add("isused", "1");
            string where = "batchnumber='" + batch + "'";
            update(field, Master.NAMA_TABLE_TEMP, where);
        }

        public static string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            throw new Exception("Local IP Address Not Found!");
        }
        public string ipAddress;
        
        public string[] cekLine(string where)
        {
            ipAddress = GetLocalIPAddress();
            List<string> field = new List<string>();
            string[] temp = new string[0];
            field.Add("lineName,linePackaging.linePackagingId");
            string from = "linePackagingDetail INNER JOIN linePackaging ON linePackagingDetail.linePackagingId = linePackaging.linePackagingId";
            //string where1 = where + "= '" + GetLocalIPAddress() + "'";
            string where1 = where + "= '192.168.14.101'";
            List<string[]> ds = selectList(field, from, where1);
            if (num_rows > 0)
                return ds[0];
            else
                return temp;
        }

        public bool decomission(string blister)
        {
            //update vaccine
            Dictionary<string, string> field = new Dictionary<string, string>();
            field.Add("innerBoxGsOneId", "null");
            string where = "innerBoxGsOneId like '%" + blister + "%' and isreject = 0";
            update(field, "vaccine", where);

            //update reject
            field = new Dictionary<string, string>();
            field.Add("isreject", "2");
            where = "gsOneInnerBoxId= '" + blister + "'";
            update(field, "innerbox", where);

            //system log
            eventname = "Decomission " + blister + " success";
            to = blister ;
            systemlog();
            return true;
        }

        public bool onprogress(string batch)
        {
            List<string> field = new List<string>();
            field.Add("status,packagingqty");
            string where = "batchnumber = '" + batch + "'";
            List<string[]> ds = selectList(field, "packaging_order", where);
            if (num_rows > 0)
            {
                string status = ds[0][0];
                if (status.Equals("99"))
                    return false;
                else
                    return true;
                PackagingqtyOnprogress = int.Parse(ds[0][1]);
            }
            else
            {
                return false;
            }
        }

        public int getcountProduct(string batch)
        {
            List<string> field = new List<string>();
            field.Add(Control.NAMA_SEQ);
            string where = "batchnumber = '" + batch + "'";
            List<string[]> ds = selectList(field, "packaging_order", where);
            if (num_rows > 0)
                return int.Parse(ds[0][0]);
            else
                return 0;
        }

        public void updatesequence(string batch, string status)
        {
            Dictionary<string, string> field = new Dictionary<string, string>();
            field.Add(Control.NAMA_SEQ, status);
            string where = "batchnumber = '" + batch + "'";
            update(field, "packaging_order", where);
        }

        public bool sampleData(string productid)
        {
            //update Vaccine 
            Dictionary<string, string> field = new Dictionary<string, string>();
            field.Add("isreject", "3");
            string where = "(innerBoxId like '%" + productid + "%' or innerBoxGsOneId like '%" + productid + "%') and isreject = 0";
            update(field, "vaccine", where);

            //update reject
            field = new Dictionary<string, string>();
            field.Add("isreject", "3");
            where = "(gsOneInnerBoxId like '%" + productid + "%' or infeedInnerBoxId like '%" + productid + "%') and isreject = 0";
            update(field, "innerbox", where);
            //system log
            eventname = "Sample " + productid + " success";
            systemlog();

            return true;

        }

        public bool cekRole(string adminid, string module)
        {
            List<string> field = new List<string>();
            field.Add("permission.[module]");
            string from = "[user] INNER JOIN user_role ON [user].role = user_role.role_id INNER JOIN permission ON permission.roleId = user_role.role_id";
            string where = "permission.[read] = '0' AND [user].userId = '" + adminid + "' AND permission.[module] = '" + module + "'";
            List<string[]> ds = selectList(field, from, where);
            if (num_rows > 0)
                return true;
            else
                return false;
        }

        internal string[] dataPO(string batch, string AGGREGATION)
        {
            throw new NotImplementedException();
        }

        public bool cekInfeed(string databarcode, int status)
        {
            List<string> field = new List<string>();
            field.Add("*");
            string where = "gsOneInnerBoxId = '" + databarcode + "' and isreject='" + status + "'";
            selectList(field, "innerbox", where);
            if (num_rows > 0)
                return true;
            return false;
        }
        public bool validasi(string username)
        {
            try
            {
                if (username.Length > 0)
                {
                    List<string> field = new List<string>();
                    field.Add("id");
                    string where = "username = '" + username + "'";
                    List<string[]> result = selectList(field, "[users]", where);
                    if (result.Count > 0)
                    {
                        return true;
                    }
                    else
                        return false;
                }
                else
                {
                    return false;
                }
            }
            catch (NullReferenceException nl)
            {
                simpanLog("Error  00100: " + nl.Message);
                return false;
            }

        }

        public List<string[]> validasi(string username, string password, string status)
        {
            try
            {
                if (username.Length > 0 && password.Length > 0)
                {
                    List<string> field = new List<string>();
                    field.Add("id");
                    string where = "username = '" + username + "' AND password = '" + md5hash(password) + "' AND Active = " + status;
                    List<string[]> result = selectList(field, "[users]", where);
                    if (result.Count > 0)
                    {
                        from = "Bottle Station";
                        adminid = result[0][0];
                        eventtype = "1";
                        eventname = "Login";
                        systemlog();
                        return result;
                    }
                    else
                        return null;
                }
                else
                {
                    string code = "0010";
                    simpanLog(code + " : USERNAME DAN PASSWORD SALAH");
                    return null;
                }
            }
            catch (NullReferenceException nl)
            {
                var st = new StackTrace(nl, true);
                var frame = st.GetFrame(0);
                string line = frame.ToString();
                simpanLog("Error  00100: " + line);
                return null;
            }

        }

        public bool validasi(string username, string password)
        {
            try
            {
                if (username.Length > 0 && password.Length > 0)
                {
                    List<string> field = new List<string>();
                    field.Add("id");
                    string where = "username = '" + username + "' AND password = '" + md5hash(password) + "'";
                    List<string[]> result = selectList(field, "[users]", where);
                    if (result.Count > 0)
                    {
                        return true;
                    }
                    else
                        return false;
                }
                else
                {
                    string code = "0010";
                    simpanLog(code + " : USERNAME DAN PASSWORD SALAH");
                    return false;
                }
            }
            catch (NullReferenceException nl)
            {
                var st = new StackTrace(nl, true);
                var frame = st.GetFrame(0);
                string line = frame.ToString();
                simpanLog("Error  00100: " + line);
                return false;
            }

        }

        //tambahan
        internal List<string[]> getRole(string adminid)
        {
            List<string> field = new List<string>();
            field.Add("first_name, company");
            string where = "id = '" + adminid + "';";
            string from = "dbo.[users]";
            List<string[]> result = selectList(field, from, where);
            Connection.Close();
            if (result.Count > 0)
            {
                return result;
            }
            else
                return null;
        }


        public int selectCountPass(string batchNumber)
        {
            try
            {
                string sql = "SELECT Count(*) from (select a.innerBoxGsOneId from [Vaccine] a where a.batchNumber = '" + batchNumber + "' and a.isReject = 0 and a.innerBoxGsOneId is not null group by a.innerBoxGsOneId) as s";

                simpanLog(sql);
                Connect();
                Command = new SqlCommand(sql, Connection);
                DataReader = Command.ExecuteReader();
                int Results = 0;
                while (DataReader.Read())
                {
                    string[] data = new string[DataReader.FieldCount];
                    for (int u = 0; u < DataReader.FieldCount; u++)
                    {
                        data[u] = DataReader.GetValue(u).ToString();
                    }

                    Results = Int32.Parse(data[0]);
                }

                return Results;
            }
            catch (SqlException sq)
            {
                simpanLog("Error SQLSERVER : " + sq.ToString());
            }
            finally
            {
                try
                {
                    DataReader.Close();
                    Command.Dispose();
                }
                catch (SqlException sq)
                {
                    simpanLog("Error SQLSERVER : " + sq.ToString());
                }
                catch (NullReferenceException nl)
                {
                    simpanLog("Error SQLSERVER : " + nl.ToString());
                }

            }
            return 0;
        }

        public int selectCountReject(string batchNumber)
        {
            try
            {
                string sql = "SELECT Count(*) from (select a.blisterpackId, a.isReject from [Vaccine_Reject] a where a.batchNumber = '" + batchNumber + "' and a.blisterpackId is not null group by a.blisterpackId, a.isReject) as m";

                simpanLog(sql);
                Connect();
                Command = new SqlCommand(sql, Connection);
                DataReader = Command.ExecuteReader();
                int Results = 0;
                while (DataReader.Read())
                {
                    string[] data = new string[DataReader.FieldCount];
                    for (int u = 0; u < DataReader.FieldCount; u++)
                    {
                        data[u] = DataReader.GetValue(u).ToString();
                    }

                    Results = Int32.Parse(data[0]);
                }

                return Results;
            }
            catch (SqlException sq)
            {
                simpanLog("Error SQLSERVER : " + sq.ToString());
            }
            finally
            {
                try
                {
                    DataReader.Close();
                    Command.Dispose();
                }
                catch (SqlException sq)
                {
                    simpanLog("Error SQLSERVER : " + sq.ToString());
                }
                catch (NullReferenceException nl)
                {
                    simpanLog("Error SQLSERVER : " + nl.ToString());
                }

            }
            return 0;
        }

        public bool decommToBlister(string batchNumber)
        {
            Connect();
            Command = Connection.CreateCommand();
            SqlTransaction transaction;
            transaction = Connection.BeginTransaction("DecommToBlister");

            Command.Connection = Connection;
            Command.Transaction = transaction;

            try
            {
                Command.CommandText =
                    "delete FROM [Development_Packaging].[dbo].[blisterPack] where batchnumber = '" + batchNumber + "' and innerBoxId not in (SELECT [infeedInnerBoxId] FROM [Development_Packaging].[dbo].[innerBox] where batchnumber = '" + batchNumber + "' and basketId is not null)";
                Command.ExecuteNonQuery();
                Command.CommandText =
                    "delete FROM [Development_Packaging].[dbo].[innerBox] where batchnumber = '" + batchNumber + "' and basketId is null";
                Command.ExecuteNonQuery();
                Command.CommandText =
                    "update Vaccine set blisterpackId = null, isReject = 0, innerBoxId = null, innerBoxGsOneId = null where batchNumber = '" + batchNumber + "' and basketId is null";
                Command.ExecuteNonQuery();
                Command.CommandText =
                    "delete FROM [Development_Packaging].[dbo].[Vaccine_Reject] where blisterpackId is not null and batchNumber = '" + batchNumber + "'";
                Command.ExecuteNonQuery();
                transaction.Commit();

                return true;
            }
            catch (Exception ex)
            {
                simpanLog("Error SQLSERVER : Commit Exception Type: {0}" + ex.GetType());
                simpanLog("Error SQLSERVER : Message: {0}" + ex.Message);
                // Attempt to roll back the transaction.
                try
                {
                    transaction.Rollback();
                }
                catch (Exception ex2)
                {
                    // This catch block will handle any errors that may have occurred
                    // on the server that would cause the rollback to fail, such as
                    // a closed connection.
                    simpanLog("Error SQLSERVER : Rollback Exception Type: {0}" + ex2.GetType());
                    simpanLog("Error SQLSERVER : Message: {0}" + ex2.Message);
                }
                return false;
            }
        }

    }
}

