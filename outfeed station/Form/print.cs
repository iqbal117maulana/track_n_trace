﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace Outfeed_Station
{
    public partial class print : Form
    {
        tcp_testing tcp;
        List<string> dataCap = new List<string>();
        sqlitecs sql;
        public string statusLeibinger;
        public print()
        {
            InitializeComponent();
            tcp = new tcp_testing();
            // tcp.pr = this;
        }

        public bool cekKoneksi()
        {
            tcp = new tcp_testing();
            sql = new sqlitecs();
            string ip = sql.config["ipprinter"];
            string port = sql.config["portprinter"];
            bool res = tcp.Connect(ip, port, sql.config["timeout"]);
            return res;
        }

        public print(List<string> data)
        {
            dataCap = data;
            kirimData();
        }


        public int kirimData(List<string[]> data,int hit)
        {
            string disimpan = "";
            hitung = hit;
            for (int i = 0; i < data.Count; i++)
            {
                string[] datagsone = data[i];
                string simpan = "BUFFERDATA " + hitung + " \"" + datagsone[0] + "\" \"" + datagsone[1] + "\" \"" + datagsone[2] + "\" \"" + datagsone[3] + "\" " + "\"" + datagsone[5] + "\" " + "\"" + datagsone[6] + "\" " + "\"" + datagsone[7] + "\"";
                tcp.send(simpan);
                disimpan = disimpan + " \n| " + simpan;
                hitung++;
            }
            simpanLog(disimpan);
            tcp.dc();
            return hitung;
        }

        public void kon()
        {

        }

        public void simpanLog(String line)
        {
            DateTime now = DateTime.Now;
            string tahun = now.ToString("yyyy");
            string bulan = now.ToString("MM");
            string hari = now.ToString("dd");
            string dates = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            line = dates + "\t" + line;
            // cek file 
            bool cekfile = false;
            try
            {
                while (!cekfile)
                {
                    if (Directory.Exists("eventlog\\Printer"))
                    {
                        if (Directory.Exists("eventlog\\Printer" + @"\" + tahun))
                        {
                            if (Directory.Exists("eventlog\\Printer" + @"\" + tahun + @"\" + bulan))
                            {

                                using (StreamWriter outputFile = new StreamWriter("eventlog\\Printer" + @"\" + tahun + @"\" + bulan + @"\" + hari + ".txt", true))
                                {
                                    outputFile.WriteLine(line);
                                }
                                cekfile = true;
                            }
                            else
                            {

                                Directory.CreateDirectory("eventlog\\Printer" + @"\" + tahun + @"\" + bulan);
                            }
                        }
                        else
                        {
                            Directory.CreateDirectory("eventlog\\Printer" + @"\" + tahun);
                        }
                    }
                    else
                    {

                        Directory.CreateDirectory("eventlog\\Printer");
                    }
                }
            }
            catch (Exception ex)
            {
                // simpanLog(ex.Message);
            }
        }

        private void kirimData()
        {
            sql = new sqlitecs();
            for (int i = 0; i < dataCap.Count; i++)
            {
                tcp = new tcp_testing();
                tcp.Connect(sql.config["ipLibringer"], sql.config["portLibringer"], sql.config["timeout"]);
                Console.WriteLine("^0=MR" + hitung + " " + dataCap[i]);
                 tcp.send("^0=MR" + hitung + " " + dataCap[i]);
                //tcp.send("Data Blister " + hitung +" : "+ dataCap[i]);
                hitung++;
            }
            tcp.send("^0=MR" + hitung + " " + "End Of");

        }

        private void button7_Click(object sender, EventArgs e)
        {
            tcp.Connect(txtIp.Text, txtPort.Text,"10");
            //tcp.updatestatus("Connected");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            tcp.send("^0!PO");
            tcp.updatestatus("Power ON");
        }

        private void button4_Click(object sender, EventArgs e)
        {
            tcp.send("^0!NO");
            tcp.updatestatus("Noozle ON");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            tcp.send("^0!PF");
            tcp.updatestatus("Power OFF");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            tcp.send("^0!NC");
            tcp.updatestatus("Nozzle Close");
        }

        private void button6_Click(object sender, EventArgs e)
        {
            tcp.send("^0=ET " + txtExternal.Text);
            tcp.updatestatus("EXTERNAL TEXT : "+txtExternal.Text);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            tcp.send("^0!GO");
            hitung = 0;
            tcp.updatestatus("PRINT...");
        }
        int hitung=0;
        private void button8_Click(object sender, EventArgs e)
        {
            tcp.send("^0=MR" + hitung + " " + txtMailing.Text);
            tcp.updatestatus("MAILING RECORD " + hitung + " " + txtMailing.Text);
            hitung++;
        }

        private void label3_Click(object sender, EventArgs e)
        {
            tcp.send("^0?SM");
            tcp.updatestatus("Mail Status");
        }

        public void clearBuffer()
        {
            tcp.send("BUFFERCLEAR");
            tcp.dc();
        }


        public bool cekStatusPrinter()
        {
            string data = tcp.sendBack("GETMARKMODE");
            bool balik;
            string teksbalikan = "";
            if (data.Length > 0)
            {
                char nozzle = data[5];
                switch (nozzle)
                {
                    case '0':
                        statusLeibinger = "Printer Not Ready";
                        balik = false;
                        break;
                    default:
                        statusLeibinger = "Printer Ready";
                        balik = true;
                        break;
                }
                return balik;
            }
            return false;
        }



        internal void disconect()
        {
            tcp.dc();
        }
    }
}
