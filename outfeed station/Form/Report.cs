﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.Globalization;
using System.Text.RegularExpressions;
using Excel = Microsoft.Office.Interop.Excel;
using System.IO;

namespace Outfeed_Station
{
    public partial class Report : Form
    {
        public Dashboard dbss;
        string product;
        ListBatchNumber lbn;
        dbaccess db = new dbaccess();
        string linenumber;
        DataTable table1 = new DataTable();
        DataTable table2 = new DataTable();
        DataTable table3 = new DataTable();
        Boolean pass = false;
        Boolean fail = false;
        Boolean sample = false;
        string batch;
        List<string[]> data;

        public Report(Dashboard dsb, string line)
        {
            InitializeComponent();
            this.Text = this.Text + " V " + System.Windows.Forms.Application.ProductVersion;
            dbss = dsb;
            linenumber = line;
            GetComboBoxData("('03.003', '03.004');");
            btnGetReport.Enabled = false;
            btnExport.Enabled = false;
            refresh();
        }

        internal void refresh()
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new Action(refresh));
                return;
            }
            table1 = new DataTable();
            table1.Columns.Add("No");
            table1.Columns.Add("Blister ID");
            table1.Columns.Add("Cap ID");
            table1.Columns.Add("GS1 ID");
            table1.Columns.Add("Created Time");
            table1.Columns.Add("Expire Date");
            dataGridView1.DataSource = table1;
            this.dataGridView1.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridView1.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridView1.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridView1.Columns[3].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridView1.Columns[4].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridView1.Columns[5].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;

            table2 = new DataTable();
            table2.Columns.Add("No");
            table2.Columns.Add("Blister ID");
            table2.Columns.Add("Cap ID");
            table2.Columns.Add("GS1 ID");
            table2.Columns.Add("Created Time");
            table2.Columns.Add("Expire Date");
            table2.Columns.Add("Description");
            dataGridView2.DataSource = table2;
            this.dataGridView2.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridView2.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridView2.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridView2.Columns[3].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridView2.Columns[4].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridView2.Columns[5].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridView2.Columns[6].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;

            table3 = new DataTable();
            table3.Columns.Add("No");
            table3.Columns.Add("Blister ID");
            table3.Columns.Add("Cap ID");
            table3.Columns.Add("GS1 ID");
            table3.Columns.Add("Created Time");
            table3.Columns.Add("Expire Date");
            dataGridView3.DataSource = table3;
            this.dataGridView3.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridView3.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridView3.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridView3.Columns[3].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridView3.Columns[4].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridView3.Columns[5].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            lblTime.Text = DateTime.Now.ToLongTimeString();
            timer1.Start();
        }

        private void Report_Load(object sender, EventArgs e)
        {
            timer1.Start();
            lblTime.Text = DateTime.Now.ToLongTimeString();
            lblDate.Text = DateTime.Now.ToShortDateString();
        }

        private void pictureBox1_DoubleClick(object sender, EventArgs e)
        {
            dbss.Show();
            this.Hide();
        }

        private void pictureBox1_MouseEnter(object sender, EventArgs e)
        {
            this.pictureBox1.BackColor = ColorTranslator.FromHtml("#00505E");
        }

        private void pictureBox1_MouseLeave(object sender, EventArgs e)
        {
            this.pictureBox1.BackColor = ColorTranslator.FromHtml("#002F36");
        }

        private void Report_FormClosed(object sender, FormClosedEventArgs e)
        {
            dbss.Show();
            this.Hide();
        }

        public void GetComboBoxData(string parameter)
        {
            List<string> field = new List<string>();
            field.Add("model_name,product_model");
            string where = "product_model IN " + parameter + "";
            List<string[]> datacombo = db.selectList(field, "[product_model]", where);
            List<ComboboxItem> da = new List<ComboboxItem>();
            for (int i = 0; i < datacombo.Count; i++)
            {
                da.Add(new ComboboxItem() { Text = datacombo[i][0], Value = datacombo[i][1] });
            }
            cmbProduct.DataSource = da;
            cmbProduct.DisplayMember = "Text";
            cmbProduct.ValueMember = "Value";
        }

        public class ComboboxItem
        {
            public string Text { get; set; }
            public object Value { get; set; }
        }

        private void cmbProduct_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboboxItem ci1 = cmbProduct.SelectedItem as ComboboxItem;
            product = ci1.Value.ToString();
        }

        private void txtBatch_Click(object sender, EventArgs e)
        {
            lbn = new ListBatchNumber(null, linenumber, "(" + product + ")", "");
            lbn.ShowDialog();
            txtBatch.Text = lbn.batch;
            btnGetReport.Focus();
            if (txtBatch.Text.Length > 0)
            {
                btnGetReport.Enabled = true;
            }
            else
            {
                btnGetReport.Enabled = false;
            }
        }

        public void SetDataGridPass()
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new Action(SetDataGridPass));
                return;
            }
            List<string> field = new List<string>();
            field.Add("blisterpackId 'Blister ID', capId 'Cap ID', gsOneVialId 'GS1 ID', createdTime 'Created Time', expDate 'Expire Date'");
            string where = "batchNumber = '" + txtBatch.Text + "' and isReject = 0 and blisterpackId is not null;";
            List<string[]> dss = db.selectList(field, "[Vaccine]", where);
            if (db.num_rows > 0)
            {
                lblPas.Text = dss.Count.ToString();
                table1 = db.dataHeader;
                table1.Columns.Add("No").SetOrdinal(0);
                int i = 0;
                foreach (string[] Row in dss.Reverse<string[]>())
                {
                    DateTime dateExpiry = DateTime.ParseExact(Row[4], "yyMMdd", CultureInfo.InvariantCulture);
                    var a = new DateTime(1970, 1, 1, 0, 0, 0).AddSeconds(long.Parse(Row[3])).ToLocalTime();
                    i++;
                    DataRow row = table1.NewRow();
                    row[0] = i;
                    row[1] = "'" + Row[0];
                    row[2] = "'" + Row[1];
                    row[3] = Row[2];
                    row[4] = a;
                    row[5] = dateExpiry.ToShortDateString();
                    table1.Rows.Add(row);
                }
                pass = true;
            }
            else
            {
                lblPas.Text = "0";
                pass = false;
            }
        }

        public void SetDataGridFail()
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new Action(SetDataGridFail));
                return;
            }

            List<string> field2 = new List<string>();
            field2.Add("blisterpackId 'Blister ID', capId 'Cap ID', gsOneVialId 'GS1 ID', createdTime 'Created Time', expDate 'Expire Date', isReject");
            string where2 = "batchNumber = '" + batch + "' and isReject in (5,4,3,6,1) and blisterpackId is not null;";
            List<string[]> dss2 = db.selectList(field2, "[Vaccine_Reject]", where2);

            List<string> field = new List<string>();
            field.Add("blisterpackId 'Blister ID', capId 'Cap ID', gsOneVialId 'GS1 ID', createdTime 'Created Time', expDate 'Expire Date', isReject");
            string where = "batchNumber = '" + batch + "' and isReject = 2 and blisterpackId is not null;";
            List<string[]> dss = db.selectList(field, "[Vaccine]", where);
            if (dss2.Count > 0 && dss.Count > 0)
            {
                dss.AddRange(dss2);
                data = dss;
            }
            else
            {
                if (dss.Count <= 0 && dss2.Count > 0)
                {
                    data = dss2;
                }
                else
                {
                    data = dss;
                }
            }
            lblFail.Text = data.Count.ToString();
            table2 = db.dataHeader;
            table2.Columns.Add("No").SetOrdinal(0);
            table2.Columns.Add("Description").SetOrdinal(6);
            table2.Columns.Remove("isReject");
            int i = 0;
            foreach (string[] Row in data.Reverse<string[]>())
            {
                DateTime dateExpiry = DateTime.ParseExact(Row[4], "yyMMdd", CultureInfo.InvariantCulture);
                var a = new DateTime(1970, 1, 1, 0, 0, 0).AddSeconds(long.Parse(Row[3])).ToShortDateString();
                i++;
                DataRow row = table2.NewRow();
                row[0] = i;
                row[1] = "'" + Row[0];
                row[2] = "'" + Row[1];
                row[3] = Row[2];
                row[4] = a;
                row[5] = dateExpiry.ToShortDateString();
                if (Row[0].Equals("XXXXXX") || Row[0].Equals("XXXXX") || Row[0].Equals("XXXXXXX") || Row[0].Equals("xxxxx") || Row[0].Equals("xxxxxx") || Row[0].Equals("xxxxxxx"))
                {
                    row[6] = "Blister Pack ID can not be read";
                }
                else if (Row[1].Equals("XXXXXX") || Row[1].Equals("XXXXX") || Row[1].Equals("XXXXXXX") || Row[1].Equals("xxxxx") || Row[1].Equals("xxxxxx") || Row[1].Equals("xxxxxxx"))
                {
                    row[6] = "Cap ID can not be read";
                }
                else if (Row[5].Equals("2"))
                {
                    row[6] = "Decommission vaccine";
                }
                else if (Row[5].Equals("4"))
                {
                    row[6] = "Cap ID already registered / not found";
                }
                else if (Row[5].Equals("6"))
                {
                    row[6] = "Cap ID not found";
                }
                else if (Row[5].Equals("3"))
                {
                    row[6] = "Blister Pack ID already registered";
                }
                else if (Row[5].Equals("5"))
                {
                    row[6] = "Decommission blister";
                }
                table2.Rows.Add(row);
            }
            fail = true;
        }

        public void SetDataGridSample()
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new Action(SetDataGridSample));
                return;
            }

            List<string> field = new List<string>();
            field.Add("blisterpackId 'Blister ID', capId 'Cap ID', gsOneVialId 'GS1 ID', createdTime 'Created Time', expDate 'Expire Date'");
            string where = "batchNumber = '" + txtBatch.Text + "' and isReject = 3 and blisterpackId is not null;";
            List<string[]> dss = db.selectList(field, "[Vaccine]", where);
            if (db.num_rows > 0)
            {
                lblSample.Text = dss.Count.ToString();
                table3 = db.dataHeader;
                table3.Columns.Add("No").SetOrdinal(0);
                int i = 0;
                foreach (string[] Row in dss.Reverse<string[]>())
                {
                    DateTime dateExpiry = DateTime.ParseExact(Row[4], "yyMMdd", CultureInfo.InvariantCulture);
                    var a = new DateTime(1970, 1, 1, 0, 0, 0).AddSeconds(long.Parse(Row[3])).ToLocalTime();
                    i++;
                    DataRow row = table3.NewRow();
                    row[0] = i;
                    row[1] = "'" + Row[0];
                    row[2] = "'" + Row[1];
                    row[3] = Row[2];
                    row[4] = a;
                    row[5] = dateExpiry.ToShortDateString();
                    table3.Rows.Add(row);
                }
                sample = true;
            }
            else
            {
                lblSample.Text = "0";
                sample = false;
            }
        }

        private void btnGetReport_Click(object sender, EventArgs e)
        {
            refresh();
            batch = txtBatch.Text;
            backgroundWorker1 = new BackgroundWorker();
            backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork_1);
            backgroundWorker1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker1_RunWorkerCompleted_1);
            backgroundWorker1.RunWorkerAsync();
            loadingGif.Visible = true;
            btnGetReport.Enabled = false;
            btnExport.Enabled = false;
        }

        private void Task()
        {
            SetDataGridPass();
            SetDataGridFail();
            SetDataGridSample();
        }

        private void backgroundWorker1_DoWork_1(object sender, DoWorkEventArgs e)
        {

            Task();
        }

        private void backgroundWorker1_RunWorkerCompleted_1(object sender, RunWorkerCompletedEventArgs e)
        {
            showDataToGrid();
            if (pass == true || fail == true || sample == true)
            {
                btnExport.Enabled = true;
            }
            else if (pass == false || fail == false || sample == false)
            {
                btnExport.Enabled = false;
            }
            btnGetReport.Enabled = true;
            loadingGif.Visible = false;
        }

        private void showDataToGrid()
        {
            this.dataGridView1.DataSource = table1;
            this.dataGridView1.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridView1.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridView1.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridView1.Columns[3].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridView1.Columns[4].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridView1.Columns[5].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;

            this.dataGridView2.DataSource = table2;
            this.dataGridView2.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridView2.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridView2.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridView2.Columns[3].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridView2.Columns[4].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridView2.Columns[5].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridView2.Columns[6].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;

            this.dataGridView3.DataSource = table3;
            this.dataGridView3.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridView3.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridView3.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridView3.Columns[3].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridView3.Columns[4].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridView3.Columns[5].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            cmbProduct.Enabled = false;
            btnGetReport.Enabled = false;
            btnExport.Enabled = false;
            exportWithPaste();
            cmbProduct.Enabled = true;
            btnGetReport.Enabled = true;
            btnExport.Enabled = true;
        }

        private void copyAlltoClipboard()
        {
            dataGridView1.SelectAll();
            DataObject dataObj = dataGridView1.GetClipboardContent();
            if (dataObj != null)
                Clipboard.SetDataObject(dataObj);
        }

        private void copyAlltoClipboard1()
        {
            dataGridView2.SelectAll();
            DataObject dataObj = dataGridView2.GetClipboardContent();
            if (dataObj != null)
                Clipboard.SetDataObject(dataObj);
        }

        private void copyAlltoClipboard2()
        {
            dataGridView3.SelectAll();
            DataObject dataObj = dataGridView3.GetClipboardContent();
            if (dataObj != null)
                Clipboard.SetDataObject(dataObj);
        }

        private void exportWithPaste()
        {
            string dir = "c:\\Report TnT\\Blister Station";
            Directory.CreateDirectory(dir);
            string filelocation = dir + "\\" + cmbProduct.Text + " Batch Number " + txtBatch.Text + ".xls";
            Microsoft.Office.Interop.Excel.Application xlexcel;
            Microsoft.Office.Interop.Excel.Workbook xlWorkBook;
            Microsoft.Office.Interop.Excel.Sheets worksheet;
            object misValue = System.Reflection.Missing.Value;
            xlexcel = new Excel.Application();
            xlexcel.Visible = true;
            xlWorkBook = xlexcel.Workbooks.Add(misValue);
            worksheet = (Excel.Sheets)xlWorkBook.Worksheets;

            //PASS
            copyAlltoClipboard();
            var worksheet1 = (Excel.Worksheet)worksheet.Add(worksheet[1], Type.Missing, Type.Missing, Type.Missing);
            worksheet1.Name = "Pass Vaccine";
            for (int i = 1; i < dataGridView1.Columns.Count + 1; i++)
            {
                worksheet1.Cells[1, i] = dataGridView1.Columns[i - 1].HeaderCell.Value;
                Excel.Range crg = (Excel.Range)worksheet1.Cells[1, i];
                crg.Font.Bold = true;
            }
            Excel.Range CR = (Excel.Range)worksheet1.Cells[2, 1];
            CR.Select();
            worksheet1.PasteSpecial(CR, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, true);
            xlexcel.Columns.AutoFit();

            //FAIL
            copyAlltoClipboard1();
            var worksheet2 = (Excel.Worksheet)worksheet.Add(worksheet[2], Type.Missing, Type.Missing, Type.Missing);
            worksheet2.Name = "Fail Vaccine";
            for (int i = 1; i < dataGridView2.Columns.Count + 1; i++)
            {
                worksheet2.Cells[1, i] = dataGridView2.Columns[i - 1].HeaderCell.Value;
                Excel.Range crg = (Excel.Range)worksheet2.Cells[1, i];
                crg.Font.Bold = true;
            }
            Excel.Range CR1 = (Excel.Range)worksheet2.Cells[2, 1];
            CR1.Select();
            worksheet2.PasteSpecial(CR1, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, true);
            xlexcel.Columns.AutoFit();

            //SAMPLE
            copyAlltoClipboard2();
            var worksheet3 = (Excel.Worksheet)worksheet.Add(worksheet[3], Type.Missing, Type.Missing, Type.Missing);
            worksheet3.Name = "Sample Vaccine";
            for (int i = 1; i < dataGridView3.Columns.Count + 1; i++)
            {
                worksheet3.Cells[1, i] = dataGridView3.Columns[i - 1].HeaderCell.Value;
                Excel.Range crg = (Excel.Range)worksheet3.Cells[1, i];
                crg.Font.Bold = true;
            }
            Excel.Range CR2 = (Excel.Range)worksheet3.Cells[2, 1];
            CR2.Select();
            worksheet3.PasteSpecial(CR1, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, true);
            xlexcel.Columns.AutoFit();


            xlWorkBook.SaveAs(filelocation, Excel.XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue, Excel.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);
            xlWorkBook.Close(true, misValue, misValue);
            xlexcel.Quit();

            releaseObject(worksheet);
            releaseObject(xlWorkBook);
            releaseObject(xlexcel);

            MessageBox.Show("Excel file created " + filelocation);
        }

        private void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
                MessageBox.Show("Exception Occured while releasing object " + ex.ToString());
            }
            finally
            {
                GC.Collect();
            }
        }
    }
}
