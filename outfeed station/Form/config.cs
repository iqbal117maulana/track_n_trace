﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace Outfeed_Station
{
    public partial class config : Form
    {
        Outfeed blis;
        dbaccess db;
        public config(Outfeed bl, int stat, string admin)
        {
            blis = bl;
            InitializeComponent();
            getConfig();
            if (bl != null)
            {
                db = new dbaccess();
                db.adminid = bl.lblAdmin.Text;
                db.from = "Outfeed Station";
            }
        }

        void getConfig()
        {
            sqlitecs sql = new sqlitecs();
            try
            {
                txtIpCamera.Text = sql.config["ipCamera"];
                txtPortCamera.Text = sql.config["portCamera"];
                txtIpPrinter.Text = sql.config["ipprinter"];
                txtPortPrinter.Text = sql.config["portprinter"];
                txtPortServer.Text = sql.config["portserver"];
                txtIpServer.Text = sql.config["ipServer"];
                txtStationName.Text = sql.config["Servername"];
                txtIpDB.Text = sql.config["ipDB"];
                txtPortDb.Text = sql.config["portDB"];
                txtTimeoutDb.Text = sql.config["timeout"];
                txtIpCamera2.Text = sql.config["ipCamera2"];
                txtPortCamera2.Text = sql.config["portCamera2"];
                txtIpCamera3.Text = sql.config["ipCamera3"];
                txtPortCamera3.Text = sql.config["portCamera3"];
                txtIpCamera4.Text = sql.config["ipCamera4"];
                txtPortCamera4.Text = sql.config["portCamera4"];
                txtNamaDB.Text = sql.config["namaDB"];


                txtAudit.Text = sql.config["audit"];
                txtSend.Text = sql.config["qtysend"];
                txtGenerate.Text = sql.config["qtygenerate"];
                txtDelay.Text = sql.config["delay"];
                txtIpPlc.Text = sql.config["IpPLC"];
                txtPortPlc.Text = sql.config["PortPLC"];
            }
            catch (KeyNotFoundException k)
            {
                new Confirm("Error");
                sql.simpanLog("Data Tidak Ditemukan : " + k.ToString());
            }
        }

        bool cekBesarGenerate(string generate, string send)
        {
            int generatebalik = int.Parse(generate);
            int sendbalik = int.Parse(send);
            if (generatebalik < sendbalik)
                return true;
            return false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Confirm cf = new Confirm("Are you sure? ", "Confirmation");
            bool hasilDialog = cf.conf();
            if (hasilDialog)
            {
                sqlitecs sql = new sqlitecs();
                Dictionary<string, string> field = new Dictionary<string, string>();
                if (cekKosong())
                {
                    if (validasi())
                    {
                        if (cekBesarGenerate(txtSend.Text, txtGenerate.Text))
                        {
                            field.Add("ipCamera", txtIpCamera.Text);
                            field.Add("portCamera", "" + int.Parse(txtPortCamera.Text));
                            field.Add("ipprinter", txtIpPrinter.Text);
                            field.Add("portPrinter", "" + int.Parse(txtPortPrinter.Text));
                            field.Add("portserver", "" + int.Parse(txtPortServer.Text));
                            field.Add("ipServer", txtIpServer.Text);
                            field.Add("Servername", txtStationName.Text);
                            field.Add("ipDB", txtIpDB.Text);
                            field.Add("portDB", "" + int.Parse(txtPortDb.Text));
                            field.Add("timeout", txtTimeoutDb.Text);
                            field.Add("ipCamera2", txtIpCamera2.Text);
                            field.Add("portCamera2", "" + int.Parse(txtPortCamera2.Text));
                            field.Add("ipCamera3", txtIpCamera3.Text);
                            field.Add("portCamera3", "" + int.Parse(txtPortCamera3.Text));
                            field.Add("ipCamera4", txtIpCamera4.Text);
                            field.Add("portCamera4", "" + int.Parse(txtPortCamera4.Text));
                            field.Add("namadb", txtNamaDB.Text);
                            field.Add("audit", txtAudit.Text);
                            field.Add("qtysend", txtSend.Text);
                            field.Add("qtygenerate", txtGenerate.Text);
                            field.Add("delay", txtDelay.Text);
                            field.Add("IpPLC", txtIpPlc.Text);
                            field.Add("PortPLC", txtPortPlc.Text);
                            sql.update(field, "config", "");
                            cf = new Confirm("Configuration is successfully saved", "Information", MessageBoxButtons.OK);
                            if (blis != null)
                            {
                                blis.refresh();
                                db.eventname = "Config success saved";
                                db.systemlog();
                            }
                            this.Dispose();
                        }
                        else
                        {
                            cf = new Confirm("Qty generate must be more than Qty send", "Information", MessageBoxButtons.OK);
                        }
                    }
                    else
                    {
                        cf = new Confirm("Configuration wrong format", "Information",MessageBoxButtons.OK);
                    }
                }
                else
                {
                    cf = new Confirm("Configuration can not be empty", "Information", MessageBoxButtons.OK);
                }
            }
        }

        private bool cekKosong()
        {
            if (!cek(txtIpCamera.Text))
                return false;
            if (!cek(txtIpCamera2.Text))
                return false;
            if (!cek(txtIpCamera3.Text))
                return false;
            if (!cek(txtIpCamera4.Text))
                return false;
            if (!cek(txtIpDB.Text))
                return false;
            if (!cek(txtIpPrinter.Text))
                return false;
            if (!cek(txtIpServer.Text))
                return false;
            if (!cek(txtPortCamera.Text))
                return false;
            if (!cek(txtPortCamera2.Text))
                return false;
            if (!cek(txtPortCamera3.Text))
                return false;
            if (!cek(txtPortCamera4.Text))
                return false;
            if (!cek(txtPortPrinter.Text))
                return false;
            if (!cek(txtPortServer.Text))
                return false;
            return true;
        }

        private bool cek(string p)
        {
            if (p.Length > 0) return true; else return false;
        }

        private bool validasi()
        {
            if(!cekIP(txtIpCamera.Text))
                return false; 
            if (!cekIP(txtIpCamera2.Text))
                return false;
            if (!cekIP(txtIpCamera3.Text))
                return false;
            if (!cekIP(txtIpCamera4.Text))
                return false;
            if (!cekIP(txtIpDB.Text))
                return false;
            if (!cekIP(txtIpPrinter.Text))
                return false;
            if (!cekIP(txtIpServer.Text))
                return false;
            if (!cekAngka(txtPortCamera.Text))
                return false;
            if (!cekAngka(txtPortCamera2.Text))
                return false;
            if (!cekAngka(txtPortCamera3.Text))
                return false;
            if (!cekAngka(txtPortCamera4.Text))
                return false;
            if (!cekAngka(txtPortPrinter.Text))
                return false;
            if (!cekAngka(txtPortServer.Text))
                return false;
            return true;
            
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {

        }

        bool cekIP(string data)
        {
            if (data.Length == 0)
                return true;

            int errorCounter = Regex.Matches(data, @"[a-zA-Z]").Count;
            if (errorCounter == 0)
            {
                string[] dapat = data.Split('.');
                if (dapat[3].Equals("0"))
                {
                    new Confirm("Error : the fourth digit of ip cannot be 0 "+data);
                    return false;
                }
                if (dapat.Length != 4)
                {
                    new Confirm("Error : IP must 4 point "+data);
                    return false;
                }
                bool kosong = false;
                if (dapat[0].Equals("000"))
                    kosong = true;
                for (int i = 0; i < dapat.Length; i++)
                {
                    //if (dapat[i] == "" || dapat[i].Length <= 0)
                    //{
                    //    new Confirm("Error : IP must 4 point");
                    //    return false;
                    //}

                    if (dapat[i].Equals("00"))
                    {
                        new Confirm("Error : IP cannot fill 00 "+data);
                        return false;
                    }

                    bool rege = Regex.Match(dapat[i], @"^[0-9]+$").Success;
                    if (!rege)
                    {
                        new Confirm("Error : IP cannot contain alphabeth and symbols " + data);
                        return false;
                    }
                    if (dapat[i].Length == 0)
                    {
                        new Confirm("Error : Cannot empty "+data);
                        return false;
                    }
                    int temp = int.Parse(dapat[i]);
                    if (temp >= 255)
                    {
                        new Confirm("Error : IP must be less then 255 "+data);
                        return false;
                    }
                    if (temp == 0 && kosong)
                    {
                        new Confirm("Error : First digit of ip cannot be 0 "+data);
                        return false;
                    }
                    if (dapat[i].Equals("000") && !kosong)
                    {
                        new Confirm("Error : IP cannot fill 000 "+data);
                        return false;
                    }
                    if (dapat[i][0].Equals('0') && temp != 0 && dapat[i].Length > 1)
                    {
                        new Confirm("Error : Ip cannot fill '0' "+data);
                        return false;
                    }
                }
                return true;
            }
            else
            {
                new Confirm("Error : IP cannot contain alphabeth " + data);
                return false;
            }
            return true;
        }

        bool cekAngka(string data)
        {
            if (data.Length == 0)
                return true;
            bool rege = Regex.Match(data, @"^[0-9]+$").Success;
            if (!rege)
            {
                new Confirm("Error : IP cannot contain alphabeth and symbols " + data);
                return false;
            }
            return true;
        }
        
    }
}
