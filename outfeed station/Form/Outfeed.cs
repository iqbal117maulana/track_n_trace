﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Net.Sockets;
using System.Net;

namespace Outfeed_Station
{
    public partial class Outfeed : Form
    {
        List<string> dataBlister = new List<string>();
        sqlitecs sqlite;
        generateCode Code;
        dbaccess db;
        Control ms;
        bool start = false;
        bool startprinting = false;
        bool audit = false;
        config cfg;
        Sample smp;
        Decomission dc;

        Dashboard dbss;
        string parameter;
        public ListBatchNumber lbn;
        string[] linenumber;

        //tambahan disable close button
        private const int CP_NOCLOSE_BUTTON = 0x200;
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams myCp = base.CreateParams;
                myCp.ClassStyle = myCp.ClassStyle | CP_NOCLOSE_BUTTON;
                return myCp;
            }
        }
        //end of disable close button

        public Outfeed(Dashboard dbs, string adminid, string[] line, string param, Control ctrl)
        {
            InitializeComponent();
            this.Text = this.Text + " V " + System.Windows.Forms.Application.ProductVersion;
            sqlite = new sqlitecs();
            Code = new generateCode();
            db = new dbaccess();
            ms = ctrl;
            ms.sqlite = sqlite;
            ms.bl = this;
            db.adminid = adminid;
            ms.db = db;
            ms.Code = Code;
            ms.adminid = adminid;
            ms.linename = line[0];
            ms.linenumber = line[1];
            ms.startStation();
            labelLine.Text = line[1];
            ms.delay();

            dbss = dbs;
            parameter = param;
            linenumber = line;
            labelBatch.Text = "";
            labelProduct.Text = "";
            cekDevice();
            cekCamera();
            cekDB();

        }
        public void close(string ti)
        {
            try
            {
                if (InvokeRequired)
                {
                    this.Invoke(new Action<string>(close), new object[] { ti });
                    return;
                }
                if (ms.srv.isRunning)
                {
                    ms.srv.closeServer("");
                }
                if(!ms.srv2.alredyOpened)
                {
                    ms.srv2.StreamClose();
                }
                Application.Exit();
                Environment.Exit(Environment.ExitCode);
            }
            catch (Exception ex)
            {
            }
        }

        public void close2(string ti)
        {
            try
            {
                if (InvokeRequired)
                {
                    this.Invoke(new Action<string>(close), new object[] { ti });
                    return;
                }
                if (ms.srv.isRunning)
                {
                    ms.srv.closeServer("");
                }
            }
            catch (Exception ex_a)
            {
            }
        }

        private void Outfeed_FormClosed(object sender, FormClosedEventArgs e)
        {
            close("");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Confirm cf = new Confirm("Are you sure? ", "Confirmation");
            bool hasilDialog = cf.conf();
            if (hasilDialog)
            {
                this.Dispose();
                Login lg = new Login();
                lg.Show();
            }
        }

        private void cmbBatch_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            config cfg = new config(this, 0, "");
            cfg.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }

        private void Outfeed_Load(object sender, EventArgs e)
        {
            timer1.Start();
            lblTime.Text = DateTime.Now.ToLongTimeString();
            lblDate.Text = DateTime.Now.ToShortDateString();
        }

        private void btnAudit_Click(object sender, EventArgs e)
        {
            Confirm cf = new Confirm("Are you sure? ", "Confirmation");
            bool hasilDialog = cf.conf();
            if (hasilDialog)
            {
                //if (cmbBatch.SelectedIndex > -1)
                //{
                    print pr = new print();
                    if (pr.cekKoneksi())
                    {
                        pr.disconect();
                        ms.batch = txtBatchNumber.Text;
                        ms.clearBuffer();
                        ms.startAudit();
                        audit = true;
                        ms.SendToPrinter(ms.getDataSendToPrinter(int.Parse(sqlite.config["audit"])));
                        ms.SetDataGridSend("0", "");
                        //Confirm cf2 = new Confirm("Pre-Check Success?", "Confirmation");
                        //bool hasilDialog2 = cf2.conf();
                        //if (hasilDialog2)
                        //{
                        //    btnStart.Enabled = true;
                        //}
                    }
                    else
                    {
                        new Confirm("Printer not connected!", "Information", MessageBoxButtons.OK);
                    }
                //}
                //else
                //{
                //    new Confirm("Batchnumber not recognize!", "Information", MessageBoxButtons.OK);
                //}
            }
        }

        public bool cekDevice()
        {
            print pr = new print();
            if (pr.cekKoneksi())
            {
                lblPrinter.Text = "Online";
                onPrinter.Visible = true;
                offPrinter.Visible = false;
                return true;
            }
            else
            {
                lblPrinter.Text = "Offline";
                onPrinter.Visible = false;
                offPrinter.Visible = true;
            }
            return false;
        }

        private bool Is_FirstConnect = false;
        private bool cekPLCConnect()
        {
            if (!Is_FirstConnect)
            {
                Is_FirstConnect = ms.checkPLCConnected();
                return Is_FirstConnect;
            }
            else
                return true;
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            Confirm cf = new Confirm("Are you sure? ", "Confirmation");
            bool hasilDialog = cf.conf();
            if (hasilDialog)
            {
                if (!start)
                {
                    if (cekCamera())
                    {
                        if (cekDevice())
                        {
                            if (cekPLCConnect())
                            {
                                //if (cmbBatch.SelectedIndex > -1)
                                //{

                                    ms.batch = txtBatchNumber.Text;
                                    ms.clearBuffer();
                                    ms.clearDataReceive();
                                    ms.sendStartPrinter();
                                    ms.disable();
                                    btnAudit.Enabled = false;
                                    start = true;
                                    //pictureBox1.Enabled = false;
                                    //cmbBatch.Enabled = false;
                                    txtBatchNumber.Enabled = false;
                                    btnStart.Text = "Stop";
                                    ms.SetDataGridSend("0", "");
                                    btnStart.Image = global::Outfeed_Station.Properties.Resources.stop;
                                //}
                                //else

                                //    new Confirm("Batchnumber not recognition", "Information", MessageBoxButtons.OK);
                            }
                            else
                            {
                                new Confirm("PLC not ready!", "Information", MessageBoxButtons.OK);
                            }
                        }
                        else
                        {
                            new Confirm("Printer not ready!", "Information", MessageBoxButtons.OK);
                        }
                    }
                }
                else
                {
                    //pictureBox1.Enabled = true;
                    btnStart.Image = global::Outfeed_Station.Properties.Resources.start;
                    ms.Stop();
                    btnAudit.Enabled = true;
                    start = false;
                    ms.enable();
                }
            }
        }

        public bool cekCamera()
        {
            if (ms.PingHost(sqlite.config["ipCamera"]))
            {
                lblCamera.Text = "Online";
                onCamera1.Visible = true;
                offCamera1.Visible = false;

                if (ms.PingHost(sqlite.config["ipCamera2"]))
                {
                    lblCamera2.Text = "Online";
                    onCamera2.Visible = true;
                    offCamera2.Visible = false;

                    if (ms.PingHost(sqlite.config["ipCamera3"]))
                    {
                        lblCamera3.Text = "Online";
                        onCamera3.Visible = true;
                        offCamera3.Visible = false;

                        if (ms.PingHost(sqlite.config["ipCamera4"]))
                        {
                            lblCamera4.Text = "Online";
                            onCamera4.Visible = true;
                            offCamera4.Visible = false;
                            return true;
                        }
                        else
                        {
                            lblCamera4.Text = "Offline";
                            onCamera4.Visible = false;
                            offCamera4.Visible = true;
                            new Confirm("Camera 4 not connected");
                        }
                    }
                    else
                    {
                        lblCamera3.Text = "Offline";
                        onCamera3.Visible = false;
                        offCamera3.Visible = true;
                        new Confirm("Camera 3 not connected");
                    }
                }
                else
                {
                    lblCamera2.Text = "Offline";
                    onCamera2.Visible = false;
                    offCamera2.Visible = true;
                    new Confirm("Camera 2 not connected");
                }
            }
            else
            {
                lblCamera.Text = "Offline";
                onCamera1.Visible = false;
                offCamera1.Visible = true;
                new Confirm("Camera 1 not connected");
            }
            return false;
        }


        private void btnConfig_Click(object sender, EventArgs e)
        {
            config cfg = new config(this, 0, "");
            cfg.ShowDialog();
        }

        private void btnLogout_Click(object sender, EventArgs e)
        {
            Confirm cf = new Confirm("Are you sure? ", "Confirmation");
            bool result = cf.conf();
            if (result)
            {
                
                if (db.Connect())
                {
                    db.eventname = "Logout outfeed station ";
                    db.systemlog();
                    db.closeConnection();
                }
                close2("");
                Application.Exit();
                //Environment.Exit(Environment.ExitCode);
                //db.eventname = "Logout";
                //db.systemlog();
                //this.Dispose();
                //Login lg = new Login();
                //lg.Show();
            }
        }

        private void btnDeco_Click(object sender, EventArgs e)
        {
            if (dc == null)
                dc = new Decomission(lblAdmin.Text,ms.linenumber);
            dc.ShowDialog();
        }

        private void stopPrinting()
        {
            ms.start = false;
            //      ms.refresh();
            ms.enable();
            ms.delayTimer.Stop();
            ms.cekQty();
        }

        private void label15_Click(object sender, EventArgs e)
        {

        }

        private void lblPrinter1_Click(object sender, EventArgs e)
        {

        }

        private void dgvSend_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            lblQtySend.Text = "" + dgvSend.Rows.Count;
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            if (smp == null)
                smp = new Sample(lblAdmin.Text);
            smp.ShowDialog();
        }

        private void label7_Click(object sender, EventArgs e)
        {
            ms.test();
        }

        private void label18_Click(object sender, EventArgs e)
        {
            ms.test();
        }


        public void refresh()
        {
            sqlite = new sqlitecs();
            db = new dbaccess();
            ms.sqlite = new sqlitecs();
            ms.db = new dbaccess();
        }

        private void dgvReceive_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
        }

        private void Outfeed_Click(object sender, EventArgs e)
        {
            dgvReceive.ClearSelection();
        }

        private void pictureBox1_DoubleClick(object sender, EventArgs e)
        {
            close2("");
            dbss.Show();
            this.Hide();
        }

        private void pictureBox1_MouseEnter(object sender, EventArgs e)
        {
            this.pictureBox1.BackColor = ColorTranslator.FromHtml("#00505E");
        }

        private void pictureBox1_MouseLeave(object sender, EventArgs e)
        {
            this.pictureBox1.BackColor = ColorTranslator.FromHtml("#002F36");
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            cekCamera();
            cekDevice();
            cekDB();
        }

        public bool cekDB()
        {
            if (db.Connect())
            {
                lblDb.Text = "Online";
                onDb.Visible = true;
                offDb.Visible = false;
                ms.StopServer();
                ms.StartServer2();
                db.closeConnection();
                return true;
            }
            else
            {
                lblDb.Text = "Offline";
                onDb.Visible = false;
                offDb.Visible = true;
                ms.StopServer();
                return false;
            }
        }

        private void txtBatchNumber_Click(object sender, EventArgs e)
        {
            lbn = new ListBatchNumber(null, linenumber[1], parameter, "");
            lbn.ShowDialog();
            txtBatchNumber.Text = lbn.batch;
            int[] cnt = ms.getCount(lbn.batch);
            //lblQuantityGood.Text = cnt[0].ToString();
            //lblQuantityReject.Text = cnt[1].ToString();
            lblQuantityReceiveAll.Text = cnt[0].ToString();
            btnAudit.Focus();
            if (txtBatchNumber.Text.Length > 0)
            {
                btnAudit.Enabled = true;
                btnStart.Enabled = true;
                btnDeco.Enabled = true;
            }
            else
            {
                btnAudit.Enabled = false;
                btnStart.Enabled = false;
                btnDeco.Enabled = false;
            }
        }

        private void button1_Click_2(object sender, EventArgs e)
        {
            Log lg = ms.lg;
            lg.Show();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            lblTime.Text = DateTime.Now.ToLongTimeString();
            timer1.Start();
        }

        private void button2_Click_2(object sender, EventArgs e)
        {
            if (db.decommToBlister(ms.batch))
            {
                Confirm cf = new Confirm("Success decommission to blister!", "Information", MessageBoxButtons.OK);
            }
            else
            {
                Confirm cf = new Confirm("Fail decommission to blister!", "Information", MessageBoxButtons.OK);
            }
        }

    }
}
