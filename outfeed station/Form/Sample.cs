﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Outfeed_Station
{
    public partial class Sample : Form
    {
        dbaccess db;
        public Sample(string admin)
        {
            InitializeComponent();
            db = new dbaccess();
            db.adminid = admin;
            db.from = "Outfeed Station";
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Enter)
            {
                sample();
            }
        }

        private void sample()
        {
            if (db.validasi(txtBlisterid.Text, 1))
            {
                if (db.cekInfeed(txtBlisterid.Text, 0))
                {
                    if (db.sampleData(txtBlisterid.Text))
                        txtLog.AppendText(txtBlisterid.Text + " set sample success\n");
                    else
                        txtLog.AppendText(txtBlisterid.Text + " set sample failed\n");
                }
                else
                    txtLog.AppendText(txtBlisterid.Text + " set sample failed\n");
               
            }
            else
                new Confirm("Outfeed ID is empty", "Information", MessageBoxButtons.OK);

            txtBlisterid.Text = "";
            txtBlisterid.Focus();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            sample();
        }

        private void Sample_Load(object sender, EventArgs e)
        {
            txtBlisterid.Text = "";
            txtBlisterid.Focus();
        }
    }
}
