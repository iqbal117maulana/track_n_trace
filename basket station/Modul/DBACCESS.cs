﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.Data;
using System.Diagnostics;

namespace Basket_Station.Modul
{
    class DBACCESS
    {
        public List<string> FIELD { get; set; }
        public Dictionary<string,string> PARAMETER { get; set; }
        public string WHERE { get; set; }
        public string TABLE { get; set; }
        public int num_rows { get; set; }
        public DataTable dataHeader;

        bool isOpen;
        sqlitecs sqlite;

        SqlConnection Connection;
        SqlDataReader DataReader;
        SqlDataAdapter dataadapter;
        SqlCommand Command;
        public string role;

        public DBACCESS()
        {
            num_rows = 0;
            Reset();
            sqlite = new sqlitecs();
        }

        public bool OpenConnection()
        {
            try
            {
                string connec = "Data Source=" + sqlite.config["ipDB"] + ";" +
                                "Initial Catalog=" + sqlite.config["namaDB"] + ";" +
                                "User id=" + sqlite.config["usernameDB"] + ";" +
                                //"Password=" + sqlite.config["passDB"] + ";" +
                                //"Password=docotronics;" +
                                "Password=Biofarma2020#;" + 
                                 "Connection Timeout=" + sqlite.config["timeout"] + ";";
                Connection = new SqlConnection(connec);
                try
                {
                    Connection.Open();
                    isOpen = true;
                }
                catch (Exception ex)
                {
                    isOpen = false;
                    new Confirm("Can not open connection to Database Server.", "Information", MessageBoxButtons.OK);
                    Util.Common.simpanLog("Error SQLSERVER : " + ex.ToString());
                   // config cfg = new config(null, 0, "");
                   // cfg.ShowDialog();
                }
            }
            catch (TimeoutException t)
            {
                isOpen= false;
                new Confirm("Can not open connection to Database Server.", "Information", MessageBoxButtons.OK);
                //config cfg = new config(null, 0, "");
                //cfg.Show();
            }
            return false;
        }

        public bool CloseConnection()
        {
            try
            {
                isOpen = false;
                Connection.Close();
                Reset();
                return true;
            }
            catch (Exception ex)
            {
                Reset();
                return false;
            }
        }

        public List<string[]> SELECT()
        {
            List<String[]> Results = new List<String[]>();
            try
            {
                string sql = "SELECT ";
                for (int j = 0; j < FIELD.Count; j++)
                {
                    sql += FIELD[j];
                    if (j + 1 < FIELD.Count)
                    {
                        sql += ",";
                        j++;
                    }
                }
                sql += " From " + TABLE;

                if (WHERE.Length > 0)
                    sql += " WHERE " + WHERE;

                Util.Common.simpanLog(sql);
                if (!isOpen)
                    OpenConnection();
                Command = new SqlCommand(sql, Connection);
                DataReader = Command.ExecuteReader();
                num_rows = 0;
                while (DataReader.Read())
                {
                    string[] data = new string[DataReader.FieldCount];
                    for (int u = 0; u < DataReader.FieldCount; u++)
                    {
                        data[u] = DataReader.GetValue(u).ToString();
                    }

                    Results.Add(data);
                    num_rows++;
                }
                dataHeader = new DataTable();
                for (int i = 0; i < DataReader.FieldCount; i++)
                {
                    dataHeader.Columns.Add(DataReader.GetName(i));
                }
                return Results;
            }
            catch (SqlException sq)
            {
                try
                {
                    Util.Common.simpanLog("Error  00100: " + sq.Message);
                    CloseConnection();
                }
                catch (Exception ex)
                {
                    Util.Common.simpanLog("Error  00100: " + ex.Message);
                    CloseConnection();
                }
            }
            catch (NullReferenceException nl)
            {
                Util.Common.simpanLog("Error  00100: " + nl.Message);
                CloseConnection();
            }
            catch (InvalidOperationException ide)
            {
                Util.Common.simpanLog("Error  00100: " + ide.Message);
                CloseConnection();
            }
            catch (Exception ex)
            {
                Util.Common.simpanLog("Error  00100: " + ex.Message);
                CloseConnection();
            }
            finally
            {
                try
                {
                    DataReader.Close();
                    Command.Dispose();
                    CloseConnection();
                }
                catch (SqlException sq)
                {
                    Util.Common.simpanLog("Error SQLSERVER : " + sq.ToString());
                    CloseConnection();
                }
                catch (NullReferenceException nl)
                {
                    Util.Common.simpanLog("Error SQLSERVER : " + nl.ToString());
                    CloseConnection();
                }
                catch (Exception ex)
                {
                    Util.Common.simpanLog("Error SQLSERVER : " + ex.ToString());
                    CloseConnection();
                }

            }
            Reset();
            return Results;
        }

        public bool UPDATE()
        {
            try
            {
                string sql = "UPDATE " + TABLE + " SET ";
                int i = 0;
                foreach (KeyValuePair<string, string> key in PARAMETER)
                {
                    if (key.Value.Equals("null"))
                    {
                        sql += key.Key + " = " + key.Value;
                    }
                    else
                    {
                        sql += key.Key + " = " + "'" + key.Value + "'";
                    }
                    if (i + 1 < PARAMETER.Count)
                    {
                        sql += ",";
                        i++;
                    }
                }

                if (WHERE.Length > 0)
                    sql += " WHERE " + WHERE;

                Util.Common.simpanLog(sql);
                if (!isOpen)
                    OpenConnection();
                Command = new SqlCommand(sql, Connection);
                Command.ExecuteNonQuery();
                return true;
            }
            catch (SqlException sq)
            {
                try
                {
                    Util.Common.simpanLog("Error SQl : " + sq.ToString());
                    Connection.Close();
                }
                catch (Exception ex)
                {
                    Util.Common.simpanLog("Error SQl : " + sq.ToString());
                    Connection.Close();
                }
            }
            catch (Exception ex)
            {
                Util.Common.simpanLog("Error SQl : " + ex.ToString());
                Connection.Close();
            }
            finally
            {
                Reset();
            }
            return false;
        }

        public void Reset()
        {
            FIELD = new List<string>();
            PARAMETER = new Dictionary<string, string>();
            WHERE = "";
            TABLE = "";
            dataHeader = new DataTable();
        }
    }
}
