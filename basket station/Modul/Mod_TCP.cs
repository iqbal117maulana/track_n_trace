﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.IO;
using System.Windows.Forms;
using System.Threading;

namespace Basket_Station
{
    class Mod_TCP
    {
        private static TcpClient Client;
        private static Byte[] Data = new Byte[256];
        private static Stream ClientStream;

        public string IPAddress="";
        public string port="";
        public string timeout="";
        public string statusFail;

        internal static bool IsConnected = false;

        internal bool checkConnecteion()
        {
            bool temp = OpenConnection();
            if (temp)
                CloseConnection();
            return temp;
        }

        internal bool OpenConnection()
        {
            if (checkParameter())
            {
                try
                {
                    Log("Try Open Connection IP "+IPAddress+" Port "+port);
                    Client = new TcpClient();
                   // IAsyncResult result = Client.BeginConnect(IPAddress, Int32.Parse(port), null, null);
                    //bool success = result.AsyncWaitHandle.WaitOne(Int32.Parse(timeout), true);
                    Client.Connect(IPAddress,int.Parse(port));
                    
                    if (Client.Connected)
                    {
                        ClientStream = Client.GetStream();
                        Log("Open Connection Success");
                        IsConnected = true;
                        return true;
                    }
                    else
                    {
                        
                        Log("Open Connection Failed [1] ");
                        IsConnected = false;
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    Log("Open Connection Failed "+ex.Message);
                    statusFail = ex.Message;
                }
            }
            else
            {
                Log("Configuration Parameter Not Good");
                statusFail = "Configuration Parameter Not Good";
                return false;
            }
            return false;
        }

        internal void CloseConnection()
        {
            ClientStream.Close();
            Client.Close();
            
        }

        internal bool CheckConnection()
        {
            return Client.Connected;
        }

        private bool checkParameter()
        {
            if (IPAddress.Length == 0 || port.Length == 0 || timeout.Length == 0)
            {
                return false;
            }
            return true;
        }

        internal string listener()
        {
            try
            {
                byte[] data = new Byte[256];
                if (IsConnected)
                {
                    if (ClientStream == null)
                    {
                        send("cek");
                    }
                    // ClientStream.ReadTimeout = 8000;
                    Int32 bytes = ClientStream.Read(data, 0, data.Length);
                    string responseData = System.Text.Encoding.ASCII.GetString(data, 0, bytes);
                    return responseData;
                }
                else
                {
                    OpenConnection();
                }
            }
            catch (Exception ex)
            {
                CloseConnection();
                IsConnected = false;
                if (OpenConnection())
                {
                    send("^ME");
                    send("^MB");
                    send("^SM TESTN");
                    send(dataPrint);


                }
                return "err "+ex.Message;
            }
            return "";
        }
        public string dataPrint;
        internal void send(string dataSend)
        {
            try
            {
                //wait printer connected 
                // jika sudah ke send 
                bool send = false;
                // count retry try to connect
                int count = 0;
                while (!send&& count < 5)
                {
                    if (IsConnected)
                    {
                        StreamWriter sw = new StreamWriter(ClientStream);
                        sw.WriteLine(dataSend);
                        sw.Flush();
                        send = true;
                    }
                    else
                    {
                        count++;
                        Thread.Sleep(2000);
                        Log("Try to reconnected "+count);
                        OpenConnection();
                    }
                }
            }
            catch (Exception ex)
            {
                CloseConnection();
                IsConnected = false;
                if (OpenConnection())
                {
                    send("^ME");
                    send("^MB");
                    send("^SM TESTN");
                    send(dataSend);
                }
            }
        }

        
        public void Log(String line)
        {
            DateTime now = DateTime.Now;
            string tahun = now.ToString("yyyy");
            string bulan = now.ToString("MM");
            string hari = now.ToString("dd");
            string dates = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            line = dates + "\t" + line;
            // cek file 
            bool cekfile = false;
            string namaFolder = "eventlog\\TCP";
            try
            {
                while (!cekfile)
                {
                    if (Directory.Exists(namaFolder))
                    {
                        if (Directory.Exists(namaFolder + @"\" + tahun))
                        {
                            if (Directory.Exists(namaFolder + @"\" + tahun + @"\" + bulan))
                            {

                                using (StreamWriter outputFile = new StreamWriter(namaFolder + @"\" + tahun + @"\" + bulan + @"\" + hari + ".txt", true))
                                {
                                    outputFile.WriteLine(line);
                                }
                                cekfile = true;
                            }
                            else
                            {

                                Directory.CreateDirectory(namaFolder + @"\" + tahun + @"\" + bulan);
                            }
                        }
                        else
                        {
                            Directory.CreateDirectory(namaFolder + @"\" + tahun);
                        }
                    }
                    else
                    {

                        Directory.CreateDirectory(namaFolder);
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }
   
    }
}
