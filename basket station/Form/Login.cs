﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;

namespace Basket_Station
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
            this.Text = this.Text + " V " + System.Windows.Forms.Application.ProductVersion;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            do_login();
        }

        private void mp_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Close();
        }

        public static string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            throw new Exception("Local IP address not found.");
        }

        private void Login_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void txtUsername_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (txtUsername.Text.Length > 0)
                {
                    this.ActiveControl = txtPassword;
                }
                else
                {
                    new Confirm("Username cannot empty", "Information", MessageBoxButtons.OK);
                    txtUsername.Focus();
                }
            }
        }

        private void txtPassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (txtUsername.Text.Length > 0)
                {
                    if (txtPassword.Text.Length > 0)
                    {
                        do_login();
                    }
                    else
                    {
                        new Confirm("Password cannot empty", "Information", MessageBoxButtons.OK);
                        txtPassword.Focus();
                    }
                }
                else
                {
                    new Confirm("Username cannot empty", "Information", MessageBoxButtons.OK);
                    txtUsername.Focus();
                }
            }
        }

        private void Login_Load(object sender, EventArgs e)
        {

        }

        private void do_login()
        {
            dbaccess db = new dbaccess();
            try
            { //cek username
                if (db.validasi(txtUsername.Text))
                {
                    //cek Password
                    if (db.validasi(txtUsername.Text, txtPassword.Text))
                    {
                        List<string[]> res = db.validasi(txtUsername.Text, txtPassword.Text, "0");
                        if (res != null)
                        {
                            string adminid = res[0][0];
                            if (db.cekPermision(adminid, "basket_station", "read"))
                            {
                                string[] lineBottle = db.cekLine("basketStationIp");
                                if (lineBottle.Length > 0)
                                {
                                    List<string[]> resu = db.getRole(adminid);
                                    Dashboard ds = new Dashboard(adminid, lineBottle, resu[0][2].ToString());
                                    ds.lblUserId.Text = resu[0][0].ToString();
                                    ds.lblRole.Text = resu[0][1].ToString();
                                    ds.Show();
                                    this.Hide();
                                }
                                else
                                {
                                    string ip = db.ipAddress;
                                    Confirm cf = new Confirm("IP Address " + ip + " Is Not Recognize,\n Contact Your Administrator", "Information", MessageBoxButtons.OK);
                                }
                            }
                            else
                            {
                                new Confirm("Dont have permission", "Information", MessageBoxButtons.OK);
                            }
                        }
                        else
                        {
                            new Confirm("User is inactive", "Information", 0);
                            db.simpanLog("Error 0010 : Username dan Password Salah !");
                        }
                    }
                    else
                    {
                        new Confirm("Invalid Password", "Information", MessageBoxButtons.OK);
                        db.simpanLog("Error 0010 : Username dan Password Salah !");
                        txtPassword.Focus();
                        txtPassword.Text = "";
                    }
                }
                else
                {
                    new Confirm("Invalid Username", "Information", MessageBoxButtons.OK);
                    db.simpanLog("Error 0010 : Username Salah !");
                    txtUsername.Focus();
                    clearText();
                }
            }
            catch (ArgumentException ae)
            {
                db.simpanLog("Error " + ae.ToString());
            }
        }
        
        private void clearText()
        {
            txtUsername.Text = "";
            txtPassword.Text = "";
            txtUsername.Focus();
        }

        private void buttonLogin_Click(object sender, EventArgs e)
        {
            do_login();
        }
     
    }
}
