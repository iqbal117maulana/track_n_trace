﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Basket_Station;
using System.Text.RegularExpressions;

namespace Basket_Station
{
    public partial class config : Form
    {
        ManualPacker bot;
        public config(ManualPacker bt)
        {
            InitializeComponent();
            bot = bt;
            getConfig();
        }

        void getConfig()
        {
            sqlitecs sql = new sqlitecs();
            try
            {
                txtIpCamera.Text = sql.config["ipCamera"];
                txtPortCamera.Text = sql.config["portCamera"];
                txtIpPrinter.Text = sql.config["ipprinter"];
                txtPortPrinter.Text = sql.config["portprinter"];
                txtPortServer.Text = sql.config["portserver"];
                txtIpServer.Text = sql.config["ipServer"];
                txtIpDB.Text = sql.config["ipDB"];
                txtPortDb.Text = sql.config["portDB"];
                txtDBName.Text = sql.config["namaDB"];
                txtTimeoutDb.Text = sql.config["timeout"];
                txtQtyFullBasket.Text = sql.config["qtyfullbasket"];
                txtCompId.Text = sql.config["compId"];
            }
            catch (KeyNotFoundException k)
            {
                sql.simpanLog("Data not found : " + k.ToString());
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Confirm cf = new Confirm("Are you sure? ", "Confirmation");
            bool hasilDialog = cf.conf();
            if (hasilDialog)
            {
                if (cekKosong())
                {
                    if (validate())
                    {

                        sqlitecs sql = new sqlitecs();
                        Dictionary<string, string> field = new Dictionary<string, string>();
                        field.Add("ipCamera", txtIpCamera.Text);
                        field.Add("portCamera", "" + int.Parse(txtPortCamera.Text));
                        field.Add("ipPrinter", txtIpPrinter.Text);
                        field.Add("portPrinter", "" + int.Parse(txtPortPrinter.Text));
                        field.Add("portserver", "" + int.Parse(txtPortServer.Text));
                        field.Add("ipServer", txtIpServer.Text);
                        field.Add("ipDB", txtIpDB.Text);
                        field.Add("portDB", "" + int.Parse(txtPortDb.Text));
                        field.Add("namaDB", "" + txtDBName.Text);
                        field.Add("timeout", txtTimeoutDb.Text);
                        field.Add("qtyfullbasket", txtQtyFullBasket.Text);
                        field.Add("compId", txtCompId.Text);
                        sql.update(field, "config", "");
                        Confirm cf1 = new Confirm("Configuration is successfully saved.", "Information", MessageBoxButtons.OK);
                        this.Dispose();
                    }
                    else
                    {
                        Confirm cf2 = new Confirm("Wrong format in one of the configuration field.", "Information", MessageBoxButtons.OK);
                    }
                }
                else
                {
                    Confirm cf4 = new Confirm("Configuration field can not be empty.", "Information", MessageBoxButtons.OK);
                }
            }
        }

        private bool cekKosong()
        {
            if (!cek(txtIpCamera.Text))
            {
                return false;
            }


            if (!cek(txtIpDB.Text))
            {
                return false;
            }

            if (!cek(txtIpDB.Text))
            {
                return false;
            }

            if (!cek(txtIpPrinter.Text))
            {
                return false;
            }

            if (!cek(txtIpServer.Text))
            {
                return false;
            }

            if (!cek(txtPortCamera.Text))
            {
                return false;
            }

            if (!cek(txtPortDb.Text))
            {
                return false;
            }

            if (!cek(txtPortPrinter.Text))
            {
                return false;
            }

            if (!cek(txtPortServer.Text))
            {
                return false;
            }
            if (!cek(txtTimeoutDb.Text))
            {
                return false;
            }
            return true;
        }

        private bool cek(string p)
        {
            if (p.Length > 0) return true; else return false;
        }
        string valid;

        bool cekIP(string data)
        {
            if (data.Length < 3 & data.Length > 0)
                return false;
            int errorCounter = Regex.Matches(data, @"[a-zA-Z]").Count;
            if (errorCounter == 0)
            {
                string[] dapat = data.Split('.');
                if (dapat.Length != 4)
                {
                    valid = "Error : IP must 4 point";
                    return false;
                }
                if (dapat[3].Equals("0"))
                {
                    valid = "Error : the fourth digit of ip cannot be 0";
                    return false;
                }

                bool kosong = false;
                if (dapat[0].Equals("000"))
                    kosong = true;
                for (int i = 0; i < dapat.Length; i++)
                {
                    //if (dapat[i] == "" || dapat[i].Length <= 0)
                    //{
                    //    new Confirm("Error : IP must 4 point");
                    //    return false;
                    //}
                    if (dapat[i][0].Equals("0"))
                        return false;
                    if (dapat[i].Equals("00"))
                    {
                        valid = "Error : IP cannot fill 00";
                        return false;
                    }

                    bool rege = Regex.Match(dapat[i], @"^[0-9]+$").Success;
                    if (!rege)
                    {
                        valid = "Error : IP cannot contain alphabeth and symbols " + data;
                        return false;
                    }
                    if (dapat[i].Length == 0)
                    {
                        valid = "Error : Cannot empty";
                        return false;
                    }
                    int temp = int.Parse(dapat[i]);
                    if (temp >= 255)
                    {
                        valid = "Error : IP must be less then 255";
                        return false;
                    }
                    if (temp == 0 && kosong)
                    {
                        valid = "Error : First digit of ip cannot be 0";
                        return false;
                    }
                    if (dapat[i].Equals("000") && !kosong)
                    {
                        valid = "Error : IP cannot fill 000";
                        return false;
                    }
                    if (dapat[i][0].Equals('0') && temp != 0 && dapat[i].Length > 1)
                    {
                        valid = "Error : Ip cannot fill '0'";
                        return false;
                    }
                }
                return true;
            }
            else
            {
                valid = "Error : IP cannot contain alphabeth " + errorCounter;
                return false;
            }
            return true;
        }

        public bool validasi(string data, int val)
        {
            //text ip val = 0
            if (val == 0)
                return cekIP(data);
            else if (val == 1)
            {
                //text tidak kosong val =1
                if (data.Length == 0)
                {
                    valid = "Data empty";
                    return false;
                }
            }
            else if (val == 2)
            {
                //text tidak ada angka
                int errorCounter = Regex.Matches(data, @"[a-zA-Z]").Count;
                if (errorCounter > 0)
                {
                    valid = "Data cannot contain numeric";
                    return false;
                }
            }
            else if (val == 3)
            {
                //text tidak ada angka

                if (data.Length > 6)
                    return false;
                bool rege = Regex.Match(data, @"^[0-9]+$").Success;
                if (!rege)
                {
                    valid = "Data cannot contain numeric";
                    return false;
                }
            }
            return true;

        }
        
        private bool validate()
        {
            string kesalahan = "Error input from ";
            bool kembali = true;
            if (!validasi(txtIpCamera.Text, 0))
            {
                kesalahan = kesalahan + txtIpCamera.Text+"\n";
                kembali = false;
            }

            if (!validasi(txtIpDB.Text, 0))
            {
                kesalahan = kesalahan + txtIpDB.Text + "\n";
                kembali = false;
            }

            

            if (!validasi(txtIpPrinter.Text, 0))
            {
                kesalahan = kesalahan + txtIpPrinter.Text + "\n";
                kembali = false;
            }

            if (!validasi(txtIpServer.Text, 0))
            {
                kesalahan = kesalahan + txtIpServer.Text + "\n";
                kembali = false;
            }

            if (!validasi(txtPortCamera.Text, 3))
            {
                kesalahan = kesalahan + txtPortCamera.Text + "\n";
                kembali = false;
            }

           

            if (!validasi(txtPortDb.Text, 3))
            {
                kesalahan = kesalahan + txtPortDb.Text + "\n";
                kembali = false;
            }

            
            if (!validasi(txtPortPrinter.Text, 3))
            {
                kesalahan = kesalahan + txtPortPrinter.Text + "\n";
                kembali = false;
            }

            if (!validasi(txtPortServer.Text, 3))
            {
                kesalahan = kesalahan + txtPortServer.Text + "\n";
                kembali = false;
            }

            if (!kembali)
            {
                Confirm cf = new Confirm(kesalahan, "Information", MessageBoxButtons.OK);
            }
                return kembali;
        }

        private void config_Load(object sender, EventArgs e)
        {

        }
    }
}
