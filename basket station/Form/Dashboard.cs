﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Net;
using System.Net.Sockets;

namespace Basket_Station
{
    public partial class Dashboard : Form
    {
        dbaccess db;
        public string[] linenumber;
        public string admin;
        public DataTable table = new DataTable();
        ListBatchNumber lbn;
        public string param;
        Control ctrl = new Control();
        string roleid;

        public Dashboard(string adminid, string[] line, string role_id)
        {
            InitializeComponent();
            btnReport.Enabled = false;
            this.Text = this.Text + " V " + System.Windows.Forms.Application.ProductVersion;
            db = new dbaccess();
            db.adminid = adminid;
            admin = adminid;
            db.from = "Blister Station";
            linenumber = line;
            roleid = role_id;
            lblCountBatchPentabio.Text = GetOpenBatch(linenumber[1]).Count.ToString();
        }

        private void Dashboard_Load(object sender, EventArgs e)
        {
            timer1.Start();
            lblTime.Text = DateTime.Now.ToLongTimeString();
            lblDate.Text = DateTime.Now.ToShortDateString();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            lblTime.Text = DateTime.Now.ToLongTimeString();
            timer1.Start();
        }

        private void Dashboard_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        //tambahan
        public List<string[]> GetOpenBatch(string line)
        {
            List<string> field = new List<string>();
            field.Add("batchNumber,productModelId");
            string where = "status = '1' and linepackagingid = '" + line + "'";
            return db.selectList(field, "[packaging_order]", where);
        }
        //end tambahan

        private void pnlBatchPentabio_MouseEnter(object sender, EventArgs e)
        {
            this.pnlBatchPentabio.BackColor = ColorTranslator.FromHtml("#cccccc");
        }

        private void pnlBatchPentabio_MouseLeave(object sender, EventArgs e)
        {
            this.pnlBatchPentabio.BackColor = ColorTranslator.FromHtml("#ffffff");
        }

        private void lblCountBatchPentabio_MouseEnter(object sender, EventArgs e)
        {
            this.pnlBatchPentabio.BackColor = ColorTranslator.FromHtml("#cccccc");
        }

        private void lblCountBatchPentabio_MouseLeave(object sender, EventArgs e)
        {
            this.pnlBatchPentabio.BackColor = ColorTranslator.FromHtml("#ffffff");
        }

        private void lblPentabio_MouseEnter(object sender, EventArgs e)
        {
            this.pnlBatchPentabio.BackColor = ColorTranslator.FromHtml("#cccccc");
        }

        private void lblPentabio_MouseLeave(object sender, EventArgs e)
        {
            this.pnlBatchPentabio.BackColor = ColorTranslator.FromHtml("#ffffff");
        }

        private void pbPentabio_MouseEnter(object sender, EventArgs e)
        {
            this.pnlBatchPentabio.BackColor = ColorTranslator.FromHtml("#cccccc");
        }

        private void pbPentabio_MouseLeave(object sender, EventArgs e)
        {
            this.pnlBatchPentabio.BackColor = ColorTranslator.FromHtml("#ffffff");
        }

        private void pnlBatchPentabio_DoubleClick(object sender, EventArgs e)
        {
            lbn = new ListBatchNumber(this, linenumber[1], "dashboard pentabio");
            lbn.ShowDialog();
        }

        private void lblCountBatchPentabio_DoubleClick(object sender, EventArgs e)
        {
            lbn = new ListBatchNumber(this, linenumber[1], "dashboard pentabio");
            lbn.ShowDialog();
        }

        private void lblPentabio_DoubleClick(object sender, EventArgs e)
        {
            lbn = new ListBatchNumber(this, linenumber[1], "dashboard pentabio");
            lbn.ShowDialog();
        }

        private void pbPentabio_DoubleClick(object sender, EventArgs e)
        {
            lbn = new ListBatchNumber(this, linenumber[1], "dashboard pentabio");
            lbn.ShowDialog();
        }

        private void btnPentabio_Click(object sender, EventArgs e)
        {
            ManualPacker bl = new ManualPacker(this, admin, linenumber, ctrl);
            bl.lblUserId.Text = lblUserId.Text;
            bl.lblRole.Text = lblRole.Text;
            bl.btnStart.Enabled = false;
            if (roleid == "1")
            {
                bl.btnConfig.Visible = true;
            }
            else
            {
                bl.btnConfig.Visible = false;
            }
            bl.Show();
            this.Hide();
        }

        private void btnFlubio_Click(object sender, EventArgs e)
        {
            Report rp = new Report(this, linenumber[1]);
            rp.lblUserId.Text = lblUserId.Text;
            rp.lblRole.Text = lblRole.Text;
            rp.Show();
            this.Hide();
        }
        
    }
}
