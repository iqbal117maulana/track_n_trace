﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Basket_Station
{
    public partial class Decomission : Form
    {
        dbaccess db;
        Model.Vaccine vaccine;
        Model.innerBox innerbox;
        Model.Basket basket;
        string linenumber;
        ManualPacker bskt;

        public Decomission(string line, ManualPacker mp)
        {
            InitializeComponent();
            db = new dbaccess();
            txtProduct.Focus();
            vaccine = new Model.Vaccine();
            innerbox = new Model.innerBox();
            basket = new Model.Basket();
            linenumber = line;
            SetBatchNumber();
            bskt = mp;
        }

        private void SetBatchNumber()
        {
            List<string> field = new List<string>();
            field.Add("batchNumber,productModelId");
            string where = "status = '1' and linepackagingid = '" + linenumber + "'";
            List<string[]> datacombo = db.selectList(field, "[packaging_order]", where);
            List<string> da = new List<string>();
            for (int i = 0; i < datacombo.Count; i++)
            {
                da.Add(datacombo[i][0]);
            }
            cmbBatch.DataSource = da;
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            
            if (e.KeyCode == Keys.Enter)
            {
                decomision();
            }
        }

        private void decomision()
        {
            vaccine = new Model.Vaccine();
            innerbox = new Model.innerBox();
            basket = new Model.Basket();
            txtProduct.Text = Util.Common.REMOVE_UNUSED_CHAR(txtProduct.Text);
            if (!Util.Common.CHECK_EMPTY(txtProduct))
            {
                // cek blister
                basket.BASKETID = txtProduct.Text;
                basket.BATCHNUMBER = cmbBatch.Text;

                if (basket.CHECK_BASKET())
                {
                    decommission_BASKET();
                }
                else
                {
                    log("Product " + txtProduct.Text + " Not Found");
                }
            }

            txtProduct.Text = "";
            txtProduct.Focus();
        }

        private void decommission_BASKET()
        {
            vaccine = new Model.Vaccine();
            basket = new Model.Basket();
            innerbox = new Model.innerBox();

            basket.BASKETID = txtProduct.Text;
            if (basket.CHECK_BASKET())
            {
                basket.BATCHNUMBER = cmbBatch.Text;
                if (basket.CHECK_BASKET())
                {
                    basket.ISREJECT = "0";
                    if (basket.CHECK_BASKET())
                    {
                        innerbox.BASKETID = txtProduct.Text;
                        innerbox.BATCHNUMBER = cmbBatch.Text;
                        if (innerbox.UPDATE_EMPTY_BASKETID())
                        {
                            if (basket.DECOMMISSION())
                            {
                                vaccine.BASKETID = txtProduct.Text;
                                vaccine.BATCHNUMBER = cmbBatch.Text;
                                if (vaccine.UPDATE_EMPTY_BASKET())
                                {
                                    db.eventname = "Decomission =" + txtProduct.Text + " success";
                                    db.systemlog();
                                    log(txtProduct.Text + " decommission success");
                                    DataTable dt = bskt.dataGridView2.DataSource as DataTable;
                                    //dt.Select("basket = '" + txtProduct.Text + "'").ToList().ForEach(x => x.Delete());
                                    //dt.AcceptChanges();
                                    //bskt.dataGridView2.DataSource = dt;
                                }
                                else
                                {
                                    db.eventname = "Decommission failed," + txtProduct.Text + " Something wrong with Database[Vaccine]";
                                    db.systemlog();
                                    log("Decommission failed," + txtProduct.Text + " Something wrong with Database");

                                }
                            }
                            else
                            {
                                db.eventname = "Decommission failed," + txtProduct.Text + " Something wrong with Database[Blister]";
                                db.systemlog();
                                log("Decommission failed," + txtProduct.Text + " Something wrong with Database");
                            }
                        }
                        else
                        {
                            db.eventname = "Decommission failed," + txtProduct.Text + " Something wrong with Database[InnerBox]";
                            db.systemlog();
                            log("Decommission failed," + txtProduct.Text + " Something wrong with Database");

                        }
                    }
                    else
                    {

                        db.eventname = "Decommission failed," + txtProduct.Text + " is not a good product";
                        db.systemlog();
                        log("Decommission failed," + txtProduct.Text + " is not a good product ");
                    }
                }
                else
                {
                    db.eventname = "Decommission failed," + txtProduct.Text + " Wrong BatchNumber";
                    db.systemlog();
                    log("Decommission failed," + txtProduct.Text + " Wrong BatchNumber");

                }
            }
            else
            {
                db.eventname = "Decomission =" + txtProduct.Text + " Not Found";
                db.systemlog();
                log("Decommission failed," + txtProduct.Text + " Not Found");
            }
        }
        
        private void button1_Click(object sender, EventArgs e)
        {
            decomision();
            
        }

        private void Decomission_Load(object sender, EventArgs e)
        {
            txtProduct.Text = "";
            txtProduct.Focus();
            
        }
        
        private void log(string line)
        {
            line = Util.Common.SetWithDate(line);
            txtLog.AppendText(line + "\n");
        }
    }
}
