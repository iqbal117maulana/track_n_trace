﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Drawing.Printing;
using System.IO;
using System.Management;
using System.Configuration;
using System.Net.Sockets;
using System.Threading;
using System.Text.RegularExpressions;

namespace Basket_Station
{
    public partial class ManualPacker : Form
    {
        private Dictionary<string, string[]> databasket;
        private Thread CameraAsyncTask;
        private Thread CameraDataAsyncTask;
        private int Interval = 10000; //ms
		bool nyala = true;
        public string linenumber;
        public string linename;
        dbaccess db;
        sqlitecs sqlite;
        string[] dataProd;
        string BatchNumber;
        string GTIN;
        string ExpiryDate;
        string ProductModelId;
        string ProductModelName;
        string adminid;
        List<string[]> datacombo;
        bool isStarted= false;
        string basketId = "";
        public System.Timers.Timer delaydevice;
        Control ms;
        int CountScannedData = 0; 
        int ordersItemIndex2;
        string LastDataPrinter;

        public Dashboard dbss;
        string parameter;
        public ListBatchNumber lbn;
        string[] linenumberrs;
        public bool isSAS;
        public string ProdModelId;

        //tambahan disable close button
        private const int CP_NOCLOSE_BUTTON = 0x200;
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams myCp = base.CreateParams;
                myCp.ClassStyle = myCp.ClassStyle | CP_NOCLOSE_BUTTON;
                return myCp;
            }
        }
        //end of disable close button

        public ManualPacker(Dashboard dbs, string admin, string[] line, Control ctrl)
        {
            InitializeComponent();
            this.Text = this.Text + " V " + System.Windows.Forms.Application.ProductVersion;
            databasket = new Dictionary<string, string[]>();
            db = new dbaccess();
            lblLine.Text = line[0];
            linenumber = line[1];
            //SetComboBoxBatchNumber();
            lblAdmin.Text = GetAdminName(adminid);
            adminid = admin;
            sqlite = new sqlitecs();
            ms = ctrl;
            //ms = new Control();
            ms.wr = this;
            ms.adminid = adminid;            
            ClearAllScanned();
            ms.GetComboBoxAllocation();
            //DelayCheckDevice();
            SetForm(false);
            //log("Port server: "+sqlite.config["portserver"]);

            CheckDevice();
            cekDB();
            dbss = dbs;
            linenumberrs = line;
            lblBatch.Text = "";
            lblProductName.Text = "";
        }

        void DelayCheckDevice()
        {
            delaydevice = new System.Timers.Timer();
            delaydevice.Interval = 1000;
            delaydevice.Elapsed += new System.Timers.ElapsedEventHandler(delaydevice_Elapsed);
            delaydevice.Start();
            CheckDevice();

        }

        void delaydevice_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            CheckDevice();
        }
     
        internal string GetAdminName(string admin)
        {
            List<string> field = new List<string>();
            field.Add("first_name   ");
            string where = "id ='" + admin + "'";
            List<string[]> ds = db.selectList(field, "[USERS]", where);
            if (db.num_rows > 0)
                return ds[0][0];
            else
                return "";
        }
      
        private void CheckDevice()
        {
            //if (ms.PingHost(sqlite.config["ipCamera"]))
            //{
            //    lblCamera.Text = "Online";
            //    onCamera1.Visible = true;
            //    offCamera1.Visible = false;
            //}
            //else
            //{
            //    lblCamera.Text = "Offline";
            //    onCamera1.Visible = false;
            //    offCamera1.Visible = true;
            //}

            if (ms.PingHost(sqlite.config["ipprinter"]))
            {
                lblPrinter.Text = "Online";
                onPrinter.Visible = true;
                offPrinter.Visible = false;
            }
            else
            {
                lblPrinter.Text = "Offline";
                onPrinter.Visible = false;
                offPrinter.Visible = true;
            }
        }

        /**
         * Check batch number
         */
        private void GetBatchnumber(string line)
        {
            BatchNumber = db.getBatchFromLine(line);
            if (BatchNumber.Length <= 0)
            {
                Confirm cf = new Confirm("No batch number in this packaging line.", "Information", MessageBoxButtons.OK);
            }
        }

        bool CheckData_DataGridview(string data, DataGridView dg)
        {
            for (int i = 0; i < dg.Rows.Count; i++)
            {
                if (data.Equals(dg.Rows[i].Cells[0].Value.ToString()))
                {
                    return true;
                }
            }
            return false;

        }

        string RemoveCharacter(string data)
        {
            data = data.Replace(" ", "");
            data = data.Replace("\0", "");
            data = data.Replace("\n", "");
            data = data.Replace("\t", "");
            return data;
        }

        /**
         * Update scanned data.
         */
      
        public void CheckScannedData(string DataCapture)
        {
            if (isStarted)
            {
                List<String> Data;
                Data = new List<String>();
                string[] datapecah = DataCapture.Split(',');
                string batch = txtBatchNumber.Text;
                int j = 0;
                for (int i = 0; i < datapecah.Length; i++)
                {
                    List<string> fieldInnerBox = new List<string>();
                    List<string[]> innerBoxList = new List<string[]>();
                    //if (isSAS == false)
                    //{
                        fieldInnerBox.Add("innerBoxId,innerBoxGsOneId");
                        string where = "(innerBoxId like '%" + datapecah[i] + "%' OR innerBoxGsOneId like '%" + datapecah[i] + "%') AND basketId IS NULL and isreject = 0 and batchnumber ='" + BatchNumber + "' and innerBoxGsOneId != ''";
                        innerBoxList = db.selectList(fieldInnerBox, "vaccine", where);
                    //}
                    //else
                    //{
                    //    fieldInnerBox.Add("innerBoxId,innerBoxGsOneId");
                    //    string where = "(innerBoxGsOneId like '%" + datapecah[i] + "%') AND basketId IS NULL and isreject = 0 and batchnumber ='" + BatchNumber + "' and innerBoxGsOneId != '' and innerBoxGsOneId is not null";
                    //    innerBoxList = db.selectList(fieldInnerBox, "vaccine", where);
                    //}

                    if (innerBoxList.Count > 0)
                    {   if (isSAS == false){
                            List<string> field = new List<string>();
                            field.Add("outbox_qty");
                            List<string[]> qty_outbox = new List<string[]>();
                            qty_outbox = db.selectList(field, "product_model", "product_model = '" + ProdModelId + "'");
                            int qty = int.Parse(qty_outbox[0][0]);
                            if (innerBoxList.Count == qty)                        
                            {
                                if (checkScannedToSqlite(innerBoxList[0][0], innerBoxList[0][1]))
                                {
                                //Data.Add(innerBoxList[0][0]);
                                    Data.Add(innerBoxList[0][1]);
                                    sqlite.insertScanned(RemoveCharacter(innerBoxList[0][0]), innerBoxList[0][1], db.getUnixString());
                                    j++;
                                    CountScannedData++;
                                    ms.log("Data " + datapecah[i] + " Success !" + Environment.NewLine);
                                }
                                else
                                {
                                    new Confirm("Data " + datapecah[i] + " Already Exist !", "Information", MessageBoxButtons.OK);
                                    ms.log("Data " + datapecah[i] + " Already Exist !" + Environment.NewLine);
                                }
                            }
                            else
                            {
                                new Confirm("Data " + datapecah[i] + " GS1 not Full !", "Information", MessageBoxButtons.OK);
                                ms.log("Data " + datapecah[i] + " GS1 not Full !" + Environment.NewLine);
                            }
                    
                        }else{
                            if (checkScannedToSqlite(innerBoxList[0][0], innerBoxList[0][1]))
                            {
                                //Data.Add(innerBoxList[0][0]);
                                Data.Add(innerBoxList[0][1]);
                                sqlite.insertScanned(RemoveCharacter(innerBoxList[0][0]), innerBoxList[0][1], db.getUnixString());
                                j++;
                                CountScannedData++;
                                ms.log("Data " + datapecah[i] + " Success !" + Environment.NewLine);
                            }
                            else
                            {
                                new Confirm("Data " + datapecah[i] + " Already Exist !", "Information", MessageBoxButtons.OK);
                                ms.log("Data " + datapecah[i] + " Already Exist !" + Environment.NewLine);
                            }

                        }
                    }
                    else
                    {
                        new Confirm("Data " + datapecah[i] + " not found !", "Information", MessageBoxButtons.OK);
                        ms.log("Data " + datapecah[i] + " not found !" + Environment.NewLine);
                    }
                }
                // Add sqlite here
                if (j > 0)
                {
                    sqlite.Begin();
                    UpdateScannedTable(Data);
                    CheckScannedForPrint();
                }
                else
                {
                    ms.log("No data inserted" + Environment.NewLine);
                }
            }
        }

        private bool checkCountdata(string p)
        {
            List<string> fieldInnerBox = new List<string>();
            fieldInnerBox.Add("innerBoxGsOneId");
            string where = "(innerBoxId like '%" + p + "%' OR innerBoxGsOneId like '%" + p + "%') AND basketId IS NULL and isreject = 0 and batchNumber ='" + BatchNumber + "'";
            List<string[]> innerBoxList = db.selectList(fieldInnerBox, "vaccine", where);
            if (innerBoxList.Count == 10)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool checkDataBatch(string p,string batch)
        {
            List<string> fieldInnerBox = new List<string>();
            fieldInnerBox.Add("innerBoxId,innerBoxGsOneId");
            string where = "(innerBoxId like '%" + p + "%' OR innerBoxGsOneId like '%" + p + "%') AND basketId IS NULL and isreject = 0 and batchNumber ='" + batch + "'";
            List<string[]> innerBoxList = db.selectList(fieldInnerBox, "vaccine", where);
            if (innerBoxList.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        private bool checkScannedToSqlite(string Innerbox, string InnerboxGsone)
        {
              //ambil data sqlite
            Innerbox = Innerbox.Replace("\0","");
            //List<string> field = new List<string>();
            //field.Add("innerboxid");
            //string where = "innerboxid = '" + Innerbox + "' or innerboxGsid = " + "'" + InnerboxGsone + "'";
            //List<string[]> ds = sqlite.select(field, "tmp_captured", where);
            sqlite.checkScanned(Innerbox, InnerboxGsone);
            if (sqlite.num_rows > 0)
            {
                return false;
            }
            return true;
        }

        private void CheckScannedForPrint()
        {
            // Jika data sudah 40 maka print
            int qtyfullbasket = int.Parse(sqlite.config["qtyfullbasket"]);
            int qtyScanned = int.Parse(dataGridView1.Rows.Count.ToString());
            //if (CountScannedData == qtyfullbasket)
            if (qtyScanned == qtyfullbasket)            
            {
                Print();
            }
        }

		/**
		 * Refresh the table with a new list of scanned cartons.
		 * @param Objects a new list of the cartons
		 */
        private void UpdateScannedTable(List<String> Objects)
        {
            if (InvokeRequired)
            {
                this.Invoke(new Action<List<String>>(UpdateScannedTable), new object[] { Objects });
                return;
            }

            if (!Objects[0].Equals("0"))
            {
                foreach (String Carton in Objects)
                {
                    string dates = DateTime.Now.ToString("HH:mm:ss");
                    dataGridView1.Rows.Insert(0, new object[] { Carton, dates });
                }
            }

            lblQtyScanned.Text = dataGridView1.Rows.Count.ToString();
        }

        /**
         * Simpan basket
         */
        private void SaveData()
        {
            List<string> field = new List<string>();
            List<string> datainner = new List<String>();

            //ambil data sqlite
            field.Add("innerboxid, innerboxGsid");
            List<string[]> ds = sqlite.select(field, "tmp_captured", "");
            if (sqlite.num_rows > 0)
            {
                foreach (string[] da in ds)
                {
                    Dictionary<string, string> fieldUpdate = new Dictionary<string, string>();
                    fieldUpdate.Add("basketid", basketId);
                    string where = "infeedinnerboxid ='" + da[0] + "'  and batchnumber ='" + BatchNumber + "'";
                    db.update(fieldUpdate, "innerbox", where);

                    datainner.Add(da[1]);
                    ms.log("Aggregate Inner Box ID '" + da[1] + "' to Basket ID '" + basketId + "'" + Environment.NewLine);

                    db.Movement("", da[0], basketId, "4", da[0]);
                    where = "innerBoxId ='" + da[0] + "' and batchnumber ='" + BatchNumber + "'";
                    db.update(fieldUpdate, "vaccine", where);
                }
                Dictionary<string, string> fieldStatus = new Dictionary<string, string>();
                fieldStatus.Add("rejectTime = NULL, isReject", "0");
                string where1 = "basketid ='" + basketId + "' AND batchnumber='" + BatchNumber + "'" ;
                db.update(fieldStatus, "basket", where1);

                //simpan data basket
                string[] data = datainner.ToArray();
                databasket.Add(txtBasket.Text, data);

                sqlite.delete("", "tmp_captured");
                dataGridView1.Rows.Clear();

                ms.log("Basket ID '" + basketId + "' Done" + Environment.NewLine);
            }
            else
            {
                new Confirm("No data captured", "Information", MessageBoxButtons.OK);
                ms.log("No data captured" + Environment.NewLine);
            }
        }

        /**
         * Tampil data 
         */
        private void ShowData()
        {
            //dataGridView2.Rows.Clear();
            foreach (string key in databasket.Keys)
            {
                string[] value12 = databasket[key];
                for (int i = 0; i < value12.Length; i++)
                {
                    dataGridView2.Rows.Add(value12[i], key);
                }
            }
            databasket = new Dictionary<string, string[]>();
            lblQtyHistory.Text = databasket.Count().ToString();
        }

        /**
         * Clear all scanned data.
         */
        private void ClearAllScanned()
        {
            // Clear DB
            sqlite.delete("", "tmp_captured");
            // Clear Gridview
            dataGridView1.Rows.Clear();
            CountScannedData = 0;
            lblQtyScanned.Text = "0";
        }

        /**
         * Clear a single selected data.
         */
        private void ClearSelectedScanned(String InnerBoxId)
        {
            // Clear DB
            sqlite.delete("InnerBoxId = '" + InnerBoxId + "' OR innerboxGSid = '" + InnerBoxId + "'", "tmp_captured");
            // Clear Gridview
           // dataGridView1.Rows.RemoveAt(dataGridView1.SelectedRows[0].Index);
        }

        /**
         * Generate a new code by checking the last code in the database table.
         */
        private int GenerateNewCode()
        {
            int seq = db.GetLastSequence(BatchNumber);
            seq = seq + 1;

            db.UpdateLastSequence(BatchNumber, seq);

            return seq;
        }

        /**
         * Print a new generated code.
         */
        private void Print() 
        {
            
            // Generate new code
            int seq = GenerateNewCode();
            string CurrentSN = Guid.NewGuid().ToString().Replace("-", "").Substring(0, 12).ToUpper();

            List<string> fields = new List<string>();
            fields.Add("innerBoxId");
            List<string[]> results = sqlite.select(fields, "tmp_captured", "");
            int Qty = results.Count;
            if (sqlite.num_rows > 0)
            {

                // Insert to database
                //basketId = "01" + GTIN + "10" + BatchNumber + "17" + ExpiryDate + "21" + CurrentSN;
                basketId = "01" + GTIN + "10" + BatchNumber + "17" + ExpiryDate + "21" +  ms.compId + CurrentSN.Remove(0,1);
                db.InsertNewBasketID(basketId,BatchNumber);
                ms.log("Generate new ID : " + basketId + Environment.NewLine);

                //Send to printer
                //LastDataPrinter = Printer.Print(GTIN, CurrentSN, BatchNumber, ExpiryDate, Qty.ToString(), cmbAllocation.Text, ProductModelName);
                LastDataPrinter = Printer.Print(GTIN, ms.compId + CurrentSN.Remove(0, 1), BatchNumber, ExpiryDate, Qty.ToString(), cmbAllocation.Text, ProductModelName);
                //LastDataPrinter = Printer.Print("89949570003458", "1" + CurrentSN.Remove(0, 1), "2611020", "211126", "50", "Indonesia", "Vaksin Covid19");
                ms.log("Send to printer : " + basketId + Environment.NewLine);
                txtBasket.Focus();
            }
            else
            {
                ms.log("Data captured empty!" + Environment.NewLine);
            }
        }

        public void log(string data)
        {
            string dates = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            string simpan = dates + "\t" + data + "\n";
            txtLog.AppendText(simpan + Environment.NewLine);
        }


        void SetForm(bool a)
        {
            buttonClearAll.Enabled = a;
            buttonClearSelected.Enabled = a;
            buttonPrint.Enabled = a;
            btnReprint.Enabled = a;
            lblQty.Enabled = a;
            lblqty2.Enabled = a;
            lblQtyScanned.Enabled = a;
            lblBasket.Enabled = a;
            lblQtyHistory.Enabled = a;
            txtBasket.Enabled = a;
            txtInfeedId.Enabled = a;
            lblCartoon.Enabled = a;
            lblQty3.Enabled = a;
            lblQtyHistoryAll.Enabled = a;
        }

        private void Match(string data)
        {
            if (data.Equals(basketId))
            {
                SaveData();
                ShowData();
                txtBasket.Text = "";
                txtInfeedId.Focus();
                ClearAllScanned();
                lblQtyScanned.Text = "0";
            }
            else
            {
                new Confirm("Data basket not match", "Information", MessageBoxButtons.OK);
                ms.log("Data basket not match" + Environment.NewLine);
            }
        }

        //public void SetComboBoxBatchNumber()
        //{
        //    List<string> field = new List<string>();
        //    field.Add("batchNumber,productModelId");
        //    string where = "status = '1' and linepackagingid = '" + linenumber + "'";
        //    datacombo = db.selectList(field, "[packaging_order]", where);
        //    List<string> da = new List<string>();
        //    for (int i = 0; i < datacombo.Count; i++)
        //    {
        //        da.Add(datacombo[i][0]);
        //    }
        //    cmbBatch.DataSource = da;
        //}

        private void ToggleBatch()
        {
            //if (CheckCamera())
            //{
                if (!isStarted)
                {
                    SetForm(true);
                    // Check if batch exist
                    BatchNumber = txtBatchNumber.Text;
                    string[] details = db.GetBatchNumberDetails(BatchNumber);

                    if (details != null)
                    {
                        sqlite.delete("", "tmp_captured");
                        ExpiryDate = details[1];
                        ProductModelId = details[2];
                        ProductModelName = details[3];
                        GTIN = details[4];
                        lblBatch.Text = BatchNumber;
                        lblProductName.Text = ProductModelName;
                        isStarted = true;
                        btnStart.Text = "Stop";
                        btnStart.Image = global::Basket_Station.Properties.Resources.stop;
                        txtInfeedId.Focus();
                        //cmbBatch.Enabled = false;
                        txtBatchNumber.Enabled = false;
                        pictureBox1.Enabled = false;
                        btnIndicator.Enabled = false;
                        btnDeco.Enabled = false;
                        btnConfig.Enabled = false;
                        btnLogout.Enabled = false;
                    }
                    else
                    {
                        Confirm cf = new Confirm("Batch number is not recognized.", "Information", MessageBoxButtons.OK);
                        //SetComboBoxBatchNumber();
                    }
                }
                else
                {
                    //cmbBatch.Enabled = true;
                    txtBatchNumber.Enabled = true;
                    pictureBox1.Enabled = true;
                    btnIndicator.Enabled = true;
                    btnDeco.Enabled = true;
                    btnConfig.Enabled = true;
                    btnLogout.Enabled = true;
                    btnStart.Text = "Start";
                    btnStart.Image = global::Basket_Station.Properties.Resources.start;
                    lblBatch.Text = "-";
                    lblProductName.Text = "-";
                    //ms.StopServer();
                    isStarted = false;
                    SetForm(false);
                }
            //}
        }

        private bool CheckCamera()
        {
            if (ms.PingHost(sqlite.config["ipCamera"]))
           {
                lblCamera.Text = "Online";
                onCamera1.Visible = true;
                offCamera1.Visible = false;
                return true;
            }
            else
            {
                lblCamera.Text = "Offline";
                onCamera1.Visible = false;
                offCamera1.Visible = true;
                new Confirm("Camera 1 not connected");
                return true;
            }
        }

        void close(string ti)
        {
            if (InvokeRequired)
            {
                this.Invoke(new Action<string>(close), new object[] { ti });
                return;
            }
            //ms.srv.closeServer("");
            Application.Exit();
        }

        /**
        * Form control.
        */
        private void ShowDecomForm()
        {
            Decomission dc = new Decomission(linenumber, this);
            dc.ShowDialog();
        }

        private void buttonPrint_Click(object sender, EventArgs e)
        {
            Confirm cf = new Confirm("Are you sure? ", "Confirmation");
            bool result = cf.conf() ;
            if (result)
            {
                Print();
            }
        }

        private void ManualPacker_FormClosed(object sender, FormClosedEventArgs e)
        {
            close("");
            nyala = false;
        }

        private void lblProdctionCode_Click(object sender, EventArgs e)
        {
            GetBatchnumber(linenumber);
        }

        private void btnConfig_Click(object sender, EventArgs e)
        {
            config cfg = new config(this);
            cfg.ShowDialog();
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            //if (!lblCamera.Text.Equals("Offline"))
            //{
                if (!lblPrinter.Text.Equals("Offline"))
                {
                    Confirm cf = new Confirm("Are you sure?", "Confirmation");
                    if (cf.conf())
                        ToggleBatch();
                }
                else
                {
                    new Confirm("Printer not connected.", "Information", MessageBoxButtons.OK);
                }
            //}
            //else
            //{
            //    new Confirm("Camera not connected.", "Information", MessageBoxButtons.OK);
            //}
        }

        private void btnLogout_Click(object sender, EventArgs e)
        {
            Confirm cf = new Confirm("Are you sure? ", "Confirmation");
            bool result = cf.conf() ;
            if (result)
            {
                Application.Exit();
            }
        }

        private void buttonClearAll_Click(object sender, EventArgs e)
        {
            Confirm cf = new Confirm("Are you sure remove all innerbox ?", "Confirmation");
            bool result = cf.conf() ;
            if (result)
            {
                ClearAllScanned();
                txtInfeedId.Focus();
            }
        }

        private void buttonClearSelected_Click(object sender, EventArgs e)
        {
            //ini benerin
            if (dataGridView1.Rows[ordersItemIndex2].Cells[0].Value.ToString() != null)
            {
                string InnerBoxId = dataGridView1.Rows[ordersItemIndex2].Cells[0].Value.ToString();
                Confirm cf = new Confirm("Are you sure remove innerbox " + InnerBoxId + " ?", "Confirmation");
                bool result = cf.conf();
                if (result)
                {
                    if (ordersItemIndex2 > -1)
                    {
                        dataGridView1.Rows.RemoveAt(ordersItemIndex2);
                        lblQtyScanned.Text = dataGridView1.Rows.Count.ToString();
                        ClearSelectedScanned(InnerBoxId);
                        CountScannedData = dataGridView1.Rows.Count;
                        txtInfeedId.Focus();
                    }
                }
            }
            
        }

        private void btnDeco_Click(object sender, EventArgs e)
        {
            ShowDecomForm();
        }

        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                ordersItemIndex2 = dataGridView1.SelectedRows[0].Index;
            }
            else
            {
                ordersItemIndex2 = 0;
            }
        }

        private void txtBasket_KeyDown_1(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (txtBasket.Text.Length > 0)
                {
                    Match(RemoveCharacter(txtBasket.Text));
                    lblQtyHistory.Text = "" + dataGridView2.Rows.Count;
                    int[] cnt = ms.getCount(BatchNumber);
                    lblQtyHistoryAll.Text = cnt[0].ToString();
                }
                else
                    log("Data basket empty");

                txtBasket.Text = "";
            } 
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
               

                if (txtInfeedId.Text.Length > 0)
                {
                    string data = RemoveCharacter(txtInfeedId.Text);
                    CheckScannedData(data);

                }

                txtInfeedId.Text = "";
            }
        }

        private void btnReprint_Click(object sender, EventArgs e)
        {
            if(LastDataPrinter!=null )
                Printer.Print(LastDataPrinter);
            else
                new Confirm("Data empty.", "Information", MessageBoxButtons.OK);
        }

        private void pictureBox1_DoubleClick(object sender, EventArgs e)
        {
            dbss.Show();
            this.Hide();
        }

        private void txtBatchNumber_Click(object sender, EventArgs e)
        {
            string CurrentSN = Guid.NewGuid().ToString().Replace("-", "").Substring(0, 12).ToUpper();
            //LastDataPrinter = Printer.Print("89949570003458", "16064095910333", "2611020", "211126", "50", "Indonesia", "Vaksin Covid19");                        
            
            lbn = new ListBatchNumber(null, linenumberrs[1], "");
            lbn.ShowDialog();
            txtBatchNumber.Text = lbn.batch;
            ProdModelId = lbn.productmodelid;
            isSAS = lbn.is_SAS;
            int[] cnt = ms.getCount(lbn.batch);
            lblQtyHistoryAll.Text = cnt[0].ToString();
            if (txtBatchNumber.Text.Length > 0)
            {
                btnStart.Enabled = true;
            }
            else
            {
                btnStart.Enabled = false;
            }
        }

        public bool cekDB()
        {
            if (db.Connect())
            {
                lblDb.Text = "Online";
                onDb.Visible = true;
                offDb.Visible = false;
                //ms.StopServer();
                //ms.StartServer2();
                db.closeConnection();
                return true;
            }
            else
            {
                lblDb.Text = "Offline";
                onDb.Visible = false;
                offDb.Visible = true;
                //ms.StopServer();
                return false;
            }
        }

        private void ManualPacker_Load(object sender, EventArgs e)
        {
            timer1.Start();
            lblTime.Text = DateTime.Now.ToLongTimeString();
            lblDate.Text = DateTime.Now.ToShortDateString();
        }

        private void pictureBox1_MouseEnter(object sender, EventArgs e)
        {
            this.pictureBox1.BackColor = ColorTranslator.FromHtml("#00505E");
        }

        private void pictureBox1_MouseLeave(object sender, EventArgs e)
        {
            this.pictureBox1.BackColor = ColorTranslator.FromHtml("#002F36");
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            lblTime.Text = DateTime.Now.ToLongTimeString();
            timer1.Start();
        }

        private void btnIndicator_Click(object sender, EventArgs e)
        {
            CheckDevice();
            cekDB();
        }

        private void cmbAllocation_Click(object sender, EventArgs e)
        {
            ms.GetComboBoxAllocation();
        }

    }
}
