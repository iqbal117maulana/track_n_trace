﻿namespace Basket_Station
{
    partial class Dashboard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Dashboard));
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.lblRole = new System.Windows.Forms.Label();
            this.lblUserId = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.lblDate = new System.Windows.Forms.Label();
            this.lblTime = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnReport = new System.Windows.Forms.Button();
            this.btnPentabio = new System.Windows.Forms.Button();
            this.pnlBatchPentabio = new System.Windows.Forms.Panel();
            this.lblCountBatchPentabio = new System.Windows.Forms.Label();
            this.lblPentabio = new System.Windows.Forms.Label();
            this.pbPentabio = new System.Windows.Forms.PictureBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.pnlBatchPentabio.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbPentabio)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(148)))), ((int)(((byte)(172)))));
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.lblRole);
            this.panel1.Controls.Add(this.lblUserId);
            this.panel1.Controls.Add(this.pictureBox2);
            this.panel1.Controls.Add(this.lblDate);
            this.panel1.Controls.Add(this.lblTime);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1024, 86);
            this.panel1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(464, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(237, 37);
            this.label1.TabIndex = 112;
            this.label1.Text = "Basket Station";
            // 
            // lblRole
            // 
            this.lblRole.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblRole.AutoSize = true;
            this.lblRole.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRole.ForeColor = System.Drawing.Color.White;
            this.lblRole.Location = new System.Drawing.Point(931, 44);
            this.lblRole.Name = "lblRole";
            this.lblRole.Size = new System.Drawing.Size(48, 18);
            this.lblRole.TabIndex = 111;
            this.lblRole.Text = "admin";
            // 
            // lblUserId
            // 
            this.lblUserId.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblUserId.AutoSize = true;
            this.lblUserId.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUserId.ForeColor = System.Drawing.Color.White;
            this.lblUserId.Location = new System.Drawing.Point(930, 24);
            this.lblUserId.Name = "lblUserId";
            this.lblUserId.Size = new System.Drawing.Size(57, 20);
            this.lblUserId.TabIndex = 110;
            this.lblUserId.Text = "admin";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::Basket_Station.Properties.Resources.user;
            this.pictureBox2.Location = new System.Drawing.Point(879, 20);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(45, 45);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox2.TabIndex = 109;
            this.pictureBox2.TabStop = false;
            // 
            // lblDate
            // 
            this.lblDate.AutoSize = true;
            this.lblDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDate.ForeColor = System.Drawing.Color.White;
            this.lblDate.Location = new System.Drawing.Point(216, 38);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(33, 15);
            this.lblDate.TabIndex = 108;
            this.lblDate.Text = "Date";
            // 
            // lblTime
            // 
            this.lblTime.AutoSize = true;
            this.lblTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTime.ForeColor = System.Drawing.Color.White;
            this.lblTime.Location = new System.Drawing.Point(216, 20);
            this.lblTime.Name = "lblTime";
            this.lblTime.Size = new System.Drawing.Size(39, 15);
            this.lblTime.TabIndex = 107;
            this.lblTime.Text = "Time";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(47)))), ((int)(((byte)(54)))));
            this.panel2.Controls.Add(this.pictureBox1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(210, 86);
            this.panel2.TabIndex = 0;
            // 
            // pictureBox1
            // 
            this.pictureBox1.ImageLocation = "logo_biofarma.png";
            this.pictureBox1.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.InitialImage")));
            this.pictureBox1.Location = new System.Drawing.Point(30, 5);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(150, 75);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 36;
            this.pictureBox1.TabStop = false;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.panel3);
            this.panel4.Controls.Add(this.pnlBatchPentabio);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(0, 86);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1024, 654);
            this.panel4.TabIndex = 2;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(47)))), ((int)(((byte)(54)))));
            this.panel3.Controls.Add(this.btnReport);
            this.panel3.Controls.Add(this.btnPentabio);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(210, 654);
            this.panel3.TabIndex = 3;
            // 
            // btnReport
            // 
            this.btnReport.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(47)))), ((int)(((byte)(54)))));
            this.btnReport.FlatAppearance.BorderSize = 0;
            this.btnReport.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(80)))), ((int)(((byte)(94)))));
            this.btnReport.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(80)))), ((int)(((byte)(94)))));
            this.btnReport.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReport.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReport.ForeColor = System.Drawing.Color.White;
            this.btnReport.Image = global::Basket_Station.Properties.Resources.report_vial;
            this.btnReport.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnReport.Location = new System.Drawing.Point(0, 129);
            this.btnReport.Name = "btnReport";
            this.btnReport.Size = new System.Drawing.Size(210, 123);
            this.btnReport.TabIndex = 5;
            this.btnReport.Text = "Report";
            this.btnReport.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnReport.UseVisualStyleBackColor = true;
            this.btnReport.Visible = false;
            // 
            // btnPentabio
            // 
            this.btnPentabio.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(47)))), ((int)(((byte)(54)))));
            this.btnPentabio.FlatAppearance.BorderSize = 0;
            this.btnPentabio.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(80)))), ((int)(((byte)(94)))));
            this.btnPentabio.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(80)))), ((int)(((byte)(94)))));
            this.btnPentabio.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPentabio.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPentabio.ForeColor = System.Drawing.Color.White;
            this.btnPentabio.Image = global::Basket_Station.Properties.Resources.basket;
            this.btnPentabio.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnPentabio.Location = new System.Drawing.Point(0, 0);
            this.btnPentabio.Name = "btnPentabio";
            this.btnPentabio.Size = new System.Drawing.Size(210, 123);
            this.btnPentabio.TabIndex = 3;
            this.btnPentabio.Text = "Generate Basket ID";
            this.btnPentabio.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnPentabio.UseVisualStyleBackColor = true;
            this.btnPentabio.Click += new System.EventHandler(this.btnPentabio_Click);
            // 
            // pnlBatchPentabio
            // 
            this.pnlBatchPentabio.BackColor = System.Drawing.Color.White;
            this.pnlBatchPentabio.Controls.Add(this.lblCountBatchPentabio);
            this.pnlBatchPentabio.Controls.Add(this.lblPentabio);
            this.pnlBatchPentabio.Controls.Add(this.pbPentabio);
            this.pnlBatchPentabio.Location = new System.Drawing.Point(239, 33);
            this.pnlBatchPentabio.Name = "pnlBatchPentabio";
            this.pnlBatchPentabio.Size = new System.Drawing.Size(363, 186);
            this.pnlBatchPentabio.TabIndex = 2;
            this.pnlBatchPentabio.DoubleClick += new System.EventHandler(this.pnlBatchPentabio_DoubleClick);
            this.pnlBatchPentabio.MouseLeave += new System.EventHandler(this.pnlBatchPentabio_MouseLeave);
            this.pnlBatchPentabio.MouseEnter += new System.EventHandler(this.pnlBatchPentabio_MouseEnter);
            // 
            // lblCountBatchPentabio
            // 
            this.lblCountBatchPentabio.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblCountBatchPentabio.AutoSize = true;
            this.lblCountBatchPentabio.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCountBatchPentabio.ForeColor = System.Drawing.Color.Black;
            this.lblCountBatchPentabio.Location = new System.Drawing.Point(110, 63);
            this.lblCountBatchPentabio.Name = "lblCountBatchPentabio";
            this.lblCountBatchPentabio.Size = new System.Drawing.Size(36, 37);
            this.lblCountBatchPentabio.TabIndex = 107;
            this.lblCountBatchPentabio.Text = "0";
            this.lblCountBatchPentabio.MouseLeave += new System.EventHandler(this.lblCountBatchPentabio_MouseLeave);
            this.lblCountBatchPentabio.DoubleClick += new System.EventHandler(this.lblCountBatchPentabio_DoubleClick);
            this.lblCountBatchPentabio.MouseEnter += new System.EventHandler(this.lblCountBatchPentabio_MouseEnter);
            // 
            // lblPentabio
            // 
            this.lblPentabio.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblPentabio.AutoSize = true;
            this.lblPentabio.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPentabio.ForeColor = System.Drawing.Color.Black;
            this.lblPentabio.Location = new System.Drawing.Point(53, 115);
            this.lblPentabio.Name = "lblPentabio";
            this.lblPentabio.Size = new System.Drawing.Size(144, 24);
            this.lblPentabio.TabIndex = 106;
            this.lblPentabio.Text = "Opened Batch";
            this.lblPentabio.MouseLeave += new System.EventHandler(this.lblPentabio_MouseLeave);
            this.lblPentabio.DoubleClick += new System.EventHandler(this.lblPentabio_DoubleClick);
            this.lblPentabio.MouseEnter += new System.EventHandler(this.lblPentabio_MouseEnter);
            // 
            // pbPentabio
            // 
            this.pbPentabio.Image = global::Basket_Station.Properties.Resources.batch_pentabio;
            this.pbPentabio.Location = new System.Drawing.Point(241, 40);
            this.pbPentabio.Name = "pbPentabio";
            this.pbPentabio.Size = new System.Drawing.Size(113, 99);
            this.pbPentabio.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pbPentabio.TabIndex = 0;
            this.pbPentabio.TabStop = false;
            this.pbPentabio.DoubleClick += new System.EventHandler(this.pbPentabio_DoubleClick);
            this.pbPentabio.MouseLeave += new System.EventHandler(this.pbPentabio_MouseLeave);
            this.pbPentabio.MouseEnter += new System.EventHandler(this.pbPentabio_MouseEnter);
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // Dashboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1024, 740);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Dashboard";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Basket Station";
            this.TransparencyKey = System.Drawing.Color.AntiqueWhite;
            this.Load += new System.EventHandler(this.Dashboard_Load);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Dashboard_FormClosed);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.pnlBatchPentabio.ResumeLayout(false);
            this.pnlBatchPentabio.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbPentabio)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        public System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lblDate;
        private System.Windows.Forms.Label lblTime;
        private System.Windows.Forms.PictureBox pictureBox2;
        public System.Windows.Forms.Label lblRole;
        public System.Windows.Forms.Label lblUserId;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel pnlBatchPentabio;
        public System.Windows.Forms.Label lblCountBatchPentabio;
        public System.Windows.Forms.Label lblPentabio;
        private System.Windows.Forms.PictureBox pbPentabio;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button btnReport;
        private System.Windows.Forms.Button btnPentabio;
        private System.Windows.Forms.Timer timer1;
        public System.Windows.Forms.Label label1;

    }
}

