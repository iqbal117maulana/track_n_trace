﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Printing;
using System.Runtime.InteropServices;
using System.IO;

namespace Basket_Station
{
    public partial class print : Form
    {

        public print()
        {
            InitializeComponent();
            printDocument1.PrintPage +=
                new PrintPageEventHandler(printDocument1_PrintPage);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            StringBuilder ZplBuilder = new StringBuilder();

            // Exemple ZPL String

			ZplBuilder.Append("^XA~TA000~JSN^LT0^MNW^MTT^PON^PMN^LH0,0^JMA^PR3,3~SD21^JUS^LRN^CI0^XZ");
			ZplBuilder.Append("^XA");
			ZplBuilder.Append("^MMT");
			ZplBuilder.Append("^PW1181");
			ZplBuilder.Append("^LL1181");
			ZplBuilder.Append("^LS0");
			ZplBuilder.Append("^FO0,0^GFA,15360,15360,00060,:Z64:eJztmj2O3DYUgCnTCQ3YMF1uYSy3S2sgjQsj3CPkCD5CyhRGyIWDpEnhAwSIj5AbREfIEeQbqDAQwVHEvB9Ko9HInuGT4Y2ReYA0sxx9IkU+vj+tUmc5y1nOcpaPLde3xG6R+Pmx1RZWjir9P2O/vCX24WfIPv6M2OsN7FX+fCZgH21g7+EpKvW9gCVdrDewr2WsjnD6XalOwFY1nF7JWNXA8auqROx3cPwiZJ/CuH9SupWwl8D+HI2ItRtYE1X1snGi5zW10jetjIVZ0jcpiVhYHR2JvSqHe3jklHpVvS5nB2ADsOa6nPXRRp8G9UzC1g7Y9LKry1nXhehSSm9iOWtTinCkm3J0YiUe2AALR28ErAYWjlbKVinVEk+Y2ShhkVOgV5dS1rdSFhYqqm+kLMgPn5jVmR3wdKeMNWnYsYUuzTFb4dnWZayHtUU2jX2fLjBVZNd1qlUotPCw7RtmWwtKUiL3U3qnmE08+NPl7vgFtjAP4HSZrAWysYylmCGzhUNW6nn+tHm+S2QM6ICtP3Tdmozb1ma1LhHYuSSjWpeIaUa2eKqmCMcI3LfO3enyaea9J2RV1qZKxNb5U8JmxkvYPL9OwuaJNoIQa2R1uW6oSRfL2Wpiyx2DnvaPK2Z3ew+i4UJxE6vrUjbs9nxTiKJZHr8/LWThcSd78aCQ9TO20PnikJmtUqmxs5MX8qUu5S54Tc9ssVvQMF5L+6cqdgsujntPF3uyBg5O50x2wycLJew6s4LtqzR2DhP+ydka81grygiBDbWQNRHdiWzMBrKUFkyAiKWoUMZ+SarsBG5fqYcVbkMZ+wCjspnJK2KdnL0IGA26N6X7COURoKl2NShIsXzdGGSVa8rZr3DfN04ZAfsY2fZSVEp6TrbqUljCArZ7LPHdKKlGVpJMYnj24rESJbHAqsuLXTRcJoN6eCHx+yi9MheztKOQ1cAW+32STlWvFJcqpWxpzIBSgTb+qWR1c2S/VbL3IlWj1JMNLNbdJe+9Rlbyrm4KBq83sFcCNuYvjz501RFWIKvs1YnsWmMjZ0/dkGsVQrOBPbUqtKbHbgMbIp6PR+NrsT571OO9r7AVs74RsJrZIGHvcwRxPA24d9hkT2UvNrArW89ytHW8PLTCuo/AHkPXJLPHU/GrFVYWQGxm/RZWFDBllt5GmcR1WpCAN+OUkfQGJ5MVqFoqUKBsGFlPCYzhP2n2KdnFqiJnvQdZCsXFxAb6iSqQjvJNjCApBM2pq19G3oGqszCoXEugzDpQCdNiegGnznLKfFCLRbZFrc5lWvqTq70O0wu4V09fKNev91iMxztk79NPeEGnqY2nAk7vPA1Jp2XWnagO4DB/wc6w994wiz/9iCeeE7vKDqhdefD4J9cVcAjpJmWJdPM9ZagWLF6AzwdtOMb0cmRrfPIVFhKfvuIHxwtaTyyOfDAjS5O/v8CoK8B6ZP8CNqR/UoOnhMP/o8cTML/RZX/vp3QVaQJotYb2hCOHJAhG6JHtdOtSZ2jmUUFi2GNRiW2KvsMKL+wp6M9gcc5iAteoxqeaNavVvBwL1jBLhRe44E4Ft4E2B3ML/dMrNoVsNxrGGQuuIbQWB98jRsVevB+qKI4SFSO1hlV3JnANs9iLG6jOCw+HbR47CT3XJgOw9ftZtNWIKZPHQvrfcU000HuU97EDs/WubTQCpNurLLnC1KDdcoNhltvaXKvGA1m1YO0e22OX1GZGtsl9tzvbc8h2O3ZgtmHV+SBL1+HT+Z5+ZPYGWU1bmvpGQ76/vnwd3B7fZM1Ym6pxyr8gtnG9WujVeB2xoaMb2z63cUTB/fJDncjqkc3jXmcdLmughXQTO1/qHdsvWT+QduBiTizeL57AhszqkR1m7DRu5PYdH05s6lkDDWtIZudqMrHdgtVoGmAH42ZnNi1VjNhhycICWmThodHe0I+o9b43Sc3ZiBstLNg24L4GAwnmNHp2hc2+ihGPxmHJov0k65Q9SMtt3Z7zNfwfCft+gQx4bdml4LCzX0msJsfZaIdcnw6JHm6f5XGbA5bcgZqzuY1VzPcTqw9YRUPN7hcdb3a0WU18P427Ys8yl0AutKfbNmpy8Et2WGM9eT/2mBGot3y/9JbUZFRiUt0D1lFsglY0DRzUKHbjxLI14b79AUsxEd7es7uOedEPWZcOXsKi9UXWwk3zfx5UxHZztuflXLBYVKLrXuxeT7mJbXfPjINesHOpyt8MT6I3sfFWWHNrrBjdxNrbYiVl/CybkrQNyZKkmj6x9QZWjgqL2iyS//cd5ckG9vkG9noDe5aznOUs/0n5F1LetlE=:851D^BY312,312^FT708,696^BXN,12,200,0,0,1,~");
			ZplBuilder.Append("^BY312,312^FT708,696^BXN,12,200,0,0,1,~");
			ZplBuilder.Append("^FH\\^FD(01)12345678901234(21)ABCD123456(10)BIO123456(17)22072017(22)050^FS");
			ZplBuilder.Append("^FT638,291^A0N,62,91^FH\\^FDPENTA BIO^FS");
			ZplBuilder.Append("^FT765,830^A0N,42,45^FH\\^FD(01)12345678901234^FS");
			ZplBuilder.Append("^FT765,882^A0N,42,45^FH\\^FD(21)ABCD123456^FS");
			ZplBuilder.Append("^FT765,934^A0N,42,45^FH\\^FD(10)BIO123456^FS");
			ZplBuilder.Append("^FT765,986^A0N,42,45^FH\\^FD(17)22072017^FS");
			ZplBuilder.Append("^FT765,1038^A0N,42,45^FH\\^FD(22)050^FS");
			ZplBuilder.Append("^FT30,294^A0N,42,40^FH\\^FDBANDUNG - INDONESIA^FS");
			ZplBuilder.Append("^FT56,556^A0N,58,57^FH\\^FDsimpan antara ^FS");
			ZplBuilder.Append("^FT56,629^A0N,58,57^FH\\^FD+2 \\F8C dan +8 \\F8C^FS");
			ZplBuilder.Append("^FT39,1035^A0N,42,40^FH\\^FDPT. Bio Farma (Persero)^FS");
			ZplBuilder.Append("^FT39,1087^A0N,42,40^FH\\^FDJl. Pasteur No. 28, Bandung^FS");
			ZplBuilder.Append("^FT39,1139^A0N,42,40^FH\\^FDIndonesia ^FS");
			ZplBuilder.Append("^FT591,837^A0N,40,50^FH\\^FDGTIN^FS");
			ZplBuilder.Append("^FT591,887^A0N,40,50^FH\\^FDSN^FS");
			ZplBuilder.Append("^FT591,937^A0N,40,50^FH\\^FDLOT NO^FS");
			ZplBuilder.Append("^FT591,987^A0N,40,50^FH\\^FDEXP^FS");
			ZplBuilder.Append("^FT591,1037^A0N,40,50^FH\\^FDQTY^FS");
			ZplBuilder.Append("^PQ1,0,1,Y^XZ");
            // End ZPL

            string ZplString = ZplBuilder.ToString();

            MemoryStream lmemStream = new MemoryStream();

            StreamWriter lstreamWriter = new StreamWriter(lmemStream);
            lstreamWriter.Write(ZplString);
            lstreamWriter.Flush();
            lmemStream.Position = 0;

            byte[] byteArray = lmemStream.ToArray();

            IntPtr cpUnmanagedBytes = new IntPtr(0);
            int cnLength = byteArray.Length;
            cpUnmanagedBytes = Marshal.AllocCoTaskMem(cnLength);
            Marshal.Copy(byteArray, 0, cpUnmanagedBytes, cnLength);
            RawPrinterHelper.SendBytesToPrinter("ZDesigner ZT410-300dpi ZPL", cpUnmanagedBytes, cnLength);
            Marshal.FreeCoTaskMem(cpUnmanagedBytes);

        }

        string command ="^XA~TA000~JSN^LT0^MNW^MTT^PON^PMN^LH0,0^JMA^PR3,3~SD21^JUS^LRN^CI0^XZ\n" +
                       "^XA\n" +
                       "^MMT\n" +
                       "^PW1181\n" +
                       "^LL1181\n" +
                       "^LS0\n" +
                       "^FO32,32^GFA,11648,11648,00052,:Z64:\n" +
                       "eJztWcFu2zAMpYwaCHyKgOoe9FT4K3zo7jEQ/4/QU+Cv0NHQV46PslOr6yEktmLYwjYRrYh+eqZI0zLRQx7ykP9dmn8M52CwefomHIt8F87JYHM02KRvwjn9cZwo38mAMxhsDDhON/wQKS7vaSN2t5zbQWfAznntktaGvD/rDK6U+ryoI6idLjqDFzo1OTdRieP8s87g9ZoYJ2txaJp0F655OVHOSZ15/Kgb3zEVpvOqxQlvuvHgw+vtpMXRSpcTxRiTwXTQDAYfsmUelXRzRLMYTAfF2JVOk6lJWhxNioul6XJWM3p6exu0Np0hUlXyuuHoHfSkodNtOFGNoxL2flTjDKXRZIRtlWWFjaUEaVaAPiqMViKainTD6TQ4Bmk4vYmk+21UC22VfsPRiME/2YBjyAO9BUcvh2UugXNdrvF+M+2jDx7jetFK1r5XtP5B8SGJp8tXjZ3lUU5wmheDpU4ONC/6NNoqq17cGPiW0Oj8Q25U45xwi1P6h6vrSYtDKHqVfJzzXgszR1S9cYkqq2kalDinkuKUOyJe7yDqluWgfZ5rDTHU6Yt4xtFLF/GUpXswMWcEpVg2KSw4FrFsIln4bJVo/MM4J4ONRSw4w2/AUe7ciNxT/n0eYomN/o4xn3EsPpaqVrlNJBs3uu2oxoDjhE9QsbLgtIWPxoSaRY/j9HcWvrXobfSljA3HGdZbL9sCXJyVAi0vN23t4zaif2fTys7atNUAqGxC0dhxP1iTncR6P5FxMhZ3I3hcQaUPjTEWaTFqZ+PHcSTCP2oAxwdu0wb0oUdGfcLhSXN9hlP15cy5bH9AK9sg9XZimEBkAq8L6EwDa0JvekNfKHyrigc4PPGNFx+syKsGAdn9Q1DwwZN7Js9CLoRwFI37PT78K8cWF3BhxwfnWZoc0cZ3fIkmZ84bzpyrzR2eZzi7C/GsQWp4Em1wmD80Vi8IsrBbl/A24xBIJQQtHnv71DEJ9PUgQ/1SrcsRIcR/TMGTF8dAc5t2hlq+P+Mwo3J2Ei1Cw28FJ9c4E0LIXVqsswGT5umzhrBy6Dujg8nt4qwpOGDEOH0qiWivCQ6W+M0G4eOO7thyGwhOKJpff/PS+CrONhxMt5z9poFTFJwogz5wLuCDP7CAU6BNQ1g1bsGHaj44c5dwmlzjrJrwqfKG+OHsz2h9YXGG0zbND3DfscqDcm02HMybaq2PX+DAG+fijwnzlltR0YIw4yGFdI3Ds++267PHSXucVPkHPFoEyc4r0PzmHyzGUPmHk1hmPnBHwYkrDq5d8QynuH6Hc8GyEj4cJJLKbszCUJaj+AcHFU6P69bL4haGjEMrjtyhZNHfbGgcPLzhRn4IFR+4UQ6o+EbCi8YjDm4ikd5LeuPFnZPkmNysOGWZSNLY4UzTGv+TfPGpWROPFN8IwemX/AYcyTMNEttXOH2Ng/SGuCnp7RmveoI/OrzugW+epcCTJPghJf/H8qwrSQzM3nEF+YMWyaDO13JngReYViuTDjs+7aWEUM0HSYzWtNbJpBu5grT5pripuv9I7kJ8+JLQoI1HCSt/SwnMtnr/g3p0JryF2/Y85vnaYbOAe9FiQDfP9LX8dbVCpI9XCAup35LuYuRu2cfIvbKPkXvF8DLWxifqbdS7OCQBoxblO1+RZLB5yEMe8pCH/N/yE/061d8=:ED77\n" +
                       "^BY312,312^FT708,696^BXN,12,200,0,0,1,~\n" +
                       "^FH\\^FD(01)12345678901234(21)ABCD123456(10)BIO123456(17)22072017(22)050^FS\n" +
                       "^FT638,291^A0N,62,91^FH\\^FDPENTA BIO^FS\n" +
                       "^FT765,830^A0N,42,45^FH\\^FD(01)12345678901234^FS\n" +
                       "^FT765,882^A0N,42,45^FH\\^FD(21)ABCD123456^FS\n" +
                       "^FT765,934^A0N,42,45^FH\\^FD(10)BIO123456^FS\n" +
                       "^FT765,986^A0N,42,45^FH\\^FD(17)22072017^FS\n" +
                       "^FT765,1038^A0N,42,45^FH\\^FD(22)050^FS\n" +
                       "^FT30,294^A0N,42,40^FH\\^FDBANDUNG - INDONESIA^FS\n" +
                       "^FT56,556^A0N,58,57^FH\\^FDsimpan antara ^FS\n" +
                       "^FT56,629^A0N,58,57^FH\\^FD+2 \\F8C dan +8 \\F8C^FS\n" +
                       "^FT39,1035^A0N,42,40^FH\\^FDPT. Bio Farma (Persero)^FS\n" +
                       "^FT39,1087^A0N,42,40^FH\\^FDJl. Pasteur No. 28, Bandung^FS\n" +
                       "^FT39,1139^A0N,42,40^FH\\^FDIndonesia ^FS\n" +
                       "^FT591,837^A0N,40,50^FH\\^FDGTIN^FS\n" +
                       "^FT591,887^A0N,40,50^FH\\^FDSN^FS\n" +
                       "^FT591,937^A0N,40,50^FH\\^FDLOT NO^FS\n" +
                       "^FT591,987^A0N,40,50^FH\\^FDEXP^FS\n" +
                       "^FT591,1037^A0N,40,50^FH\\^FDQTY^FS\n" +
                       "^PQ1,0,1,Y^XZ\n";
        private PrintPreviewDialog printPreviewDialog1 = new PrintPreviewDialog();
        private PrintDocument printDocument1 = new PrintDocument();
        private string documentContents;
        private string stringToPrint;
       
        private void button2_Click(object sender, EventArgs e)
        {

        }
        void printDocument1_PrintPage(object sender, PrintPageEventArgs e)
        {
            int charactersOnPage = 0;
            int linesPerPage = 0;

            // Sets the value of charactersOnPage to the number of characters 
            // of stringToPrint that will fit within the bounds of the page.
            e.Graphics.MeasureString(stringToPrint, this.Font,
                e.MarginBounds.Size, StringFormat.GenericTypographic,
                out charactersOnPage, out linesPerPage);

            // Draws the string within the bounds of the page.
            e.Graphics.DrawString(stringToPrint, this.Font, Brushes.Black,
            e.MarginBounds, StringFormat.GenericTypographic);

            // Remove the portion of the string that has been printed.
            stringToPrint = stringToPrint.Substring(charactersOnPage);

            // Check to see if more pages are to be printed.
            e.HasMorePages = (stringToPrint.Length > 0);

            // If there are no more pages, reset the string to be printed.
            if (!e.HasMorePages)
                stringToPrint = documentContents;
        }
    }

}
