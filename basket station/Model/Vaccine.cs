﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Basket_Station.Model
{
    class Vaccine
    {
        public string CAPID { get; set; }
        public string GS1VIALID { get; set; }
        public string BLISTERID { get; set; }
        public string INNERBOXID { get; set; }
        public string INNERBOXGSONEID { get; set; }
        public string BASKETID { get; set; }
        public string MASTERBOXID { get; set; }
        public string MASTERBOXGS1ID { get; set; }

        public string PRODUCTMODELID { get; set; }
        public string BATCHNUMBER { get; set; }

        public string CREATEDTIME { get; set; }
        public string ISREJECT { get; set; }
        public string LINENUMBER { get; set; }
        public string LASTLOCATIONID { get; set; }
        public string SHIPPINGSTATUS { get; set; }
        public string USAGETIME { get; set; }
        public string USAGECOUNT { get; set; }
        public string ISEMPTY { get; set; }
        public string REMARKS { get; set; }
        public string SERIALNUMBER { get; set; }
        public string EXPDATE { get; set; }
        public string FLAG { get; set; }


        Modul.DBACCESS db = new Modul.DBACCESS();

        public List<string[]> GETDATA()
        {
            return null;
        }

        public bool CHECK_VACCINE()
        {
            try
            {
                db.FIELD.Add("capid");
                db.WHERE = "(Capid ='" + CAPID + "' OR gsOneVialId='" + GS1VIALID + "')";

                if (BATCHNUMBER != null)
                    db.WHERE += "AND BATCHNUMBER = '" + BATCHNUMBER + "'";

                if (ISREJECT != null)
                    db.WHERE += "AND isreject = '" + ISREJECT + "'";

                db.TABLE = "Vaccine";
                db.SELECT();
                if (db.num_rows > 0)
                {
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                Util.Common.simpanLog("Error Vaccine : " + ex.Message);
                return false;
            }
        }
        
        internal bool DECOMMISSION()
        {
            db.PARAMETER.Add("isreject", "2");
            db.WHERE = "Capid ='" + CAPID + "' OR gsOneVialId='" + GS1VIALID + "'";
            db.TABLE = "Vaccine";
            if (BATCHNUMBER.Length > 0)
                db.WHERE += "AND BATCHNUMBER = '" + BATCHNUMBER + "'";
            return db.UPDATE();
        }

        internal bool UPDATE_EMPTY_BLISTER()
        {
            db.PARAMETER.Add("blisterpackId", "null");
            db.WHERE = "blisterpackId ='" + BLISTERID + "'";
            db.TABLE = "Vaccine";
            if (BATCHNUMBER.Length > 0)
                db.WHERE += " AND BATCHNUMBER = '" + BATCHNUMBER + "'";
            return db.UPDATE();
        }

        internal bool UPDATE_EMPTY_INNERBOX()
        {
            db.PARAMETER.Add("INNERBOXID", "null");
            db.PARAMETER.Add("INNERBOXGSONEID", "null");
            db.WHERE = "INNERBOXID ='" + INNERBOXID + "' OR INNERBOXGSONEID='" + INNERBOXGSONEID + "'";
            db.TABLE = "Vaccine";
            if (BATCHNUMBER.Length > 0)
                db.WHERE += " AND BATCHNUMBER = '" + BATCHNUMBER + "'";
            return db.UPDATE();
        }

        internal bool UPDATE_EMPTY_BASKET()
        {
            db.PARAMETER.Add("BASKETID", "null");
            db.WHERE = "BASKETID ='" + BASKETID + "'";
            db.TABLE = "Vaccine";
            if (BATCHNUMBER.Length > 0)
                db.WHERE += " AND BATCHNUMBER = '" + BATCHNUMBER + "'";
            return db.UPDATE();
        }
        
    }
}
