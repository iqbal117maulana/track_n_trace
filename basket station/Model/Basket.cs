﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Basket_Station.Model
{
    class Basket
    {
        public string BASKETID { get; set; }
        public string CREATEDTIME { get; set; }
        public string ISREJECT { get; set; }
        public string ISDECOMMISSIONED { get; set; }
        public string DECOMMISSIONEDTIME { get; set; }
        public string LOCATIONID { get; set; }
        public string BATCHNUMBER { get; set; }

        Modul.DBACCESS db = new Modul.DBACCESS();

        public bool CHECK_BASKET()
        {
            try
            {
                db.FIELD.Add("BASKETID");
                db.WHERE = "(BASKETID = '" + BASKETID + "')";

                if (BATCHNUMBER != null)
                    db.WHERE += " AND BATCHNUMBER = '" + BATCHNUMBER + "'";

                if (ISREJECT != null)
                    db.WHERE += " AND isreject = '" + ISREJECT + "'";

                db.TABLE = "BASKET";
                db.SELECT();
                if (db.num_rows > 0)
                {
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                Util.Common.simpanLog("Error BASKET : " + ex.Message);
                return false;
            }
        }

        internal bool DECOMMISSION()
        {
            db.PARAMETER.Add("isreject", "2");
            db.WHERE = "(BASKETID = '" + BASKETID + "')";
            db.TABLE = "BASKET";
            if (BATCHNUMBER.Length > 0)
                db.WHERE += "AND BATCHNUMBER = '" + BATCHNUMBER + "'";
            return db.UPDATE();
        }

    }
}
