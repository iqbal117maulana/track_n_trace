﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Basket_Station.Model
{
    class innerBox
    {
        public string INERBOXID { get; set; }
        public string GS1INNERBOXID { get; set; }
        public string CREATEDDATE { get; set; }
        public string ISREJECT { get; set; }
        public string BASKETID { get; set; }
        public string MASTERBOXID { get; set; }
        public string FLAG { get; set; }
        public string BATCHNUMBER { get; set; }


        Modul.DBACCESS db = new Modul.DBACCESS();

        public bool CHECK_INNERBOX()
        {
            try
            {
                db.FIELD.Add("infeedInnerBoxId");
                db.WHERE = "(infeedInnerBoxId = '" + INERBOXID + "' OR gsOneInnerBoxId = '"+GS1INNERBOXID+"')";

                if (BATCHNUMBER != null)
                    db.WHERE += " AND BATCHNUMBER = '" + BATCHNUMBER + "'";

                if (ISREJECT != null)
                    db.WHERE += " AND isreject = '" + ISREJECT + "'";

                db.TABLE = "innerBox";
                db.SELECT();
                if (db.num_rows > 0)
                {
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                Util.Common.simpanLog("Error INFEED : " + ex.Message);
                return false;
            }
        }

        internal bool DECOMMISSION()
        {
            db.PARAMETER.Add("isreject", "2");             
            db.WHERE = "(infeedInnerBoxId ='" + INERBOXID + "' OR gsOneInnerBoxId = '" + GS1INNERBOXID + "')";

            db.TABLE = "innerBox";
            if (BATCHNUMBER.Length > 0)
                db.WHERE += "AND BATCHNUMBER = '" + BATCHNUMBER + "'";
            return db.UPDATE();
        }

        internal bool UPDATE_EMPTY_BASKETID()
        {
            db.PARAMETER.Add("BASKETID", "null");
            db.WHERE = "BASKETID ='" + BASKETID + "'";
            db.TABLE = "innerBox";
            if (BATCHNUMBER.Length > 0)
                db.WHERE += " AND BATCHNUMBER = '" + BATCHNUMBER + "'";
            return db.UPDATE();
        }
    }
}
