﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.Configuration;

namespace Basket_Station
{
    class DBAccess
    {
        public static SqlConnection Connection;
        public static SqlCommand Command;
        public static SqlDataReader DataReader;

        public static void Connect()
        {
            Connection = new SqlConnection(ConfigurationManager.ConnectionStrings["production"].ConnectionString);
            try
            {
                Connection.Open();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Can not open connection ! " + ex.ToString());
                Application.Exit();
            }
            finally
            {
                Connection.Close();
            }
        }

        public static String Login(String Username, String Password)
        {
            String Result = null;
            String Query = String.Format("SELECT TOP 1 * FROM master_user WHERE id = '{0}' AND password = '{1}' AND is_active = '1'", Username, Password);

            Connection.Open();
            Command = new SqlCommand(Query, Connection);
            DataReader = Command.ExecuteReader();

            while (DataReader.Read())
            {
                Result = DataReader.GetValue(0).ToString(); ;
            }

            DataReader.Close();
            Command.Dispose();
            Connection.Close();

            return Result;
        }

        public static List<String> ScanInnerBox(String Data)
        {
            List<String> Results = new List<String>();
            Data = Data.Replace(",", "','");
            String Query = String.Format("SELECT id FROM master_inner_box WHERE id IN ('{0}') AND is_active = '1'", Data);

            Connection.Open();
            Command = new SqlCommand(Query, Connection);
            DataReader = Command.ExecuteReader();

            while (DataReader.Read())
            {
                Results.Add(DataReader.GetValue(0).ToString());
            }

            DataReader.Close();
            Command.Dispose();
            Connection.Close();

            return Results;
        }

        public static String GetLastSN()
        {
            String Result = null;
            String Query = String.Format("SELECT TOP 1 id, MAX(created_date) latest_date FROM master_basket GROUP BY id");

            Connection.Open();
            Command = new SqlCommand(Query, Connection);
            DataReader = Command.ExecuteReader();

            while (DataReader.Read())
            {
                Result = DataReader.GetValue(0).ToString(); ;
            }

            DataReader.Close();
            Command.Dispose();
            Connection.Close();

            return Result;
        }

        public static List<String> GetCurrentPODetails()
        {
            List<String> Results = new List<String>();
            String Query = String.Format("SELECT TOP 1 production_order_number, product_code, product_expiry_date FROM transaction_production_order WHERE status = 'PROGRESS' AND is_active = '1'");

            Connection.Open();
            Command = new SqlCommand(Query, Connection);
            DataReader = Command.ExecuteReader();

            while (DataReader.Read())
            {
                Results.Add(DataReader.GetValue(0).ToString());
                Results.Add(DataReader.GetValue(1).ToString());
                Results.Add(DataReader.GetValue(2).ToString());
            }

            DataReader.Close();
            Command.Dispose();
            Connection.Close();

            return Results;
        }

        public static void InsertNewSN(String SN)
        {
            String Query = String.Format("INSERT INTO master_basket (id, created_date, status, is_active) VALUES('{0}', GETDATE(), 'PRINTED', '1')", SN);

            Connection.Open();
            Command = new SqlCommand(Query, Connection);
            Command.ExecuteNonQuery();
            Command.Dispose();
            Connection.Close();
        }

        public static void UpdateNewSN(String SN)
        {
            String Query = String.Format("UPDATE master_basket SET updated_date = GETDATE(), status = 'SCANNED' WHERE id = '{0}'", SN);

            Connection.Open();
            Command = new SqlCommand(Query, Connection);
            Command.ExecuteNonQuery();
            Command.Dispose();
            Connection.Close();
        }

        public static void UpdateInnerBox(List<String> Cartons, String SN)
        {
            String CartonGroups = String.Empty;
            foreach (String Carton in Cartons)
            {
                CartonGroups += Carton;
                if (!Cartons[Cartons.Count - 1].Equals(Carton))
                {
                    CartonGroups += "','";
                }
            }

            String Query = String.Format("UPDATE master_inner_box SET basket_id = '{0}', updated_date = GETDATE() WHERE id IN ('{1}') ", SN, CartonGroups);

            Connection.Open();
            Command = new SqlCommand(Query, Connection);
            Command.ExecuteNonQuery();
            Command.Dispose();
            Connection.Close();
        }
    }
}
