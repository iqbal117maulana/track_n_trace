﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Net.NetworkInformation;

namespace Basket_Station
{
    public class Control
    {
        public const char delimit = ',';
        public ManualPacker wr;
        private Thread serverThread;
        //private server srv;
        public Mod_Server srv;
        sqlitecs sq;
        public string adminid;
        public dbaccess db;
        public List<string[]> datacombo;
        public string compId;

        public int  qtyPass = 0;

        public Control()
        {
            sq = new sqlitecs();
            db = new dbaccess();
            compId = sq.config["compId"];
            //StartServer();
        }

        //public void StartServer()
        //{
        //    serverThread = new Thread(new ThreadStart(MulaiServer));
        //    serverThread.Start();
        //}

        //public void StopServer()
        //{
        //    serverThread.Interrupt();
        //    serverThread.Abort();
        //    server.isRunning = false;
        //}

        //public void MulaiServer()
        //{
        //   // srv = new server(wr, sq.config["portserver"], this);
        //    srv = new Mod_Server();
        //    srv.Start(this, sq.config["portserver"]);
        //}

        internal void tambahData(string data)
        {
            if (wr.InvokeRequired)
            {
                wr.Invoke(new Action<string>(tambahData), new object[] { data });
                return;
            }
            log("Receive data : " + data);
            data = data.Replace("\r", "");
            data = data.Replace("\n", "");
            data = data.Replace("]d2", "");
            data = data.Replace(" ", "");
            data = data.Replace(""+(char)29, "");
            if (!data.Contains("?") && data.Length > 6)
            {
                wr.CheckScannedData(data);
                //string[] hasilSplit = data.Split(delimit);
                //foreach (string da in hasilSplit)
                //{
                //    wr.CheckScannedData(da);
                //}

            }
        }
      
        public void log(string data)
        {
            try
            {
                string dates = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                string simpan = dates + "\t" + data + "\n";
                wr.txtLog.Invoke(new Action(() => { wr.txtLog.AppendText(simpan); }));

            }
            catch
            {

            }
        }

        public bool PingHost(string nameOrAddress)
        {
            if (nameOrAddress.Length == 0)
                return false;
            bool pingable = false;
            Ping pinger = new Ping();
            try
            {
                PingReply reply = pinger.Send(nameOrAddress);
                pingable = reply.Status == IPStatus.Success;
            }
            catch (PingException)
            {
                // Discard PingExceptions and return false;
            }
            return pingable;
        }



        internal void close(string p)
        {
            throw new NotImplementedException();
        }

        //tambahan
        //public string[] InsertBlisterFail(string[] hasilsplit)
        //{
        //    for (int i = 2; i < hasilsplit.Length; i++)
        //    {
        //        InsertToDatabaseReject(hasilsplit[i], hasilsplit[1], hasilsplit[0]);
        //    }
        //    return hasilsplit;
        //}

        //private void InsertToDatabaseReject(string dataCapid, string datablisterpackId, string status)
        //{
        //    if (batch != null && productmodelid != null)
        //    {
        //        if (batch.Length > 0 && productmodelid.Length > 0)
        //        {
        //            List<string> fieldz = new List<string>();
        //            fieldz.Add("[gsOneVialId]");
        //            DataSet getGs1 = db.select(fieldz, "[Vaccine]", "capId = '" + dataCapid + "' and batchNumber = '" + batch + "'");
        //            string gsone = null;
        //            if (getGs1.Tables[0].Rows.Count != 0)
        //            {
        //                gsone = getGs1.Tables[0].Rows[0]["gsOneVialId"].ToString();
        //            }

        //            Dictionary<string, string> field = new Dictionary<string, string>();
        //            field.Add("capid", dataCapid);
        //            field.Add("blisterpackid", datablisterpackId);
        //            field.Add("gsonevialid", gsone);
        //            if (status != "1")
        //            {
        //                field.Add("flag", "0");
        //            }
        //            else
        //            {
        //                field.Add("flag", "1");
        //            }
        //            field.Add("batchnumber", batch);
        //            field.Add("productModelId", productmodelid);
        //            field.Add("expdate", exp);
        //            field.Add("createdtime", db.getUnixString());
        //            field.Add("isreject", status);
        //            field.Add("linenumber", linenumber);
        //            if (!db.insert(field, "vaccine_reject"))
        //            {
        //                sendToPLC();
        //            }
        //        }
        //        else
        //        {
        //            status = "1";
        //            log("Data reject not saved.");
        //        }
        //    }
        //    else
        //    {
        //        status = "1";
        //        log("Data reject not saved.");
        //    }
        //}

        public int[] getCount(string btch)
        {
            int[] cnt = new int[1];
            qtyPass = db.selectCountBasket(btch);
            cnt[0] = qtyPass;
            return cnt;
        }

        public void GetComboBoxAllocation()
        {
            List<string> field = new List<string>();
            field.Add("allocation_name");
            string where = "";
            datacombo = db.selectList(field, "[allocation]", where);
            List<string> da = new List<string>();
            for (int i = 0; i < datacombo.Count; i++)
            {
                da.Add(datacombo[i][0]);
            }
            wr.cmbAllocation.DataSource = da;
        }

        //end tambahan

    }
}
