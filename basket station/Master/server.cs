﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Net;
using System.Threading;
using System.Data;
using System.Windows.Forms;

namespace Basket_Station
{
    class server
    {
        ManualPacker ad;
        int port;
        Control ms;
        bool alredyOpened;
        public server(ManualPacker bl, string ports, Control mst)
        {
            ms = mst;
            port = int.Parse(ports);
            ad = bl;
            isRunning = true;
            mThread t;
            if (!cekPort(ports))
            {
                alredyOpened = false;
                t = new mThread(StartListening);
                Thread masterThread = new Thread(() => t(ref isRunning));
                masterThread.IsBackground = true; //better to run it as a background thread
                masterThread.Start();
            }
            else
            {
                alredyOpened = true;
                t = new mThread(StartListening);
                Thread masterThread = new Thread(() => t(ref isRunning));
                masterThread.IsBackground = true; //better to run it as a background thread
                masterThread.Start();
                //   new Confirm("Port already opened", "Information", MessageBoxButtons.OK);
                mst.db.simpanLog("Port :" + ports + " Used");
            }
        }
        public static bool isRunning;

        delegate void mThread(ref bool isRunning);
        delegate void AccptTcpClnt(ref TcpClient client, TcpListener listener);


        public static void AccptClnt(ref TcpClient client, TcpListener listener)
        {
            try
            {
                if (client == null)
                    client = listener.AcceptTcpClient();
            }
            catch (Exception ex)
            {

            }
        }
        TcpClient handler = null;

        TcpListener listener;
        Thread tt;
        NetworkStream stream;
        public void StartListening(ref bool isRunning)
        {
            listener = new TcpListener(port);
            Byte[] bytes = new Byte[256];

            if (!alredyOpened)
                listener.Start();
                while (isRunning)
                {
                    AccptTcpClnt t = new AccptTcpClnt(AccptClnt);
                    Thread tt = new Thread(() => t(ref handler, listener));
                    tt.IsBackground = true;
                    tt.Start(); //the AcceptTcpClient() is a blocking method, so we are invoking it    in a seperate thread so that we can break the while loop wher isRunning becomes false

                    while (isRunning && tt.IsAlive && handler == null)
                        Thread.Sleep(0); //change the time as you prefer

                    if (handler != null)
                    {
                        string data = null;
                        stream = handler.GetStream();
                        ms.log("Connected " + handler.Client.RemoteEndPoint);
                        int i=0;
                        try
                        {
                            String Message = "";
                            ms.log("Waiting Data");
                            Int32 da = stream.Read(bytes, 0, bytes.Length);
                            while (true)
                            {

                                if (da == 0)
                                    break;
                                Message = Message + System.Text.Encoding.ASCII.GetString(bytes, 0, da);
                                if (da < 256)
                                    break;
                                da = stream.Read(bytes, 0, bytes.Length);
                            }
                            ms.tambahData(Message);
                            stream.Close();
                        }
                        catch (Exception ui)
                        {
                            stream.Close();
                        }
                    }
                    else if (!isRunning && tt.IsAlive)
                    {
                        tt.Abort();
                    }
                    handler = null;
                }
                listener.Stop();
        }

        public bool cekPort(string port)
        {
            using (TcpClient tcpClient = new TcpClient())
            {
                try
                {
                    tcpClient.Connect("127.0.0.1", int.Parse(port));
                    return true;
                }
                catch (Exception)
                {
                    Console.WriteLine("Port closed");
                    return false;
                }
            }
            return false;
        }
    }
}
    
