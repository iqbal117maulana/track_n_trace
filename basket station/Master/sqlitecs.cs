﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Finisar.SQLite;
using System.IO;
using System.Windows.Forms;

namespace Basket_Station
{
    class sqlitecs
    {
        private SQLiteConnection sql_con;
        private SQLiteCommand sql_cmd;
        private SQLiteDataAdapter DB;
        private SQLiteDataReader datareader;
        public DataTable dataHeader;
        private DataSet DS = new DataSet();
        private DataTable DT = new DataTable();
        public int num_rows = 0;
        public List<string> dataInsert = new List<string>();
        public List<string> dataUpdate = new List<string>();
        public Dictionary<string, string> config = new Dictionary<string, string>();

        public sqlitecs()
        {
            SetConnection();
            getConfig();
        }


        private void SetConnection()
        {
            sql_con = new SQLiteConnection
                ("Data Source=config.db;Version=3;New=False;Compress=True;");
        }

        public void ExecuteQuery(string txtQuery)
        {
            sql_con.Close();
            try
            {
                SetConnection();
                sql_con.Open();
                sql_cmd = sql_con.CreateCommand();
                sql_cmd.CommandText = txtQuery;
                sql_cmd.ExecuteNonQuery();
                sql_con.Close();
            }
            catch (SQLiteException sq)
            {
                simpanLog("Error : " + sq.ToString());
                //new Confirm("Error SQLLite : " + sq.ToString(), "Error");
            }
            finally
            {
                sql_con.Close();
            }
        }

        public void Add(string data)
        {
            string txtSQLQuery = "insert into  innerbox (innerbox) values ('" + data + "')";
            ExecuteQuery(txtSQLQuery);
        }

        public void insert(Dictionary<string, string> field, string table)
        {
            string sql = "INSERT INTO " + table + " (";
            int i = 0;
            foreach (string key in field.Keys)
            {
                sql += "'" + key + "'";
                if (i + 1 < field.Count)
                {
                    sql += ",";
                    i++;
                }
            }

            sql += ") values (";

            i = 0;
            foreach (string key in field.Values)
            {
                sql += "'" + key + "'";
                if (i + 1 < field.Count)
                {
                    sql += ",";
                    i++;
                }
            }
            sql += ")";
            simpanLog(sql);
            ExecuteQuery(sql);
        }

        public void simpanLog(String line)
        {
            DateTime now = DateTime.Now;
            string tahun = now.ToString("yyyy");
            string bulan = now.ToString("MM");
            string hari = now.ToString("dd");
            string dates = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            line = dates + "\t" + line;
            // cek file 
            bool cekfile = false;
            while (!cekfile)
            {
                if (Directory.Exists("eventlog"))
                {
                    if (Directory.Exists("eventlog" + @"\" + tahun))
                    {
                        if (Directory.Exists("eventlog" + @"\" + tahun + @"\" + bulan))
                        {

                            using (StreamWriter outputFile = new StreamWriter("eventlog" + @"\" + tahun + @"\" + bulan + @"\" + hari + ".txt", true))
                            {
                                outputFile.WriteLine(line);
                            }
                            cekfile = true;
                        }
                        else
                        {

                            Directory.CreateDirectory("eventlog" + @"\" + tahun + @"\" + bulan);
                        }
                    }
                    else
                    {
                        Directory.CreateDirectory("eventlog" + @"\" + tahun);
                    }
                }
                else
                {

                    Directory.CreateDirectory("eventlog");
                }
            }
        }

        private void getConfig()
        {
            string sql = "Select config.portServer," +
                            "config.Servername," +
                            "config.ipprinter," +
                            "config.portprinter," +
                            "config.ipCamera," +
                            "config.portCamera," +
                            "config.ipDB," +
                            "config.portDB," +
                            "config.namaDB," +
                            "config.usernameDB," +
                            "config.passDB," +
                            "config.ipServer," +
                            "config.portlisten," +
                            "config.timeout," +
                            "config.qtyFullBasket," +
                            "config.compId" +
                            " From config";
            sql_con.Open();
            sql_cmd = new SQLiteCommand(sql, sql_con);
            datareader = sql_cmd.ExecuteReader();
            while (datareader.Read())
            {
                config.Add("portserver", datareader.GetValue(0).ToString());
                config.Add("Servername", datareader.GetValue(1).ToString());
                config.Add("ipprinter", datareader.GetValue(2).ToString());
                config.Add("portprinter", datareader.GetValue(3).ToString());
                config.Add("ipCamera", datareader.GetValue(4).ToString());
                config.Add("portCamera", datareader.GetValue(5).ToString());
                config.Add("ipDB", datareader.GetValue(6).ToString());
                config.Add("portDB", datareader.GetValue(7).ToString());
                config.Add("namaDB", datareader.GetValue(8).ToString());
                config.Add("usernameDB", datareader.GetValue(9).ToString());
                config.Add("passDB", datareader.GetValue(10).ToString());
                config.Add("ipServer", datareader.GetValue(11).ToString());
                config.Add("portlisten", datareader.GetValue(12).ToString());
                config.Add("timeout", datareader.GetValue(13).ToString());
                config.Add("qtyfullbasket", datareader.GetValue(14).ToString());
                config.Add("compId", datareader.GetValue(15).ToString());
            }
            sql_con.Close();
        }

        public void update(Dictionary<string, string> field, string table, string where)
        {

            string sql = "UPDATE " + table + " SET";
            int i = 0;
            foreach (KeyValuePair<string, string> key in field)
            {
                sql += "'" + key.Key + "' = " + "'" + key.Value + "'";
                if (i + 1 < field.Count)
                {
                    sql += ",";
                    i++;
                }
            }
            if (where.Length > 0)
                sql += " WHERE " + where + "";
            simpanLog(sql);
            ExecuteQuery(sql);
        }

        public void delete(string where, string table)
        {
            string sql = "DELETE FROM " + table;

            if (where.Length > 0)
                sql += " WHERE " + where;

            simpanLog(sql);
            ExecuteQuery(sql);
        }

        public List<string[]> select(List<string> field, string table, string where)
        {
            sql_con.Close();

            try
            {
                string sql = "SELECT ";
                for (int j = 0; j < field.Count; j++)
                {
                    sql += field[j];
                    if (j + 1 < field.Count)
                    {
                        sql += ",";
                    }
                }
                sql += " From " + table;

                if (where.Length > 0)
                    sql += " WHERE " + where + "";

                int num = 0;
                simpanLog(sql);
                sql_con.Open();
                sql_cmd = new SQLiteCommand(sql, sql_con);
                datareader = sql_cmd.ExecuteReader();
                List<String[]> Results = new List<String[]>();

                while (datareader.Read())
                {
                    string[] data = new string[datareader.FieldCount];
                    for (int u = 0; u < datareader.FieldCount; u++)
                    {
                        data[u] = datareader.GetValue(u).ToString();
                    }

                    Results.Add(data);
                    num++;
                }
                num_rows = num;
                dataHeader = new DataTable();
                for (int i = 0; i < datareader.FieldCount; i++)
                {
                    dataHeader.Columns.Add(datareader.GetName(i));
                }
                sql_con.Close();
                return Results;
            }
            catch(Exception e)
            {
                simpanLog(e.Message);
                simpanLog(where);
                List<String[]> Results = new List<String[]>();
                return Results;
            }
            finally
            {
                sql_con.Close();
            }
        }

        public List<string[]> checkScanned(string innerBox, string innerboxGsid)
        {
            try
            {
                string sql = "SELECT innerboxid From tmp_captured WHERE innerboxid = @innerBox or innerboxGsid = @innerboxGsid";
                int num = 0;
                simpanLog(sql);
                sql_con.Open();
                sql_cmd = sql_con.CreateCommand();
                sql_cmd.CommandText = sql;
                sql_cmd.Parameters.Add("@innerBox", System.Data.SqlDbType.Char);
                sql_cmd.Parameters.Add("@innerboxGsid", System.Data.SqlDbType.Char);
                sql_cmd.Parameters["@innerBox"].Value = innerBox;
                sql_cmd.Parameters["@innerboxGsid"].Value = innerboxGsid;
                datareader = sql_cmd.ExecuteReader();
                List<String[]> Results = new List<String[]>();

                while (datareader.Read())
                {
                    string[] data = new string[datareader.FieldCount];
                    for (int u = 0; u < datareader.FieldCount; u++)
                    {
                        data[u] = datareader.GetValue(u).ToString();
                    }

                    Results.Add(data);
                    num++;
                }
                num_rows = num;
                dataHeader = new DataTable();
                for (int i = 0; i < datareader.FieldCount; i++)
                {
                    dataHeader.Columns.Add(datareader.GetName(i));
                }
                sql_con.Close();
                return Results;
            }
            catch (Exception e)
            {
                simpanLog(e.Message);
                List<String[]> Results = new List<String[]>();
                return Results;
            }
            finally
            {
                sql_con.Close();
            }
        }

        public bool insertScanned(string innerBox, string innerboxGsid, string timestamp)
        {
            try
            {
                string sql = "INSERT INTO tmp_captured (timestamp,innerBoxId,innerboxGSid) values (@timestamp, @innerBox, @innerboxGsid)";
                int num = 0;
                simpanLog(sql);
                sql_con.Open();
                sql_cmd = sql_con.CreateCommand();
                sql_cmd.CommandText = sql;
                sql_cmd.Parameters.Add("@timestamp", System.Data.SqlDbType.Char);
                sql_cmd.Parameters.Add("@innerBox", System.Data.SqlDbType.Char);
                sql_cmd.Parameters.Add("@innerboxGsid", System.Data.SqlDbType.Char);
                sql_cmd.Parameters["@timestamp"].Value = timestamp;
                sql_cmd.Parameters["@innerBox"].Value = innerBox;
                sql_cmd.Parameters["@innerboxGsid"].Value = innerboxGsid;
                sql_cmd.ExecuteNonQuery();
                
                return true;
            }
            catch (Exception e)
            {
                simpanLog(e.Message);
                return false;
            }
            finally
            {
                sql_con.Close();
            }
        }


        public void insertData(Dictionary<string, string> field, string table)
        {
            string sql = "INSERT INTO " + table + " (";
            int i = 0;
            foreach (string key in field.Keys)
            {
                sql += "" + key + "";
                if (i + 1 < field.Count)
                {
                    sql += ",";
                    i++;
                }
            }

            sql += ") values (";

            i = 0;
            foreach (string key in field.Values)
            {
                sql += "'" + key + "'";
                if (i + 1 < field.Count)
                {
                    sql += ",";
                    i++;
                }
            }
            sql += ")";
            simpanLog(sql);
            dataInsert.Add(sql);
        }

        public void Begin()
        {
            //string sql = "BEGIN TRANSACTION;\n ";
            string sql = "";

            for (int i = 0; i < dataInsert.Count; i++)
            {
                sql += dataInsert[i] + "\n";
            }
            //sql += "COMMIT;";
             simpanLog(sql);
            
            ExecuteQuery(sql);
            dataInsert = new List<string>();
        }

        public void BeginUpdate()
        {
            string sql = "BEGIN TRANSACTION;\n ";

            for (int i = 0; i < dataUpdate.Count; i++)
            {
                sql += dataUpdate[i] + "\n";
            }
            sql += "COMMIT;";
            simpanLog(sql);
            ExecuteQuery(sql);
            dataUpdate = new List<string>();
        }

        public void updateData(Dictionary<string, string> field, string table, string where)
        {

            string sql = "UPDATE " + table + " SET";
            int i = 0;
            foreach (KeyValuePair<string, string> key in field)
            {
                sql += "'" + key.Key + "' = " + "'" + key.Value + "'";
                if (i + 1 < field.Count)
                {
                    sql += ",";
                    i++;
                }
            }
            if (where.Length > 0)
                sql += " WHERE  " + where + ";";
            dataUpdate.Add(sql);
        }

        public string[] getDatacap()
        {
            List<string> field = new List<string>();
            string[] temp;
            field.Add("capid");
            string where = "isused = '0' or isused = '1'";
            List<string[]> ds = select(field, "tmp_cap_id", where);
            temp = new string[num_rows];
            int i = 0;
            foreach (string[] datac in ds)
            {
                temp[i] = datac[0];
                i++;
            }
            return temp;
        }

        public List<string[]> getDatagsone()
        {
            List<string> field = new List<string>();
            List<string[]> temp;
            field.Add("gtin,batchnumber,expire,sn");
            string where = "isused = '0'";
            List<string[]> ds = select(field, "tmp_gsOneVial", where);
            return ds;
        }

        public int getDatacapAll()
        {
            List<string> field = new List<string>();
            string[] temp;
            field.Add("capid");
            string where = "isused = '0' or isused = '1'";
            List<string[]> ds = select(field, "tmp_cap_id", "");
            temp = new string[num_rows];
            int i = 0;
            foreach (string[] datac in ds)
            {
                temp[i] = datac[0];
                i++;
            }
            return temp.Length;
        }

        public int getDatagsoneAll()
        {
            List<string> field = new List<string>();
            List<string[]> temp;
            field.Add("gtin,batchnumber,expire,sn");
            string where = "isused = '0'";
            List<string[]> ds = select(field, "tmp_gsOneVial", "");
            return ds.Count;
        }

        public List<string> getDatagsonedetail()
        {
            List<string> field = new List<string>();
            List<string> temp = new List<string>();
            field.Add("gsone");
            string where = "isused = '0' or isused = '1'";
            List<string[]> ds = select(field, "tmp_gsOneVial", where);

            foreach (string[] da in ds)
            {
                temp.Add(da[0]);
            }
            return temp;
        }

        public void updatestatusconfig(string batch)
        {
            Dictionary<string, string> field = new Dictionary<string, string>();
            field.Add("batchnumber", batch);
            update(field, "config", "");
        }

        public List<string[]> getcaptable()
        {
            List<string> field = new List<string>();
            field.Add("capid 'Cap ID'");
            List<string[]> ds = select(field, "tmp_cap_id", "isused = '0'");
            return ds;
        }

        public int getCountCap(string status)
        {
            List<string> field = new List<string>();
            field.Add("count(*)");
            string where = "isused " + status + "";
            List<string[]> ds = select(field, "tmp_cap_id", where);
            int temp = int.Parse(ds[0][0]);
            return temp;
        }

        public int getCountGs(string status)
        {
            List<string> field = new List<string>();
            field.Add("count(*)");
            string where = "isused " + status + "";
            List<string[]> ds = select(field, "tmp_gsOneVials", where);
            int temp = int.Parse(ds[0][0]);
            return temp;
        }

        public void RefreshData()
        {
            string sql = "DELETE FROM tmp_cap_id";
            ExecuteQuery(sql);
            sql = "Delete from tmp_gsOneVial";
            ExecuteQuery(sql);
            Dictionary<string, string> field = new Dictionary<string, string>();
            field.Add("batchnumber", "");
            update(field, "config", "");
        }


        public int getDatacapAllNotReciv()
        {
            List<string> field = new List<string>();
            string[] temp;
            field.Add("capid");
            string where = "isused <> '2' ";
            List<string[]> ds = select(field, "tmp_cap_id", where);
            temp = new string[num_rows];
            int i = 0;
            foreach (string[] datac in ds)
            {
                temp[i] = datac[0];
                i++;
            }
            return temp.Length;
        }

        public int getDatagsoneNotReciv()
        {
            List<string> field = new List<string>();
            List<string[]> temp;
            field.Add("gtin,batchnumber,expire,sn");
            string where = "isused <> '2'";
            List<string[]> ds = select(field, "tmp_gsOneVial", where);
            return ds.Count;
        }

        public int getCountResult()
        {
            List<string> field = new List<string>();
            field.Add("count(*)");
            List<string[]> ds = select(field, "result", "");
            int temp = int.Parse(ds[0][0]);
            return temp;
        }

    }
}
