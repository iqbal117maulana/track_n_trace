﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Configuration;
using System.Windows.Forms;
using System.Data;
using System.IO;
using System.Diagnostics;
using System.Security.Cryptography;
using System.Net;
using System.Net.Sockets;
using System.Net.NetworkInformation;
using System.Text.RegularExpressions;
using System.Security.AccessControl;

namespace Basket_Station
{
    public class dbaccess
    {
        public static SqlConnection Connection;
        public static SqlCommand Command;
        public static SqlDataReader DataReader;
        public static SqlDataAdapter dataadapter;
        public DataTable dataHeader ;
        public int num_rows = 0;
        sqlitecs sqlite;

        public string ipAddress;
        public string eventname;
        string eventtype;
        public bool sysLog = true;
        public string adminid;
        public string from;
        public string valid;
        public string movementType = "6";

        public int fcon = 0;
        public string errorMessage;
        public string role;

        public dbaccess()
        {
            sqlite = new sqlitecs();
            Connect();
        }

        public bool Connect()
        {
            //if (!isConnect)
            //{
            try
            {
                string connec = "Data Source=" + sqlite.config["ipDB"] + ";" +
                                "Initial Catalog=" + sqlite.config["namaDB"] + ";" +
                                "User id=" + sqlite.config["usernameDB"] + ";" +
                                //"Password=" + sqlite.config["passDB"] + ";" +
                                //"Password=docotronics;" +
                                "Password=Biofarma2020#;" + 
                                 "Connection Timeout=" + sqlite.config["timeout"] + ";";
                Connection = new SqlConnection(connec);
                Connection.Open();
                fcon = 0;
                errorMessage = "";
                return true;
            }
            catch (TimeoutException t)
            {
                //Console.WriteLine("MASUK CATCH 1");
                fcon++;
                errorMessage = t.Message;
                if (fcon < 2)
                {
                    //Console.WriteLine("MASUK CATCH 1 IF");
                    new Confirm("Can not open connection to Database Server.", "Information", MessageBoxButtons.OK);
                    //config cfg = new config(null);
                    //cfg.ShowDialog();
                }
            }
            catch (SqlException se)
            {
                //Console.WriteLine("MASUK CATCH 2 : " + se.Message);
                fcon++;
                //Console.WriteLine("FCON : " + fcon);
                errorMessage = se.Message;
                if (fcon < 2)
                {
                    //Console.WriteLine("MASUK CATCH 2 IF : " + se.Message);
                    errorMessage = se.Message;
                    new Confirm("Can not open connection to Database Server.", "Information", MessageBoxButtons.OK);
                    //config cfg = new config(null);
                    //cfg.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                //Console.WriteLine("MASUK CATCH 3");
                fcon++;
                errorMessage = ex.Message;
                if (fcon < 2)
                {
                    //Console.WriteLine("MASUK CATCH 3 IF");
                    new Confirm("Can not open connection to Database Server.", "Information", MessageBoxButtons.OK);
                    simpanLog("Error SQLSERVER : " + ex.ToString());
                    //config cfg = new config(null);
                    //cfg.ShowDialog();
                }
            }
            return false;
            //}
        }

        public void closeConnection()
        {
            Connection.Close();
        }

        public static List<String[]> LoadProductionOrders()
        {
            List<String[]> Results = new List<String[]>();
            String Query = "SELECT production_order_number, created_date, status FROM transaction_production_order";

            Connection.Open();
            Command = new SqlCommand(Query, Connection);
            DataReader = Command.ExecuteReader();

            while (DataReader.Read())
            {
                String[] Data = new String[] { DataReader.GetValue(0).ToString(), DataReader.GetValue(1).ToString(), DataReader.GetValue(2).ToString() };
                Results.Add(Data);
            }

            DataReader.Close();
            Command.Dispose();
            Connection.Close();

            return Results;
        }

        public string[] dataPO(string batch)
        {
            List<string> field = new List<string>();
            field.Add("gtin,expired,packagingqty,model_name,product_model");
            string from = "packaging_order INNER JOIN product_model ON packaging_order.productModelId = product_model.product_model";
            string where = "batchnumber = '" + batch + "'";
            List<string[]> ds = selectList(field, from, where);
            return ds[0];
        }
        

        public void insert(Dictionary<string, string> field, string table)
        {
            try
            {
                string sql = "INSERT INTO " + table + " (";
                int i = 0;
                foreach (string key in field.Keys)
                {
                    sql += key;
                    if (i + 1 < field.Count)
                    {
                        sql += ",";
                        i++;
                    }
                }

                sql += ") values (";

                i = 0;
                foreach (string key in field.Values)
                {
                    sql += "'" + key + "'";
                    if (i + 1 < field.Count)
                    {
                        sql += ",";
                        i++;
                    }
                }
                sql += ")";
                simpanLog(sql);
                Connection.Open();
                Command = new SqlCommand(sql, Connection);
                Command.ExecuteNonQuery();
                Connection.Close();
            }
            catch (SqlException sq)
            {
                simpanLog("Error SQl : " + sq.ToString());
            }
            finally
            {
                Connection.Close();
            }
        }

        //public void update(Dictionary<string, string> field, string table, string where)
        //{
        //    try
        //    {
        //        string sql = "UPDATE " + table + " SET ";
        //        int i = 0;
        //        foreach (KeyValuePair<string, string> key in field)
        //        {
        //            sql += key.Key+" = "+"'" + key.Value + "'";
        //            if (i + 1 < field.Count)
        //            {
        //                sql += ",";
        //                i++;
        //            }
        //        }

        //        if (where.Length > 0)
        //            sql += " WHERE " + where;

        //        simpanLog(sql);
        //        Connection.Open();
        //        Command = new SqlCommand(sql, Connection);
        //        Command.ExecuteNonQuery();
        //    }
        //    catch (SqlException sq)
        //    {
        //        simpanLog("Error SQl : " + sq.ToString());
        //    }
        //    finally
        //    {
        //        Connection.Close();
        //    }
        //}

        public void update(Dictionary<string, string> field, string table, string where)
        {
            try
            {
                string sql = "UPDATE " + table + " SET ";
                int i = 0;
                foreach (KeyValuePair<string, string> key in field)
                {
                    if (key.Value.Equals("null"))
                    {
                        sql += key.Key + " = " + key.Value;
                    }
                    else
                    {
                        sql += key.Key + " = " + "'" + key.Value + "'";
                    }
                    if (i + 1 < field.Count)
                    {
                        sql += ",";
                        i++;
                    }
                }

                if (where.Length > 0)
                    sql += " WHERE " + where;

                simpanLog(sql);
                eventtype = "1";
                Connection.Open();
                Command = new SqlCommand(sql, Connection);
                Command.ExecuteNonQuery();
                Connection.Close();
            }
            catch (SqlException sq)
            {
                simpanLog("Error SQl : " + sq.ToString());
                Connection.Close();
            }
        }


        public DataSet select(List<string> field, string table, string where)
        {
            try
            {
                string sql = "SELECT ";
                for (int j = 0; j < field.Count; j++)
                {
                    sql += "'" + field[j] + "'";
                    if (j + 1 < field.Count)
                    {
                        sql += ",";
                        j++;
                    }
                }

                sql += " From " + table;

                if (where.Length > 0)
                    sql += " WHERE " + where;

                simpanLog(sql);
                Connection.Open();
                dataadapter = new SqlDataAdapter(sql, Connection);
                DataSet Results = new DataSet();
                dataadapter.Fill(Results);
                return Results;
            }
            catch (SqlException sq)
            {
                simpanLog("Error SQLSERVER : " + sq.ToString());
            }
            finally
            {
                try
                {
                    DataReader.Close();
                    Command.Dispose();
                    Connection.Close();
                }
                catch (SqlException sq)
                {
                    simpanLog("Error SQLSERVER : " + sq.ToString());
                }
                catch (NullReferenceException nl)
                {
                    simpanLog("Error SQLSERVER : " + nl.ToString());
                }

            }
            return null;
        }
        
        public List<String[]> selectList(List<string> field, string table, string where)
        {
            try
            {
                string sql = "SELECT ";
                for (int j = 0; j < field.Count; j++)
                {
                    sql +=  field[j] ;
                    if (j + 1 < field.Count)
                    {
                        sql += ",";
                    }
                }
                sql += " FROM " + table;

                if (where.Length > 0)
                    sql += " WHERE " + where;

                simpanLog(sql);
                List<String[]> Results = new List<String[]>();
                int num = 0;
                if (Connect())
                {
                    Command = new SqlCommand(sql, Connection);
                    DataReader = Command.ExecuteReader();
                    while (DataReader.Read())
                    {
                        string[] data = new string[DataReader.FieldCount];
                        for (int u = 0; u < DataReader.FieldCount; u++)
                        {
                            data[u] = DataReader.GetValue(u).ToString();
                        }

                        Results.Add(data);
                        num++;
                    }

                    num_rows = num;
                    dataHeader = new DataTable();
                    for (int i = 0; i < DataReader.FieldCount; i++)
                    {
                        dataHeader.Columns.Add(DataReader.GetName(i));
                    }
                }
                closeConnection();
                return Results;
                
            }
            catch (SqlException sq)
            {
                var st = new StackTrace(sq, true);
                var frame = st.GetFrame(0);
                string line = frame.ToString();
                simpanLog("Error  00100: " + line);
            }
            catch (NullReferenceException nl)
            {

                var st = new StackTrace(nl, true);
                var frame = st.GetFrame(0);
                string line = frame.ToString();
                simpanLog("Error  00100: " + line);
            }
            finally
            {
                try
                {
                    DataReader.Close();
                    Command.Dispose();
                    Connection.Close();
                }
                catch (SqlException sq)
                {
                    simpanLog("Error SQLSERVER : " + sq.ToString());
                }
                catch (NullReferenceException nl)
                {
                    simpanLog("Error SQLSERVER : " + nl.ToString());
                }

            }
            return null;
        }

        public string[] GetBatchNumberDetails(string batchNumber)
        {
            List<string> fields = new List<string>();
            fields.Add("PO.batchNumber");
            fields.Add("PO.expired");
            fields.Add("PO.productmodelId");
            fields.Add("PM.model_name");
            fields.Add("PM.gtinbasket");
            string from = "packaging_order PO INNER JOIN product_model PM ON PO.productModelId = PM.product_model";
            string where = "PO.batchNumber = '" + batchNumber + "'";

            List<string[]> resultList = selectList(fields, from, where);

            if (resultList.Count > 0)
                return resultList[0];
            else
                return null;
        }

        public string GetLastGeneratedSN(string batchNumber)
        {
            List<string> fields = new List<string>();
            fields.Add("basketId, MAX(createdTime)");
            string where = "basketId LIKE '" + batchNumber + "%'";
            where += " GROUP BY basketId ORDER BY createdTime";

            List<string[]> data = selectList(fields, "basket", where);
            if (data != null)
            {
                return data[0][0];
            }
            else
                return null;
        }

        public int GetLastSequence(string batchNumber)
        {
            List<string> field = new List<string>();
            field.Add("basketseq");
            string where = "batchNumber = '" + batchNumber + "'";

            List<string[]> data = selectList(field, "packaging_order", where);
            if (data != null&& data.Count>0)
            {
                string temp = data[0][0];
                return Int32.Parse(temp);
            }
            else
                return 0;
        }

        public void UpdateLastSequence(string batchNumber, int seq)
        {
            Dictionary<string, string> fields = new Dictionary<string, string>();
            fields.Add("basketseq", seq.ToString());
            string where = "batchNumber = '" + batchNumber + "'";

            update(fields, "packaging_order", where);
        }

        public void InsertNewBasketID(string basketId,string batchnumber)
        {
            Dictionary<string, string> fields = new Dictionary<string, string>();
            fields.Add("basketId", basketId);
            fields.Add("createdTime", getUnixString());
            fields.Add("isReject", "1");
            fields.Add("rejectTime", getUnixString());
            fields.Add("isDecommissioned", "0");
            fields.Add("Batchnumber", batchnumber);

            insert(fields, "basket");
        }

        public void simpanLog(String line)
        {
            try
            {
                DateTime now = DateTime.Now;
                string tahun = now.ToString("yyyy");
                string bulan = now.ToString("MM");
                string hari = now.ToString("dd");
                string dates = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                line = dates + "\t" + line;
                // cek file 
                bool cekfile = false;
                while (!cekfile)
                {
                    if (Directory.Exists("eventlog"))
                    {
                        if (Directory.Exists("eventlog" + @"\" + tahun))
                        {
                            if (Directory.Exists("eventlog" + @"\" + tahun + @"\" + bulan))
                            {

                                using (StreamWriter outputFile = new StreamWriter("eventlog" + @"\" + tahun + @"\" + bulan + @"\" + hari + ".txt", true))
                                {
                                    outputFile.WriteLine(line);
                                }
                                cekfile = true;
                            }
                            else
                            {

                                Directory.CreateDirectory("eventlog" + @"\" + tahun + @"\" + bulan);
                            }
                        }
                        else
                        {
                            Directory.CreateDirectory("eventlog" + @"\" + tahun);
                        }
                    }
                    else
                    {

                        Directory.CreateDirectory("eventlog");
                    }
                }
            }
            catch (Exception te)
            {
                //MessageBox.Show("Error");
            }
        }

 
        public string[] cekLine(string where)
        {
            ipAddress = GetLocalIPAddress();
            List<string> field = new List<string>();
            string[] temp = new string[0];
            field.Add("lineName,linePackaging.linePackagingId");
            string from = "linePackagingDetail INNER JOIN linePackaging ON linePackagingDetail.linePackagingId = linePackaging.linePackagingId";
            //string where1 = where + "= '" + GetLocalIPAddress() + "'";
            string where1 = where + "= '172.16.160.105'";
            
            List<string[]> ds = selectList(field, from, where1);
            if (num_rows > 0)
                return ds[0];
            else
                return temp;
        }

        public static string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            throw new Exception("Local IP Address Not Found!");
        }

        public string md5hash(string source)
        {
            using (MD5 md5Hash = MD5.Create())
            {
                string hash = GetMd5Hash(md5Hash, source);
                return hash;
            }
            return null;
        }

        static string GetMd5Hash(MD5 md5Hash, string input)
        {

            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }

        static bool VerifyMd5Hash(MD5 md5Hash, string input, string hash)
        {
            // Hash the input.
            string hashOfInput = GetMd5Hash(md5Hash, input);

            // Create a StringComparer an compare the hashes.
            StringComparer comparer = StringComparer.OrdinalIgnoreCase;

            if (0 == comparer.Compare(hashOfInput, hash))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool validasi(string data, int val)
        {
            //text ip val = 0
            if (val == 0)
                return cekIP(data);
            else if (val == 1)
            {
                //text tidak kosong val =1
                if (data.Length == 0)
                {
                    valid = "Data empty";
                    return false;
                }
            }
            else if (val == 2)
            {
                //text tidak ada angka
                int errorCounter = Regex.Matches(data, @"[a-zA-Z]").Count;
                if (errorCounter > 0)
                {
                    valid = "Data cannot contain numeric";
                    return false;
                }
            }
            else if (val == 3)
            {
                //text tidak ada angka

                if (data.Length > 6)
                    return false;
                bool rege = Regex.Match(data, @"^[0-9]+$").Success;
                if (!rege)
                {
                    valid = "Data cannot contain numeric";
                    return false;
                }
            }
            return true;

        }

        bool cekIP(string data)
        {
            if (data.Length < 3 & data.Length > 0)
                return false;
            int errorCounter = Regex.Matches(data, @"[a-zA-Z]").Count;
            if (errorCounter == 0)
            {
                string[] dapat = data.Split('.');
                if (dapat.Length != 4)
                {
                    valid = "Error : IP must 4 point";
                    return false;
                }
                if (dapat[3].Equals("0"))
                {
                    valid = "Error : the fourth digit of ip cannot be 0";
                    return false;
                }

                bool kosong = false;
                if (dapat[0].Equals("000"))
                    kosong = true;
                for (int i = 0; i < dapat.Length; i++)
                {
                    //if (dapat[i] == "" || dapat[i].Length <= 0)
                    //{
                    //    new Confirm("Error : IP must 4 point");
                    //    return false;
                    //}
                    if (dapat[i][0].Equals("0"))
                        return false;
                    if (dapat[i].Equals("00"))
                    {
                        valid = "Error : IP cannot fill 00";
                        return false;
                    }

                    bool rege = Regex.Match(dapat[i], @"^[0-9]+$").Success;
                    if (!rege)
                    {
                        valid = "Error : IP cannot contain alphabeth and symbols " + data;
                        return false;
                    }
                    if (dapat[i].Length == 0)
                    {
                        valid = "Error : Cannot empty";
                        return false;
                    }
                    int temp = int.Parse(dapat[i]);
                    if (temp >= 255)
                    {
                        valid = "Error : IP must be less then 255";
                        return false;
                    }
                    if (temp == 0 && kosong)
                    {
                        valid = "Error : First digit of ip cannot be 0";
                        return false;
                    }
                    if (dapat[i].Equals("000") && !kosong)
                    {
                        valid = "Error : IP cannot fill 000";
                        return false;
                    }
                    if (dapat[i][0].Equals('0') && temp != 0 && dapat[i].Length > 1)
                    {
                        valid = "Error : Ip cannot fill '0'";
                        return false;
                    }
                }
                return true;
            }
            else
            {
                valid = "Error : IP cannot contain alphabeth " + errorCounter;
                return false;
            }
            return true;
        }

        public void systemlog()
        {
            Dictionary<string, string> field = new Dictionary<string, string>();
            field.Add("eventtime", getUnixString());
            field.Add("eventname", eventname);
            field.Add("userid", adminid);
            field.Add("[from]", from);
            field.Add("eventtype", eventtype);
            insert(field, "system_log");
        }

        public bool PingHost(string nameOrAddress)
        {
            bool pingable = false;
            Ping pinger = new Ping();
            try
            {
                PingReply reply = pinger.Send(nameOrAddress);
                pingable = reply.Status == IPStatus.Success;
            }
            catch (PingException)
            {
                // Discard PingExceptions and return false;
            }
            return pingable;
        }
      
        public string getUnixString()
        {
            string date = "" + (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1)).TotalSeconds);
            return date;
        }

        public string getBatchFromLine(string line)
        {
            List<string> field = new List<string>();
            field.Add("batchnumber");
            string where = "linepackagingid ='"+line+"' and status = '5'";
            List<string[]> ds = selectList(field, "packaging_order", where);
            if (num_rows > 0)
            {
                return ds[0][0];
            }
            else
            {
                return "";
            }
        }

        public bool checkInnerBox(string innerboxid)
        {
             List<string> fieldlist = new List<string>();
            fieldlist.Add("infeedInnerBoxId");
            selectList(fieldlist, "innerBox", "infeedInnerBoxId = '" + innerboxid + "' OR gsOneInnerBoxId ='" + innerboxid + "'");
            if (num_rows > 0)
            {
                return true;
            }
            return false;
        }

        public bool decomission(string blister)
        {
            List<string> fieldlist = new List<string>();
            fieldlist.Add("basketid");
            selectList(fieldlist, "basket", "basketid = '" + blister + "'");
            if (num_rows > 0)
            {
                Dictionary<string, string> field = new Dictionary<string, string>();
                field.Add("basketId", "null");
                string where = "basketId = '" + blister + "'";
                update(field, "Vaccine", where);

                field = new Dictionary<string, string>();
                field.Add("isreject", "2"); 
                where = "basketId = '" + blister + "'";
                update(field, "basket", where);
                return true;
            }
            else
            {
                Confirm cf = new Confirm("Data Not Found !", "Information", MessageBoxButtons.OK);
                return false;
            }
        }

        public bool decomission_innerbox(string blister)
        {
            List<string> fieldlist = new List<string>();
            fieldlist.Add("infeedInnerBoxId");
            selectList(fieldlist, "innerBox", "infeedInnerBoxId = '" + blister + "' OR gsOneInnerBoxId ='" + blister + "'");
            if (num_rows > 0)
            {
                Dictionary<string, string> field = new Dictionary<string, string>();
                field.Add("innerBoxId", "null");
                field.Add("innerBoxGsOneId", "null");
                string where = "innerBoxId = '" + blister + "' OR innerBoxGsOneId ='" + blister + "'";
                update(field, "Vaccine", where);

                field = new Dictionary<string, string>();
                field.Add("isreject", "2");
                where = "infeedInnerBoxId = '" + blister + "' OR gsOneInnerBoxId ='" + blister + "'";
                update(field, "innerBox", where);
                return true;
            }
            else
            {
                Confirm cf = new Confirm("Data Not Found !", "Information", MessageBoxButtons.OK);
                return false;
            }
        }

        public void Movement(string order, string from, string to, string qty, string itemid)
        {
            string dates = DateTime.Now.ToString("ssMMddmmHHssyy");
            Dictionary<string, string> field = new Dictionary<string, string>();
            field.Add("movementId", dates);
            field.Add("orderNumber", order);
            field.Add("[from]", from);
            field.Add("movementType", movementType);
            field.Add("[to]", to);
            field.Add("qty", qty);
            field.Add("uom", "Innerbox");
            field.Add("userid", adminid);
            field.Add("itemid", itemid);
            field.Add("eventTime", getUnixString());
            insert(field, "movement_history");
        }

        internal bool cekPermision(string adminid, string modul_name, string permission)
        {
            List<string> field = new List<string>();
            field.Add("permissions_name");
            string where = "dbo.users.id = '" + adminid + "' AND dbo.permissions.module_name = '" + modul_name + "' AND dbo.permissions.permissions_name = '" + permission + "'";
            string from = "dbo.groups INNER JOIN dbo.permissions ON dbo.groups.id = dbo.permissions.role_id INNER JOIN dbo.users_groups ON dbo.users_groups.group_id = dbo.groups.id  INNER JOIN dbo.users ON dbo.users_groups.user_id = dbo.users.id";
            selectList(field, from, where);
            if (num_rows > 0)
                return true;
            return false;
        }


        public bool validasi(string username)
        {
            try
            {
                if (username.Length > 0)
                {
                    List<string> field = new List<string>();
                    field.Add("id");
                    string where = "username = '" + username + "'";
                    List<string[]> result = selectList(field, "[users]", where);
                    if (result.Count > 0)
                    {
                        return true;
                    }
                    else
                        return false;
                }
                else
                {
                    return false;
                }
            }
            catch (NullReferenceException nl)
            {
                simpanLog("Error  00100: " + nl.Message);
                return false;
            }

        }

        public List<string[]> validasi(string username, string password, string status)
        {
            try
            {
                if (username.Length > 0 && password.Length > 0)
                {
                    List<string> field = new List<string>();
                    field.Add("id");
                    string where = "username = '" + username + "' AND password = '" + md5hash(password) + "' AND Active = " + status;
                    List<string[]> result = selectList(field, "[users]", where);
                    if (result.Count > 0)
                    {
                        from = "Bottle Station";
                        adminid = result[0][0];
                        eventtype = "1";
                        eventname = "Login";
                        systemlog();
                        return result;
                    }
                    else
                        return null;
                }
                else
                {
                    string code = "0010";
                    simpanLog(code + " : USERNAME DAN PASSWORD SALAH");
                    return null;
                }
            }
            catch (NullReferenceException nl)
            {
                var st = new StackTrace(nl, true);
                var frame = st.GetFrame(0);
                string line = frame.ToString();
                simpanLog("Error  00100: " + line);
                return null;
            }

        }

        public bool validasi(string username, string password)
        {
            try
            {
                if (username.Length > 0 && password.Length > 0)
                {
                    List<string> field = new List<string>();
                    field.Add("id");
                    string where = "username = '" + username + "' AND password = '" + md5hash(password) + "'";
                    List<string[]> result = selectList(field, "[users]", where);
                    if (result.Count > 0)
                    {
                        return true;
                    }
                    else
                        return false;
                }
                else
                {
                    string code = "0010";
                    simpanLog(code + " : USERNAME DAN PASSWORD SALAH");
                    return false;
                }
            }
            catch (NullReferenceException nl)
            {
                var st = new StackTrace(nl, true);
                var frame = st.GetFrame(0);
                string line = frame.ToString();
                simpanLog("Error  00100: " + line);
                return false;
            }

        }

        //tambahan
        internal List<string[]> getRole(string adminid)
        {
            List<string> field = new List<string>();
            field.Add("a.first_name, c.name, c.id as role");
            string where = "a.id = '" + adminid + "';";
            string from = "users a inner join users_groups b on a.id = b.user_id inner join groups c on b.group_id = c.id";
            List<string[]> result = selectList(field, from, where);
            Connection.Close();
            if (result.Count > 0)
            {
                role = result[0][2].ToString();
                return result;
            }
            else
                return null;
        }

        public void insertDecommission(string blister)
        {
            try
            {
                Console.WriteLine("MASUK INSERT DECOMM");
                string sql = "INSERT INTO [Vaccine_Reject] (capId, gsOneVialId, blisterpackId, createdTime, lineNumber, batchNumber, isReject, productModelId, expDate, flag) " +
                                "SELECT capId, gsOneVialId, blisterpackId, createdTime, lineNumber, batchNumber, isReject = 5, productModelId, expDate, flag from [Vaccine] where blisterpackId = '" + blister + "'";

                simpanLog(sql);
                eventtype = "0";
                Connect();
                Command = new SqlCommand(sql, Connection);
                Command.ExecuteNonQuery();
                Connection.Close();
            }
            catch (Exception sq)
            {
                Connection.Close();
                simpanLog("Error SQl : " + sq.ToString());
            }
        }

        public int selectCountBasket(string batchNumber)
        {
            try
            {
                string sql = "SELECT Count(*) from (select a.innerBoxGsOneId from [Vaccine] a where a.batchNumber = '" + batchNumber + "' and a.isReject = 0 and a.innerBoxGsOneId is not null and basketId is not null group by a.innerBoxGsOneId) as m";

                simpanLog(sql);
                Connect();
                Command = new SqlCommand(sql, Connection);
                DataReader = Command.ExecuteReader();
                int Results = 0;
                while (DataReader.Read())
                {
                    string[] data = new string[DataReader.FieldCount];
                    for (int u = 0; u < DataReader.FieldCount; u++)
                    {
                        data[u] = DataReader.GetValue(u).ToString();
                    }

                    Results = Int32.Parse(data[0]);
                }
                closeConnection();
                return Results;
            }
            catch (SqlException sq)
            {
                simpanLog("Error SQLSERVER : " + sq.ToString());
            }
            finally
            {
                try
                {
                    DataReader.Close();
                    Command.Dispose();
                    Connection.Close();
                }
                catch (SqlException sq)
                {
                    simpanLog("Error SQLSERVER : " + sq.ToString());
                }
                catch (NullReferenceException nl)
                {
                    simpanLog("Error SQLSERVER : " + nl.ToString());
                }

            }
            return 0;
        }

        public int selectCountReject(string batchNumber)
        {
            try
            {
                string sql = "SELECT Count(*) from (select a.blisterpackId, a.isReject from [Vaccine_Reject] a where a.batchNumber = '" + batchNumber + "' and a.blisterpackId is not null group by a.blisterpackId, a.isReject) as m";

                simpanLog(sql);
                Connect();
                Command = new SqlCommand(sql, Connection);
                DataReader = Command.ExecuteReader();
                int Results = 0;
                while (DataReader.Read())
                {
                    string[] data = new string[DataReader.FieldCount];
                    for (int u = 0; u < DataReader.FieldCount; u++)
                    {
                        data[u] = DataReader.GetValue(u).ToString();
                    }

                    Results = Int32.Parse(data[0]);
                }

                return Results;
            }
            catch (SqlException sq)
            {
                simpanLog("Error SQLSERVER : " + sq.ToString());
            }
            finally
            {
                try
                {
                    DataReader.Close();
                    Command.Dispose();
                }
                catch (SqlException sq)
                {
                    simpanLog("Error SQLSERVER : " + sq.ToString());
                }
                catch (NullReferenceException nl)
                {
                    simpanLog("Error SQLSERVER : " + nl.ToString());
                }

            }
            return 0;
        }

        public int selectCountVaccine(string batchNumber, string blister)
        {
            try
            {
                string sql = "SELECT Count(*) from [Vaccine] where batchNumber = '" + batchNumber + "' and isReject = 0 and blisterpackId = '" + blister + "'";

                simpanLog(sql);
                if (Connect())
                {
                    Command = new SqlCommand(sql, Connection);
                    DataReader = Command.ExecuteReader();
                    int Results = 0;
                    while (DataReader.Read())
                    {
                        string[] data = new string[DataReader.FieldCount];
                        for (int u = 0; u < DataReader.FieldCount; u++)
                        {
                            data[u] = DataReader.GetValue(u).ToString();
                        }

                        Results = Int32.Parse(data[0]);
                    }
                    Connection.Close();
                    return Results;
                }
            }
            catch (SqlException sq)
            {
                simpanLog("Error SQLSERVER : " + sq.ToString());
            }
            finally
            {
                try
                {
                    DataReader.Close();
                    Command.Dispose();
                }
                catch (SqlException sq)
                {
                    simpanLog("Error SQLSERVER : " + sq.ToString());
                }
                catch (NullReferenceException nl)
                {
                    simpanLog("Error SQLSERVER : " + nl.ToString());
                }

            }
            return 0;
        }

        //end of tambahan
    }
}