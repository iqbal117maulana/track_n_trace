﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.IO;
using System.Runtime.InteropServices;

namespace Basket_Station
{
    class Printer
    {
        static tcp tcp = new tcp();
        static sqlitecs sql = new sqlitecs();

        private static String ZplTemplate = "^XA~TA000~JSN^LT0^MNW^MTT^PON^PMN^LH0,0^JMA^PR2,3~SD25^JUS^LRN^CI0^XZ\r\n"
            + "^XA\r\n"
            + "^MMT\r\n"
            + "^PW709\r\n"
            + "^LL0709\r\n"
            + "^LS0\r\n"
            + "^FO0,0^GFA,08448,08448,00044,:Z64:eJzt2E1u2zgUB3Aq9ITFwKhmOYug7BG67CIIfRQfIcsuipJBDtArMehByu5mye60EPT6f6SVSJ7Yei4aoDMwIbhm/LNM8ePxsUqdy7mcy7n8x8vqheyr38D+fYL964Xsu9/Abn+53Zx83yRSGLELqV0r9YdqZPbyBKsx0UMTRbYJahW1zKqoVlsjtEmtbqX2g1q/b4X2Wq1vrNC+UWtLSWZbdcU2SKyJVy1lsxHZf64N9R9FbdB9b4g6URsa6jXRN6ltiO5EbVDUKyItt73Q+l75ZMTWhlZsefRktsOLlVka7XIQbijj1WEA46I11QY1LDfBE9/PJ5cXaUtUv0Fh0b4a7y5owuv6D9FyE5Qe7XITxhApaYJSt9X2EntdbSexN+XVCx4NMyHIbY1lLolsuaOV2fJURtaG0ltaZusoiPrMVisKaK7aNxK7mwmSAKF3FnvXYmnHGbZZtm60bxcpwulQv7LcaaYvY2Ekc/0T9gDFrU6LFLGmKSFKsNw4hLF1tNwELiVEiZbmaTY1WW7Ra0LbREAvtAGRUmrv2cr6TGPnDkJ7YYmiF4VJdXHjKX0UZkcRKcwHnSTUYJLl20YUH1a8a90qkeX53m2R0slsvt6qK6FNV9txP1q08XIriiXF/rlROshs0BtR3Cm22UiPAoaTa0Hc2dmN9Gj4uljZkbN27eUJ9kJk1/Pq0TP4+mh1Xvae6v0xu9dbR2fnnj0a4OcH6bKaDj7f3JZd/GAqOrclR/DhgJ0fpO1R+3ZuE14OhqG5LdmMcPso9tMJVnY8Ui7KHBf/UhZpaDMgB8F7i72aU3SDiuv4qvUnO/B+3vLfPH8n83Yd67FpPArsCmH7gHX4G5KL0DJA5McZT3Nqb6fZL2zUfEzrcA+KJW9Bho4zwGdOvt0004BNmo9/3A5KOCCgHdRjB/qKr/KBYWaz6QxbfP4dVb5/74hLamhim2pbzoPw+RcqbabOV6tpcgpg2xXLuyndc/Nx/1wo5bkdlIW1eGok8dZQhB30A2X0jM2lPrHIrTK6HTZdIov10XXNHUX0eNuV+mj1zmZFDX6Nc4uIZB6/0uLqbD+ZXJpHrc0+KbrjrsfIJlTMwIkybDe1PS6buSfv0eu4v0uomJ5/AjZPjgGm2ISfrzaxDY+W63HfYlDwlj9wXEEkwCEANk4s/mL27AOpCvetge3ctwGDgrfcQdXmegH6qcW3H9jyyQNBxaPCEaZYrk+tLVYPo/1Sbbm4HqY2szUDRza/6w9Eo2ety/4O/d4Xi8Vz31fLF9r7NHfw8JjggbAuOQq6nS39/C87aKRgASlbx1GQ54V5tC7h5Wmu8wSNVFaHrdP9aUzw6qbrokzmMs2TxfK0bGs/c1fztJ+vIVUtL/slO8Dit4LD4YDXXftofWxpvuZ7XkfEPvASfhwTvszMesqwHG44edVsc9kUeJoGPbOIXSX0dTWBnlm+pharpRlKOPT15FHGhEbunj/NH9y4nimy/PknrJyeYpuXssLzFBd9ihX9d8DOCo8GxSa5NeEEK6fCJPdc/sflB96IGnQ=:B49C^BY208,208^FT420,422^BXN,8,200,0,0,1,~\r\n"
            + "^FH\\^FD\\7E101{0}10{2}\\7E117{3}21{1}^FS"
            + "^FT400,163^A0N,42,52^FH\\^FDPENTA BIO^FS\r\n"
            + "^FT438,490^A0N,29,28^FH\\^FD(01){0}^FS\r\n"
            + "^FT438,526^A0N,29,28^FH\\^FD(21){1}^FS\r\n"
            + "^FT438,562^A0N,29,28^FH\\^FD(10){2}^FS\r\n"
            + "^FT438,598^A0N,29,28^FH\\^FD(17){3}^FS\r\n"
            + "^FT438,634^A0N,29,28^FH\\^FD    {4}^FS\r\n"
            + "^FT42,320^A0N,37,36^FH\\^FDsimpan antara ^FS\r\n"
            + "^FT42,366^A0N,37,36^FH\\^FD+2 \\F8C dan +8 \\F8C^FS\r\n"
            + "^FT21,209^A0N,29,28^FH\\^FDBANDUNG - INDONESIA^FS\r\n"
            + "^FT332,490^A0N,29,28^FH\\^FDGTIN^FS\r\n"
            + "^FT332,526^A0N,29,28^FH\\^FDSN^FS\r\n"
            + "^FT332,562^A0N,29,28^FH\\^FDLOT NO^FS\r\n"
            + "^FT332,598^A0N,29,28^FH\\^FDEXP^FS\r\n"
            + "^FT332,634^A0N,29,28^FH\\^FDQTY^FS\r\n"
            + "^FT25,610^A0N,29,24^FH\\^FDPT. Bio Farma (Persero)^FS\r\n"
            + "^FT25,646^A0N,29,24^FH\\^FDJl. Pasteur No. 28, Bandung^FS\r\n"
            + "^FT25,682^A0N,29,24^FH\\^FDIndonesia ^FS\r\n"
            + "^PQ1,0,1,Y^XZ";

         //private static string kirim = "CT~~CD,~CC^~CT~/n"+
         //   "^XA~TA000~JSN^LT0^MNW^MTT^PON^PMN^LH0,0^JMA^PR2,3~SD25^JUS^LRN^CI0^XZ/n"+
         //   "^XA/n"+
         //   "^MMT/n"+
         //   "^PW1181/n"+
         //   "^LL1290/n"+
         //   "^LS0/n"+
         //   "^FO0,0^GFA,08448,08448,00044,:Z64:eJzt2E1u2zgUB3Aq9ITFwKhmOYug7BG67CIIfRQfIcsuipJBDtArMehByu5mye60EPT6f6SVSJ7Yei4aoDMwIbhm/LNM8ePxsUqdy7mcy7n8x8vqheyr38D+fYL964Xsu9/Abn+53Zx83yRSGLELqV0r9YdqZPbyBKsx0UMTRbYJahW1zKqoVlsjtEmtbqX2g1q/b4X2Wq1vrNC+UWtLSWZbdcU2SKyJVy1lsxHZf64N9R9FbdB9b4g6URsa6jXRN6ltiO5EbVDUKyItt73Q+l75ZMTWhlZsefRktsOLlVka7XIQbijj1WEA46I11QY1LDfBE9/PJ5cXaUtUv0Fh0b4a7y5owuv6D9FyE5Qe7XITxhApaYJSt9X2EntdbSexN+XVCx4NMyHIbY1lLolsuaOV2fJURtaG0ltaZusoiPrMVisKaK7aNxK7mwmSAKF3FnvXYmnHGbZZtm60bxcpwulQv7LcaaYvY2Ekc/0T9gDFrU6LFLGmKSFKsNw4hLF1tNwELiVEiZbmaTY1WW7Ra0LbREAvtAGRUmrv2cr6TGPnDkJ7YYmiF4VJdXHjKX0UZkcRKcwHnSTUYJLl20YUH1a8a90qkeX53m2R0slsvt6qK6FNV9txP1q08XIriiXF/rlROshs0BtR3Cm22UiPAoaTa0Hc2dmN9Gj4uljZkbN27eUJ9kJk1/Pq0TP4+mh1Xvae6v0xu9dbR2fnnj0a4OcH6bKaDj7f3JZd/GAqOrclR/DhgJ0fpO1R+3ZuE14OhqG5LdmMcPso9tMJVnY8Ui7KHBf/UhZpaDMgB8F7i72aU3SDiuv4qvUnO/B+3vLfPH8n83Yd67FpPArsCmH7gHX4G5KL0DJA5McZT3Nqb6fZL2zUfEzrcA+KJW9Bho4zwGdOvt0004BNmo9/3A5KOCCgHdRjB/qKr/KBYWaz6QxbfP4dVb5/74hLamhim2pbzoPw+RcqbabOV6tpcgpg2xXLuyndc/Nx/1wo5bkdlIW1eGok8dZQhB30A2X0jM2lPrHIrTK6HTZdIov10XXNHUX0eNuV+mj1zmZFDX6Nc4uIZB6/0uLqbD+ZXJpHrc0+KbrjrsfIJlTMwIkybDe1PS6buSfv0eu4v0uomJ5/AjZPjgGm2ISfrzaxDY+W63HfYlDwlj9wXEEkwCEANk4s/mL27AOpCvetge3ctwGDgrfcQdXmegH6qcW3H9jyyQNBxaPCEaZYrk+tLVYPo/1Sbbm4HqY2szUDRza/6w9Eo2ety/4O/d4Xi8Vz31fLF9r7NHfw8JjggbAuOQq6nS39/C87aKRgASlbx1GQ54V5tC7h5Wmu8wSNVFaHrdP9aUzw6qbrokzmMs2TxfK0bGs/c1fztJ+vIVUtL/slO8Dit4LD4YDXXftofWxpvuZ7XkfEPvASfhwTvszMesqwHG44edVsc9kUeJoGPbOIXSX0dTWBnlm+pharpRlKOPT15FHGhEbunj/NH9y4nimy/PknrJyeYpuXssLzFBd9ihX9d8DOCo8GxSa5NeEEK6fCJPdc/sflB96IGnQ=:C813^BY312,312^FT747,678^BXN,13,200,0,0,1,~\r\n"+
         //   "^FH\\^FD\\7E101{0}10{3}\\7E117{2}21{1}^FS/n"+
         //   "^FT713,236^A0N,62,81^FH\\^FDPENTA BIO^FS/n"+
         //   "^FT570,840^A0N,50,43^FH\\^FDGTIN^FS/n"+
         //   "^FT570,903^A0N,50,43^FH\\^FDLOT NO^FS/n"+
         //   "^FT570,966^A0N,50,43^FH\\^FDEXP^FS/n"+
         //   "^FT570,1029^A0N,50,43^FH\\^FDSN^FS/n"+
         //   "^FT74,392^A0N,33,33^FH\\^FDBANDUNG - INDONESIA^FS/n"+
         //   "^FT715,839^A0N,54,50^FH\\^FD(01){0}^FS/n"+
         //   "^FT715,907^A0N,54,50^FH\\^FD(10){3}^FS/n" +
         //   "^FT715,975^A0N,54,50^FH\\^FD(17){2}^FS/n" +
         //   "^FT715,1043^A0N,54,50^FH\\^FD(21){1}^FS/n" +
         //   "^FT715,1111^A0N,54,50^FH\\^FD       {4}^FS/n"+
         //   "^FT50,602^A0N,46,45^FH\\^FDsimpan antara ^FS/n"+
         //   "^FT50,660^A0N,46,45^FH\\^FD+2 \\F8C dan +8 \\F8C^FS/n"+
         //   "^FT20,1000^A0N,50,40^FH\\^FDPT. Bio Farma (Persero)^FS/n"+
         //   "^FT20,1063^A0N,50,40^FH\\^FDJl. Pasteur No. 28, Bandung^FS/n"+
         //   "^FT20,1126^A0N,50,40^FH\\^FDIndonesia ^FS/n"+
         //   "^PQ1,0,1,Y^XZ/n";


        //private static string kirim = "CT~~CD,~CC^~CT~/n" +
        //    "^XA~TA000~JSN^LT0^MNW^MTT^PON^PMN^LH0,0^JMA^PR2,3~SD25^JUS^LRN^CI0^XZ/n" +
        //    "^XA/n" +
        //    "^MMT/n" +
        //    "^PW1181/n" +
        //    "^LL1290/n" +
        //    "^LS0/n" +
        //    "^FO0,0^GFA,08448,08448,00044,:Z64:eJzt2E1u2zgUB3Aq9ITFwKhmOYug7BG67CIIfRQfIcsuipJBDtArMehByu5mye60EPT6f6SVSJ7Yei4aoDMwIbhm/LNM8ePxsUqdy7mcy7n8x8vqheyr38D+fYL964Xsu9/Abn+53Zx83yRSGLELqV0r9YdqZPbyBKsx0UMTRbYJahW1zKqoVlsjtEmtbqX2g1q/b4X2Wq1vrNC+UWtLSWZbdcU2SKyJVy1lsxHZf64N9R9FbdB9b4g6URsa6jXRN6ltiO5EbVDUKyItt73Q+l75ZMTWhlZsefRktsOLlVka7XIQbijj1WEA46I11QY1LDfBE9/PJ5cXaUtUv0Fh0b4a7y5owuv6D9FyE5Qe7XITxhApaYJSt9X2EntdbSexN+XVCx4NMyHIbY1lLolsuaOV2fJURtaG0ltaZusoiPrMVisKaK7aNxK7mwmSAKF3FnvXYmnHGbZZtm60bxcpwulQv7LcaaYvY2Ekc/0T9gDFrU6LFLGmKSFKsNw4hLF1tNwELiVEiZbmaTY1WW7Ra0LbREAvtAGRUmrv2cr6TGPnDkJ7YYmiF4VJdXHjKX0UZkcRKcwHnSTUYJLl20YUH1a8a90qkeX53m2R0slsvt6qK6FNV9txP1q08XIriiXF/rlROshs0BtR3Cm22UiPAoaTa0Hc2dmN9Gj4uljZkbN27eUJ9kJk1/Pq0TP4+mh1Xvae6v0xu9dbR2fnnj0a4OcH6bKaDj7f3JZd/GAqOrclR/DhgJ0fpO1R+3ZuE14OhqG5LdmMcPso9tMJVnY8Ui7KHBf/UhZpaDMgB8F7i72aU3SDiuv4qvUnO/B+3vLfPH8n83Yd67FpPArsCmH7gHX4G5KL0DJA5McZT3Nqb6fZL2zUfEzrcA+KJW9Bho4zwGdOvt0004BNmo9/3A5KOCCgHdRjB/qKr/KBYWaz6QxbfP4dVb5/74hLamhim2pbzoPw+RcqbabOV6tpcgpg2xXLuyndc/Nx/1wo5bkdlIW1eGok8dZQhB30A2X0jM2lPrHIrTK6HTZdIov10XXNHUX0eNuV+mj1zmZFDX6Nc4uIZB6/0uLqbD+ZXJpHrc0+KbrjrsfIJlTMwIkybDe1PS6buSfv0eu4v0uomJ5/AjZPjgGm2ISfrzaxDY+W63HfYlDwlj9wXEEkwCEANk4s/mL27AOpCvetge3ctwGDgrfcQdXmegH6qcW3H9jyyQNBxaPCEaZYrk+tLVYPo/1Sbbm4HqY2szUDRza/6w9Eo2ety/4O/d4Xi8Vz31fLF9r7NHfw8JjggbAuOQq6nS39/C87aKRgASlbx1GQ54V5tC7h5Wmu8wSNVFaHrdP9aUzw6qbrokzmMs2TxfK0bGs/c1fztJ+vIVUtL/slO8Dit4LD4YDXXftofWxpvuZ7XkfEPvASfhwTvszMesqwHG44edVsc9kUeJoGPbOIXSX0dTWBnlm+pharpRlKOPT15FHGhEbunj/NH9y4nimy/PknrJyeYpuXssLzFBd9ihX9d8DOCo8GxSa5NeEEK6fCJPdc/sflB96IGnQ=:C813^BY312,312^FT747,678^BXN,13,200,0,0,1,~\r\n" +
        //    //"^FH\\^FD\\7E101{0}10{3}\\7E117{2}21{1}^FS/n" +
        //    "^FH\\^FD01{0}10{3}17{2}21{1}^FS/n" +
        //    "^FT660,236^A0N,62,81^FH\\^FD{6}^FS/n" +
        //    //            "^FT570,760^A0N,50,43^FH\\^FDGTIN^FS/n" +
        //    "^FT570,760^A0N,50,43^FH\\^FDNO^FS/n" +
        //    "^FT570,830^A0N,50,43^FH\\^FDLOT NO^FS/n" +
        //    "^FT570,900^A0N,50,43^FH\\^FDEXP^FS/n" +
        //    "^FT570,970^A0N,50,43^FH\\^FDSN^FS/n" +
        //    "^FT570,1040^A0N,50,43^FH\\^FDQty in Basket^FS/n" +
        //    "^FT570,1110^A0N,50,43^FH\\^FDALLOCATION^FS/n" +
        //    "^FT74,392^A0N,33,33^FH\\^FDBANDUNG - INDONESIA^FS/n" +
        //    "^FT715,760^A0N,54,50^FH\\^FD(01){0}^FS/n" +
        //    "^FT715,830^A0N,54,50^FH\\^FD(10){3}^FS/n" +
        //    "^FT715,900^A0N,54,50^FH\\^FD(17){2}^FS/n" +
        //    "^FT715,970^A0N,54,50^FH\\^FD(21){1}^FS/n" +
        //    "^FT715,1040^A0N,54,50^FH\\^FD       :{4}^FS/n" +
        //    "^FT715,1110^A0N,54,50^FH\\^FD       :{5}^FS/n" +
        //    "^FT50,602^A0N,46,45^FH\\^FDsimpan antara ^FS/n" +
        //    "^FT50,660^A0N,46,45^FH\\^FD+2 \\F8C dan +8 \\F8C^FS/n" +
        //    "^FT20,1000^A0N,50,40^FH\\^FDPT. Bio Farma (Persero)^FS/n" +
        //    "^FT20,1063^A0N,50,40^FH\\^FDJl. Pasteur No. 28, Bandung^FS/n" +
        //    "^FT20,1126^A0N,50,40^FH\\^FDIndonesia ^FS/n" +
        //    "^PQ1,0,1,Y^XZ/n";

    //    private static string kirim =
    //"^XA~TA000~JSN^LT0^MNW^MTT^PON^PMN^LH0,0^JMA^PR2,3~SD25^JUS^LRN^CI0^XZ/n" +
    //"^XA/n" +
    //"^MMT/n" +
    //"^PW1181/n" +
    //"^LL1290/n" +
    //"^LS0/n" +
    //"^FO0,0^GFA,08448,08448,00044,:Z64:eJzt2E1u2zgUB3Aq9ITFwKhmOYug7BG67CIIfRQfIcsuipJBDtArMehByu5mye60EPT6f6SVSJ7Yei4aoDMwIbhm/LNM8ePxsUqdy7mcy7n8x8vqheyr38D+fYL964Xsu9/Abn+53Zx83yRSGLELqV0r9YdqZPbyBKsx0UMTRbYJahW1zKqoVlsjtEmtbqX2g1q/b4X2Wq1vrNC+UWtLSWZbdcU2SKyJVy1lsxHZf64N9R9FbdB9b4g6URsa6jXRN6ltiO5EbVDUKyItt73Q+l75ZMTWhlZsefRktsOLlVka7XIQbijj1WEA46I11QY1LDfBE9/PJ5cXaUtUv0Fh0b4a7y5owuv6D9FyE5Qe7XITxhApaYJSt9X2EntdbSexN+XVCx4NMyHIbY1lLolsuaOV2fJURtaG0ltaZusoiPrMVisKaK7aNxK7mwmSAKF3FnvXYmnHGbZZtm60bxcpwulQv7LcaaYvY2Ekc/0T9gDFrU6LFLGmKSFKsNw4hLF1tNwELiVEiZbmaTY1WW7Ra0LbREAvtAGRUmrv2cr6TGPnDkJ7YYmiF4VJdXHjKX0UZkcRKcwHnSTUYJLl20YUH1a8a90qkeX53m2R0slsvt6qK6FNV9txP1q08XIriiXF/rlROshs0BtR3Cm22UiPAoaTa0Hc2dmN9Gj4uljZkbN27eUJ9kJk1/Pq0TP4+mh1Xvae6v0xu9dbR2fnnj0a4OcH6bKaDj7f3JZd/GAqOrclR/DhgJ0fpO1R+3ZuE14OhqG5LdmMcPso9tMJVnY8Ui7KHBf/UhZpaDMgB8F7i72aU3SDiuv4qvUnO/B+3vLfPH8n83Yd67FpPArsCmH7gHX4G5KL0DJA5McZT3Nqb6fZL2zUfEzrcA+KJW9Bho4zwGdOvt0004BNmo9/3A5KOCCgHdRjB/qKr/KBYWaz6QxbfP4dVb5/74hLamhim2pbzoPw+RcqbabOV6tpcgpg2xXLuyndc/Nx/1wo5bkdlIW1eGok8dZQhB30A2X0jM2lPrHIrTK6HTZdIov10XXNHUX0eNuV+mj1zmZFDX6Nc4uIZB6/0uLqbD+ZXJpHrc0+KbrjrsfIJlTMwIkybDe1PS6buSfv0eu4v0uomJ5/AjZPjgGm2ISfrzaxDY+W63HfYlDwlj9wXEEkwCEANk4s/mL27AOpCvetge3ctwGDgrfcQdXmegH6qcW3H9jyyQNBxaPCEaZYrk+tLVYPo/1Sbbm4HqY2szUDRza/6w9Eo2ety/4O/d4Xi8Vz31fLF9r7NHfw8JjggbAuOQq6nS39/C87aKRgASlbx1GQ54V5tC7h5Wmu8wSNVFaHrdP9aUzw6qbrokzmMs2TxfK0bGs/c1fztJ+vIVUtL/slO8Dit4LD4YDXXftofWxpvuZ7XkfEPvASfhwTvszMesqwHG44edVsc9kUeJoGPbOIXSX0dTWBnlm+pharpRlKOPT15FHGhEbunj/NH9y4nimy/PknrJyeYpuXssLzFBd9ihX9d8DOCo8GxSa5NeEEK6fCJPdc/sflB96IGnQ=:C813^BY312,312^FT747,678^BXN,13,200,0,0,1,~\r\n" +
    //        //"^FH\\^FD\\7E101{0}10{3}\\7E117{2}21{1}^FS/n" +
    //"^FH\\^FD01{0}10{3}17{2}21{1}^FS/n" +
    //"^FT660,236^A0N,62,81^FH\\^FD{6}^FS/n" +
    //        //            "^FT570,760^A0N,50,43^FH\\^FDGTIN^FS/n" +
    //"^FT570,760^A0N,50,43^FH\\^FDNO^FS/n" +
    //"^FT570,830^A0N,50,43^FH\\^FDLOT NO^FS/n" +
    //"^FT570,900^A0N,50,43^FH\\^FDEXP^FS/n" +
    //"^FT570,970^A0N,50,43^FH\\^FDSN^FS/n" +
    //"^FT570,1040^A0N,50,43^FH\\^FDQty in Basket^FS/n" +
    //"^FT570,1110^A0N,50,43^FH\\^FDALLOCATION^FS/n" +
    //"^FT74,392^A0N,33,33^FH\\^FDBANDUNG - INDONESIA^FS/n" +
    //"^FT715,760^A0N,54,50^FH\\^FD(01){0}^FS/n" +
    //"^FT715,830^A0N,54,50^FH\\^FD(10){3}^FS/n" +
    //"^FT715,900^A0N,54,50^FH\\^FD(17){2}^FS/n" +
    //"^FT715,970^A0N,54,50^FH\\^FD(21){1}^FS/n" +
    //"^FT715,1040^A0N,54,50^FH\\^FD       :{4}^FS/n" +
    //"^FT715,1110^A0N,54,50^FH\\^FD       :{5}^FS/n" +
    //"^FT50,602^A0N,46,45^FH\\^FDsimpan antara ^FS/n" +
    //"^FT50,660^A0N,46,45^FH\\^FD+2 \\F8C dan +8 \\F8C^FS/n" +
    //"^FT20,1000^A0N,50,40^FH\\^FDPT. Bio Farma (Persero)^FS/n" +
    //"^FT20,1063^A0N,50,40^FH\\^FDJl. Pasteur No. 28, Bandung^FS/n" +
    //"^FT20,1126^A0N,50,40^FH\\^FDIndonesia ^FS/n" +
    //"^PQ1,0,1,Y^XZ/n";

        private static string kirim =
            "^XA~TA000~JSN^LT0^MNW^MTT^PON^PMN^LH0,0^JMA^PR2,3~SD25^JUS^LRN^CI0^XZ/n" +
        "^XA/n" +
        "^MMT/n" +
        "^PW1181/n" +
        "^LL1290/n" +
        "^LS0/n" +
        "^FO0,0^GFA,08448,08448,00044,:Z64:eJzt2E1u2zgUB3Aq9ITFwKhmOYug7BG67CIIfRQfIcsuipJBDtArMehByu5mye60EPT6f6SVSJ7Yei4aoDMwIbhm/LNM8ePxsUqdy7mcy7n8x8vqheyr38D+fYL964Xsu9/Abn+53Zx83yRSGLELqV0r9YdqZPbyBKsx0UMTRbYJahW1zKqoVlsjtEmtbqX2g1q/b4X2Wq1vrNC+UWtLSWZbdcU2SKyJVy1lsxHZf64N9R9FbdB9b4g6URsa6jXRN6ltiO5EbVDUKyItt73Q+l75ZMTWhlZsefRktsOLlVka7XIQbijj1WEA46I11QY1LDfBE9/PJ5cXaUtUv0Fh0b4a7y5owuv6D9FyE5Qe7XITxhApaYJSt9X2EntdbSexN+XVCx4NMyHIbY1lLolsuaOV2fJURtaG0ltaZusoiPrMVisKaK7aNxK7mwmSAKF3FnvXYmnHGbZZtm60bxcpwulQv7LcaaYvY2Ekc/0T9gDFrU6LFLGmKSFKsNw4hLF1tNwELiVEiZbmaTY1WW7Ra0LbREAvtAGRUmrv2cr6TGPnDkJ7YYmiF4VJdXHjKX0UZkcRKcwHnSTUYJLl20YUH1a8a90qkeX53m2R0slsvt6qK6FNV9txP1q08XIriiXF/rlROshs0BtR3Cm22UiPAoaTa0Hc2dmN9Gj4uljZkbN27eUJ9kJk1/Pq0TP4+mh1Xvae6v0xu9dbR2fnnj0a4OcH6bKaDj7f3JZd/GAqOrclR/DhgJ0fpO1R+3ZuE14OhqG5LdmMcPso9tMJVnY8Ui7KHBf/UhZpaDMgB8F7i72aU3SDiuv4qvUnO/B+3vLfPH8n83Yd67FpPArsCmH7gHX4G5KL0DJA5McZT3Nqb6fZL2zUfEzrcA+KJW9Bho4zwGdOvt0004BNmo9/3A5KOCCgHdRjB/qKr/KBYWaz6QxbfP4dVb5/74hLamhim2pbzoPw+RcqbabOV6tpcgpg2xXLuyndc/Nx/1wo5bkdlIW1eGok8dZQhB30A2X0jM2lPrHIrTK6HTZdIov10XXNHUX0eNuV+mj1zmZFDX6Nc4uIZB6/0uLqbD+ZXJpHrc0+KbrjrsfIJlTMwIkybDe1PS6buSfv0eu4v0uomJ5/AjZPjgGm2ISfrzaxDY+W63HfYlDwlj9wXEEkwCEANk4s/mL27AOpCvetge3ctwGDgrfcQdXmegH6qcW3H9jyyQNBxaPCEaZYrk+tLVYPo/1Sbbm4HqY2szUDRza/6w9Eo2ety/4O/d4Xi8Vz31fLF9r7NHfw8JjggbAuOQq6nS39/C87aKRgASlbx1GQ54V5tC7h5Wmu8wSNVFaHrdP9aUzw6qbrokzmMs2TxfK0bGs/c1fztJ+vIVUtL/slO8Dit4LD4YDXXftofWxpvuZ7XkfEPvASfhwTvszMesqwHG44edVsc9kUeJoGPbOIXSX0dTWBnlm+pharpRlKOPT15FHGhEbunj/NH9y4nimy/PknrJyeYpuXssLzFBd9ihX9d8DOCo8GxSa5NeEEK6fCJPdc/sflB96IGnQ=:C813^BY312,312^FT487,478^BXN,10,200,0,0,1,~\r\n" +
        "^FH\\^FD01{0}10{3}17{2}21{1}^FS/n" +
        "^FT450,186^A0N,45,41^FH\\^FD{6}^FS/n" +
        "^FT400,530^A0N,32,31^FH\\^FDNO^FS/n" +
        "^FT400,570^A0N,32,31^FH\\^FDLOT NO^FS/n" +
        "^FT400,610^A0N,32,31^FH\\^FDEXP^FS/n" +
        "^FT400,650^A0N,32,31^FH\\^FDSN^FS/n" +
        "^FT400,690^A0N,32,31^FH\\^FDQty in Basket^FS/n" +
        "^FT400,730^A0N,32,31^FH\\^FDALLOCATION^FS/n" +
        "^FT30,275^A0N,25,21^FH\\^FDBANDUNG - INDONESIA^FS/n" +
        "^FT500,530^A0N,32,31^FH\\^FD(01){0}^FS/n" +
        "^FT500,570^A0N,32,31^FH\\^FD(10){3}^FS/n" +
        "^FT500,610^A0N,32,31^FH\\^FD(17){2}^FS/n" +
        "^FT500,650^A0N,32,31^FH\\^FD(21){1}^FS/n" +
        "^FT505,690^A0N,32,31^FH\\^FD       :{4}^FS/n" +
        "^FT505,730^A0N,32,31^FH\\^FD       :{5}^FS/n" +
        "^FT30,448^A0N,30,25^FH\\^FDsimpan antara ^FS/n" +
        "^FT30,478^A0N,30,25^FH\\^FD+2 \\F8C dan +8 \\F8C^FS/n" +
        "^FT30,650^A0N,30,25^FH\\^FDPT. Bio Farma (Persero)^FS/n" +
        "^FT30,690^A0N,30,25^FH\\^FDJl. Pasteur No. 28, Bandung^FS/n" +
        "^FT30,730^A0N,30,25^FH\\^FDIndonesia ^FS/n" +
        "^PQ1,0,1,Y^XZ/n";

        public static string Print(String Gtin, String SerialNumber, String LotNo, String ExpiryDate, String Quantity, String allocation, String product)
        {
            String ZplString = String.Format(kirim, Gtin, SerialNumber, ExpiryDate, LotNo, Quantity, allocation, product);
            //String ZplString = String.Format(kirim, Gtin, SerialNumber, ExpiryDate, LotNo, Quantity, "Indonesia", product);
            tcp = new tcp();
            tcp.Connect(sql.config["ipprinter"], sql.config["portprinter"], sql.config["timeout"]);
            tcp.send(ZplString);
            tcp.dc();
            return ZplString;
        }

        public static void Print(string data)
        {
            tcp = new tcp();
            tcp.Connect(sql.config["ipprinter"], sql.config["portprinter"], sql.config["timeout"]);
            tcp.send(data);
            tcp.dc();
        }
    }
}
