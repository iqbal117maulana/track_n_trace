﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Configuration;

namespace Basket_Station
{
    public partial class Config : Form
    {
        public Config()
        {
            InitializeComponent();
        }
		
		/**
		 *  Get all values from the application settings.
		 */
		static void ReadAllSettings()
        {
            try
            {
                var appSettings = ConfigurationManager.AppSettings;

                if (appSettings.Count == 0)
                {
                    Console.WriteLine("AppSettings is empty.");
                }
                else
                {
                    foreach (var key in appSettings.AllKeys)
                    {
                        Console.WriteLine("Key: {0} Value: {1}", key, appSettings[key]);
                    }
                }
            }
            catch (ConfigurationErrorsException)
            {
                Console.WriteLine("Error reading app settings");
            }
        }
		
		/**
		 *  Add a new application settings or update an existing application settings.
		 *  @param key The value which will be the key to the settings' value
		 *  @param value The value that will be updated or added
		 */
		static void AddUpdateAppSettings(string key, string value)
        {
            try
            {
                var configFile = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                var settings = configFile.AppSettings.Settings;
                if (settings[key] == null)
                {
                    settings.Add(key, value);
                }
                else
                {
                    settings[key].Value = value;
                }
                configFile.Save(ConfigurationSaveMode.Modified);
                ConfigurationManager.RefreshSection(configFile.AppSettings.SectionInformation.Name);
            }
            catch (ConfigurationErrorsException)
            {
                Console.WriteLine("Error writing app settings");
            }
        }

        private void Config_Load(object sender, EventArgs e)
        {
            txtIpCamera.Text = ConfigurationManager.AppSettings["camera_address"];
            txtPortCamera.Text = ConfigurationManager.AppSettings["camera_port"];
            txtLineCamera.Text = ConfigurationManager.AppSettings["LINE"];
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Do you want to save changes?", "Confirmation", MessageBoxButtons.YesNoCancel);
            if (result == DialogResult.Yes)
            {
                AddUpdateAppSettings("camera_address", txtIpCamera.Text);
                AddUpdateAppSettings("camera_port", txtIpCamera.Text);
                AddUpdateAppSettings("LINE", txtLineCamera.Text);
            }
            else if (result == DialogResult.No)
            {
                ReadAllSettings();
            }
        }
    }
}
