﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace Manual_Packer_Station
{
    public partial class ListKendaraan : Form
    {        
        dbaccess database = new dbaccess();
        DataTable dtVehicle;
        public int expedisiId;
        public int vehicleId = 0;
        public String vehicleNo;

        public ListKendaraan()
        {
            InitializeComponent();
            InitData();            
        }

        private void InitData(){
            vehicleId = 0;
            vehicleNo = "";
        }

        private void LoadData(){
            String sql;
            sql = "SELECT a.id, a.vehicle_no, a.expedisi_id, b.expedisi_name " +
                  "FROM vehicle a "+
                  "INNER JOIN expedisi b ON b.id = a.expedisi_id " +
                  "WHERE a.expedisi_id = " + expedisiId + " AND a.vehicle_no LIKE '%" + txtSearch.Text + "%'";
            database.OpenQuery(out dtVehicle, sql);
            dgvData.AutoGenerateColumns = false;
            dgvData.DataSource = dtVehicle;
            dgvData.Focus();
        }

        private void dgvData_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                vehicleId = int.Parse(dgvData.Rows[e.RowIndex].Cells["id"].Value.ToString());
                vehicleNo = dgvData.Rows[e.RowIndex].Cells["vehicle_no"].Value.ToString();
                this.Close();
            }
        }

        private void txtSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                LoadData();
            }
        }

        private void ListKendaraan_Load(object sender, EventArgs e)
        {
            LoadData();
        }

        private void dgvData_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                int baris = dgvData.CurrentRow.Index;
                vehicleId = int.Parse(dgvData.Rows[baris].Cells["id"].Value.ToString());
                vehicleNo = dgvData.Rows[baris].Cells["vehicle_no"].Value.ToString();
                this.Close();
            }
        }
    }
}
