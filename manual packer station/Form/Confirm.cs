﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Manual_Packer_Station
{
    public partial class Confirm : Form
    {
        public Confirm(string textConfirm, string labelatas)
        {
            InitializeComponent();
            lblMessage.Text = textConfirm;
            this.Text = labelatas;
        }

        public bool conf()
        {
            button1.DialogResult = DialogResult.Yes;
            button2.DialogResult = DialogResult.No;
            btnOk.Visible = false;
            this.ShowDialog();
            if (this.DialogResult == DialogResult.Yes)
            {
                return true;
            }
            else
                return false;
        }

        public Confirm(string textConfirm)
        {
            InitializeComponent();
            lblMessage.Text = textConfirm;
            this.Text = "Information";
            btnOk.Visible = true;
            button1.Visible = false;
            button2.Visible = false;
            this.ShowDialog();
        }
        public Confirm(string textConfirm, string labelatas, MessageBoxButtons oke)
        {
            InitializeComponent();
            lblMessage.Text = textConfirm;
            this.Text = labelatas;
            btnOk.Visible = true;
            button2.Visible = false;
            button1.Visible = false;
            this.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }
    }
}
