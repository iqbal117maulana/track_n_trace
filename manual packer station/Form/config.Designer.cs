﻿namespace Manual_Packer_Station
{
    partial class config
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtIpServer = new System.Windows.Forms.TextBox();
            this.txtPortServer = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtIpPrinter = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtPortPrinter = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.txtPortDb = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtIpDB = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtTimeoutDb = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtPortCamera2 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtIpCamera2 = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtNamaDb = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtCompID = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtNamaDb2 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 21);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "IP Server";
            // 
            // txtIpServer
            // 
            this.txtIpServer.Enabled = false;
            this.txtIpServer.Location = new System.Drawing.Point(109, 17);
            this.txtIpServer.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtIpServer.Name = "txtIpServer";
            this.txtIpServer.Size = new System.Drawing.Size(132, 22);
            this.txtIpServer.TabIndex = 1;
            // 
            // txtPortServer
            // 
            this.txtPortServer.Enabled = false;
            this.txtPortServer.Location = new System.Drawing.Point(109, 49);
            this.txtPortServer.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtPortServer.Name = "txtPortServer";
            this.txtPortServer.Size = new System.Drawing.Size(132, 22);
            this.txtPortServer.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 53);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Port Server";
            // 
            // txtIpPrinter
            // 
            this.txtIpPrinter.Location = new System.Drawing.Point(109, 81);
            this.txtIpPrinter.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtIpPrinter.Name = "txtIpPrinter";
            this.txtIpPrinter.Size = new System.Drawing.Size(132, 22);
            this.txtIpPrinter.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 85);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(66, 17);
            this.label4.TabIndex = 6;
            this.label4.Text = "IP Printer";
            // 
            // txtPortPrinter
            // 
            this.txtPortPrinter.Location = new System.Drawing.Point(109, 113);
            this.txtPortPrinter.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtPortPrinter.Name = "txtPortPrinter";
            this.txtPortPrinter.Size = new System.Drawing.Size(132, 22);
            this.txtPortPrinter.TabIndex = 4;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(9, 117);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(80, 17);
            this.label5.TabIndex = 8;
            this.label5.Text = "Port Printer";
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.button1.Image = global::Manual_Packer_Station.Properties.Resources.submit;
            this.button1.Location = new System.Drawing.Point(300, 177);
            this.button1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(200, 62);
            this.button1.TabIndex = 15;
            this.button1.Text = "Submit";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtPortDb
            // 
            this.txtPortDb.Location = new System.Drawing.Point(363, 49);
            this.txtPortDb.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtPortDb.Name = "txtPortDb";
            this.txtPortDb.Size = new System.Drawing.Size(132, 22);
            this.txtPortDb.TabIndex = 13;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(269, 53);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(57, 17);
            this.label8.TabIndex = 27;
            this.label8.Text = "Port DB";
            // 
            // txtIpDB
            // 
            this.txtIpDB.Location = new System.Drawing.Point(363, 17);
            this.txtIpDB.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtIpDB.Name = "txtIpDB";
            this.txtIpDB.Size = new System.Drawing.Size(132, 22);
            this.txtIpDB.TabIndex = 12;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(269, 21);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(43, 17);
            this.label9.TabIndex = 25;
            this.label9.Text = "IP DB";
            // 
            // txtTimeoutDb
            // 
            this.txtTimeoutDb.Location = new System.Drawing.Point(363, 81);
            this.txtTimeoutDb.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtTimeoutDb.Name = "txtTimeoutDb";
            this.txtTimeoutDb.Size = new System.Drawing.Size(132, 22);
            this.txtTimeoutDb.TabIndex = 14;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(269, 85);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(82, 17);
            this.label10.TabIndex = 23;
            this.label10.Text = "Timeout DB";
            // 
            // txtPortCamera2
            // 
            this.txtPortCamera2.Location = new System.Drawing.Point(109, 214);
            this.txtPortCamera2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtPortCamera2.Name = "txtPortCamera2";
            this.txtPortCamera2.Size = new System.Drawing.Size(132, 22);
            this.txtPortCamera2.TabIndex = 8;
            this.txtPortCamera2.Visible = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(9, 218);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(87, 17);
            this.label11.TabIndex = 35;
            this.label11.Text = "Port Camera";
            this.label11.Visible = false;
            // 
            // txtIpCamera2
            // 
            this.txtIpCamera2.Location = new System.Drawing.Point(109, 182);
            this.txtIpCamera2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtIpCamera2.Name = "txtIpCamera2";
            this.txtIpCamera2.Size = new System.Drawing.Size(132, 22);
            this.txtIpCamera2.TabIndex = 7;
            this.txtIpCamera2.Visible = false;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(9, 186);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(73, 17);
            this.label14.TabIndex = 33;
            this.label14.Text = "IP Camera";
            this.label14.Visible = false;
            // 
            // txtNamaDb
            // 
            this.txtNamaDb.Location = new System.Drawing.Point(363, 113);
            this.txtNamaDb.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtNamaDb.Name = "txtNamaDb";
            this.txtNamaDb.Size = new System.Drawing.Size(132, 22);
            this.txtNamaDb.TabIndex = 36;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(269, 117);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 17);
            this.label3.TabIndex = 37;
            this.label3.Text = "DB Name";
            // 
            // txtCompID
            // 
            this.txtCompID.Location = new System.Drawing.Point(109, 145);
            this.txtCompID.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtCompID.MaxLength = 1;
            this.txtCompID.Name = "txtCompID";
            this.txtCompID.Size = new System.Drawing.Size(132, 22);
            this.txtCompID.TabIndex = 38;
            this.txtCompID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCompID_KeyPress);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(9, 149);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(61, 17);
            this.label6.TabIndex = 39;
            this.label6.Text = "Comp ID";
            // 
            // txtNamaDb2
            // 
            this.txtNamaDb2.Location = new System.Drawing.Point(363, 145);
            this.txtNamaDb2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtNamaDb2.Name = "txtNamaDb2";
            this.txtNamaDb2.Size = new System.Drawing.Size(132, 22);
            this.txtNamaDb2.TabIndex = 40;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(269, 149);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(80, 17);
            this.label7.TabIndex = 41;
            this.label7.Text = "DB Name 2";
            // 
            // config
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(516, 254);
            this.Controls.Add(this.txtNamaDb2);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtCompID);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtNamaDb);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtPortCamera2);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.txtIpCamera2);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.txtPortDb);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtIpDB);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtTimeoutDb);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.txtPortPrinter);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtIpPrinter);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtPortServer);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtIpServer);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "config";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Configuration";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtIpServer;
        private System.Windows.Forms.TextBox txtPortServer;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtIpPrinter;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtPortPrinter;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtPortDb;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtIpDB;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtTimeoutDb;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtPortCamera2;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtIpCamera2;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtNamaDb;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtCompID;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtNamaDb2;
        private System.Windows.Forms.Label label7;
    }
}