﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace Manual_Packer_Station
{
    public partial class ListExpedisi : Form
    {        
        dbaccess database = new dbaccess();
        DataTable DtExpedisi;
        public int expedisiId;
        public String expedisiName;

        public ListExpedisi()
        {
            InitializeComponent();
            InitData();            
        }

        private void InitData(){
            expedisiId = 0;
            expedisiName = "";
        }

        private void LoadData(){
            String sql;
            sql = "SELECT id, expedisi_code, expedisi_name " +
                  "FROM expedisi "+
                  "WHERE expedisi_name LIKE '%" + txtSearch .Text+ "%'";
            database.OpenQuery(out DtExpedisi, sql);
            dgvData.AutoGenerateColumns = false;
            dgvData.DataSource = DtExpedisi;
            dgvData.Focus();
        }

        private void dgvData_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                expedisiId = int.Parse(dgvData.Rows[e.RowIndex].Cells["ID"].Value.ToString());
                expedisiName = dgvData.Rows[e.RowIndex].Cells["expedisi_name"].Value.ToString();
                this.Close();
            }
        }

        private void txtSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                LoadData();
            }
        }

        private void ListExpedisi_Load(object sender, EventArgs e)
        {
            LoadData();
        }

        private void dgvData_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                int baris = dgvData.CurrentRow.Index;
                expedisiId = int.Parse(dgvData.Rows[baris].Cells["ID"].Value.ToString());
                expedisiName = dgvData.Rows[baris].Cells["expedisi_name"].Value.ToString();
                this.Close();
            }
        }
    }
}
