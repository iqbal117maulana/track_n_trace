﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Manual_Packer_Station
{
    public partial class Decomission : Form
    {
        string uomPilihan = "";
        dbaccess db;
        public Decomission()
        {
            InitializeComponent();
            db = new dbaccess();

        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            
            if (e.KeyCode == Keys.Enter)
            {
                if (cmbTypeID.SelectedIndex == -1)
                {
                    Confirm cf = new Confirm("Choose Type Id first.");
                    return;
                }
                if (cmbTypeID.SelectedIndex == 0)
                    uomPilihan = "Masterbox";
                else if (cmbTypeID.SelectedIndex == 1)
                    uomPilihan = "Basket";
                else if (cmbTypeID.SelectedIndex == 2)
                    uomPilihan = "Innerbox";
                else
                    uomPilihan = "Vial";
                decomision();
            }
        }

        private void decomision()
        {
            if (db.validasi(txtCapid.Text, 1))
            {
                if (uomPilihan == "Masterbox")
                {
                    if (db.decomission(txtCapid.Text))
                    {
                        txtLog.AppendText(txtCapid.Text + " Succes Decomisiion" + Environment.NewLine);
                        txtCapid.Text = "";
                    }
                    else
                        txtLog.AppendText(txtCapid.Text + " Failed Decomisiion" + Environment.NewLine);
                }
                else
                {
                    if (db.decomission2(txtCapid.Text, uomPilihan))
                    {
                        txtLog.AppendText(txtCapid.Text + " Succes Decomisiion" + Environment.NewLine);
                        txtCapid.Text = "";
                    }
                    else
                        txtLog.AppendText(txtCapid.Text + " Failed Decomisiion" + Environment.NewLine);
                }
            }
            else
                new Confirm("Input Is Empty");

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (cmbTypeID.SelectedIndex == -1)
            {
                Confirm cf = new Confirm("Choose Type Id first.");
                return;
            }
            if (cmbTypeID.SelectedIndex == 0)
                uomPilihan = "Masterbox";
            else if (cmbTypeID.SelectedIndex == 1)
                uomPilihan = "Basket";
            else if (cmbTypeID.SelectedIndex == 2)
                uomPilihan = "Innerbox";
            else
                uomPilihan = "Vial";
            decomision();
        }
    }
}
