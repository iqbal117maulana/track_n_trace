﻿namespace Manual_Packer_Station
{
    partial class ManualPacker
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ManualPacker));
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.state = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.state_text = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.uom = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.qty_1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.itemid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.model_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.qty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.qty_scanned = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.uom_vial = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.timestamp = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.isSAS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.outbox_qty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.itemid2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.model_name2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.masterboxid2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colieNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblQtyScanned = new System.Windows.Forms.Label();
            this.lblQty = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblQtyTotal = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.buttonClearSelected = new System.Windows.Forms.Button();
            this.buttonClearAll = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnConfirm = new System.Windows.Forms.Button();
            this.lblMas = new System.Windows.Forms.Label();
            this.btnPrintDO = new System.Windows.Forms.Button();
            this.txtBasket = new System.Windows.Forms.TextBox();
            this.labelQtyHistory = new System.Windows.Forms.Label();
            this.lblQty1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.buttonPrint = new System.Windows.Forms.Button();
            this.txtETA = new System.Windows.Forms.TextBox();
            this.txtVehicle = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtExpedisi = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.lblPrinter = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.lblCamera = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.grpDetail = new System.Windows.Forms.GroupBox();
            this.lblUom = new System.Windows.Forms.Label();
            this.lblProductName = new System.Windows.Forms.Label();
            this.lblProd = new System.Windows.Forms.Label();
            this.txtAddress = new System.Windows.Forms.TextBox();
            this.txtCost = new System.Windows.Forms.TextBox();
            this.lblDetail2 = new System.Windows.Forms.Label();
            this.lblDetail = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.dgvDetail = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.txtScanned = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblDate = new System.Windows.Forms.Label();
            this.lblTime = new System.Windows.Forms.Label();
            this.lblRole = new System.Windows.Forms.Label();
            this.lblUserId = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.btnDeo = new System.Windows.Forms.Button();
            this.btnOpenLog = new System.Windows.Forms.Button();
            this.btnIndicator = new System.Windows.Forms.Button();
            this.onPrinter1 = new System.Windows.Forms.PictureBox();
            this.offPrinter1 = new System.Windows.Forms.PictureBox();
            this.onCamera1 = new System.Windows.Forms.PictureBox();
            this.offCamera1 = new System.Windows.Forms.PictureBox();
            this.btnStart = new System.Windows.Forms.Button();
            this.pictureBoxPrinter = new System.Windows.Forms.PictureBox();
            this.pictureBoxCamera = new System.Windows.Forms.PictureBox();
            this.btnConfig = new System.Windows.Forms.Button();
            this.btnLogout = new System.Windows.Forms.Button();
            this.bgUpdateVaccine = new System.ComponentModel.BackgroundWorker();
            this.timerUpdateVaccine = new System.Windows.Forms.Timer(this.components);
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.txtDO = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.lblQtyMasterbox = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.grpDetail.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDetail)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.onPrinter1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.offPrinter1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.onCamera1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.offCamera1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPrinter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCamera)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeColumns = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dataGridView1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle10;
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.state,
            this.state_text,
            this.uom,
            this.qty_1,
            this.itemid,
            this.model_name,
            this.qty,
            this.qty_scanned,
            this.uom_vial,
            this.timestamp,
            this.isSAS,
            this.outbox_qty});
            this.dataGridView1.Location = new System.Drawing.Point(11, 57);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(663, 489);
            this.dataGridView1.TabIndex = 6;
            this.dataGridView1.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dataGridView1_CellFormatting);
            this.dataGridView1.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dataGridView1_RowsAdded);
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            this.dataGridView1.SelectionChanged += new System.EventHandler(this.dataGridView1_SelectionChanged);
            // 
            // state
            // 
            this.state.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.state.DataPropertyName = "state";
            this.state.HeaderText = "State";
            this.state.Name = "state";
            this.state.ReadOnly = true;
            this.state.Visible = false;
            // 
            // state_text
            // 
            this.state_text.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.state_text.HeaderText = "State";
            this.state_text.Name = "state_text";
            this.state_text.ReadOnly = true;
            this.state_text.Visible = false;
            // 
            // uom
            // 
            this.uom.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.uom.DataPropertyName = "uom";
            this.uom.HeaderText = "Type Id";
            this.uom.Name = "uom";
            this.uom.ReadOnly = true;
            this.uom.Width = 84;
            // 
            // qty_1
            // 
            this.qty_1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.qty_1.DataPropertyName = "qty_1";
            this.qty_1.HeaderText = "Qty";
            this.qty_1.Name = "qty_1";
            this.qty_1.ReadOnly = true;
            this.qty_1.Visible = false;
            // 
            // itemid
            // 
            this.itemid.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.itemid.DataPropertyName = "itemid";
            this.itemid.HeaderText = "Item Id";
            this.itemid.Name = "itemid";
            this.itemid.ReadOnly = true;
            this.itemid.Width = 83;
            // 
            // model_name
            // 
            this.model_name.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.model_name.DataPropertyName = "model_name";
            this.model_name.HeaderText = "Product Name";
            this.model_name.Name = "model_name";
            this.model_name.ReadOnly = true;
            this.model_name.Width = 135;
            // 
            // qty
            // 
            this.qty.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.qty.DataPropertyName = "qty";
            this.qty.HeaderText = "Qty";
            this.qty.Name = "qty";
            this.qty.ReadOnly = true;
            // 
            // qty_scanned
            // 
            this.qty_scanned.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.qty_scanned.DataPropertyName = "qty_scanned";
            this.qty_scanned.FillWeight = 50F;
            this.qty_scanned.HeaderText = "Qty Scanned";
            this.qty_scanned.Name = "qty_scanned";
            this.qty_scanned.ReadOnly = true;
            this.qty_scanned.Width = 75;
            // 
            // uom_vial
            // 
            this.uom_vial.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.uom_vial.DataPropertyName = "uom_vial";
            this.uom_vial.HeaderText = "Uom";
            this.uom_vial.Name = "uom_vial";
            this.uom_vial.ReadOnly = true;
            // 
            // timestamp
            // 
            this.timestamp.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.timestamp.DataPropertyName = "timestamp";
            this.timestamp.HeaderText = "Timestamp";
            this.timestamp.Name = "timestamp";
            this.timestamp.ReadOnly = true;
            this.timestamp.Visible = false;
            // 
            // isSAS
            // 
            this.isSAS.DataPropertyName = "isSAS";
            this.isSAS.HeaderText = "isSAS";
            this.isSAS.Name = "isSAS";
            this.isSAS.ReadOnly = true;
            this.isSAS.Visible = false;
            // 
            // outbox_qty
            // 
            this.outbox_qty.DataPropertyName = "outbox_qty";
            this.outbox_qty.HeaderText = "outbox_qty";
            this.outbox_qty.Name = "outbox_qty";
            this.outbox_qty.ReadOnly = true;
            this.outbox_qty.Visible = false;
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.AllowUserToDeleteRows = false;
            dataGridViewCellStyle11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dataGridView2.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle11;
            this.dataGridView2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.itemid2,
            this.model_name2,
            this.masterboxid2,
            this.colieNumber});
            this.dataGridView2.Location = new System.Drawing.Point(12, 57);
            this.dataGridView2.MultiSelect = false;
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.ReadOnly = true;
            this.dataGridView2.RowHeadersVisible = false;
            this.dataGridView2.Size = new System.Drawing.Size(529, 416);
            this.dataGridView2.TabIndex = 7;
            // 
            // itemid2
            // 
            this.itemid2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.itemid2.DataPropertyName = "itemid";
            this.itemid2.HeaderText = "Item Id";
            this.itemid2.Name = "itemid2";
            this.itemid2.ReadOnly = true;
            this.itemid2.Visible = false;
            // 
            // model_name2
            // 
            this.model_name2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.model_name2.DataPropertyName = "model_name";
            this.model_name2.HeaderText = "Product Name";
            this.model_name2.Name = "model_name2";
            this.model_name2.ReadOnly = true;
            this.model_name2.Visible = false;
            // 
            // masterboxid2
            // 
            this.masterboxid2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.masterboxid2.DataPropertyName = "masterboxid";
            this.masterboxid2.HeaderText = "Box PU Id";
            this.masterboxid2.Name = "masterboxid2";
            this.masterboxid2.ReadOnly = true;
            // 
            // colieNumber
            // 
            this.colieNumber.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colieNumber.DataPropertyName = "colieNumber";
            this.colieNumber.HeaderText = "Colie";
            this.colieNumber.Name = "colieNumber";
            this.colieNumber.ReadOnly = true;
            // 
            // lblQtyScanned
            // 
            this.lblQtyScanned.AutoSize = true;
            this.lblQtyScanned.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQtyScanned.Location = new System.Drawing.Point(284, 25);
            this.lblQtyScanned.Name = "lblQtyScanned";
            this.lblQtyScanned.Size = new System.Drawing.Size(19, 21);
            this.lblQtyScanned.TabIndex = 116;
            this.lblQtyScanned.Text = "0";
            // 
            // lblQty
            // 
            this.lblQty.AutoSize = true;
            this.lblQty.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQty.Location = new System.Drawing.Point(157, 23);
            this.lblQty.Name = "lblQty";
            this.lblQty.Size = new System.Drawing.Size(129, 21);
            this.lblQty.TabIndex = 115;
            this.lblQty.Text = "Qty on Progress :";
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.lblQtyMasterbox);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.lblQtyTotal);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.lblQtyScanned);
            this.groupBox1.Controls.Add(this.dataGridView1);
            this.groupBox1.Controls.Add(this.lblQty);
            this.groupBox1.Controls.Add(this.buttonClearSelected);
            this.groupBox1.Controls.Add(this.buttonClearAll);
            this.groupBox1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(19, 142);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(685, 554);
            this.groupBox1.TabIndex = 119;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Scanned";
            // 
            // lblQtyTotal
            // 
            this.lblQtyTotal.AutoSize = true;
            this.lblQtyTotal.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQtyTotal.Location = new System.Drawing.Point(86, 25);
            this.lblQtyTotal.Name = "lblQtyTotal";
            this.lblQtyTotal.Size = new System.Drawing.Size(19, 21);
            this.lblQtyTotal.TabIndex = 121;
            this.lblQtyTotal.Text = "0";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(10, 24);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(78, 21);
            this.label9.TabIndex = 120;
            this.label9.Text = "Qty Total :";
            // 
            // buttonClearSelected
            // 
            this.buttonClearSelected.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonClearSelected.Enabled = false;
            this.buttonClearSelected.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonClearSelected.Image = global::Manual_Packer_Station.Properties.Resources.clear;
            this.buttonClearSelected.Location = new System.Drawing.Point(11, 462);
            this.buttonClearSelected.Name = "buttonClearSelected";
            this.buttonClearSelected.Size = new System.Drawing.Size(150, 40);
            this.buttonClearSelected.TabIndex = 119;
            this.buttonClearSelected.Text = "Clear Selected";
            this.buttonClearSelected.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonClearSelected.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.buttonClearSelected.UseVisualStyleBackColor = true;
            this.buttonClearSelected.Visible = false;
            this.buttonClearSelected.Click += new System.EventHandler(this.buttonClearSelected_Click_1);
            // 
            // buttonClearAll
            // 
            this.buttonClearAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonClearAll.Enabled = false;
            this.buttonClearAll.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonClearAll.Image = global::Manual_Packer_Station.Properties.Resources.clear;
            this.buttonClearAll.Location = new System.Drawing.Point(167, 462);
            this.buttonClearAll.Name = "buttonClearAll";
            this.buttonClearAll.Size = new System.Drawing.Size(150, 40);
            this.buttonClearAll.TabIndex = 118;
            this.buttonClearAll.Text = "Clear All";
            this.buttonClearAll.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonClearAll.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.buttonClearAll.UseVisualStyleBackColor = true;
            this.buttonClearAll.Visible = false;
            this.buttonClearAll.Click += new System.EventHandler(this.buttonClearAll_Click_1);
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.dataGridView2);
            this.groupBox2.Controls.Add(this.btnConfirm);
            this.groupBox2.Controls.Add(this.lblMas);
            this.groupBox2.Controls.Add(this.btnPrintDO);
            this.groupBox2.Controls.Add(this.txtBasket);
            this.groupBox2.Controls.Add(this.labelQtyHistory);
            this.groupBox2.Controls.Add(this.lblQty1);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.buttonPrint);
            this.groupBox2.Controls.Add(this.txtETA);
            this.groupBox2.Controls.Add(this.txtVehicle);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.txtExpedisi);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(701, 142);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(553, 554);
            this.groupBox2.TabIndex = 120;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Box Polyurethane";
            // 
            // btnConfirm
            // 
            this.btnConfirm.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnConfirm.Enabled = false;
            this.btnConfirm.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConfirm.Image = global::Manual_Packer_Station.Properties.Resources.submit;
            this.btnConfirm.Location = new System.Drawing.Point(393, 511);
            this.btnConfirm.Name = "btnConfirm";
            this.btnConfirm.Size = new System.Drawing.Size(150, 40);
            this.btnConfirm.TabIndex = 170;
            this.btnConfirm.Text = "Confirm";
            this.btnConfirm.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnConfirm.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnConfirm.UseVisualStyleBackColor = true;
            this.btnConfirm.Click += new System.EventHandler(this.btnConfirmLocation_Click);
            // 
            // lblMas
            // 
            this.lblMas.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblMas.AutoSize = true;
            this.lblMas.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMas.Location = new System.Drawing.Point(9, 481);
            this.lblMas.Name = "lblMas";
            this.lblMas.Size = new System.Drawing.Size(66, 21);
            this.lblMas.TabIndex = 120;
            this.lblMas.Text = "Box PU :";
            // 
            // btnPrintDO
            // 
            this.btnPrintDO.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPrintDO.Enabled = false;
            this.btnPrintDO.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrintDO.Image = global::Manual_Packer_Station.Properties.Resources.print;
            this.btnPrintDO.Location = new System.Drawing.Point(237, 511);
            this.btnPrintDO.Name = "btnPrintDO";
            this.btnPrintDO.Size = new System.Drawing.Size(150, 40);
            this.btnPrintDO.TabIndex = 125;
            this.btnPrintDO.Text = "Print Label PS";
            this.btnPrintDO.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnPrintDO.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnPrintDO.UseVisualStyleBackColor = true;
            this.btnPrintDO.Click += new System.EventHandler(this.btnPrintDO_Click);
            // 
            // txtBasket
            // 
            this.txtBasket.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBasket.Enabled = false;
            this.txtBasket.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBasket.Location = new System.Drawing.Point(81, 479);
            this.txtBasket.Name = "txtBasket";
            this.txtBasket.Size = new System.Drawing.Size(462, 26);
            this.txtBasket.TabIndex = 119;
            this.txtBasket.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtBasket_KeyDown_1);
            this.txtBasket.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBasket_KeyPress);
            // 
            // labelQtyHistory
            // 
            this.labelQtyHistory.AutoSize = true;
            this.labelQtyHistory.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelQtyHistory.Location = new System.Drawing.Point(92, 24);
            this.labelQtyHistory.Name = "labelQtyHistory";
            this.labelQtyHistory.Size = new System.Drawing.Size(19, 21);
            this.labelQtyHistory.TabIndex = 124;
            this.labelQtyHistory.Text = "0";
            // 
            // lblQty1
            // 
            this.lblQty1.AutoSize = true;
            this.lblQty1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQty1.Location = new System.Drawing.Point(9, 23);
            this.lblQty1.Name = "lblQty1";
            this.lblQty1.Size = new System.Drawing.Size(77, 21);
            this.lblQty1.TabIndex = 123;
            this.lblQty1.Text = "Quantity :";
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(24, 317);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(91, 21);
            this.label5.TabIndex = 167;
            this.label5.Text = "Vehicle No :";
            this.label5.Visible = false;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(168, 317);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 21);
            this.label2.TabIndex = 159;
            this.label2.Text = "ETT :";
            this.label2.Visible = false;
            // 
            // buttonPrint
            // 
            this.buttonPrint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonPrint.Enabled = false;
            this.buttonPrint.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonPrint.Image = global::Manual_Packer_Station.Properties.Resources.print;
            this.buttonPrint.Location = new System.Drawing.Point(81, 511);
            this.buttonPrint.Name = "buttonPrint";
            this.buttonPrint.Size = new System.Drawing.Size(150, 40);
            this.buttonPrint.TabIndex = 122;
            this.buttonPrint.Text = "Print Box PU";
            this.buttonPrint.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonPrint.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.buttonPrint.UseVisualStyleBackColor = true;
            this.buttonPrint.Click += new System.EventHandler(this.buttonPrint_Click_1);
            // 
            // txtETA
            // 
            this.txtETA.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtETA.Enabled = false;
            this.txtETA.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtETA.Location = new System.Drawing.Point(172, 341);
            this.txtETA.Name = "txtETA";
            this.txtETA.Size = new System.Drawing.Size(95, 26);
            this.txtETA.TabIndex = 2;
            this.txtETA.Visible = false;
            this.txtETA.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtETA_KeyPress);
            // 
            // txtVehicle
            // 
            this.txtVehicle.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtVehicle.Enabled = false;
            this.txtVehicle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVehicle.Location = new System.Drawing.Point(28, 341);
            this.txtVehicle.Name = "txtVehicle";
            this.txtVehicle.ReadOnly = true;
            this.txtVehicle.Size = new System.Drawing.Size(138, 26);
            this.txtVehicle.TabIndex = 3;
            this.txtVehicle.Visible = false;
            this.txtVehicle.Click += new System.EventHandler(this.txtVehicle_Click);
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(24, 264);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 21);
            this.label4.TabIndex = 165;
            this.label4.Text = "Expedisi :";
            this.label4.Visible = false;
            // 
            // txtExpedisi
            // 
            this.txtExpedisi.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtExpedisi.Enabled = false;
            this.txtExpedisi.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtExpedisi.Location = new System.Drawing.Point(28, 288);
            this.txtExpedisi.Name = "txtExpedisi";
            this.txtExpedisi.ReadOnly = true;
            this.txtExpedisi.Size = new System.Drawing.Size(239, 26);
            this.txtExpedisi.TabIndex = 1;
            this.txtExpedisi.Visible = false;
            this.txtExpedisi.Click += new System.EventHandler(this.txtExpedisi_Click);
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(273, 343);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(38, 21);
            this.label7.TabIndex = 168;
            this.label7.Text = "Jam";
            this.label7.Visible = false;
            // 
            // lblPrinter
            // 
            this.lblPrinter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblPrinter.AutoSize = true;
            this.lblPrinter.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrinter.Location = new System.Drawing.Point(1334, 273);
            this.lblPrinter.Name = "lblPrinter";
            this.lblPrinter.Size = new System.Drawing.Size(56, 21);
            this.lblPrinter.TabIndex = 129;
            this.lblPrinter.Text = "Online";
            // 
            // label16
            // 
            this.label16.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(1256, 312);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(64, 21);
            this.label16.TabIndex = 128;
            this.label16.Text = "Printer :";
            // 
            // lblCamera
            // 
            this.lblCamera.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCamera.AutoSize = true;
            this.lblCamera.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCamera.Location = new System.Drawing.Point(1334, 372);
            this.lblCamera.Name = "lblCamera";
            this.lblCamera.Size = new System.Drawing.Size(56, 21);
            this.lblCamera.TabIndex = 127;
            this.lblCamera.Text = "Online";
            this.lblCamera.Visible = false;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(1256, 412);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 20);
            this.label3.TabIndex = 126;
            this.label3.Text = "Camera : ";
            this.label3.Visible = false;
            // 
            // grpDetail
            // 
            this.grpDetail.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.grpDetail.Controls.Add(this.lblUom);
            this.grpDetail.Controls.Add(this.lblProductName);
            this.grpDetail.Controls.Add(this.lblProd);
            this.grpDetail.Controls.Add(this.txtAddress);
            this.grpDetail.Controls.Add(this.txtCost);
            this.grpDetail.Controls.Add(this.lblDetail2);
            this.grpDetail.Controls.Add(this.lblDetail);
            this.grpDetail.Controls.Add(this.button1);
            this.grpDetail.Controls.Add(this.dgvDetail);
            this.grpDetail.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpDetail.Location = new System.Drawing.Point(386, 277);
            this.grpDetail.Name = "grpDetail";
            this.grpDetail.Size = new System.Drawing.Size(868, 419);
            this.grpDetail.TabIndex = 125;
            this.grpDetail.TabStop = false;
            this.grpDetail.Text = "Detail";
            this.grpDetail.Visible = false;
            // 
            // lblUom
            // 
            this.lblUom.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblUom.AutoSize = true;
            this.lblUom.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUom.Location = new System.Drawing.Point(9, 396);
            this.lblUom.Name = "lblUom";
            this.lblUom.Size = new System.Drawing.Size(43, 20);
            this.lblUom.TabIndex = 138;
            this.lblUom.Text = "Uom";
            // 
            // lblProductName
            // 
            this.lblProductName.AutoSize = true;
            this.lblProductName.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProductName.Location = new System.Drawing.Point(164, 148);
            this.lblProductName.Name = "lblProductName";
            this.lblProductName.Size = new System.Drawing.Size(145, 21);
            this.lblProductName.TabIndex = 137;
            this.lblProductName.Text = "Costumer Address :";
            // 
            // lblProd
            // 
            this.lblProd.AutoSize = true;
            this.lblProd.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProd.Location = new System.Drawing.Point(9, 148);
            this.lblProd.Name = "lblProd";
            this.lblProd.Size = new System.Drawing.Size(117, 21);
            this.lblProd.TabIndex = 136;
            this.lblProd.Text = "Product Name :";
            // 
            // txtAddress
            // 
            this.txtAddress.Location = new System.Drawing.Point(162, 82);
            this.txtAddress.Multiline = true;
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.ReadOnly = true;
            this.txtAddress.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtAddress.Size = new System.Drawing.Size(221, 50);
            this.txtAddress.TabIndex = 135;
            // 
            // txtCost
            // 
            this.txtCost.Location = new System.Drawing.Point(162, 29);
            this.txtCost.Multiline = true;
            this.txtCost.Name = "txtCost";
            this.txtCost.ReadOnly = true;
            this.txtCost.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtCost.Size = new System.Drawing.Size(221, 47);
            this.txtCost.TabIndex = 134;
            // 
            // lblDetail2
            // 
            this.lblDetail2.AutoSize = true;
            this.lblDetail2.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDetail2.Location = new System.Drawing.Point(8, 82);
            this.lblDetail2.Name = "lblDetail2";
            this.lblDetail2.Size = new System.Drawing.Size(145, 21);
            this.lblDetail2.TabIndex = 133;
            this.lblDetail2.Text = "Costumer Address :";
            // 
            // lblDetail
            // 
            this.lblDetail.AutoSize = true;
            this.lblDetail.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDetail.Location = new System.Drawing.Point(8, 32);
            this.lblDetail.Name = "lblDetail";
            this.lblDetail.Size = new System.Drawing.Size(131, 21);
            this.lblDetail.TabIndex = 132;
            this.lblDetail.Text = "Costumer Name :";
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Image = global::Manual_Packer_Station.Properties.Resources.confirm;
            this.button1.Location = new System.Drawing.Point(530, 363);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(150, 40);
            this.button1.TabIndex = 120;
            this.button1.Text = "Done";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // dgvDetail
            // 
            this.dgvDetail.AllowUserToAddRows = false;
            this.dgvDetail.AllowUserToDeleteRows = false;
            this.dgvDetail.AllowUserToResizeColumns = false;
            this.dgvDetail.AllowUserToResizeRows = false;
            dataGridViewCellStyle12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dgvDetail.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle12;
            this.dgvDetail.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvDetail.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDetail.Location = new System.Drawing.Point(12, 171);
            this.dgvDetail.MultiSelect = false;
            this.dgvDetail.Name = "dgvDetail";
            this.dgvDetail.ReadOnly = true;
            this.dgvDetail.RowHeadersVisible = false;
            this.dgvDetail.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvDetail.Size = new System.Drawing.Size(844, 186);
            this.dgvDetail.TabIndex = 7;
            this.dgvDetail.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvDetail_CellClick);
            this.dgvDetail.SelectionChanged += new System.EventHandler(this.dgvDetail_SelectionChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(15, 108);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(119, 21);
            this.label1.TabIndex = 131;
            this.label1.Text = "Delivery Order :";
            // 
            // txtScanned
            // 
            this.txtScanned.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtScanned.Enabled = false;
            this.txtScanned.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtScanned.Location = new System.Drawing.Point(144, 702);
            this.txtScanned.Name = "txtScanned";
            this.txtScanned.Size = new System.Drawing.Size(1110, 26);
            this.txtScanned.TabIndex = 135;
            this.txtScanned.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtScanned_KeyDown);
            this.txtScanned.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtScanned_KeyPress);
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(21, 702);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(111, 21);
            this.label6.TabIndex = 134;
            this.label6.Text = "Scanned Item :";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(148)))), ((int)(((byte)(172)))));
            this.panel1.Controls.Add(this.lblDate);
            this.panel1.Controls.Add(this.lblTime);
            this.panel1.Controls.Add(this.lblRole);
            this.panel1.Controls.Add(this.lblUserId);
            this.panel1.Controls.Add(this.pictureBox3);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.label21);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1422, 86);
            this.panel1.TabIndex = 149;
            // 
            // lblDate
            // 
            this.lblDate.AutoSize = true;
            this.lblDate.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDate.ForeColor = System.Drawing.Color.White;
            this.lblDate.Location = new System.Drawing.Point(216, 35);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(31, 15);
            this.lblDate.TabIndex = 152;
            this.lblDate.Text = "Date";
            // 
            // lblTime
            // 
            this.lblTime.AutoSize = true;
            this.lblTime.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTime.ForeColor = System.Drawing.Color.White;
            this.lblTime.Location = new System.Drawing.Point(216, 17);
            this.lblTime.Name = "lblTime";
            this.lblTime.Size = new System.Drawing.Size(35, 15);
            this.lblTime.TabIndex = 151;
            this.lblTime.Text = "Time";
            // 
            // lblRole
            // 
            this.lblRole.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblRole.AutoSize = true;
            this.lblRole.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRole.ForeColor = System.Drawing.Color.White;
            this.lblRole.Location = new System.Drawing.Point(1322, 41);
            this.lblRole.Name = "lblRole";
            this.lblRole.Size = new System.Drawing.Size(54, 21);
            this.lblRole.TabIndex = 150;
            this.lblRole.Text = "admin";
            // 
            // lblUserId
            // 
            this.lblUserId.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblUserId.AutoSize = true;
            this.lblUserId.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUserId.ForeColor = System.Drawing.Color.White;
            this.lblUserId.Location = new System.Drawing.Point(1321, 21);
            this.lblUserId.Name = "lblUserId";
            this.lblUserId.Size = new System.Drawing.Size(59, 21);
            this.lblUserId.TabIndex = 149;
            this.lblUserId.Text = "admin";
            // 
            // pictureBox3
            // 
            this.pictureBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox3.Image = global::Manual_Packer_Station.Properties.Resources.user;
            this.pictureBox3.Location = new System.Drawing.Point(1270, 17);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(45, 45);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox3.TabIndex = 148;
            this.pictureBox3.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(47)))), ((int)(((byte)(54)))));
            this.panel2.Controls.Add(this.pictureBox2);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(210, 86);
            this.panel2.TabIndex = 38;
            // 
            // pictureBox2
            // 
            this.pictureBox2.ImageLocation = "logo_biofarma.png";
            this.pictureBox2.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox2.InitialImage")));
            this.pictureBox2.Location = new System.Drawing.Point(30, 5);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(150, 75);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 129;
            this.pictureBox2.TabStop = false;
            // 
            // label14
            // 
            this.label14.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label14.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.White;
            this.label14.Location = new System.Drawing.Point(327, 41);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(937, 29);
            this.label14.TabIndex = 35;
            this.label14.Text = "DISTRIBUTION STATION";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label21
            // 
            this.label21.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label21.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.White;
            this.label21.Location = new System.Drawing.Point(321, 12);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(943, 29);
            this.label21.TabIndex = 36;
            this.label21.Text = "PACKING CONTROL";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // btnDeo
            // 
            this.btnDeo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDeo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeo.Image = global::Manual_Packer_Station.Properties.Resources.deco;
            this.btnDeo.Location = new System.Drawing.Point(1260, 89);
            this.btnDeo.Name = "btnDeo";
            this.btnDeo.Size = new System.Drawing.Size(150, 40);
            this.btnDeo.TabIndex = 157;
            this.btnDeo.Text = "Decommission";
            this.btnDeo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDeo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnDeo.UseVisualStyleBackColor = true;
            this.btnDeo.Visible = false;
            this.btnDeo.Click += new System.EventHandler(this.btnDeo_Click);
            // 
            // btnOpenLog
            // 
            this.btnOpenLog.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOpenLog.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOpenLog.Image = global::Manual_Packer_Station.Properties.Resources.log1;
            this.btnOpenLog.Location = new System.Drawing.Point(1260, 688);
            this.btnOpenLog.Name = "btnOpenLog";
            this.btnOpenLog.Size = new System.Drawing.Size(150, 40);
            this.btnOpenLog.TabIndex = 156;
            this.btnOpenLog.Text = "Log File";
            this.btnOpenLog.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnOpenLog.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnOpenLog.UseVisualStyleBackColor = true;
            this.btnOpenLog.Click += new System.EventHandler(this.btnOpenLog_Click);
            // 
            // btnIndicator
            // 
            this.btnIndicator.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnIndicator.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnIndicator.Image = global::Manual_Packer_Station.Properties.Resources.indicator;
            this.btnIndicator.Location = new System.Drawing.Point(1260, 642);
            this.btnIndicator.Name = "btnIndicator";
            this.btnIndicator.Size = new System.Drawing.Size(150, 40);
            this.btnIndicator.TabIndex = 155;
            this.btnIndicator.Text = "Indicator";
            this.btnIndicator.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnIndicator.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnIndicator.UseVisualStyleBackColor = true;
            this.btnIndicator.Click += new System.EventHandler(this.btnIndicator_Click);
            // 
            // onPrinter1
            // 
            this.onPrinter1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.onPrinter1.Image = global::Manual_Packer_Station.Properties.Resources.on1;
            this.onPrinter1.ImageLocation = "";
            this.onPrinter1.Location = new System.Drawing.Point(1338, 256);
            this.onPrinter1.Name = "onPrinter1";
            this.onPrinter1.Size = new System.Drawing.Size(14, 14);
            this.onPrinter1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.onPrinter1.TabIndex = 153;
            this.onPrinter1.TabStop = false;
            this.onPrinter1.Visible = false;
            // 
            // offPrinter1
            // 
            this.offPrinter1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.offPrinter1.Image = global::Manual_Packer_Station.Properties.Resources.off1;
            this.offPrinter1.ImageLocation = "";
            this.offPrinter1.Location = new System.Drawing.Point(1362, 256);
            this.offPrinter1.Name = "offPrinter1";
            this.offPrinter1.Size = new System.Drawing.Size(14, 14);
            this.offPrinter1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.offPrinter1.TabIndex = 152;
            this.offPrinter1.TabStop = false;
            this.offPrinter1.Visible = false;
            // 
            // onCamera1
            // 
            this.onCamera1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.onCamera1.Image = global::Manual_Packer_Station.Properties.Resources.on1;
            this.onCamera1.ImageLocation = "";
            this.onCamera1.Location = new System.Drawing.Point(1338, 359);
            this.onCamera1.Name = "onCamera1";
            this.onCamera1.Size = new System.Drawing.Size(14, 14);
            this.onCamera1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.onCamera1.TabIndex = 151;
            this.onCamera1.TabStop = false;
            this.onCamera1.Visible = false;
            // 
            // offCamera1
            // 
            this.offCamera1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.offCamera1.Image = global::Manual_Packer_Station.Properties.Resources.off1;
            this.offCamera1.ImageLocation = "";
            this.offCamera1.Location = new System.Drawing.Point(1362, 359);
            this.offCamera1.Name = "offCamera1";
            this.offCamera1.Size = new System.Drawing.Size(14, 14);
            this.offCamera1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.offCamera1.TabIndex = 150;
            this.offCamera1.TabStop = false;
            this.offCamera1.Visible = false;
            // 
            // btnStart
            // 
            this.btnStart.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStart.Image = global::Manual_Packer_Station.Properties.Resources.start;
            this.btnStart.Location = new System.Drawing.Point(330, 98);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(150, 40);
            this.btnStart.TabIndex = 4;
            this.btnStart.Text = "Start";
            this.btnStart.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnStart.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // pictureBoxPrinter
            // 
            this.pictureBoxPrinter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBoxPrinter.ImageLocation = "zebra-printer.png";
            this.pictureBoxPrinter.Location = new System.Drawing.Point(1260, 256);
            this.pictureBoxPrinter.Name = "pictureBoxPrinter";
            this.pictureBoxPrinter.Size = new System.Drawing.Size(55, 50);
            this.pictureBoxPrinter.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxPrinter.TabIndex = 125;
            this.pictureBoxPrinter.TabStop = false;
            // 
            // pictureBoxCamera
            // 
            this.pictureBoxCamera.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBoxCamera.ImageLocation = "genie-nano-camera.png";
            this.pictureBoxCamera.Location = new System.Drawing.Point(1260, 359);
            this.pictureBoxCamera.Name = "pictureBoxCamera";
            this.pictureBoxCamera.Size = new System.Drawing.Size(55, 50);
            this.pictureBoxCamera.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxCamera.TabIndex = 124;
            this.pictureBoxCamera.TabStop = false;
            this.pictureBoxCamera.Visible = false;
            // 
            // btnConfig
            // 
            this.btnConfig.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnConfig.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConfig.Image = global::Manual_Packer_Station.Properties.Resources.config;
            this.btnConfig.Location = new System.Drawing.Point(1260, 198);
            this.btnConfig.Name = "btnConfig";
            this.btnConfig.Size = new System.Drawing.Size(150, 40);
            this.btnConfig.TabIndex = 105;
            this.btnConfig.Text = "Config";
            this.btnConfig.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnConfig.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnConfig.UseVisualStyleBackColor = true;
            this.btnConfig.Click += new System.EventHandler(this.btnConfig_Click);
            // 
            // btnLogout
            // 
            this.btnLogout.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLogout.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLogout.Image = global::Manual_Packer_Station.Properties.Resources.logout;
            this.btnLogout.Location = new System.Drawing.Point(1260, 152);
            this.btnLogout.Name = "btnLogout";
            this.btnLogout.Size = new System.Drawing.Size(150, 40);
            this.btnLogout.TabIndex = 104;
            this.btnLogout.Text = "Logout";
            this.btnLogout.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnLogout.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnLogout.UseVisualStyleBackColor = true;
            this.btnLogout.Click += new System.EventHandler(this.btnLogout_Click);
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(554, 106);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(688, 23);
            this.progressBar1.TabIndex = 158;
            // 
            // txtDO
            // 
            this.txtDO.Font = new System.Drawing.Font("Segoe UI", 15F);
            this.txtDO.Location = new System.Drawing.Point(140, 101);
            this.txtDO.Name = "txtDO";
            this.txtDO.ReadOnly = true;
            this.txtDO.Size = new System.Drawing.Size(184, 34);
            this.txtDO.TabIndex = 159;
            this.txtDO.Click += new System.EventHandler(this.txtDO_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(347, 25);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(152, 21);
            this.label8.TabIndex = 122;
            this.label8.Text = "Max Qty Masterbox :";
            // 
            // lblQtyMasterbox
            // 
            this.lblQtyMasterbox.AutoSize = true;
            this.lblQtyMasterbox.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQtyMasterbox.Location = new System.Drawing.Point(495, 26);
            this.lblQtyMasterbox.Name = "lblQtyMasterbox";
            this.lblQtyMasterbox.Size = new System.Drawing.Size(19, 21);
            this.lblQtyMasterbox.TabIndex = 123;
            this.lblQtyMasterbox.Text = "0";
            // 
            // ManualPacker
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1422, 740);
            this.Controls.Add(this.txtDO);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.btnDeo);
            this.Controls.Add(this.btnOpenLog);
            this.Controls.Add(this.btnIndicator);
            this.Controls.Add(this.onPrinter1);
            this.Controls.Add(this.offPrinter1);
            this.Controls.Add(this.onCamera1);
            this.Controls.Add(this.offCamera1);
            this.Controls.Add(this.txtScanned);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblPrinter);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.lblCamera);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.pictureBoxPrinter);
            this.Controls.Add(this.pictureBoxCamera);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnConfig);
            this.Controls.Add(this.btnLogout);
            this.Controls.Add(this.grpDetail);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ManualPacker";
            this.Text = "Packing Control";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.ManualPacker_Load);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.ManualPacker_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.grpDetail.ResumeLayout(false);
            this.grpDetail.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDetail)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.onPrinter1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.offPrinter1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.onCamera1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.offCamera1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPrinter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCamera)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.DataGridView dataGridView1;
        public System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.Button btnConfig;
        private System.Windows.Forms.Button btnLogout;
        public System.Windows.Forms.Label lblQtyScanned;
        private System.Windows.Forms.Label lblQty;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtBasket;
        public System.Windows.Forms.Label lblPrinter;
        private System.Windows.Forms.Label label16;
        public System.Windows.Forms.Label lblCamera;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox pictureBoxPrinter;
        private System.Windows.Forms.PictureBox pictureBoxCamera;
        private System.Windows.Forms.Label lblMas;
        private System.Windows.Forms.Button buttonClearSelected;
        private System.Windows.Forms.Button buttonClearAll;
        private System.Windows.Forms.Button buttonPrint;
        public System.Windows.Forms.Label labelQtyHistory;
        private System.Windows.Forms.Label lblQty1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.TextBox txtScanned;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblDetail2;
        private System.Windows.Forms.Label lblDetail;
        public System.Windows.Forms.GroupBox grpDetail;
        public System.Windows.Forms.TextBox txtCost;
        public System.Windows.Forms.TextBox txtAddress;
        public System.Windows.Forms.DataGridView dgvDetail;
        private System.Windows.Forms.Label lblProd;
        public System.Windows.Forms.Label lblUom;
        public System.Windows.Forms.Label lblProductName;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblDate;
        private System.Windows.Forms.Label lblTime;
        public System.Windows.Forms.Label lblRole;
        public System.Windows.Forms.Label lblUserId;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.PictureBox offCamera1;
        private System.Windows.Forms.PictureBox onCamera1;
        private System.Windows.Forms.PictureBox onPrinter1;
        private System.Windows.Forms.PictureBox offPrinter1;
        private System.Windows.Forms.Button btnIndicator;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button btnOpenLog;
        public System.Windows.Forms.Button btnDeo;
        private System.Windows.Forms.Button btnPrintDO;
        private System.Windows.Forms.TextBox txtETA;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtVehicle;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtExpedisi;
        private System.Windows.Forms.Label label7;
        public System.Windows.Forms.Label lblQtyTotal;
        private System.Windows.Forms.Label label9;
        private System.ComponentModel.BackgroundWorker bgUpdateVaccine;
        private System.Windows.Forms.Timer timerUpdateVaccine;
        private System.Windows.Forms.DataGridViewTextBoxColumn itemid2;
        private System.Windows.Forms.DataGridViewTextBoxColumn model_name2;
        private System.Windows.Forms.DataGridViewTextBoxColumn masterboxid2;
        private System.Windows.Forms.DataGridViewTextBoxColumn colieNumber;
        public System.Windows.Forms.Button btnConfirm;
        private System.Windows.Forms.DataGridViewTextBoxColumn state;
        private System.Windows.Forms.DataGridViewTextBoxColumn state_text;
        private System.Windows.Forms.DataGridViewTextBoxColumn uom;
        private System.Windows.Forms.DataGridViewTextBoxColumn qty_1;
        private System.Windows.Forms.DataGridViewTextBoxColumn itemid;
        private System.Windows.Forms.DataGridViewTextBoxColumn model_name;
        private System.Windows.Forms.DataGridViewTextBoxColumn qty;
        private System.Windows.Forms.DataGridViewTextBoxColumn qty_scanned;
        private System.Windows.Forms.DataGridViewTextBoxColumn uom_vial;
        private System.Windows.Forms.DataGridViewTextBoxColumn timestamp;
        private System.Windows.Forms.DataGridViewTextBoxColumn isSAS;
        private System.Windows.Forms.DataGridViewTextBoxColumn outbox_qty;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.TextBox txtDO;
        public System.Windows.Forms.Label lblQtyMasterbox;
        private System.Windows.Forms.Label label8;
    }
}

