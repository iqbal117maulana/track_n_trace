﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Manual_Packer_Station
{
    public partial class config : Form
    { 
        public config()
        {
            InitializeComponent();
            getConfig();
        }

        void getConfig()
        {
            sqlitecs sql = new sqlitecs();
            try
            {
                txtIpPrinter.Text = sql.config["ipprinter"];
                txtPortPrinter.Text = sql.config["portprinter"];
                txtPortServer.Text = sql.config["portserver"];
                txtIpServer.Text = sql.config["ipServer"];
                txtIpDB.Text = sql.config["ipDB"];
                txtPortDb.Text = sql.config["portDB"];
                txtTimeoutDb.Text = sql.config["timeout"];
                txtIpCamera2.Text = sql.config["ipCamera"];
                txtPortCamera2.Text = sql.config["portCamera"];
                txtNamaDb.Text = sql.config["namaDB"];
                txtCompID.Text = sql.config["CompId"];
                txtNamaDb2.Text = sql.config["namaDB2"];
            }
            catch (KeyNotFoundException k)
            {
              //  sql.simpanLog("Data Tidak Ditemukan : " + k.ToString());
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Confirm cf = new Confirm("Are you sure? ", "Confirmation");
            bool hasilDialog = cf.conf();
            if (hasilDialog)
            {
                if (validate())
                {

                    sqlitecs sql = new sqlitecs();
                    Dictionary<string, string> field = new Dictionary<string, string>();
                    field.Add("ipPrinter", txtIpPrinter.Text);
                    field.Add("portPrinter", "" + int.Parse(txtPortPrinter.Text));
                    field.Add("portserver", "" + int.Parse(txtPortServer.Text));
                    field.Add("ipServer", txtIpServer.Text);
                    field.Add("ipDB", txtIpDB.Text);
                    field.Add("portDB", "" + int.Parse(txtPortDb.Text));
                    field.Add("timeout", txtTimeoutDb.Text);
                    field.Add("namadb", txtNamaDb.Text);
                    field.Add("CompId", txtCompID.Text);
                    field.Add("namaDB2", txtNamaDb2.Text);
                    sql.update(field, "config", "");
                    new Confirm("Configuration success saved");
                    this.Dispose();
                }
                else
                {
                    new Confirm("Invalid configuration data");
                }
            }
        }

        private bool validate()
        {
            dbaccess db = new dbaccess();
            string kesalahan = "Error Input from : ";
            bool kembali = true;


            if (!db.validasi(txtIpCamera2.Text, 0))
            {
                kesalahan = kesalahan + txtIpCamera2.Text + "\n";
                kembali = false;
            }

            if (!db.validasi(txtIpDB.Text, 0))
            {
                kesalahan = kesalahan + txtIpCamera2.Text + "\n";
                kembali = false;
            }

            if (!db.validasi(txtIpDB.Text, 0))
            {
                kesalahan = kesalahan + txtIpDB.Text + "\n";
                kembali = false;
            }


            if (!db.validasi(txtIpPrinter.Text, 0))
            {
                kesalahan = kesalahan + txtIpPrinter.Text + "\n";
                kembali = false;
            }

            if (!db.validasi(txtIpServer.Text, 0))
            {
                kesalahan = kesalahan + txtIpServer.Text + "\n";
                kembali = false;
            }


            if (!db.validasi(txtPortCamera2.Text, 3))
            {
                kesalahan = kesalahan + txtPortCamera2.Text + "\n";
                kembali = false;
            }

            if (!db.validasi(txtPortDb.Text, 3))
            {
                kesalahan = kesalahan + txtPortDb.Text + "\n";
                kembali = false;
            }

            if (!db.validasi(txtPortPrinter.Text, 3))
            {
                kesalahan = kesalahan + txtPortPrinter.Text + "\n";
                kembali = false;
            }

            if (!db.validasi(txtPortServer.Text, 3))
            {
                kesalahan = kesalahan + txtPortServer.Text + "\n";
                kembali = false;
            }

            if (!kembali)
                new Confirm(kesalahan);
            return kembali;
        }

        private void txtCompID_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }
        }
    }
}
