﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.IO;

namespace Manual_Packer_Station
{
    public partial class Log : Form
    {
        public bool isClear = false;

        public Log(string logfile)
        {
            InitializeComponent();
            txtLog.Text = logfile;
            //parameter = param;
            //linenumber = line;
            //InitializeComponent();
            //refresh();
            //SetDataGridListOrder();
            //dsbb = dsb;
            //frm = from;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            txtLog.Text = "";
            isClear = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            SaveFileDialog save = new SaveFileDialog();
            save.FileName = "ReceivingLog.txt";
            save.Filter = "Text File | *.txt";
            if (save.ShowDialog() == DialogResult.OK)
            {
                StreamWriter writer = new StreamWriter(save.OpenFile());                
                {
                    writer.WriteLine(txtLog.Text);
                }
                writer.Dispose();
                writer.Close();
            }
        }        
    }
}
