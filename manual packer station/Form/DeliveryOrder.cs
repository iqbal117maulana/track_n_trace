﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace Manual_Packer_Station
{
    public partial class DeliveryOrder : Form
    {
        dbaccess Db = new dbaccess();
        DataTable DtExpedisi;
        public String DeliveryOrderNumber;
        public String PLN;

        public DeliveryOrder()
        {
            InitializeComponent();
            InitData();
            LoadData();

        }
        private void InitData()
        {
            DeliveryOrderNumber = "";
            dgvData.Columns["NO"].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
        }

        private void LoadData()
        {
            DtExpedisi = Db.LoadShippingOrder();
            dgvData.DataSource = DtExpedisi;
        }

        private void dgvData_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                DeliveryOrderNumber = dgvData.Rows[e.RowIndex].Cells["DO_NUMBER"].Value.ToString();
                PLN = dgvData.Rows[e.RowIndex].Cells["PICKING_LIST_NUMBER"].Value.ToString();
                this.DialogResult = DialogResult.Cancel;
            }

        }

        private void dgvData_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                dgvData.Rows[e.RowIndex].Cells["NO"].Value = e.RowIndex + 1;
            }

        }

        private void txtSearch_KeyPress(object sender, KeyPressEventArgs e)
        {
            var regex = new Regex(@"[^a-zA-Z0-9\b]");
            if (regex.IsMatch(e.KeyChar.ToString()))
            {
                e.Handled = true;
            }

        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            (dgvData.DataSource as DataTable).DefaultView.RowFilter = string.Format("[DO_NUMBER] LIKE '%{0}%'", txtSearch.Text);
        }   

    }
}
