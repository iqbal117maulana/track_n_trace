﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Drawing.Printing;
using System.IO;
using System.Management;
using System.Configuration;
using System.Net.Sockets;
using System.Threading;

namespace Manual_Packer_Station
{
    public partial class SAS : Form
    {
        private Dictionary<string, string[]> databasket;
        private Thread CameraAsyncTask;
        private Thread CameraDataAsyncTask;
        private int Interval = 10000; //ms
        bool nyala = true;
        public string linenumber;
        public string linename;
        dbaccess db;
        sqlitecs sqlite;
        string[] dataProd;
        string ShippingOrderNumber;
        public string BatchNumber;
        string GTIN;
        string ExpiryDate;
        string ProductModelId;
        string ProductModelName;
        string CustomerName;
        string CustomerAddress;
        string adminid;
        List<string[]> datacombo;
        bool isStarted = false;
        string masterBoxId;
        string gsOneMasterBoxId;
        ControlSAS ms;
        public System.Timers.Timer delayTimer;
        public string LogFile = "";

        //tambahan disable close button
        private const int CP_NOCLOSE_BUTTON = 0x200;
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams myCp = base.CreateParams;
                myCp.ClassStyle = myCp.ClassStyle | CP_NOCLOSE_BUTTON;
                return myCp;
            }
        }
        //end of disable close button
        
        public SAS(string admin)
        {
            InitializeComponent();
            databasket = new Dictionary<string, string[]>();
            db = new dbaccess();
            adminid = admin;
            sqlite = new sqlitecs();
            ms = new ControlSAS(this);
            ms.startStation();

            setAdminName(admin);

            delayTimer = new System.Timers.Timer();
            delayTimer.Interval = 60000;
            delayTimer.Elapsed += new System.Timers.ElapsedEventHandler(delayTimer_Elapsed);
            delayTimer.Start();

            CheckDevice();

            SetDataTable();
            ResetInfo();
            ms.log("Start server camera port : " + sqlite.config["portserver"]);
        }

        public void setAdminName(string admin)
        {
            List<string> field = new List<string>();
            field.Add("first_name, company ");
            string where = "id = '" + admin + "'";
            List<string[]> ds = ms.db.selectList(field, "[USERS]", where);
            //if (ms.db.num_rows > 0)
            if (ds != null)
            {
                lblUserId.Text = ds[0][0];
                lblRole.Text = ds[0][1];
            }
        }

        private void CheckDevice()
        {
            if (ms.PingHost(sqlite.config["ipprinter"]))
            {
                lblPrinter.Text = "Online";
                onPrinter1.Visible = true;
                offPrinter1.Visible = false;
            }
            else
            {
                lblPrinter.Text = "Offline";
                onPrinter1.Visible = false;
                offPrinter1.Visible = true;
            }
        }

        DataTable dataHistory;
        private void SetDataTable()
        {
            dataHistory = new DataTable();
            dataHistory.Columns.Add("Item ID");
            dataHistory.Columns.Add("Master Box ID ");
            dataHistory.Columns.Add("Colie");
            dataGridView2.DataSource = dataHistory;
        }

        private void ResetInfo()
        {
            BatchNumber = "";
            GTIN = "";
            ExpiryDate = "";
            ShippingOrderNumber = "";
            masterBoxId = "";
            gsOneMasterBoxId = "";
            ProductModelId = "";
            ProductModelName = "";
            CustomerAddress = "";
            CustomerName = "";
            if (InvokeRequired)
            {}
            else
            {}
        }

        void delayTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            ms.CekDevice();
        }

        void delayTimer_Elapsed()
        {
            ms.CekDevice();
        }

        private void btnLogout_Click(object sender, EventArgs e)
        {
            Confirm cf = new Confirm("Are you sure? ", "Confirmation");
            bool result = cf.conf();
            if (result)
            {
                ms.close("");
                this.Dispose();
                //Login lg = new Login();
                //lg.Show();
            }
        }

        private void btnConfig_Click(object sender, EventArgs e)
        {
            config cfg = new config();
            cfg.ShowDialog();
        }

        private void btnOpenLog_Click(object sender, EventArgs e)
        {
            Log frm;
            frm = new Log(LogFile);
            frm.ShowDialog();
            if (frm.isClear == true)
                LogFile = "";
        }

        private void btnIndicator_Click(object sender, EventArgs e)
        {
            CheckDevice();
        }

        void SetFormActive(bool a)
        {
            txtBasket.Enabled = a;
            buttonPrint.Enabled = a;
            btnPrintDO.Enabled = a;
            txtETA.Enabled = !a;
        }
    }
}
