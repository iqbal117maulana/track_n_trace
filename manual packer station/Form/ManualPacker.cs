﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Drawing.Printing;
using System.IO;
using System.Management;
using System.Configuration;
using System.Net.Sockets;
using System.Threading;
using System.Text.RegularExpressions;

namespace Manual_Packer_Station
{
    public partial class ManualPacker : Form
    {
        //private Dictionary<string, string[]> databasket;       
        public string linenumber ;
        public string linename;
        dbaccess db;
        sqlitecs sqlite;
        public string BatchNumber;
        string GTIN;
        string ExpiryDate;
        string ProductModelId;
        string ProductModelName;
        string CustomerName;
        string CustomerAddress;
        string adminid;
        List<string[]> datacombo;
        bool isStarted= false;
        string masterBoxId;
        Control ms;
        public System.Timers.Timer delayTimer;
        public string LogFile = "";
        bool firstScan = true;
        bool isSas = false;
        public int colie = 0;
        List<string[]> StatusPrint;
        public string PickingListNumber;


        //int expedisiId = 0;
        //String expedisiName;
        //int vehicleId = 0;
        //String vehicleNo;

        bool SudahPrintPS = false;

        //tambahan disable close button
        private const int CP_NOCLOSE_BUTTON = 0x200;
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams myCp = base.CreateParams;
                myCp.ClassStyle = myCp.ClassStyle | CP_NOCLOSE_BUTTON;
                return myCp;
            }
        }
        //end of disable close button

        public ManualPacker(string admin)
        {
            InitializeComponent();
            db = new dbaccess();            
            adminid = admin;
            sqlite = new sqlitecs();
            ms = new Control(this);
            ms.adminid = admin;
            ms.startStation();                      

            setAdminName(admin);
            ClearAllScanned();
            progressBar1.Visible = false;

            delayTimer = new System.Timers.Timer();
            delayTimer.Interval = 60000;
            delayTimer.Elapsed += new System.Timers.ElapsedEventHandler(delayTimer_Elapsed);
            delayTimer.Start();
            CheckDevice();

            ResetInfo();
            //ms.log("Start server camera port : " + sqlite.config["portserver"]);
        }        

        public void setAdminName(string admin)
        {
            List<string> field = new List<string>();
            field.Add("a.first_name, c.name, c.id as role ");
            string where = " a.id = '" + admin + "'";
            string from = " users a INNER JOIN users_groups b ON a.id = b.user_id INNER JOIN groups c on b.group_id = c.id ";
            List<string[]> ds = ms.db.selectList(field, from, where);
            //if (ms.db.num_rows > 0)
            if (ds != null && ms.db.num_rows > 0)
            {
                lblUserId.Text = ds[0][0];
                lblRole.Text = ds[0][1];
                btnConfig.Visible = ds[0][2] == "1";
            }
        }

        private void CheckDevice()
        {
            try
            {
                if (ms.PingHost(sqlite.config["ipprinter"]))
                {
                    lblPrinter.Text = "Online";
                    onPrinter1.Visible = true;
                    offPrinter1.Visible = false;
                }
                else
                {
                    lblPrinter.Text = "Offline";
                    onPrinter1.Visible = false;
                    offPrinter1.Visible = true;
                }
            }
            catch (Exception ex)
            {

            }            
        }

        private bool CheckCamera()
        {
            if (ms.PingHost(sqlite.config["ipCamera"]))
            {
                lblCamera.Text = "Online";
                onCamera1.Visible = true;
                offCamera1.Visible = false;
                return true;
            }
            else
            {
                lblCamera.Text = "Offline";
                onCamera1.Visible = false;
                offCamera1.Visible = true;
                return true;
            }
        }

        void DelayCheckDevice()
        {
            delayTimer = new System.Timers.Timer();
            delayTimer.Interval = 1000;
            delayTimer.Elapsed += new System.Timers.ElapsedEventHandler(delayTimer_Elapsed);
            delayTimer.Start();
            CheckDevice();
        }

        void delayTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            ms.CekDevice();
        }

        void delayTimer_Elapsed()
        {
            ms.CekDevice();
        }
        bool cekdgv(string data, DataGridView dg)
        {
            for (int i = 0; i < dg.Rows.Count; i++)
            {
                if (data.Equals(dg.Rows[i].Cells[0].Value.ToString()))
                {
                    return true;
                }
            }
            return false;
        }

        public bool CekScanned(string itemid, string gsOne, string uom, bool isParent)
        {
            List<string> fieldCek = new List<string>();
            fieldCek.Add("itemid");
            fieldCek.Add("uom");
            fieldCek.Add("vial_qty");
            List<string[]> ds = sqlite.select(fieldCek, "tmp_captured",
                " (itemid = '" + itemid + "' or itemid = '" + gsOne + "') and uom = '" + uom + "'");
            if (sqlite.num_rows > 0)
            {
                if (!isParent)
                {
                    ms.log("Data " + itemid + " already scanned.");
                    Confirm cf = new Confirm("Data " + itemid + " already scanned.");
                }                
                return true;
            }
            else
            {
                return false;
            }
        }

        public int CekQtyScanned(string itemid, string gsOne, string uom)
        {
            List<string> fieldCek = new List<string>();
            fieldCek.Add("itemid");
            fieldCek.Add("uom");
            fieldCek.Add("vial_qty");
            if (itemid != "")
            {
                List<string[]> ds = sqlite.select(fieldCek, "tmp_captured",
                " (itemid IN (" + itemid.Substring(0, itemid.Length - 2) + ") " +
                " or itemid IN (" + gsOne.Substring(0, gsOne.Length - 2) + ")) and uom = '" + uom + "'");
                if (sqlite.num_rows > 0)
                {
                    return ds.Count();
                }
                else
                {
                    return 0;
                }
            }
            else
                return 0;            
        }

        bool CekParentScanned(out string hasil_itemid, string itemid, string uom, string batchN, string uomParent)
        {
            string sql = "";
            if (uom == ms.UOM_BASKET)
            {
                sql = "select distinct innerBoxId as itemid, innerboxGsOneId as gsOne, 'Innerbox' as uom from Vaccine where basketId = '" + itemid + "' " +
                        " AND batchNumber = '" + batchN + "'" +  
                      " UNION ALL " +
                      "select distinct capId as itemid, gsOneVialId as gsOne, 'Vial' as uom from Vaccine where basketId = '" + itemid + "' " +
                        " AND batchNumber = '" + batchN + "'";
            }
            else if (uom == ms.UOM_INNER_BOX)
            {
                sql = "SELECT DISTINCT basketId as itemid, '' as gsOne, 'Basket' as uom FROM Vaccine WHERE (innerBoxId = '" + itemid + "' or innerBoxGsOneId = '" + itemid + "') " +
                        " AND batchNumber = '" + batchN + "' "; 
                      //"UNION ALL " +
                      //"SELECT DISTINCT capId as itemid, gsOneVialId as gsOne, 'Vial' as uom from Vaccine where (innerBoxId = '" + itemid + "' or innerBoxGsOneId = '" + itemid + "') " +
                      //  " AND batchNumber = '" + batchN + "'";
            }
            else if (uom == ms.UOM_VIAL)
            {
                if (uomParent == ms.UOM_BASKET)
                {
                    sql = "SELECT DISTINCT basketId as itemid, '' as gsOne, 'Basket' as uom FROM Vaccine WHERE (capId = '" + itemid + "' OR gsOneVialId = '" + itemid + "') " +
                            " AND batchNumber = '" + batchN + "' " +
                          " UNION ALL " +
                          "SELECT DISTINCT innerBoxId as itemid, innerboxGsOneId as gsOne, 'Innerbox' FROM Vaccine WHERE (capId = '" + itemid + "' OR gsOneVialId = '" + itemid + "') " +
                            " AND batchNumber = '" + batchN + "'";
                }
                else if (uomParent == ms.UOM_INNER_BOX)
                {
                    sql = "SELECT DISTINCT innerBoxId as itemid, innerboxGsOneId as gsOne, 'Innerbox' FROM Vaccine WHERE (capId = '" + itemid + "' OR gsOneVialId = '" + itemid + "') " +
                            " AND batchNumber = '" + batchN + "'";
                }                
            }
            DataTable dtTmp;
            db.OpenQuery(out dtTmp, sql);
            if (dtTmp.Rows.Count > 0)
            {
                bool hasil = false;
                string tmpItemId = "";
                for (int i = 0; i <= dtTmp.Rows.Count - 1; i++)
                {
                    hasil = CekScanned(dtTmp.Rows[i][0].ToString(), dtTmp.Rows[i][1].ToString(), dtTmp.Rows[i][2].ToString(), true);
                    if (hasil)
                    {               
                        if (dtTmp.Rows[i][1].ToString() != "")
                            tmpItemId = dtTmp.Rows[i][1].ToString();
                        else
                            tmpItemId = dtTmp.Rows[i][0].ToString();
                        break;                        
                    }
                }
                hasil_itemid = tmpItemId;
                return hasil;
            }
            else
            {
                hasil_itemid = "";
                return false;
            }                
        }

        bool CekBoxExist(string itemid, string uom, int compare)
        {
            string sql = "";
            if (uom == ms.UOM_BASKET)
            {
                sql = "select count(masterboxId) from Vaccine where basketId = '" + itemid + "'";
            }
            else if (uom == ms.UOM_INNER_BOX)
            {
                sql = "select count(masterboxId) from Vaccine where innerBoxid = '" + itemid + "' OR innerBoxGsOneId = '" + itemid + "'";
            }
            else if (uom == ms.UOM_VIAL)
            {
                sql = "select count(masterboxId) from Vaccine where capId = '" + itemid + "' OR gsOneVialId = '" + itemid + "'";
            }
            DataTable dtTmp;
            db.OpenQuery(out dtTmp, sql);
            if (dtTmp.Rows.Count > 0)
            {
                if (Convert.ToInt32(dtTmp.Rows[0][0]) == 0)
                    return false;
                else
                    return Convert.ToInt32(dtTmp.Rows[0][0]) == compare;
            }
            else
                return false;
        }

        int getQtyVialBiasa(string itemid, string uom)
        {
            string sql = "";
            if (uom == ms.UOM_BASKET)
            {
                sql = "select count(gsoneVialId) from Vaccine where basketId = '" + itemid + "'";
            }
            else if (uom == ms.UOM_INNER_BOX)
            {
                sql = "select count(gsoneVialId) from Vaccine where innerBoxid = '" + itemid + "' OR innerBoxGsOneId = '" + itemid + "'";
            }
            else if (uom == ms.UOM_VIAL)
            {
                sql = "select count(gsoneVialId) from Vaccine where capId = '" + itemid + "' OR gsOneVialId = '" + itemid + "'";
            }
            DataTable dtTmp;
            db.OpenQuery(out dtTmp, sql);
            if (dtTmp.Rows.Count > 0)
            {
                return Convert.ToInt32(dtTmp.Rows[0][0]);
            }
            else
                return 0;
        }

        /**
         * Update scanned data.
         */
        //DataTable tableScanned;
        public void CheckScannedData(string DataCapture)
        {
            if (isStart)
            {
                DataCapture = ms.DeleteSpecialChar(DataCapture);
                DataRow[] foundRow;
                string[] datapecah = DataCapture.Split(',');
                if (datapecah[0] != "0")
                {                    
                    for (int i = 0; i < datapecah.Length; i++)
                    {
                        ms.cekProduct(datapecah[i]);
                        if (ms.uom == ms.UOM_BASKET)
                        {
                            foundRow = ms.dtDetailDO.Select("itemid = '" + datapecah[i] + "'");                            
                            if (foundRow.Length > 0)
                            {
                                if (foundRow[0]["state"] == "1")
                                {
                                    ms.log("Full scanned.");
                                    ms.InsertSystemLog("Full scanned");
                                    Confirm cf = new Confirm("Full scanned.");
                                    return;
                                }
                                else
                                {
                                    if (CekBoxExist(datapecah[i], ms.uom, Convert.ToInt32(foundRow[0]["qty"])))
                                    {
                                        ms.log(datapecah[i] + " already have Box Polyurethane");
                                        Confirm cf = new Confirm(datapecah[i] + " already have Box Polyurethane");
                                        return;
                                    } 

                                    if (firstScan)
                                    {
                                        firstScan = false;
                                        if (Convert.ToBoolean(foundRow[0]["isSas"]))
                                            isSas = true;
                                        else
                                            isSas = false;
                                    }
                                    else
                                    {
                                        if (isSas != Convert.ToBoolean(foundRow[0]["isSas"]))
                                        {
                                            ms.log("Different type from previous scan. About Sas or Non Sas");
                                            Confirm cf = new Confirm("Different type from previous scan. About Sas or Non Sas");
                                            return;
                                        }
                                    }                                                               

                                    if (CekScanned(datapecah[i], "", ms.uom, false))
                                    {
                                        return;
                                    }

                                    //string tmp2;
                                    //if (CekParentScanned(out tmp2, datapecah[i], ms.UOM_BASKET))
                                    //{
                                    //    ms.log(datapecah[i] + " already scanned on " + tmp2);
                                    //    Confirm cf = new Confirm(datapecah[i] + " already scanned on " + tmp2);
                                    //    return;
                                    //}

                                    int b4Scanned;
                                    int Scanned;
                                    b4Scanned = Convert.ToInt32(foundRow[0]["qty_scanned"]);
                                    Scanned = Convert.ToInt32(foundRow[0]["qty"]);
                                    lblQtyScanned.Text = Convert.ToString(Convert.ToInt32(lblQtyScanned.Text) + (Scanned - b4Scanned));
                                    if (int.Parse(lblQtyScanned.Text) > int.Parse(StatusPrint[0][1].ToString()))
                                    {
                                        Confirm cf = new Confirm("Qty Scanned More than Max Qty Masterbox.");
                                        lblQtyScanned.Text = Convert.ToString(Convert.ToInt32(lblQtyScanned.Text) - (Scanned - b4Scanned));
                                        return;
                                    }

                                    foundRow[0].SetField("qty_scanned", Scanned);
                                    foundRow[0].SetField("state", "1");
                                    dataGridView1.Refresh();

                                    Dictionary<string, string> field = new Dictionary<string, string>();
                                    field.Add("itemId", datapecah[i]);
                                    field.Add("uom", foundRow[0]["uom"].ToString());
                                    field.Add("timestamp", db.getUnixString());
                                    //field.Add("vial_qty", foundRow[0]["qty"].ToString());
                                    field.Add("vial_qty", (Convert.ToString(Scanned - b4Scanned)));
                                    field.Add("batchnumber", foundRow[0]["batchNumber"].ToString());
                                    field.Add("expdate", foundRow[0]["expired"].ToString());
                                    field.Add("shippingOrderDetail", foundRow[0]["detailNo"].ToString());
                                    sqlite.insertData(field, "tmp_captured");

                                    ms.log("Scanned item = " + datapecah[i]);
                                }
                            }
                            else
                            {
                                ms.log("Item '" + datapecah[i] + "' is not in registered in the picking list.");
                                Confirm cf = new Confirm("Item '" + datapecah[i] + "' is not in registered in the picking list.");
                                return;
                            }
                        }
                        else if (ms.uom == ms.UOM_INNER_BOX)
                        {
                            string uomParent = "";
                            foundRow = ms.dtDetailDO.Select("itemid = '" + datapecah[i] + "'");
                            if (foundRow.Length == 0)
                            {
                                foundRow = ms.dtDetailDO.Select("itemid = '" + ms.tmpBasket + "'");
                                uomParent = ms.UOM_BASKET;
                            }
                            if (foundRow.Length > 0)
                                if (foundRow[0]["state"] == "1")
                                {
                                    ms.log("Full scanned.");
                                    Confirm cf = new Confirm("Full scanned.");
                                    return;
                                }
                                else
                                {
                                    int qtyCompare;
                                    if (uomParent == ms.UOM_BASKET)
                                        qtyCompare = getQtyVialBiasa(datapecah[i], ms.uom);
                                    else
                                        qtyCompare = Convert.ToInt32(foundRow[0]["qty"]);
                                    if (CekBoxExist(datapecah[i], ms.uom, qtyCompare))
                                    {
                                        ms.log(datapecah[i] + " already have Box Polyurethane");
                                        Confirm cf = new Confirm(datapecah[i] + " already have Box Polyurethane");
                                        return;
                                    }

                                    if (firstScan)
                                    {
                                        firstScan = false;
                                        if (Convert.ToBoolean(foundRow[0]["isSas"]))
                                            isSas = true;
                                        else
                                            isSas = false;
                                    }
                                    else
                                    {
                                        if (isSas != Convert.ToBoolean(foundRow[0]["isSas"]))
                                        {
                                            ms.log("Different type from previous scan. About Sas or Non Sas");
                                            Confirm cf = new Confirm("Different type from previous scan. About Sas or Non Sas");
                                            return;
                                        }
                                    }                                    

                                    if (CekScanned(datapecah[i], "", ms.uom, false))
                                    {
                                        return;
                                    }

                                    string tmp2;
                                    string batchN = Convert.ToString(foundRow[0]["batchNumber"]);
                                    bool notParent = (datapecah[i] == Convert.ToString(foundRow[0]["itemid"]));
                                    if (!notParent)
                                    {
                                        if (CekParentScanned(out tmp2, datapecah[i], ms.UOM_INNER_BOX, batchN, uomParent))
                                        {
                                            ms.log(datapecah[i] + " already scanned on " + tmp2);
                                            Confirm cf = new Confirm(datapecah[i] + " already scanned on " + tmp2);
                                            return;
                                        }
                                    }

                                    int qtyvialTmp;
                                    if (Convert.ToBoolean(foundRow[0]["isSas"]))
                                    {
                                        //qtyvialTmp = Convert.ToInt32(foundRow[0]["outbox_qty"]);
                                        qtyvialTmp = Convert.ToInt32(foundRow[0]["outbox_qty"]) - ms.getQtySubstrac(datapecah[i], batchN);
                                    }
                                    else
                                    {
                                        if (notParent)
                                            qtyvialTmp = ms.getQtyVial(ms.UOM_INNER_BOX, datapecah[i], datapecah[i], batchN, notParent);
                                        else
                                            qtyvialTmp = ms.getQtyVial(ms.UOM_INNER_BOX, datapecah[i], ms.tmpBasket, batchN, notParent);
                                    }
                                    lblQtyScanned.Text = (Convert.ToInt32(lblQtyScanned.Text) + qtyvialTmp).ToString();
                                    if (int.Parse(lblQtyScanned.Text) > int.Parse(StatusPrint[0][1].ToString()))
                                    {
                                        Confirm cf = new Confirm("Qty Scanned More than Max Qty Masterbox.");
                                        lblQtyScanned.Text = (Convert.ToInt32(lblQtyScanned.Text) - qtyvialTmp).ToString();
                                        return;
                                    }

                                    foundRow[0].SetField("qty_scanned", Convert.ToInt32(foundRow[0]["qty_scanned"]) + qtyvialTmp);
                                    if (foundRow[0]["qty"].ToString() == foundRow[0]["qty_scanned"].ToString())
                                    {
                                        foundRow[0].SetField("state", "1");                                        
                                    }

                                    dataGridView1.Refresh();

                                    Dictionary<string, string> field = new Dictionary<string, string>();
                                    field.Add("itemId", datapecah[i]);
                                    field.Add("uom", ms.UOM_INNER_BOX);
                                    field.Add("timestamp", db.getUnixString());
                                    field.Add("vial_qty", qtyvialTmp.ToString());
                                    field.Add("batchnumber", foundRow[0]["batchNumber"].ToString());
                                    field.Add("expdate", foundRow[0]["expired"].ToString());
                                    field.Add("shippingOrderDetail", foundRow[0]["detailNo"].ToString());
                                    sqlite.insertData(field, "tmp_captured");

                                    ms.log("Scanned item = " + datapecah[i]);
                                }
                            else
                            {
                                ms.log("Item '" + datapecah[i] + "' is not in registered in the picking list.");
                                Confirm cf = new Confirm("Item '" + datapecah[i] + "' is not in registered in the picking list.");
                                return;
                            }
                        }
                        else if (ms.uom == ms.UOM_VIAL)
                        {
                            string uomParent = "";
                            foundRow = ms.dtDetailDO.Select("itemid = '" + datapecah[i] + "'");
                            if (foundRow.Length == 0)
                            {
                                foundRow = ms.dtDetailDO.Select("itemid = '" + ms.tmpInnerboxGsOneId + "'");
                                if (foundRow.Length == 0)
                                {
                                    foundRow = ms.dtDetailDO.Select("itemid = '" + ms.tmpInnerboxId + "'");
                                    if (foundRow.Length == 0)
                                    {
                                        foundRow = ms.dtDetailDO.Select("itemid = '" + ms.tmpBasket + "'");
                                        uomParent = ms.UOM_BASKET;
                                    }
                                    else
                                        uomParent = ms.UOM_INNER_BOX;
                                }
                                else
                                    uomParent = ms.UOM_INNER_BOX;
                            }
                            if (foundRow.Length > 0)
                                if (foundRow[0]["state"] == "1")
                                {
                                    ms.log("Full scanned.");
                                    Confirm cf = new Confirm("Full scanned.");
                                    return;
                                }
                                else
                                {
                                    if (CekBoxExist(datapecah[i], ms.uom, 1))
                                    {
                                        ms.log(datapecah[i] + " already have Box Polyurethane");
                                        Confirm cf = new Confirm(datapecah[i] + " already have Box Polyurethane");
                                        return;
                                    }

                                    if (firstScan)
                                    {
                                        firstScan = false;
                                        if (Convert.ToBoolean(foundRow[0]["isSas"]))
                                            isSas = true;
                                        else
                                            isSas = false;
                                    }
                                    else
                                    {
                                        if (isSas != Convert.ToBoolean(foundRow[0]["isSas"]))
                                        {
                                            ms.log("Different type from previous scan. About Sas or Non Sas");
                                            Confirm cf = new Confirm("Different type from previous scan. About Sas or Non Sas");
                                            return;
                                        }
                                    }
                                                                        
                                    if (CekScanned(datapecah[i], "", ms.uom, false))
                                    {
                                        return;
                                    }

                                    string tmp2;
                                    string batchN = Convert.ToString(foundRow[0]["batchNumber"]);
                                    bool notParent = (datapecah[i] == Convert.ToString(foundRow[0]["itemid"]));
                                    if (!notParent)
                                    {
                                        if (CekParentScanned(out tmp2, datapecah[i], ms.UOM_VIAL, batchN, uomParent))
                                        {
                                            ms.log(datapecah[i] + " already scanned on " + tmp2);
                                            Confirm cf = new Confirm(datapecah[i] + " already scanned on " + tmp2);
                                            return;
                                        }
                                    }

                                    int qtyvialTmp = 1;
                                    lblQtyScanned.Text = (Convert.ToInt32(lblQtyScanned.Text) + qtyvialTmp).ToString(); //DI KOMEN, KARNA DI GANTI DENGAN VIAL
                                    if (int.Parse(lblQtyScanned.Text) > int.Parse(StatusPrint[0][1].ToString()))
                                    {
                                        Confirm cf = new Confirm("Qty Scanned More than Max Qty Masterbox.");
                                        lblQtyScanned.Text = (Convert.ToInt32(lblQtyScanned.Text) - qtyvialTmp).ToString();
                                        return;
                                    }
                                    foundRow[0].SetField("qty_scanned", Convert.ToInt32(foundRow[0]["qty_scanned"]) + qtyvialTmp);
                                    if (foundRow[0]["qty"].ToString() == foundRow[0]["qty_scanned"].ToString())
                                    {
                                        foundRow[0].SetField("state", "1");                                        
                                    }

                                    dataGridView1.Refresh();

                                    Dictionary<string, string> field = new Dictionary<string, string>();
                                    field.Add("itemId", datapecah[i]);
                                    field.Add("uom", ms.UOM_VIAL);
                                    field.Add("timestamp", db.getUnixString());
                                    field.Add("vial_qty", qtyvialTmp.ToString());
                                    field.Add("batchnumber", foundRow[0]["batchNumber"].ToString());
                                    field.Add("expdate", foundRow[0]["expired"].ToString());
                                    field.Add("shippingOrderDetail", foundRow[0]["detailNo"].ToString());
                                    sqlite.insertData(field, "tmp_captured");

                                    ms.log("Scanned item = " + datapecah[i]);
                                }
                            else
                            {
                                ms.log("Item '" + datapecah[i] + "' is not in registered in the picking list.");
                                Confirm cf = new Confirm("Item '" + datapecah[i] + "' is not in registered in the picking list.");
                                return;
                            }
                        }
                    }

                    // Add sqlite here
                    if (sqlite.dataInsert.Count > 0)
                    {
                        sqlite.Begin();
                    }
                    //buat auto print
                        //if (int.Parse(lblQtyScanned.Text) == int.Parse(StatusPrint[0][1].ToString()))
                    if (int.Parse(lblQtyScanned.Text) == int.Parse(lblQtyMasterbox.Text))
                    {
                        if (StatusPrint[0][0] == "True")
                        {
                            Print();
                        }
                    }                                    

                    if (dataGridView1.Rows.Count <= 0)
                    {
                        ResetInfo();
                    }
                }
            }
            else ms.log("Delivery order not start");                 
        }

        private void ResetInfo()
        {
            BatchNumber = "";
            GTIN = "";
            ExpiryDate = "";
            masterBoxId = "";
            ProductModelId = "";
            ProductModelName = "";
            CustomerAddress = "";
            CustomerName = "";
            if (InvokeRequired)
            {
            }
            else
            {
            }
        }

        private void updatepickinglist(string barcode)
        {
            Dictionary<string, string> field = new Dictionary<string, string>();
            field.Add("status", "2");
            string where = "itemid = '" + barcode + "'";
            db.update(field, "pickinglist", where);
        }

        private void updateallpickinglist(string barcode)
        {
            Dictionary<string, string> field = new Dictionary<string, string>();
            field.Add("status", "2");
            string where = "itemid in (" + barcode + ")";
            db.update(field, "pickinglist", where);
        }

        private string getProductName(string itemid)
        {
            List<string> fieldPickingList = new List<string>();
            fieldPickingList.Add("d.model_name");
            string from = "pickingList a INNER JOIN shippingorder_detail b ON a.detailNo = b.shippingOrderDetailNo " +
                "INNER JOIN shippingOrder c ON c.shippingOrderNumber = b.shippingOrderNumber " +
                "INNER JOIN product_model d ON d.product_model = b.productModelId ";
            string where = "a.itemid = '" + itemid + "'";
            List<string[]> resultList = db.selectList(fieldPickingList, from, where);

            if (resultList.Count > 0)
            {
                return resultList[0][0];
            }
            else
            {
                return "";
            }
        }

        //private string getPickingList(string itemid, string detailno)
        //{
        //    List<string> fieldPickingList = new List<string>();
        //    fieldPickingList.Add("pickinglist");
        //    string from = "pickingList";
        //    string where = "itemid = '" + itemid + "' and detailNo = '"+detailno+"'";
        //    List<string[]> resultList = db.selectList(fieldPickingList, from, where);

        //    if (resultList.Count > 0)
        //    {
        //        return resultList[0][0];
        //    }
        //    else
        //    {
        //        return "";
        //    }
        //}
        private string getPickingListfromvaccine(string itemid, string uom)
        {
            List<string> fieldPickingList = new List<string>();
            fieldPickingList.Add("pickinglist");
            string from = "vaccine";
            string field = "";
            if (uom == "Vial") {
                field = "GsoneVialId";
            }
            else if(uom == "Innerbox"){
                field = "innerBoxGsOneId";
            }
            else if (uom == "Basket")
            {
                field = "basketId";
            }

            string where = field+" = '" + itemid + "' group by pickinglist";
            List<string[]> resultList = db.selectList(fieldPickingList, from, where);

            if (resultList.Count > 0)
            {
                return resultList[0][0];
            }
            else
            {
                return "";
            }
        }

        private void SaveData()
        {
            List<string> field = new List<string>();
            List<string> UpdateAgregasi = new List<string>();

            int total = 0;
            //string productName = "";
            masterBoxId = txtBasket.Text.Replace("\n", "") ;

            //ambil data sqlite
            field.Add("itemid");
            field.Add("uom");
            field.Add("vial_qty");
            field.Add("expdate");
            field.Add("batchnumber");
            field.Add("shippingOrderDetail");
            List<string[]> ds = sqlite.select(field, "tmp_captured", "");
            if (sqlite.num_rows > 0)
            {
                if (masterBoxId.Length > 0)
                {
                    progressBar1.Visible = true;
                    string capid = "";
                    string blisterid = "";
                    string innerboxid = "";
                    string basketid = "";
                    string BN = "";
                    string PL = "";
                    string PL2 = "";
                    int i = 0;
                    Dictionary<string, string> fieldUpdate = new Dictionary<string, string>();
                    fieldUpdate.Add("masterBoxId", masterBoxId);

                    bool sudahInput = false;
                    foreach (string[] da in ds)
                    {
                        i++;
                        progressBar1.PerformStep();
                        total += Int32.Parse(da[2]);
                        //updatepickinglist(da[0]); //di update sesuai UOM nya
                        if (da[1].Equals("Vial"))
                        {
                            //string pickingList = getPickingList(da[0], da[5]);
                            string pickingList = getPickingListfromvaccine(da[0],da[1]);
                            //string where = " where (capId ='" + da[0] + "' OR gsOneVialId='" + da[0] + "') " +
                                //"and batchNumber = '" + da[4] + "' AND masterBoxId IS NULL and pickingList = '" + pickingList + "' ";
                            //db.update(fieldUpdate, "vaccine", where); tutup dulu buat ujicoba
                            //UpdateAgregasi.Add("Update Vaccine set masterboxid = '" + masterBoxId +"'"+ where);
                            if (da[0] != "") { 
                                if (capid != "" ){
                                    capid = capid + ", '" + da[0] + "'";
                                }
                                else{
                                    capid = "'"+ da[0] + "'";
                                }
                            }                            
                            if (da[4] != "") { 
                                if (BN != ""){
                                    BN = BN + ", '" + da[4] + "'"; 
                                }
                                else{
                                    BN = "'" + da[4] + "'"; 
                                }
                            }
                            if (pickingList != "") { 
                                if (PL != ""){
                                    PL = PL + ", '" + pickingList + "'"; 
                                }
                                else{
                                    PL = "'" + pickingList + "'"; 
                                }
                            }                            
                            //CEK APAKAH MASTERBOXID MASIH ADA YG NULL
                            //if (ms.cekQtyPickListFull(da[0], ms.UOM_VIAL))
                            //{
                            //    //JIKA BASKET TIDAK VALID, MASIH ADA UPDATE BY Innerbox
                            //    //JIKA BASKET VALID, Innerbox tetep di jalankan
                            //    updatepickinglist(ms.getBasketId(da[0], ms.UOM_VIAL));
                            //    updatepickinglist(ms.getInnerboxId(da[0], ms.UOM_VIAL));
                            //}
                            
                            //JIKA TANPA BASKET AKAN VALID DISINI
                            //updatepickinglist(da[0]);
                            if (da[0] != "") { 
                                if (PL2 != ""){
                                    PL2 = PL2 + ", '" + da[0] + "'"; 
                                }
                                else{
                                    PL2 = "'" + da[0] + "'"; 
                                }
                            }                            


                            db.Movement("", da[0], masterBoxId, "1", da[0], "Vial");
                            ms.log("Aggregate Vaccine ID '" + da[0] + "' to Master Box ID '" + masterBoxId + "'");
                        }
                        else if (da[1].Equals("Blister"))
                        {
                            //string where = " where blisterpackId ='" + da[0] + "' and batchNumber = '" + da[4] + "' AND masterBoxId IS NULL and pickingList = '" + pickingList + "' ";
                            //db.update(fieldUpdate, "vaccine", where);
                            //UpdateAgregasi.Add("Update Vaccine set masterboxid = '" + masterBoxId +"'"+ where);
                            string pickingList = getPickingListfromvaccine(da[0], da[1]);
                            if (da[0] != "")
                            { 
                                if (blisterid != "" ){
                                    blisterid = blisterid + ", '" + da[0] + "'";
                                }
                                else{
                                    blisterid = "'"+ da[0] + "'";
                                }
                            }                            
                            if (da[4] != "") { 
                                if (BN != ""){
                                    BN = BN + ", '" + da[4] + "'"; 
                                }
                                else{
                                    BN = "'" + da[4] + "'"; 
                                }
                            }
                            if (pickingList != "") { 
                                if (PL != ""){
                                    PL = PL + ", '" + pickingList + "'"; 
                                }
                                else{
                                    PL = "'" + pickingList + "'"; 
                                }
                            }                            
                            //CEK APAKAH MASTERBOXID MASIH ADA YG NULL
                            //if (ms.cekQtyPickListFull(da[0], "Blister"))
                            //{
                            //    updatepickinglist(ms.getBasketId(da[0], "Blister"));
                            //}

                            //JIKA TANPA BASKET AKAN VALID DISINI
                            //updatepickinglist(da[0]);
                            if (da[0] != "")
                            {
                                if (PL2 != "")
                                {
                                    PL2 = PL2 + ", '" + da[0] + "'";
                                }
                                else
                                {
                                    PL2 = "'" + da[0] + "'";
                                }
                            }                            

                            db.Movement("", da[0], masterBoxId, "1", da[0], "Blister");
                            ms.log("Aggregate Blister Pack ID '" + da[0] + "' to Master Box ID '" + masterBoxId + "'");
                        }
                        else if (da[1].Equals("Innerbox"))
                        {
                            string pickingList = getPickingListfromvaccine(da[0], da[1]);
                            //string where = " where (innerBoxId ='" + da[0] + "' or innerBoxGsOneId='" + da[0] + "') " +
                                //"and batchNumber = '" + da[4] + "' AND masterBoxId IS NULL and pickingList = '" + pickingList + "' ";
                            //db.update(fieldUpdate, "vaccine", where);
                            //UpdateAgregasi.Add("Update Vaccine set masterboxid = '" + masterBoxId +"'"+ where);
                            if (da[0] != "") { 
                                if (innerboxid != "" ){
                                    innerboxid = innerboxid + ", '" + da[0] + "'";
                                }
                                else{
                                    innerboxid = "'"+ da[0] + "'";
                                }
                            }                            
                            if (da[4] != "") { 
                                if (BN != ""){
                                    BN = BN + ", '" + da[4] + "'"; 
                                }
                                else{
                                    BN = "'" + da[4] + "'"; 
                                }
                            }
                            if (pickingList != "") { 
                                if (PL != ""){
                                    PL = PL + ", '" + pickingList + "'"; 
                                }
                                else{
                                    PL = "'" + pickingList + "'"; 
                                }
                            }                            
                            //CEK APAKAH MASTERBOXID MASIH ADA YG NULL
                            //if (ms.cekQtyPickListFull(da[0], ms.UOM_INNER_BOX))
                            //{
                            //    updatepickinglist(ms.getBasketId(da[0], ms.UOM_INNER_BOX));
                            //}

                            //JIKA TANPA BASKET AKAN VALID DISINI
                            //updatepickinglist(da[0]);
                            if (da[0] != "")
                            {
                                if (PL2 != "")
                                {
                                    PL2 = PL2 + ", '" + da[0] + "'";
                                }
                                else
                                {
                                    PL2 = "'" + da[0] + "'";
                                }
                            }                            

                            db.Movement("", da[0], masterBoxId, "1", da[0], "Inner Box");
                            ms.log("Aggregate Inner Box ID '" + da[0] + "' to Master Box ID '" + masterBoxId + "'");
                        }
                        else if (da[1].Equals("Basket"))
                        {
                            string pickingList = getPickingListfromvaccine(da[0], da[1]);
                            //updatepickinglist(da[0]);
                            if (da[0] != "")
                            {
                                if (PL2 != "")
                                {
                                    PL2 = PL2 + ", '" + da[0] + "'";
                                }
                                else
                                {
                                    PL2 = "'" + da[0] + "'";
                                }
                            }                            

                            //string where = " where basketId ='" + da[0] + "' and batchNumber = '" + da[4] + "' AND masterBoxId IS NULL and pickingList = '" + pickingList + "' ";
                            //db.update(fieldUpdate, "vaccine", where);
                            //UpdateAgregasi.Add("Update Vaccine set masterboxid = '" + masterBoxId +"'"+ where);
                            if (da[0] != "") { 
                                if (basketid != "" ){
                                    basketid = basketid + ", '" + da[0] + "'";
                                }
                                else{
                                    basketid = "'"+ da[0] + "'";
                                }
                            }                            
                            if (da[4] != "") { 
                                if (BN != ""){
                                    BN = BN + ", '" + da[4] + "'"; 
                                }
                                else{
                                    BN = "'" + da[4] + "'"; 
                                }
                            }
                            if (pickingList != "") { 
                                if (PL != ""){
                                    PL = PL + ", '" + pickingList + "'"; 
                                }
                                else{
                                    PL = "'" + pickingList + "'"; 
                                }
                            }
                            db.Movement("", da[0], masterBoxId, "1", da[0], "basket");
                            ms.log("Aggregate Basket ID '" + da[0] + "' to Master Box ID '" + masterBoxId + "'");
                        }
                    }
                    //BN.Remove(1,1);
                    //PL.Remove(1, 1);
                    if (capid != "") {
                        //capid.Remove(1,1);

                        string where = " (capId in (" + capid + ") OR gsOneVialId in (" + capid + ")) " +
                            "and batchNumber in (" + BN + ") AND masterBoxId IS NULL and pickingList in (" + PL + ") ";
                        db.update(fieldUpdate, "vaccine", where);

                    }
                    if (blisterid != "")
                    {
                        //blisterid.Remove(1, 1);
                        string where = " blisterpackId in (" + blisterid + ") and batchNumber in (" + BN + ") AND masterBoxId IS NULL and pickingList in (" + PL + ") ";
                        db.update(fieldUpdate, "vaccine", where);

                    }
                    if (innerboxid != "")
                    {
                        //innerboxid.Remove(1, 1);
                        string where = " (innerBoxId in (" + innerboxid + ") or innerBoxGsOneId in (" + innerboxid + ")) " +
                            "and batchNumber in (" + BN + ") AND masterBoxId IS NULL and pickingList in (" + PL + ") ";
                        db.update(fieldUpdate, "vaccine", where);
                    }
                    if (basketid != "")
                    {
                        //basketid.Remove(1, 1);
                        string where = " basketId in (" + basketid + ") and batchNumber in (" + BN + ") AND masterBoxId IS NULL and pickingList in (" + PL + ") ";
                        db.update(fieldUpdate, "vaccine", where);
                    }
                    if (PL2 != "") {
                        updateallpickinglist(PL2);
                    }

                    //db.UpdateAgregasiAll(UpdateAgregasi);

                    //qty total masterbox
                    //List<string> field2 = new List<string>();
                    //field2.Add("capid");
                    //string where2 = "masterboxid = '" + masterBoxId + "'";
                    //List<string[]> ds2 = db.selectList(field2, "vaccine", where2);

                    //qty total masterbox
                    // ini ditutup aja, gak perlu lagi ngambil qty dan bandingin sas non sas, karena sekarang ambil qty nya dari sqlite semua
                    //List<string> field2 = new List<string>();
                    //field2.Add("case when p.isSas = 0 then count_big(capid) else (count_big(capid) * p.outbox_qty) end as qty");
                    //string where2 = "v.masterboxid = '" + masterBoxId + "' and v.productModelId = p.product_model and v.batchNumber = '" + ds[0][4] + "'" +
                    //    " group by p.issas, p.outbox_qty";
                    //List<string[]> ds2 = db.selectList(field2, "vaccine v, product_model p", where2);

                    //isi masterbox detail
                    List<string> fieldGetDet = new List<string>();
                    fieldGetDet.Add("batchnumber");
                    fieldGetDet.Add("shippingOrderDetail");
                    fieldGetDet.Add("expdate");
                    fieldGetDet.Add("sum(vial_qty)");
                    List<string[]> getDet = sqlite.select(fieldGetDet, "tmp_captured group by batchnumber, shippingOrderDetail, expdate", "");

                    for (int x = 0; x < getDet.Count; x++)
                    {

                        Dictionary<string, string> fieldDetailMasterbox = new Dictionary<string, string>();
                        fieldDetailMasterbox.Add("masterBoxid", masterBoxId);
                        //fieldDetailMasterbox.Add("batchnumber", ds[0][4]);
                        fieldDetailMasterbox.Add("batchnumber", getDet[x][0]);
                        //fieldDetailMasterbox.Add("expdate", ds[0][3]);
                        fieldDetailMasterbox.Add("expdate", getDet[x][2]);
                        //fieldDetailMasterbox.Add("qty", "" + ds2[0][0]);
                        fieldDetailMasterbox.Add("qty", "" + getDet[x][3]);
                        //fieldDetailMasterbox.Add("shippingOrderDetailNo", ds[0][5]);
                        fieldDetailMasterbox.Add("shippingOrderDetailNo", getDet[x][1]);
                        db.insert(fieldDetailMasterbox, "masterBox_detail");
                    }

                    //dapetin colie number
                    //List<string> fieldSelectColie = new List<string>();
                    //fieldSelectColie.Add("colieseq");
                    string whereColie = "shippingOrderNumber = '" + PickingListNumber + "' And shippingType = '2'";
                    //List<string[]> resultColie = db.selectList(fieldSelectColie, "shippingOrder", whereColie);
                    //Console.WriteLine("RESULT " + resultColie[0][0].ToString());
                    //colie = int.Parse(resultColie[0][0].ToString()) + 1;
                    //colieNumber = colie.ToString();

                    Dictionary<string, string> fieldUpdateColie = new Dictionary<string, string>();
                    fieldUpdateColie.Add("colieseq", "" + colie);
                    db.update(fieldUpdateColie, "shippingOrder", whereColie);

                    //Disini belum ada data full masterbox
                    Dictionary<string, string> fieldStatus = new Dictionary<string, string>();
                    fieldStatus.Add("rejectTime = NULL, isReject", "0");
                    fieldStatus.Add("qty", total.ToString());
                    fieldStatus.Add("colieNumber", colie.ToString());
                    string where1 = "masterBoxid ='" + masterBoxId + "'";
                    db.update(fieldStatus, "masterBox", where1);

                    for (int x = 0; x <= dataGridView1.Rows.Count - 1; x++)
                    {
                        if (dataGridView1.Rows[x].Cells["qty"].Value.ToString() == dataGridView1.Rows[x].Cells["qty_scanned"].Value.ToString())
                        {
                            updatepickinglist(dataGridView1.Rows[x].Cells["itemId"].Value.ToString());
                        }
                    }

                    if (colie.ToString() == "1" )
                    {
                        Dictionary<string, string> fieldPS = new Dictionary<string, string>();
                        fieldPS.Add("status", "3");
                        db.update(fieldPS, "packing_slip", " PackingSlipId = '" + txtDO.Text + "'");
                    }

                        //for (int k = 0; k <= datainner.Rows.Count - 1; k++)
                        //{
                        //    DataRow row = dataHistory.NewRow();
                        //    row[0] = datainner.Rows[k]["itemid"].ToString(); //datainner.Rows[k]("itemid").Tostring();
                        //    row[1] = txtBasket.Text;
                        //    row[2] = colieNumber.ToString();
                        //    row[3] = datainner.Rows[k]["productName"].ToString();
                        //    dataHistory.Rows.Add(row);
                        //}
                    progressBar1.Visible = false;
                    sqlite.delete("", "tmp_captured");
                    ResetInfo();
                    ms.log("Updating Master Box ID '" + masterBoxId + "'");
                }
            }
        }

        /**
         * Tampil data 
         */
        //private void ShowData()
        //{
        //    foreach (string key in databasket.Keys)
        //    {
        //        string[] value12 = databasket[key];
        //        for (int i = 0; i < value12.Length; i++)
        //        {
        //            dataGridView2.Rows.Add(value12[i], key);
        //        }
        //    }
        //    labelQtyHistory.Invoke(new Action(() => { labelQtyHistory.Text = databasket.Count().ToString(); }));
        //    labelQtyHistory.Text = databasket.Count.ToString();
        //}

        /**
         * Clear all scanned data.
         */
        private void ClearAllScanned()
        {
            // Clear DB
            //sqlite.delete("", "tmp_captured");
            //tableScanned = new DataTable();
            //tableScanned.Columns.Add("itemid");
            //tableScanned.Columns.Add("uom");
            //tableScanned.Columns.Add("qty");
            //tableScanned.Columns.Add("timestamp");
            //tableScanned.Columns.Add("model_name");
            lblQtyScanned.Text = "0";
            //dataGridView1.AutoGenerateColumns = false;
            //dataGridView1.DataSource = tableScanned;
        }

        /**
         * Clear a single selected data.
         */
        private void ClearSelectedScanned(String InnerBoxId)
        {
            // Clear DB
            //sqlite.delete("InnerBoxId = '" + InnerBoxId + "'", "tmp_captured");
            // Clear Gridview
            dataGridView1.Rows.RemoveAt(dataGridView1.SelectedRows[0].Index);
            if (dataGridView1.Rows.Count <= 0)
            {
                ResetInfo();
            }
        }

        /**
         * Generate a new code by checking the last code in the database table.
         */
        private int GenerateNewCode()
        {            
            int seq = db.GetLastSequence(BatchNumber);
            seq = seq + 1;
            db.UpdateLastSequence(BatchNumber, seq);
            return seq;
        }

        /**
         * Print a new generated code.
         */
        private void Print()
        {
            // Generate new code
            ms.setBatch(PickingListNumber);
            int seq = GenerateNewCode();
            //string CurrentSN = seq.ToString().PadLeft(5, '0'); //PadLeft(6, '0')
            string CurrentSN = seq.ToString().PadLeft(3, '0'); //PadLeft(6, '0')
            List<string> fieldSelectColie = new List<string>();
            fieldSelectColie.Add("(colieseq)+1");
            string whereColie = "shippingOrderNumber = '" + PickingListNumber + "' And shippingType = '2'";
            List<string[]> resultColie = db.selectList(fieldSelectColie, "shippingOrder", whereColie);
            Console.WriteLine("RESULT " + resultColie[0][0].ToString());
            colie = int.Parse(resultColie[0][0].ToString());

            CurrentSN = colie.ToString() + sqlite.config["CompId"].ToString(); ;
            int pjg = 5;
            //int PanjangColi = pjg - CurrentSN.Length;
            CurrentSN = CurrentSN.PadRight(pjg, '0');
            //string GSOneSN = Guid.NewGuid().ToString().Replace("-", "").Substring(0, 12).ToUpper();
            List<string> fields = new List<string>();
            fields.Add("itemId");
            List<string[]> results = sqlite.select(fields, "tmp_captured", "1 = 1");

            // Insert to database
            //masterBoxId = cmbDelivery.Text.Replace("-","") + "04" + CurrentSN;
            masterBoxId = txtDO.Text.Replace("-", "") + CurrentSN;
            //gsOneMasterBoxId = "01" + GTIN + "10" + BatchNumber + "17" + ExpiryDate + "21" + GSOneSN; //TIDAK DI PAKE, PROSES NYA ADA DI LABEL APLIKATOR

            //TAMBAH PEMBEDA KETIKA 2  PC GENERETE
            //masterBoxId = masterBoxId + sqlite.config["CompId"].ToString();
            //if (masterBoxId.Length == 14)
            //{
            //    masterBoxId = masterBoxId + "0";
            //}
            //else if (masterBoxId.Length == 13)
            //{
            //    masterBoxId = masterBoxId + "00";            
            //}
            //int panjangamasterbox = 15 - masterBoxId.Length;
            //masterBoxId = masterBoxId.PadRight(panjangamasterbox, '0');

            getInformation();
            //gsOneMasterBoxId = TIDAK DI PAKE
            //GSOneSN = TIDAK DI PAKE
            //db.InsertNewMasterBoxID(masterBoxId, gsOneMasterBoxId, CustomerName, CustomerAddress, GTIN, BatchNumber, ExpiryDate, GSOneSN);
            //db.InsertNewMasterBoxID(masterBoxId, "", CustomerName, CustomerAddress, GTIN, BatchNumber, ExpiryDate, GSOneSN);
            //db.InsertNewMasterBoxID(masterBoxId, "", CustomerName, CustomerAddress, GTIN, BatchNumber, ExpiryDate, "");
            db.InsertNewMasterBoxID(masterBoxId);
            ms.log("Generate new Master Box ID : " + masterBoxId);
            ms.InsertSystemLog("Generate new Master Box ID : " + masterBoxId);
            //GET PRODUCT NAME DARI MASTERBOX ID 
            string nmProduct = ms.getProductMasterBox(PickingListNumber);

            // Send to printer
            try
            {
                List<string> fieldGetDet = new List<string>();
                fieldGetDet.Add("batchnumber");
                fieldGetDet.Add("sum(vial_qty)");
                List<string[]> getDet = sqlite.select(fieldGetDet, "tmp_captured group by batchnumber", "");
                string[] tempp = new string[2];

                int countRes = getDet.Count;
                if (countRes > 5)
                {
                    string[] temp = new string[2];
                    temp[0] = "dst....";
                    temp[1] = "";
                    for (int a = countRes - 1; 4 <= a; a--)
                    {
                        getDet.RemoveAt(a);
                    }
                    getDet.Add(temp);
                }
                else if (countRes == 4)
                {
                    string[] temp = new string[2];
                    temp[0] = "";
                    temp[1] = "";
                    getDet.Add(temp);
                }
                else if (countRes == 3)
                {
                    string[] temp = new string[2];
                    temp[0] = "";
                    temp[1] = "";
                    getDet.Add(temp);
                    getDet.Add(temp);
                }
                else if (countRes == 2)
                {
                    string[] temp = new string[2];
                    temp[0] = "";
                    temp[1] = "";
                    getDet.Add(temp);
                    getDet.Add(temp);
                    getDet.Add(temp);
                }
                else if (countRes == 1)
                {
                    string[] temp = new string[2];
                    temp[0] = "";
                    temp[1] = "";
                    getDet.Add(temp);
                    getDet.Add(temp);
                    getDet.Add(temp);
                    getDet.Add(temp);
                }

                Printer.Print(masterBoxId, getDet, nmProduct, lblQtyScanned.Text);
                ms.log("Send to printer : " + masterBoxId);
                txtBasket.Focus();
            }
            catch (Exception e)
            {
                ms.log("Fail send to printer : " + e.Message);
            }
        }

        private void getInformation()
        {
            //dapetin 
            //customer name, customer address,Gtin,expiry date,qty, colie number
        }

        /**
         * Show a new form for decommission purposes.
         */
        // Controls

        private void buttonScan_Click(object sender, EventArgs e)
        {
            if (dataGridView1.Rows.Count > 0)
            {
                if(txtBasket.Text.Length>0)
                    Match();
                else
                    ms.log("Master Box ID is empty");
            }
            else
                ms.log("No data to be inserted");            
        }

        private void kosong()
        {
            dataGridView1.Rows.Clear(); ;
        }

        private void buttonPrint_Click(object sender, EventArgs e)
        {
            if (dataGridView1.Rows.Count > 0)
                Print();
            else
                ms.log("No data to be inserted");
        }

        private void ManualPacker_FormClosed(object sender, FormClosedEventArgs e)
        {
            close("");
        }

        private void Match()
        {
            SaveData();
            //ShowData();
        }

        private void txtBasket_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Match();
            }
        }

        private void btnConfig_Click(object sender, EventArgs e)
        {
            config cfg = new config();
            cfg.ShowDialog();
        }

        private void btnLogout_Click(object sender, EventArgs e)
        {
            Confirm cf = new Confirm("Are you sure? ", "Confirmation");
            bool result = cf.conf();
            if (result)
            {
                sqlite.delete("", "tmp_captured");
                ms.srv.closeServer("");
                ms.InsertSystemLog("Logout User : "+lblUserId.Text);
                this.Dispose();
                Login lg = new Login();
                lg.Show();
            }
        }

        private void buttonClearAll_Click(object sender, EventArgs e)
        {
            if (dataGridView1.Rows.Count > 0)
            {
                Confirm cf = new Confirm("Are you sure? ", "Confirmation");
                bool hasilDialog = cf.conf();
                if (hasilDialog)
                {
                    ClearAllScanned();
                    ResetInfo();
                }
            }
            else
                ms.log("No data to be inserted");
        }

        private void buttonClearSelected_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0) {
                string InnerBoxId = dataGridView1.SelectedRows[0].Cells[0].Value.ToString();
                Confirm cf = new Confirm("Are you sure? ", "Confirmation");
                bool hasilDialog = cf.conf();
                if (hasilDialog)
                {
                    ClearSelectedScanned(InnerBoxId);
                }
            }
            else
                ms.log("No data to be inserted");
        }

        private void btnDeco_Click(object sender, EventArgs e)
        {

        }

        void close(string ti)
        {
            if (InvokeRequired)
            {
                this.Invoke(new Action<string>(close), new object[] { ti });
                return;
            }
            Application.Exit();
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (txtScanned.Text.Length > 0)
                    CheckScannedData(txtScanned.Text);
                txtScanned.Focus();
                txtScanned.Text = "";
            }
        }

        void simpanpickinglist(string itemid, string qty, string detailno, string uom)
        {
            itemid = itemid.Replace("\n", "");
            Dictionary<string, string> field = new Dictionary<string, string>();
            field.Add("itemid", itemid);
            field.Add("qty", qty);
            field.Add("detailno", detailno);
            field.Add("uom", uom);
            field.Add("status", "0");
            string dates = DateTime.Now.ToString("mmMMyyssddHH");
            field.Add("pickinglist", dates);
            db.insert(field, "pickinglist");
        }

        private void txtBasket_KeyDown_1(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (dataGridView1.Rows.Count > 0)
                {
                    if (Convert.ToInt32(lblQtyScanned.Text) == 0)
                    {
                        ms.log("Qty on progress = 0");
                        Confirm cf = new Confirm("Qty on progress = 0");
                        return;
                    }

                    if (txtBasket.Text.Length > 0)
                    {
                        txtBasket.Text = txtBasket.Text.Replace("\n", "");
                        if (ms.IsAdaMasterBox(txtBasket.Text) == true)
                        {
                            Match();
                        }
                        else {
                            Confirm cf = new Confirm("Masterbox Id not Found");
                            txtBasket.Text = "";
                            return;                            
                        }
                        //tableScanned = new DataTable();
                        //tableScanned.Columns.Add("itemid");
                        //tableScanned.Columns.Add("uom");
                        //tableScanned.Columns.Add("qty");
                        //tableScanned.Columns.Add("timestamp");
                        //tableScanned.Columns.Add("model_name");
                        //dataGridView1.AutoGenerateColumns = false;
                        //dataGridView1.DataSource = tableScanned;
                        //tableScanned.Rows.Clear();

                        //ms.dtDetailDO.Rows.Clear();
                        //dataGridView1.Refresh();

                        ms.GetDetailDO(PickingListNumber);
                        ms.GetDetailBoxPU(PickingListNumber);

                        //lblQtyScanned.Text = "0";
                        //labelQtyHistory.Text = dataGridView2.Rows.Count.ToString();

                        int qtyVial;
                        qtyVial = ms.getQtyMasterBox(txtBasket.Text);
                        simpanpickinglist(txtBasket.Text, qtyVial.ToString(), PickingListNumber, "masterbox");
                        //cekPenuhShipping();//PINDAH KE CONFIRM
                        firstScan = true;
                        isSas = false;
                        txtScanned.Focus();

                        ms.log("Scanned box pu = " + txtBasket.Text);
                    }
                    else
                    {
                        ms.log("Master Box ID is empty");
                        Confirm cf = new Confirm("Master Box ID is empty");
                    }
                }
                else
                {
                    ms.log("No data to be inserted");
                    Confirm cf = new Confirm("No data to be inserted");
                }
                txtBasket.Text = "";
            }
        }

        private string getShippingDetail(string p)
        {
            List<string> field = new List<string>();
            field.Add("shippingOrderDetail");
            List<string[]> ds = sqlite.select(field, "tmp_captured", "itemid = '"+p+"'");
            if (sqlite.num_rows > 0)
            {
                return ds[0][0];
            }
            return "";
        }

        private string getBatchNumber(string p)
        {
            List<string> field = new List<string>();
            field.Add("TOP 1 batchNumber");
            p = p.Replace("\n", "");
            string where = "Vaccine.masterBoxId = '" + p + "'";
            List<string[]> data = db.selectList(field, "vaccine", where);
            return data[0][0];
        }

        private string getShippingDetail(string batchnumber, string shipipingOrderNumber)
        {
            List<string> field = new List<string>();
            field.Add("shippingOrderDetailNo");
            string where = "shippingOrderNumber = '"+shipipingOrderNumber+"' and batchnumber = '"+batchnumber+"'";
            string data = db.selectList(field, "shippingOrder_detail", where)[0][0];
            return data;
        }

        private void cekPenuhShipping()
        {
            List<string> field = new List<string>();
            field.Add("COUNT(*)"); //<- TETEP PAKE COUNT(*), CZ BUKAN KE VACCINE
            string from = "dbo.shippingOrder_detail INNER JOIN dbo.pickingList ON dbo.pickingList.detailNo = dbo.shippingOrder_detail.shippingOrderDetailNo";
            string where = "dbo.shippingOrder_detail.shippingOrderNumber = '" + PickingListNumber + "' AND dbo.pickingList.status = '1'";
            List<string[]> ds = db.selectList(field, from, where);
            //if (db.num_rows > 0)
            if (ds != null && db.num_rows > 0)
                {
                string banyakPickinglist = ds[0][0];
                if(banyakPickinglist.Equals("0"))
                {
                    Dictionary<string,string> fieldAct = new Dictionary<string,string>();
                    fieldAct.Add("flag", "3");
                    where = "shippingOrderNumber='" + PickingListNumber + "'";
                    db.update(fieldAct, "shippingorder_detail", where);                    
                    db.update(fieldAct, "shippingorder", where);

                    where = "pickingListNumber='" + PickingListNumber + "'"; //DO-5FBEA84E77C7C 
                    db.updateDB2(fieldAct, "pickingList_erp", where);

                    where = "pickingList_ERP='" + PickingListNumber + "'"; //DO-5FBEA84E77C7C 
                    db.updateDB2(fieldAct, "pickingList_erp_detail", where);

                    db.Connect();

                    //GA PERLU UPDATE VEHICLE, ETT
                    //Dictionary<string, string> fieldShp = new Dictionary<string, string>();
                    //DataTable dtTmpPS;
                    //string sql;
                    //sql = "SELECT id FROM packing_slip WHERE PackingSlipId = '" + cmbDelivery.Text + "'";
                    //db.OpenQuery(out dtTmpPS, sql);
                    //if (dtTmpPS.Rows.Count > 0)
                    //{
                    //    fieldShp.Add("packing_slip_id", dtTmpPS.Rows[0]["id"].ToString());//txtETA.Text); //GANTI ID !!!!
                    //    fieldShp.Add("ett", txtETA.Text);
                    //    fieldShp.Add("vehicle_id", System.Convert.ToString(vehicleId));
                    //    db.insert(fieldShp, "shipment");

                    //    fieldShp = new Dictionary<string, string>();
                    //    fieldShp.Add("ett", txtETA.Text);
                    //    fieldShp.Add("vehicle_id", System.Convert.ToString(vehicleId));
                    //    db.updateDB2(fieldShp, "packingSlip", "packingSlipId = '" + cmbDelivery.Text + "'"); //cmbDelivery.Text

                    //    db.Connect();
                    //}
                    //else
                    //{
                    //    Confirm cf = new Confirm("Packing slip " + cmbDelivery.Text + " not found.");
                    //}



                    Dictionary<string, string> fieldPS = new Dictionary<string, string>();
                    fieldPS.Add("status", "1");
                    db.update(fieldPS, "packing_slip", " PackingSlipId = '" + txtDO.Text + "' AND status <> 2");

                    sqlite.delete("", "tmp_captured");

                    //DataTable dtTmpDetailNO;
                    //sql = "select shippingOrder_detail.shippingOrderDetailNo FROM "+
                    //    "shippingOrder_detail where shippingOrderNumber = '" + PickingListNumber + "'";
                    //db.OpenQuery(out dtTmpDetailNO, sql);
                    //if (dtTmpDetailNO.Rows.Count > 0)
                    //{
                    //    sqlite.delete("shippingOrderDetail = '" + dtTmpDetailNO.Rows[0][0].ToString() + "'", "tmp_captured"); //BERDASARKAN DetailNO
                    //}
                    //else
                    //{
                    //    Confirm cf = new Confirm("Shipping order detail " + PickingListNumber + " not found.");
                    //}

                    txtDO.Enabled = true;
                    PickingListNumber = "";
                    btnStart.Text = "Start";
                    btnStart.Image = Manual_Packer_Station.Properties.Resources.start;
                    txtETA.Text = "";
                    txtExpedisi.Text = "";
                    txtVehicle.Text = "";
                    //expedisiId = 0;
                    //vehicleId = 0;
                    lblQtyTotal.Text = "0";
                    lblQtyScanned.Text = "0";
                    lblQtyMasterbox.Text = "0";
                    isStart = false;
                    SetFormActive(false);

                    ms.dtDetailDO.Rows.Clear();
                    ms.dtDetailBoxPU.Rows.Clear();
                    dataGridView1.Refresh();
                    dataGridView2.Refresh();

                    ms.startStation();
                }
            }
        }

        bool isStart = false;
        private void btnStart_Click(object sender, EventArgs e)
        {
            if (txtDO.Text != "")
            {               
                if (!isStart)
                {
                    sqlite.delete("", "tmp_captured");
                    SetFormActive(true);
                    txtDO.Enabled = false;
                    btnStart.Text = "Stop";
                    btnStart.Image = Manual_Packer_Station.Properties.Resources.stop;
                    isStart = true;
                    ms.GetDetailDO(PickingListNumber);
                    ms.GetDetailBoxPU(PickingListNumber);
                    SudahPrintPS = false;
                    btnConfirm.Enabled = false;
                    //List<string> fieldSelectColie = new List<string>();
                    //fieldSelectColie.Add("(colieseq)+1");
                    //string whereColie = "shippingOrderNumber = '" + PickingListNumber + "' And shippingType = '2'";
                    //List<string[]> resultColie = db.selectList(fieldSelectColie, "shippingOrder", whereColie);
                    //Console.WriteLine("RESULT " + resultColie[0][0].ToString());
                    //colie = int.Parse(resultColie[0][0].ToString());
                    StatusPrint = db.GetIsAutoPrint(PickingListNumber);
                    lblQtyMasterbox.Text = StatusPrint[0][1];
                    txtScanned.Focus();
                }
                else
                {
                    if (CekSudahScanTapiBelumSemua())
                    {
                        Confirm cf = new Confirm("data is already being scanned, continue to stop ? " + Environment.NewLine +
                            "because when it starts again, the scan process will start from the beginning", "Warning");
                        bool result = cf.conf();
                        if (!result)
                        {
                            //sqlite.delete("", "tmp_captured");
                            return;
                        }
                    }

                    sqlite.delete("", "tmp_captured");
                    SetFormActive(false);
                    txtDO.Enabled = true;
                    btnStart.Text = "Start";
                    btnStart.Image = Manual_Packer_Station.Properties.Resources.start;
                    isStart = false;
                    firstScan = true;
                    isSas = false;
                    ms.startStation();
                    ms.dtDetailDO.Rows.Clear();
                    ms.dtDetailBoxPU.Rows.Clear();
                    dataGridView1.Refresh();
                    dataGridView2.Refresh();

                    //expedisiId = 0;
                    //vehicleId = 0;
                    //vehicleNo = "";

                    lblQtyTotal.Text = "0";
                    lblQtyScanned.Text = "0";
                    labelQtyHistory.Text = "0";
                    lblQtyMasterbox.Text = "0";
                    
                    txtExpedisi.Text = "";
                    txtVehicle.Text = "";
                    txtETA.Text = "";

                    btnConfirm.Enabled = false;

                    //SET FILL GRID
                    for (int i = 0; i < dataGridView1.Columns.Count; i++)
                    {
                        dataGridView1.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                    }
                }                
            }
            else {
                Confirm cf = new Confirm("No delivery order.");
                ms.log("No delivery order");
            }
        }

        private bool CekSudahScanTapiBelumSemua()
        {
            bool ScanAlready = false;
            for (int i = 0; i < ms.dtDetailDO.Rows.Count; i++)
            {
                if (ms.dtDetailDO.Rows[i]["state"].ToString() == "1")
                {
                    ScanAlready = true;
                }
            }
            return ScanAlready;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            grpDetail.Visible = false;
        }

        private void cmbDelivery_SelectionChangeCommitted(object sender, EventArgs e)
        {
            lblDetail.Text = "Costumer Name :";
            lblDetail2.Visible = false;
            txtAddress.Visible = false;
            lblProd.Visible = false;
            lblProductName.Visible = false;
            lblUom.Visible = false;
            grpDetail.Visible = true;
            ms.setDetailCmb(PickingListNumber);
            ms.log("Delivery Order : " + txtDO.Text + " Selected");
            ms.InsertSystemLog("Delivery Order : " + txtDO.Text + " Selected");
        }

        private void buttonPrint_Click_1(object sender, EventArgs e)
        {
            if (dataGridView1.Rows.Count > 0)
            {
                if (Convert.ToInt32(lblQtyScanned.Text) == 0)
                {
                    ms.log("Qty on progress = 0");
                    Confirm cf = new Confirm("Qty on progress = 0");
                    return;
                }
                Print();
            }
            else
                ms.log("No data to be inserted");
        }

        private void buttonClearAll_Click_1(object sender, EventArgs e)
        {
            if (dataGridView1.Rows.Count > 0)
            {
                Confirm cf = new Confirm("Are you sure? ", "Confirmation");
                bool hasilDialog = cf.conf();
                if (hasilDialog)
                {
                    ClearAllScanned();
                    ResetInfo();
                }
            }
            else
                ms.log("No data to be inserted");
        }

        private void buttonClearSelected_Click_1(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                string InnerBoxId = dataGridView1.SelectedRows[0].Cells[0].Value.ToString();
                Confirm cf = new Confirm("Are you sure? ", "Confirmation");
                bool hasilDialog = cf.conf();

                if (hasilDialog)
                {
                    ClearSelectedScanned(InnerBoxId);
                }
            }
            else
                ms.log("No data to be inserted");
        }

        private void txtScanned_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (txtScanned.Text.Length > 0)
                {
                    if (ms.dtDetailDO.Rows.Count > 0)
                        CheckScannedData(txtScanned.Text);
                    else
                    {
                        ms.log("Empty data");
                        Confirm cf = new Confirm("Empty data");
                        txtScanned.Text = "";
                    }

                }

                try
                {
                    object tmpVial;
                    object tmpScanned;
                    tmpVial = ms.dtDetailDO.Compute("SUM(qty)", string.Empty);
                    tmpScanned = ms.dtDetailDO.Compute("SUM(qty_scanned)", string.Empty);
                    lblQtyTotal.Text = Convert.ToString(int.Parse(tmpVial.ToString()) - int.Parse(tmpScanned.ToString()));
                    //lblQtyScanned.Text = tmpScanned.ToString();
                    txtScanned.Text = "";
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                
            }
        }
        int ordersItemIndex;
        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
                ordersItemIndex = dataGridView1.SelectedRows[0].Index;
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            string itemid =dataGridView1.Rows[ordersItemIndex].Cells[0].Value.ToString();
            string uom =dataGridView1.Rows[ordersItemIndex].Cells[1].Value.ToString();
            grpDetail.Visible = true;
            lblProd.Visible = true;
            lblProductName.Visible = true;
            lblUom.Visible = true;
            lblDetail2.Visible = true;
            txtAddress.Visible = true;
            lblDetail.Text = "Batch Number :";
            lblDetail2.Text = "Expiry date :";
            lblUom.Text = "Infeed";
            ms.getDetailVial(itemid, uom);
            ms.setDetailVial(itemid);
        }

        void SetFormActive(bool a)
        {
            buttonClearAll.Enabled = a;
            buttonClearSelected.Enabled = a;
            txtScanned.Enabled = a;
            txtBasket.Enabled = a;
            buttonPrint.Enabled = a;
            btnPrintDO.Enabled = a;
            txtETA.Enabled = a;
            txtExpedisi.Enabled = a;
            txtVehicle.Enabled = a;
            //btnConfirm.Enabled = a;
        }

        int orderitem;
        private void dgvDetail_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            string dataitem = dgvDetail.Rows[orderitem].Cells[0].Value.ToString();
            ms.getDetailVial(dataitem, lblUom.Text);
        }

        private void dgvDetail_SelectionChanged(object sender, EventArgs e)
        {
            if (dgvDetail.SelectedRows.Count > 0)
                orderitem = dgvDetail.SelectedRows[0].Index;
        }

        private void ManualPacker_Load(object sender, EventArgs e)
        {
            timer1.Start();
            lblTime.Text = DateTime.Now.ToLongTimeString();
            lblDate.Text = DateTime.Now.ToShortDateString();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ms.startStation();
        }

        private void dataGridView1_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            //lblQtyScanned.Text = dataGridView1.Rows.Count.ToString();
        }

        private void btnIndicator_Click(object sender, EventArgs e)
        {
            CheckDevice();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            lblTime.Text = DateTime.Now.ToLongTimeString();
            timer1.Start();
        }

        private void btnOpenLog_Click(object sender, EventArgs e)
        {
            Log frm;
            frm = new Log(LogFile);
            frm.ShowDialog();
            if (frm.isClear == true)
                LogFile = "";
        }

        private void btnDeo_Click(object sender, EventArgs e)
        {
            Decomission dc = new Decomission();
            dc.ShowDialog();
            if (PickingListNumber != "")
                ms.GetDetailBoxPU(PickingListNumber);
        }

        private void txtETA_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }
        }

        private void btnPrintDO_Click(object sender, EventArgs e)
        {
            if (dataGridView1.Rows.Count > 0)
            {
                ms.log("There is still an Item Id that doesn't have Box Poliurethane");
                Confirm cf = new Confirm("There is still an Item Id that doesn't have Box Poliurethane");
                return;
            }
            if (dataGridView2.Rows.Count == 0)
            {
                Confirm cf1 = new Confirm("Box Poliuretan is empty");
                ms.log("Box Poliuretan is empty");
                return;
            }

            String custName = "";
            String custAddrs = "";
            String salesOrder = "";
            Int32 intCount = 0;
            IsiAngka frm = new IsiAngka();
            frm.Text = "Count Print";
            frm.ShowDialog();

            if (frm.resultAngka != 0)
            {
                intCount = frm.resultAngka;
            }
            else
            {
                Confirm cf2 = new Confirm("Please fill Count Print.");
                ms.log("Please fill Count Print.");
                return;
            }

            List<string> field = new List<string>();
            //field.Add("shippingOrderNumber");
            //field.Add("customerName");
            //field.Add("customerAddress");
            //field.Add("salesOrder");
            //string where = "shippingOrderNumber = '" + PickingListNumber + "'";
            field.Add("s.shippingOrderNumber");
            field.Add("uc.customer_Name");
            field.Add("uca.Address_detail");
            field.Add("s.salesOrder"); 
            string where = "s.shippingOrderNumber = '" + PickingListNumber + "' and s.customerId = uc.Id and s.addressId = uca.id";
            string from = " shippingOrder s, user_customer uc, user_customer_address uca ";

            List<string[]> ds = db.selectList(field, from, where);
            //if (db.num_rows > 0)
            if (ds != null && db.num_rows > 0)
            {
                List<string> dataCombo = new List<string>();
                foreach (string[] da in ds)
                {
                    custName = da[1];
                    custAddrs = da[2];
                    salesOrder = da[3];
                }
            }
            for (int i = 0; i <= intCount - 1; i++)
            {
                Printer.PrintDO(txtDO.Text, salesOrder, custName, custAddrs);
            }
            ms.log("Send to printer (Print Count = " + intCount + "): " + txtDO.Text);
            ms.InsertSystemLog("Print label PS : " + txtDO.Text);
            SudahPrintPS = true;
            btnConfirm.Enabled = true;
        }

        private void txtExpedisi_Click(object sender, EventArgs e)
        {
        //    ListExpedisi lte;
        //    lte = new ListExpedisi();
        //    lte.ShowDialog();
        //    if (lte.expedisiId != 0)
        //    {
        //        expedisiId = lte.expedisiId;
        //        expedisiName = lte.expedisiName;
        //        txtExpedisi.Text = expedisiName;
        //    }
        //    else
        //    {
        //        expedisiId = 0;
        //        expedisiName = "";
        //        txtExpedisi.Text = "";
        //    }
        }

        private void txtVehicle_Click(object sender, EventArgs e)
        {
        //    if (expedisiId == 0)
        //    {
        //        Confirm cf = new Confirm("Please choose Expedisi first");
        //        return;
        //    }

        //    ListKendaraan ltk;
        //    ltk = new ListKendaraan();
        //    ltk.expedisiId = expedisiId;
        //    ltk.ShowDialog();
        //    if (ltk.vehicleId != 0)
        //    {
        //        vehicleId = ltk.vehicleId;
        //        vehicleNo = ltk.vehicleNo;
        //        txtVehicle.Text = vehicleNo;
        //        txtETA.Text = "48";
        //    }
        //    else
        //    {
        //        vehicleId = 0;
        //        vehicleNo = "";
        //        txtVehicle.Text = "";
        //        txtETA.Text = "";
        //    }
        }

        private void dataGridView1_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (dataGridView1.Rows[e.RowIndex].Cells["state"].Value.ToString() == "0")
            {
                dataGridView1.Rows[e.RowIndex].Cells["state_text"].Value = "Not Scanned";                
            }
            else
            {
                dataGridView1.Rows[e.RowIndex].Cells["state_text"].Value = "Scanned";
                dataGridView1.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.Lime;                
            }
        }

        private void txtScanned_KeyPress(object sender, KeyPressEventArgs e)
        {
            var regex = new Regex(@"[^a-zA-Z0-9\b]");
            if (regex.IsMatch(e.KeyChar.ToString()))
            {
                e.Handled = true;
            }
        }

        private void txtBasket_KeyPress(object sender, KeyPressEventArgs e)
        {
            var regex = new Regex(@"[^a-zA-Z0-9\b]");
            if (regex.IsMatch(e.KeyChar.ToString()))
            {
                e.Handled = true;
            }
        }

        private void btnConfirmLocation_Click(object sender, EventArgs e)
        {
            if (txtDO.Text != "")
            {
                if (dataGridView1.Rows.Count > 0)
                {
                    ms.log("There is still an Item Id that doesn't have Box Poliurethane");
                    Confirm cf = new Confirm("There is still an Item Id that doesn't have Box Poliurethane");
                    return;
                }
                //if (expedisiId == 0)
                //{
                //    ms.log("Expedisi required");
                //    Confirm cf = new Confirm("Expedisi required");
                //    return;
                //}
                //if (txtETA.Text == "")
                //{
                //    ms.log("ETA not filled");
                //    Confirm cf = new Confirm("ETA not filled.");
                //    return;
                //}
                //if (vehicleId == 0)
                //{
                //    ms.log("Vehicle required");
                //    Confirm cf = new Confirm("Vehicle No required");
                //    return;
                //}

                Confirm cf2 = new Confirm("Are you sure to Confirm ?", "Confirmation");
                bool result = cf2.conf();
                if (result)
                {
                    ms.InsertSystemLog("Confirm Finish DO : " + txtDO.Text);
                    cekPenuhShipping();
                    txtScanned.Focus();
                    btnConfirm.Enabled = false;
                    txtDO.Text = "";
                }
            }
            else
            {
                Confirm cf = new Confirm("No delivery order.");
                ms.log("No delivery order");
            }
        }

        private void txtDO_Click(object sender, EventArgs e)
        {
            
            DeliveryOrder frm = new DeliveryOrder();
            frm.ShowDialog();
            txtDO.Text = frm.DeliveryOrderNumber;
            PickingListNumber = frm.PLN;
            lblDetail.Text = "Costumer Name :";
            lblDetail2.Visible = false;
            txtAddress.Visible = false;
            lblProd.Visible = false;
            lblProductName.Visible = false;
            lblUom.Visible = false;
            grpDetail.Visible = true;
            ms.setDetailCmb(PickingListNumber);
            ms.log("Delivery Order : " + txtDO.Text + " Selected");
            ms.InsertSystemLog("Delivery Order : " + txtDO.Text + " Selected");
        }

        private void AutoPrint() {
            if (dataGridView1.Rows.Count > 0)
            {
                if (Convert.ToInt32(lblQtyScanned.Text) == 0)
                {
                    ms.log("Qty on progress = 0");
                    Confirm cf = new Confirm("Qty on progress = 0");
                    return;
                }
                Print();
            }
            else
                ms.log("No data to be inserted");
        }
    }
}
