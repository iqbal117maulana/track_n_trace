﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Drawing.Printing;
using System.IO;
// Tambahan Kenzi as of 2017-07-23
using System.Management;
using System.Configuration;
using System.Net.Sockets;
using System.Threading;

namespace Manual_Packer_Station
{
    public partial class Dasboard : Form
    {
        ControlDasboard ms;
        String adminid; 

        //tambahan disable close button
        //private const int CP_NOCLOSE_BUTTON = 0x200;
        //protected override CreateParams CreateParams
        //{
        //    get
        //    {
        //        CreateParams myCp = base.CreateParams;
        //        myCp.ClassStyle = myCp.ClassStyle | CP_NOCLOSE_BUTTON;
        //        return myCp;
        //    }
        //}
        //end of disable close button

        public Dasboard(string admin)
        {
            InitializeComponent();
            ms = new ControlDasboard(this);
            setAdminName(admin);
            this.adminid = admin;
        }

        public void setAdminName(string admin)
        {
            List<string> field = new List<string>();
            field.Add("first_name, company ");
            string where = "id = '" + admin + "'";
            List<string[]> ds = ms.db.selectList(field, "[USERS]", where);
            //if (ms.db.num_rows > 0)
            if (ds != null && ms.db.num_rows > 0)
            {
                lblUserId.Text = ds[0][0];
                lblRole.Text = ds[0][1];
            }
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            ManualPacker bl = new ManualPacker(adminid);
            bl.ShowDialog();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            lblTime.Text = DateTime.Now.ToLongTimeString();
            timer1.Start();
        }

        private void Dasboard_Load(object sender, EventArgs e)
        {
            timer1.Start();
            lblTime.Text = DateTime.Now.ToLongTimeString();
            lblDate.Text = DateTime.Now.ToShortDateString();
        }
    }
}
