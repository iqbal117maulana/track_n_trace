﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Drawing.Printing;

namespace Manual_Packer_Station
{
    class Printer
    {
        static tcp tcp = new tcp();
        static sqlitecs sql = new sqlitecs();

        //private static String ZplTemplate = "^XA~TA000~JSN^LT0^MNW^MTT^PON^PMN^LH0,0^JMA^PR3,3~SD23^MD25^JUS^LRN^CI0^XZ\r\n"
        //    + "^XA\r\n"
        //    + "^MMT\r\n"
        //    + "^PW709\r\n"
        //    + "^LL0709\r\n"
        //    + "^LS0\r\n"
        //    + "^FO20,0^GFA,08448,08448,00044,:Z64:eJzt2E1u2zgUB3Aq9ITFwKhmOYug7BG67CIIfRQfIcsuipJBDtArMehByu5mye60EPT6f6SVSJ7Yei4aoDMwIbhm/LNM8ePxsUqdy7mcy7n8x8vqheyr38D+fYL964Xsu9/Abn+53Zx83yRSGLELqV0r9YdqZPbyBKsx0UMTRbYJahW1zKqoVlsjtEmtbqX2g1q/b4X2Wq1vrNC+UWtLSWZbdcU2SKyJVy1lsxHZf64N9R9FbdB9b4g6URsa6jXRN6ltiO5EbVDUKyItt73Q+l75ZMTWhlZsefRktsOLlVka7XIQbijj1WEA46I11QY1LDfBE9/PJ5cXaUtUv0Fh0b4a7y5owuv6D9FyE5Qe7XITxhApaYJSt9X2EntdbSexN+XVCx4NMyHIbY1lLolsuaOV2fJURtaG0ltaZusoiPrMVisKaK7aNxK7mwmSAKF3FnvXYmnHGbZZtm60bxcpwulQv7LcaaYvY2Ekc/0T9gDFrU6LFLGmKSFKsNw4hLF1tNwELiVEiZbmaTY1WW7Ra0LbREAvtAGRUmrv2cr6TGPnDkJ7YYmiF4VJdXHjKX0UZkcRKcwHnSTUYJLl20YUH1a8a90qkeX53m2R0slsvt6qK6FNV9txP1q08XIriiXF/rlROshs0BtR3Cm22UiPAoaTa0Hc2dmN9Gj4uljZkbN27eUJ9kJk1/Pq0TP4+mh1Xvae6v0xu9dbR2fnnj0a4OcH6bKaDj7f3JZd/GAqOrclR/DhgJ0fpO1R+3ZuE14OhqG5LdmMcPso9tMJVnY8Ui7KHBf/UhZpaDMgB8F7i72aU3SDiuv4qvUnO/B+3vLfPH8n83Yd67FpPArsCmH7gHX4G5KL0DJA5McZT3Nqb6fZL2zUfEzrcA+KJW9Bho4zwGdOvt0004BNmo9/3A5KOCCgHdRjB/qKr/KBYWaz6QxbfP4dVb5/74hLamhim2pbzoPw+RcqbabOV6tpcgpg2xXLuyndc/Nx/1wo5bkdlIW1eGok8dZQhB30A2X0jM2lPrHIrTK6HTZdIov10XXNHUX0eNuV+mj1zmZFDX6Nc4uIZB6/0uLqbD+ZXJpHrc0+KbrjrsfIJlTMwIkybDe1PS6buSfv0eu4v0uomJ5/AjZPjgGm2ISfrzaxDY+W63HfYlDwlj9wXEEkwCEANk4s/mL27AOpCvetge3ctwGDgrfcQdXmegH6qcW3H9jyyQNBxaPCEaZYrk+tLVYPo/1Sbbm4HqY2szUDRza/6w9Eo2ety/4O/d4Xi8Vz31fLF9r7NHfw8JjggbAuOQq6nS39/C87aKRgASlbx1GQ54V5tC7h5Wmu8wSNVFaHrdP9aUzw6qbrokzmMs2TxfK0bGs/c1fztJ+vIVUtL/slO8Dit4LD4YDXXftofWxpvuZ7XkfEPvASfhwTvszMesqwHG44edVsc9kUeJoGPbOIXSX0dTWBnlm+pharpRlKOPT15FHGhEbunj/NH9y4nimy/PknrJyeYpuXssLzFBd9ihX9d8DOCo8GxSa5NeEEK6fCJPdc/sflB96IGnQ=:B49C^BY208,208"
        //    + "^FT390,422"
        //    + "^BXN,14,200,0,0,1,~\r\n"
        //    + "^FH\\^FD{0}^FS"
        //    //+ "^FT380,290^FB350,4,3,L,0^A0N,37,45^FH\\^FD{1}^FS\r\n" //^FT380,163^A0N,42,52^FH\\^FD{1}^FS\r\n
        //    + "^FT380,290^FB350,4,3,L,0^A0N,37,45^FH\\^FDQty : {1}^FS\r\n" //^FT380,163^A0N,42,52^FH\\^FD{1}^FS\r\n
        //    + "^FT390,490^A0N,29,28^FH\\^FD{0}^FS\r\n"
        //    + "^FT52,320^A0N,37,36^FH\\^FDsimpan antara suhu^FS\r\n"
        //    + "^FT52,366^A0N,37,36^FH\\^FD+2 \\F8C dan +8 \\F8C^FS\r\n"
        //    + "^FT52,209^A0N,29,28^FH\\^FDBANDUNG - INDONESIA^FS\r\n"
        //    + "^FT332,490^A0N,29,28^FH\\^FDSN :^FS\r\n"
        //    //+ "^FT332,526^A0N,29,28^FH\\^FDQTY^FS\r\n"
        //    + "^FT52,610^A0N,29,24^FH\\^FDPT. Bio Farma (Persero)^FS\r\n"
        //    + "^FT52,646^A0N,29,24^FH\\^FDJl. Pasteur No. 28, Bandung^FS\r\n"
        //    + "^FT52,682^A0N,29,24^FH\\^FDIndonesia ^FS\r\n"
        //    + "^PQ1,0,1,Y^XZ";

        //public static void Print(String SerialNumber, String ProductName)
        //{
        //    String ZplString = String.Format(ZplTemplate, SerialNumber, ProductName);

        //    tcp = new tcp();
        //    tcp.Connect(sql.config["ipprinter"], sql.config["portprinter"], sql.config["timeout"]);
        //    tcp.send(ZplString);
        //    tcp.dc();
        //}

        private static string SetDataSendPrinter(string masterboxid, List<string[]> batch, string product, string total)
        {
            try
            {
                string boxPU = 
                    "CT~~CD,~CC^~CT~"+
                    "^XA~TA000~JSN^LT0^MNW^MTT^PON^PMN^LH0,0^JMA^PR6,6~SD30^JUS^LRN^CI0^XZ"+
                    "~DG000.GRF,07680,040,"+
                    ",::::::::::::::::::::::::::::::::::::iL010,iL030,:iL060,:::iL0E0,iL0C0,iK01C0,:iK03C0,:hU030N0380,hU0380M0780,hU01E0I04C00F80,hV0F0I04C00F80,hV0780H0CF01F80,hV03C0H0C7C3F80,hV03E0H0C3FHF80,hW0F801C1FHFC0,hW0FC01E1FHFE0,hW07F03C1FIF070,hW03FCFC0FKF2,hW01FDFC05FDFDC4,hW01FHFC07FIF8E,hX0IFC03FIF0C,hX0IFC01FHF80C,hX07FFC00FHF01C,hX07FFC007F803C,hX07FFC001C003C,hX07FF80K07C,hX0FDF80K07C0J04,hW03FHF80K0FE0I0FE,hV01FIFL01FF001FF0,hV07FHFE0K01FKF80,hV07FHFM01FJFC,iM03FJF0,hU01C0O03FIF80,hU01E0O03FHFE,hV0780N01FDF8,hV03E0N03FFE0,hV01F0N01FFC0,hV01FC0M01FFC0,hW0HFO0HF80,hW0HF80M07F80,hW07FC0M03FC0,hW0HFE0N0FE0,hW0HFC0N07E0,hV01FHFO03F0,hV03FHFP078,hV0JFP03C,hU07FIFQ0F,hT01FJFQ03,hT07FIFE0L017F7F,hS03FJFE0L0JFE,hR01DFC05FC0K01DFDE0,hR07F8001FC0K03FHF80,hR040J0FC0K07FFC,hX0F80K0IF8,hX070H040H07FF8,hX0F003F800FHF8,hX0F01FFC00FHF8,hX0E0FHFE00FHF8,hX0C1FHFD00FDFC,hW01C3FIF80FHFE,hW0187FIF80FHFE,hW03BFJFC0FIF,hW017C7FHFC0F03F80,hX0601FHFE0F01F80,iG0JF0E007C0,iG0JF0E003E0,iG05F0F0C001F0,iG07E078C0H0F8,iG07C01CC0H03C,iG07C00C80H01E,iG07800480I0E,iG0F80020J07,iG070N0180,iG0F0O0C0,iG0E0O040,iG0E,iG0C,L020hS0C,L040hR01C,K01C0hR018,K03C0hR018,K0FC0hR018,J01DC0K01C0hJ010,J03F80K07E0hJ030,J07F80K0HFhK030,J0HF80K0HF0,J0HFM0HF0,J0HFM0FE0,I01FF0L0FE0,I01FE0L0F80,I01FE,I03FE,I03FIFJ03FC0H07F0K017F0I01FF0K01FC0I07FC0H07F0K017F,I03FIFE0H03FC003FFE0J0IFE0H0IFC0J0IF8003FHF807FFE0J0IFE0,I03FJF8007FC00FIF80H01FIFH03FHFE0I07FHFE007FHFC1FIF80H01FIF0,I03FJFE007F803FIFC0H07FIF80FJF80H0KF01FIFE3FIFC0H07FIF8,I07FJFE007F807FIFE0H0KFC1FJFC001FJF83FJF7FIFE0H0KFC,I07FKFH0HF80FKFH01FJFE3FJFC003FJF8FPFE001FJFE,I05FDFDFD00FD01FDFDFD003DFDFDE5FDFDFC007DFDFDCDFDFDFDFDFDF001FDFDFD,I0MF80FF03FKF807FJF8FKFE00FKF1FQF807FKF,I0HFH01FF80FF07FHF1FF80FHFC7F1FHFC7FF01FHF07E3FHF1FIFD3FF80FHF41FF,I0HF800FF80FE0FHF80FF80FFE00E1FFE03FF03FFC0387FF807FHFH0HF80FFE00FF80,I0HFI07FC1FE0FFC007FC1FFC0043FF0H0HF03FF80107FF003FFC007F81FFC007F80,H01FF0H03FC1FE1FF8003FC1FF80H03FF0H0HF07FE0I0HFE003FF8007F83FF8007F80,H01FF0H03FC1FE1FF0H03FC3FF0I07FC0H0HF07FC0I0HFC003FF0H07F83FF0H07F80,H03FE0H03FC3FE3FE0H03FC3FE0I07FC0H0HF0FFC0I0HF8003FF0H07F83FE0H0HF80,H03DE0H07FC1FC1DC0H07FC1FC0I07D80H0HF0DF80H01FD0H01FE0H07F83FC0H07D80,H03FE0H07F83FC3FE0H07F87FC0I0HF8001FF0FF80H01FF0H03FE0H0HF83F80H0HF80,H03FC0H07F83FC3FC0H07F87FC0I07F0H01FF0FF0I01FE0H07FC0H0HF87F80H0HF,H03FC0H0HF87F83FC0H0HF87F80I0HF8001FE0FF0I03FE0H07FC0H0HF87F80H0HF,H03FC001FF07F83FC001FF07F80I07F0H01FE1FF0I03FE0H07FC0H0HF07F8001FF,H03FE003FF07F83FC007FF0FF80I0HF8003FE1FE0I03FE0H07F8001FF07F8001FE,H03FE007FF07F03FE007FF0FF0J07F8003FC1FE0I03FC0H07F8001FF07FC001FE,H03FF83FFE0FF03FF83FFE0FF0J07FE003FC3FE0I07FC0H0HF8001FE03FE003FE,H01DFDFDFC0DF01DFDFDFC1DFDFC007DFDFDFC3DC0I05FC0H0DF0H01DE03DFDFDFC,H01FKF80FF01FKF81FIFC003FKFC3FC0I07F80H0HFI03FE03FKFE,I0LF01FF00FKF01FIFI03FKF83FC0I07F8001FF0H03FC01FKFC,I0KFE01FE00FJFE03FHFE0H01FKF87FC0I0HF8001FF0H03FC00FKFC,I07FIFC01FE007FIFC01FHFC0I0LF87FC0I0HFI01FE0H07FC007FJFC,I03FIF803FE003FIF803FHF80I07FJF87F80I0HFI03FE0H07FC003FJF8,J07FFE003FC0017FFC003FFE0J03FJF07F80H01FF0H03FE0H07FC001FJF8,J03FF8003FC0H03FF0H03FFC0K0KF0FF80H01FF0H03FE0H07F80H07FIF8,gH07DC0,gH07F80,:gH0HF80,gH0HF,gH0HF80,gH0HF,gH0FE,gG01FC,gG01F8,gG01F0,gG03E0,gG0180,,:::::::::::::^XA"+
                    "^MMT"+
                    "^PW709"+
                    "^LL0709"+
                    "^LS0"+
                    "^FT64,192^XG000.GRF,1,1^FS"+
                    "^FT70,219^A0N,26,26^FH\\^FDBANDUNG - INDONESIA^FS"+
                    "^BY256,256^FT389,494^BXN,16,200,0,0,1,~"+
                    "^FH\\^FD"+masterboxid+"^FS"+
                    "^FT70,290^A0N,26,26^FH\\^FDBATCH :^FS"+
                    "^FT70,330^A0N,26,26^FH\\^FD" + batch[0][0] + "^FS" +
                    "^FT70,370^A0N,26,26^FH\\^FD" + batch[1][0] + "^FS" +
                    "^FT70,410^A0N,26,26^FH\\^FD" + batch[2][0] + "^FS" +
                    "^FT70,450^A0N,26,26^FH\\^FD" + batch[3][0] + "^FS" +
                    "^FT70,490^A0N,26,26^FH\\^FD" + batch[4][0] + "^FS" +
                    "^FT70,530^A0N,26,26^FH\\^FDTotal^FS"+
                    "^FT230,290^A0N,26,26^FH\\^FDQTY :^FS"+
                    "^FT230,330^A0N,26,26^FH\\^FD" + batch[0][1] + "^FS" +
                    "^FT230,370^A0N,26,26^FH\\^FD" + batch[1][1] + "^FS" +
                    "^FT230,410^A0N,26,26^FH\\^FD" + batch[2][1] + "^FS" +
                    "^FT230,450^A0N,26,26^FH\\^FD" + batch[3][1] + "^FS" +
                    "^FT230,490^A0N,26,26^FH\\^FD" + batch[4][1] + "^FS" +

                    "^FT230,530^A0N,26,26^FH\\^FD"+total+"^FS"+
                    //"^FT70,293^A0N,26,26^FH\\^FD" + batch[0][0] + "^FS"+
                    //"^FT70,334^A0N,26,26^FH\\^FD" + batch[1][0] + "^FS" +
                    //"^FT70,378^A0N,26,26^FH\\^FD" + batch[2][0] + "^FS" +
                    //"^FT70,418^A0N,26,26^FH\\^FD" + batch[3][0] + "^FS" +
                    //"^FT70,461^A0N,26,26^FH\\^FD" + batch[4][0] + "^FS" +
                    //"^FT70,504^A0N,26,26^FH\\^FDTotal^FS"+
                    //"^FT230,293^A0N,26,26^FH\\^FD" + batch[0][1] + "^FS" +
                    //"^FT230,334^A0N,26,26^FH\\^FD" + batch[1][1] + "^FS" +
                    //"^FT230,378^A0N,26,26^FH\\^FD" + batch[2][1] + "^FS" +
                    //"^FT230,418^A0N,26,26^FH\\^FD" + batch[3][1] + "^FS" +
                    //"^FT230,461^A0N,26,26^FH\\^FD" + batch[4][1] + "^FS" +
                    //"^FT230,504^A0N,26,26^FH\\^FD"+total+"^FS"+

                    "^FT389,539^A0N,26,26^FH\\^FDSN : ^FS"+
                    "^FT443,539^A0N,26,26^FH\\^FD"+masterboxid+"^FS"+
                    "^FT61,590^A0N,26,26^FH\\^FDPT. Bio Farma (Persero)^FS"+
                    "^FT61,622^A0N,26,26^FH\\^FDJl. Pasteur No.28, Bandung^FS"+
                    "^FT61,654^A0N,26,26^FH\\^FDIndonesia^FS"+
                    "^FB270,3,,,"+
                    "^FT389,220^A0N,26,26^FH\\^FD"+product+"^FS"+
                    "^PQ1,0,1,Y^XZ"+
                    "^XA^ID000.GRF^FS^XZ";

                return boxPU;
            }
            catch(Exception e)
            {
                return "" + e.Message;
            }
        }

        public static void Print(String SerialNumber, List<string[]> batch, string product, string total)
        //public static void Print(String SerialNumber, String qty)
        {

            //String ZplString = String.Format(ZplTemplate, SerialNumber, qty);

            tcp = new tcp();
            tcp.Connect(sql.config["ipprinter"], sql.config["portprinter"], sql.config["timeout"]);
            tcp.send(SetDataSendPrinter(SerialNumber, batch, product, total));
            tcp.dc();
        }

        //private static String ZplTemplateDO = "CT~~CD,~CC^~CT~/n" +
        private static String ZplTemplateDO = "^XA~TA000~JSN^LT0^MNW^MTT^PON^PMN^LH0,0^JMA^PR2,3~SD25^JUS^LRN^CI0^XZ\r\n" +
            "^XA~SD25^MD25^MU203"
            + "^FX "
            + "^FO40,70^GB540,250,3^FS"
            + "^FO60,120"
            + "^BXN,12,200,,,,,"
            + "^FD{0}^FS"
            + "^FT255,110^A0N20,30^FDDO STICKER^FS"
            + "^FO255,120^A0N,20,20^FH^FDPacking Slip^FS"
            + "^FO360,120^A0N,20,20^FH^FD{1}^FS"
            + "^FO255,150^A0N,20,20^FH^FDSO^FS"
            + "^FO360,150^A0N,20,20^FH^FD{2}^FS"
            + "^FO255,180^A0N,20,20^FH^FDCustomer^FS"
            + "^FO360,180^FB220,2,3,L,12^A0N,20,20^FH^FD{3}^FS"
            + "^FO255,230^A0N,20,20^FH^FDAddress^FS"
            + "^FO360,230^FB220,6,3,L,12^A0N,20,20^FH^FD{4}^FS"
            + "^XZ";

        //private static String ZplTemplateDO = "^XA~TA000~JSN^LT0^MNW^MTT^PON^PMN^LH0,0^JMA^PR2,3~SD25^JUS^LRN^CI0^XZ\r\n" +
        //    "^XA~SD40^MU203"
        //    + "^FO25,330"
        //    + "^BXB,15,200,,,,,"
        //    + "^FD{0}^FS"
        //    + "^FT45,315^A0B20,30^FDDO STICKER^FS"
        //    + "^FO55,290^A0B,20,20^FH^FDDO^FS"
        //    + "^FO55,120^A0B,20,20^FH^FD{1}^FS"
        //    + "^FO80,290^A0B,20,20^FH^FDSO^FS"
        //    + "^FO80,93^A0B,20,20^FH^FD{2}^FS"
        //    + "^FO105,238^A0B,20,20^FH^FDCustomer^FS"
        //    + "^FO105,1^FB220,2,3,L,12^A0B,20,20^FH^FD{3}^FS"
        //    + "^FO153,248^A0B,20,20^FH^FDAddress^FS"
        //    + "^FO153,1^FB220,4,3,L,12^A0B,20,20^FH^FD{4}^FS"
        //    + "^XZ";

        public static void PrintDO(
            String NoDO,
            String NoSO,
            String CustName,
            String CustAddrs)
        {
            List<object> val = new List<object>();
            val.Add(NoDO);
            val.Add(": " + NoDO);
            val.Add(": " + NoSO);
            val.Add(": " + CustName);
            val.Add(": " + CustAddrs);
            String ZplString = String.Format(ZplTemplateDO, val[0], val[1], val[2], val[3], val[4]);

            //TEST PRINT PAKE USB JOSSS
            //PrintTest(ZplString);

            //HARUS PAKE YG INI
            tcp = new tcp();
            tcp.Connect(sql.config["ipprinter"], sql.config["portprinter"], sql.config["timeout"]);
            tcp.send(ZplString);
            tcp.dc();
        }

        public static void PrintTest(string koding)
        {
            RawPrinterHelper.SendStringToPrinter("ZDesigner ZT420-300dpi ZPL", koding);
        }
    }
}
