﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Net.NetworkInformation;
using System.Windows.Forms;
using System.Data;

namespace Manual_Packer_Station
{
    class ControlSAS
    {
        public const char delimit = ',';
        public SAS wr;
        private Thread serverThread;
        public sqlitecs sq;
        public dbaccess db;
        public string shippingOrderID;
        public ControlSAS(SAS man)
        {
            wr = man;
            sq = new sqlitecs();
            db = new dbaccess();
            serverThread = new Thread(new ThreadStart(MulaiServer));
            serverThread.Start();
        }
        public ServerMultiClient srv;
        public void MulaiServer()
        {
            srv = new ServerMultiClient();
            srv.Start(this, sq.config["portserver"]);
        }

        public void close(string da)
        {
            if (wr.InvokeRequired)
            {
                wr.Invoke(new Action<string>(close), new object[] { da });
                return;
            }
            srv.closeServer("");
        }

        public void CekDevice()
        {
            try
            {
                if (PingHost(sq.config["ipprinter"]))
                    wr.lblPrinter.Text = "Online";
                else
                    wr.lblPrinter.Text = "Offline";
            }
            catch (Exception ex)
            {
            }
        }

        public bool PingHost(string nameOrAddress)
        {
            if (nameOrAddress.Length == 0)
                return false;
            bool pingable = false;
            Ping pinger = new Ping();
            try
            {
                PingReply reply = pinger.Send(nameOrAddress);
                pingable = reply.Status == IPStatus.Success;
            }
            catch (PingException)
            {
                // Discard PingExceptions and return false;
            }
            return pingable;
        }

        bool cekdgv(string data, DataGridView dg)
        {
            for (int i = 0; i < dg.Rows.Count; i++)
            {
                if (data.Equals(dg.Rows[i].Cells[0].Value.ToString()))
                {
                    return true;
                }
            }
            return false;

        }

        //internal void tambahData(string data)
        //{
        //    if (wr.InvokeRequired)
        //    {
        //        wr.Invoke(new Action<string>(tambahData), new object[] { data });
        //        return;
        //    }


        //    log("Receive data : " + data);
        //    data = data.Replace("\r", "");
        //    data = data.Replace("\n", "");
        //    data = data.Replace("]d2", "");
        //    if (!data.Contains("?") && data.Length > 6)
        //    {
        //        wr.CheckScannedData(data);
        //    }
        //    else
        //    {
        //        log("Data not found!");
        //    }
        //}
      
        public void checkScannedItem(string data)
        {

        }

        public void log(string data)
        {
            if (wr.InvokeRequired)
            {
                wr.Invoke(new Action<string>(log), new object[] { data });
                return;
            }
            try
            {
                string dates = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                string simpan = dates + "\t" + data + "\n";
                //wr.txtLog.AppendText(simpan);
                wr.LogFile = wr.LogFile + simpan + Environment.NewLine;

            }
            catch (Exception ex )
            {
                //new Confirm("data" + ex.ToString());
            }
        }


        internal void startStation()
        {
            cmbDeliveryOrder();
        }

        public void cmbDeliveryOrder()
        {
            List<string> field = new List<string>();
            field.Add("dbo.shippingOrder.shippingOrderNumber");
            string where = "dbo.shippingOrder.flag = '2' and shippingType = '2' and status = '1'";
            List<string[]> ds = db.selectList(field, "shippingOrder",where);
            //if (db.num_rows > 0)
            if (ds != null && db.num_rows > 0)
            {
                List<string> dataCombo = new List<string>();
                foreach (string[] da in ds)
                {
                    dataCombo.Add(da[0]);
                }
                wr.cmbDelivery.DataSource = dataCombo;
            }
        }

        internal void setDetailCmb(string shippingOrder)
        {
            setProfile(shippingOrder);
            setBatch(shippingOrder);
        }

        private void setProfile(string shipping)
        {
            List<string> field = new List<string>();
            field.Add("dbo.shippingOrder.customerName,dbo.shippingOrder.customerAddress");
            string where = "dbo.shippingOrder.shippingOrderNumber = '" + shipping + "'";
            List<string[]> ds = db.selectList(field, "shippingOrder", where);
            //if (db.num_rows > 0)
            if (ds != null && db.num_rows > 0)
            {
                wr.txtAddress.Text = ds[0][1];
                wr.txtCost.Text = ds[0][0];
            }
        }

        public void setBatch(string shipping)
        {
            List<string> field = new List<string>();
            field.Add("dbo.shippingOrder_detail.batchnumber 'Batch Number',dbo.product_model.model_name 'Product Model',dbo.shippingOrder_detail.qty 'Qty'");
            string from = "dbo.shippingOrder_detail INNER JOIN dbo.product_model ON dbo.shippingOrder_detail.productModelId = dbo.product_model.product_model";
            string where = "dbo.shippingOrder_detail.shippingOrderNumber = '" + shipping + "'";
            List<string[]> ds = db.selectList(field, from, where);
            //if (db.num_rows > 0)
            if (ds != null && db.num_rows > 0)
            {
                wr.BatchNumber = ds[0][0];
                wr.dgvDetail.DataSource = setDataTable(ds,db.dataHeader);
            }
        }

        private DataTable setDataTable(List<string[]> ds, DataTable dataheader)
        {
            DataTable datatemp = dataheader;
            foreach (string[] Row in ds)
            {
                DataRow row = datatemp.NewRow();
                for (int k = 0; k < Row.Length; k++)
                {
                        row[k] = Row[k];
                }
                datatemp.Rows.Add(row);
            }
            return datatemp;
        }

        internal void getDetailVial(string itemid, string uom)
        {
            if (uom == "Basket")
            {
                wr.lblUom.Text = "Infeed";
                getinfeed(itemid);
            }
            else if (uom == "Infeed")
            {
                wr.lblUom.Text = "Vial";
                getVial(itemid);
            }
        }

        public void getinfeed(string basket)
        {
            List<string> field = new List<string>();
            field.Add("dbo.innerBox.infeedInnerBoxId,dbo.innerBox.gsOneInnerBoxId");
            string where = "dbo.innerBox.basketId = '" + basket + "'";
            List<string[]> ds = db.selectList(field, "innerbox", where);
            //if (db.num_rows>0)
            if (ds != null && db.num_rows > 0)
            {
                wr.dgvDetail.DataSource = setDataTable(ds,db.dataHeader);
            }
                
        }

        public void getVial(string infeed)
        {
            List<string> field = new List<string>();
            field.Add("dbo.Vaccine.capId,dbo.Vaccine.gsOneVialId");
            string where = "dbo.Vaccine.innerBoxId = '" + infeed + "' OR dbo.Vaccine.innerBoxGsOneId = '" + infeed + "'";
            List<string[]> ds = db.selectList(field, "dbo.Vaccine", where);
            //if (db.num_rows > 0)
            if (ds != null && db.num_rows > 0)
            {
                wr.dgvDetail.DataSource = setDataTable(ds, db.dataHeader);
            }

        }

        public void setDetailVial(string pickinglist)
        {
            List<string> field = new List<string>();
            field.Add("dbo.shippingOrder_detail.batchnumber,dbo.product_model.model_name,dbo.packaging_order.expired");
            string from = "dbo.pickingList INNER JOIN dbo.shippingOrder_detail ON dbo.pickingList.detailNo = dbo.shippingOrder_detail.shippingOrderDetailNo INNER JOIN dbo.product_model ON dbo.shippingOrder_detail.productModelId = dbo.product_model.product_model INNER JOIN dbo.packaging_order ON dbo.packaging_order.packagingOrderNumber = dbo.shippingOrder_detail.batchnumber";
            string where = "dbo.pickingList.itemId = '" + pickinglist + "'";
            List<string[]> ds = db.selectList(field, from, where);
            //if (db.num_rows > 0)
            if (ds != null && db.num_rows > 0)
            {
                wr.txtAddress.Text = ds[0][2];
                wr.txtCost.Text = ds[0][0];
                wr.lblProductName.Text = ds[0][1];
            }
        }

    }
}
