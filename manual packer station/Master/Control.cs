﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Net.NetworkInformation;
using System.Windows.Forms;
using System.Data;

namespace Manual_Packer_Station
{
    class Control
    {
        public const char delimit = ',';
        public ManualPacker wr;
        private Thread serverThread;
        public sqlitecs sq;
        public dbaccess db;
        public string shippingOrderID;
        public string adminid; 

        public DataTable dtDetailDO;
        public DataTable dtDetailBoxPU;

        public String UOM_BASKET = "Basket";
        public String UOM_INNER_BOX = "Innerbox";
        public String UOM_VIAL = "Vial";

        public String UOM_BASKET_TEXT = "Basket";
        public String UOM_INNER_BOX_TEXT = "Innerbox";
        public String UOM_VIAL_TEXT = "Vial";  

        public Control(ManualPacker man)
        {
            wr = man;
            sq = new sqlitecs();
            db = new dbaccess();
            serverThread = new Thread(new ThreadStart(MulaiServer));
            serverThread.Start();
        }

        public ServerMultiClient srv;
        public void MulaiServer()
        {
            srv = new ServerMultiClient();
            srv.Start(this, sq.config["portserver"]);
        }

        public void close(string da)
        {
            if (wr.InvokeRequired)
            {
                wr.Invoke(new Action<string>(close), new object[] { da });
                return;
            }
            srv.closeServer("");
        }

        public string DeleteSpecialChar(string data)
        {
            data = data.Replace("\r", "");
            data = data.Replace("\n", "");
            data = data.Replace("]d2", "");
            data = data.Replace("$", "");
            data = data.Replace(" ", "");
            data = data.Replace("<GS>", "");
            char da = (char)29;
            data = data.Replace("" + da, "");

            return data;
        }

        public void CekDevice()
        {
            try
            {
                if (PingHost(sq.config["ipCamera"]))
                    wr.lblCamera.Text = "Online";
                else
                    wr.lblCamera.Text = "Online";

                if (PingHost(sq.config["ipprinter"]))
                    wr.lblPrinter.Text = "Online";
                else
                    wr.lblPrinter.Text = "Offline";
            }
            catch (Exception ex)
            {

            }
        }

        public bool PingHost(string nameOrAddress)
        {
            if (nameOrAddress.Length == 0)
                return false;
            bool pingable = false;
            Ping pinger = new Ping();
            try
            {
                PingReply reply = pinger.Send(nameOrAddress);
                pingable = reply.Status == IPStatus.Success;
            }
            catch (PingException)
            {
                // Discard PingExceptions and return false;
            }
            return pingable;
        }

        bool cekdgv(string data, DataGridView dg)
        {
            for (int i = 0; i < dg.Rows.Count; i++)
            {
                if (data.Equals(dg.Rows[i].Cells[0].Value.ToString()))
                {
                    return true;
                }
            }
            return false;
        }

        internal void tambahData(string data)
        {
            if (wr.InvokeRequired)
            {
                wr.Invoke(new Action<string>(tambahData), new object[] { data });
                return;
            }

            log("Receive data : " + data);
            data = data.Replace("\r", "");
            data = data.Replace("\n", "");
            data = data.Replace("]d2", "");
            if (!data.Contains("?") && data.Length > 6)
            {
                wr.CheckScannedData(data);
            }
            else
            {
                log("Data not found!");
            }
        }
      
        public void log(string data)
        {
            if (wr.InvokeRequired)
            {
                wr.Invoke(new Action<string>(log), new object[] { data });
                return;
            }
            try
            {
                string dates = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                string simpan = dates + "\t" + data + "\n";
                wr.LogFile = wr.LogFile + simpan + Environment.NewLine;
                db.simpanLog(simpan);
            }
            catch (Exception ex)
            {
                //BLANK
            }
        }

        internal void startStation()
        {
            log("Login successfully admin ID = " + adminid);
            db.adminid = adminid;
            db.from = "Packing Control";
            db.eventname = "Start Station";
            db.eventtype = "12";
            db.systemlog();
            //cmbDeliveryOrder();
        }        

        internal void setDetailCmb(string shippingOrder)
        {
            setProfile(shippingOrder);
            setBatch(shippingOrder);
        }

        private void setProfile(string shipping)
        {
            List<string> field = new List<string>();
            field.Add("dbo.shippingOrder.customerName,dbo.shippingOrder.customerAddress");
            string where = "dbo.shippingOrder.shippingOrderNumber = '" + shipping + "'";
            List<string[]> ds = db.selectList(field, "shippingOrder", where);
            //if (db.num_rows > 0)
            //ds
            if (ds != null && db.num_rows > 0)
            {
                wr.txtAddress.Text = ds[0][1];
                wr.txtCost.Text = ds[0][0];
            }
        }

        public void setBatch(string shipping)
        {
            List<string> field = new List<string>();
            field.Add("dbo.shippingOrder_detail.batchnumber 'Batch Number',dbo.product_model.model_name 'Product Model',dbo.shippingOrder_detail.qty 'Qty'");
            string from = "dbo.shippingOrder_detail INNER JOIN dbo.product_model ON dbo.shippingOrder_detail.productModelId = dbo.product_model.product_model";
            string where = "dbo.shippingOrder_detail.shippingOrderNumber = '" + shipping + "'";
            List<string[]> ds = db.selectList(field, from, where);
            //if (db.num_rows > 0)
            if (ds != null && db.num_rows > 0)
            {
                wr.BatchNumber = ds[0][0];
                wr.dgvDetail.DataSource = setDataTable(ds,db.dataHeader);
            }
        }

        private DataTable setDataTable(List<string[]> ds, DataTable dataheader)
        {
            DataTable datatemp = dataheader;
            foreach (string[] Row in ds)
            {
                DataRow row = datatemp.NewRow();
                for (int k = 0; k < Row.Length; k++)
                {
                        row[k] = Row[k];
                }
                datatemp.Rows.Add(row);
            }
            return datatemp;
        }

        internal void getDetailVial(string itemid, string uom)
        {
            if (uom == "Basket")
            {
                wr.lblUom.Text = "Infeed";
                getinfeed(itemid);
            }
            else if (uom == "Infeed")
            {
                wr.lblUom.Text = "Vial";
                getVial(itemid);
            }
        }

        public void getinfeed(string basket)
        {
            List<string> field = new List<string>();
            field.Add("dbo.innerBox.infeedInnerBoxId,dbo.innerBox.gsOneInnerBoxId");
            string where = "dbo.innerBox.basketId = '" + basket + "'";
            List<string[]> ds = db.selectList(field, "innerbox", where);
            //if (db.num_rows>0)
            if (ds != null && db.num_rows > 0)
            {
                wr.dgvDetail.DataSource = setDataTable(ds,db.dataHeader);
            }                
        }

        public void getVial(string infeed)
        {
            List<string> field = new List<string>();
            field.Add("dbo.Vaccine.capId,dbo.Vaccine.gsOneVialId");
            string where = "dbo.Vaccine.innerBoxId = '" + infeed + "' OR dbo.Vaccine.innerBoxGsOneId = '" + infeed + "'";
            List<string[]> ds = db.selectList(field, "dbo.Vaccine", where);
            //if (db.num_rows > 0)
            if (ds != null && db.num_rows > 0)
            {
                wr.dgvDetail.DataSource = setDataTable(ds, db.dataHeader);
            }
        }

        public void setDetailVial(string pickinglist)
        {
            List<string> field = new List<string>();
            field.Add("dbo.shippingOrder_detail.batchnumber,dbo.product_model.model_name,dbo.packaging_order.expired");
            string from = "dbo.pickingList INNER JOIN dbo.shippingOrder_detail ON dbo.pickingList.detailNo = dbo.shippingOrder_detail.shippingOrderDetailNo INNER JOIN dbo.product_model ON dbo.shippingOrder_detail.productModelId = dbo.product_model.product_model INNER JOIN dbo.packaging_order ON dbo.packaging_order.packagingOrderNumber = dbo.shippingOrder_detail.batchnumber";
            string where = "dbo.pickingList.itemId = '" + pickinglist + "'";
            List<string[]> ds = db.selectList(field, from, where);
            //if (db.num_rows > 0)
            if (ds != null && db.num_rows > 0)
            {
                wr.txtAddress.Text = ds[0][2];
                wr.txtCost.Text = ds[0][0];
                wr.lblProductName.Text = ds[0][1];
            }
        }

        //public void cmbDeliveryOrder()
        //{
        //    string sql;
        //    DataTable dtDO;
        //    sql = "SELECT id, PackingSlipId, ShipmentOrderNumber, status FROM packing_slip WHERE status = 0";
        //    db.OpenQuery(out dtDO, sql);

        //    wr.cmbDelivery.ValueMember = "ShipmentOrderNumber"; //ShipmentOrderNumber
        //    wr.cmbDelivery.DisplayMember = "PackingSlipId"; //ShipmentOrderNumber
        //    wr.cmbDelivery.DataSource = dtDO.Copy();
        //    wr.cmbDelivery.SelectedIndex = -1;
        //}

        public void GetDetailBoxPU(string noDO)
        {
            dtDetailBoxPU = new DataTable();
            string sql;
            sql = "select DISTINCT a.masterBoxId, a.colieNumber " +
                    "from masterBox a " +
                        " INNER JOIN masterbox_detail b ON b.masterBoxid = a.masterBoxId " +
                        " INNER JOIN shippingOrder_detail c ON c.shippingOrderDetailNo = b.shippingOrderDetailNo " +
                    " WHERE a.isReject = 0 AND c.shippingOrderNumber = '" + noDO + "' " + //AND c.status = '2'  " +
                    " order by masterBoxId desc";
            //KOMEN DULU, BISI KEPAKE LAGI
            //sql = "select DISTINCT a.masterBoxId, d.model_name, a.colieNumber from masterBox a " +
            //            " INNER JOIN shippingOrder_detail b ON b.batchnumber = a.batchnumber " +
            //            " INNER JOIN pickingList c ON c.detailNo = b.shippingOrderDetailNo " +
            //            " INNER JOIN product_model d ON d.product_model = b.productModelId " +
            //        " WHERE a.isReject = 0 AND b.shippingOrderNumber = '" + noDO + "' " + //AND c.status = '2'  " +
            //        " order by masterBoxId desc";
            db.OpenQuery(out dtDetailBoxPU, sql);
            wr.dataGridView2.AutoGenerateColumns = false;
            wr.dataGridView2.DataSource = dtDetailBoxPU;
            wr.labelQtyHistory.Text = dtDetailBoxPU.Rows.Count.ToString();
            //SET READONLY COLUMN TO FALSE
            for (int i = 0; i <= dtDetailBoxPU.Columns.Count - 1; i++)
            {
                dtDetailBoxPU.Columns[i].ReadOnly = false;
            }
        }

        public void GetDetailDO(string noDO)
        {
            dtDetailDO = new DataTable();
            string sql;
            //sql = "SELECT 0 as state, a.itemId, a.uom, c.ShipmentOrderNumber, b.productModelId, d.model_name, e.batchNumber, " +
            //            "d.gtin, e.expired, a.detailNo, a.qty, 'vial' as uom_vial, 0 as qty_1, " +
            //            "(SELECT " +
            //                " CASE WHEN a.uom = 'Basket' THEN (SELECT count(f.capId) FROM Vaccine f WHERE b.batchnumber = f.batchNumber AND f.basketId = a.itemId AND f.masterBoxId IS NOT NULL) " +
            //                    " WHEN a.uom = 'Innerbox' THEN (SELECT count(f.capId) FROM Vaccine f WHERE b.batchnumber = f.batchNumber AND f.innerBoxId = a.itemId AND f.masterBoxId IS NOT NULL) " +
            //                    " WHEN a.uom = 'Vial' THEN (SELECT count(f.capId) FROM Vaccine f WHERE b.batchnumber = f.batchNumber AND f.gsOneVialId = a.itemId AND f.masterBoxId IS NOT NULL) " +
            //                " END  " +
            //            ") as qty_scanned, d.isSAS, d.outbox_qty " +
            //        "FROM pickingList a INNER JOIN shippingOrder_detail b ON a.detailNo = b.shippingOrderDetailNo " +
            //            "INNER JOIN packing_slip c ON c.ShipmentOrderNumber = b.shippingOrderNumber " +
            //            "INNER JOIN product_model d ON d.product_model = b.productModelId " +
            //            "INNER JOIN packaging_order e ON e.batchNumber = b.batchNumber AND e.productModelId = b.productModelId " +
            //        "WHERE c.ShipmentOrderNumber= '" + noDO + "' AND a.status = '1' AND c.status = '0' AND d.isSAS = 0 " +
            //        " UNION ALL " +
            //        "SELECT 0 as state, a.itemId, a.uom, c.ShipmentOrderNumber, b.productModelId, d.model_name, e.batchNumber, " +
            //            "d.gtin, e.expired, a.detailNo, a.qty, 'vial' as uom_vial, 0 as qty_1, " +
            //            "(SELECT " +
            //                " CASE WHEN a.uom = 'Basket' THEN (SELECT count(f.basketId) FROM Vaccine f WHERE b.batchnumber = f.batchNumber AND f.basketId = a.itemId AND f.masterBoxId IS NOT NULL) " +
            //                    " WHEN a.uom = 'Innerbox' THEN (SELECT count(f.innerBoxId) FROM Vaccine f WHERE b.batchnumber = f.batchNumber AND f.innerBoxId = a.itemId AND f.masterBoxId IS NOT NULL) " +
            //                " END  " +
            //            ") * d.outbox_qty as qty_scanned, d.isSAS, d.outbox_qty " +
            //        "FROM pickingList a INNER JOIN shippingOrder_detail b ON a.detailNo = b.shippingOrderDetailNo " +
            //            "INNER JOIN packing_slip c ON c.ShipmentOrderNumber = b.shippingOrderNumber " +
            //            "INNER JOIN product_model d ON d.product_model = b.productModelId " +
            //            "INNER JOIN packaging_order e ON e.batchNumber = b.batchNumber AND e.productModelId = b.productModelId " +
            //        "WHERE c.ShipmentOrderNumber= '" + noDO + "' AND a.status = '1' AND c.status = '0' AND d.isSAS = 1 ";

            //ALTERNATIP QUERY
            sql = "SELECT 0 as state, a.itemId, a.uom, c.ShipmentOrderNumber, b.productModelId, d.model_name, b.batchNumber, " +
                        "d.gtin, e.expired, a.detailNo, a.qty, 'vial' as uom_vial, 0 as qty_1, " +
                        "count(f.capId) as qty_scanned, d.isSAS, d.outbox_qty " +
                    "FROM pickingList a INNER JOIN shippingOrder_detail b ON a.detailNo = b.shippingOrderDetailNo " +
                        "INNER JOIN packing_slip c ON c.ShipmentOrderNumber = b.shippingOrderNumber " +
                        "INNER JOIN product_model d ON d.product_model = b.productModelId " +
                        "INNER JOIN packaging_order e ON e.batchNumber = b.batchNumber AND e.productModelId = b.productModelId " +
                        "LEFT JOIN vaccine f ON b.batchnumber = f.batchNumber AND f.basketId = a.itemId and f.pickinglist = a.pickinglist AND f.masterboxId IS NOT NULL " +
                    "WHERE c.ShipmentOrderNumber= '" + noDO + "' AND a.status = '1' AND c.status in ('0','3') AND d.isSAS = 0 AND a.uom = 'Basket' and b.flag = 2 " +
                    "GROUP BY a.itemId, a.uom, c.ShipmentOrderNumber, b.productModelId, d.model_name, b.batchNumber, d.gtin, e.expired, a.detailNo, a.qty, d.isSAS, d.outbox_qty " +
                    " UNION ALL " +
                   "SELECT 0 as state, a.itemId, a.uom, c.ShipmentOrderNumber, b.productModelId, d.model_name, b.batchNumber, " +
                        "d.gtin, e.expired, a.detailNo, a.qty, 'vial' as uom_vial, 0 as qty_1, " +
                        "count(f.capId) as qty_scanned, d.isSAS, d.outbox_qty " +
                    "FROM pickingList a INNER JOIN shippingOrder_detail b ON a.detailNo = b.shippingOrderDetailNo " +
                        "INNER JOIN packing_slip c ON c.ShipmentOrderNumber = b.shippingOrderNumber " +
                        "INNER JOIN product_model d ON d.product_model = b.productModelId " +
                        "INNER JOIN packaging_order e ON e.batchNumber = b.batchNumber AND e.productModelId = b.productModelId " +
                        "LEFT JOIN vaccine f ON b.batchnumber = f.batchNumber AND (f.innerBoxId = a.itemId OR f.innerboxGsOneId = a.itemId) and f.pickinglist = a.pickinglist AND f.masterboxId IS NOT NULL " +
                    "WHERE c.ShipmentOrderNumber= '" + noDO + "' AND a.status = '1' AND c.status in ('0','3') AND d.isSAS = 0 AND a.uom = 'Innerbox' and b.flag = 2 " +
                    "GROUP BY a.itemId, a.uom, c.ShipmentOrderNumber, b.productModelId, d.model_name, b.batchNumber, d.gtin, e.expired, a.detailNo, a.qty, d.isSAS, d.outbox_qty " +
                    " UNION ALL " +
                   "SELECT 0 as state, a.itemId, a.uom, c.ShipmentOrderNumber, b.productModelId, d.model_name, b.batchNumber, " +
                        "d.gtin, e.expired, a.detailNo, a.qty, 'vial' as uom_vial, 0 as qty_1, " +
                        "count(f.capId) as qty_scanned, d.isSAS, d.outbox_qty " +
                    "FROM pickingList a INNER JOIN shippingOrder_detail b ON a.detailNo = b.shippingOrderDetailNo " +
                        "INNER JOIN packing_slip c ON c.ShipmentOrderNumber = b.shippingOrderNumber " +
                        "INNER JOIN product_model d ON d.product_model = b.productModelId " +
                        "INNER JOIN packaging_order e ON e.batchNumber = b.batchNumber AND e.productModelId = b.productModelId " +
                        "LEFT JOIN vaccine f ON b.batchnumber = f.batchNumber AND (f.capId = a.itemId OR f.gsoneVialId = a.itemId) and f.pickinglist = a.pickinglist AND f.masterboxId IS NOT NULL " +
                    "WHERE c.ShipmentOrderNumber= '" + noDO + "' AND a.status = '1' AND c.status in ('0','3') AND d.isSAS = 0 AND a.uom = 'Vial' and b.flag = 2 " +
                    "GROUP BY a.itemId, a.uom, c.ShipmentOrderNumber, b.productModelId, d.model_name, b.batchNumber, d.gtin, e.expired, a.detailNo, a.qty, d.isSAS, d.outbox_qty " +
                    " UNION ALL " +
                   "SELECT 0 as state, a.itemId, a.uom, c.ShipmentOrderNumber, b.productModelId, d.model_name, b.batchNumber, " +
                        "d.gtin, e.expired, a.detailNo, a.qty, 'vial' as uom_vial, 0 as qty_1, " +
                        "(count(f.capId)*d.outbox_qty)-sum(isnull(f.substraction,0)) as qty_scanned, d.isSAS, d.outbox_qty " +
                    "FROM pickingList a INNER JOIN shippingOrder_detail b ON a.detailNo = b.shippingOrderDetailNo " +
                        "INNER JOIN packing_slip c ON c.ShipmentOrderNumber = b.shippingOrderNumber " +
                        "INNER JOIN product_model d ON d.product_model = b.productModelId " +
                        "INNER JOIN packaging_order e ON e.batchNumber = b.batchNumber AND e.productModelId = b.productModelId " +
                        "LEFT JOIN vaccine f ON b.batchnumber = f.batchNumber AND f.basketId = a.itemId and f.pickinglist = a.pickinglist AND f.masterboxId IS NOT NULL " +
                    "WHERE c.ShipmentOrderNumber= '" + noDO + "' AND a.status = '1' AND c.status in ('0','3') AND d.isSAS = 1 AND a.uom = 'Basket' and b.flag = 2 " +
                    "GROUP BY a.itemId, a.uom, c.ShipmentOrderNumber, b.productModelId, d.model_name, b.batchNumber, d.gtin, e.expired, a.detailNo, a.qty, d.isSAS, d.outbox_qty " +
                    " UNION ALL " +
                   "SELECT 0 as state, a.itemId, a.uom, c.ShipmentOrderNumber, b.productModelId, d.model_name, b.batchNumber, " +
                        "d.gtin, e.expired, a.detailNo, a.qty, 'vial' as uom_vial, 0 as qty_1, " +
                        "(count(f.capId)*d.outbox_qty)-sum(isnull(f.substraction,0)) as qty_scanned, d.isSAS, d.outbox_qty " +
                    "FROM pickingList a INNER JOIN shippingOrder_detail b ON a.detailNo = b.shippingOrderDetailNo " +
                        "INNER JOIN packing_slip c ON c.ShipmentOrderNumber = b.shippingOrderNumber " +
                        "INNER JOIN product_model d ON d.product_model = b.productModelId " +
                        "INNER JOIN packaging_order e ON e.batchNumber = b.batchNumber AND e.productModelId = b.productModelId " +
                        "LEFT JOIN vaccine f ON b.batchnumber = f.batchNumber AND (f.innerBoxId = a.itemId OR f.innerboxGsOneId = a.itemId) and f.pickinglist = a.pickinglist AND f.masterboxId IS NOT NULL " +
                    "WHERE c.ShipmentOrderNumber= '" + noDO + "' AND a.status = '1' AND c.status in ('0','3') AND d.isSAS = 1 AND a.uom = 'Innerbox' and b.flag = 2 " +
                    "GROUP BY a.itemId, a.uom, c.ShipmentOrderNumber, b.productModelId, d.model_name, b.batchNumber, d.gtin, e.expired, a.detailNo, a.qty, d.isSAS, d.outbox_qty ";

            db.OpenQuery(out dtDetailDO, sql);
            wr.dataGridView1.AutoGenerateColumns = false;
            wr.dataGridView1.DataSource = dtDetailDO;
            //wr.lblQtyTotal.Text = dtDetailDO.Rows.Count.ToString(); //GANTI DENGAN QTY TOTAL VIALNYA
            //SET READONLY COLUMN TO FALSE (LOOP COLUMN)
            for (int i = 0; i <= dtDetailDO.Columns.Count-1; i++)
            {
                dtDetailDO.Columns[i].ReadOnly = false;
            }

            //object tmpVial;
            //object tmpScanned;
            //tmpVial = dtDetailDO.Compute("SUM(qty)", string.Empty);
            //tmpScanned = dtDetailDO.Compute("SUM(qty_scanned)", string.Empty);
            //if (tmpVial == null || tmpVial.ToString().Equals(""))
            //{
            //    tmpVial == "0";
            //}

            //if (tmpScanned == null || tmpScanned.ToString().Equals(""))
            //{
            //    tmpScanned == "0";
            //}
            //wr.lblQtyTotal.Text = Convert.ToString(int.Parse(tmpVial.ToString()) - int.Parse(tmpScanned.ToString()));
            //wr.lblQtyScanned.Text = "0";

            object tmpVial;
            object tmpScanned;
            if (dtDetailDO.Rows.Count > 0)
            {
                tmpVial = dtDetailDO.Compute("SUM(qty)", string.Empty);
                tmpScanned = dtDetailDO.Compute("SUM(qty_scanned)", string.Empty);
                wr.lblQtyTotal.Text = Convert.ToString(int.Parse(tmpVial.ToString()) - int.Parse(tmpScanned.ToString()));
            }
            else {
                wr.lblQtyTotal.Text = "0";
            }
            wr.lblQtyScanned.Text = "0";

            //AMBIL QTY UOM-1 (LOOP ROOWS)
            
            //for (int i = 0; i <= dtDetailDO.Rows.Count - 1; i++)
            //{
                //DI KOMEN KARNA KOLOM NYA UDAH DI HIDE
                //dtDetailDO.Rows[i]["qty_1"] = getQtyproduct(
                //    dtDetailDO.Rows[i]["uom"].ToString().ToUpper(),
                //    dtDetailDO.Rows[i]["itemid"].ToString()
                //);
            //}
            //wr.lblQtyTotal.Text = Convert.ToString(tmpVial - tmpScanned);
            //wr.lblQtyScanned.Text = Convert.ToString(tmpScanned);

            //SET ALLCELLS GRID
            if (dtDetailDO.Rows.Count > 0)
            {
                for (int i = 0; i < wr.dataGridView1.Columns.Count; i++)
                {
                    wr.dataGridView1.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                }
            }
            else
            {
                for (int i = 0; i < wr.dataGridView1.Columns.Count; i++)
                {
                    wr.dataGridView1.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                }
            }
        }

        private int getQtyproduct(string Uom, string itemid)
        {
            String sql = "";
            if (Uom.Equals("BASKET"))
            {
                sql = "SELECT COUNT(gsOneInnerBoxId) FROM InnerBox WHERE basketId = '" + itemid + "'";
            }
            else if (Uom.Equals("INNERBOX"))
            {
                sql = "SELECT COUNT(CapId) FROM Vaccine WHERE innerboxId = '" + itemid + "' OR innerBoxGsOneId = '" + itemid + "'";
            }
            else if (Uom.Equals("VIAL"))
            {
                sql = "SELECT COUNT(CapId) FROM Vaccine WHERE [gsOneVialId] = '" + itemid + "' OR capid = '" + itemid + "'";
            }
            DataTable dtTmp = new DataTable();
            db.OpenQuery(out dtTmp, sql);
            int hasil;
            if (dtTmp.Rows.Count > 0)
            {
                hasil = Convert.ToInt32(dtTmp.Rows[0][0]);
            }
            else
            {
                hasil = 0;
            }
            return hasil;
        }

        public int getQtyMasterBox(string masterboxId)
        {
            string sql;
            DataTable dtTmp;
            sql = "SELECT SUM(qty) FROM masterBox_detail WHERE masterboxId = '" + masterboxId + "'";
            db.OpenQuery(out dtTmp, sql);
            return Convert.ToInt32(dtTmp.Rows[0][0]);
        }

        public int getQtyPenguranganVial(string Uom, string itemid, string basketid, string batchN)
        {
            String sql = "";
            int hasil = 0;
            //bool hasil2 = false;
            string listItemId = "";
            string listItemGsId = "";
            if (Uom.Equals(UOM_INNER_BOX))
            {
                sql = "SELECT DISTINCT capId as itemid, gsOneVialId as gsOne, 'Vial' as uom " +
                        " from Vaccine where (innerBoxId = '" + itemid + "' or innerBoxGsOneId = '" + itemid + "') " +
                        " AND batchNumber = '" + batchN + "' " +
                        " and pickingList in (select pickingList from pickingList where detailNo in " +
                        " (select shippingOrderDetailNo from shippingOrder_detail where shippingOrderNumber = '" + wr.PickingListNumber + "') " +
                        " and (itemid = '" + basketid + "' or itemid = '" + basketid + "'))";
                DataTable dtTmp = new DataTable();
                db.OpenQuery(out dtTmp, sql);
                for (int i = 0; i <= dtTmp.Rows.Count - 1; i++)
                {
                    listItemId += "'" + dtTmp.Rows[i][0].ToString() + "', ";
                    listItemGsId += "'" + dtTmp.Rows[i][1].ToString() + "', ";
                    //hasil2 = wr.CekScanned(dtTmp.Rows[i][0].ToString(), dtTmp.Rows[i][1].ToString(), dtTmp.Rows[i][2].ToString(), true);
                    //if (hasil2)
                    //{
                    //    hasil += 1;
                    //}
                }
                hasil = wr.CekQtyScanned(listItemId, listItemGsId, "Vial");
            }
            return hasil;
        }

        public int getQtyVial(string Uom, string itemid, string tmpBasket, string batchN, bool notParent)
        {
            String sql = "";
            if (Uom.Equals(UOM_BASKET))
            {
                sql = "SELECT COUNT(CapId) FROM Vaccine WHERE masterboxId IS NULL AND basketId = '" + itemid + "'";
            }
            else if (Uom.Equals(UOM_INNER_BOX))
            {
                sql = "SELECT COUNT(CapId) FROM Vaccine WHERE masterboxId IS NULL " +
                        "AND (innerboxId = '" + itemid + "' OR innerBoxGsOneId = '" + itemid + "') " +
                        "AND batchNumber = '" + batchN + "' and pickingList in (select pickingList from pickingList where detailNo in " +
                        "(select shippingOrderDetailNo from shippingOrder_detail where shippingOrderNumber = '" + wr.PickingListNumber + "')" +
                        "AND itemId = '" + tmpBasket + "')";
            }
            DataTable dtTmp = new DataTable();
            db.OpenQuery(out dtTmp, sql);
            int hasil;
            if (dtTmp.Rows.Count > 0)
            {
                if (notParent)
                    hasil = Convert.ToInt32(dtTmp.Rows[0][0]) - getQtyPenguranganVial(Uom, itemid, itemid, batchN);
                else
                    hasil = Convert.ToInt32(dtTmp.Rows[0][0]) - getQtyPenguranganVial(Uom, itemid, tmpBasket, batchN);
            }
            else
            {
                hasil = 0;
            }
            return hasil;
        }

        public int getQtySubstrac(string itemid, string batchN)
        {
            String sql = "";
                sql = "SELECT substraction FROM Vaccine WHERE masterboxId IS NULL and (innerboxId = '" + itemid + "' OR innerBoxGsOneId = '" + itemid + "') " +
                      " and batchNumber = '" + batchN + "'";
            DataTable dtTmp = new DataTable();
            db.OpenQuery(out dtTmp, sql);
            int hasil;
            if (dtTmp.Rows.Count > 0)
            {
                    hasil = Convert.ToInt32(dtTmp.Rows[0][0]);
            }
            else
            {
                hasil = 0;
            }
            return hasil;
        }

        public string uomId = "";
        public string uom = "";
        public string tmpBasket = "";
        public string tmpInnerboxId = "";
        public string tmpInnerboxGsOneId = "";
        public bool cekProduct(string itemid)
        {
            if (cekBasket(itemid))
            {
                uom = UOM_BASKET;
                return true;
            }
            if (cekInnerbox(itemid))
            {
                uom = UOM_INNER_BOX;
                return true;
            }

            if (cekVial(itemid))
            {
                uom = UOM_VIAL;
                return true;
            }
            return false;
        }

        bool cekBasket(string itemid)
        {
            string sql;
            sql = "SELECT COUNT(CapId) FROM Vaccine WHERE basketId = '" + itemid + "'";
            DataTable dtTmp;
            db.OpenQuery(out dtTmp, sql);
            if (dtTmp.Rows.Count > 0)
                return Convert.ToInt32(dtTmp.Rows[0][0]) > 0;
            else
                return false;
        }

        bool cekInnerbox(string itemid)
        {
            string sql;
            sql = "SELECT COUNT(CapId), basketId FROM Vaccine WHERE innerBoxId = '" + itemid + "' OR innerBoxGsOneId = '" + itemid + "'" +
                    " GROUP BY basketId";
            DataTable dtTmp;
            db.OpenQuery(out dtTmp, sql);
            if (dtTmp.Rows.Count > 0)
            {
                tmpBasket = dtTmp.Rows[0][1].ToString();
                tmpInnerboxId = "";
                tmpInnerboxGsOneId = "";
                return Convert.ToInt32(dtTmp.Rows[0][0]) > 0;
            }
            else
            {
                tmpBasket = "";
                return false;
            }
        }

        bool cekVial(string itemid)
        {
            string sql;
            sql = "SELECT COUNT(CapId), basketId, innerBoxId, innerBoxGsOneId FROM Vaccine WHERE capId = '" + itemid + "' OR gsOneVialId = '" + itemid + "'" +
                    " GROUP BY basketId, innerBoxId, innerBoxGsOneId";
            DataTable dtTmp;
            db.OpenQuery(out dtTmp, sql);
            if (dtTmp.Rows.Count > 0)
            {
                tmpBasket = dtTmp.Rows[0][1].ToString();
                tmpInnerboxId = dtTmp.Rows[0][2].ToString();
                tmpInnerboxGsOneId = dtTmp.Rows[0][3].ToString();
                return Convert.ToInt32(dtTmp.Rows[0][0]) > 0;
            }
            else
            {
                tmpBasket = "";
                return false;
            }
        }

        public string getProductMasterBox(string doNumber)
        {
            string sql;
            sql = "SELECT DISTINCT d.model_name FROM shippingOrder_detail b " +
                    "INNER JOIN product_model d ON d.product_model = b.productModelId " + 
                    "WHERE b.shippingOrderNumber = '" + doNumber + "'";
            DataTable dtTmp = new DataTable();
            db.OpenQuery(out dtTmp, sql);
            string hasil;
            if (dtTmp.Rows.Count > 0)
                hasil = dtTmp.Rows[0][0].ToString();
            else
                hasil = "";
            return hasil;   
        }

        public bool cekQtyPickListFull(string itemid, string uom)
        {
            string sql = "";
            bool hasil = false;
            if (uom == UOM_INNER_BOX)
            {
                //sql = "select count(CapId) From Vaccine " +
                //        " where basketid in " +
                //            "(select distinct basketId from vaccine where innerboxid = '" + itemid + "' OR innerBoxGsOneId = '" + itemid + "') " +
                //        " AND masterBoxId IS NULL";
                sql = "select count(v1.CapId) From Vaccine v1 " +
                        " where exists " +
                        "(select distinct v2.basketId from vaccine v2 where (v2.innerboxid = '" + itemid + "' OR v2.innerBoxGsOneId = '" + itemid +"'"+
                        ") and v2.basketid = v1.basketid ) AND v1.masterBoxId IS NULL";
            }
            else if (uom == UOM_VIAL)
            {
                //sql = "select count(CapId) From Vaccine " +
                //        " where innerBoxGsOneId in " +
                //            "(select distinct innerBoxGsOneId from vaccine where capId = '" + itemid + "' OR gsOneVialId = '" + itemid + "') " +
                //        " AND masterBoxId IS NULL";
                sql = "select count(CapId) From Vaccine v1 " +
                        " where exists " +
                        "(select distinct v2.innerBoxGsOneId from vaccine v2 where (v2.capId = '" + itemid + "' OR v2.gsOneVialId = '" + itemid +"'"+
                        ") and v1.basketid = v2.basketid ) AND masterBoxId IS NULL";
            }
            DataTable dtTmp = new DataTable();
            db.OpenQuery(out dtTmp, sql);
            if (dtTmp.Rows.Count > 0)
            {
                if (Convert.ToInt32(dtTmp.Rows[0][0]) == 0)
                {
                    hasil = true;
                }
            }
            else
                hasil = false;
            return hasil;
        }

        public bool cekQtyPickListFullIn(string itemid, string uom)
        {
            string sql = "";
            bool hasil = false;
            if (uom == UOM_INNER_BOX)
            {
                sql = "select count(v1.CapId) From Vaccine v1 " +
                        " where exists " +
                        "(select distinct v2.basketId from vaccine v2 where (v2.innerboxid in (" + itemid + ") OR v2.innerBoxGsOneId in (" + itemid + ")" +
                        ") and v2.basketid = v1.basketid ) AND v1.masterBoxId IS NULL";
            }
            else if (uom == UOM_VIAL)
            {
                sql = "select count(CapId) From Vaccine v1 " +
                        " where exists " +
                        "(select distinct v2.innerBoxGsOneId from vaccine v2 where (v2.capId = '" + itemid + "' OR v2.gsOneVialId = '" + itemid + "'" +
                        ") and v1.basketid = v2.basketid ) AND masterBoxId IS NULL";
            }
            DataTable dtTmp = new DataTable();
            db.OpenQuery(out dtTmp, sql);
            if (dtTmp.Rows.Count > 0)
            {
                if (Convert.ToInt32(dtTmp.Rows[0][0]) == 0)
                {
                    hasil = true;
                }
            }
            else
                hasil = false;
            return hasil;
        }

        public string getBasketId(string itemid, string uom)
        {
            string sql = "";
            if (uom == UOM_INNER_BOX)
            {
                sql = "select distinct basketId From Vaccine " +
                        " where innerboxid = '" + itemid + "' OR innerBoxGsOneId = '" + itemid + "'";                            
            }
            else if (uom == UOM_VIAL)
            {
                sql = "select distinct basketId From Vaccine " +
                        " where capId = '" + itemid + "' OR gsOneVialId = '" + itemid + "'"; 
            }
            else if (uom == "Blister")
            {
                sql = "select distinct basketId From Vaccine " +
                        " where blisterpackId = '" + itemid + "'";
            }
            DataTable dtTmp = new DataTable();
            db.OpenQuery(out dtTmp, sql);
            string hasil;
            if (dtTmp.Rows.Count > 0)
                hasil = dtTmp.Rows[0][0].ToString();
            else
                hasil = "";
            return hasil;
        }

        public string getInnerboxId(string itemid, string uom)
        {
            string sql = "";
            if (uom == UOM_VIAL)
            {
                sql = "select distinct innerboxGsOneId From Vaccine " +
                        " where capId = '" + itemid + "' OR gsOneVialId = '" + itemid + "'";
            }
            DataTable dtTmp = new DataTable();
            db.OpenQuery(out dtTmp, sql);
            string hasil;
            if (dtTmp.Rows.Count > 0)
                hasil = dtTmp.Rows[0][0].ToString();
            else
                hasil = "";
            return hasil;
        }

        public void InsertSystemLog(string EventName) {
            db.eventname = EventName;
            db.systemlog();
        }

        public bool IsAdaMasterBox(string masterboxId)
        {
            string sql;
            DataTable dtTmp;
            sql = "SELECT count(m.masterboxid) FROM masterBox m WHERE m.masterboxId = '" + masterboxId + "'"+
                  " and not exists (select distinct v.masterboxid from vaccine v where v.masterboxid = m.masterboxid)";
            db.OpenQuery(out dtTmp, sql);
            return Convert.ToInt32(dtTmp.Rows[0][0]) > 0;
        }

    }
}
