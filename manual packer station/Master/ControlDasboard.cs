﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Net.NetworkInformation;
using System.Windows.Forms;
using System.Data;

namespace Manual_Packer_Station
{
    class ControlDasboard
    {
        public Dasboard wr;
        public sqlitecs sq;
        public dbaccess db;

        public ControlDasboard(Dasboard man)
        {
            wr = man;
            sq = new sqlitecs();
            db = new dbaccess();
        }

        public void close(string da)
        {
            if (wr.InvokeRequired)
            {
                wr.Invoke(new Action<string>(close), new object[] { da });
                return;
            }
        }
        

        private DataTable setDataTable(List<string[]> ds, DataTable dataheader)
        {
            DataTable datatemp = dataheader;
            foreach (string[] Row in ds)
            {
                DataRow row = datatemp.NewRow();
                for (int k = 0; k < Row.Length; k++)
                {
                        row[k] = Row[k];
                }
                datatemp.Rows.Add(row);
            }
            return datatemp;
        }

    }
}
