﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;

namespace Id_Shipment_Station
{
    class Dummy
    {
    //    public DataTable dtEkspedisi;
    //    public DataTable DtMaster;
    //    public DataTable DtDetail;

        public String Shipment1 = "SH-77DTEBDG25DG7";

        public Dummy()
        {
            //dtEkspedisi = new DataTable();
            //DtMaster = new DataTable();
            //DtDetail = new DataTable();
            //IsiDummyEkspedisi();
            //IsiDtMaster();
            //IsiDtDetail();
        }

        public void setDummyEkspedisi(out DataTable dtEkspedisi){
            dtEkspedisi = new DataTable();
            dtEkspedisi.Columns.Add("ID");
            dtEkspedisi.Columns.Add("EKSPEDISI");

            DataRow row = dtEkspedisi.NewRow();
            row["ID"] = "1";
            row["EKSPEDISI"] = "Easy GO";
            dtEkspedisi.Rows.Add(row);

            DataRow row2 = dtEkspedisi.NewRow();
            row2["ID"] = "2";
            row2["EKSPEDISI"] = "XYZ";
            dtEkspedisi.Rows.Add(row2);
        }

        public void setDummyMaster(out DataTable DtMaster)
        {
            DtMaster = new DataTable();
            DtMaster.Columns.Add("DO");
            DtMaster.Columns.Add("ALAMAT");
            DtMaster.Columns.Add("CUSTOMER");

            DataRow row = DtMaster.NewRow();
            row["DO"] = "DO-5F9078FBCB4FC";
            row["ALAMAT"] = "Banten";
            row["CUSTOMER"] = "Gudang Vaksin Dinkes Prov. Banten";            
            DtMaster.Rows.Add(row);
        }

        
        public void setDummyDetail(out DataTable DtDetail)
        {
            DtDetail = new DataTable();

            DtDetail.Columns.Add("DO");
            DtDetail.Columns.Add("GS1");
            DtDetail.Columns.Add("UOM");
            DtDetail.Columns.Add("QTY");

            DataRow row = DtDetail.NewRow();
            row["DO"] = "DO-5F9078FBCB4FC";
            row["GS1"] = "01899495700076710TES212017211021211C99FBE4B6DE";
            row["UOM"] = "Innser Box";
            row["QTY"] = "10";
            DtDetail.Rows.Add(row);

            DataRow row1 = DtDetail.NewRow();
            row1["DO"] = "DO-5F9078FBCB4FC";
            row1["GS1"] = "01899495700076710TES2120172110212112B6560CEBDA";
            row1["UOM"] = "Innser Box";
            row1["QTY"] = "10";
            DtDetail.Rows.Add(row1);

            DataRow row2 = DtDetail.NewRow();
            row2["DO"] = "DO-5F9078FBCB4FC";
            row2["GS1"] = "01899495700076710TES21201721102121AD95C741C31A";
            row2["UOM"] = "Innser Box";
            row2["QTY"] = "10";
            DtDetail.Rows.Add(row2);

            DataRow row3 = DtDetail.NewRow();
            row3["DO"] = "DO-5F9078FBCB4FC";
            row3["GS1"] = "01899495700076710TES2120172110212166C62A082D6E";
            row3["UOM"] = "Innser Box";
            row3["QTY"] = "10";
            DtDetail.Rows.Add(row3);

            DataRow row4 = DtDetail.NewRow();
            row4["DO"] = "DO-5F9078FBCB4FC";
            row4["GS1"] = "01899495700076710TES21201721102121DF644A4843E9";
            row4["UOM"] = "Innser Box";
            row4["QTY"] = "10";
            DtDetail.Rows.Add(row4);

            DataRow row5 = DtDetail.NewRow();
            row5["DO"] = "DO-5F9078FBCB4FC";
            row5["GS1"] = "01899495700076710TES212017211021211D9746F3BE90";
            row5["UOM"] = "Innser Box";
            row5["QTY"] = "10";
            DtDetail.Rows.Add(row5);

            DataRow row6 = DtDetail.NewRow();
            row6["DO"] = "DO-5F9078FBCB4FC";
            row6["GS1"] = "01899495700076710TES21201721102121D48A1056E77F";
            row6["UOM"] = "Innser Box";
            row6["QTY"] = "10";
            DtDetail.Rows.Add(row6);

            DataRow row7 = DtDetail.NewRow();
            row7["DO"] = "DO-5F9078FBCB4FC";
            row7["GS1"] = "01899495700076710TES21201721102121ACA06038F4E1";
            row7["UOM"] = "Innser Box";
            row7["QTY"] = "10";
            DtDetail.Rows.Add(row7);

            DataRow row8 = DtDetail.NewRow();
            row8["DO"] = "DO-5F9078FBCB4FC";
            row8["GS1"] = "01899495700076710TES212017211021214C73CFFE54C1";
            row8["UOM"] = "Innser Box";
            row8["QTY"] = "10";
            DtDetail.Rows.Add(row8);

            DataRow row9 = DtDetail.NewRow();
            row9["DO"] = "DO-5F9078FBCB4FC";
            row9["GS1"] = "01899495700076710TES2120172110212141A385102B24";
            row9["UOM"] = "Innser Box";
            row9["QTY"] = "10";
            DtDetail.Rows.Add(row9);
            
        }

        //private void IsiDummyEkspedisi()
        //{

        //    dtEkspedisi.Columns.Add("ID");
        //    dtEkspedisi.Columns.Add("EKSPEDISI");

        //    DataRow row = dtEkspedisi.NewRow();
        //    row["ID"] = "1";
        //    row["EKSPEDISI"] = "Ekspedisi 1";
        //    dtEkspedisi.Rows.Add(row);

        //    DataRow row2 = dtEkspedisi.NewRow();
        //    row2["ID"] = "2";
        //    row2["EKSPEDISI"] = "Ekspedisi 2";
        //    dtEkspedisi.Rows.Add(row2);

        //    DataRow row3 = dtEkspedisi.NewRow();
        //    row3["ID"] = "3";
        //    row3["EKSPEDISI"] = "Ekspedisi 3";
        //    dtEkspedisi.Rows.Add(row3);

        //}



        //private void IsiDtMaster()
        //{
        //    DtMaster.Columns.Add("DO");
        //    DtMaster.Columns.Add("ALAMAT");
            
        //    DataRow row = DtMaster.NewRow();            
        //    row["DO"] = "D0-10101010";
        //    row["ALAMAT"] = "ALAMAT 1";
        //    DtMaster.Rows.Add(row);

        //    DataRow row2 = DtMaster.NewRow();
        //    row2["DO"] = "D0-10101012";
        //    row2["ALAMAT"] = "ALAMAT 2";
        //    DtMaster.Rows.Add(row2);

        //    DataRow row3 = DtMaster.NewRow();
        //    row3["DO"] = "D0-10101013";
        //    row3["ALAMAT"] = "ALAMAT 3";
        //    DtMaster.Rows.Add(row3);
        //}

        //private void IsiDtDetail()
        //{
        //    DtDetail.Columns.Add("DO");
        //    DtDetail.Columns.Add("GS1");
        //    DtDetail.Columns.Add("UOM");
        //    DtDetail.Columns.Add("QTY");

        //    DataRow row = DtDetail.NewRow();
        //    row["DO"] = "D0-10101010";
        //    row["GS1"] = "GS00001";
        //    row["UOM"] = "INNER BOX";
        //    row["QTY"] = "10";
        //    DtDetail.Rows.Add(row);

        //    DataRow row2 = DtDetail.NewRow();
        //    row2["DO"] = "D0-10101010";
        //    row2["GS1"] = "GS00001";
        //    row2["UOM"] = "INNER BOX";
        //    row2["QTY"] = "10";
        //    DtDetail.Rows.Add(row2);
        //}


    }
}
