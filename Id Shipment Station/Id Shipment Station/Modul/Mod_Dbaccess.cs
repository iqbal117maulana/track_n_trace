﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Configuration;
using System.Windows.Forms;
using System.Data;
using System.IO;
using System.Diagnostics;
using System.Security.Cryptography;
using System.Net.Sockets;
using System.Net;
using System.Text.RegularExpressions;

namespace Id_Shipment_Station
{
    class dbaccess
    {
        public static SqlConnection Connection;
        public static SqlCommand Command;
        public static SqlDataReader DataReader;
        public static SqlDataAdapter dataadapter;
        public DataTable dataHeader;
        public int num_rows = 0;
        sqlitecs sqlite;
        public string eventname;
        public string eventType = "12";
        public string movementType = "4";
        public string from;
        public string uom;
        public string adminid;
        string eventtype;
        public int fcon = 0;
        public string errorMessage;
        String sql;
        
        public dbaccess()
        {
            sqlite = new sqlitecs();
            Connect();
        }

        public bool Connect()
        {
            try
            {
                string connec = "Data Source=" + sqlite.config["ipDB"] + ";" +
                                "Initial Catalog=" + sqlite.config["namaDB"] + ";" +
                                "User id=" + sqlite.config["usernameDB"] + ";" +
                                "Password=" + sqlite.config["passDB"] + ";";
                Connection = new SqlConnection(connec);
                Connection.Open();
                fcon = 0;
                errorMessage = "";
                return true;
            }
            catch (TimeoutException t)
            {
                Console.WriteLine("MASUK CATCH 1");
                fcon++;
                errorMessage = t.Message;
                if (fcon < 2)
                {
                    Console.WriteLine("MASUK CATCH 1 IF");
                    cGlobal.Inform("Can not open connection to Database Server.");
                    Config cfg = new Config(null);
                    cfg.ShowDialog();
                }
            }
            catch (SqlException se)
            {
                Console.WriteLine("MASUK CATCH 2 : " + se.Message);
                fcon++;
                Console.WriteLine("FCON : " + fcon);
                errorMessage = se.Message;
                if (fcon < 2)
                {
                    Console.WriteLine("MASUK CATCH 2 IF : " + se.Message);
                    errorMessage = se.Message;
                    cGlobal.Inform("Can not open connection to Database Server.");
                    Config cfg = new Config(null);
                    cfg.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("MASUK CATCH 3");
                fcon++;
                errorMessage = ex.Message;
                if (fcon < 2)
                {
                    Console.WriteLine("MASUK CATCH 3 IF");
                    cGlobal.Inform("Can not open connection to Database Server.");
                    simpanLog("Error SQLSERVER : " + ex.ToString());
                    Config cfg = new Config(null);
                    cfg.ShowDialog();
                }
            }
            return false;
            //}
        }

        public void closeConnection()
        {
            Connection.Close();
        }        

        public bool insert(Dictionary<string, string> field, string table)
        {
            try
            {
                string sql = "INSERT INTO " + table + " (";
                int i = 0;
                foreach (string key in field.Keys)
                {
                    sql += "" + key + "";
                    if (i + 1 < field.Count)
                    {
                        sql += ",";
                        i++;
                    }
                }

                sql += ") values (";

                i = 0;
                foreach (string key in field.Values)
                {
                    sql += "'" + key + "'";
                    if (i + 1 < field.Count)
                    {
                        sql += ",";
                        i++;
                    }
                }
                sql += ")";
                simpanLog(sql);
                Connect();
                Command = new SqlCommand(sql, Connection);
                Command.ExecuteNonQuery();
                Connection.Close();
                return true;
            }
            catch (SqlException sq)
            {
                simpanLog("Error SQl : " + sq.ToString());
                return false;
            }
            finally
            {
                Connection.Close();                
            }
        }

        public void update(Dictionary<string, string> field, string table, string where)
        {
            try
            {
                string sql = "UPDATE " + table + " SET ";
                int i = 0;
                foreach (KeyValuePair<string, string> key in field)
                {
                    if (key.Value.Equals("null"))
                        sql += key.Key + " = " + "" + key.Value + "";
                    else
                        sql += key.Key + " = " + "'" + key.Value + "'";
                    if (i + 1 < field.Count)
                    {
                        sql += ",";
                        i++;
                    }
                }

                if (where.Length > 0)
                    sql += " WHERE " + where;

                simpanLog(sql);
                Connect();
                Command = new SqlCommand(sql, Connection);
                Command.ExecuteNonQuery();
            }
            catch (SqlException sq)
            {
                simpanLog("Error SQl : " + sq.ToString());
            }
            finally
            {
                Connection.Close();
            }
        }

        public DataSet select(List<string> field, string table, string where)
        {
            try
            {
                string sql = "SELECT ";
                for (int j = 0; j < field.Count; j++)
                {
                    sql += "'" + field[j] + "'";
                    if (j + 1 < field.Count)
                    {
                        sql += ",";
                        j++;
                    }
                }

                sql += " From " + table;

                if (where.Length > 0)
                    sql += " WHERE " + where;

                simpanLog(sql);
                Connect();
                dataadapter = new SqlDataAdapter(sql, Connection);
                DataSet Results = new DataSet();
                dataadapter.Fill(Results);
                return Results;
            }
            catch (SqlException sq)
            {
                simpanLog("Error SQLSERVER : " + sq.ToString());
            }
            finally
            {
                try
                {
                    DataReader.Close();
                    Command.Dispose();
                    Connection.Close();
                }
                catch (SqlException sq)
                {
                    simpanLog("Error SQLSERVER : " + sq.ToString());
                }
                catch (NullReferenceException nl)
                {
                    simpanLog("Error SQLSERVER : " + nl.ToString());
                }

            }
            return null;
        }

        public List<String[]> selectList(List<string> field, string table, string where)
        {
            try
            {
                string sql = "SELECT ";
                for (int j = 0; j < field.Count; j++)
                {
                    sql += field[j];
                    if (j + 1 < field.Count)
                    {
                        sql += ",";
                    }
                }
                sql += " FROM " + table;

                if (where.Length > 0)
                    sql += " WHERE " + where;

                simpanLog(sql);
                List<String[]> Results = new List<String[]>();
                int num = 0;
                if (Connect())
                {
                    Command = new SqlCommand(sql, Connection);
                    DataReader = Command.ExecuteReader();
                    while (DataReader.Read())
                    {
                        string[] data = new string[DataReader.FieldCount];
                        for (int u = 0; u < DataReader.FieldCount; u++)
                        {
                            data[u] = DataReader.GetValue(u).ToString();
                        }

                        Results.Add(data);
                        num++;
                    }

                    num_rows = num;
                    dataHeader = new DataTable();
                    for (int i = 0; i < DataReader.FieldCount; i++)
                    {
                        dataHeader.Columns.Add(DataReader.GetName(i));
                    }
                }
                closeConnection();
                return Results;

            }
            catch (SqlException sq)
            {
                var st = new StackTrace(sq, true);
                var frame = st.GetFrame(0);
                string line = frame.ToString();
                simpanLog("Error  00100: " + line);
            }
            catch (NullReferenceException nl)
            {

                var st = new StackTrace(nl, true);
                var frame = st.GetFrame(0);
                string line = frame.ToString();
                simpanLog("Error  00100: " + line);
            }
            finally
            {
                try
                {
                    DataReader.Close();
                    Command.Dispose();
                    Connection.Close();
                }
                catch (SqlException sq)
                {
                    simpanLog("Error SQLSERVER : " + sq.ToString());
                }
                catch (NullReferenceException nl)
                {
                    simpanLog("Error SQLSERVER : " + nl.ToString());
                }

            }
            return null;
        }

        public int selectCount(String sql)
        {
            int Result = 0;
            try
            {
                simpanLog(sql);
                List<String[]> Results = new List<String[]>();
                int num = 0;
                if (Connect())
                {
                    Command = new SqlCommand(sql, Connection);
                    DataReader = Command.ExecuteReader();
                    while (DataReader.Read())
                    {
                        Result = int.Parse(DataReader.GetValue(0).ToString());
                        
                    }
                }
                closeConnection();
                return Result;

            }
            catch (SqlException sq)
            {
                var st = new StackTrace(sq, true);
                var frame = st.GetFrame(0);
                string line = frame.ToString();
                simpanLog("Error  00100: " + line);
            }
            catch (NullReferenceException nl)
            {

                var st = new StackTrace(nl, true);
                var frame = st.GetFrame(0);
                string line = frame.ToString();
                simpanLog("Error  00100: " + line);
            }
            finally
            {
                try
                {
                    DataReader.Close();
                    Command.Dispose();
                    Connection.Close();
                }
                catch (SqlException sq)
                {
                    simpanLog("Error SQLSERVER : " + sq.ToString());
                }
                catch (NullReferenceException nl)
                {
                    simpanLog("Error SQLSERVER : " + nl.ToString());
                }

            }
            return Result;
        }

        public static List<String> GetPODetails(String PO)
        {
            List<String> Results = new List<String>();
            String Query = String.Format("SELECT production_order_number, product_code, product_name, product_quantity, status FROM transaction_production_order WHERE production_order_number = '{0}'", PO);

            Connection.Open();
            Command = new SqlCommand(Query, Connection);
            DataReader = Command.ExecuteReader();

            while (DataReader.Read())
            {
                for (int i = 0; i < 5; i++)
                {
                    Results.Add(DataReader.GetValue(i).ToString());
                }
            }

            DataReader.Close();
            Command.Dispose();
            Connection.Close();

            return Results;
        }

        public void simpanLog(String line)
        {
            DateTime now = DateTime.Now;
            string tahun = now.ToString("yyyy");
            string bulan = now.ToString("MM");
            string hari = now.ToString("dd");
            string dates = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            line = dates + "\t" + line;
            // cek file 
            bool cekfile = false;
            while (!cekfile)
            {
                if (Directory.Exists("eventlog"))
                {
                    if (Directory.Exists("eventlog" + @"\" + tahun))
                    {
                        if (Directory.Exists("eventlog" + @"\" + tahun + @"\" + bulan))
                        {

                            using (StreamWriter outputFile = new StreamWriter("eventlog" + @"\" + tahun + @"\" + bulan + @"\" + hari + ".txt", true))
                            {
                                outputFile.WriteLine(line);
                            }
                            cekfile = true;
                        }
                        else
                        {

                            Directory.CreateDirectory("eventlog" + @"\" + tahun + @"\" + bulan);
                        }
                    }
                    else
                    {
                        Directory.CreateDirectory("eventlog" + @"\" + tahun);
                    }
                }
                else
                {

                    Directory.CreateDirectory("eventlog");
                }
            }
        }

        public string md5hash(string source)
        {
            using (MD5 md5Hash = MD5.Create())
            {
                string hash = GetMd5Hash(md5Hash, source);
                return hash;
            }
            return null;
        }

        static string GetMd5Hash(MD5 md5Hash, string input)
        {

            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }

        static bool VerifyMd5Hash(MD5 md5Hash, string input, string hash)
        {
            // Hash the input.
            string hashOfInput = GetMd5Hash(md5Hash, input);

            // Create a StringComparer an compare the hashes.
            StringComparer comparer = StringComparer.OrdinalIgnoreCase;

            if (0 == comparer.Compare(hashOfInput, hash))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            throw new Exception("Local IP Address Not Found!");
        }

        public bool cekRole(string adminid, string module)
        {
            List<string> field = new List<string>();
            field.Add("permission.[module]");
            string from = "[user] INNER JOIN user_role ON [user].role = user_role.role_id INNER JOIN permission ON permission.roleId = user_role.role_id";
            string where = "permission.[read] = '0' AND [user].userId = '" + adminid + "' AND permission.[module] = '" + module + "'";
            List<string[]> ds = selectList(field, from, where);
            if (num_rows > 0)
                return true;
            else
                return false;
        }
      
        public string getUnixString()
        {
            string date = "" + (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1)).TotalSeconds);
            return date;
        }
        
        public void systemlog()
        {
            Dictionary<string, string> field = new Dictionary<string, string>();
            field.Add("eventtime", getUnixString());
            field.Add("eventname", eventname);
            field.Add("eventtype", eventType);
            field.Add("userid", adminid);
            field.Add("[from]", from);
            insert(field, "system_log");
        }

        public void Movement(string order, string from, string to, string qty, string itemid)
        {
            string dates = DateTime.Now.ToString("ssMMddmmHHssyy");
            Dictionary<string, string> field = new Dictionary<string, string>();
            field.Add("movementId", dates);
            field.Add("orderNumber", order);
            field.Add("[from]", from);
            field.Add("movementType", movementType);
            field.Add("[to]", to);
            field.Add("qty", qty);
            field.Add("uom", uom);
            field.Add("userid", adminid);
            field.Add("itemid", itemid);
            field.Add("eventTime", getUnixString());
            insert(field, "movement_history");
        }
        
        internal bool cekPermision(string adminid, string modul_name, string permission)
        {
            List<string> field = new List<string>();
            field.Add("permissions_name");
            string where = "dbo.users.id = '" + adminid + "' AND dbo.permissions.module_name = '" + modul_name + "' AND dbo.permissions.permissions_name = '" + permission + "'";
            string from = "dbo.groups INNER JOIN dbo.permissions ON dbo.groups.id = dbo.permissions.role_id INNER JOIN dbo.users_groups ON dbo.users_groups.group_id = dbo.groups.id  INNER JOIN dbo.users ON dbo.users_groups.user_id = dbo.users.id";
            selectList(field, from, where);
            if (num_rows > 0)
                return true;
            return false;
        }

        public bool validasi(string username)
        {
            try
            {
                if (username.Length > 0)
                {
                    List<string> field = new List<string>();
                    field.Add("id");
                    string where = "username = '" + username + "'";
                    List<string[]> result = selectList(field, "[users]", where);
                    if (result.Count > 0)
                    {
                        return true;
                    }
                    else
                        return false;
                }
                else
                {
                    return false;
                }
            }
            catch (NullReferenceException nl)
            {
                simpanLog("Error  00100: " + nl.Message);
                return false;
            }

        }

        public string valid;
        public bool validasi(string data, int val)
        {
            //text ip val = 0
            if (val == 0)
                return cekIP(data);
            else if (val == 1)
            {
                //text tidak kosong val =1
                if (data.Length == 0)
                {
                    valid = "Data kosong";
                    return false;
                }
            }
            else if (val == 2)
            {
                //text tidak ada angka
                int errorCounter = Regex.Matches(data, @"[a-zA-Z]").Count;
                if (errorCounter > 0)
                {
                    valid = "data mengandung angka";
                    return false;
                }
            }
            else if (val == 3)
            {
                //text tidak ada angka

                if (data.Length > 6)
                    return false;
                bool rege = Regex.Match(data, @"^[0-9]+$").Success;
                if (!rege)
                {
                    valid = "data mengandung angka";
                    return false;
                }
            }
            return true;
        }
 
        public List<string[]> validasi(string username, string password, string status)
        {
            try
            {
                if (username.Length > 0 && password.Length > 0)
                {
                    List<string> field = new List<string>();
                    field.Add("id");
                    string where = "username = '" + username + "' AND password = '" + md5hash(password) + "' AND Active = " + status;
                    List<string[]> result = selectList(field, "[users]", where);
                    if (result.Count > 0)
                    {
                        from = "Bottle Station";
                        adminid = result[0][0];
                        eventType = "1";
                        eventname = "Login";
                        systemlog();
                        eventType = "12";
                        return result;
                    }
                    else
                        return null;
                }
                else
                {
                    string code = "0010";
                    simpanLog(code + " : USERNAME DAN PASSWORD SALAH");
                    return null;
                }
            }
            catch (NullReferenceException nl)
            {
                var st = new StackTrace(nl, true);
                var frame = st.GetFrame(0);
                string line = frame.ToString();
                simpanLog("Error  00100: " + line);
                return null;
            }

        }

        public bool validasi(string username, string password)
        {
            try
            {
                if (username.Length > 0 && password.Length > 0)
                {
                    List<string> field = new List<string>();
                    field.Add("id");
                    string where = "username = '" + username + "' AND password = '" + md5hash(password) + "'";
                    List<string[]> result = selectList(field, "[users]", where);
                    if (result.Count > 0)
                    {
                        return true;
                    }
                    else
                        return false;
                }
                else
                {
                    string code = "0010";
                    simpanLog(code + " : USERNAME DAN PASSWORD SALAH");
                    return false;
                }
            }
            catch (NullReferenceException nl)
            {
                var st = new StackTrace(nl, true);
                var frame = st.GetFrame(0);
                string line = frame.ToString();
                simpanLog("Error  00100: " + line);
                return false;
            }

        }
        static string valid1;
        internal static bool cekIP(string data)
        {
            if (data.Length < 3 & data.Length > 0)
                return false;
            int errorCounter = Regex.Matches(data, @"[a-zA-Z]").Count;
            if (errorCounter == 0)
            {
                string[] dapat = data.Split('.');
                if (dapat.Length != 4)
                {
                    valid1 = "Error : IP must 4 point";
                    return false;
                }
                if (dapat[3].Equals("0"))
                {
                    valid1 = "Error : the fourth digit of ip cannot be 0";
                    return false;
                }

                bool kosong = false;
                if (dapat[0].Equals("000"))
                    kosong = true;
                for (int i = 0; i < dapat.Length; i++)
                {                    
                    if (dapat[i][0].Equals("0"))
                        return false;
                    if (dapat[i].Equals("00"))
                    {
                        valid1 = "Error : IP cannot fill 00";
                        return false;
                    }

                    bool rege = Regex.Match(dapat[i], @"^[0-9]+$").Success;
                    if (!rege)
                    {
                        valid1 = "Error : IP cannot contain alphabeth and symbols " + data;
                        return false;
                    }
                    if (dapat[i].Length == 0)
                    {
                        valid1 = "Error : Cannot empty";
                        return false;
                    }
                    int temp = int.Parse(dapat[i]);
                    if (temp >= 255)
                    {
                        valid1 = "Error : IP must be less then 255";
                        return false;
                    }
                    if (temp == 0 && kosong)
                    {
                        valid1 = "Error : First digit of ip cannot be 0";
                        return false;
                    }
                    if (dapat[i].Equals("000") && !kosong)
                    {
                        valid1 = "Error : IP cannot fill 000";
                        return false;
                    }
                    if (dapat[i][0].Equals('0') && temp != 0 && dapat[i].Length > 1)
                    {
                        valid1 = "Error : Ip cannot fill '0'";
                        return false;
                    }
                }
                return true;
            }
            else
            {
                valid1 = "Error : IP cannot contain alphabeth " + errorCounter;
                return false;
            }
            return true;
        }

        public void OpenQuery(out DataTable data , String sql)
        {
            data = new DataTable();
            try
            {
                
                simpanLog(sql);                
                if (Connect())
                {
                    
                    Command = new SqlCommand(sql, Connection);
                    DataReader = Command.ExecuteReader();
                    data.Load(DataReader);

                }
                closeConnection();                

            }
            catch (SqlException sq)
            {
                var st = new StackTrace(sq, true);
                var frame = st.GetFrame(0);
                string line = frame.ToString();
                simpanLog("Error  00100: " + line);
            }
            catch (NullReferenceException nl)
            {

                var st = new StackTrace(nl, true);
                var frame = st.GetFrame(0);
                string line = frame.ToString();
                simpanLog("Error  00100: " + line);
            }
            finally
            {
                try
                {
                    DataReader.Close();
                    Command.Dispose();
                    Connection.Close();
                }
                catch (SqlException sq)
                {
                    simpanLog("Error SQLSERVER : " + sq.ToString());
                }
                catch (NullReferenceException nl)
                {
                    simpanLog("Error SQLSERVER : " + nl.ToString());
                }

            }            
        }

        public bool CekShippingOrder(String ScanShipping)
        {
            sql = "SELECT COUNT(*) FROM dbo.shippingOrder " +
                  "WHERE shippingOrderNumber = '" + ScanShipping + "'";
            DataTable dt = new DataTable();
            OpenQuery(out dt, sql);
            return dt.Rows.Count > 0;

        }


        public DataTable GetShippingOrder(String shippingOrderNumber)
        {
            sql =   "SELECT m.shippingOrderNumber AS DO, m.customerAddress AS ALAMAT, customerName AS CUSTOMER " +
                    "FROM shippingOrder m , shippingOrder_detail d " +
                    "WHERE m.shippingOrderNumber = d.shippingOrderNumber AND m.shippingType = 2 AND m.flag = 2 AND m.status = 1 "+
                    "AND m.shippingOrderNumber = '" + shippingOrderNumber + "'";
             DataTable dt = new DataTable();
             OpenQuery(out dt, sql);
             return dt;
        }

        public DataTable GetShippingOrderDetail(String shippingOrderNumber)
        {

            sql = "SELECT p.itemId AS GS1, p.uom AS UOM, p.qty AS QTY " +
                  "FROM pickingList p, shippingOrder sm, shippingOrder_detail sd " +
                  "WHERE p.detailNo = sd.shippingOrderDetailNo AND sm.shippingOrderNumber = sd.shippingOrderNumber " +
                  "AND sm.shippingType = 2 AND sm.flag = 2 AND sm.status = 1 AND sm.shippingOrderNumber = '" + shippingOrderNumber + "'";
            DataTable dt = new DataTable();
            OpenQuery(out dt, sql);
            return dt;
        }
    }
}