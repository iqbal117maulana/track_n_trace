﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Id_Shipment_Station
{
    class Printer
    {
        static tcp tcp = new tcp();
        static sqlitecs sql = new sqlitecs();
        
        private static String ZplTemplateIdShip = "CT~~CD,~CC^~CT~/n" +
            "^XA"
            + "^FO135,40"
            + "^BXN,9,200,,,,,"
            + "^FD{0}^FS"
            + "^FT145,22^A0N20,20^FDID SHIPMENT^FS"
            + "^FO155,25^A0N,13,13^FH^FD{0}^FS"
            + "^XZ";

        public static void PrintIDShipment(
            String NoShipment)
        {
            List<object> val = new List<object>();
            val.Add(NoShipment);
            String ZplString = String.Format(ZplTemplateIdShip, val[0]);

            tcp = new tcp();
            tcp.Connect(sql.config["ipprinter"], sql.config["portprinter"], sql.config["timeout"]);
            tcp.send(ZplString);
            tcp.dc();
        }

    }
}
