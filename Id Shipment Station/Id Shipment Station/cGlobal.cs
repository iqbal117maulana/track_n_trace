﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Id_Shipment_Station
{
    public static class cGlobal
    {

        public static void Inform(String Message)
        {
            DialogResult msgBox = MessageBox.Show(Message, "Information", MessageBoxButtons.OK);
        }

        public static bool Confirm(String Message)
        {
            DialogResult msgBox = MessageBox.Show(Message, "Confirmation", MessageBoxButtons.YesNo);
            return msgBox == DialogResult.Yes;
        }

    }
}
