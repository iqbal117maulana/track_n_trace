﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Id_Shipment_Station
{
    public partial class ListKendaraan : Form
    {
        dbaccess db;
        DataTable dtDetail;
        String shippingOrderNumber;
        public String NoKendaraan;
        public ListKendaraan()
        {
            InitializeComponent();            
            InitData();
            LoadData();
            NoKendaraan = "";
        }

        private void IsiDummyKendaraan() {

            dtDetail.Columns.Add("NO");
            dtDetail.Columns.Add("NO_POL");

            DataRow row = dtDetail.NewRow();            
            row["NO_POL"] = "D 9090 KR";
            dtDetail.Rows.Add(row);

            DataRow row2 = dtDetail.NewRow();
            row2["NO_POL"] = "D 8765 MK";
            dtDetail.Rows.Add(row2);

            DataRow row3 = dtDetail.NewRow();
            row3["NO_POL"] = "D 2463 CMD";
            dtDetail.Rows.Add(row3);
        }

        private void InitData(){            

            db = new dbaccess();
            dtDetail = new DataTable();
            dgvDetail.DataSource = dtDetail;
            dgvDetail.AutoGenerateColumns = false;

            dgvDetail.Columns["NO"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            dgvDetail.Columns["NO_POL"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;            
        }

        private void LoadData(){
            dtDetail = new DataTable();
            IsiDummyKendaraan();            
            dgvDetail.AutoGenerateColumns = false;
            dgvDetail.DataSource = dtDetail;

        }

        private void dgvDetail_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            

        }

        private void dgvDetail_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                dgvDetail.Rows[e.RowIndex].Cells["NO"].Value = e.RowIndex + 1;
            }
        }

        private void dgvDetail_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                NoKendaraan = dgvDetail.Rows[e.RowIndex].Cells["NO_POL"].Value.ToString();
                this.DialogResult = DialogResult.Cancel;
            }
        }        
    }
}
