﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Id_Shipment_Station
{
    public partial class DetailShipment : Form
    {
        dbaccess db;
        DataTable dtDetail;
        String shippingOrderNumber;

        Dummy dm;

        DataTable dtD0Dummy;
        DataTable dtDetailDummy;

        public DetailShipment(String shippingOrderNumber)
        {
            InitializeComponent();
            this.shippingOrderNumber = shippingOrderNumber;
            dm = new Dummy();
            InitData();
            LoadDataDummy();
        }

        private void InitData(){
            lblDO.Text = "-";
            lblCustomer.Text = "-";
            lblAlamat.Text = "-";

            db = new dbaccess();
            dtDetail = new DataTable();
            dgvDetail.DataSource = dtDetail;
            dgvDetail.AutoGenerateColumns = false;

            dgvDetail.Columns["NO"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            dgvDetail.Columns["GS1"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dgvDetail.Columns["UOM"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dgvDetail.Columns["QTY"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
        }

        private void LoadData(){
            DataTable DtMaster = new DataTable();
            DtMaster = db.GetShippingOrder(shippingOrderNumber);
            
            if (DtMaster.Rows.Count > 0)
            {
                lblDO.Text = DtMaster.Rows[0]["DO"].ToString();
                lblCustomer.Text = DtMaster.Rows[0]["CUSTOMER"].ToString();
                lblAlamat.Text = DtMaster.Rows[0]["ALAMAT"].ToString();
            }

            dtDetail = db.GetShippingOrderDetail(shippingOrderNumber);
            dgvDetail.AutoGenerateColumns = false;
            dgvDetail.DataSource = dtDetail;
            

        }

        
        private void LoadDataDummy(){

            dm.setDummyMaster(out dtD0Dummy);
            DataRow[] foundRow = dtD0Dummy.Select("[DO]='" + shippingOrderNumber + "'");

            
            DataTable DtMaster = new DataTable();
            DtMaster.Columns.Add("DO");
            DtMaster.Columns.Add("CUSTOMER");
            DtMaster.Columns.Add("ALAMAT");
            DtMaster.ImportRow(foundRow[0]);
 
            
            if (DtMaster.Rows.Count > 0)
            {
                lblDO.Text = DtMaster.Rows[0]["DO"].ToString();
                lblCustomer.Text = DtMaster.Rows[0]["CUSTOMER"].ToString();
                lblAlamat.Text = DtMaster.Rows[0]["ALAMAT"].ToString();
            }



            dm.setDummyDetail(out dtDetailDummy);
            DataRow[] foundRowDet = dtDetailDummy.Select("[DO]='" + shippingOrderNumber + "'");


            dtDetail = new DataTable();
            dtDetail.Columns.Add("DO");
            dtDetail.Columns.Add("GS1");
            dtDetail.Columns.Add("UOM");
            dtDetail.Columns.Add("QTY");

            for (int i = 0; i < foundRowDet.Length; i++)
            {
                dtDetail.ImportRow(foundRowDet[i]);
            }

            
            dgvDetail.AutoGenerateColumns = false;
            dgvDetail.DataSource = dtDetail;

        }

          
        private void dgvDetail_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                dgvDetail.Rows[e.RowIndex].Cells["NO"].Value = e.RowIndex + 1;
            }
        }
    }
}
