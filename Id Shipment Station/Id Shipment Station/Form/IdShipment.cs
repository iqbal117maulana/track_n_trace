﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Id_Shipment_Station
{
    public partial class IdShipment : Form
    {
        dbaccess db;
        DataTable dtD0;
        DataTable dtD0Dummy;
        DataTable dtEkspedisi;
        DataTable dtShipment;
        int IdExpedisi = 0;
        int IdKendaraan = 0;
        String LogFile = "";

        private static String STATUS_START = "Start";
        private static String STATUS_STOP = "Stop";

        private Dummy dm;

        public IdShipment(string admin)
        {
            InitializeComponent();
            dm = new Dummy();
            InitData();
            SetStart(false);
        }

       

        private void ScanShippmentDummy()
        {
            if (txtDO.Text == "")
            {
                txtDO.Text = "";
                return;
            }

            if (DoIsExists(txtDO.Text))
            {
                log("DO sudah ada");
                cGlobal.Inform("DO sudah ada");
                txtDO.Text = "";
                return;
            }


            DataRow[] foundRow = dtD0Dummy.Select("[DO]='" + txtDO.Text + "'");


            if (foundRow.Length <= 0)
            {
                log("DO tidak ditemukan");
                cGlobal.Inform("DO tidak ditemukan");
                txtDO.Text = "";
                return;

            }

            DataTable dt = new DataTable();
            dt.Columns.Add("DO");
            dt.Columns.Add("ALAMAT");
            dt.ImportRow(foundRow[0]);
 

            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dtRow = dtD0.NewRow();
                    dtRow = dt.Rows[i];
                    dtD0.ImportRow(dtRow);
                }

                lblQtyScanned.Text = dtD0.Rows.Count.ToString();
                db.simpanLog(txtDO.Text +"berhasil discan");
            }
            else
            {
                log("DO tidak ditemukan");
                cGlobal.Inform("DO tidak ditemukan");
            }
            dgvDO.AutoGenerateColumns = false;
            dgvDO.DataSource = dtD0;
            txtDO.Text = "";
        }

        private void PrintDummy()
        {
            if (dtD0.Rows.Count <= 0){
                cGlobal.Inform("Belum ada DO");
                return;
            }

            dtShipment = new DataTable();
            dtShipment.Columns.Add("ID_SHIPMENT");
            dtShipment.Columns.Add("QTY");                        

            DataRow row = dtShipment.NewRow();
            row["ID_SHIPMENT"] = dm.Shipment1;
            row["QTY"] = hitungQtyDO();
            dtShipment.Rows.Add(row);

            dgvShipment.AutoGenerateColumns = false;
            dgvShipment.DataSource = dtShipment;

            dgvShipment.Columns["ID_SHIPMENT"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            dgvShipment.Columns["QTY"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;

            lblQtyShipment.Text = dtD0.Rows.Count.ToString();

        }

        private int hitungQtyDO(){

            DataTable dtDetailDummy = new DataTable();
            dm.setDummyDetail(out dtDetailDummy);

            int qty = 0;

            for (int i = 0; i < dtD0.Rows.Count; i++ )            {

                String NoDO = dtD0.Rows[i]["DO"].ToString();



                DataRow[] foundRowDet = dtDetailDummy.Select("[DO]='" + NoDO + "'");

                foreach(DataRow row in foundRowDet)
                {
                    qty = qty + Convert.ToInt32(row["QTY"]);
                }

            }
            return qty;

        }

        /// ATAS DUMMY
        /// ////////////////////////////////////////////////////////////////// ///////////////////////////////////////////////////////////////
        

        private void InitData()
        {
            db = new dbaccess();
            dtD0 = new DataTable();
            dtEkspedisi = new DataTable();
            dtShipment = new DataTable();
            int IdExpedisi = 0;
            int IdKendaraan = 0;
            
            cmbExpedisi.SelectedIndex = -1;
            txtDO.Text = "";
            txtKendaraan.Text = "";
            


            //EKSPEDISI========================================
            //Data Ekspedisi percobaan
            //dtEkspedisi.Columns.Add("ID");
            //dtEkspedisi.Columns.Add("EKSPEDISI");
            //for (int i = 0; i < dm.dtEkspedisi.Rows.Count; i++)
            //{
            //    DataRow dtRow = dtEkspedisi.NewRow();
            //    dtRow = dm.dtEkspedisi.Rows[i];
            //    dtEkspedisi.ImportRow(dtRow);
            //}

            dm.setDummyEkspedisi(out dtEkspedisi);
            
            cmbExpedisi.DataSource = dtEkspedisi;
            cmbExpedisi.DisplayMember = "EKSPEDISI";
            cmbExpedisi.ValueMember = "ID";            
            cmbExpedisi.SelectedIndex = -1;           
            //EKSPEDISI========================================

            dm.setDummyMaster(out dtD0Dummy);


            dtD0 = db.GetShippingOrder("");
            dgvDO.AutoGenerateColumns = false;
            dgvDO.DataSource = dtD0;
            dgvDO.Columns["NO"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;            
            
        }

        private void txtDO_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                //ScanShippment();
                ScanShippmentDummy();
            }
        }

        private void ScanShippment() {
            if (txtDO.Text == ""){
                cGlobal.Inform("Empty");
                txtDO.Text = "";
                return;
            }

            if (DoIsExists(txtDO.Text))
            {
                cGlobal.Inform("DO sudah ada");
                txtDO.Text = "";
                return;
            }

            DataTable dt = new DataTable();
            dt = db.GetShippingOrder(txtDO.Text);

            
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i<dt.Rows.Count; i++){
                    DataRow dtRow = dtD0.NewRow();
                    dtRow = dt.Rows[i];                    
                    dtD0.ImportRow(dtRow);
                }

                lblQtyScanned.Text = dtD0.Rows.Count.ToString();
            }
            else
            {
                cGlobal.Inform("Not found");
            }
            dgvDO.AutoGenerateColumns = false;
            dgvDO.DataSource = dtD0;
            txtDO.Text = "";
        }

        private bool DoIsExists(String shippingOrderNumber)
        {

            for (int i = 0; i<dtD0.Rows.Count; i++)
            {
                if (dtD0.Rows[i]["DO"].ToString() == shippingOrderNumber)
                {
                    return true;
                }
            }
            return false;

        }

        private void SetStart(bool Status) {
            if (Status){
                //if (IdExpedisi <= 0)
                //{
                //    cGlobal.Inform("Expedisi belum dipilih");
                //    return;
                //}

                //if (IdKendaraan <= 0)
                //{
                //    cGlobal.Inform("Kendaraan belum dipilih");
                //    return;
                //}

                if (cmbExpedisi.SelectedIndex <= -1)
                {
                    cGlobal.Inform("Expedisi belum dipilih");
                    return;
                }

                if (txtKendaraan.Text == "")
                {
                    cGlobal.Inform("Kendaraan belum dipilih");
                    return;
                }

                btnStart.Text = STATUS_STOP;
            }
            else
            {
                btnStart.Text = STATUS_START;
                InitData();                
            }
            btnPrintDO.Enabled = Status;
            txtKendaraan.Enabled = !Status;
            txtScanned.Enabled = Status;
            cmbExpedisi.Enabled = !Status;
            txtDO.Enabled = Status;
        }


        private void splitContainer1_Panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void splitContainer1_SplitterMoved(object sender, SplitterEventArgs e)
        {

        }

        private void dgvDO_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            if (e.RowIndex > -1) {
                dgvDO.Rows[e.RowIndex].Cells["NO"].Value = e.RowIndex + 1;
            }
        }

        private void txtScanned_TextChanged(object sender, EventArgs e)
        {

        }

        private void dgvDO_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                Form frm = new DetailShipment(dgvDO.Rows[e.RowIndex].Cells["DO"].Value.ToString());
                frm.ShowDialog();
            }
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            SetStart(btnStart.Text == STATUS_START);
        }

        private void btnConfig_Click(object sender, EventArgs e)
        {
            Config cfg = new Config(this);
            cfg.ShowDialog();
        }

        private void btnLogout_Click(object sender, EventArgs e)
        {
            if (cGlobal.Confirm("Are you sure? "))
            {
                this.Dispose();
                Login lg = new Login();
                lg.Show();
            }
            
        }

        private void btnOpenLog_Click(object sender, EventArgs e)
        {
            Log frm;
            frm = new Log(LogFile);
            frm.ShowDialog();
            if (frm.isClear == true)
                LogFile = "";
        }

        //tambahan disable close button
        private const int CP_NOCLOSE_BUTTON = 0x200;
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams myCp = base.CreateParams;
                myCp.ClassStyle = myCp.ClassStyle | CP_NOCLOSE_BUTTON;
                return myCp;
            }
        }

        private void txtKendaraan_Click(object sender, EventArgs e)
        {             
            ListKendaraan frm = new ListKendaraan();
            frm.ShowDialog();
            txtKendaraan.Text = frm.NoKendaraan;
        }

        private void btnPrintDO_Click(object sender, EventArgs e)
        {
            PrintDummy();
            Printer.PrintIDShipment(dm.Shipment1);
        }

        public void log(string data)
        {
            try
            {
                string dates = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                string simpan = dates + "\t" + data + "\n";
                LogFile = LogFile + simpan + Environment.NewLine;
                db.simpanLog(simpan);
            }
            catch (ObjectDisposedException ob)
            {

            }
        }
    }
}

