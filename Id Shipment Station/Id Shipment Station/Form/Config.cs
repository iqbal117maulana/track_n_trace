﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Id_Shipment_Station
{
    public partial class Config : Form
    {
        IdShipment bot;        
        public Config(IdShipment bt)
        {
            InitializeComponent();
            bot = bt;
            getConfig();
        }

        void getConfig()
        {
            sqlitecs sql = new sqlitecs();
            try
            {
                txtIpPrinter.Text = sql.config["ipprinter"];
                txtPortPrinter.Text = sql.config["portprinter"];
                txtPortServer.Text = sql.config["portserver"];
                txtIpServer.Text = sql.config["ipServer"];
                txtIpDB.Text = sql.config["ipDB"];
                txtPortDb.Text = sql.config["portDB"];
                txtTimeoutDb.Text = sql.config["timeout"];
                txtIpCamera2.Text = sql.config["ipCamera"];
                txtPortCamera2.Text = sql.config["portCamera"];
            }
            catch (KeyNotFoundException k)
            {
              //  sql.simpanLog("Data Tidak Ditemukan : " + k.ToString());
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            bool hasilDialog;
            hasilDialog = cGlobal.Confirm("Are you sure? ");            
            if (hasilDialog)
            {
                if (validate())
                {

                    sqlitecs sql = new sqlitecs();
                    Dictionary<string, string> field = new Dictionary<string, string>();
                    field.Add("ipPrinter", txtIpPrinter.Text);
                    field.Add("portPrinter", "" + int.Parse(txtPortPrinter.Text));
                    field.Add("portserver", "" + int.Parse(txtPortServer.Text));
                    field.Add("ipServer", txtIpServer.Text);
                    field.Add("ipDB", txtIpDB.Text);
                    field.Add("portDB", "" + int.Parse(txtPortDb.Text));
                    field.Add("timeout", txtTimeoutDb.Text);
                    sql.update(field, "config", "");
                    cGlobal.Inform("Configuration success saved");
                    this.Dispose();
                }
                else
                {
                    cGlobal.Inform("Invalid configuration data");
                }
            }

        }

        private bool validate()
        {
            dbaccess db = new dbaccess();
            string kesalahan = "Error Input from : ";
            bool kembali = true;
            
            if (!db.validasi(txtIpDB.Text, 0))
            {
                kesalahan = kesalahan + txtIpDB.Text + "\n";
                kembali = false;
            }


            if (!db.validasi(txtIpPrinter.Text, 0))
            {
                kesalahan = kesalahan + txtIpPrinter.Text + "\n";
                kembali = false;
            }

            if (!db.validasi(txtIpServer.Text, 0))
            {
                kesalahan = kesalahan + txtIpServer.Text + "\n";
                kembali = false;
            }
            
            if (!db.validasi(txtPortDb.Text, 3))
            {
                kesalahan = kesalahan + txtPortDb.Text + "\n";
                kembali = false;
            }

            if (!db.validasi(txtPortPrinter.Text, 3))
            {
                kesalahan = kesalahan + txtPortPrinter.Text + "\n";
                kembali = false;
            }

            if (!db.validasi(txtPortServer.Text, 3))
            {
                kesalahan = kesalahan + txtPortServer.Text + "\n";
                kembali = false;
            }

            if (!kembali)
                cGlobal.Inform(kesalahan);
            return kembali;
        }

    }
}
