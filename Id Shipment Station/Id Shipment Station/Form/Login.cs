﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


namespace Id_Shipment_Station
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }

        private bool Validasi()
        {
            if (txtUsername.Text == "")
            {
                cGlobal.Inform("Username cannot empty");
                return false;
            }

            if (txtPassword.Text == "")
            {
                cGlobal.Inform("Password cannot empty");
                return false;
            }

            return true;
        }

        private void buttonLogin_Click(object sender, EventArgs e)
        {
            do_login();            
        }

        private void do_login()
        {
            if (Validasi())
            {
                dbaccess db = new dbaccess();
                try
                { //cek useraname
                    if (db.validasi(txtUsername.Text))
                    {
                        //cek Password
                        if (db.validasi(txtUsername.Text, txtPassword.Text))
                        {
                            List<string[]> res = db.validasi(txtUsername.Text, txtPassword.Text, "0");
                            if (res != null)
                            {

                                string adminid = res[0][0];
                                IdShipment frm = new IdShipment(adminid);
                                frm.Show();
                                this.Hide();

                                //string adminid = res[0][0];
                                //if (db.cekPermision(adminid, "id_shipment", "read"))
                                //{
                                //    IdShipment frm = new IdShipment(adminid);
                                //    frm.Show();
                                //    this.Hide();

                                //}
                                //else
                                //{                                    
                                //    cGlobal.Inform("Dont have permission");
                                //}

                            }
                            else
                            {
                                cGlobal.Inform("User is inactive");                                
                            }
                        }
                        else
                        {
                            cGlobal.Inform("Invalid Password");                            
                            db.simpanLog("Error 0010 : Username dan Password Salah !");
                            
                        }
                    }
                    else
                    {
                        cGlobal.Inform("Invalid Username");                        
                        db.simpanLog("Error 0010 : Username Salah !");
                        txtUsername.Focus();                        
                    }
                }
                catch (ArgumentException ae)
                {
                    db.simpanLog("Error " + ae.ToString());
                }
            }
        }

        private void Login_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }
    }
}
