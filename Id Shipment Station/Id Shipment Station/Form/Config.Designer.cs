﻿namespace Id_Shipment_Station
{
    partial class Config
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtPortCamera2 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtIpCamera2 = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtPortDb = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtIpDB = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtTimeoutDb = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.txtPortPrinter = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtIpPrinter = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtPortServer = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtIpServer = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txtPortCamera2
            // 
            this.txtPortCamera2.Location = new System.Drawing.Point(239, 400);
            this.txtPortCamera2.Name = "txtPortCamera2";
            this.txtPortCamera2.Size = new System.Drawing.Size(100, 20);
            this.txtPortCamera2.TabIndex = 45;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(164, 403);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(65, 13);
            this.label11.TabIndex = 54;
            this.label11.Text = "Port Camera";
            // 
            // txtIpCamera2
            // 
            this.txtIpCamera2.Location = new System.Drawing.Point(239, 374);
            this.txtIpCamera2.Name = "txtIpCamera2";
            this.txtIpCamera2.Size = new System.Drawing.Size(100, 20);
            this.txtIpCamera2.TabIndex = 43;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(164, 377);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(56, 13);
            this.label14.TabIndex = 53;
            this.label14.Text = "IP Camera";
            // 
            // txtPortDb
            // 
            this.txtPortDb.Location = new System.Drawing.Point(88, 90);
            this.txtPortDb.Name = "txtPortDb";
            this.txtPortDb.Size = new System.Drawing.Size(100, 20);
            this.txtPortDb.TabIndex = 47;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(13, 93);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(44, 13);
            this.label8.TabIndex = 52;
            this.label8.Text = "Port DB";
            // 
            // txtIpDB
            // 
            this.txtIpDB.Location = new System.Drawing.Point(88, 64);
            this.txtIpDB.Name = "txtIpDB";
            this.txtIpDB.Size = new System.Drawing.Size(100, 20);
            this.txtIpDB.TabIndex = 46;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(13, 67);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(35, 13);
            this.label9.TabIndex = 51;
            this.label9.Text = "IP DB";
            // 
            // txtTimeoutDb
            // 
            this.txtTimeoutDb.Location = new System.Drawing.Point(88, 116);
            this.txtTimeoutDb.Name = "txtTimeoutDb";
            this.txtTimeoutDb.Size = new System.Drawing.Size(100, 20);
            this.txtTimeoutDb.TabIndex = 48;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(13, 119);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(63, 13);
            this.label10.TabIndex = 50;
            this.label10.Text = "Timeout DB";
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.button1.Image = global::Id_Shipment_Station.Properties.Resources.submit;
            this.button1.Location = new System.Drawing.Point(16, 205);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(172, 50);
            this.button1.TabIndex = 49;
            this.button1.Text = "Submit";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtPortPrinter
            // 
            this.txtPortPrinter.Location = new System.Drawing.Point(88, 170);
            this.txtPortPrinter.Name = "txtPortPrinter";
            this.txtPortPrinter.Size = new System.Drawing.Size(100, 20);
            this.txtPortPrinter.TabIndex = 41;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 173);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(59, 13);
            this.label5.TabIndex = 44;
            this.label5.Text = "Port Printer";
            // 
            // txtIpPrinter
            // 
            this.txtIpPrinter.Location = new System.Drawing.Point(88, 144);
            this.txtIpPrinter.Name = "txtIpPrinter";
            this.txtIpPrinter.Size = new System.Drawing.Size(100, 20);
            this.txtIpPrinter.TabIndex = 40;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 147);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(50, 13);
            this.label4.TabIndex = 42;
            this.label4.Text = "IP Printer";
            // 
            // txtPortServer
            // 
            this.txtPortServer.Location = new System.Drawing.Point(88, 37);
            this.txtPortServer.Name = "txtPortServer";
            this.txtPortServer.Size = new System.Drawing.Size(100, 20);
            this.txtPortServer.TabIndex = 39;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 13);
            this.label2.TabIndex = 38;
            this.label2.Text = "Port Server";
            // 
            // txtIpServer
            // 
            this.txtIpServer.Location = new System.Drawing.Point(88, 11);
            this.txtIpServer.Name = "txtIpServer";
            this.txtIpServer.Size = new System.Drawing.Size(100, 20);
            this.txtIpServer.TabIndex = 37;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 13);
            this.label1.TabIndex = 36;
            this.label1.Text = "IP Server";
            // 
            // Config
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(204, 267);
            this.Controls.Add(this.txtPortCamera2);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.txtIpCamera2);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.txtPortDb);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtIpDB);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtTimeoutDb);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.txtPortPrinter);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtIpPrinter);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtPortServer);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtIpServer);
            this.Controls.Add(this.label1);
            this.Name = "Config";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Config";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtPortCamera2;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtIpCamera2;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtPortDb;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtIpDB;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtTimeoutDb;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtPortPrinter;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtIpPrinter;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtPortServer;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtIpServer;
        private System.Windows.Forms.Label label1;

    }
}