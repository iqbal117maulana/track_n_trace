﻿namespace Label_manual
{
    partial class config
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtIpServer = new System.Windows.Forms.TextBox();
            this.txtPortServer = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtIpCamera = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtPortCamera = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.txtPortPrinter = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtIpPrinter = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtIpDb = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtPortDb = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtNamaDb = new System.Windows.Forms.TextBox();
            this.txtPortCamera2 = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtIpCamera2 = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtPortCamera4 = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtIpCamera4 = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtPortCamera3 = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtIpCamera3 = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtStopBits = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtDatabits = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txtBaudrate = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.txtParity = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.txtHandshake = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.cmbPort = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtIpPrinter2 = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtPortPrinter2 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtIpPrinter3 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtPortPrinter3 = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.txtTimeOut = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(752, 289);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "IP Server";
            // 
            // txtIpServer
            // 
            this.txtIpServer.Location = new System.Drawing.Point(828, 286);
            this.txtIpServer.Name = "txtIpServer";
            this.txtIpServer.Size = new System.Drawing.Size(100, 20);
            this.txtIpServer.TabIndex = 0;
            // 
            // txtPortServer
            // 
            this.txtPortServer.Location = new System.Drawing.Point(82, 23);
            this.txtPortServer.Name = "txtPortServer";
            this.txtPortServer.Size = new System.Drawing.Size(100, 20);
            this.txtPortServer.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Port Server";
            // 
            // txtIpCamera
            // 
            this.txtIpCamera.Location = new System.Drawing.Point(82, 18);
            this.txtIpCamera.Name = "txtIpCamera";
            this.txtIpCamera.Size = new System.Drawing.Size(100, 20);
            this.txtIpCamera.TabIndex = 2;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 21);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "IP Camera 1";
            // 
            // txtPortCamera
            // 
            this.txtPortCamera.Location = new System.Drawing.Point(790, 13);
            this.txtPortCamera.Name = "txtPortCamera";
            this.txtPortCamera.Size = new System.Drawing.Size(100, 20);
            this.txtPortCamera.TabIndex = 3;
            this.txtPortCamera.TextChanged += new System.EventHandler(this.txtPortCamera_TextChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(714, 16);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(74, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "Port Camera 1";
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.button1.Image = global::Label_manual.Properties.Resources.submit;
            this.button1.Location = new System.Drawing.Point(260, 119);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(136, 36);
            this.button1.TabIndex = 13;
            this.button1.Text = "Submit";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtPortPrinter
            // 
            this.txtPortPrinter.Location = new System.Drawing.Point(79, 48);
            this.txtPortPrinter.Name = "txtPortPrinter";
            this.txtPortPrinter.Size = new System.Drawing.Size(100, 20);
            this.txtPortPrinter.TabIndex = 8;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 52);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(59, 13);
            this.label12.TabIndex = 32;
            this.label12.Text = "Port Printer";
            // 
            // txtIpPrinter
            // 
            this.txtIpPrinter.Location = new System.Drawing.Point(79, 22);
            this.txtIpPrinter.Name = "txtIpPrinter";
            this.txtIpPrinter.Size = new System.Drawing.Size(100, 20);
            this.txtIpPrinter.TabIndex = 7;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 26);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(50, 13);
            this.label13.TabIndex = 29;
            this.label13.Text = "IP Printer";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 25);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 13);
            this.label4.TabIndex = 23;
            this.label4.Text = "IP DB";
            // 
            // txtIpDb
            // 
            this.txtIpDb.Location = new System.Drawing.Point(79, 22);
            this.txtIpDb.Name = "txtIpDb";
            this.txtIpDb.Size = new System.Drawing.Size(100, 20);
            this.txtIpDb.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 51);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 13);
            this.label3.TabIndex = 25;
            this.label3.Text = "Port DB";
            // 
            // txtPortDb
            // 
            this.txtPortDb.Location = new System.Drawing.Point(79, 48);
            this.txtPortDb.Name = "txtPortDb";
            this.txtPortDb.Size = new System.Drawing.Size(100, 20);
            this.txtPortDb.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 77);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 13);
            this.label5.TabIndex = 27;
            this.label5.Text = "DB Name";
            // 
            // txtNamaDb
            // 
            this.txtNamaDb.Location = new System.Drawing.Point(79, 73);
            this.txtNamaDb.Name = "txtNamaDb";
            this.txtNamaDb.Size = new System.Drawing.Size(100, 20);
            this.txtNamaDb.TabIndex = 6;
            // 
            // txtPortCamera2
            // 
            this.txtPortCamera2.Location = new System.Drawing.Point(790, 65);
            this.txtPortCamera2.Name = "txtPortCamera2";
            this.txtPortCamera2.Size = new System.Drawing.Size(100, 20);
            this.txtPortCamera2.TabIndex = 41;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(714, 68);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(74, 13);
            this.label14.TabIndex = 43;
            this.label14.Text = "Port Camera 2";
            // 
            // txtIpCamera2
            // 
            this.txtIpCamera2.Location = new System.Drawing.Point(82, 44);
            this.txtIpCamera2.Name = "txtIpCamera2";
            this.txtIpCamera2.Size = new System.Drawing.Size(100, 20);
            this.txtIpCamera2.TabIndex = 40;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(6, 47);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(65, 13);
            this.label15.TabIndex = 42;
            this.label15.Text = "IP Camera 2";
            // 
            // txtPortCamera4
            // 
            this.txtPortCamera4.Location = new System.Drawing.Point(790, 172);
            this.txtPortCamera4.Name = "txtPortCamera4";
            this.txtPortCamera4.Size = new System.Drawing.Size(100, 20);
            this.txtPortCamera4.TabIndex = 45;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(714, 175);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(74, 13);
            this.label16.TabIndex = 47;
            this.label16.Text = "Port Camera 4";
            // 
            // txtIpCamera4
            // 
            this.txtIpCamera4.Location = new System.Drawing.Point(82, 96);
            this.txtIpCamera4.Name = "txtIpCamera4";
            this.txtIpCamera4.Size = new System.Drawing.Size(100, 20);
            this.txtIpCamera4.TabIndex = 44;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(6, 99);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(65, 13);
            this.label17.TabIndex = 46;
            this.label17.Text = "IP Camera 4";
            // 
            // txtPortCamera3
            // 
            this.txtPortCamera3.Location = new System.Drawing.Point(790, 117);
            this.txtPortCamera3.Name = "txtPortCamera3";
            this.txtPortCamera3.Size = new System.Drawing.Size(100, 20);
            this.txtPortCamera3.TabIndex = 49;
            this.txtPortCamera3.TextChanged += new System.EventHandler(this.textBox5_TextChanged);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(714, 120);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(74, 13);
            this.label18.TabIndex = 51;
            this.label18.Text = "Port Camera 3";
            // 
            // txtIpCamera3
            // 
            this.txtIpCamera3.Location = new System.Drawing.Point(82, 70);
            this.txtIpCamera3.Name = "txtIpCamera3";
            this.txtIpCamera3.Size = new System.Drawing.Size(100, 20);
            this.txtIpCamera3.TabIndex = 48;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(6, 73);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(65, 13);
            this.label19.TabIndex = 50;
            this.label19.Text = "IP Camera 3";
            // 
            // txtStopBits
            // 
            this.txtStopBits.Location = new System.Drawing.Point(79, 97);
            this.txtStopBits.Name = "txtStopBits";
            this.txtStopBits.Size = new System.Drawing.Size(100, 20);
            this.txtStopBits.TabIndex = 57;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(6, 100);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(49, 13);
            this.label20.TabIndex = 63;
            this.label20.Text = "Stop Bits";
            // 
            // txtDatabits
            // 
            this.txtDatabits.Location = new System.Drawing.Point(79, 71);
            this.txtDatabits.Name = "txtDatabits";
            this.txtDatabits.Size = new System.Drawing.Size(100, 20);
            this.txtDatabits.TabIndex = 56;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(6, 74);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(50, 13);
            this.label21.TabIndex = 62;
            this.label21.Text = "Data Bits";
            // 
            // txtBaudrate
            // 
            this.txtBaudrate.Location = new System.Drawing.Point(79, 45);
            this.txtBaudrate.Name = "txtBaudrate";
            this.txtBaudrate.Size = new System.Drawing.Size(100, 20);
            this.txtBaudrate.TabIndex = 55;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(6, 48);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(53, 13);
            this.label22.TabIndex = 61;
            this.label22.Text = "Baudrate ";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(6, 22);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(26, 13);
            this.label23.TabIndex = 60;
            this.label23.Text = "Port";
            // 
            // txtParity
            // 
            this.txtParity.Location = new System.Drawing.Point(79, 149);
            this.txtParity.Name = "txtParity";
            this.txtParity.Size = new System.Drawing.Size(100, 20);
            this.txtParity.TabIndex = 53;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(6, 152);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(33, 13);
            this.label24.TabIndex = 59;
            this.label24.Text = "Parity";
            // 
            // txtHandshake
            // 
            this.txtHandshake.Location = new System.Drawing.Point(79, 123);
            this.txtHandshake.Name = "txtHandshake";
            this.txtHandshake.Size = new System.Drawing.Size(100, 20);
            this.txtHandshake.TabIndex = 52;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(6, 126);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(65, 13);
            this.label25.TabIndex = 58;
            this.label25.Text = "Handshake ";
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.button2.Location = new System.Drawing.Point(32, 175);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(150, 50);
            this.button2.TabIndex = 64;
            this.button2.Text = "Check Com Port";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // cmbPort
            // 
            this.cmbPort.FormattingEnabled = true;
            this.cmbPort.Location = new System.Drawing.Point(79, 17);
            this.cmbPort.Name = "cmbPort";
            this.cmbPort.Size = new System.Drawing.Size(100, 21);
            this.cmbPort.TabIndex = 65;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label23);
            this.groupBox1.Controls.Add(this.cmbPort);
            this.groupBox1.Controls.Add(this.label25);
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Controls.Add(this.txtHandshake);
            this.groupBox1.Controls.Add(this.txtStopBits);
            this.groupBox1.Controls.Add(this.label24);
            this.groupBox1.Controls.Add(this.label20);
            this.groupBox1.Controls.Add(this.txtParity);
            this.groupBox1.Controls.Add(this.txtDatabits);
            this.groupBox1.Controls.Add(this.label22);
            this.groupBox1.Controls.Add(this.label21);
            this.groupBox1.Controls.Add(this.txtBaudrate);
            this.groupBox1.Location = new System.Drawing.Point(431, 16);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(207, 242);
            this.groupBox1.TabIndex = 66;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Weigher";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label26);
            this.groupBox2.Controls.Add(this.txtTimeOut);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.txtIpPrinter);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.txtPortPrinter);
            this.groupBox2.Location = new System.Drawing.Point(208, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(188, 100);
            this.groupBox2.TabIndex = 67;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Printer";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.txtIpDb);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.txtPortDb);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.txtNamaDb);
            this.groupBox3.Location = new System.Drawing.Point(8, 12);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(188, 100);
            this.groupBox3.TabIndex = 68;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Database";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label2);
            this.groupBox4.Controls.Add(this.txtPortServer);
            this.groupBox4.Location = new System.Drawing.Point(123, 1);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(207, 63);
            this.groupBox4.TabIndex = 69;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Server";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label6);
            this.groupBox5.Controls.Add(this.groupBox4);
            this.groupBox5.Controls.Add(this.txtIpCamera);
            this.groupBox5.Controls.Add(this.label15);
            this.groupBox5.Controls.Add(this.txtIpCamera2);
            this.groupBox5.Controls.Add(this.txtIpCamera3);
            this.groupBox5.Controls.Add(this.label19);
            this.groupBox5.Controls.Add(this.label17);
            this.groupBox5.Controls.Add(this.txtIpCamera4);
            this.groupBox5.Location = new System.Drawing.Point(39, 298);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(200, 128);
            this.groupBox5.TabIndex = 70;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Camera";
            this.groupBox5.Visible = false;
            this.groupBox5.Enter += new System.EventHandler(this.groupBox5_Enter);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.label11);
            this.groupBox6.Controls.Add(this.txtIpPrinter2);
            this.groupBox6.Controls.Add(this.label10);
            this.groupBox6.Controls.Add(this.txtPortPrinter2);
            this.groupBox6.Controls.Add(this.label9);
            this.groupBox6.Controls.Add(this.txtIpPrinter3);
            this.groupBox6.Controls.Add(this.label8);
            this.groupBox6.Controls.Add(this.txtPortPrinter3);
            this.groupBox6.Location = new System.Drawing.Point(48, 298);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(199, 178);
            this.groupBox6.TabIndex = 79;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Printer";
            this.groupBox6.Visible = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(17, 48);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(45, 13);
            this.label11.TabIndex = 83;
            this.label11.Text = "IP Right";
            // 
            // txtIpPrinter2
            // 
            this.txtIpPrinter2.Location = new System.Drawing.Point(90, 45);
            this.txtIpPrinter2.Name = "txtIpPrinter2";
            this.txtIpPrinter2.Size = new System.Drawing.Size(100, 20);
            this.txtIpPrinter2.TabIndex = 79;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(17, 74);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(54, 13);
            this.label10.TabIndex = 84;
            this.label10.Text = "Port Right";
            // 
            // txtPortPrinter2
            // 
            this.txtPortPrinter2.Location = new System.Drawing.Point(90, 71);
            this.txtPortPrinter2.Name = "txtPortPrinter2";
            this.txtPortPrinter2.Size = new System.Drawing.Size(100, 20);
            this.txtPortPrinter2.TabIndex = 80;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(17, 100);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(39, 13);
            this.label9.TabIndex = 85;
            this.label9.Text = "IP Top";
            // 
            // txtIpPrinter3
            // 
            this.txtIpPrinter3.Location = new System.Drawing.Point(90, 97);
            this.txtIpPrinter3.Name = "txtIpPrinter3";
            this.txtIpPrinter3.Size = new System.Drawing.Size(100, 20);
            this.txtIpPrinter3.TabIndex = 81;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(17, 126);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(48, 13);
            this.label8.TabIndex = 86;
            this.label8.Text = "Port Top";
            // 
            // txtPortPrinter3
            // 
            this.txtPortPrinter3.Location = new System.Drawing.Point(90, 123);
            this.txtPortPrinter3.Name = "txtPortPrinter3";
            this.txtPortPrinter3.Size = new System.Drawing.Size(100, 20);
            this.txtPortPrinter3.TabIndex = 82;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(6, 77);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(50, 13);
            this.label26.TabIndex = 34;
            this.label26.Text = "Time Out";
            // 
            // txtTimeOut
            // 
            this.txtTimeOut.Location = new System.Drawing.Point(79, 73);
            this.txtTimeOut.Name = "txtTimeOut";
            this.txtTimeOut.Size = new System.Drawing.Size(100, 20);
            this.txtTimeOut.TabIndex = 33;
            // 
            // config
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(407, 165);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtPortCamera);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.txtPortCamera3);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.txtIpServer);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.txtPortCamera2);
            this.Controls.Add(this.txtPortCamera4);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "config";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Configuration";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtIpServer;
        private System.Windows.Forms.TextBox txtPortServer;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtIpCamera;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtPortCamera;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtPortPrinter;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtIpPrinter;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtIpDb;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtPortDb;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtNamaDb;
        private System.Windows.Forms.TextBox txtPortCamera2;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtIpCamera2;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtPortCamera4;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtIpCamera4;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtPortCamera3;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtIpCamera3;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtStopBits;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtDatabits;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtBaudrate;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox txtParity;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txtHandshake;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ComboBox cmbPort;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox txtTimeOut;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtIpPrinter2;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtPortPrinter2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtIpPrinter3;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtPortPrinter3;
    }
}