﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Label_manual
{
    public partial class print : Form
    {
        tcp_testing tcp;
        tcp_testing tcp2;
        tcp_testing tcp3;
        List<string> dataCap = new List<string>();
        List<string> dataVial= new List<string>();
        sqlitecs sql;
        public int hitung = 0;
        int printer;
        public print(int i)
        {
            printer = i;
            if (i == 1)
                tcp = new tcp_testing();
            if (i == 2)
                tcp2 = new tcp_testing();
            if (i == 3)
                tcp3 = new tcp_testing();

        }

        public print(List<string> data, List<string> dataVi)
        {
            dataCap = data;
            //kirimData();
            dataVial = dataVi;
        }

        public bool cekKoneksi(string ip, string port)
        {
            bool res = false ;
            if (printer == 1)
                res = tcp.Connect(ip, port, "5",1);
            if (printer == 2)
                res = tcp2.Connect(ip, port, "5", 2);
            if (printer == 3)
                res = tcp3.Connect(ip, port, "5", 3);
            return res;
        }
     
        public void close()
        {
            tcp.dc();
        }
        
        public void kirimDatags(string data)
        {
            if (printer == 1)
                tcp.send(data);
            if (printer == 2)
                tcp2.send(data);
            if (printer == 3)
                tcp3.send(data);
            //tcp.dc();
        }

        public void kirimData(string[] data)
        {
            string dataKirim = "";
            for (int i = 0; i < data.Length; i++)
            {
                dataKirim = dataKirim + "," + data[i];
            }
            tcp.send(dataKirim);
            tcp.dc();
        }



        public void kirimData(string data)
        {
            tcp.send(data);
            tcp.dc();
        }

        private void button7_Click(object sender, EventArgs e)
        {
           // tcp.Connect(txtIp.Text, txtPort.Text,"10");
            //tcp.updatestatus("Connected");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            tcp.send("^0!PO");
            tcp.updatestatus("Power ON");
        }

        private void button4_Click(object sender, EventArgs e)
        {
            tcp.send("^0!NO");
            tcp.updatestatus("Noozle ON");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            tcp.send("^0!PF");
            tcp.updatestatus("Power OFF");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            tcp.send("^0!NC");
            tcp.updatestatus("Nozzle Close");
        }

        private void button6_Click(object sender, EventArgs e)
        {
            tcp.send("^0=ET " + txtExternal.Text);
            tcp.updatestatus("EXTERNAL TEXT : "+txtExternal.Text);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            tcp.send("^0!GO");
            hitung = 0;
            tcp.updatestatus("PRINT...");
        }

        private void button8_Click(object sender, EventArgs e)
        {
            tcp.send("^0=MR" + hitung + " " + txtMailing.Text);
            tcp.updatestatus("MAILING RECORD " + hitung + " " + txtMailing.Text);
            hitung++;
        }

        private void label3_Click(object sender, EventArgs e)
        {
            tcp.send("^0?SM");
            tcp.updatestatus("Mail Status");
        }

        public string cekprinterStatus()
        {
            return tcp.sendBack("^0?RS");
        }

        private void button9_Click(object sender, EventArgs e)
        {
            //tcp = new tcp_testing();
            //if (tcp.Connect(txtIp.Text, txtPort.Text, "2"))
            //{
            //    string data = tcp.sendBack("^0?RS");
            //    txtStatus.AppendText(data + "\n");
            //}
            //else
            //    new Confirm("Gak konek");
        }
       
        public string statusLeibinger;
     
        public bool cekStatusPrinter()
        {
            string data = tcp.sendBack("^0?RS");
            bool balikan = false;
            bool balikan1 = false;
            string teksbalikan="";
            if (data.Length > 0)
            {
                char nozzle = data[5];
                switch (nozzle)
                {
                    case '1':
                        statusLeibinger = "Nozzle Opens";
                        break;
                    case '2':
                        statusLeibinger = "Nozzle Is Open";
                        balikan = true;
                        break;
                    case '3':
                        statusLeibinger = "Nozzle Closes";
                        break;
                    case '4':
                        statusLeibinger = "Nozzle Is Closed";
                        break;
                    case '5':
                        statusLeibinger = "Nozzle In Between";
                        break;
                    default:
                        statusLeibinger = "Nozzle Is Invalid";
                        break;
                }

                char state = data[7];
                switch (state)
                {
                    case '1':
                        statusLeibinger = statusLeibinger + ", State : Standby";
                        break;
                    case '2':
                        statusLeibinger = statusLeibinger + ", State : Initialization";
                        break;
                    case '3':
                        statusLeibinger = statusLeibinger + ", State : Interval";
                        break;
                    case '4':
                        statusLeibinger = statusLeibinger + ", State : Ready For Action";
                        break;
                    case '5':
                        statusLeibinger = statusLeibinger + ", State : Ready For PrintStart";
                        
                        break;
                    default:
                        statusLeibinger = statusLeibinger + ", State : Printing"; 
                        balikan1 = true;
                        break;
                }
                if (balikan && balikan1)
                    return true;
                else
                    return false;
            }
            else
            {
                return false;
            }
        }
    }
}
