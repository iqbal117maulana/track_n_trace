﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO.Ports;

namespace Label_manual
{
    public partial class HyperTerminal : Form
    {
        SerialPort ComPort = new SerialPort();
        delegate void SetTextCallback(string text);
        private SerialPinChangedEventHandler SerialPinChangedEventHandler1;
        public Control ms;
        public HyperTerminal()
        {
            InitializeComponent(); 
            SerialPinChangedEventHandler1 = new SerialPinChangedEventHandler(PinChanged);
            ComPort.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(port_DataReceived_1);

        }
        string InputData = String.Empty;
        private void port_DataReceived_1(object sender, SerialDataReceivedEventArgs e)
        {
            InputData = ComPort.ReadExisting();
            if (InputData != String.Empty)
            {
                this.BeginInvoke(new SetTextCallback(SetText), new object[] { InputData });
                string[] result = InputData.Split(new char[] { '\t' });
                if (result[0].StartsWith("Gross"))
                {
                    string weight = result[1];
                    ms.tambahData("1,"+weight);
                }

            }
        }
        internal void PinChanged(object sender, SerialPinChangedEventArgs e)
        {
            SerialPinChange SerialPinChange1 = 0;
            bool signalState = false;

            SerialPinChange1 = e.EventType;
            lblCTSStatus.BackColor = Color.Green;
            lblDSRStatus.BackColor = Color.Green;
            lblRIStatus.BackColor = Color.Green;
            lblBreakStatus.BackColor = Color.Green;
            switch (SerialPinChange1)
            {
                case SerialPinChange.Break:
                    lblBreakStatus.BackColor = Color.Red;
                    //MessageBox.Show("Break is Set");
                    break;
                case SerialPinChange.CDChanged:
                    signalState = ComPort.CtsHolding;
                    //  MessageBox.Show("CD = " + signalState.ToString());
                    break;
                case SerialPinChange.CtsChanged:
                    signalState = ComPort.CDHolding;
                    lblCTSStatus.BackColor = Color.Red;
                    //MessageBox.Show("CTS = " + signalState.ToString());
                    break;
                case SerialPinChange.DsrChanged:
                    signalState = ComPort.DsrHolding;
                    lblDSRStatus.BackColor = Color.Red;
                    // MessageBox.Show("DSR = " + signalState.ToString());
                    break;
                case SerialPinChange.Ring:
                    lblRIStatus.BackColor = Color.Red;
                    //MessageBox.Show("Ring Detected");
                    break;
            }
        }
        private void SetText(string text)
        {
            this.rtbIncoming.Text += text;
        }
        private void btnGetSerialPorts_Click(object sender, EventArgs e)
        {
            string[] ArrayComPortsNames = null;
            int index = -1;
            string ComPortName = null;

            //Com Ports
            ArrayComPortsNames = SerialPort.GetPortNames();
            if (ArrayComPortsNames.Length > 0)
            {
                do
                {

                    index += 1;
                    cboPorts.Items.Add(ArrayComPortsNames[index]);



                } while (!((ArrayComPortsNames[index] == ComPortName) || (index == ArrayComPortsNames.GetUpperBound(0))));
                Array.Sort(ArrayComPortsNames);

                if (index == ArrayComPortsNames.GetUpperBound(0))
                {
                    ComPortName = ArrayComPortsNames[0];
                }
                //get first item print in text
                cboPorts.Text = ArrayComPortsNames[0];
                //Baud Rate
                //cboBaudRate.Items.Add(300);
                //cboBaudRate.Items.Add(600);
                //cboBaudRate.Items.Add(1200);
                //cboBaudRate.Items.Add(2400);
                cboBaudRate.Items.Add(9600);
                //cboBaudRate.Items.Add(14400);
                //cboBaudRate.Items.Add(19200);
                //cboBaudRate.Items.Add(38400);
                //cboBaudRate.Items.Add(57600);
                //cboBaudRate.Items.Add(115200);
                cboBaudRate.Items.ToString();
                //get first item print in text
                cboBaudRate.Text = cboBaudRate.Items[0].ToString();
                //Data Bits
                cboDataBits.Items.Add(7);
                cboDataBits.Items.Add(8);
                //get the first item print it in the text 
                cboDataBits.Text = cboDataBits.Items[0].ToString();

                //Stop Bits
                cboStopBits.Items.Add("One");
                cboStopBits.Items.Add("OnePointFive");
                cboStopBits.Items.Add("Two");
                //get the first item print in the text
                cboStopBits.Text = cboStopBits.Items[0].ToString();
                //Parity 
                cboParity.Items.Add("None");
                cboParity.Items.Add("Even");
                cboParity.Items.Add("Mark");
                cboParity.Items.Add("Odd");
                cboParity.Items.Add("Space");
                //get the first item print in the text
                cboParity.Text = cboParity.Items[0].ToString();
                //Handshake
                cboHandShaking.Items.Add("None");
                cboHandShaking.Items.Add("XOnXOff");
                cboHandShaking.Items.Add("RequestToSend");
                cboHandShaking.Items.Add("RequestToSendXOnXOff");
                //get the first item print it in the text 
                cboHandShaking.Text = cboHandShaking.Items[0].ToString();
            }
            else
                MessageBox.Show("Com port not found");
        }

        public HyperTerminal(string port, string baudrate, string databits, string stopbits, string handshaking, string parity)
        {
            SerialPinChangedEventHandler1 = new SerialPinChangedEventHandler(PinChanged);
            ComPort.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(port_DataReceived_1);
            ComPort.PortName = Convert.ToString(port);
            ComPort.BaudRate = Convert.ToInt32(baudrate);
            ComPort.DataBits = Convert.ToInt16(databits);
            ComPort.StopBits = (StopBits)Enum.Parse(typeof(StopBits), stopbits);
            ComPort.Handshake = (Handshake)Enum.Parse(typeof(Handshake), handshaking);
            ComPort.Parity = (Parity)Enum.Parse(typeof(Parity), parity);
            
        }

        public void startTerimnal()
        {
            try
            {
                ComPort.Open();
            }
            catch (Exception ex)
            {
                
            }
        }

        private void btnPortState_Click(object sender, EventArgs e)
        {

            if (btnPortState.Text == "Closed")
            {
                btnPortState.Text = "Open";
                ComPort.PortName = Convert.ToString(cboPorts.Text);
                ComPort.BaudRate = Convert.ToInt32(cboBaudRate.Text);
                ComPort.DataBits = Convert.ToInt16(cboDataBits.Text);
                ComPort.StopBits = (StopBits)Enum.Parse(typeof(StopBits), cboStopBits.Text);
                ComPort.Handshake = (Handshake)Enum.Parse(typeof(Handshake), cboHandShaking.Text);
                ComPort.Parity = (Parity)Enum.Parse(typeof(Parity), cboParity.Text);
                ComPort.Open();
            }
            else if (btnPortState.Text == "Open")
            {
                btnPortState.Text = "Closed";
                ComPort.Close();
            }
        }
    }
}
