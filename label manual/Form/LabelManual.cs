﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace Label_manual
{
    public partial class LabelManual : Form
    {
        DataTable table;        
        Control ms;                
        List<string[]> dataOrder = new List<string[]>();        
        DataTable datMas = new DataTable();
        public DataTable datGs = new DataTable();        
        public List<string[]> datacombo;
        public string LogFile = "";
        private String Admin;
        public DataTable DtGsScanned = new DataTable();
        public string role = "";
        
        public LabelManual(string admin)
        {
            InitializeComponent();
            ms = new Control();
            ms.wr = this;
            ms.startstation(admin);
            this.Admin = admin;
            setAdminName(admin);
        }

        //internal string getAdminName(string admin)
        //{
        //    List<string> field = new List<string>();
        //    field.Add("first_name   ");
        //    string where = "id ='" + admin + "'";
        //    List<string[]> ds = ms.db.selectList(field, "[USERS]", where);
        //    if (ms.db.num_rows > 0)
        //        return ds[0][0];
        //    else
        //        return "";
        //}

        private void setAdminName(string admin)
        {
            List<string> field = new List<string>();
            field.Add("a.first_name, c.name, c.id as role ");
            string where = "a.id ='" + admin + "'";
            string from = "users a inner join users_groups b on a.id = b.user_id inner join groups c on b.group_id = c.id";            
            List<string[]> ds = ms.db.selectList(field, from, where);
            if (ms.db.num_rows > 0)
            {
                lblUserId.Text = ds[0][0];
                lblRole.Text = ds[0][1];
                role = ds[0][2];
                btnConfig.Visible = role == "1";
            }
        }

        private void Warehouse_FormClosed(object sender, FormClosedEventArgs e)
        {
            ms.close("");
            Application.Exit();
        }

        //private void textBox1_KeyDown(object sender, KeyEventArgs e)
        //{
        //    if (e.KeyCode == Keys.Enter)
        //    {
        //        textBox1.Text = textBox1.Text.Replace("\n", "");
        //        if (textBox1.Text.Length > 0)
        //            ms.tambahData("0,"+textBox1.Text);
        //        textBox1.Text = "";
        //    }
        //}

      
        private void button1_Click(object sender, EventArgs e)
        {
           
        }

        private void button9_Click(object sender, EventArgs e)
        {
            config cfg = new config();
            cfg.setWr(this);
            cfg.ShowDialog();
        }


        private void dataGridView2_SelectionChanged(object sender, EventArgs e)
        {
           
        }
      
        private void dataGridView2_CellClick(object sender, DataGridViewCellEventArgs e)
        {
        }

        private void button3_Click(object sender, EventArgs e)
        {
            dgvGsOne.DataSource = "";
            table = new DataTable();
            table.Columns.Add("Basket ID");
            table.Columns.Add("Timestamp");
            lblQtyReceiving.Text = "" + dgvGsOne.Rows.Count;
        }

        //private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        //{
        //    if (dgvReceiving.SelectedRows.Count > 0)
        //        ordersItemIndex = dgvReceiving.SelectedRows[0].Index;
        //}

        private void button2_Click(object sender, EventArgs e)
        {            
            lblQtyReceiving.Text = "" + dgvGsOne.Rows.Count;
        }

        private void textBox2_KeyDown(object sender, KeyEventArgs e)
        {
           
            
        }

       

        private void Warehouse_Load(object sender, EventArgs e)
        {
            datGs.Columns.Add("GSONE");
            datGs.Columns.Add("MASTER_BOX");
            datGs.Columns.Add("WEIGHT");
            datGs.Columns.Add("COLI");
            dgvGsOne.DataSource = datGs;            
        }                

        

        private void cmbStatusOld_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        private void button8_Click(object sender, EventArgs e)
        {
            Confirm cf = new Confirm("Are you sure? ", "Confirmation");
            bool result = cf.conf();
            if (result)
            {
                ms.close("");
                Application.Exit();
            }
        }


        private void dgvReceiving_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            lblQtyReceiving.Text = dgvGsOne.Rows.Count.ToString();
        }       
      
        private void txtGs1MasterBox_KeyDown(object sender, KeyEventArgs e)
        {          
            if (e.KeyCode == Keys.Enter)
            {
                txtGs1MasterBox.Text = txtGs1MasterBox.Text.Replace("\\7E1", "");
                ms.ScanGSOne(txtGs1MasterBox.Text);
                txtIdMasterBox.Enabled = true;
                txtWeight.Enabled = true;
                SetSizeAcolumn();
            }
        }

        private void txtIdMasterBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                ms.MasterBoxScanned = "";
                if (txtIdMasterBox.Text == "")
                {
                    new Confirm("Master Box GS1 ID empty", "Information", MessageBoxButtons.OK);
                    ms.log("Master Box GS1 ID empty");
                    return;
                }

                string Status = ms.cekStatusMasterboxPickinglist(txtIdMasterBox.Text);
                //Status harus 0
                if (!Status.Equals("0"))
                {
                    if (Status.Equals("1"))
                    {
                        new Confirm("Master Box GS1 ID already exists", "Information", MessageBoxButtons.OK);
                        ms.log("Master Box GS1 ID already exists");
                        txtIdMasterBox.Text = "";
                    }
                    else
                    {
                        new Confirm("Master Box GS1 ID not found", "Information", MessageBoxButtons.OK);
                        ms.log("Master Box GS1 ID not found");
                        txtIdMasterBox.Text = "";
                    }
                    return;
                }

                //cek ada masterbox nya 
                if (!ms.getStatusMasterbox(txtIdMasterBox.Text))
                {
                    new Confirm("Master Box GS1 ID not found", "Information", MessageBoxButtons.OK);
                    ms.log("Master Box GS1 ID not found");
                    txtIdMasterBox.Text = "";
                    return;
                }
                else
                {
                    txtWeight.Focus();
                }

                ms.MasterBoxScanned = txtIdMasterBox.Text;
            }
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            if (txtIdMasterBox.Text == "")
            {
                new Confirm("ID master box empty", "Information", MessageBoxButtons.OK);
                ms.log("ID master box empty");
                txtIdMasterBox.Text = "";
                return;
            }

            else if (ms.MasterBoxScanned == "")
            {
                new Confirm("ID master box not scanned", "Information", MessageBoxButtons.OK);
                ms.log("ID master box not scanned");
                txtIdMasterBox.Text = "";
                return;
            }

            else if (txtWeight.Text == "")
            {
                new Confirm("Weight (kg) empty", "Information", MessageBoxButtons.OK);
                ms.log("Weight (kg) empty");
                return;
            }

            else if (ms.MasterBoxScanned == "")
            {
                new Confirm("Master Box GS1 ID not scanned", "Information", MessageBoxButtons.OK);
                ms.log("Master Box GS1 ID not scanned");
                return;
            }
            else
            {
                ms.WightScanned = txtWeight.Text;
                ms.SendDataToPrint(Admin, ms.MasterBoxScanned, ms.WightScanned);
                txtIdMasterBox.Enabled = false;
                txtWeight.Enabled = false;
                txtGs1MasterBox.Focus();
            }

            
        }

        private void btnLog_Click(object sender, EventArgs e)
        {
            Log frm;
            frm = new Log(LogFile);
            frm.ShowDialog();
            if (frm.isClear == true)
                LogFile = "";
        }

        private void txtIdMasterBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            var regex = new Regex(@"[^a-zA-Z0-9\b]");
            if (regex.IsMatch(e.KeyChar.ToString()))
            {
                e.Handled = true;
            }
        }

        private void txtWeight_KeyPress(object sender, KeyPressEventArgs e)
        {
            var regex = new Regex(@"[^0-9.\b]");
            if (regex.IsMatch(e.KeyChar.ToString()))
            {
                e.Handled = true;
            }
        }

        public void SetSizeAcolumn()
        {
            if (dgvGsOne.Rows.Count > 0)
            {                
                for (int i = 0; i < dgvGsOne.Columns.Count; i++)
                {
                    dgvGsOne.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                }
                dgvGsOne.Columns[dgvGsOne.Columns.Count-1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }
            else
            {
                for (int i = 0; i < dgvGsOne.Columns.Count; i++)
                {
                    dgvGsOne.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                }
            }
        }

    }
}
