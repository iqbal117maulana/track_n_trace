﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Label_manual
{
    class Var_Masterbox
    {
        string masterboxId;
        string gsOneMasterBoxId;
        string status;
        string timestamp;
        string weight;
        string shippingOrderNumber;
        string customerId;
        string customerName;
        string customerAddress;
        string contactName;
        string contactPhone;
        string postcode;
        List<string[]> batchAndQty;
        string qtytotal;

        string createdTime;
        string colieMax;
        string gsOneMasterBoxIdPrint;
        string packingSlipId;

        public string ColieMax
        {
            get { return colieMax; }
            set { colieMax = value; }
        }

        public string CreatedTime
        {
            get { return createdTime; }
            set { createdTime = value; }
        }
        string batchnumber;

        public string Batchnumber
        {
            get { return batchnumber; }
            set { batchnumber = value; }
        }
        string expDate;

        public string ExpDate
        {
            get { return expDate; }
            set { expDate = value; }
        }
        string serialNumber;

        public string SerialNumber
        {
            get { return serialNumber; }
            set { serialNumber = value; }
        }
        string gtinmasterbox;

        public string Gtinmasterbox
        {
            get { return gtinmasterbox; }
            set { gtinmasterbox = value; }
        }
        string qty;

        public string Qty
        {
            get { return qty; }
            set { qty = value; }
        }
        string model_name;

        public string Model_name
        {
            get { return model_name; }
            set { model_name = value; }
        }
        string colieNumber;

        public string ColieNumber
        {
            get { return colieNumber; }
            set { colieNumber = value; }
        } 

        public string Qtytotal
        {
            get { return qtytotal; }
            set { qtytotal = value; }
        }

        public List<string[]> BatchAndQty
        {
            get { return batchAndQty; }
            set { batchAndQty = value; }
        }



        public string MasterboxId
        {
            get { return masterboxId; }
            set { masterboxId = value; }
        }

        public string GsOneMasterBoxId
        {
            get { return gsOneMasterBoxId; }
            set { gsOneMasterBoxId = value; }
        }

        public string Status
        {
            get { return status; }
            set { status = value; }
        }

        public string Timestamp
        {
            get { return timestamp; }
            set { timestamp = value; }
        }

        public string Weight
        {
            get { return weight; }
            set { weight = value; }
        }

        public string ShippingOrderNumber
        {
            get { return shippingOrderNumber; }
            set { shippingOrderNumber = value; }
        }

        public string CustomerId
        {
            get { return customerId; }
            set { customerId = value; }
        }

        public string CustomerName
        {
            get { return customerName; }
            set { customerName = value; }
        }

        public string CustomerAddress
        {
            get { return customerAddress; }
            set { customerAddress = value; }
        }

        public string ContactName
        {
            get { return contactName; }
            set { contactName = value; }
        }

        public string ContactPhone
        {
            get { return contactPhone; }
            set { contactPhone = value; }
        }

        public string Postcode
        {
            get { return postcode; }
            set { postcode = value; }        
        }

        public string GsOneMasterBoxIdPrint
        {
            get { return gsOneMasterBoxIdPrint; }
            set { gsOneMasterBoxIdPrint = value; }
        }

        public string PackingSlipId
        {
            get { return packingSlipId; }
            set { packingSlipId = value; }
        }
    }
}
