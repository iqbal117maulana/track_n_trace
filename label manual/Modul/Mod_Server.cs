﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Net.Sockets;
using System.Net;
using System.Windows.Forms;

namespace Label_manual
{
    class ServerMultiClient
    {
        //  static List<Listener> listeners = new List<Listener>();
        Control ctrl;
        bool isRunning = true;

        TcpClient client;
        TcpListener listener;
        public void Start(Control ctrl1, string port)
        {
            try
            {

                ctrl = ctrl1;
                listener = new TcpListener(IPAddress.Any, int.Parse(port));

                listener.Start();

                while (isRunning) // Add your exit flag here
                {
                    if (!listener.Pending())
                    {
                        Thread.Sleep(500); // choose a number (in milliseconds) that makes sense
                        continue; // skip to next iteration of loop
                    }
                    client = listener.AcceptTcpClient();
                    ThreadPool.QueueUserWorkItem(ThreadProc, client);
                }
            }
            catch (Exception ex)
            {
                new Confirm("Port aleady used, Restart Application", "Information", MessageBoxButtons.OK);
                Application.Exit();
                Environment.Exit(Environment.ExitCode);
            }

        }

        public void closeServer(string data)
        {
            try
            {
                isRunning = false;
                listener.Stop();
                client.Close();
            }
            catch (Exception ex)
            {

            }
        }

        string tampung = "";
        public void clearCache()
        {
            tampung = "";
        }


        private void ThreadProc(object obj)
        {
            string bufferincmessage;
            TcpClient client = (TcpClient)obj;
            NetworkStream clientStream = client.GetStream();
            //ctrl.log("Connected ");
            byte[] message = new byte[4096];
            int bytesRead;
            int Counting = 0;
            int countSucces = 0;
            while (isRunning)
            {
                bytesRead = 0;
                try
                {
                    //blocks until a client sends a message

                    //ctrl.log("Waiting data");
                    bytesRead = clientStream.Read(message, 0, 4096);
                }
                catch
                {
                    //a socket error has occured
                    break;
                }
                if (bytesRead == 0)
                {
                    //the client has disconnected from the server
                    break;
                }

                //message has successfully been received
                ASCIIEncoding encoder = new ASCIIEncoding();
                bufferincmessage = encoder.GetString(message, 0, bytesRead);

                //ctrl.log("Receive : " + bufferincmessage);
                //ctrl.log("Close Connection");
                tampung = tampung + bufferincmessage;
                if (bufferincmessage.Contains('|'))
                {
                    string dataKirim = tampung;
                    if (tampung.Contains('$'))
                    {
                        string[] temp = tampung.Split(new char[] { '$' }, StringSplitOptions.RemoveEmptyEntries);
                        if (temp.Length > 1)
                        {
                            dataKirim = temp[0];
                            bool hapusTampung = false;
                            for (int i = 0; i < temp.Length; i++)
                            {
                                if (temp[i].Contains('|'))
                                {
                                    ctrl.tambahData(temp[i]);
                                }
                                else
                                {
                                    tampung = "$" + temp[i];
                                    hapusTampung = true;
                                    //ctrl.log(tampung);
                                }
                            }

                            if (!hapusTampung)
                            {
                                tampung = "";
                            }
                        }
                        else
                        {
                            ctrl.tambahData(tampung);
                            tampung = "";
                        }
                    }
                    else
                    {
                    }
                    Counting = 0;
                }
                else
                {
                    //ctrl.log("Tampung : " + tampung);
                    Counting++;
                }
            }
        }
    }
        


//# data yg lama
    //    private void ThreadProc(object obj)
    //    {
    //        string bufferincmessage;
    //        TcpClient client = (TcpClient)obj;
    //        NetworkStream clientStream = client.GetStream();
    //        ctrl.log("Connected ");
    //        byte[] message = new byte[4096];
    //        int bytesRead;
    //        while (isRunning)
    //        {
    //            bytesRead = 0;
    //            try
    //            {
    //                //blocks until a client sends a message

    //                ctrl.log("Waiting data");
    //                bytesRead = clientStream.Read(message, 0, 4096);
    //            }
    //            catch
    //            {
    //                //a socket error has occured
    //                break;
    //            }
    //            if (bytesRead == 0)
    //            {
    //                //the client has disconnected from the server
    //                break;
    //            }

    //            //message has successfully been received
    //            ASCIIEncoding encoder = new ASCIIEncoding();
    //            bufferincmessage = encoder.GetString(message, 0, bytesRead);

    //            ctrl.log("Receive : " + bufferincmessage);
    //            ctrl.tambahData(bufferincmessage);
    //        }
        

    //    }
    //}

    class Listener
    {
        Thread listenThread;
        string bufferincmessage;
        TcpListener tcplistener;
        Control ctrl;
    }
}