﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.IO.Ports;
using System.Windows.Forms;
using System.IO;

namespace Label_manual
{
    class hyperTerimnal
    {
        private Thread read;
        SerialPort ComPort = new SerialPort();
        delegate void SetTextCallback(string text);
        public Control ms;
        string InputData = String.Empty;
        private void port_DataReceived_1(object sender, SerialDataReceivedEventArgs e)
        {
            try
            {
                int i = 0;
                while (i != 4)
                {
                    Thread.Sleep(200);
                    SetText(ComPort.ReadExisting());
                    i++;
                }
                if (InputData != String.Empty)
                {
                  //  string[] result = InputData.Split('\n');
                    //InputData = InputData.Replace("?", "");
                    //string[] result = InputData.Split(new string[] { "\n" }, StringSplitOptions.None);
                    //if(result.Length ==0)
                     //   result = InputData.Split(new string[] { "\n" }, StringSplitOptions.None);

                    string[] result = InputData.Split(new string[] { "," }, StringSplitOptions.None);
                    string weight = result[2];
                    weight = parsingWeight(weight);
                    ms.tambahData("1," + weight);
                    InputData = "";
                }
            }
            catch (Exception te)
            {
                MessageBox.Show("Data yang diterima dari weigher \n" + InputData + "\n" + te.Message);
                InputData = "";
            }

        }

        private string parsingWeight(string weight)
        {
            weight = weight.Replace("kg", "");
            weight = weight.Replace(" ", "");
            weight = weight.Replace("+", "");
            weight = weight.Replace("?", "");
            weight = weight.Replace("\r", "");
            weight = weight.Replace("\n", "");
            bool ketemu= false;
            int i = 0;
            for (; i < weight.Length && !ketemu; i++)
            {
                if(!weight[i].Equals('0'))
                {
                    ketemu = true;
                }
            }
            weight = weight.Substring(i-1);
            return weight;
        }

        private void SetText(string text)
        {
           //  MessageBox.Show(text);
           // Thread.Sleep(2000);
            InputData += text;
            simpanLog(InputData);
        }
        public hyperTerimnal(string port, string baudrate, string databits, string stopbits, string handshaking, string parity)
        {
            //SerialPinChangedEventHandler1 = new SerialPinChangedEventHandler(PinChanged);
            try
            {
                ComPort.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(port_DataReceived_1);
                ComPort.PortName = Convert.ToString(port);
                ComPort.BaudRate = Convert.ToInt32(baudrate);
                ComPort.DataBits = Convert.ToInt16(databits);
                ComPort.StopBits = (StopBits)Enum.Parse(typeof(StopBits), stopbits);
                ComPort.Handshake = (Handshake)Enum.Parse(typeof(Handshake), handshaking);
                ComPort.Parity = (Parity)Enum.Parse(typeof(Parity), parity);
                ComPort.Open();
                //read = new Thread(new ThreadStart(terimaData));
                //read.Start();
            }
            catch (Exception ex)
            {

            }
        }

        public void simpanLog(String line)
        {
            DateTime now = DateTime.Now;
            string tahun = now.ToString("yyyy");
            string bulan = now.ToString("MM");
            string hari = now.ToString("dd");
            string dates = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            line = dates + "\t" + line;
            // cek file 
            bool cekfile = false;
            while (!cekfile)
            {
                if (Directory.Exists("eventlog"))
                {
                    if (Directory.Exists("eventlog" + @"\" + tahun))
                    {
                        if (Directory.Exists("eventlog" + @"\" + tahun + @"\" + bulan))
                        {

                            using (StreamWriter outputFile = new StreamWriter("eventlog" + @"\" + tahun + @"\" + bulan + @"\" + hari + ".txt", true))
                            {
                                outputFile.WriteLine(line);
                            }
                            cekfile = true;
                        }
                        else
                        {

                            Directory.CreateDirectory("eventlog" + @"\" + tahun + @"\" + bulan);
                        }
                    }
                    else
                    {
                        Directory.CreateDirectory("eventlog" + @"\" + tahun);
                    }
                }
                else
                {

                    Directory.CreateDirectory("eventlog");
                }
            }
        }

   
        private void terimaData()
        {

           // while (true)
            {

            }
        }
        public bool statusTimbangan;
        public void startTerimnal()
        {

            if (ComPort.IsOpen)
            {
                statusTimbangan = true;
                simpanLog("Weigher Connected");
            }
            else
            {
                statusTimbangan = false;
                simpanLog("Weigher Not Connected");  
            }
        }

        internal void CloseTerimnal()
        {
            ComPort.Close();
        }
    }
}
