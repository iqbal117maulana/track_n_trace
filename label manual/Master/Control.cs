﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Data;
using System.Net.NetworkInformation;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace Label_manual
{
    public class Control
    {
        sqlitecs sqlite;
        public LabelManual wr;
        public dbaccess db;
        dbaccessDist dbdist;
        
        private Thread serverThread;
        private Thread serverThread2;        
        public char delimit = ',';
        public System.Timers.Timer delayTimer;
        public string adminid;
        public System.Timers.Timer delaydevice;
        hyperTerimnal hp;
        ServerMultiClient srv;
        ServerMultiClient srv2;
        List<string> tempDataMasterbox = new List<string>();
        int onGoing = 0;
        int masterBoxWeight = 0;
        print pr3;
        print pr2;
        print pr;
        public string dapatGSoneMasterbox;
        bool isConnectPrinter;
        List<Var_Masterbox> LIST_MASTERBOX = new List<Var_Masterbox>();
        public string GsOneScanned = "";
        public string MasterBoxScanned = "";
        public string WightScanned = "";
        public string NoDoclose = "";

        public Control()
        {
            sqlite = new sqlitecs();
            db = new dbaccess();
            dbdist = new dbaccessDist();
            serverThread = new Thread(new ThreadStart(MulaiServer));
            serverThread.Start();
            serverThread2 = new Thread(new ThreadStart(MulaiServer2));
            serverThread2.Start();
            delay();
            //startHyperTerminal();
        }

        public void resetTerimnal()
        {
            hp.CloseTerimnal();
            startHyperTerminal();
        }

        public void startHyperTerminal()
        {
            hp = new hyperTerimnal(sqlite.config["port"], sqlite.config["baudrate"], sqlite.config["databits"], sqlite.config["stopbits"], sqlite.config["handshake"], sqlite.config["parity"]);
            hp.ms = this;
            hp.startTerimnal();
        }

        public void close(string da)
        {
            if (wr.InvokeRequired)
            {
                wr.Invoke(new Action<string>(close), new object[] { da });
                return;
            }
            srv.closeServer("");
            srv2.closeServer("");
           // hp.CloseTerimnal();
        }

        public void initialTable()
        {
            if (hp.statusTimbangan)
            {
                //log("Weigher Connected");
            }
            else
            {
                //log("Weigher Not Connected");
            }
        }

        public void log(string data)
        {
            try
            {
                string dates = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                string simpan = dates + "\t" + data + "\n";
                //wr.txtLog1.AppendText(simpan);
                wr.LogFile = wr.LogFile + simpan + Environment.NewLine;
                db.simpanLog(simpan);
            }
            catch (ObjectDisposedException ob)
            {

            }
        } 

        private List<string> GetSplitReceiveData(string data)
        {
            char[] delimiters = new char[] { '|' };
            List<string> hasilSplit = new List<string>(data.Split(delimiters, StringSplitOptions.RemoveEmptyEntries));
            return hasilSplit;
        }
        private List<string[]> dataMass(string data)
        {
            try
            {
                char[] delimiters = new char[] { '|' };
                List<string> hasilSplit = new List<string>(data.Split(delimiters, StringSplitOptions.RemoveEmptyEntries));
                List<string[]> hasilSplitTemp = new List<string[]>();
                foreach (string da in hasilSplit)
                {
                    hasilSplitTemp.Add(da.Split(','));
                }
                return hasilSplitTemp;
            }
            catch (NullReferenceException ne)
            {
                return null;
            }
        }
        int onprogress = 0;


        void onProgress(bool tambah)
        {
            if (tambah)
                onprogress++;
            else onprogress--;

            //wr.lblOnProgress.Text = "" + onprogress;
        }


        public void ReceiveData(string masterbox, string weight)
        {
            if (!masterbox.Equals("0"))
            {
                //cek data sudah beres belum 
                if (cekMasterboxPickinglist(masterbox))
                {
                    //cek yang ada disqlite
                    //  if (!CheckExistSqlite(data))
                    {
                        //cek ada masterbox nya 
                        if (getStatusMasterbox(masterbox))
                        {
                            onProgress(true);
                            //log("Get Data Masterbox");
                            string dates = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                            string[] dataMasterbox = getMasterbox(masterbox);
                            string shipiingOrderNumber = getShippingOrderNumber(masterbox);
                            string[] ShippingOrder = getShippingOrder(shipiingOrderNumber);
                            List<string[]> batchnumberAndQty = getbatchnumber(masterbox);
                            string qtytotal = getQtyTotal(masterbox);

                            updateWeight(masterbox,weight);

                            //log("Updae Weight data");
                            Var_Masterbox temp_Masterbox = new Var_Masterbox();
                            temp_Masterbox.MasterboxId = masterbox;
                            temp_Masterbox.Timestamp = dates;
                            temp_Masterbox.Weight = weight;
                            temp_Masterbox.ShippingOrderNumber = shipiingOrderNumber;
                            temp_Masterbox.CustomerId = ShippingOrder[0];
                            temp_Masterbox.CustomerName = ShippingOrder[1];
                            temp_Masterbox.CustomerAddress = ShippingOrder[2];
                            temp_Masterbox.ContactName = ShippingOrder[3];
                            temp_Masterbox.ContactPhone = ShippingOrder[4];
                            temp_Masterbox.Postcode = ShippingOrder[5];
                            temp_Masterbox.BatchAndQty = batchnumberAndQty;
                            temp_Masterbox.Qtytotal = qtytotal;
                            temp_Masterbox.CreatedTime = dataMasterbox[2];
                            temp_Masterbox.Batchnumber = dataMasterbox[5];
                            temp_Masterbox.ExpDate = dataMasterbox[6];
                            temp_Masterbox.SerialNumber = dataMasterbox[7];
                            temp_Masterbox.Gtinmasterbox = dataMasterbox[8];
                            temp_Masterbox.Qty = dataMasterbox[9];
                            temp_Masterbox.Model_name = dataMasterbox[10];
                            temp_Masterbox.ColieNumber = dataMasterbox[11];
                            temp_Masterbox.ColieMax = ShippingOrder[7];
                            temp_Masterbox.Status = "0";
                            LIST_MASTERBOX.Add(temp_Masterbox);

                            //log("Start Send To Prnter");
                            SentToPrinter(temp_Masterbox);
                            //log("Sent To Printer");
                            db.Movement("", "Manual Packer", "Label aplicator", "1", masterbox);
                        }
                    }
                }
            }
        }

        private void updateWeight(string masterbox,string weight)
        {
            Dictionary<string,string> field = new Dictionary<string, string>();
            field.Add("weight", weight);
            string where = "masterboxid ='" + masterbox + "'";;
            db.update(field, "masterbox", where);
        }

        private void updateWeightDanSN(string masterbox, string weight, string SerialNumber)
        {
            Dictionary<string, string> field = new Dictionary<string, string>();
            field.Add("weight", weight);
            field.Add("serialNumber", SerialNumber);
            string where = "masterboxid ='" + masterbox + "'"; ;
            db.update(field, "masterbox", where);
        }

        public void FrontCamera(string data)
        {
            if (!data.Equals("0"))
            {
                //cek data sudah beres belum 
                if (cekMasterboxPickinglist(data))
                {
                    //cek yang ada disqlite
                  //  if (!CheckExistSqlite(data))
                    {
                        //cek ada masterbox nya 
                        if (getStatusMasterbox(data))
                        {
                            onProgress(true);
                            tempDataMasterbox.Add(data);
                            string[] dataMasterbox = getMasterbox(data);
                            db.Movement("", "Manual Packer", "Label aplicator", "1", data);
                            string shipiingOrderNumber = getShippingOrderNumber(data);
                            string gsone = simpanMasterbox(data, shipiingOrderNumber);
                        }
                        else
                        {
                            //log("Data masterbox empty front cam");
                        }
                    }
                   // else
                    {
                    //    log("Data masterbox already exist");

                    }
                }
                else
                {
                    //log("Data masterbox not found");
                }
            }
            else
            {
                //log("Data masterbox fail to read");
            }
        }

        public void ProsesWeigher(string data)
        {
            if (tempDataMasterbox.Count != 0)
            {
                if (tempDataMasterbox.Count > masterBoxWeight)
                {
                    data = ambilData(data);
                    //log("Weigher :" + data);
                    updateWeightMasterbox(tempDataMasterbox[masterBoxWeight], data);
                    masterBoxWeight++;
                    string[] dataMasterbox = getMasterbox(tempDataMasterbox[onGoing]);
                    string shipiingOrderNumber = getShippingOrderNumber(tempDataMasterbox[onGoing]);
                    string[] dataPribadi = getShippingOrder(shipiingOrderNumber);
                    //log("Start Printing");
                    List<string[]> batchnumberAndQty = getbatchnumber(tempDataMasterbox[onGoing]);
                    string qtytotal = getQtyTotal(tempDataMasterbox[onGoing]); ;
                    Kirim3Printer(dataPribadi, dataMasterbox, data, shipiingOrderNumber, batchnumberAndQty, qtytotal);
                }
                else
                {
                    //log("Data masterbox not recognize");
                }
            }
            else
            {
                //log("Data masterbox empty weigher");
            }
        }

        public void tambahData(string data)
        {
            if (wr.InvokeRequired)
            {
                wr.Invoke(new Action<string>(tambahData), new object[] { data });
                return;
            }
            //log("Receive Data:" + data);
            db.simpanLog("Proses Data: " + data);
            data = data.Replace("\r", "");
            data = data.Replace("\n", "");
            data = data.Replace("]d2", "");
            data = data.Replace("$", "");
            data = data.Replace(" ", "");
            if (!data.Equals("0"))
            {
                List<string[]> dataDapat = dataMass(data);
                for (int i = 0; i < dataDapat.Count; i++)
                {
                    try
                    {
                        data = dataDapat[i][1];
                        int func = int.Parse(dataDapat[i][0]);
                        if (!data.Contains("?") && data.Length > 0)
                        {
                            // dapat data Kamera
                            if (func == 0)
                            {
                                if (!data.Equals("0"))
                                {
                                    //cek data sudah beres belum 
                                    if (cekMasterboxPickinglist(data))
                                    {
                                        //cek yang ada disqlite
                                        if (!CheckExistSqlite(data))
                                        {
                                            //cek ada masterbox nya 
                                            if (getStatusMasterbox(data))
                                            {
                                                onProgress(true);
                                                tempDataMasterbox.Add(data);
                                                string[] dataMasterbox = getMasterbox(data);
                                                db.Movement("", "Manual Packer", "Label aplicator", "1", data);
                                                string shipiingOrderNumber = getShippingOrderNumber(data);
                                                string gsone = simpanMasterbox(data, shipiingOrderNumber);
                                            }
                                            else
                                            {
                                                //log("Data masterbox emptym");
                                            }
                                        }
                                        else
                                        {
                                            //log("Data masterbox already exist");

                                        }
                                    }
                                    else
                                    {
                                        //log("Data masterbox not found");
                                    }
                                }
                                else
                                {
                                    //log("Data masterbox fail to read");
                                }
                            }
                            //dapat data weigher
                            else if (func == 1)
                            {
                                if (tempDataMasterbox.Count != 0)
                                {
                                    if (tempDataMasterbox.Count > masterBoxWeight)
                                    {
                                        data = ambilData(data);
                                        //log("Weigher :" + data);
                                        updateWeightMasterbox(tempDataMasterbox[masterBoxWeight], data);
                                        masterBoxWeight++;
                                        string[] dataMasterbox = getMasterbox(tempDataMasterbox[onGoing]);
                                        string shipiingOrderNumber = getShippingOrderNumber(tempDataMasterbox[onGoing]);
                                        string[] dataPribadi = getShippingOrder(shipiingOrderNumber);
                                        //log("Start Printing");
                                        List<string[]> batchnumberAndQty = getbatchnumber(tempDataMasterbox[onGoing]);
                                        string qtytotal = getQtyTotal(tempDataMasterbox[onGoing]); ;
                                        Kirim3Printer(dataPribadi, dataMasterbox, data, shipiingOrderNumber, batchnumberAndQty, qtytotal);
                                    }
                                    else
                                    {
                                        //log("Data masterbox not recognize");
                                    }
                                }
                                else
                                {
                                    //log("Data masterbox empty");
                                }

                                // tampilBtn(true);
                            }
                            // dapat status
                            else if (func == 2)
                            {
                                if (!data.Equals("0"))
                                {
                                    if (onGoing < LIST_MASTERBOX.Count)
                                    {
                                        if (LIST_MASTERBOX.Count > 0)
                                        {
                                            //cek data timbangan sudah ada belum 
                                         //   if (CekWeightMasterbox(data))
                                            {
                                                if (cekDataMasterStatus(LIST_MASTERBOX[onGoing].MasterboxId))
                                                {

                                                    LIST_MASTERBOX[onGoing].GsOneMasterBoxId = data;
                                                    LIST_MASTERBOX[onGoing].Status = "1";
                                                    updateGsoneMasterbox(data);     
                                                    db.Movement("", "Manual Packer", "Label activator", "1", data);
                                                    updateStatusShipping(data, "1");
                                                    updateStatusMasterbox(data, "0");
                                                    kirimkeDatabaseDistribusi(data);
                                                    string getIdMasterbox = getMasterboxid(data);
                                                    string shippingorder = updatePickinglist(LIST_MASTERBOX[onGoing].MasterboxId);
                                                    UpdateShippingOrderServerDanPickingSlip(shippingorder, LIST_MASTERBOX[onGoing].ShippingOrderNumber, LIST_MASTERBOX[onGoing].ColieMax);
                                                    //updateToServer(data);
                                                    onProgress(false);
                                                    onGoing++;
                                                }
                                                else
                                                {
                                                    //log("Data camera2 masterbox not found");
                                                }
                                            }
                                           // else
                                            {
                                             //   log("Data not progress on weigher");
                                            }
                                        }
                                        else
                                        {
                                            //log("Data Queue Empty");
                                        }
                                    }
                                    else
                                    {
                                        //log("Data Queue Empty");
                                    }
                                }
                                else
                                {
                                    //log("Data from camera error");
                                }
                            }
                            else if (func == 3)
                            {
                                if (dataDapat[i].Length == 3)
                                {
                                    ReceiveData(dataDapat[i][2], dataDapat[i][1]);
                            //        FrontCamera(dataDapat[i][2]);
                                  //  ProsesWeigher(dataDapat[i][1]);
                                }
                                else
                                {
                                    //log("Invalid format data");
                                }
                            }
                            //update data gridnya
                            updateGridData();
                          //  getDataSqlite();
                        }
                    }
                    catch (NullReferenceException ne)
                    {
                        //log("Data from camera error");
                    }


                    //string[] hasilSplit = data.Split(delimit);
                    //// ini weight
                    //if (hasilSplit[0].Length == 0)
                    //{
                    // //   if (cekDataMaster(hasilSplit[1]))
                    //    {
                    //        // baca weight
                    //        hasilSplit[3] = hasilSplit[3].Replace("_", ".");

                    //        string[] dataMasterbox = getMasterbox(hasilSplit[1]);
                    //        db.Movement("", "Input Data Station", "Label aplicator", "1", hasilSplit[1]);
                    //        string shipiingOrderNumber = getShippingOrderNumber(hasilSplit[1]);
                    //        string gsone = simpanMasterbox(hasilSplit[1], shipiingOrderNumber);
                    //        updateWeightMasterbox(hasilSplit[1], hasilSplit[3]);
                    //        string[] dataPribadi = getShippingOrder(shipiingOrderNumber);
                    //        Kirim3Printer(dataPribadi, dataMasterbox, hasilSplit[3]);
                    //    }
                    //}
                    //else
                    //{
                    //    // ini status 
                    //    if (cekDataMasterStatus(hasilSplit[2]))
                    //    {
                    //        db.Movement("", "Input Data Station", "Label activator", "1", hasilSplit[2]);
                    //        updateStatusShipping(hasilSplit[2], hasilSplit[0]);
                    //        updateStatusMasterbox(hasilSplit[2], hasilSplit[0]);
                    //        kirimkeDatabaseDistribusi(hasilSplit[2]);
                    //        updateToServer(hasilSplit[2]);
                    //    }
                    //}
                }
            }
            else
            {
                //log("Data from camera error: " + data);
            }
        }

        private void updateGridData()
        {
            DataTable dat = new DataTable();
            dat.Columns.Add("Master Box ID");
            dat.Columns.Add("GS1 Master Box ID");
            dat.Columns.Add("Weight");
            dat.Columns.Add("Status");
            foreach (Var_Masterbox Row in LIST_MASTERBOX)
            {
                DataRow row = dat.NewRow();
                row[0] = Row.MasterboxId;
                if (String.IsNullOrEmpty(Row.GsOneMasterBoxId))
                    row[1] = "";
                else
                    row[1] = Row.GsOneMasterBoxId;


                if (String.IsNullOrEmpty(Row.Weight))
                    row[2] = "";
                else
                    row[2] = Row.Weight;

                if (Row.Status == "0")
                    row[3] = "On Progress";
                else
                    row[3] = "Done";
                dat.Rows.Add(row);
            }
            wr.dgvGsOne.DataSource = dat;
        }

        private bool CekWeightMasterbox(string data)
        {
            List<string> field = new List<string>();
            field.Add("masterboxId");
            string where = "masterboxId ='" + data + "' and weight <> 0 and status =1";
            List<string[]> ds = sqlite.select(field, "Masterbox", where);
            if (db.num_rows > 0)
            {
                return true;
            }
            return false;
        }        

        private bool cekMasterboxPickinglist(string data)
        {
            List<string> field = new List<string>();
            field.Add("itemid");
            string where = "itemid ='" + data + "' and status = 0";
            List<string[]> ds = db.selectList(field, "pickinglist", where);
            if (db.num_rows > 0)
            {
                return true;
            }
            return false;

        }

        private string getMasterboxid(string data)
        {
            List<string> field = new List<string>();
            field.Add("masterBoxId");
            string where = "masterBoxId ='" + data + "'";
            List<string[]> ds = db.selectList(field, "masterBox", where);
            if (db.num_rows > 0)
            {
                return ds[0][0];
            }
            return null;
        }

        private void UpdateShippingOrderServerDanPickingSlip(string shippingorder, string packingSlip, string colie)
        {
            NoDoclose = "";
            //cek banyak pickinglist nya 
            List<string> field = new List<string>();
            field.Add("itemid");
            string where = "detailno ='" + shippingorder + "' and status = 0";
            List<string[]> ds = db.selectList(field, "pickinglist", where);
            if (db.num_rows == 0 || ds == null)
            {
                ///jika sudah 0 maka update
                Dictionary<string, string> fieldUpdate = new Dictionary<string, string>();
                fieldUpdate.Add("status", "2");
                fieldUpdate.Add("colieSeq", colie);
                where = "shippingOrderNumber ='" + shippingorder + "'";
                db.update(fieldUpdate, "shippingorder", where);
                dbdist.update(fieldUpdate, "pickingList_ERP", "[pickingListNumber] ='" + shippingorder + "'");

                Dictionary<string, string> fieldUpdate2 = new Dictionary<string, string>();
                fieldUpdate2.Add("status", "2");
                where = "[ShipmentOrderNumber] ='" + shippingorder + "'";
                db.update(fieldUpdate2, "[packing_slip]", where);
                dbdist.update(fieldUpdate2, "[packingSlip]", "[pickingList_ERP] ='" + shippingorder + "'");

                //log("DO : " + packingSlip + " Close");
                //new Confirm("DO " + packingSlip + " Close", "Information", MessageBoxButtons.OK);
                NoDoclose = shippingorder;
            }
        }

        private string updatePickinglist(string data)
        {
            Dictionary<string, string> field = new Dictionary<string, string>();
            field.Add("status", "1");
            string where = "itemid ='" + data + "'";
            db.update(field, "pickinglist", where);

            //ambil ordernya 
            List<string> fieldSelect = new List<string>();
            fieldSelect.Add("detailno");
            return db.selectList(fieldSelect, "pickinglist", where)[0][0];
        }

        private bool CheckExistSqlite(string data)
        {
            List<string> field = new List<string>();
            field.Add("masterboxid");
            string where = "masterboxid ='" + data + "'";
            List<string[]> ds = sqlite.select(field, "masterbox", where);
            if (sqlite.num_rows > 0)
            {
                return true;
            }
            return false;
        }

        private string getQtyTotal(string tempDataMasterbox)
        {
            List<string> field = new List<string>();
            field.Add("COUNT(Vaccine.capId)");
            string where = "masterboxid ='" + tempDataMasterbox + "'";
            List<string[]> ds = db.selectList(field, "Vaccine", where);
            return ds[0][0];
        }

        private List<string[]> getbatchnumber(string tempDataMasterbox)
        {
            List<string> field = new List<string>();
            field.Add("d.batchnumber,(select count(capid) from Vaccine where batchnumber = d.batchnumber and  masterboxid = '" + tempDataMasterbox + "') as qty, d.expdate, p.model_name");
            string where = "masterboxid ='" + tempDataMasterbox + "' AND d.shippingOrderDetailNo = sd.shippingOrderDetailNo AND sd.productModelId = p.product_model ";
            List<string[]> ds = db.selectList(field, "masterBox_detail d, shippingOrder_detail sd, product_model p ", where);
            if (ds.Count == 2)
            {
                ds[0][2] = ds[0][2].Insert(2, "-");
                ds[0][2] = ds[0][2].Insert(5, "-");
                ds[1][2] = ds[1][2].Insert(2, "-");
                ds[1][2] = ds[1][2].Insert(5, "-");
                string[] temp = new string[4];
                temp[0] = "-";
                temp[1] = "-";
                temp[2] = "-";                
                ds.Add(temp);
            }
            else if (ds.Count == 1)
            {

                ds[0][2] = ds[0][2].Insert(2, "-");
                ds[0][2] = ds[0][2].Insert(5, "-");
                string[] temp = new string[4];
                temp[0] = "-";
                temp[1] = "-";
                temp[2] = "-";
                ds.Add(temp);
                ds.Add(temp);
            }
            return ds;
        }

        private bool getBanyakVaccine(string data)
        {
            List<string> field = new List<string>();
            field.Add("count(capid)");
            string where = "isreject ='0' and masterBoxId='" + data + "'";
            List<string[]> ds = db.selectList(field, "vaccine", where);
            if (db.num_rows > 0)
            {
                if (!ds[0][0].Equals("0"))
                    return true;
            }
            return false;

            //List<string> field = new List<string>();
            field.Add("isSAS");
            string from = "product_model";
            //string where = "product_model = '" + product_model + "'";
            //List<string[]> ds = db.selectList(field, from, where);
            if (db.num_rows > 0)
            {
                return ds[0][0] == "1";
            }
            return false;

        }

        public bool getStatusMasterbox(string data)
        {
            List<string> field = new List<string>();
            field.Add("masterboxid");
            string where = "isreject ='0'";
            List<string[]> ds = db.selectList(field, "masterbox", where);
            if (db.num_rows > 0)
            {
                if (getBanyakVaccine(data))
                {
                    return true;
                }
            }
            return false;
        }

        private string ambilData(string data)
        {
            data = data.Replace("Gross", "");
            data = data.Replace("kg", "");
            data = data.Replace(" ", "");

            return data;

        }

        private void kirimkeDatabaseDistribusi(string Masterbox)
        {
            string gs = Masterbox.Replace("\n", "").Replace("\r", "").Replace("]d2", "").Replace("\x0A", "").Replace("\\7E1", "").Replace(" ", "");
            db.insertManual(gs);
            string shipping = getShippingOrderNumber(gs);
            //db.insertPickingListERP(getShippingOrderNumber(Masterbox));
            db.insertPickilist(gs, shipping);
            //db.insertReceivingDinkes(shipping);
        }

        //private void kirimkeDatabaseDistribusi(string Masterbox)
        //{
        //    db.insertManual(Masterbox);
        //    string shipping = getshipping(Masterbox);
        //    db.insertPickilist(dapatGSoneMasterbox, shipping);
        //    //db.insertReceivingDinkes(shipping);
        //}

        private void SentToPrinter(Var_Masterbox masterbox)
        {
            try
            {
                if (isConnectPrinter)
                {
                    //log("Send Printer 1");
                    pr.kirimDatags(SetDataSendPrinterLeft(masterbox)); // alamat
                    //log("Send Printer 2");
                    pr2.kirimDatags(SetDataSendPrinterRight(masterbox)); // amaat
                    //log("Send Printer 3");
                    pr3.kirimDatags(SetDataSendPrinterGS(masterbox)); //gs one            
                  //  pr3.kirimDatags(dataKirimPrinter(batchAndQty[0][0], gsonemasterbox[6], gsonemasterbox[8], gsonemasterbox[7], weigh, gsonemasterbox[9], gsonemasterbox[10], gsonemasterbox[11], shippingOrderNumber, gsonemasterbox[2], batchAndQty[0][0], batchAndQty[1][0], batchAndQty[2][0], batchAndQty[0][1], batchAndQty[1][1], batchAndQty[2][1], batchAndQty[0][2], batchAndQty[1][2], batchAndQty[2][2], gsonemasterbox[11], alamat[7]));
                   // pr2.kirimDatags(dataKirimAlamatKanan(alamat[1], alamat[2], alamat[2], alamat[5], alamat[3], alamat[4], "2"));
               //     pr.kirimDatags(dataKirimAlamatKiri(alamat[1], alamat[2], alamat[2], alamat[5], alamat[3], alamat[4], "1"));
                }
            }
            catch (Exception ex)
            {
                //log("Err " + ex.Message);
            }
        }


        void Kirim3Printer(string[] alamat, string[] gsonemasterbox, string weigh, string shippingOrderNumber, List<string[]> batchAndQty, string qtytotal)
        {
            try
            {
                if (isConnectPrinter)
                {
                    pr3.kirimDatags(dataKirimPrinter(batchAndQty[0][0], gsonemasterbox[6], gsonemasterbox[8], gsonemasterbox[7], weigh, gsonemasterbox[9], gsonemasterbox[10], gsonemasterbox[11], shippingOrderNumber, gsonemasterbox[2], batchAndQty[0][0], batchAndQty[1][0], batchAndQty[2][0], batchAndQty[0][1], batchAndQty[1][1], batchAndQty[2][1], batchAndQty[0][2], batchAndQty[1][2], batchAndQty[2][2], gsonemasterbox[11], alamat[7]));
                    updateGsoneMasterbox(dapatGSoneMasterbox);
                    pr2.kirimDatags(dataKirimAlamatKanan(alamat[1], alamat[2], alamat[2], alamat[5], alamat[3], alamat[4], "2"));
                    pr.kirimDatags(dataKirimAlamatKiri(alamat[1], alamat[2], alamat[2], alamat[5], alamat[3], alamat[4], "1"));
                }
                // ngeprint alamat
                //bool cekKonek = false;
                //int batasKirim=0;
                //while (!cekKonek)
                //{
                //    if (pr3.cekKoneksi(sqlite.config["ipprinter3"], sqlite.config["portprinter3"]))
                //    {
                //cekKonek = true;
                //pr3.kirimDatags(dataKirimPrinter(batchAndQty[0][0], gsonemasterbox[6], gsonemasterbox[8], gsonemasterbox[7], weigh, gsonemasterbox[9], gsonemasterbox[10], gsonemasterbox[11], shippingOrderNumber, gsonemasterbox[2], batchAndQty[0][0], batchAndQty[1][0], batchAndQty[2][0], batchAndQty[0][1], batchAndQty[1][1], batchAndQty[2][1], batchAndQty[0][2], batchAndQty[1][2], batchAndQty[2][2], gsonemasterbox[11], alamat[7]));
                //updateGsoneMasterbox(dapatGSoneMasterbox);
                //    }
                //    else
                //    {
                //        log("Top printer not connected");
                //        if (batasKirim == 5)
                //        {
                //            cekKonek = true;
                //            log("Print fail.Check Top printer");
                //        }
                //        else
                //        {
                //            cekKonek = false;
                //            batasKirim++;
                //            log("Top printer reconnect " + batasKirim);
                //        }
                //    }
                //}
                //cekKonek = false;
                //batasKirim = 0;
                //while (!cekKonek)
                //{
                //    if (pr2.cekKoneksi(sqlite.config["ipprinter2"], sqlite.config["portprinter2"]))
                //    {
                //pr2.kirimDatags(dataKirimAlamatKanan(alamat[1], alamat[2], alamat[2], alamat[5], alamat[3], alamat[4], "2"));
                //        cekKonek = true;
                //    }
                //    else
                //    {
                //        log("Right printer not connected");
                //        if (batasKirim == 5)
                //        {

                //            log("Print fail.Check Right printer");
                //            cekKonek = true;
                //        }
                //        else
                //        {
                //            cekKonek = false;
                //            batasKirim++;
                //            log("Right printer reconnect " + batasKirim);
                //        }
                //    }
                //}
                ////Thread.Sleep(9000);

                ////ngeprint status  
                //cekKonek = false;
                //batasKirim = 0;
                //while (!cekKonek)
                //{
                //    if (pr.cekKoneksi(sqlite.config["ipprinter"], sqlite.config["portprinter"]))
                //    {
                //pr.kirimDatags(dataKirimAlamatKiri(alamat[1], alamat[2], alamat[2], alamat[5], alamat[3], alamat[4], "1"));
                //        cekKonek = true;
                //    }
                //    else
                //    {
                //        log("Left printer not connected");
                //        if (batasKirim == 5)
                //        {
                //            log("Print fail.Check Left printer");
                //            cekKonek = true;
                //        }
                //        else
                //        {
                //            cekKonek = false;
                //            batasKirim++;
                //            log("Left printer reconnect " + batasKirim);
                //        }
                //    }
                //}
                //hread.Sleep(9000);


            }
            catch (Exception uy)
            {

            }
        }

        private void updateGsoneMasterbox(string gsone)
        {
            Dictionary<string, string> field = new Dictionary<string, string>();
            field.Add("masterBoxGsOneId", gsone);
            string where = "masterboxid='" + LIST_MASTERBOX[onGoing].MasterboxId + "'";
            db.update(field, "vaccine", where);

            field = new Dictionary<string, string>();
            field.Add("gsOneMasterBoxId", gsone);
            db.update(field, "masterbox", where);
        }

        private bool cekDataMasterStatus(string masterid)
        {
            List<string> field = new List<string>();
            field.Add("masterboxid");
            string where = "masterboxid ='" + masterid + "' OR gsOneMasterBoxId ='" + masterid + "'";
            List<string[]> ds = db.selectList(field, "masterbox", where);
            if (db.num_rows > 0)
            {
                return true;
            }
            return false;
        }

        public void MulaiServer()
        {
            srv = new ServerMultiClient();
            srv.Start(this, sqlite.config["portserver"]);
        }

        public void MulaiServer2()
        {
            srv2 = new ServerMultiClient();
            srv2.Start(this, "2204");
        }

        public void CekDevice()
        {
            try
            {
                //if (PingHost(sqlite.config["ipCamera"]))
                //    wr.lblCamera.Text = "Online";
                //else
                //    wr.lblCamera.Text = "Offline";

                //if (PingHost(sqlite.config["ipCamera2"]))
                //    wr.lblCamera2.Text = "Online";
                //else
                //    wr.lblCamera2.Text = "Offline";

                //if (PingHost(sqlite.config["ipCamera3"]))
                //    wr.lblCamera3.Text = "Online";
                //else
                //    wr.lblCamera3.Text = "Offline";

                //if (PingHost(sqlite.config["ipCamera4"]))
                //    wr.lblCamera4.Text = "Online";
                //else
                //    wr.lblCamera4.Text = "Offline";

                if (PingHost(sqlite.config["ipprinter"]))
                {
                    wr.lblPrinter.Text = "Online";
                    wr.onPrinter.Visible = true;
                    wr.offPrinter.Visible = false;
                }
                else {
                    wr.lblPrinter.Text = "Offline";
                    wr.offPrinter.Visible = true;
                    wr.onPrinter.Visible = false;
                }
                    

                //if (PingHost(sqlite.config["ipprinter2"]))
                //    wr.lblPrinter2.Text = "Online";
                //else
                //    wr.lblPrinter2.Text = "Offline";

                //if (PingHost(sqlite.config["ipprinter3"]))
                //    wr.lblPrinter3.Text = "Online";
                //else
                //    wr.lblPrinter3.Text = "Offline";
            }
            catch (Exception ex)
            {

            }
        }

        public void delay()
        {
            delayTimer = new System.Timers.Timer();
            delayTimer.Interval = 100000;
            delayTimer.Elapsed += new System.Timers.ElapsedEventHandler(delayTimer_Elapsed);
            delayTimer.Start();
        }

        void delayTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            CekDevice();
        }

        public bool PingHost(string nameOrAddress)
        {
            if (nameOrAddress.Length == 0)
                return false;
            bool pingable = false;
            Ping pinger = new Ping();
            try
            {
                PingReply reply = pinger.Send(nameOrAddress);
                pingable = reply.Status == IPStatus.Success;
            }
            catch (PingException)
            {
                // Discard PingExceptions and return false;
            }
            return pingable;
        }

        internal void startstation(string admin)
        {
            adminid = admin;
            db.adminid = adminid;
            db.from = "Input Data Station";
            db.eventname = "Start Station";
            db.systemlog();
            //getDataSqlite();
            //log("Login successfully admin ID = " + adminid);
            //log("Server Port : " + sqlite.config["portserver"]);
            hapusSqlite();
            CekDevice();
            //startConnectPrinter();
        }

        private void startConnectPrinter()
        {
            isConnectPrinter = true;
            pr3 = new print(3);
            bool cekKonek = false;
            int batasKirim = 0;
            while (!cekKonek)
            {
                if (pr3.cekKoneksi(sqlite.config["ipprinter3"], sqlite.config["portprinter3"]))
                {
                    isConnectPrinter = true;
                    //log("Top printer connected");
                    cekKonek = true;
                }
                else
                {
                    //log("Top printer not connected");
                    if (batasKirim == 5)
                    {
                        isConnectPrinter = false;
                        cekKonek = true;
                        //log("Print fail.Check Top printer");
                        wr.lblPrinter.Text = "Online";
                    }
                    else
                    {
                        isConnectPrinter = true;
                        cekKonek = false;
                        batasKirim++;
                        //log("Top printer reconnect " + batasKirim);
                        wr.lblPrinter.Text = "Online";
                    }
                }
            }
            pr2 = new print(2);
            cekKonek = false;
            batasKirim = 0;
            while (!cekKonek)
            {
                if (pr2.cekKoneksi(sqlite.config["ipprinter2"], sqlite.config["portprinter2"]))
                {
                    isConnectPrinter = true;
                    //log("Right printer connected");
                    cekKonek = true;
                }
                else
                {
                    //log("Right printer not connected");
                    if (batasKirim == 5)
                    {

                        isConnectPrinter = false;
                        cekKonek = true;
                        //wr.lblPrinter2.Text = "Offline";
                        //log("Print fail.Check Right printer");
                    }
                    else
                    {
                        isConnectPrinter = true;
                        cekKonek = false;
                        batasKirim++;
                        //log("Right printer reconnect " + batasKirim);
                        //wr.lblPrinter2.Text = "Online";
                    }
                }
            }


            pr = new print(1);
            cekKonek = false;
            batasKirim = 0;
            while (!cekKonek)
            {
                if (pr.cekKoneksi(sqlite.config["ipprinter"], sqlite.config["portprinter"]))
                {
                    cekKonek = true;
                    isConnectPrinter = true;
                    //log("Left printer connected");
                }
                else
                {
                    //log("Left printer not connected");
                    if (batasKirim == 5)
                    {
                        cekKonek = true;
                        isConnectPrinter = false;
                        //wr.lblPrinter2.Text = "Offline";
                        //log("Print fail Check Left printer");
                    }
                    else
                    {
                        //wr.lblPrinter2.Text = "Online";
                        isConnectPrinter = true;
                        cekKonek = false;
                        batasKirim++;
                        //log("Left printer reconnect " + batasKirim);
                    }
                }
            }
        }

        private void hapusSqlite()
        {
            sqlite.delete("", "masterbox");
            sqlite.delete("", "shipping");
        }

        public string[] getMasterbox(string data)
        {
            if (data.Length > 0)
            {
                List<string> field = new List<string>();
                field.Add("dbo.masterBox.masterBoxId,dbo.masterBox.gsOneMasterBoxId,dbo.masterBox.createdTime,dbo.masterBox.isReject,dbo.masterBox.rejectTime,dbo.masterBox.batchnumber,dbo.Vaccine.expDate,dbo.masterBox.serialNumber,dbo.product_model.gtinmasterbox,count(*) as qty,dbo.product_model.model_name,dbo.masterBox.colieNumber");
                string from = "masterBox INNER JOIN Vaccine ON Vaccine.masterBoxId = masterBox.masterBoxId INNER JOIN dbo.product_model ON Vaccine.productModelId = product_model.product_model";
                string where = "masterBox.gsOneMasterBoxId = '" + data + "' OR masterBox.masterBoxId = '" + data + "' and Vaccine.isReject = '0' GROUP BY dbo.masterBox.masterBoxId,dbo.masterBox.gsOneMasterBoxId,dbo.masterBox.weight,dbo.masterBox.createdTime,dbo.masterBox.isReject,dbo.masterBox.rejectTime,dbo.masterBox.customerName,dbo.masterBox.customerAddress,dbo.masterBox.gtin,dbo.masterBox.batchnumber,dbo.Vaccine.expDate,dbo.masterBox.qty,dbo.masterBox.colieNumber,dbo.masterBox.serialNumber,dbo.product_model.model_name,dbo.product_model.gtinmasterbox";

                List<string[]> ds = db.selectList(field, from, where);
                if (db.num_rows > 0)
                {
                    return ds[0];
                }
                else
                    return null;

            }
            else
            {
                return null;
            }
        }

        public string simpanSqliteMasterbox(string[] data, string shiping)
        {
            string dates = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            Dictionary<string, string> field = new Dictionary<string, string>();
            if (data[0].Length > 0)
                field.Add("masterboxid", data[0]);
            if (data[1].Length > 0)
                field.Add("gsOneMasterBoxId", data[1]);
            field.Add("timestamp", dates);
            field.Add("status", "0");
            field.Add("weight", "0");
            field.Add("shippingOrderNumber", shiping);
            sqlite.insert(field, "masterbox");
            return data[0];

        }

        public void simpanSqliteShipping(string[] data, string masterbox)
        {
            string dates = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            Dictionary<string, string> field = new Dictionary<string, string>();
            if (data[0].Length > 0)
                field.Add("customerId", data[0]);
            if (data[1].Length > 0)
                field.Add("customerName", data[1]);
            if (data[2].Length > 0)
                field.Add("customerAddress", data[2]);
            if (data[3].Length > 0)
                field.Add("contactName", data[3]);
            if (data[4].Length > 0)
                field.Add("contactPhone", data[4]);
            if (data[5].Length > 0)
                field.Add("postcode", data[5]);
            if (data[6].Length > 0)
                field.Add("shippingordernumber", data[6]);
            //field.Add("status", "0");
            field.Add("masterboxid", masterbox);
            sqlite.insert(field, "shipping");

        }

        //public string[] getShippingOrder(string data)
        //{
        //    List<string> field = new List<string>();
        //    field.Add("shippingOrder.customerId,shippingOrder.customerName,shippingOrder.customerAddress,shippingOrder.contactName,shippingOrder.contactPhone,shippingOrder.postcode,shippingOrder.shippingordernumber,shippingOrder.colieseq");
        //    string where = "";
        //    if (data.Length > 0)
        //        where = "shippingOrderNumber='" + data + "'";
        //    List<string[]> ds = db.selectList(field, "shippingOrder", where);
        //    if (db.num_rows > 0)
        //    {
        //        return ds[0];
        //    }
        //    return null;
        //}

        public string[] getShippingOrder(string data)
        {
            List<string> field = new List<string>();
            field.Add("s.customerId, uc.customer_name, ua.address_detail, ua.address_contact_name, ua.address_contact_number, ua.address_postal_code, s.shippingordernumber, s.colieseq");
            string where = "";
            if (data.Length > 0)
                where = "s.shippingOrderNumber='" + data + "' AND s.addressId = ua.id AND uc.customer_id = ua.customer_id ";
            string from = " shippingOrder s, user_customer uc, user_customer_address ua ";
            List<string[]> ds = db.selectList(field, from, where);
            if (db.num_rows > 0)
            {
                return ds[0];
            }
            return null;
        }
        
        string cek(string status)
        {
            if (status.Length > 0)
            {
                if (status.Equals("0"))
                    return "On queue";
                else if (status.Equals("1"))
                    return "On Printing";
                else
                    return "Pass";
            }
            else
                return "On Queue";
        }

        public void getDataSqlite()
        {
            List<string> field = new List<string>();
            field.Add("DISTINCT Masterbox.masterboxId 'Master Box ID',Masterbox.gsOneMasterBoxId 'GS1 Master Box',Masterbox.weight 'Weight' ,shipping.shippingOrderNumber 'Order Number',Masterbox.status 'Status'");
            List<string[]> ds = sqlite.select(field, "Masterbox INNER JOIN shipping ON shipping.shippingOrderNumber = Masterbox.shippingOrderNumber ", "");
            if (sqlite.num_rows > 0)
            {
                DataTable dat = sqlite.dataHeader;
                foreach (string[] Row in ds)
                {
                    DataRow row = dat.NewRow();
                    for (int k = 0; k < Row.Length; k++)
                    {
                        if (k == 4)
                            row[k] = cek(Row[k]);
                        else
                            row[k] = Row[k];
                    }
                    dat.Rows.Add(row);
                }
                wr.dgvGsOne.DataSource = dat;
            }
            else
            {
                wr.dgvGsOne.DataSource = sqlite.dataHeader;
            }

        }

        public string simpanMasterbox(string masterbox, string shipping)
        {
            string gsone = simpanSqliteMasterbox(getMasterbox(masterbox), shipping);
            simpanSqliteShipping(getShippingOrder(shipping), gsone);
            return gsone;
        }

        public void updateStatusMasterbox(string masterbox, string status)
        {
            Dictionary<string, string> field = new Dictionary<string, string>();
            field.Add("status", status);
            string where = "gsOneMasterBoxId ='" + masterbox + "'";
            sqlite.update(field, "masterbox", where);
        }

        public void updateWeightMasterbox(string masterbox, string weight)
        {
            Dictionary<string, string> field = new Dictionary<string, string>();
            field.Add("status", "1");
            field.Add("weight", weight);
            string where = "masterboxid ='" + masterbox + "'";
            sqlite.update(field, "masterbox", where);

            field = new Dictionary<string, string>();
            field.Add("weight", weight);
            db.update(field, "masterbox", where);
        }

        public void updateStatusShipping(string masterbox, string weight)
        {
            Dictionary<string, string> field = new Dictionary<string, string>();
            field.Add("status", weight);
            string where = "masterboxid ='" + masterbox + "'";
            sqlite.update(field, "shipping", where);
        }

        string getshipping(string masterbox)
        {
            List<string> field = new List<string>();
            field.Add("shipping.shippingOrderNumber");
            string where = "masterbox.gsOneMasterBoxId = '" + masterbox + "' OR masterbox.masterboxid = '" + masterbox + "'";
            List<string[]> ds = sqlite.select(field, "shipping inner join masterbox on masterbox.shippingOrderNumber = shipping.shippingOrderNumber ", where);
            if (sqlite.num_rows > 0)
            {
                return ds[0][0];
            }
            return "";
        }

        public string getShippingOrderNumber(string masterbox)
        {
            string sql = "SELECT " +
                        " DISTINCT dbo.shippingOrder.shippingOrderNumber" +
                        " FROM" +
                        " Vaccine AS a" +
                        " INNER JOIN pickingList AS b ON a.capId = b.itemId OR a.innerBoxId = b.itemId OR a.masterBoxId = b.itemId" +
                        " INNER JOIN masterBox ON masterBox.masterBoxId = a.masterBoxId" +
                        " INNER JOIN shippingOrder_detail ON shippingOrder_detail.shippingOrderDetailNo = b.detailNo OR shippingOrder_detail.shippingOrderNumber = b.detailNo " +
                        " INNER JOIN shippingOrder ON shippingOrder_detail.shippingOrderNumber = shippingOrder.shippingOrderNumber WHERE dbo.masterBox.masterBoxId = '" + masterbox + "' OR dbo.masterBox.gsOneMasterBoxId = '" + masterbox + "'";
            
            string sql2 = "SELECT dbo.pickingList.detailNo "+
                          "FROM [dbo].[pickingList] "+
                          "WHERE dbo.pickingList.itemId = '" + masterbox + "'";
            List<string[]> ds = db.selectListManual(sql);
            if (db.num_rows > 0)
            {
                return ds[0][0];
            }
            return null;
        }

        public string getPackingListId(string shippingOrderNumber)
        {
            string sql = "SELECT packingSlipId " +
                         "FROM packing_slip " +
                         "WHERE ShipmentOrderNumber = '" + shippingOrderNumber + "'";
            List<string[]> ds = db.selectListManual(sql);
            if (db.num_rows > 0)
            {
                return ds[0][0];
            }
            return null;
        }

        public void CekDeviceDelay()
        {
            delaydevice = new System.Timers.Timer();
            delaydevice.Interval = 10000;
            delaydevice.Elapsed += new System.Timers.ElapsedEventHandler(delaydevice_Elapsed);
            delaydevice.Start();
        }

        void delaydevice_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            CekDevice();
        }

        string SetDataSendPrinterGS(Var_Masterbox masterbox)
        {

            string createdtime = db.getUnixString(double.Parse(masterbox.CreatedTime));
            dapatGSoneMasterbox = "01" + masterbox.Gtinmasterbox + "13" + createdtime.Replace("/", "") + "400" + masterbox.ShippingOrderNumber.Replace("-", "") + "21" + masterbox.SerialNumber;

            string kirimBetul = "CT~~CD,~CC^~CT~\n" +
                              "~DG000.GRF,04608,016,\n" +
                              ",:::::::::::::::::::::::::::::::::L01E,L0HFE0,K01FHFC,K03FIFC0,K07FJFC,K07FKF,K0FE1FIF80,K0F801FHFC0,K0F8007FFC0,K0F8007FFE0,K0F8007C3F0,K0F8007C038,K0FC007C0,:K07E007C0,:K07F007C0,K03FC0FC0,K01FIF80,L0JF80,L07FHF,L03FFE,L01FFC,M03F0,,K0E,K0FC0,K0HFC,K0IFD0,K0JF8,K0KF80,L0JFC0,L01FHFC7,M01FFCF80,N01FCFC0,O01C780,Q07C0,L03F0H0380,L0HFC0,K03FHF0,K07FHF8,K07FHFC,K0HF7FE,K0FC0FF,K0FC07F,K0F803F80,K0F801F80,K0F800F80,K0F800FC0,:K0FC007C0,K07E007C0,K07F007C0,K07F80FC0,K03FC1FC0,K01FIF80,L0JF,L07FHF,L03FFE,H0780H0HFC,H03F8001E0,H01FF8,H01FHF,I0JF0,I07FIF,I03FIFC0,J07FIF0,K0JF8,J01FIFC,K0F8FFE,K0781FF,K07807F,K03803F,K01801F80,K01800FC0,L0800FC0,O07C0,:::O0FC0,N01F80,:O0F80,L01007,L03F06,L0HFC0,K03FHF0,K07FHF8,K07FHFC,K0HF7FE,K0FC0FE,J01FC07F,K0F803F80,K0F801F80,K0F800F80,J01F800FC0,K0F800FC0,K0F8007C0,:J01F8007C0,K0FC00FC0,K0HFC1FC0,K0KF80,:K0KF,K01FHFE,L01FF8,M01E0,,K0F,K0FE0,K0HFE,K0IFC0,K0JF0,K07FHF8,L07FFC,M0HFE,M01FF,N07F,N03F80,N01F80,N01FC0,O0FC0,N017C0,O07C0,::O0FC0,:N01FC0,K0F0H0F80,J01FF007,K0HFE06,K0IFC0,K0JF0,K07FHF8,L07FFC,M07FE,M01FE,N07F,N03F80,N01F80,O0F80,O0FC0,:O07C0,:K0C0H07C0,K0FC00FC0,K0HFC1FC0,K0KF80,K0KF,:K01FHFE0M040,L01FFE,L0H1HFN020,N0HFN020,N03F80L030,N01F80L030,N01F80L010,O0FC0L018,O0FC0L0180L08,O07C0L0180K010,O07C0L0140K030,K080H07C0L01C010I040,K0F800FC0L01E0220H080,K0HFH0FC0L01F0E2003,J01FJFC0L01FFC3007,K0KF80L01FFC300E,K0KFL0303FF8303C,K07FHFE0L0JF03CF8,L07FFC0K053FHF03FF0,M07F80K040FFE03FF0,V0207FC03FF0,V0300F803FE0,V030J03FE0,V0380I01FF0,L07F0M0380I01FF0,K01FFE0H078007C0J0HFC,K03FHFJ07F1FC0J01FFD0,K07FHF80H01FHFC,K07FHFC0I07FFE,K0FE3FE0J0HFE0M040,K0FC07F0J07FE0L01C0,K0F807F0J03FE0L03,K0F801F80I01FC0L0F,K0F801F80I01F80K01E,J01F800FC0I01F0L07E,K0F800FC0I01C0L0FE,K0F8007C0I0380K01FE,K0F8007C0I060L01FF,J01F8007C0I0C0L01FFC0,K0F8007C0Q01FFE0,K0FC00FC0Q01FHFC,K0HFC1F80H03FFC0K0JF80,J01FF7FF80I01FF80J0FC01F0,K0KF80J07FC0J0F0I0C,K0KFL03FE0J070,K01FHFE0K03FE0J030,L01FFC0K03FF0038030,M03E0L03FF00FF010,V03FF01FFC10,V07FF03FHF08,V07FF03FHFD0,V0F0707FF060,U01C030FFE0,U038030FFE0,U070011C3C0,U04001103C0,U08001201C0,T010H010H0C0,T020H010H0D0,gH060,:gH020,::gH010,::,::::::::::::::::::::::::::::::::~DG001.GRF,02304,008,\n" +
                              ",:0F,::0F0FLF,::::0F0517F515,0F0H03F8,0F0H01FC,0F0I0FE,0F0I07F,0F0I07F80,0F0H01FFC0,0F0H03FFE0,0F0017FHF0,0F003FF3F8,0F007FC1FC,0F03FF80FE,0F07FF007F,0F0FFC003F,0F0FF0H01F,0F0FE0I0F,0F0F40I07,0F0F0J03,0F0C0J01,0F08,0F,:0F005FC0,0F00FHF8,0F01FHFC,0F03FHFE,0F07FIF,0F07E03F,0F07C01F,0F0F800F80,0F0F0H0780,:::0F070H07,0F07800F,0F07D01F,0F03FBFE,0F03FHFE,0F01FHFC,0F017FF0,0F003FE0,0F0H05,0F,::0F005FD0,0F00FHF8,0F01FHFC,0F03FHFE,0F07FIF,0F07E03F80,0F07C01F,0F0F0H0780,::0F070H07,:0F07C00F,0F03C01E,0F05F77F77,0F0FLF,:::0F02L2,0F,::0F0H02,0F005FD0,0F00FHF8,0F01FHFC,0F03FHFE,0F07F77F,0F07C71F,0F070707,0F0F070780,::0F0F0707,0F0F070F,0F07C71F,0F07C7FF,:0F0387FE,0F0187FC,0F0087E0,0F0H01,0F,::::::::::::::0F07L7,0F0FLF,:::0F0223E22F,0F0H01E00F,:::::0F0H01F01F,0F0H01F00F,0F0H01F01F,0F0I0FCBE,0F0H01FIF,0F0I07FFE,0F0I07FFC,0F0I03FF8,0F0I017D0,0F,::0F0015,0F003FE0,0F01FHFC,0F03FHFC,0F07FIF,:0F07C71F,0F0F870F80,0F07070780,0F0F070780,::0F070717,0F07871F,0F07C7FF,0F0787FE,0F0387FC,0F0187F8,0F0H07D0,0F,::0F0J0780,:0F015H57D0,0F03FJF8,0F07FJFC,0F0FKFE,0F0FLF,0F0FBABFBA,0F0F0H0780,:0F070H0780,0F02,0F,:0F001I1,0F00FIF80,0F01FIF80,0F03FIF80,0F07FIF80,0F0FJF80,0F0FD404,0F0F80,0F0F,:0F07,:0F0780,0F03E0,0F05FFDD80,0F0FJF80,:::0F,::::0F405750,0FE0FHF8,0FC1FHFC,0FC3FHFE,0FC7FF7F,0FC7E03F,0FCFC01F,0F0F0H0F80,0F0F0H0780,:0F070H07,0F078007,0F07801F,0F83E03E,0FDDF57D,0FLF80,::0F7FJF80,0F,::::0F01F010,0F03FC1C,0F07FC1C,0F0FFE3F,0F07DF3F,0F0F8E0F,0F0F0707,0F0F070780,0F07070780,0F07030780,0F07870780,0F03C38F80,0F01F7FF,0F0FJF,:0F0FIFE,0F0FIF4,0F0E,0F,:::0F01C07C,0F03C1FE,0F07C1FF,0F07E3FF,0F07C7FF,0F0F87C780,0F0707C780,0F0F078780,0F0F07C780,0F0F0F8780,0F0F1F87,0F0F0F8F80,0F07DF1F,0F07FF0F,0F07FF1F,0F03FE0E,0F01FC0C,0F00F8,0F0010,0F,,:::::::::::::::::::::::~DG002.GRF,02688,012,\n" +
                              ",:L0F0,::L0F0FLF0,::::L0F0F157D1F0,L0F0F00780F0,::::::::L0F07007C1F0,L0F0F80FEBE0,L0F07F7FIF0,L0F07FFEFFE0,L0F07FFC7FC0,L0F03FFC3F80,L0F017F015,L0F003E0,L0F0,::L0F0H020,L0F005FD,L0F00FHF80,L0F01FHFC0,L0F03FHFE0,L0F07F77F0,L0F07C71F0,L0F0707070,L0F0F07078,::L0F0F07070,L0F0F070F0,L0F07C71F0,L0F07C7FF0,:L0F0387FE0,L0F0187FC0,L0F0087E,L0F0H010,L0F0,::L0F0551550,L0F0FJF8,:::L0F0FIFE0,L0F0I05F0,L0F0J0F0,:L0F0J0F8,::L0F0J070,L0F0,L0F004,L0F03F80,L0F07FC1C0,L0F07FE1E0,L0F07FE1F0,L0F0FFE3F0,L0F0F071F0,L0F0F07078,::L0F0707078,L0F0383078,L0F03C39F0,L0F03FIF8,L0F07FIF0,L0F0FJF0,L0F0FIFE0,L0F0FIFC0,L0F0D1H1,L0F0,::L0F0J078,:L0F015H57D,L0F03FJF80,L0F07FJFC0,L0F0FKFE0,:L0F0FAHAFAA0,L0F0F0H078,:L0F070H078,L0F0,::::::::::::::L0F0D5H5D550,L0F0FLF0,:::L0F0H07F,L0F0H03F,L0F0H01F80,L0F0I0FC0,L0F0I07F0,:L0F0H01FFC,L0F0H07FHF,L0F0H0JF,L0F003FF3FC0,L0F00FFE1FE0,L0F01FFC0FF0,L0F0FFE007F0,L0F0FFC003F0,L0F0FF0H01F0,L0F0FC0I070,L0F0F80I070,L0F0F0J010,L0F0C0J010,L0F0,:L0F0H010,L0F003FE,L0F007FF,L0F01FHFC0,L0F03FHFD0,L0F03FHFE0,L0F07D05F0,L0F07800F0,L0F070H070,L0F0F0H078,::::L0F07C01F0,:L0F07F57F0,L0F03FHFE0,L0F01FHFC0,L0F00FHF80,L0F005FF,L0F0H020,L0F0,:L0F0J078,:L0F015H57D,L0F03FJF80,L0F07FJFC0,L0F0FKFE0,L0F0FLF0,L0F0FAHAFAA0,L0F0F0H078,:L0F070H078,L0F0,::L0F005FC,L0F00FHF80,L0F01FHFC0,L0F03FHFE0,L0F07FIF0,L0F07E03F0,L0F07C01F0,L0F0F800F8,L0F0F0H078,:::L0F070H070,L0F07800F0,L0F07D01F0,L0F03FBFE0,L0F03FHFE0,L0F01FHFC0,L0F017FF,L0F003FE,L0F0H054,L0F0,::L0F05J50,L0F0FJF8,:::L0F0FIFE0,L0F01015F0,L0F0J0F0,:L0F0J0F8,::L0F0J070,,::::::~DG003.GRF,03456,012,\n" +
                              ",:::::P01FF4,P07FFE,O01FIFC0,O03FIFE0,O07FF7FF0,O07F00FF8,O07C005FC,O0F80H0FC,O070I07C,O0F0I03E,O0F0I01F,O0F0J0E,O0F0J0F,:O070J0F,O0780I0F,O07C0H01F,O03E0H01F,O01F4001F,O01FE003E,O01FF55FC,P07FIFC,:Q0JF0,Q07FFD0,R0BF80,M05,M0FE0,M0IF,M0JF8,M0KF50,M03FJFE,N017FIF80,O01FIF80,O01F7FF80,O03C07F80,O07001F,O0F0H0E,O0F0H07,::O0F800780,O07C00780,O07E00F80,O07FD7F,O03FIF,O01FIF,P07FFE,P017F4,Q02A0,,:P015,P03F80,O01FHF0,O03FHF8,O07FHFC,O07FHFE,O07C77F,O0F871F,O0F070F,O0F070780,::O07070780,O07870F80,O07C71F,O0387FF,O0187FF,O0187FE,Q07FC,Q03E0,,:O0D40,O0HF8,O0IF50,O0JFE,O0KF80,P0JF80,P017FF80,R0HF80,R07D,R01C,R01F,S0F,:S0F80,P0100780,O01F00380,O07FC01,O07FE,O07FF1C,O0HFE1E,O0F071F,:O07071F,O07030780,O07070780,O07830780,O03C30780,O0FE38780,O0IF8780,O0KF80,O0KF,:O045FHF,Q03FC,Q0H10,,::O015005,O07F80780,O07FFD780,O0KF80,O0KFC0,O0KFE0,O0F17FHF0,O0F003FF8,O070017FC,O010H07BC,S0780,S0380,P01501,P07F80,O01FHF0,O03FHF8,O07FHFC,O07FBFE,O07C07F,O0F801F,O0F001F,O0F0H0780,::O070H0780,O07800780,O07C007,O03F81F,O03FF7F,O01FHFE,P0IFC,P03FF8,P017F0,,::O0F,O0HF8,O0IF40,O0JFE,O07FIF80,P0JF80,P017FF80,R0HF80,R075,R03C,R01F,S0F,:S0F80,S0780,S0380,S01,,::::::::::P017F4,P07FFE,O01FIFC0,O03FIFE0,O07FJF0,O07F83FF8,O07C005FC,O0F80H0FC,O070I07C,O0F0I03E,O0F0I01F,O0F0J0E,O0F0J0F,:O0780I0F,:O07F0H01F,O03F8001E,O01F0H05E,O01F800FE,O017001FC,P03001F8,P01001F0,T0E0,S01,,P014,P07F80,O01FHF0,O03FHF8,O07FHFC,O07FHFE,O07D17F,O0F803F,O0F001F,O0F0H0F80,O0F0H0780,:O070H0780,O07800780,O07D007,O03F81F,O03FIF,O01FHFE,O017FFD,P03FF8,P017F0,,::P014,P0HF80,O01FHF0,O03FHF8,O07FHFC,O0JFE,O0FD17F,O0F803F,O0F0H0F,O0F0H0780,O070H0780,:O078007,O0BC00F80,O0HF01F,O0HFE3E,O0JFC,O0JFE,O07FJF0,P03FJF,Q05FIF,R03FHF,R0H17F,T03F,Q040H01,P03F80,O01FHF0,O03FHF8,O07FHFC,O07FHFE,O07D77F,O0F871F,O0F070F,O0F070780,::O07070780,O07870F80,O07C71F,O0387FF,O01C7FF,O0187FE,O0107FC,Q03A0,,:::::::::::~DG004.GRF,02048,008,\n" +
                              ",:I01E002,I01E00E,I01E03E,I01E07E,I01E0FE,I01E0FF,I01E0FA,I01E1F0,I01E1E0,:::I01E0E0,I01E0FC,I01E0FKFE0,I01E07FJFE0,I01E03FJFE0,I01E01FJFE0,I01E00AFHFBE0,I01E0,::::I01E002I20,I01E01FIF0,I01E03FIF0,I01E0FJF0,:I01E1FJF0,I01E1FA08,I01E1F0,I01E1E0,:I01E0E0,:I01E0F0,I01E07C,I01E0BFFBB0,I01E1FJF0,:::I01E0,::::I01E0AJA0,I01E1FJF0,::I01E1FIFE0,I01E17H7FC0,I01E0I03C0,I01E0I01E0,I01E0J0E0,I01E0J0F0,::I01E02AABF0,I01E1FJF0,I01E1FIFE0,:I01E1FIFA0,I01E1FIF80,I01E0I03C0,I01E0I01C0,I01E0J0E0,I01E0J0F0,::I01E02H23F0,I01E1FJF0,I01E1FIFE0,::I01E1FIF,I01E00808,I01E0,::::I01E0ALA0,I01E1FKFE0,:::I01E04L40,I01E0,::I01E004,I01E03E02,I01E07F8380,I01E0FF83E0,I01E0FFC7E0,I01E1FBC7E0,I01E1F1C1F0,I01E1E0E0E0,I01E1E0E0F0,I01E0E0E0F0,I01E0E060F0,:I01E07871F0,I01E03FHFE0,I01E1FIFE0,:I01E1FIFC0,I01E1FIF80,I01E1C0,I01E0,::::I01E1FKFE0,::::I01E0AHABAHA0,I01E0I01C0,I01E0J0E0,::I01E0J0F0,:I01E0I01F0,I01E1FBBFE0,I01E1FIFE0,:I01E1FIFC0,I01E1FIF80,I01E0,:::::::::::::::I01E0K01E0,::::::I01E08J89E0,I01E1FKFE0,::::I01E02J23E0,I01E0K01E0,::::::I01E0L020,I01E0,I01E00BF8,I01E01FHF,I01E03FHF80,I01E07FHFC0,I01E0FIFE0,I01E0FC07E0,I01E0F803E0,I01E1F001F0,I01E1E0H0F0,:::I01E0E0H0E0,I01E0F001E0,I01E0FA03E0,I01E07F7FC0,I01E07FHFC0,I01E03FHF80,I01E02FFE,I01E007FC,I01E0H0A0,I01E0,I01E0J020,I01E0J0F0,:I01E015H5F5,I01E0FKF80,::I01E1FKFC0,I01E1EAHAFAA0,I01E1E0H0F0,I01E0E0H0F0,I01E1E0H0F0,I01E0A0H0A0,I01E0,I01E008,I01E07F,I01E0FF8380,I01E0FFC3C0,I01E0FFC3E0,I01E1FFC7E0,I01E1E0E3E0,I01E1E0E0F0,::I01E0E0E0F0,I01E07060F0,I01E07873E0,I01E07FIF0,I01E0FIFE0,I01E1FIFE0,I01E1FIFC0,I01E1FIF80,I01E1A2H2,I01E0,::::I01E0ALA0,I01E1FKFE0,:::I01E0,::,:::::::::::~DG005.GRF,02304,008,\n" +
                              ",::::N020,L017FD,L03FHF80,L07FIF0,L0KF8,K01FD5FFC,K01F800FE,K01F0H07F,K03E0H01F,K01C0I0F,K03C0I0F80,K03C0I07C0,K03C0I0380,K03C05007C0,K03C07803C0,K01C07803C0,K03E07803C0,K01E07807C0,K01E07803C0,K01F47807C0,L0IF80F80,L0IF85F80,L0IF87F80,L07FF87F,M0HF83E,M04587C,P020,,:K03C0,K03FE,K03FFD0,K03FIF80,K01FIFE0,L03FHFE0,M05FFE0,N03FE0,N01D40,O0F,O07C0,O03C0,:O03E0,O01E0,P0E0,L05FD040,L07FFC,L0JF,:K01FDFFC0,K01E01FC0,K01C007C0,K03C003E0,K03C001C0,K03C001E0,:K03E001E0,K01F001C0,K01F803E0,K01FD07C0,L0JFC0,L07FHFC0,L03FHF80,L01FHF,M03F8,N040,,L010,L030,L070,L0F81C,K01F07F,K01F07F80,K01C0FFC0,K03C0FFC0,K03C1F1C0,K03C1F1E0,:K03C3E1E0,K01C3E1E0,K03E3E1E0,K01FFC1C0,L0HFC3E0,K01FFC7C0,L07F83C0,L05F07C0,M080380,O01,,L010,L070,:L0F83E,K01F87F,K01F8FF80,K01E0FFC0,K03C0FFC0,K03C1F5C0,K03C1F1E0,:K03C3E1E0,K01C7E1E0,K01E7C1E0,K01F7C1E0,K01FFC3E0,L0HFC7C0,L0HF87C0,L07F07C0,O0380,O05,,::::::::::::::P04040,O0AFFC0,K03FKFC0,:::K03FF5H50,K03FF,K017FC0,L03FF0,L017FC,M03FF,N07FC0,N03FF0,N017FC,O03FF,O017FC0,P03FC0,K015H57FFC0,K03FKFC0,::K03FF5I540,K03FE,K01FFC0,L07FE0,L01FF4,M07FE,M01FFC0,N07FE0,N01FFC,O03FF,O01FFC0,P07FC0,P01FC0,Q03C0,L01FD001C0,L03FFC,L07FHF,L0JF,K01FDFFC0,K01E1CFC0,K01C1C7C0,K03C1C3E0,K03C1C1C0,K03C1C1E0,:K03E1C1E0,K01E1C1C0,K01F1C3C0,K01F1FFC0,L0E1FF80,L061FF,M01FE,M0154,,:K03C0,K03F5,K03FFE0,K03FHFD40,K03FIFE0,K017FHFE340,M03FFE3C0,N05FE3C0,P0E3C0,Q03C0,J030L0C0,J0F00440,J0E03FE0,I01F07FFC,I01E0FHFE,I01F1FIF,I03C3FEFF80,I03C3F01FC0,I03C3E007C0,I03C1C005C0,I03C3C001E0,I01C1C001E0,I03E1E001E0,I01F5E001C0,I01FEF003C0,I01FHFC07C0,J0JFAF80,J07FJF,K0KFC0,K05FIFE0,M0IFE0,M015FE0,O0BE0,P040,,K03C0,K03FC,K03FFD0,K03FHFE,K01FIFD4,L0LF80,M05FIFC0,N03FHFC0,O0F7FC0,O078FC0,O038140,O0180,O01C0,K038001E0,K03F501E0,K03FFE3E0,K03FIFE0,:K017FHFC0,M03FFC0,N05F40,,:::L0C00180,K01FD01E0,K01FFE1E0,K01FHFDE0,K03FJF8,K03FJFC,K03C3FHFC,K03C057FC,K03C001FE,L04001F7,O01E0,O0160,,::::::::::::::::::::::::::::~DG006.GRF,02304,012,\n" +
                              ",:M03C0,::M03C3FKFC0,::::M03C04H45FFC0,M03C0I01FF,M03C0I07FC,M03C0I0HF8,M03C0H07FC0,M03C0H0HFC0,M03C001FF,M03C007FC,M03C05FF0,M03C07FE0,M03C1FFC0,M03C3FF,M03C3FFDIDC0,M03C3FKFC0,::M03C1DKDC0,M03C0,::::M03C017F0,M03C03FFE,M03C07FHF,M03C0FIF80,M03C1FIFC0,M03C1F80FC0,M03C1F007C0,M03C3E003E0,M03C3C001E0,:::M03C1C001C0,M03C1E003C0,M03C1F407C0,M03C0FEFF80,M03C0FIF80,M03C07FHF,M03C05FFC,M03C00FF8,M03C00140,M03C0,:::M03C3E0,::::M03C040,M03C0,:::::::::::::::M03C3FKFC0,::::M03C3D55F57C0,M03C3C01E03C0,::::::::M03C1C01F07C0,M03C3E03FCF80,M03C1F57FHF80,M03C1FHFBFF80,M03C1FHF1FF,M03C0FFE0FE,M03C07FC054,M03C00F80,M03C004,M03C0,::M03C07C04,M03C0FF07,M03C1FF07,M03C3FF8FC0,M03C1F7CFC0,M03C3E383C0,M03C3C1C1C0,M03C3C1C1E0,M03C1C1C1E0,M03C1C0C1E0,M03C1E1C1E0,M03C0F0E3E0,M03C07DFFC0,M03C3FIFC0,:M03C3FIF80,M03C3FHFD,M03C380,M03C0,:M03C0J040,M03C0I01E0,:M03C02AABEA,M03C1FKF,::M03C3FKF80,M03C3D5H5F540,M03C3C001E0,M03C1C001E0,M03C3C001E0,M03C1400140,M03C0,M03C001,M03C00FF8,M03C05FFC,M03C07FHF,M03C07FHF80,M03C0FIF80,M03C1F447C0,M03C1E003C0,M03C1C001C0,M03C3C001E0,:::M03C3E003E0,M03C1F40FC0,M03C1FC0FC0,M03C1FC07C0,M03C0F80780,M03C05C04,M03C008,M03C0,:::M03C35H575540,M03C3FKFC0,:::M03C0I0F80,M03C0I07C0,M03C0I01C0,:M03C0I01E0,::M03C0I07E0,M03C3FIFE0,M03C3FIFC0,:M03C3FIF80,M03C3FHFE,M03C04I4,M03C0,:,::::::~DG007.GRF,02304,008,\n" +
                              ",::::::O01C0,O01E0,:J0A0I01E0,I01FC0H01E0,I01FFA801E0,I01FHFD01E0,I01FIFEBE0,I01FKFE0,J02FJFE0,L07FHFE0,L02BFFE0,N07FE0,O03E0,O01E0,::::::K0280H0E0,K0HF0,J03FFE,J07FHF,J0JF80,J0JFC0,J0F82FE0,I01F007E0,I01E003E0,I01E001F0,I01E0H0F0,:J0E0H0F0,J0F0H0F0,J0FA00E0,J07F03E0,J07FHFE0,J03FHFC0,J02FHF80,K07FF,K02FE,,:::J050H0C0,J0FEA0F0,J0IF4F0,J0KF0,I01FJFC,I01EFIFE,I01E07FFE,I01E00BFF,I01E0H0HF,J020H0F280,N0F0,K0800B0,J01F,J0HF80,J0HFC0,J0HFC380,I01FFC1C0,I01E2E3E0,I01E0E3E0,J0E0E3E0,I01E061F0,J0E0E0E0,J0F060F0,J0F860F0,I01FC70F0,I01FHF0E0,I01FJF0,I01FIFE0,:J0ABFFE0,L07F80,M0A,,J0A0,I01F0,I01FEA0,I01FHFC,I01FIFE0,I017FIFC,J02FJFA0,L07FHFE0,M0AFFE0,N07FE0,O0AE0,,:::::::::::L040,K0HFE,J01FHFC0,J03FHFE8,J07FIF8,J0HFEFFE,I01FC07FF,I01F800FF80,I01E0H01F80,I03E0I0F80,I03C0I07C0,I03C0I03C0,I03C40H01C0,I03CE0H03E0,I03FE0H01E0,:I01FC0H01E0,I03F80H03E0,I03FC0H03E0,I0HFE0H03E0,I0IF800FC0,I0FBFEABF80,I0E1FJF80,I060FJF80,K03FHFE,K02FHFA,L01FE0,N080,,J020,J07F,J0HFE8,I01FIF,J0JFE0,I01FJF0,I01EAFHF0,I01E01FF0,J0E002F0,J0E0,:J070,J0B8,I01FE,I01FFA8,I01FIF,I01FIFE0,J07FIF0,K0BFHF0,L01FF0,M02F0,N010,,:J02E,J07F80,J0HF82,I01FFC1,J0FBE380,I01F1E3C0,I01E0E3E0,I01E0E1E0,J0E0E0E0,J0E060F0,J0E0E0F0,J07060F0,J0FE60F0,I01FHF1F0,I01FHFBE0,I01FIFE0,:K07FFC0,L0BF80,,::I01A8,I01FF,I01FFEA,I01FIFC0,I01FJF0,J01FIF0,K02FHF0,L01FF0,M0FA0,M0380,M01E0,N0E0,:I01C0H0F0,I01FE00F0,I01FFC1F0,I01FJF0,:J0AFHFE0,K05FFE0,L0AF80,,:::J010H0C0,J0FEA0F0,J0IF4F0,J0KF0,I01FJFC,I01EFIFE,I01E07FFE,I01E00BFF,I01E0H0HF,J020H0F280,N0F0,N0B0,I01C0,I01FE80,I01FHF0,I01FHFE80,I01FJF0,J0AFIF1A0,K01FHF1E0,L02FF1E0,M0171E0,N020E0,P060,,J010H0C0,J0FEA0F0,J0IF4F0,J0KF0,I01FJFC,I01EFIFE,I01E07FFE,I01E00BFF,I01E0H0HF,J020H0F280,N0F0,H0E0J0B0,01E0,01E0J020,01F0I07F0,01F80ABFF0,01FC7FIF0,H0MF0,H07FKF0,H03FHFHA,I0HF0,I03FE,I01FF,J0BFE0,J01FF0,K03FE,K01FF80,L0HFE0,L01FF0,M0HF0,M03F0,N0F0,N010,,:::::::::::::::::::::~DG008.GRF,00768,012,\n" +
                              ",::::::::::::::P028,P070H01C,P0F8003E,O01F8001F,O03F8003F,O03E0H01F,O0380I0F80,O07801C0780,O07801E0780,:O03801F0F80,O03C03F9F,O03EAFIF,O03FKF,O01FHFBFE,P0IF1FC,P03FE0A8,P01F80,Q08,,:::::::::::::::::::::::::::::~DG009.GRF,00512,008,\n" +
                              ",:::::::::::::H010,H01E0I0A0,H01F8001F0,H01FE0H0F8,H01FE001FC,H01FF801FC,H01FFC007C,H01FFE003E,H01F7E001E,H01F3F801E,H01F1F801E,H01F0FE03E,H01F07E03E,H01F03F8BE,H01F01FHFC,H01F00FHF8,H01F007FF0,H01F003FE0,H01F0H07C0,I020H02,,:::::::::::::::::::::::::::::~DG010.GRF,00512,008,\n" +
                              ",:::::::::::::::O03C0,O03E0,O01E0,P0E0,P0F0,P0F8,P07C,K02AIAFE,K07FKF80,:::K0M5,,::::::::::::::::::::::::::::::::::~DG011.GRF,01920,012,\n" +
                              ",:K0780,::K0787FKF80,::::K07808H8BFF80,K0780I03FE,K0780I0HF8,K0780H01FF0,K0780H0HF80,K078001FF80,K078003FE,K07800FF8,K0780BFE0,K0780FFC0,K0783FF80,K0787FE,K0787FFBIB80,K0787FKF80,::K0783BKB80,K0780,::::K07802FE0,K07807FFC,K0780FHFE,K0781FIF,K0783FIF80,K0783F01F80,K0783E00F80,K0787C007C0,K07878003C0,:::K0783800380,K0783C00780,K0783E80F80,K0781FDFF,K0781FIF,K0780FHFE,K0780BFF8,K07801FF0,K07800280,K0780,:::K0787C0,::::K078080,K0780,:::::::::::::::K0787FKF80,::::K0782ABFAHA80,K078001FC,K0780H0FE,K0780H07F,K0780H03F80,K0780H03FC0,K0780H0HFE0,K078001FHF0,K07800FIF8,K07801FFDFC,K0780BFE8FE,K0781FFC07F80,K0783FF803F80,K0787FE001F80,K0787F80H0F80,K0787F0I0780,K0787E0I0380,K07870J0180,K07860K080,K0780,::K07800BE8,K07807FF8,K0780FHFE,K0781FIF,K0783FBBF80,K0783F01F80,K0783E00F80,K0787C007C0,K0783800380,K07878003C0,::K0783800380,K0783C00780,K0783E02F80,K0781FD7F,K0781FIF80,K0780FHFE,K07803FFA,K07801FF0,K07800A80,K0780,::K078080H080,K0785D5D5D580,K0787FKF80,:::K0782AKA80,K0780,::::K0782AIA8280,K0787FIFC780,:::K0781J101,K0780,:,:::::::::~DG012.GRF,02304,012,\n" +
                              ",:::M0340,M03F8,M03FFD0,M03FHFE,M03FIFD0,M03FJFE,M03D7FIFC0,M03C0FIFC0,M03C03FHFC0,M03C03C3FC0,M03C03C07C0,M03C03C03C0,::::M01C03C03C0,M03E03E03C0,M01F47C07C0,M01FIF07C0,M01FHF7FFC0,N0IF7FF80,N07FE7FF,N03FC3FF,O0501FC,,::N07C,N0HF,M01FF04,M01FF86,M03F7C7,M03E3C380,M03C1C7C0,M03C1C3C0,M01C1C1C0,M01C0C1E0,:N0E0C1E0,M01F5C1E0,M03FFE3E0,M03FIFC0,::O0IF80,P05F,,:::N0C00180,M01FD01E0,M01FFE1E0,M01FHFDE0,M03FJF8,M03FJFC,M03C3FHFC,M03C057FC,M03C001FE,N04001F7,Q01E0,Q0160,,N01FD0,N07FF8,N0IFC,M01FIF,M01FDFF,M03E03F80,M03C007C0,M03C003C0,M03C001C0,M03C001E0,M01C001E0,M01E001E0,M01F001E0,N0F803E0,N0700FC0,N0780FC0,N01007C0,Q07,Q04,,:M0380,M03FD40,M03FFE0,M03FHFD,M03FJF0,N05FJF40,O03FIFC0,P05FHFC0,Q0IFC0,Q0757C0,Q0380C0,Q01C0,:M034001C0,M03FC01E0,M03FF57E0,M03FIFE0,M03FIFC0,N0JFC0,O05FFC0,P03F,Q04,,::::::::::::M0340,M03F8,M03FFD0,M03FHFE,M03FIFD0,N0KFE,N045FIFC0,Q0IFC0,Q017FC0,R07FC0,Q05FFC0,Q0IF80,P07FFC,O03FFE0,N01FHF40,N07FFC,M01FHF0,M03FF80,M03FC,M03FFC0,M03FHF5,M03FIFE0,N05FJF40,O02FIFC0,P05FHFC0,R0HFC0,R0H5C0,T0C0,O05,N01FE0,N07FFC,N0IFE,M01FIF,M01FIF80,M01F45FC0,M03E00FC0,M03C007C0,M03C003E0,M03C001E0,:M01C001E0,M01E001E0,M01F401C0,N0FE07C0,N0JFC0,N07FHF80,N05FHF40,O0HFE,O05F4,,::::::::::::::::::~DG013.GRF,04224,012,\n" +
                              ",::::::Q02,Q0E,P03E,P07E,P0FE,P0HF,P0FA,O01F0,O01E0,:::P0E0,P0FC,P0LFE0,P07FJFE0,P03FJFE0,P01FJFE0,Q0AFHFBE0,,::::Q0J20,P01FIF0,P03FIF0,P0KF0,:O01FJF0,O01FA08,O01F0,O01E0,:P0E0,:P0F0,P07C,P0BFFBB0,O01FJF0,:::,::::P0KA0,O01FJF0,::O01FIFE0,O017H7FC0,S03C0,S01E0,T0E0,T0F0,::P02AABF0,O01FJF0,O01FIFE0,:O01FIFA0,O01FIF80,S03C0,S01C0,T0E0,T0F0,::P0I23F0,O01FJF0,O01FIFE0,::O01FIF,Q0808,,::::P0MA0,O01FKFE0,:::P0M40,,::Q04,P03E02,P07F8380,P0HF83E0,P0HFC7E0,O01FBC7E0,O01F1C1F0,O01E0E0E0,O01E0E0F0,P0E0E0F0,P0E060F0,:P07871F0,P03FHFE0,O01FIFE0,:O01FIFC0,O01FIF80,O01C0,,::::O01FKFE0,::::P0IABAHA0,S01C0,T0E0,::T0F0,:S01F0,O01FBBFE0,O01FIFE0,:O01FIFC0,O01FIF80,,:::::::::::::O01,O01E8,O01FF,O01FFEA,P07FHF,Q0BFHF8,R07FHF40,S02FFE0,T07FE0,U0BE0,V020,:,::::::::::R0H2,R07FC0,Q03FHF8,Q0JFC,P03FIFE,P03FJF,P07FAAFF80,P07C001F80,P0F80H0FC0,P0F0I07C0,P0E0I03E0,O01E0I01C0,O01E0I01E0,O01E40H01E0,O01EE0H01E0,O01EF0H01E0,P0HEI03E0,P0FC0H03C0,P0FC0H03E0,P0FC0H07C0,P0FE002F80,P0HFC07F80,O01FKF80,O01FKF,O03EFIFE,O07C7FHF0,O0380FFA0,O0180,,:::P02FIF0,P07FIF0,P0KF0,:P0HFEFE0,O01F0,O01E0,:P0E0,::P070,P03A2020,O017FIF0,O01FJF0,:::Q0808,,::Q08,P07F,P0HF8380,P0HFC3C0,P0HFC3E0,O01FFC7E0,O01E0E3E0,O01E0E0F0,::P0E0E0F0,P07060F0,P07873E0,P07FIF0,P0JFE0,O01FIFE0,O01FIFC0,O01FIF80,O01A2H2,,::::P0KA0,O01FJF0,:::R01780,S03E0,T0E0,:T0F0,::S03F0,O01FJF0,O01FIFE0,:O01FIFC0,O01FIF,P0J2,,::T0F0,:P02AHAFA,P07FJF,P0LF80,O01FKFC0,:O01F5H5F540,O01E0H0F0,:P0E0H0F0,,:::O015J5140,O01FJF1E0,:::P0KA0A0,,::T0F0,:P02AHAFA,P07FJF,P0LF80,O01FKFC0,:O01F5H5F540,O01E0H0F0,:P0E0H0F0,,T020,T070,N0E0I0BF0,M01E0H01FF0,M01E0H0IF0,M01E007FHF0,M01F82FHF80,M01FC7FFC,N0JFE0,N07FHF,N03FF8,N01FHF,O03FFE0,P07FFC,P02FHFA0,Q01FHF0,R0BFF0,S07F0,S02F0,T030,T020,,:::::::::::::::~DG014.GRF,02816,008,\n" +
                              ",::::::K01,K07,J01F,J03F,J07F,J07F80,J07D,J0F8,J0F0,:::J070,J07E,J07FKF0,J03FKF0,J01FKF0,K0LF0,K057FFDF0,,::::K0J10,K0JF8,J01FIF8,J07FIF8,:J0KF8,J0FD04,J0F8,J0F0,:J070,:J078,J03E,J05FFDD8,J0KF8,:::,::::J0K50,J0KF8,::J0KF0,J0IBFE0,M01E0,N0F0,N070,N078,::J015H5F8,J0KF8,J0KF0,:J0JFD0,J0JFC0,M01E0,N0E0,N070,N078,::J0J1F8,J0KF8,J0KF0,::J0JF80,K0404,,::::J0M50,J0MF0,:::J0M20,,::K02,J01F01,J03FC1C0,J07FC1F0,J07FE3F0,J0FDE3F0,J0F8E0F8,J0F07070,J0F07078,J0707078,J0703078,:J03C38F8,J01FIF0,J0KF0,:J0JFE0,J0JFC0,J0E0,,::::J0MF0,::::J0J5D550,N0E0,N070,::N078,:N0F8,J0FDDFF0,J0KF0,:J0JFE0,J0JFC0,,:::::::::::::J080,J0F4,J0HF80,J0IF5,J03FHF80,K05FHFC,L03FHFA0,M017FF0,N03FF0,O05F0,P010,:,::::::::::L0H1,L03FE0,K01FHFC,K07FHFE,J01FJF,J01FJF80,J03FD57FC0,J03E0H0FC0,J07C0H07E0,J0780H03E0,J070I01F0,J0F0J0E0,J0F0J0F0,J0F20I0F0,J0F70I0F0,J0F780H0F0,J0H7I01F0,J07E0H01E0,J07E0H01F0,J07E0H03E0,J07F0017C0,J07FE03FC0,J0LFC0,J0LF80,I01F7FIF,I03E3FHF8,I01C07FD0,J0C0,,:::J017FHF8,J03FIF8,J07FIF8,:J07FF7F0,J0F8,J0F0,:J070,::J038,J01D1010,J0BFIF8,J0KF8,:::K0404,,::K04,J03F80,J07FC1C0,J07FE1E0,J07FE1F0,J0HFE3F0,J0F071F0,J0F07078,::J0707078,J0383078,J03C39F0,J03FIF8,J07FIF0,J0KF0,J0JFE0,J0JFC0,J0D1H1,,::::J0K50,J0KF8,:::M0BC0,M01F0,N070,:N078,::M01F8,J0KF8,J0KF0,:J0JFE0,J0JF80,J0J1,,::N078,:J015H57D,J03FJF80,J07FJFC0,J0LFE0,:J0FAHAFAA0,J0F0H078,:J070H078,,:::J0KA8A0,J0KF8F0,:::J0K5050,,::N078,:J015H57D,J03FJF80,J07FJFC0,J0LFE0,:J0FAHAFAA0,J0F0H078,:J070H078,,N010,N038,H070I05F8,H0F0I0HF8,H0F0H07FF8,H0F003FHF8,H0FC17FFC0,H0FE3FFE,H07FIF0,H03FHF80,H01FFC,I0IF80,I01FHF0,J03FFE,J017FFD0,L0IF8,L05FF8,M03F8,M0178,N018,N010,,:::::::::::::::~DG015.GRF,01280,008,\n" +
                              ",::::::M01,M03E0,M0D5C,L01802,L03101,L023E080,L0475CC0,L0HC0C40,L04C0440,L0C40660,:L0C70C60,L04DFC40,L0HC3E60,L0H404C0,L0H6H080,L03701,L039AE,L01854,,::M0I540,M0IFE0,M0101,,:M0IA,M0H7C,O0C,O04,O06,M0106,M0HFE,M0H54,,:O06,M0H5740,M0IFE0,M010760,O0260,P040,M01F8,M075C,M060E,M0C04,M0C06,:M060E,M075C,M03F8,M0H10,,:::::::M0IFE0,M0I540,,::M0H54,M0HFE,M0H14,O06,::M0H5C,M0HF8,O04,:O06,:M0H5C,M0HF8,M01,,M0H54,M07FE,M0510,M0C,M04,M02,M0H76,M0HBA,,:M0H54,M0HFE,O04,O06,::M0D5C,M0FE8,,:M0H5H40,M0HFE60,M01,,M0614,M0E3C,M0476,M0C66,M0C46,M0HCE,M07CC,M0380,,:M07,M078C,M0C44,M0HC6,M0C46,M0I6,M0744,M0HFC,M0H50,,:M0238,M0474,M0C66,:M0CE6,M04C4,M078C,M0540,,:M0BAA20,M0HFE40,,:::::::::::::~DG016.GRF,01280,008,\n" +
                              ",::::::::::::::::::L01AHA80,L01FDFC0,,::L01554,M0EF8,N018,O08,O0C,:L01FF8,M0HA8,,:O0C,M0HAE80,L01FDFC0,O0HC0,O04C0,P080,M03F0,M0EB8,M0C1C,L01808,L0180C,M080C,M0C1C,M0EB8,M07F0,M02,,::::::M0202,L01FHFC0,M0IA80,,::M0HA8,L01FFC,M0H28,O0C,::M0AB8,L01FF0,O08,:O0C,:M0AB8,L01FF0,N020,,M0HA8,M0HFC,M0A,L018,M08,M04,M0HEC,L01774,,:M0HA8,L01FFC,O08,O0C,::M0AB8,L01FF0,M02,,M0HAH80,L01FFCC0,M0H20,,M0C28,M0C78,M08EC,L018CC,L0188C,L0199C,M0F98,M07,,:M0E,M0F18,L018H8,L0198C,M0H8C,M0IC,M0E88,L01FF8,M0HA8,,:M0470,M08E8,L018CC,:L019CC,M0B88,M0F18,M0A,,:L0177440,L01FFCC0,,:::::::::::::::::::::::~DG017.GRF,10752,008,\n" +
                              ",:::::::::::::M01,M0380,M07,M0C,::M0I540,M03FFE0,M0H15,,:M0JA0,M0I760,,::M04,M08,,:::::::::M0IFE0,N03060,:::::N03860,N01DC0,O0F80,,:M07,M078C,M0C44,M0HC6,M0H46,M0I6,M0744,M0HFC,M0H50,,:M0238,M0474,M0C66,:M0CE6,M05C4,M078C,M05,,:O06,M05DFC0,M0EAE80,M0C06,,N040,M03F8,M0H7C,M0I6,M0C66,::M0466,M047C,M0278,,:M0H54,M07FE,M05,M0C,M04,M02,M0H76,M0HBA,,:M0514,M0HFE,M0H5C,O06,:O02,,::::::M0J20,M0IFE0,P0E0,O07,O0E,N054,N0E0,M0140,M07,M0FDDC0,M0JA0,,:M01D0,M07BC,M0514,M0C06,::M0504,M073C,M01F0,,::M04,M0C,,:::::::O01,M0E0180,M0F01C0,M0D8060,M0DC060,M0C6060,M0C7060,M0C38E0,M0C1FC0,M0803,,:M0141,M07E780,M041D40,M0C1860,::M041C40,M07E7C0,M0141,,::::::::M0IFE0,M0C1860,::::M041840,M0638E0,M0I7C0,M03C0,M01,,M0704,M0E8C,M0C44,M0C46,:M0C66,M0H74,M0HFC,M0D54,,:M0HBA,M0HFC,O0C,O04,O06,:M0HFC,M0H54,,N040,M01F8,M075C,M0606,M0C06,:M0404,M0208,M0FDDC0,M0JA0,,:M0H54,M07FE,M0510,M0C,M04,M02,M0H76,M0HBA,,:M0H54,M0HFE,O04,O06,::M0D5C,M0FE8,,:L041F0,L0E71C,L04504,L0HC06,:L0HC04,L0571C,L03FFE,L01554,,::::::N040,N0C0,::::N040,,::::::M01C0,M01B0,M019C,M0186,M0181,M0181C0,M0IFE0,M0180,M01,,:N0FE,M075740,M0600C0,M0C0040,M0C0060,M040060,M0600E0,M0755C0,N0FE,,::O02,O01,O0180,M0I5C0,M0IFE0,M0511,,::N010,M01FE,M0H75C0,M0618C0,M041C60,M0C0C60,:M0E08E0,M0H71C0,M03F1,,::O02,O01,O0180,M0H51C0,M0IFE0,M0I540,,::::::::::M0IFE0,N03060,::::::N01DC0,O0F80,,:N054,M01FF,M030180,M0600C0,M040040,M0C0060,::M040060,M0C00E0,M0701C0,M030180,M01D7,N078,N010,,::::::M0H5140,M0IFE0,M0D5D60,M0C1860,::::M041540,M072780,M01C1,,:N038,M01DF,M030180,M0701C0,M060060,M0C0040,M0C0060,::M040040,M0600C0,M070180,M01FF,N054,,:M0C0020,M070060,M0381C0,M0147,N03E,N03C,N07E,M01C7,M038180,M070040,M080020,,::::::::O02,O01,O0180,M0515C0,M0IFE0,M0I540,,::::O01,O03,O01,P080,M0IFE0,,::::M0101,M070180,M040040,M0C0060,M0C1860,:M051540,M0I380,M01C1,,N010,M01FE,M0H75C0,M0618C0,M041C60,M0C0C60,:M0E08E0,M0H71C0,M03F1,,::::::P020,P060,:::M0I560,M0IFE0,P060,:::P040,,M01D0,M07EC,M0564,M0C66,::M0464,M0H68,M0470,,:M0IBA0,M0IFE0,,::M0C,:,::::::::N030,::M0130,M03BB,M0175,N030,::N010,,M0H10,M01FF,M0H75C0,M0618C0,M041C60,M0C0C60,:M0E08E0,M0H71C0,M03E1,,:M0C01,M0F00C0,M0D0040,M0C8060,M0C4060,M0C2060,M0C1440,M0C0FC0,M0405,,N040,N0C0,::::N040,,M0C01,M0F00C0,M0D0040,M0C8060,M0C4060,M0C2060,M0C1140,M0C0F80,M0405,,O01,M0E0180,M0F01C0,M0D8060,M0DC060,M0C6060,M0C7060,M0C38E0,M0C1FC0,M0803,,:N0C0,::::,:M0E0180,M0F01C0,M0D8060,M0DC060,M0C4060,M0C7060,M0C38E0,M0C1DC0,M0C02,,:M0155,M03EF80,M070140,M0C0060,::M070140,M03AB80,M017D,,M01,M0301,M0701C0,M0E00C0,M0C1060,M0C1860,M0C1C40,M0E1CC0,M0I740,M03E0,,:M0101,M070180,M040040,M0C0060,M0C1860,:M0415C0,M07B780,M0141,,:P060,:M0A0060,M0FC060,N0F860,N01560,O0360,O01E0,P060,,:M0H14,M071F80,M041960,M0C0860,M0C0C60,:M051C60,M03B860,M01D040,,:M0310,M071D40,M0E08E0,M0C0C60,::M060860,M0H7060,M03F0,,:::::::M0I540,M0IFE0,M051D60,N01860,::::N01060,P060,,:M0704,M0E8C,M0C44,M0C46,:M0C66,M0H74,M0HFC,M0D54,,:M0C02,M071C,M01B8,M01F0,M03B8,M071C,M0C06,,::M0C,M04,,::::::::N010,N030,::M0175,M03FF,M0131,N030,::,:M01FD,M03BB80,M051940,M0C0860,M0C0C60,:M041D40,M03B9C0,M01F1,,O01,M0E0180,M0F01C0,M0D8060,M0DC060,M0C6060,M0C7060,M0C38E0,M0C1FC0,M0803,,:N0C0,::::,:M0C0180,M0F01C0,M0D8060,M0DC060,M0C4060,M0C7060,M0C38E0,M0C1DC0,M0C02,,:M0C01,M0F00C0,M0D0040,M0C8060,M0C4060,M0C2060,M0C1140,M0C0F80,M0405,,:N0C0,::::N040,,M0D01,M0F01C0,M0D0040,M0C8060,M0C4060,M0C2060,M0C1440,M0C0FC0,M0405,,:N0FE,M075740,M0600C0,M0C0040,M0C0060,:M0600C0,M0755C0,N0FE,N010,,M01C0,M01A0,M0194,M0186,M0181,M0180C0,M0IFE0,M0180,:,::O01,O03,O01,O0180,M0IFC0,,::::M0101,M070180,M040040,M0C0060,M0C1860,:M0415C0,M07B780,M0141,,:N0FE,M075740,M0600C0,M0C0040,M0C0060,M040060,M0600E0,M0755C0,N0FE,,:M01FD,M03BB80,M051940,M0C0860,M0C0C60,:M051D40,M03B9C0,M01D1,,::::::::M0I760,M0IBA0,,:M01,M0BAA,M0FDC,O0C,O04,O06,:M0HFC,M0H54,,N040,M01F8,M075C,M0606,M0C06,:M0404,M0208,M0FDDC0,M0JA0,,:M01D0,M07B8,M0504,M0C06,::M0404,M0738,M01F0,,M01,M0BAA,M0FDC,O0C,O04,O06,:M0HFC,M0H54,,N040,M01F8,M0H7C,M0I6,M0C66,::M0466,M067C,M0268,,:M0614,M063C,M0476,M0C66,M0C46,M0HCE,M07CC,M0380,,:M0H5H40,M0HFE60,,:M07,M078C,M0C44,M0HC6,M0C46,M0I6,M0744,M0HFC,M0H50,,::L054,L038,,::::::::M0IA,M0H7E,O08,O04,O06,M0106,M0BAE,M0FDC,O08,O04,O06,:M0ABE,M0H74,,:M0388,M07C4,M0HC6,M0C46,:M0H46,M0I6,M0HFC,M0EA8,,:M0H5H40,M0HFE60,M01,,M0I540,M0IFE0,N010,,M0H50,M0E3C,L01407,L0300380,L065D1C0,L0H638C0,L0H41440,L0HC0660,L0C40660,L0C60660,L0C71460,L0CFF860,L04C5C40,L06C00C0,L07601C0,L0H30380,L0H1D7,M0838,,:M0I540,M0IFE0,M0715,M0406,M0C06,:M0504,M072C,M01F4,,:M0HBA20,M0HFE60,,M01,M03F0,M075C,M0E0C,M0406,M0C06,:M0E0E,M075C,M03F8,,:O06,M0HBF80,M0H57C0,O0H60,O0460,P020,M0704,M0E8C,M0C44,M0C46,:M0C66,M0H74,M0HFC,M0D54,,::M0HFE,O08,O04,O06,:,M0H54,M0HFE,N01C,O04,O06,:M0H5C,M0HF8,N014,O06,::M0H5C,M0HF8,,:M07,M078C,M0C44,M0HC6,M0C46,M0I6,M0744,M0HFC,M0H50,,::M04,M08,,::M03F0,M075C,M0E0E,M0C06,:M0404,M071C,M0H10,,:M01F8,M075C,M060E,M0C04,M0C06,M0406,M060E,M075C,M03F8,M01,,:M0C,M04,,::M0H5H40,M0HFE60,,:M01F0,M073C,M0404,M0C06,:M0406,M051C,M0IFE0,M0I540,,:::::::N0C0,::::N040,,:::::O02,N05E,N0E8,M07,M0E,M05C0,N038,N01E,N03A,M05D0,M0E,M0740,N0F0,N05C,,O04,N02E,M0174,M0F80,M0D,M03A0,N05C,O0E,M0174,M03A0,M0F,M0F80,M0170,N01E,O04,O02,N016,N0F8,M0740,M0E,M0H50,N03A,N01E,N0BC,M0750,M0E,M0740,N0F8,N016,O02,,:M04,M08,,::M0J20,M0IFE0,M020C,M0404,M0C06,:M0E0E,M075C,M03F8,N040,,M0H5H40,M0HFE60,,:M0170,M07B8,M0404,M0C06,::M0404,M079C,M0170,,O04,O06,M0H5740,M0HBFE0,O0H60,:M010040,M0388,M07CC,M0HC6,M0C46,:M0H46,M0H6E,M07FC,M0HB8,,:M0514,M0HFE,M0H5C,O06,:O02,,M0IA,M0H7E,O08,O04,O06,:M0HBE,M0HFC,O08,O04,O06,:M0ABE,M0H74,,:M0388,M07CC,M0HC6,M0C46,:M0H46,M0I6,M07FC,M0EA8,,::M0C,M04,,::M0170,M07BC,M0404,M0C06,:M060E,M071C,M01,:,M01D0,M07BC,M0514,M0C06,::M0504,M073C,M01F0,,::M04,M0C,,::M0IA20,M0H7H60,,N040,M01F8,M075C,M0606,M0C06,:M0404,M0208,M0FDDC0,M0JA0,,:::::::::::::::::~DG018.GRF,02048,008,\n" +
                              ",::M0J20,M07FHF0,:M0H1D70,N01C70,:::O0EF0,O0FE0,O07C0,O01,,P050,P070,:::M07FHF0,:M03BBF0,P070,:::,::::::M0J20,M07FHF0,:M071D70,M071C70,:::M071E70,M075FF0,M03F3E0,M0171,,:M02AA20,M07FF30,:,:M0170,M03FE,M07DE,M0306,M0703,M0603,M0707,M038E,M01FC,N0F8,N040,,::::::M0J50,M07FHF0,:N01C70,:::::P070,,:M01C4,M07E6,M0767,M0623,M0673,M0233,M07F7,M07FE,M0H54,,:M0I2,M07FF,:M0H17,O03,::O01,M07FF,:M07FE,M0107,O03,O07,M07FF,:M03BE,O06,O03,M0103,M07FF,:M07E8,,:M01C4,M03E6,M0767,M0623,M0673,M0233,M07F7,M07FE,M0H54,,:::::::N0A8,L017FF,L03FBFE0,L0740570,L040H010,,:M0J50,M07FHF0,M0H7F70,N01C70,::::N01DF0,O0FE0,O0740,,N040,N0F8,M03FF,M03B6,M0733,M0633,M0733,M0H3E,M0H3C,N038,N010,,M0I5,M07FF,M0H7F,O06,O07,O03,O01,,M0H1C,M0H3E,M073F,M0633,M0673,:M0I7,M03E6,M01C4,,N010,M01F8,M01FC,M03BE,M0733,M0633,M0637,M0H3E,M0H3D,M0138,,:M0I5,M07FF,:O06,O03,:O01,,M0170,M03FE,M07DE,M0306,M0703,M0603,M0707,M038E,M01FC,N0F8,N040,,L040H010,L070H070,L05D55D0,M0IF80,M0175,,::::::::::::::::::::::::::::::::~DG019.GRF,02816,008,\n" +
                              ",::::::M01,M07,L01F,L03F,L07F,L07F80,L07D,L0F8,L0F0,:::L070,L07E,L07FKF0,L03FKF0,L01FKF0,M0LF0,M057FFDF0,,::::M0J10,M0JF8,L01FIF8,L07FIF8,:L0KF8,L0FD04,L0F8,L0F0,:L070,:L078,L03E,L05FFDD8,L0KF8,:::,::::L0K50,L0KF8,::L0KF0,L0IBFE0,O01E0,P0F0,P070,P078,::L015H5F8,L0KF8,L0KF0,:L0JFD0,L0JFC0,O01E0,P0E0,P070,P078,::L0J1F8,L0KF8,L0KF0,::L0JF80,M0404,,::::L0M50,L0MF0,:::L0M20,,::M02,L01F01,L03FC1C0,L07FC1F0,L07FE3F0,L0FDE3F0,L0F8E0F8,L0F07070,L0F07078,L0707078,L0703078,:L03C38F8,L01FIF0,L0KF0,:L0JFE0,L0JFC0,L0E0,,::::L0MF0,::::L0J5D550,P0E0,P070,::P078,:P0F8,L0FDDFF0,L0KF0,:L0JFE0,L0JFC0,,:::::::::::::L080,L0F4,L0HF80,L0IF5,L03FHF80,M05FHFC,N03FHFA0,O017FF0,P03FF0,Q05F0,R010,:,::::::::::N0H1,N03FE0,M01FHFC,M07FHFE,L01FJF,L01FJF80,L03FD57FC0,L03E0H0FC0,L07C0H07E0,L0780H03E0,L070I01F0,L0F0J0E0,L0F0J0F0,L0F20I0F0,L0F70I0F0,L0F780H0F0,L0H7I01F0,L07E0H01E0,L07E0H01F0,L07E0H03E0,L07F0017C0,L07FE03FC0,L0LFC0,L0LF80,K01F7FIF,K03E3FHF8,K01C07FD0,L0C0,,:::L017FHF8,L03FIF8,L07FIF8,:L07FF7F0,L0F8,L0F0,:L070,::L038,L01D1010,L0BFIF8,L0KF8,:::M0404,,::M04,L03F80,L07FC1C0,L07FE1E0,L07FE1F0,L0HFE3F0,L0F071F0,L0F07078,::L0707078,L0383078,L03C39F0,L03FIF8,L07FIF0,L0KF0,L0JFE0,L0JFC0,L0D1H1,,::::L0K50,L0KF8,:::O0BC0,O01F0,P070,:P078,::O01F8,L0KF8,L0KF0,:L0JFE0,L0JF80,L0J1,,::P078,:L015H57D,L03FJF80,L07FJFC0,L0LFE0,:L0FAHAFAA0,L0F0H078,:L070H078,,:::L0KA8A0,L0KF8F0,:::L0K5050,,::P078,:L015H57D,L03FJF80,L07FJFC0,L0LFE0,:L0FAHAFAA0,L0F0H078,:L070H078,,P010,P038,J070I05F8,J0F0I0HF8,J0F0H07FF8,J0F003FHF8,J0FC17FFC0,J0FE3FFE,J07FIF0,J03FHF80,J01FFC,K0IF80,K01FHF0,L03FFE,L017FFD0,N0IF8,N05FF8,O03F8,O0178,P018,P010,,:::::::::::::::~DG020.GRF,02048,008,\n" +
                              ",:I01E002,I01E00E,I01E03E,I01E07E,I01E0FE,I01E0FF,I01E0FA,I01E1F0,I01E1E0,:::I01E0E0,I01E0FC,I01E0FKFE0,I01E07FJFE0,I01E03FJFE0,I01E01FJFE0,I01E00AFHFBE0,I01E0,::::I01E00FFE,I01E01FHF,I01E03FHF80,I01E07FHFC0,I01E0FEEFE0,I01E0F0E3E0,I01E0E0E0E0,I01E1E0E0F0,:::I01E1E0E1F0,I01E0F8E3E0,I01E0F8FFE0,I01E0F8FFC0,I01E070FF80,I01E030FE80,I01E010FC,I01E0H0A0,I01E0,:::I01E1FJF0,::::I01E0AHAFA0,I01E0I01C0,I01E0J0E0,::I01E0J0F0,:I01E0I01F0,I01E0EHEFE0,I01E1FIFE0,:I01E1FIFC0,I01E1FHFE80,I01E0,:::::I01E15J5140,I01E1FJF1E0,:::I01E0AJA0A0,I01E0,::I01E02802,I01E0381F,I01E0383F80,I01E0787FC0,I01E0F87FE0,I01E0FC7FE0,I01E0F8F8E0,I01E1E0F8F0,:I01E1E1F0F0,::I01E0E3E0E0,I01E0F3E3E0,I01E0FFE3E0,I01E0FFC1E0,I01E07FE3E0,I01E03FC1,I01E00A80,I01E0,:::::::::::::::I01E1FKFE0,::::I01E1E2AFA3E0,I01E1E00F01E0,::::::::I01E0E00F83E0,I01E1F01FD7C0,I01E0FEFIFE0,I01E0FHFDFFC0,I01E0FHF8FF80,I01E07FF87F,I01E02FE02A,I01E007C0,I01E0,::I01E004,I01E03E02,I01E07F8380,I01E0FF83E0,I01E0FFC7E0,I01E1FBC7E0,I01E1F1C1F0,I01E1E0E0E0,I01E1E0E0F0,I01E0E0E0F0,I01E0E060F0,:I01E07871F0,I01E03FHFE0,I01E1FIFE0,:I01E1FIFC0,I01E1FIF80,I01E1C0,I01E0,:::I01E0AA2AA0,I01E1FJF0,:::I01E1FIFC0,I01E0I0BE0,I01E0I01E0,:I01E0I01F0,::I01E0J0E0,I01E0,I01E008,I01E07F,I01E0FF8380,I01E0FFC3C0,I01E0FFC3E0,I01E1FFC7E0,I01E1E0E3E0,I01E1E0E0F0,::I01E0E0E0F0,I01E07060F0,I01E07873E0,I01E07FIF0,I01E0FIFE0,I01E1FIFE0,I01E1FIFC0,I01E1FIF80,I01E1A2H2,I01E0,::::I01E0AJA0,I01E1FJF0,:::I01E0H01780,I01E0I03E0,I01E0J0E0,:I01E0J0F0,::I01E0I03F0,I01E1FJF0,I01E1FIFE0,:I01E1FIFC0,I01E1FIF,I01E02I2,I01E0,::I01E0H020,I01E407FC,I01F82FHF80,I01FC7FHFC0,I01F8FIFE0,:I01F8FA0BE0,I01F1F001F0,I01E1E0H0F0,:::I01E0E0H0E0,I01E07001C0,I01F8380B80,I01FKF80,I01FLF0,:::I01E2AJA0,I01E0,:,::::::~DG021.GRF,01536,008,\n" +
                              ",:::::L017F4,L07FFE,K01FIFC0,K03FIFE0,K07FJF0,K07F83FF8,K07C005FC,K0F80H0FC,K070I07C,K0F0I03E,K0F0I01F,K0F0J0E,K0F0J0F,:K0780I0F,:K07F0H01F,K03F8001E,K01F0H05E,K01F800FE,K017001FC,L03001F8,L01001F0,P0E0,O01,,L014,L07F80,K01FHF0,K03FHF8,K07FHFC,K07FHFE,K07D17F,K0F803F,K0F001F,K0F0H0F80,K0F0H0780,:K070H0780,K07800780,K07D007,K03F81F,K03FIF,K01FHFE,K017FFD,L03FF8,L017F0,,:K08,K0F40,K0HF8,K0IF50,K0JFE,K07FIFD4,L07FJF,L015FIF,N03FHF,O05FF,Q0F,K0F0J01,K0FE0,K0IF40,K0JF8,K07FIF,K03FIF8E,L057FF8F,N0HF8F,N0158F,Q0F,Q05,,L07F40,L0IF0,K01FHFC,K03FHFC,K07F7FF,K07873F,K07071F,K0F070F,K0F0707,K0F070780,:K0F870780,K078707,K07C70F,K07C7FF,K0387FE,K0187FC,M07F8,M0H50,,::::::::::::K05,K0FE0,K0HFD40,K0JF8,K0KF50,K03FJFA,L017FIF,N07FHF,N015FF,O01FF,O07FF,N03FFE,M01FHF0,M0IF80,L05FFD,L0IF8,K07FF40,K0HFE,K0HF0,K0HFE80,K0JF5,K0KFA0,K057FIFD,M0KF,M017FHF,O03FF,P05F,Q01,L0150H01,L07F80,K01FHF0,K03FHF8,K07FHFC,K07FBFE,K07C07F,K0F801F,K0F001F,K0F0H0780,::K070H0780,K07800780,K07C007,K03F81F,K03FF7F,K01FHFE,L0IFC,L03FF8,L017F0,,::::::::::::::::::::::::::::::::~DG022.GRF,07296,012,\n" +
                              ",:::::::::::::::::J01F80,:::J01F80EOE,J01F81FNFE,::::::J01F81FE020202FE,J01F81FC0K0FE,::::::::J01F80FC0K0FE,J01F81FC0J01FC,J01F80FE0J01FE,J01F80FE0J03FC,J01F80FF80I03FC,J01F807FC0I0HFC,J01F807FEA22BFF8,J01F807FMF0,J01F803FMF0,J01F801FLFE0,J01F800FLFE0,J01F8007FKF,J01F8003FJFE,J01F80H07FIF8,J01F80H02AFEA0,J01F80,:::J01F81C0,J01F81F8,J01F81FF,J01F81FFA0,J01F81FHF0,J01F81FHFE,J01F81FIFC0,J01F80FIFE8,J01F807FIFC,J01F800BFIF80,J01F8001FJF0,J01F80H0KFE,J01F80H0LF80,J01F80H0FEFIFE8,J01F80H0FE1FIFC,J01F80H0FE03FHFE,J01F80H0FE007FFE,J01F80H0FE002FFE,J01F80H0FE0H07FE,J01F80H0FE002FFE,J01F80H0FE00FHFE,J01F80H0FE0BFHFE,J01F80H0FE1FIFC,J01F80H0FEFIFE0,J01F80H0LF,J01F80H0KFE,J01F8001FJF0,J01F800FJFA0,J01F803FIFE,J01F80FJF8,J01F81FIFC0,J01F81FHFE,J01F81FHF0,J01F81FFE0,J01F81FF,J01F81FA,J01F81E0,J01F8180,J01F80,:::J01F81FNFE,::::::J01F81FHFBFHFBFE,J01F80J0FE00FE,:::::::::::::::J01F80J05400FE,J01F80N0FE,:J01F80N0HA,J01F80,:::J01F80N054,J01F80N0FE,:::::::::::J01F81FNFE,::::::J01F80N0FE,:::::::::::J01F80,:J01F81C0,J01F81F8,J01F81FF,J01F81FFA0,J01F81FHF0,J01F81FHFE,J01F81FIFC0,J01F80FIFE8,J01F807FIFE,J01F800FJF80,J01F8001FJF0,J01F80H0KFE,J01F80H0LF80,J01F80H0FEFIFE8,J01F80H0FE1FIFC,J01F80H0FE03FHFE,J01F80H0FE007FFE,J01F80H0FE002FFE,J01F80H0FE0H07FE,J01F80H0FE002FFE,J01F80H0FE00FHFE,J01F80H0FE0BFHFE,J01F80H0FE1FIFC,J01F80H0FEFIFE0,J01F80H0LF,J01F80H0KFE,J01F8001FJF0,J01F802FJFA0,J01F803FIFE,J01F80FJF8,J01F81FIFC0,J01F81FHFE,J01F81FHF0,J01F81FFE0,J01F81FF,J01F81FA,J01F81E0,J01F8180,J01F80,:::J01F81FNFE,:::::::J01F80J0FE00FE,:::::J01F80I01FE00FE,J01F80I03FE00FE,:J01F80I0HFE00FE,J01F80H01FFE00FE,J01F80H03FFE00FE,J01F80H0IFE00FE,J01F8003FHFE00FE,J01F8007FF7F01FE,J01F800FHF3F83FE,J01F803FFE3FIFC,J01F80FHFE3FIFC,J01F81FHFC3FIFC,J01F81FHF01FIF8,J01F81FFC00FIF0,J01F81FF800FHFE0,J01F81FE0H07FFC0,J01F81F80I0HF80,J01F81F0,J01F81E0,J01F81,J01F80,::::::::::::::::::::J01F81FNFE,:::::::J01F80,:::::J01F80H010,J01F8002F0I08,J01F8007F0H07F,J01F802FF002FFE0,J01F803FF007FHF0,J01F803FF80FIF8,J01F807FF80FIF8,J01F80FFE80FIFE,J01F80FFC01FIFE,J01F81FF803FEBFE,J01F81FE003FC0FE,J01F83FE003F80FE,J01F81FC007F807F,J01F83F8007F807F,:J01F83F800FF007F,:::J01F83F800FE00FE,J01F83FC01FE00FE,J01F83FC03FE00FE,J01F81FE03FC03FE,J01F81FE07FE0FFE,J01F81FJFC1FFC,J01F80FJF81FF8,:J01F807FIF01FE0,J01F803FHFE01FE0,J01F803FHFE01F80,J01F8007FF801C,J01F8002EE,J01F80,::::::J01F80EOE,J01F81FNFE,::::::J01F802O2,J01F80,::::::::::::::::::::::J01F80EOE,J01F81FNFE,::::::J01F81FE02FE02FE,J01F81FC00FE00FE,::::::::::J01F80FC00FE00FE,J01F81FC00FF01FE,J01F80FE01FF83FE,J01F81FE01FFC7FC,J01F80FF83FJFE,J01F80FNFC,J01F80FIFEFIF8,J01F807FHFE7FHF0,J01F803FHFE3FFE0,J01F803FHFC1FFC0,J01F801FHF80BF80,J01F8007FF,J01F8002FA,J01F80,:::J01F81C0,J01F81F8,J01F81FF,J01F81FFA0,J01F81FHF0,J01F81FHFE,J01F81FIFC0,J01F80FIFE8,J01F807FIFC,J01F800FJF80,J01F8001FJF0,J01F80H0KFE,J01F80H0LF80,J01F80H0FEFIFE8,J01F80H0FE1FIFC,J01F80H0FE03FHFE,J01F80H0FE007FFE,J01F80H0FE002FFE,J01F80H0FE0H07FE,J01F80H0FE002FFE,J01F80H0FE00FHFE,J01F80H0FE0BFHFE,J01F80H0FE1FIFC,J01F80H0FEFIFE0,J01F80H0LF,J01F80H0KFA,J01F8001FJF0,J01F800FJFA0,J01F803FIFE,J01F80FJF8,J01F81FIFC0,J01F81FHFE,J01F81FHF0,J01F81FFE0,J01F81FF,J01F81FA,J01F81E0,J01F8180,J01F80,:::J01F81FNFE,::::::J01F81FBFLFE,J01F80J0FE00FE,:::::J01F80I01FE00FE,J01F80I03FE00FE,:J01F80I0HFE00FE,J01F80H01FFE00FE,J01F80H03FFE00FE,J01F80H0IFE00FE,J01F8003FHFE00FE,J01F8007FF7F01FE,J01F800FHFBF83FE,J01F807FFE3FIFC,J01F80FHFE3FIFC,J01F81FHFC3FIFC,J01F81FHF81FIF8,J01F81FFC00FIF0,J01F81FF800FHFE0,J01F81FE0H07FFC0,J01F81F80I0HF80,J01F81F0,J01F81E0,J01F81,J01F80,J01F81,J01F81E0,J01F81F0,J01F81FE80,J01F81FFC0,J01F81FFE8,J01F81FHFE,J01F81FIF80,J01F81FJF0,J01F803FIFE,J01F8007FIFC0,J01F8002FIFE8,J01F80H0KFC,J01F80H0LFA0,J01F80H0FE7FIF0,J01F80H0FE3FIFA,J01F80H0FE07FHFE,J01F80H0FE00FHFE,J01F80H0FE001FFE,J01F80H0FE0H0HFE,J01F80H0FE001FFE,J01F80H0FE02FHFE,J01F80H0FE07FHFE,J01F80H0FE3FIFA,J01F80H0MF0,J01F80H0LFA0,J01F80H0KFC,J01F80H0JFE8,J01F8007FIFC0,J01F803FIFE,J01F81FJF0,J01F81FIFA0,J01F81FIF,J01F81FFE8,J01F81FFE0,J01F81FE80,J01F81FC,J01F81E0,J01F81,J01F80,:::J01F80AOA,J01F81FNFE,:::::J01F804K47FFE,J01F80L0IF8,J01F80K03FHF0,J01F80K0IFE0,J01F80J01FHF,J01F80J0BFFE,J01F80I01FHFC,J01F80I03FFE0,J01F80I07FFC0,J01F80H03FHF80,J01F80H07FFE,J01F8002FHF8,J01F8003FHF0,J01F800FHFE0,J01F801FHF,J01F80FHFE,J01F81FHF8,J01F81FHFMA,J01F81FNFE,:::::J01F817575757574,J01F80,:::::J01F80J020,J01F80I05FF,J01F80H03FHFE8,J01F80H0KFC,J01F8003FJFE,J01F8007FKF80,J01F800FLFE0,J01F801FLFE0,J01F803FHFEFIF0,J01F807FFC007FF8,J01F80FFE0H02FF8,J01F80FF80I07FC,J01F80FF0J03FE,J01F81FE0J01FE,J01F81FE0K0FE,J01F81FC0K0FE,J01F83F80K0FE,J01F83F80K07F,:::J01F83F807F0H07F,::J01F83F807F0H0FE,J01F81FC07F0H0HF,J01F83FC07F0H0FE,J01F81FC07F001FE,J01F81FE07F003FE,J01F81FE07F00FFC,J01F80FFAFF03FFC,J01F80FJF01FF8,J01F80FJF03FF8,J01F807FIF01FF0,J01F803FIF01FE0,J01F803FIF01FC0,J01F803FIFH0E,J01F800FIFH0C,J01F800AIA0,J01F80,::,:::::::::::::::::::~DG023.GRF,02560,008,\n" +
                              ",:::I0340,I03F8,I03FFD0,I03FHFE,I03FIFD0,J0KFE,J045FIFC0,M0IFC0,M017FC0,N07FC0,M05FFC0,M0IF80,L07FFC,K03FFE0,J05FHF40,J07FFC,I01FHF0,I03FF80,I03FC,I03FF80,I03FHF5,I03FIFE0,J05FJF40,K02FIFC0,L057FFC0,N0HFC0,N015C0,P0C0,J07C,J0HF,I01FF04,I01FF86,I03F7C7,I03E3C380,I03C1C7C0,I03C1C3C0,I01C1C1C0,I01C0C1E0,:J0E0C1E0,I01F5C1E0,I03FFE3E0,I03FIFC0,::K0IF80,L05F,,::I03D4,I03FE,I03FFD4,I03FIF80,I01FIFE0,J03FHFE0,K05FFE0,L01FE0,M0740,M0380,M01C0,I020H01C0,I03D401C0,I03FF81E0,I03FHF7E0,I03FIFE0,I01FIFC0,K0IFC0,K017FC0,M0F,M07,M0380,M01C0,I020H01C0,I03D401C0,I03FF81E0,I03FHF7E0,I03FIFE0,I015FHFC0,K0IFC0,K017FC0,M02,,:K01,K0FE0,J07FFC,J0IFE,I01FIF,I01FIF80,I01F5DFC0,I03E1C7C0,I03C1C3C0,I03C1C1E0,::I01C1C1E0,I01E1C3E0,I01F1C7C0,J0E1FFC0,J071FFC0,J061FF80,J041FF,L0E8,,:::::::::::::J05FD0,J07FFC,J07FHF,J0JF,I01FDFFC0,I01E01FC0,I01C007C0,I03C003E0,I03C001C0,I03C001E0,:I03E001E0,I01F001C0,I01F803E0,I01FD07C0,J0JFC0,J07FHFC0,J03FHF80,J01FHF,K03F8,L040,,:I02,I03D401C0,I03FF81E0,I03FHF5E0,I03FIFE0,I01FJFC,K0KF,K017FHF80,M07FF80,M01FFC0,M01EFC0,M01E3C0,N0E3C0,N043C0,O0380,O0180,,:::::::I0380,I03FC,I03FFC0,I03FHF5,I03FIFE0,I01FJFD40,K0KFC0,K017FHFC0,L07BFFC0,L0785FC0,L07803C0,:::::L03803C0,L07C03C0,L03E03C0,L03F17C0,L03FHF80,L01FHFC0,M0IF80,M07FF,M01FE,J040H054,I03C0,I03FD,I03FFE0,I03FHFD40,I03FIFE0,I015FHFE0,K03FFE0,L05FE0,M0E20,M07,M0780,M07C0,M03E0,::K054140,J01FE0,J07FFC,J0IFE,I01FIF,I01FEFF80,I01F01FC0,I03E007C0,I03C007C0,I03C001E0,::I01C001E0,I01E001E0,I01F001C0,J0FE07C0,J0HFDFC0,J07FHF80,J03FHF,K0HFE,K05FC,,::K0540,J03FE0,J07FF4,J0IFE,I01FIF,I03FEFF80,I03F01FC0,I03E007C0,I03C007C0,I03C001E0,I01C001E0,:I01C001C0,J0F003C0,I03F407C0,I03FFAF80,I03FIF,I03FIF80,I01FJF5,K0KF80,K057FHFC0,M0IFC0,M017FC0,O03C0,J040J040,J0FE,I01FFD0,I03FHFE,I01FIFC0,I03FIFE0,I03D5FFE0,I03C03FE0,I01C005E0,I01C0,:J0E0,I0170,I03FC,I03FF50,I03FHFE,I03FIFC0,J0JFE0,J017FFE0,L03FE0,M05E0,N020,,:K0540,J01FE0,J07FF4,J0IFE,I01FIF,I01FIF80,I01F05FC0,I03C00FC0,I03C007C0,I03C001E0,:I03E001E0,I01F001E0,I01F801E0,I01F007C0,J0F80FC0,J07007C0,J0300F80,M07,M06,M04,,:J0200180,I01FD41E0,I01FFE9E0,I01FIFE0,I03FJF8,I03DFIFC,I03C0FHFC,I03C017FE,I03C001FE,J04001E5,M01E0,M0160,,::~DG024.GRF,05760,012,\n" +
                              ",::::::::::::::::::::::::::::::::L028,L03F40,L03FFA80,L03FIF8,L03FIFEA0,L03FLF,L03FMFA80,L03FNFC0,L03FAFLFC0,L03F807FJFC0,L03F800ABFHFC0,L03F80H01FHFC0,L03F80J0BFC0,L03F80K01C0,L03F80,:::::::::L02F80,,::L020,L03F,L03FFA80,L03FIF0,L03FIFEA0,L03FKFC,L03FMFA80,L01FNFC0,N0AFLFC0,O01FKFC0,Q0BFIFC0,R01FHFC0,T0BFC0,U01C0,V080,N018,M02F8,M07FC,M0HF8,L01FFC0H05,L01FFC003FF8,L01FF400FHFC,L03F8001FHFE,L03F8001FIF80,L03F8003FIF80,L07F0H07FIFC0,L07F0H0HF83FC0,L07F0H0HF01FC0,L07F0H0FE00FE0,L07F001FE00FE0,L03F803FE00FE0,L07F003FC00FE0,L03F803F800FE0,L03F807F800FE0,L03FE0FF800FE0,L01FJFH01FC0,M0JFE003FC0,M0JFC007FC0,M0BFHF803FF80,M03FHF803FF,N0HFE003FE,N01F8003FC,O080H03F8,S01C0,,:::::T01A80,T01FC0,::L03A80J01FC0,L03FD0J01FC0,L03FFEA0H01FC0,L03FIFC001FC0,L03FJFA81FC0,L03FLF1FC0,L03FNFC0,M01FMFC0,N08BFKFC0,P01FJFC0,Q02FIFC0,S07FFC0,T0BFC0,T01FC0,::::::::U0BC0,V040,,:::::::::::O0HAH80,N01FHFC0,N0KFA,M01FKF,M03FKF80,M0NF0,M0NF8,L01FFC01FHFC,L03FE8003FFE,L03FC0I07FF,L03F80I03FF80,L07F80J0HF80,L07F80J03F80,L07F0K01FC0,L07F0K01FE0,L07F0K01FC0,L03F0L0FE0,L07F0L0FE0,L03F80K0FE0,L03FC0K0FE0,L03FE0K0FE0,L01FF0J01FC0,M0HF80I01FE0,M0HFC0I03FC0,M0BFF80H0BFC0,M03FHFH01FF80,M03FHFEAFHF80,N0NF,N0BFKFE,N01FKFC,O03FJF8,P0JFC0,P02BFA80,,::L03FA8,L03FFD,L03FHFEA,L03FJFC0,L03FKFA88,L03FMFC,L02AFMF80,N01FLFC0,P0BFJFC0,P01FJFC0,P01FEBFFC0,P01FC07FC0,P01FC01FC0,::::::::::::Q05C01FC0,T01FC0,:U0BC0,V040,,:::::::::::::O0BFBA8,N07FIF8,M02FJFE,M07FKFC0,M0MFE0,L01FMF8,L03FFEABFHF8,L03FF0H07FFE,L03FE0I0BFE,L03FC0I01FF,L03F80J0HF80,L07F0K07FC0,L07F0K03FC0,L07F0K01FC0,L07F0L0FE0,L07F0L0FC0,L03F80K0FE0,::L03FC0K0FE0,L01FE0K0FE0,L01FF80I01FC0,M0HFE0I03FC0,M07FE0I07FC0,M03FE0H03FF80,M01FC0H07FF80,N0FE0H03FE,N01C0H07FE,O080H03F8,S03F0,S02A0,,::O02AA,N01FHFC0,N0KF8,M01FKF,M03FKFA0,M0MFE0,M0NF8,L01FFC01FHFC,L03FE0H03FFE,L03FC0I07FF,L03F80I01FF80,L07F80J0HF80,L07F80J03F80,L07F0K03FC0,L07F0K01FC0,:L03F0L0FE0,L07F0L0FE0,L03F80K0FE0,:L03FE0K0FE0,L01FE0K0FE0,M0HF80I01FE0,M0HFC0I03FC0,M07FE80H0HFC0,M03FHFH01FF80,M01FIFBFHF80,N0NF,N03FKFE,N01FKFC,O0BFJF8,O01FIFC0,P02BFE80,,:L010,L03EA0,L03FFC,L03FHFA8,L03FJFC0,L03FKFA80,L03FMF4,M0ABFLF80,N017FKFC0,P0AFJFC0,Q01FIFC0,S0IFC0,R01FHFC0,R0JF80,Q07FIF,P03FIF8,O01FIFC0,O0BFHFE,N01FIF0,M08FIF80,M0JFC,L03FHFE8,L03FHF,L03FFE80,L03FIF8,L03FIFEA0,L03FLF,L03FMFH80,M01FMFC0,N02AFKFC0,P01FJFC0,R0BFHFC0,S01FFC0,T02BC0,V040,,:::T01A80,T01FC0,::L03880J01FC0,L03FD0J01FC0,L03FFEA0H01FC0,L03FIFC001FC0,L03FJFA81FC0,L03FLF9FC0,L03FNFC0,M01FMFC0,O0BFKFC0,P01FJFC0,Q02FIFC0,S07FFC0,T0BFC0,T01FC0,:::::::L010L01FC0,L03EA0K0BC0,L03FFC,L03FHFA8,L03FJFC0,L03FKFE80,L03FMF4,L03FBFLF80,L03F97FKFC0,L03F80BFJFC0,L03F800FJFC0,L03F800FEBFFC0,L03F800FE01FC0,:::::::::::::L01F800FE01FC0,M0A800BE01FC0,Q01601FC0,T01FC0,:U0BC0,L010,L03EA0,L03FFC,L03FHFA8,L03FJFC0,L03FKFA80,L03FMF4,M0ABFLF80,N017FKFC0,P0AFJFC0,Q01FIFC0,S0IFC0,R01FHFC0,R0JF80,Q07FIF,P03FIF8,O01FIFC0,O0BFHFE,N01FIF0,M08FIF80,M0JFC,L03FHFE8,L03FHF,L03FFE80,L03FIF8,L03FIFEA0,L03FLF,L03FMFH80,M01FMFC0,N02AFKFC0,P01FJFC0,R0BFHFC0,S01FFC0,T02BC0,V040,,:::T01A80,T01FC0,::L03880J01FC0,L03FD0J01FC0,L03FFEA0H01FC0,L03FIFC001FC0,L03FJFA81FC0,L03FLF9FC0,L03FNFC0,M01FMFC0,O0BFKFC0,P01FJFC0,Q02FIFC0,S07FFC0,T0BFC0,T01FC0,::::::::U0BC0,,:::::::::::::::::~DG025.GRF,00512,008,\n" +
                              ",::::::::::::::M020202,,::M0IFC0,L01FHFE0,L03FHFA0,L01FHFE0,L03F02A2,L01E00E0,L03C27A0,L01CE7C0,L03CE6A0,L01CE7E0,L03CE7A0,L01CE7C0,L03FFEA2,L01FHFE0,L01FHFA0,M0IFC0,,::::::::::::::::::::::::::::~DG026.GRF,00512,008,\n" +
                              ",::::::::::::::::::::K02020202,,:::L03FHFC0,::K023FHFC2,L03FHFC0,:L03F1FC0,::O01C0,:K023F1AC2,L03F18C0,L03FD8C0,L03FHFC0,:,::K02020202,,:::::::::::::::::^XA\n" +
                              "^PW878\n" +
                              "^FT704,352^XG000.GRF,1,1^FS\n" +
                              "^FT96,384^XG001.GRF,1,1^FS\n" +
                              "^FT160,320^XG002.GRF,1,1^FS\n" +
                              "^FT32,384^XG003.GRF,1,1^FS\n" +
                              "^FT256,352^XG004.GRF,1,1^FS\n" +
                              "^FT128,384^XG005.GRF,1,1^FS\n" +
                              "^FT448,288^XG006.GRF,1,1^FS\n" +
                              "^FT224,384^XG007.GRF,1,1^FS\n" +
                              "^FT288,480^XG008.GRF,1,1^FS\n" +
                              "^FT352,480^XG009.GRF,1,1^FS\n" +
                              "^FT384,480^XG010.GRF,1,1^FS\n" +
                              "^FT544,256^XG011.GRF,1,1^FS\n" +
                              "^FT416,288^XG012.GRF,1,1^FS\n" +
                              "^FT288,1632^XG013.GRF,1,1^FS\n" +
                              "^FT352,1632^XG014.GRF,1,1^FS\n" +
                              "^FT0,1984^XG015.GRF,1,1^FS\n" +
                              "^FT0,1824^XG016.GRF,1,1^FS\n" +
                              "^FT0,1664^XG017.GRF,1,1^FS\n" +
                              "^FT0,352^XG018.GRF,1,1^FS\n" +
                              "^FT384,1632^XG019.GRF,1,1^FS\n" +
                              "^FT640,352^XG020.GRF,1,1^FS\n" +
                              "^FT512,288^XG021.GRF,1,1^FS\n" +
                              "^FT736,1376^XG022.GRF,1,1^FS\n" +
                              "^FT608,416^XG023.GRF,1,1^FS\n" +
                              "^FT672,1312^XG024.GRF,1,1^FS\n" +
                              "^FT0,1856^XG025.GRF,1,1^FS\n" +
                              "^FT0,1696^XG026.GRF,1,1^FS\n" +
                              "^FO1,79^GB833,1913,8^FS\n" +
                              "^FT332,1875^A0R,39,38^FH\\^FDvial^FS\n" +
                              "^FT373,1875^A0R,39,38^FH\\^FDvial^FS\n" +
                              "^FT418,1875^A0R,34,40^FH\\^FDvial^FS\n" +
                              "^FT604,1640^A0R,28,28^FH\\^FDSN^FS\n" +
                              "^FT656,1640^A0R,28,28^FH\\^FDDO^FS\n" +
                              "^FT705,1638^A0R,28,28^FH\\^FDPKG DATE^FS\n" +
                              "^FT755,1636^A0R,28,28^FH\\^FDGTIN^FS\n" +
                              "^FT79,469^A0R,56,55^FH\\^FD{18}^FS\n" +
                              "^FT178,469^A0R,51,50^FH\\^FD{17}^FS\n" +
                              "^FT252,469^A0R,56,55^FH\\^FD{16}^FS\n" +
                              "^FT334,1687^A0R,39,38^FH\\^FD{15}^FS\n" +
                              "^FT372,1687^A0R,39,38^FH\\^FD{12}^FS\n" +
                              "^FT414,1687^A0R,39,38^FH\\^FD{9}^FS\n" +
                              "^FT328,469^A0R,39,38^FH\\^FD{13}^FS\n" +
                              "^FT365,469^A0R,39,38^FH\\^FD{10}^FS\n" +
                              "^FT319,842^A0R,39,38^FH\\^FDExp Date ({14})^FS\n" +
                              "^FT364,840^A0R,39,38^FH\\^FDExp Date ({11})^FS\n" +
                              "^FT406,842^A0R,39,38^FH\\^FDExp Date ({8})^FS\n" +
                              "^FT404,469^A0R,39,38^FH\\^FD{7}^FS\n" +
                              "^FT554,469^A0R,56,55^FH\\^FD{1} / {2}^FS\n" +
                              "^FT645,469^A0R,56,55^FH\\^FD{0}^FS\n" +
                              "^FT90,429^A0R,39,38^FH\\^FD:^FS\n" +
                              "^FT180,429^A0R,39,38^FH\\^FD:^FS\n" +
                              "^FT263,429^A0R,39,38^FH\\^FD:^FS\n" +
                              "^FT472,429^A0R,39,38^FH\\^FD:^FS\n" +
                              "^FT560,429^A0R,39,38^FH\\^FD:^FS\n" +
                              "^FT654,429^A0R,39,38^FH\\^FD:^FS\n" +
                              "^BY162,162^FT570,1384^BXR,9,200,0,0,1,~\n" +
                                "^FH\\^FD" + dapatGSoneMasterbox + "^FS\n" +
                              "^FT610,1789^A0R,28,28^FH\\^FD{6}^FS\n" +
                              "^FT607,1773^A0R,28,28^FH\\^FD:^FS\n" +
                              "^FT657,1790^A0R,28,28^FH\\^FD{5}^FS\n" +
                              "^FT656,1774^A0R,28,28^FH\\^FD:^FS\n" +
                              "^FT706,1788^A0R,28,28^FH\\^FD{4}^FS\n" +
                              "^FT705,1772^A0R,28,28^FH\\^FD:^FS\n" +
                              "^FT755,1786^A0R,28,28^FH\\^FD{3}^FS\n" +
                              "^FT755,1770^A0R,28,28^FH\\^FD:^FS\n" +
                              "^PQ1,0,1,Y^XZ\n" +
                              "^XA^ID000.GRF^FS^XZ\n" +
                              "^XA^ID001.GRF^FS^XZ\n" +
                              "^XA^ID002.GRF^FS^XZ\n" +
                              "^XA^ID003.GRF^FS^XZ\n" +
                              "^XA^ID004.GRF^FS^XZ\n" +
                              "^XA^ID005.GRF^FS^XZ\n" +
                              "^XA^ID006.GRF^FS^XZ\n" +
                              "^XA^ID007.GRF^FS^XZ\n" +
                              "^XA^ID008.GRF^FS^XZ\n" +
                              "^XA^ID009.GRF^FS^XZ\n" +
                              "^XA^ID010.GRF^FS^XZ\n" +
                              "^XA^ID011.GRF^FS^XZ\n" +
                              "^XA^ID012.GRF^FS^XZ\n" +
                              "^XA^ID013.GRF^FS^XZ\n" +
                              "^XA^ID014.GRF^FS^XZ\n" +
                              "^XA^ID015.GRF^FS^XZ\n" +
                              "^XA^ID016.GRF^FS^XZ\n" +
                              "^XA^ID017.GRF^FS^XZ\n" +
                              "^XA^ID018.GRF^FS^XZ\n" +
                              "^XA^ID019.GRF^FS^XZ\n" +
                              "^XA^ID020.GRF^FS^XZ\n" +
                              "^XA^ID021.GRF^FS^XZ\n" +
                              "^XA^ID022.GRF^FS^XZ\n" +
                              "^XA^ID023.GRF^FS^XZ\n" +
                              "^XA^ID024.GRF^FS^XZ\n" +
                              "^XA^ID025.GRF^FS^XZ\n" +
                              "^XA^ID026.GRF^FS^XZ";
            String ZplStringtest = String.Format(kirimBetul, masterbox.Model_name, masterbox.ColieNumber,
                masterbox.ColieMax, masterbox.Gtinmasterbox, createdtime, masterbox.ShippingOrderNumber, 
                masterbox.SerialNumber, masterbox.BatchAndQty[0][0],
                masterbox.BatchAndQty[0][2], masterbox.BatchAndQty[0][1], masterbox.BatchAndQty[1][0], 
                masterbox.BatchAndQty[1][2], masterbox.BatchAndQty[1][1], masterbox.BatchAndQty[2][0], 
                masterbox.BatchAndQty[2][2], masterbox.BatchAndQty[2][1], masterbox.Qtytotal, masterbox.Weight, 
                adminid);

            // String ZplString = String.Format(kirimBaru, gtin, batch, expiry, serial, weight, qtytotal, productname, colie,shippingOrderNumber,createdtime);

            return ZplStringtest;
        }

        string SetDataSendPrinterLeft(Var_Masterbox masterbox)
        {
            string[] address = masterbox.CustomerAddress.Split('-');
            if (address.Length == 1)
            {
                address[1] = "";
            }
            string kirimFix = "CT~~CD,~CC^~CT~\n" +
            "~DG000.GRF,25600,040,\n" +
            ",:::::::::::::::::::::::::::::::::::::::::::::::::::::::::W01FFC0,W0JF8,V03FJF,V0MF8,U01FLFE,U03FMFA0,U0PFC,U0PFE80,U0RF0,T01FQFE,T03FRFC0,T03FSF8,T07FTF80,T0VFC0,:T0JFH0PFE0,S01FHFC0H0PF0,S01FHF80801FNF8,S01FHFK01FMFC,S01FHF80J03FLFC,S01FHFL03FLFE,S03FFE0K03FLFE,S03FHFL03FMF,S03FFE0K03FMF80,S03FFE0K03FFE1FIFC0,S03FFE0K03FFE01FHFE0,S03FHFL03FHFH01FFE0,S03FFE0K03FHFI03FE0,S03FHFL03FHFJ03F0,S01FFE0K03FHFK0B8,S01FHFL03FHF0,S01FHF80J03FHF0,::T0IFC0J03FHF0,:T0IFE0J03FHF0,T07FHFK03FHF0,T07FHFK03FFE0,T03FHF80I03FFE0,T03FHFC0I03FFE0,T01FHFE0I07FFE0,T01FIF80H07FFE0,U0JF80H0IFE0,U0JFE001FHFC0,U0KF801FHFC0,U07FJF0FIFC0,U03FOF80,U01FOF80,U08FOFH8,V07FNF,V03FMFE,V01FMFC,W0NFH80,W07FLF8,W03FKFE0,X0LFC0,X03FIFB80,Y0JFD,Y03FHF8,g03FC0,,:S0380,S03F8,S03FB80,S03FHF8,S03FHFE,S03FIFE0,S03FJFA,S03FKFC0,S03FLFC,S03FMFC0,S03FNF8,S03FOF,S03FPF0,S03FPFE,T03FPFA0,U07FPF0,V0BFOF0,W0PF0,U0800FNF01F,X01FMF03FC0,Y0NF07FC0,g0MF07FE0,gG0LF0FHF0,gG01FJF0FHF8,gH02FIF0FHF8,gI03FHF07FF8,gJ03FF07FF8,gK07F07FF8,gL0603FF8,gN01FF8,gO0HF8,W03FFC0M07F0,V01FIF80L0380,V07FIFE,V0LF80,U03FKFC0,U07FLF8,U0NF8,U0NFE,T01FNF,T03FNF80,:T07FMFBE8,T07FNFE0,T0JFEFKF8,T0JFH0KF8,T0IFC00BFIF8,S01FHF80H0JFC,S01FHF80H07FHFC,S01FHFJ01FHFE,S03FFE0I08FHFA,S03FHFK07FHF,S03FFE0J03FHF80,:S03FFE0J01FHF80,S03FFE0J01FHFC0,S03FFE0K0IFC0,S03FFE0K07FFC0,:S01FHFL07FFE0,S01FHF80J07FFE0,S01FHFL07FFE0,S01FHF80J03FFE0,:T0IF80J03FFE0,T0IFC0J03FHF0,T0IF80J03FHF8,T07FFE0J03FHF0,T07FHFK03FHF0,:T03FHF80I03FFE0,T03FHFC0I03FFE0,T01FHFE0I07FFE0,T01FIF80H07FFE0,U0JF80H0IFC0,U0KFH01FHFC0,U07FIFE03FHFC0,U03FOF80,:U01FOF80,V0PF,V07FMFE,V03BFLFE,V01FMFC,W07FLF8,M0C0N03FLF0,M0780N0LFE0,M07FC0M07FJF80,M03FF80M0JFE,M01FHFN07FHFC,N0IFA80L0BFE8,N0KF0,N07FIFE,N03FJFE0,N01FKF8,N01FLFC0,O0MFE0,O0NFC,O07FMFA8,O03FNFC,O03FOF80,O01FPF0,P0QFE,P07FPF,Q0QFE0,Q01FPF0,R02BFNF8,S03FNFC,S03FNFE,S03FOF,S03FOF80,S03FOFC0,S03FFE0FKFE0,S01FFE01FKF0,S01FFE003FBFHF8,T0IFI07FIF8,T0HFE0I0JFC,T07FE0I07FHFC,T03FE0I03FHFE,T01FE0I01FIF,U0FE0J0JF,U0FE0J07FHF80,U0FE0J03FHF80,U07F0J01FHF80,U03E0K0IF80,U01E0K0IFC0,V0E0K0IFC0,V0E0K07FFC0,V060K07FFE0,V020K07FFE0,V020K03FFE0,gI03FFE0,:::::::::gI07FFE0,:gI0IFC0,gH01FHFC0,gH03FHFC0,gH01FHF80,gI0IF80,:gI07FF,gI03FE,gI03FC,gI01FC,W03FFC0H0F8,V01BFHF800F8,V07FIFE0070,V0LF8020,U03FKFE0,U07FLF8,U0NF8,U0NFE,T01FMFE,T03FMFB88,T03FNF80,T07FNFE0,:T0JFABFJF8,T0JFH07FIF8,T0IFC003FIF8,S01FHFC0H07FHFC,S01FHF80H03FHFC,S01FHFJ01FHFE,S01FFE80I0IFE,S03FFE0J07FHF,S03FFE0J03FHF80,:S03FFE0J01FHF80,S03FFE0J01FHFC0,S03FFE0K0IFC0,:S03FFE0K07FFC0,S03FFE0K07FFE0,:S03FFE0K03FFE0,:::S03FFE0K03FHF0,:::S03FFE0K03FFE0,:S03FFE0K07FFE0,S03FHF8080H07FFE8,S03FIF80I07FFE0,S03FJF80H0IFC0,S03FKFH01FHFC0,S03FKFE0FIF80,S03FQF80,:S03FQF,:S03FPFE,S03FPFC,T0QFC,U0PF8,U01FNF0,V03FLFE0,W01FKF80,X0LF80,X01FIFC,Y02FFE8,,::S03E0,S03FC,S03FFC008,S03FHF8,S03FHFE80,S03FJF0,S03FKF80,S03FKFE0,S03FLF8,S03FMF,S03FMFC0,S03FMFE0,S03FMFB8,S03FNFC,T0BFMFA,U0OF,V0NF80,V01FLFC0,W03FKFE0,X01FJFE0,Y03FJF8,g0KF8,g03FIFC,gG0JFC,gG03FHFE,gG01FIF,gH0JF08,gH07FHF,gH03FHF80,:gI0IFC0,::gI07FFC0,:gI07FFE0,:gI03FFE0,:gI03FHF0,:::::::gI07FFE0,::gI0IFC8,gH01FHFC0,S0380L01FHFC0,S03F0M0IF80,S03FE0L0IF80,S03FFE0K07FF80,S03FHFE0J07FF,S03FIFC0I03FE,S03FJF80H09F808,S03FKF8001F8,S03FIFEFE0H0F8,S03FLFC0060,S03FMF8020,S03FMFC0,S03FNF0,S03FNF8,:T07FMFE,U0OF80,V07FLF80,V03FLF80,W03FKFC0,X0BFJFE0,Y07FJF0,W0808FJF8,g03FIF8,gG0JFC,gG07FHFE,gG03FHFE,gG01FHFE,gH0JF,gH07FHF80,gH03FHF80,gH01FHF80,gI0IFC0,::gI07FFE0,::gI0BFFE8,gI03FFE0,:gI03FHF0,::S020N03FHF0,S03C0M03FFE0,S03F80L03FFE8,S03FF80K03FFE0,S03FHF80J07FFE0,S03FIFK07FFE0,S03FJF80H0IFE0,S03FKFH01FHFC0,S03FLFBFIFC0,S03FQF80,S03FLFBFIFH8,S03FQF80,S03FQF,:S03FPFE,S03FPFC,T0BFOF80X02,U07FNFg02,U08FMFE80X0B,V01FLFC0Y03,W03FKFE0Y03,X01FKFg0180,Y03FJF80X0180,g07FIF80X0180,g01FIFC0X0180,gG07FHFC0X01C0,gG03FHFE0Y0C0,gG01FIFg0E0,gH0JFg0E0,gH07FHFg0E0,gH03FHF80X0F0,gH03FHF80X070,gH01FHFC0X070,gI0IFC0X078,gI0IFC80W078,gI07FFE0X07C,gI07FFE0X07C0W020,gI07FFE0X07C0W040,gI03FFE0X03E0V0180,gI03FFE0X03E0V03,gI03FFE0X03E0V06,gI03FFE0X03F0K0H4O0C,gI03FFE80W03F80J0840M018,gI03FFE0X03F80I01040M030,gI03FFE0X03F80I03040M020,S03C0M03FFE0X03F80I07040M0E0,S03F80L03FFE0X03FC0I0E840L01E0,S03FF0L03FFE0X01FC0H01E060L03C0,S03FFE0K03FFE0X03FE0H03C060L0F80,S03FHFE0J07FFE0X03FF0H078060K01D,S03FHFB880H07FFE0X03FF800F80E0K01A,S03FJF80H0IFC0X03FFC01F0070K03E,S03FKFH03FHFC0X03FHF8FF0070K0FC,S03FQFC0X07FJFE00780I01F8,S03FQF80X07FJFE00780I03F8,S03FQF80X07FJFC00780I07E0,S03FQF80X0LF8007C0H01FE0,S03FQFU0C0I0LF8007C0H01FC0,S03FPFE080R0F8803FKF800FE0H07F80,S03FPFC0T01FC0FLFI07F001FF,T0BFOF80U0PFI07F80FHF,U0PF80S0403FMFE0H07FJFE,V0NFA80S0600FMFE0H07FJFC,V01FLFC0T06007FLFC0H07FJFC,W02FKFA0T030H0KFEF0I07FJF8,X05FIFE0U030H0MFJ07FJF0,W080BFBF80U038003BFIFE0I07FJF0,g03FC0V01C0H0KFE0I07FIFE0,h01E0H03FIFC0I03FIFE0,h01E0I0JF80I03FIFE0,h01E0I03FFE80I03FIFE0,h01F0J07FC0J03FIFE0,hG0F80J0A0K03FIFE0,hG0F80Q03FIFE0,:h01FC0Q01FIFE0,h01FE0Q01FJF0,W01FF80X01FE0R0KF0,W0JF80M080N01FE0R0KF8,V03FIFC0M0780M01FF0R07FIFE,V0LF80L02F80L03FF80Q03FJF80,U01FKFE0M01FC0K07FF80Q03FJFE0,U03FLF80M0IF80I0IF80R0BFJF8,U0NF80M01FHFC007FHFC0R01FKFE0,U0NFC0N03FNFC0T03FIFE0,T01FMFE0O07FMFE,T03FNF80O0NFE,T03FNF80O01FLFE,T03FNF80O03FLFE,T07FNFC0P07FLFgG08,T0KFBFIFE80O01FLFg038,T0JF81FJFR07FKFg060,T0IFE003FIF80Q0LFY01E0,S01FHFC001FIFC0Q0LFY07C0,S01FHF80H07FHFC0Q0BFJFX01F80,S01FHFJ03FHFE0Q01FIFE0W03F,S01FFE80H01FHFE0R0JFE0W0FE,S01FFE0J0IFE0R07FHFC0V01FC,S03FFE80I03FFB0R0BFHF80V07F8,S03FHFK03FHF80Q07FHF80V0HF8,S03FFE0J03FHF80Q07FHF80U03FF0,S03FFE0J01FHF80Q07FFE0V07FF0,S03FFE0K0IFC0Q07FFC0V0BFE0,S03FFE0K07FFC0Q07FF80U01FFE0,S03FFE0K07FFE0Q0IFW03FFE0,S03FFE0K07FFE0Q0HFC0V0IFE0,S03FFE0K0BFFE80P0HF80V0IFE0,S03FFE0K07FFE0P01FF0V01FHFE0,S03FFE0K03FFE0P03FA0V03FIF0,S03FFE0K03FFE0P07F0W03FIF0,S03FFE0K03FFE0P0FE0W07FIF8,S03FFE0K03FHFQ0FC0W07FIFC,S03FFE0K03FHFP01F0X07FIFE,S03FFE0K03FHFP03C0X07FJF80,S03FFE0K03FHF80N03080W0LFC0,S03FHFL03FFE0O0C0Y07FKF0,S03FFE0K03FFE0N0180Y07FKF8,S03FFE0K03FFE0gP07FLF,S03FHF80J07FFE0gP07FLFE0,S03FIFK07FFE0gP07FMF8,S03FIFE0I0IFE0gP03FMFE,S03FJFE001FHFC0N03FKF80S03FNFC0,S03FJFBE03FHFC80O0KFB80R01FHFE00BFHF8,S03FQF80P01FKFS01FHFJ01FFE,S03FQF80Q03FJFC0R0HFE0K02FC0,S03FQF80R0KFE0R0HFC0L01F8,S03FQFT0KFE0R0HF80M0BC,S03FPFE0S03FIFB0R07F80N03,S03FPFE0S01FJF80Q07F8,S01FPFC0T0KF80Q07F0,S08BFOF80T0KF80Q03F8,U07FNFV07FIFC0Q01F0,V0NFE0U07FIFC0Q01F8,W07FKFC0U07FIFC0R0F0,X0LF80U07FIFE0J03FE0J0F8,X01FIFC0V07FIFE0J07FF80I070,Y03FHF80V07FIFE0J0IFE0I078,hG07FIFE0I01FIFC0H038,gI080V0KFE0I03FIFE80038,hG0KFE0I07FJF80018,h01FJFE0I0LFE0018,h01FJFE0H01FLF801C,h03FJFE0H01FLFE00C,h07FJFE0H03FMFH06,h07FJFE0H03FMFC06,h0HFE03FE0H07FMFE02,h0HF800FE008FLF8BB80,gY01FF0H07E0H0LFC003E0,gY03F80H03E001FKF80H030,gY03F0I03F003FKFJ018,gY07E0I01F003FJFE,gY0FC0I01F007FJFE,gY0F80J0E00FKFE,gX01F0K0E00FE0FHFC,gX03E0K0E01F803FFC,gX07C0K0601F001FFC,gX0F80K0603E001FFC,gW01E0L060380H0HFC,gW03C0L020780H07FC,gW0380L02040I03FC,gW060M02060I01FC,gW0C0M02080I01FC,gV0180M02880J0FC,gV030N02180J0FC,gV030O010K0FC,gV060V07C,gV080V07E,gU010W03E,hT03E,:hM080K03A,hT01E,hU0E,hU0F,:hU07,::hU0380,::hU0180,:hU01C0,hV0C0,::hV040,,::::::::::::::::::~DG001.GRF,01792,008,\n" +
            ",:::::::::::K017FC0,K03E2F0,K07405C,K0E0H0E,J01C0H07,J0183F8180,J0307FC1C0,J030E0F0C0,J031C070C0,J061801860,J061800C60,J060800C60,J060400C60,J060600860,J061FC5060,J061FHF060,J06197F440,J021803CC0,J031C005C0,J030C00180,J0187007,J01C380E,K0C1D5C,K060FE8,M0140,,::K0K1,K01FIFE0,K01DFDFC0,,::::K015H54,K01FHFC,K015574,N018,N01C,O0C,::N01C,N038,K01FHF0,K01FFE0,,:O0C,:K015H5D,K01FIFC0,K015H5DE0,O0C60,:P060,L0154040,L03FE0,L07570,L0C018,K01C01C,K01800C,::K01C01C,L0C018,L07570,L03FE0,L01540,,::::::::::L0K40,K01FIFE0,:,::::K015H54,K01FHFC,K015574,N018,N01C,O0C,::N01C,K01FHF8,K01FHF0,N030,N018,O08,O0C,::N01C,K017H78,K01FHF0,L04540,,::L05DDC,L0IFC,K01D554,K01C0,:K0180,:L080,L040,L068,K01FHFC,:L04040,,::K01DHDC,K01FHFC,K015574,N018,O0C,:::N01C,N038,K01FHF0,K01FFE0,L04040,,::K01DHDC40,K01FHFC60,K015H5H40,,::L071F0,L0E1F8,K01C39C,K01838C,K01830C,K01870C,:K018618,L0DE38,L0FC30,L054,,:L038,L0FC30,L0HE38,K01C71C,K01830C,::K01C30C,L0C18C,L075DC,L0IF8,K01FHF0,K01,,:L01050,L070F0,L0D1DC,L0C38C,K01830C,:K01870C,K01860C,K01C61C,L0HE38,L07C10,L038,,:::::::::::::::::::~DG002.GRF,01792,008,\n" +
            ",::::::::::::::::::::::::::::K0K1,K01FIFE0,K01DIDC0,,::::K015H54,K01FHFC,K015574,N018,N01C,O0C,::N01C,N038,K01FHF0,K01FFE0,,:O0C,:K015H5D,K01FIFC0,K015H5DE0,O0C60,:P060,L0154040,L03FE0,L07570,L0C018,K01C01C,K01800C,::K01C01C,L0C018,L07570,L03FE0,L01540,,::::::::::L0K40,K01FIFE0,:,::::K015H54,K01FHFC,K015574,N018,N01C,O0C,::L0405C,K01FHF8,K01FHF0,M0230,N018,O08,O0C,::N01C,K0177F8,K01FHF0,L0I40,,::L05DDC,L0IFC,K01D554,K01C0,K0180,::L080,L040,L068,K01FHFC,:M0I4,,::K01DHDC,K01FHFC,K015574,N018,O0C,:::N01C,N038,K01FHF0,K01FFE0,M0H40,,::K01DHDC40,K01FHFC60,K015H5H40,,::L071F0,L0E1F8,K01C3DC,K01838C,K01830C,K01870C,:K018618,L0DE38,L0FC30,L054,,:L038,L0FC30,L0HE38,K01C71C,K01830C,::K01C30C,L0C18C,L075DC,L0IF8,K01FHF0,K01,,:L01050,L070F0,L0D1DC,L0C38C,K01830C,:K01870C,K01860C,K01C61C,L0HE38,L07C10,L038,,:L0404H40,K01FHFC60,:,:::::::::::::::::::::::::~DG003.GRF,15104,008,\n" +
            ",:::::L038,L078,L0E0,K01C0,K0180,::K01C0,L0EAEAE0,L07FHFE0,M0BFFE0,,:::K01DIDC0,K01FIFE0,K015I540,,::K01,K0180,:L080,,:::::::::::K015I540,K01FIFE0,K0155D560,M018060,::::::M01C060,:N0C1C0,N0FBC0,N07FC0,O0E,,:L054,L0FC30,K01D638,K018718,K01830C,::L0830C,L0C18C,L0619C,K017FF8,K01FHF0,K014H40,,:L02020,L071F0,L0E1F8,K01C39C,K01830C,K01870C,::K018618,L0CE38,L0FC20,L054,,O04,O0C,L0I5D40,L0JFC0,K01D55D40,K01800C,:,:M0F80,L07FF0,L0FB70,L0C358,K01C30C,K01830C,:::K01C318,L0E338,L063F0,L023C0,,::L03FFC,L07FFC,L0E0,K01C0,K0180,::L0C0,:K017554,K01FHFC,K01DHDC,,::L0J4,K01FHFC,:N038,N01C,O0C,::O04,,::::::::K015I540,K01FIFE0,K015I5C0,O03C0,O07,N01E,N07C,N0E0,M01C0,M0780,L01F,L03C,L070,K01F2H2A0,K01FIFE0,L0KE0,,::M0F80,L07FF0,L07AF0,L0C05C,K01800C,::::L0C018,L0F078,L05FD0,M0F80,,:::K0180,:K01,,::::::::::K014004,K01E007,K01F007C0,K01B800C0,K019C0040,K018C0060,K01870060,:K0183C060,K0181C0E0,K0181F1C0,K01807F80,K01801F,L080,,:L014,L03F8E,L0H7DFC0,L0C0FBC0,K01C07040,K01806060,::K01C06060,L0C0F0C0,L0D1DFC0,L07F8F80,L05D04,,:::::::::::L0K80,K01FIFE0,:K0180C060,:::::::K01C1E0C0,L0E1FFC0,L07F1F,L03E0A,M04,,:L038,L0FC30,L0HE38,K01C71C,K01830C,::K01C30C,L0C18C,L075DC,L0IF8,K01FHF0,K01,,::K01FHFC,:L0H8F8,N010,O08,O0C,:::L0I5C,K01FHF8,K01FFD0,,::L01540,L03FE0,L07570,L0E018,K01C01C,K01800C,::K01C01C,L0C018,L0757440,K01FIFE0,:,:::L03FFC,L07FFC,L0E8H8,K01C0,K0180,::K01C0,L0C0,L07554,K01FHFC,:,:::K01FHFC,:L0H8F8,N010,O08,O0C,:::L0I5C,K01FHF8,K01FFD0,,::K041540,J01C3FE0,J03C7570,J030C038,J071C01C,J061800C,::J071C01C,J030C018,J01C7574,K0JFC,K05FHFC,,::::::::::M06,::::::,::::::::M04,M0E,M0D,M0DC0,M0C70,M0C30,M0C1C,M0C0E,M0C07,M0C01C0,K01FIFC0,K01FIFE0,L01C10,M0C,M04,,:M02A0,L05FFD,L07EEF80,L0F001C0,L0C0H0E0,K01C0H060,K0180H060,::L0C001C0,L0F803C0,L07FHF,M0HFC,N040,,:::O0C,:O06,O07,O0380,K015H57C0,K01FIFE0,K015I540,,:::::M03F8,L05FFD,L07FCF80,L0D041C0,L0C060C0,K01C07060,K01803060,::K01C07040,L0C060C0,L075C3C0,L03F83,M05,,:::O0C,:O06,O07,O0380,K015H57C0,K01FIFE0,K015I540,,::::::::::::::K015I540,K01FIFE0,K0155D560,M018060,::::::::N0C1C0,N0FBC0,N07F80,N01E,O04,,M0150,M07F8,L01FFD,L0380F80,L0700180,L0E0H0C0,L0C0H0C0,K0180H060,:::::L0C0H0C0,L0C001C0,L0E00180,L07417,L03FFE,M07FC,N0E0,,::::::::::L0K80,K01FIFE0,:K01C0C060,K0180C060,::::::K01C1E0C0,L0E1FBC0,L07F1F,L03E0A,M04,,::M07FC,L01FFE,L07D17,L0600380,L0C001C0,L0C0H0C0,K01C0H060,K0180H060,::::L0C0H0C0,:L07001C0,L0780780,L05DDF,M0HFC,M0H50,,:K0180,K01C0H060,L0E0H0E0,L07001C0,L03C0780,L01F17,M073E,M05FC,M01E0,M01F0,M07BC,L01F1C,L03C0780,L07001C0,L0E0H0E0,K01C0H060,K0180H020,K01,,::::::::::O04,O0E,O06,O07,O03,L0IABC0,K01FIFC0,L0EAEAE0,,:::::::O04,O0E,O06,O07,O03,L0IABC0,K01FIFC0,L0EAEAE0,,::::::L03803,L07803C0,L0E0H0C0,K01C0H040,K0180H060,K01806060,:K01807060,L08070C0,L0C1DDC0,L07B8F80,L05F04,M0C,,:M05D4,L03FFE,L075D7,L0E0E1C0,K01C06040,K01803060,:::L08030E0,L0C061C0,L07BC380,L05FC3,M0E,,::::::::P060,::::::K01FIFE0,:L0J460,P060,::::P040,,L01D40,L03FE0,L0I70,L0E318,K01C31C,K01830C,::K01C30C,L0C318,L0C370,L063E0,L041C0,,::K01DIDC0,K01FIFE0,K015I540,,::K01,K0180,:L080,,:::::::::::M0180,::::L07FFE,:M0180,::::M01,,N040,M0HF8,L01FHF,L07F8F80,L0C041C0,L0C060C0,K01803060,:::K01C07060,L0E060C0,L075C3C0,L03F8380,L015,,:K0180,K01C007,K01F00780,K01F801C0,K01B800E0,K019C0060,K018E0060,K01870060,K01838060,K0181C060,K0180E0C0,K01807FC0,K01803F80,K010H04,,M04,M06,::::::M04,,K014004,K01E007,K01F007C0,K01F800C0,K019C0040,K018E0060,K01860060,K01870060,K01838060,K0181C0E0,K0181F1C0,K01807F80,K01801F,,::K014004,K01E007,K01F007C0,K01F800C0,K019C0040,K018E0060,K01860060,K01870060,K01838060,K0181C0E0,K0181F1C0,K01807F80,K01801F,,::M06,:::::M04,,:K0180,K01C007,K01F00780,K01F001C0,K01B800E0,K019C0060,K018E0060,K01870060,K01838060,K0181C040,K0180E0C0,K01807FC0,K01803F,K010H04,,:M02A0,L05FFD,L07EEF80,L0F001C0,L0C0H0E0,K0180H060,:::K01C001C0,L0F803C0,L07FHF,M0HFE,M0H40,,::L05803,L0780380,L0C001C0,L080H0E0,K01804060,K01806060,::K01C070C0,L0C0DFC0,L075DF,L03F82,M04,,::L05803,L0780380,L0C001C0,L080H0E0,K01804060,K01806060,::K01C070C0,L0C0DFC0,L075DF,L03F82,M04,,:P020,P060,:K010I060,K01E0H060,K01FD0060,M0FC060,M01F060,N03C60,N01F60,O07E0,O01E0,P0E0,P040,,::L05854,L0787F80,L0C077E0,K018010E0,K01801860,:::K01C01060,L0C03060,L0757060,L03FC020,M05,,::L05854,L0787F80,L0C077E0,K018010E0,K01801860,:::K01C01060,L0C03060,L0757060,L03FC020,M05,,::::::::::K015I540,K01FIFE0,K0155D560,N0C060,::::::::N04060,P020,,:L054,L0FC30,K01D638,K018718,K01830C,::L0830C,L0C18C,L0619C,K017FF8,K01FHF0,K014H40,,K01,K018004,K01C01C,L0F038,L07D70,M0FC0,M07C0,M0FC0,L05C70,L0F038,K01C01C,K01800C,K01,,::K0180,:K01,,::::::::::M01,M0180,::::L0H7F6,L07FFE,L045C4,M0180,:::N080,,:L015D4,L03FFE,L075D7,L0E0E1C0,K01C06040,K01803060,:::L08030E0,L0C061C0,L07BC380,L05FC3,M0A,,:K014004,K01E007,K01F007C0,K01B800C0,K019C0040,K018C0060,K01870060,:K0185C060,K0181C0E0,K0181F1C0,K01807F80,K01805F,L080,,:M04,M06,:::::,:K0180,K01C007,K01F00780,K01F801C0,K01B800E0,K019C0060,K018E0060,K01870060,K01838060,K0181C060,K0180E0C0,K01807FC0,K01803F80,K010H04,,:K0180,K01C007,K01F00780,K01F801C0,K01B800E0,K019C0060,K018E0060,K01870060,K01838060,K0181C060,K0180E0C0,K01807FC0,K01803F80,K010H04,,M04,M06,::::::M04,,K014004,K01E007,K01F007C0,K01F800C0,K019C0040,K018E0060,K01860060,K01870060,K01838060,K0181C0E0,K0181F1C0,K01807F80,K01801F,,::M0H50,L03FFE,L07D5FC0,L0E001C0,K01C0H0C0,K0180H060,::K01C0H060,L0C0H0C0,L07445C0,L03FHF80,L017F5,,::M0F,:M0DC0,M0CE0,M0C70,M0C1C,M0C05,M0C0380,K0H1D11C0,K01FIFE0,K01DFDFC0,M0C,:M08,,:::O0C,O0E,O07,:O0380,L0IEFC0,K01FIFE0,L0KA0,,:::::L010,L03803,L07803C0,L0E0H0C0,K01C0H040,K0180H060,K01806060,:K01807060,L08070C0,L0C1DDC0,L07BCF80,L07F05,M0A,,:M0H50,L03FFE,L07D5FC0,L0E001C0,K01C0H0C0,K0180H060,::K01C0H060,L0C0H0C0,L07445C0,L03FHF80,L017F5,,::L01554,L03FFE,L075D7C0,L0E0C1C0,K01C06040,K01803060,:::K018030E0,L0C061C0,L0FBC3C0,L07FC3,M02,,:::::::::::K01FIFE0,:K010101,,:::K01FHFC,:L0H8F8,N010,O08,O0C,:::K01455C,K01FHF8,K01FFD0,,::L01540,L03FE0,L07570,L0E018,K01C01C,K01800C,::K01C01C,L0C018,K01757540,K01FIFE0,K01FHF7E0,,::L01540,L03FE0,L07570,L0C018,K01C01C,K01800C,::K01C01C,L0E018,L07570,L07FE0,L01740,,::K015H54,K01FHFC,K015574,N018,N01C,O0C,::N01C,N038,K01FHF0,K01FFE0,,::M0F80,L07FF0,L0FB70,L0C35C,K01C30C,K01830C,:::K01C318,L0E338,L063F0,L023C0,,:L05040,L070F8,L0F1D8,L0C38C,K01C30C,K01830C,K01870C,:K01C61C,L0CE38,L07C30,L038,,::K01FHFC60,:,::L054,L0FC30,K01D638,K018718,K01830C,::L0830C,L0C18C,L0619C,K017FF8,K01FHF0,K014H40,,:::J02180,J01D80,,::::::::::::K015H54,K01FHFC,K015574,N018,N01C,O0C,::L0405C,K01FHF8,K01FHF0,M0230,N018,O08,O0C,::N01C,K017H78,K01FHF0,L0I40,,:L038,L0FC30,L0HE38,K01C71C,K01830C,::K01C30C,L0C18C,L075DC,L0IF8,K01FHF0,K01,,::K01FHFC60,:,::L0K40,K01FIFE0,:,::L015,L0HF80,K01D570,K07801C,K040H07,J01C0H03,J0185F4180,J030FFE0C0,J031D170C0,J0318038C0,J061801C40,J061800C60,:J060C00C60,J060401C60,J060F83860,J061FF7060,J0618FFCE0,J071805CC0,J03080H0C0,J030C00180,J0186003,J01C5447,K0E0EFC,K0607F0,K02,,::L0KA0,K01FIFE0,L0E8FAE0,L04010,L08008,K01800C,:::L0C05C,L0F8F8,L07FF0,M0F80,M04,,:K01FHFC60,:,::L01540,L03FE0,L07570,L0C018,K01C01C,K01800C,::K01C01C,L0C018,L07570,L03FE0,L01540,,O04,O0C,K015H5D40,K01FIFC0,K015H5DE0,O0C60,:O0860,L0540040,L0FE30,K01D618,K018708,K01830C,::L0830C,L0C11C,L0619C,K01FHFC,K01FHF8,K0150,,::K0177F4,K01FHFC,L0H454,O08,O0C,:O04,,:L0IA8,K01FHFC,L0HEF8,N010,O08,O0C,::N01C,K01DDFC,K01FHF8,K015570,N018,O0C,:::L0H45C,K01FHF8,K01FHF0,,::L054,L0FE30,K01D618,K018708,K01830C,::L0830C,L0C11C,L0619C,K01FHFC,K01FHF8,K0150,,::K01,K0180,:L080,,::M0A80,L07FF0,L0FAF8,L0C01C,K01800C,::K01C00C,L0C018,L07070,L03860,L01040,,:M0F80,L05FD0,L0F8F8,L0C018,K01800C,::::L0C05C,L0F8F8,L07FF0,M0F80,,:::K0180,:K01,,:::K01FHFC60,:,::L01540,L03FE0,L07570,L0E018,K01C01C,K01800C,::K01C01C,L0C018,K01757540,K01FIFE0,K01DIDC0,,::::::::::M04,M06,:::::,::::::::O04,N03C,M05FC,L03F80,K01F4,K01E0,K01F4,L03F80,M05FC,N03C,M017C,L03FE0,K01FD40,K01E0,K01F5,L01F80,M05FC,N03C,N014,,N05C,M03F8,L05FC0,L0FA,K01C0,L0F8,L05F50,M01F8,N07C,N0FC,L057D0,K01FA,K01D0,L0FC,L05F50,M03F8,N05C,O08,O04,N03C,M05FC,L01FC0,K0175,K01E0,K01FC,L02F80,M05F4,N03C,M05FC,L01FE0,K01FD,K01E0,K01FC40,M0FC0,M05F4,N03C,O04,,::K0180,:K01,,:::L0KA0,K01FIFE0,L0E8FAE0,L04010,L08008,K01800C,:::L0C05C,L0F8F8,L07FF0,M0F80,M04,,:K01FHFC60,:,::L01540,L03FE0,L07570,L0C018,K01C01C,K01800C,::K01C01C,L0C018,L07570,L03FE0,L01540,,O04,O0C,K015H5D40,K01FIFC0,K015H5DE0,O0C60,:O0860,L0540040,L0FE30,K01D618,K018708,K01830C,::L0830C,L0C19C,L0619C,K01FHFC,K01FHF8,K0151,,::K017H74,K01FHFC,L0H454,O08,O0C,:O04,,:L0IA8,K01FHFC,L0AEF8,N010,O08,O0C,::N01C,K01DDFC,K01FHF8,K015570,N018,O0C,:::L0H45C,K01FHF8,K01FHF0,,::L054,L0FE30,K01D618,K018708,K01830C,::L0830C,L0C19C,L0619C,K01FHFC,K01FHF8,K0151,,::K01,K0180,:L080,,::M0A80,L07FF0,L0FAF8,L0C05C,K01800C,::K01C00C,L0C018,L07078,L03860,L01040,,:M0F80,L05FD0,L0F8F8,L0C018,K01800C,::::L0C05C,L0F8F8,L07FF0,M0F80,,:::K0180,:K01,,:::K01FHFC60,:,::L01540,L03FE0,L07570,L0E018,K01C01C,K01800C,::K01C01C,L0C018,K01757540,K01FIFE0,K01DIDC0,,:::::::::::::::::::::::::::::::::~DG004.GRF,02816,008,\n" +
            ",::::::::::::L0H7F770,L0KF0,:L0AEFEF0,N0F0F0,:::::N0FDF0,N07FE0,:N03FC0,N01540,,::P0F0,:::::L0J5F0,L0KF0,:L0JEF0,P0F0,::::P0A0,,:::::::::L0K70,L0KF0,:L0FAFAF0,L0F0F0F0,::::::L0F9F9F0,L07FDFE0,:L05F87C0,M0E,,:N040,L0EAEA60,L0IFE70,:L0IDC50,,::M05C0,L01FF0,L03FF8,L07FFC,L0745C,L0E00E,::::L0701C,L07EFC,L05FFC,L01FF0,M07C0,,::::::::::L0575H50,L0KF0,:L0HEFEF0,N0F0F0,:::::::N050F0,P0A0,,:L01410,L07E18,L07F1C,L0HF3C,L0F71E,L0E38E,:L0638E,L071DE,L03FFE,L0IFC,:L0F5D0,L080,,::L0IFE,::L057FC,N01C,O0C,O0E,:O06,O04,,L0I76,L0IFE,:L0EAF8,N01C,O0C,O0E,:N01E,L0IFE,L0IFC,L0IF8,L0H578,N018,O0C,O0E,::L0IDE,L0IFC,:L0HFE0,,::L038,L07F1C,L0HF3C,L0F73C,L0E39E,L0E38E,:L0618E,L0718E,L07DDE,L0IFC,:L0IF0,L04040,,:::::::::M0280,K017FFC,K03FIF80,J017FIFC0,J03F8003F0,J0340I070,J020,,:L0404H40,L0KF0,::L0H5F5F0,N0F0F0,:::::N0F9F0,N07FF0,N07FE0,N01FC0,O0F,,:M0540,L03FF0,L07FFC,:L0739C,L0E38E,:::L0E39E,L073FC,L063F8,L063F0,M0380,,::L0IFE,::L057FC,N01C,O0C,O0E,:O06,O04,L01860,L079FC,:L071DC,L0E3CE,:L0E38E,L0E78E,L0E79E,L0H79C,L07F1C,L05F10,M08,,:M05C0,L01FF0,L07FFC,:L0F3DC,L0E38E,:::L0E39E,L073FC,L063F8,L063F0,M02C0,,::L0IFE,::L0H5FC,N01C,O0E,:::,M0380,L01FF0,L03FF8,L07FFC,L0783C,L0F01E,L0E00E,::L0F01E,L0781C,L07D7C,L03FF8,L01FF0,M0FC0,,:P050,J0380I0F0,J03F5015F0,K0KFE0,K05FIF40,L0IFC,L01550,,:::::::::::::::::::::::::::::~DG005.GRF,00512,008,\n" +
            ",::::::::::::::::::::::::::K0404040404,,::::L07FIF0,L0KFC,K05FHFDFC04,K01FJFE,K01FJF5,K01FKF,K01FFC05D,K01FC001E,K01F80015,K01F0H03F,K05F465DD04,K01F0E1FE,K01F1E1F5,K01E1E1FF,K01E1E1D5,K01E1E1FE,K01F1E1F5,K01F1E3FF,K05FDF5DD04,K01FJFE,K01FJF5,K01FJFE,K01FIFDC,L0KFC,L07FIF0,,K0404040404,,:::~DG006.GRF,00512,008,\n" +
            ",::::::::::::::K0808080808,,:::::L0KFC,K08FJFC08,L0KFC,::::::K08FF0FFC08,L0HF0FFC,:P07C,P03C,P01C,L0HF0E1C,L0HF0F1C,K08FF0F1C08,L0HFEF1C,L0KFC,::,::K0808080808,,:::::::::::::::^XA\n" +
            "^PW879\n" +
            "^FT544,2816^XG000.GRF,1,1^FS\n" +
            "^FT32,2816^XG001.GRF,1,1^FS\n" +
            "^FT32,2560^XG002.GRF,1,1^FS\n" +
            "^FT32,2336^XG003.GRF,1,1^FS\n" +
            "^FT32,480^XG004.GRF,1,1^FS\n" +
            "^FT32,2592^XG005.GRF,1,1^FS\n" +
            "^FT32,2368^XG006.GRF,1,1^FS\n" +
            "^FO28,117^GB829,2680,8^FS\n" +
            "^FT722,170^A0R,113,112^FH\\^FDKEPADA :^FS\n" +
            "^FT115,170^A0R,124,122^FH\\^FDUP. {0} ({1})^FS\n" +
            "^FT236,170^A0R,124,122^FH\\^FD{4}^FS\n" +
            "^FT358,445^A0R,124,122^FH\\^FD       {2}^FS\n" +
            "^FT480,170^A0R,124,122^FH\\^FD{5}^FS\n" +
            "^FT358,170^A0R,124,122^FH\\^FDD/A {3}^FS\n" +
            "^FT600,170^A0R,124,122^FH\\^FDGUDANG VAKSIN^FS\n" +
            "^PQ1,0,1,Y^XZ\n" +
            "^XA^ID000.GRF^FS^XZ\n" +
            "^XA^ID001.GRF^FS^XZ\n" +
            "^XA^ID002.GRF^FS^XZ\n" +
            "^XA^ID003.GRF^FS^XZ\n" +
            "^XA^ID004.GRF^FS^XZ\n" +
            "^XA^ID005.GRF^FS^XZ\n" +
            "^XA^ID006.GRF^FS^XZ";


            string zplstring = string.Format(kirimFix, masterbox.ContactName, masterbox.ContactPhone, address[0], masterbox.Postcode, address[1].Replace(" ", ""), masterbox.CustomerName);
            return zplstring;
        }

        string SetDataSendPrinterRight(Var_Masterbox masterbox)
        {
            string[] address = masterbox.CustomerAddress.Split('-');
            if (address.Length == 1)
            {
                address[1] = "";
            }
            string kirimSent = "CT~~CD,~CC^~CT~\n" +
                                "~DG000.GRF,21888,036,\n" +
                                ",:::::::::X020,:X030,:X010,X018,::X01C,Y0C,::Y0E,Y0E80,Y07,::Y0780,:Y0380,Y03C0,Y02C0L080,Y03E0,:Y03E0W08,Y03F0V030,Y01F0V060,Y03F80J040O0E0,Y01F80J0C0O0C0,Y01F80J0820M0280,Y01FC0I01820M03,Y01FC0I03020M0E,Y01FC0I07020M0C,Y01FE0I0E020L01C,Y01FF0H01E070L038,Y01FF8003C030L0F0,Y01FFC007C070K01F0,Y01FFE00E80380J02E0,Y01FHF83F80780J07C0,Y03FKFH0380J0F80,Y03FJFE00780I03F,Y03FJFE00780I0FE,T080I07FJFC007C0I0FE,T060I0LF8003E0H03FC,T03C001FKF8007E0H07FC,T08EE8FLF800BE800FF8,S0203FNFI07FC03FF0,S0201FMFE0H03FJFE0,S02007FLFE0H03FJFE0,S01803FLFC0H03FJFC0,S01800FLF80H03FJFC0,S018003FKF80H03FJF80,S01C001FKFJ03FJF,S01E0H07FIFE0I03FJFX080,S01E0H01FIFC0I03FJF,T0F0I03FHF80I03FIFE0V02FHF8,T0F0I01FHFK03FIFE0V07FIFC0,T0F80I03F80J03FIFE0V0KFE8,T0F80Q01FIFE0U03FLF,T0FC0Q01FIFE0U07FLFE0,T0FC0Q01FIFE0U0OFC,T0FE0R0JFE0T01FNFHE0,T0FE0R0KFU03FPF8,T0HFS0KF80S07FPFC,I060N01FF0R05FIFC0S07FPFC,I01E80L01FF80Q07FJFT0RFC,J0FC0L01FF80Q07FJF80Q01FQFC,J01FA0K03FF80Q03FJFE0Q01FQFC,K03FFC0I07FFC0R0LFC0P01FQFC,L0FEFE803FHFC0R03EFHFEF80O0BFFEE03FKFC,L01FNFE0S01FKFC0N01FHF8007FJFC,M03FMFE0gP03FHFJ0KFC,N0OFgQ03FFE0J0JFC,N03FMFgQ03FFE0K0IFC,O07FLFgQ03FFC0K07FFC,O01FLFgG080N03FFC0K07FFC,P07FKFg070O07FFC0K07FFC,P01FKF80W08E80N0IFC0K07FFC,Q0LFY01C0O07FFC0K07FFC,Q03FJFY0F80O07FFC0K07FFC,Q01FJFX01F0P07FFC0K07FFC,R0KFX0BF0P0HFEC0K07FFC,R0JFE0W0FE0P07FFC0K07FFC,R07FHFE0V03FC0P03FFC0K07FFC,R07FHFC0V07FC0P07FFC0K07FFC,R03FHF80V0HF80P03FFE0K07FFC,R03FHFW03FF0Q03FFE0K07FFC,R03FFE0V0IFR03FHFL07FFC,R07FFC0V0IFR01FHFL07FFC,R03FE80U02FFE0Q01FHF80J07FFC,R07FF0V03FFE0Q01FHF80J07FFC,R0HFE0V0IFE0Q01FHFC0J07FFC,R0HF80V0JFR01FHFE0J07FFC,R0FE0V02FHFE80Q0IFE0J07FFC,Q01FC0V03FIFS07FHFK07FFC,Q03F80V03FIF80Q07FHF80I0IF8,Q07E0W07FIFC0Q07FHFC0I0IF8,Q0F80W06FIFE0Q03FHFE0I0IF8,P01F0X07FJFR01FIF8003FHF8,P03C0X07FJF80P01FIFE007FHF0,P070Y07FJFE0Q0KFC1FIF0,P0E080W07FKF80P07FIFEFIFE0,gQ07FLFQ03FNFE0,gQ03FLFC0O03FNFE0,gQ07FLFC0O01FNFC0,gQ03FMF80O0OFC0,gQ03FNFP07FMF80,P03FIFE0T03FNFA0N03FMF80,P07FKFC0R01FIFH07FHF80M01FMF,P080FKF80R0IF80I0HFE080L0MFE80,R03FJFC0R0IFL07FC0M03FKF8,S0KFE0R0HFE0L03F80L01FKF0,S03FJFS07FC0M01E0M07FIFC0,S01FJF80Q03FC0X0JF80,T0KF80Q03F80X01FF8,T0KFC0Q03F80,T07FIFC0Q01F80,T03FIFE0M080H09F80,T03FIFE0R0F80,T03FIFE0K0280J0F80,T03FIFE0J03FE0J0780,T03FIFE0J0IFE0I0780,T07FIFE0J0JF80H0780,T07FIFE0I01FIFE0H0380,T07FJFJ03FJFI01C0V05FC0,T0KFE0I03FJFC001C0T082FHFE0080,T0LFJ07FKFI0C0U07FIFC0,S01FKFJ0MF800E0T03FKFA,S01FKFI01FMFH060T07FLFC0,S01FKFI03FMF8020T0NFE8,S03FKFI03FMFE030S01FOF80,S07FF81FF0H07FJFEFHF80T03FOFE0,S07FC007F0H07FKF01F80T07FPFC,S0HFI0BF0H0LFC0H0E0S087FPFC,R01FC0H03F0H0LF80H030T0RFC,R03F80H01F002FKF80W01FQFC,R03F0J0F003FKFY01FQFC,R0FE0J0F003FJFE0X01FQFC,R0FC0J0F007FJFE0X01FQFC,Q01F80J0700FF0FHFE0X03FHFC00FKFC,Q03E0K0700FC03FFE0X01FHFJ0KFC,Q03C0K0380F880FFE0X03FFE80H01EFHFC,Q05C0K0301F0H0HFC0X03FFE0J07FHFC,Q0F0L0303C0H07FE0X03FFE0K0IFC,P01E0L030380H03FC0X07FFC0L0HFC,P03C0L030B0I03FC0X07FFC0M0FC,P0780L01060I01FC0X07FFC0M01C,P060M01040J0FE0X0IFC,P0E0M01040J0FE0X07FFC,O0180M01080J0FE0X0IFC,O010N0H1L07E0X07FFC,O020V03E0X07FFC,O060V03E0X07FFC,O0C0V03E0X03FFE,N010W01E0X03FFE,N020W01E0X03FFE,gM01F0X03FHF,gN0F0X03FHF80,gN0F0X01FHF80,:gN070X01FHFC0,gN0780W01FHFE0,gN0380W01FHFE0,gN0380X0JF0,gN0380X0JF8,gN0380X07FHFC80,gN0180X03FIF,gO080X03FIF80,gO080X01FIFE0,gO080Y0KFA,gO080Y0LFC0,gO040Y03FKF8,gO040Y03FLF80,gO040Y0NFE0,gO040Y0PF,gO060X01FOFE0,gO060X03FPFC,hO07FPFC,:hO0RFC,hN01FQFC,hN09FFEFNFC,hN01FQFC,hN03FIFBFLFC,hN01FHFC00FKFC,hN03FHF80H0KFC,hN03FFE0I01FIFC,hN03FFE0J01FHFC,hN07FFC0K01FFC,hN0BFFC0L03FC,hN07FFC0M03C,hN07FFC,:hN0IFC,hN07FFC,hN03FFC,hN07FFC,hN03FFC80,hN03FFE,::hN03FHF,hN01FHF,hN03FHF80,hN01FHFC0,hN01FHFE0,:hO0IFE0,hO07FHF0,hO07FHF8,hO07FHFE,hO03FIF80,hO01FIFC0,hP0JFE880,hP0LF,hP07FJFE0,hP03FKFC,hP01FLFH8,hP01FMF0,hQ0NFE,hQ07FMFC0,hP082FNFC,hQ01FNFC,hR07FMFC,hR01FMFC,hP0200FMFC,hP06003FLFC,hP0F0H0HFBFIFC,hO01F8001FKFC,hN083F88002FJFC,hO07FC0I03FIFC,hO0HFE0J03FHFC,hN01FHFL07FFC,hN01FHFM0HFC,hN01FHF80L07C,hN03FHFC0M0C,hN01FHFC0,hN03FHF80,hN03FHF,hN03FFE,hN03FFC,hN07FFC,:::hN0IFC,hN07FFC,::hN03FFC,hN07FFC,hN03FFE,:hN03FFE80,hN01FHF,hN03FHF80,hN01FHF80,:hN01FHFC0,hN01FHFE0,hO0IFE0,hO0JF8,hO07FHF8,hO03FHFE,hO03FIF,hO03FIF80,hO01FJF0,hP0KFE,hP07FJFC0,hP03FKFH80,hP03FLF80,hP01FMF0,hQ07FMF80,hQ07FMFE8,hQ01FNFC,hR0OFC,hR07FMFC,hR01FMFC,hS0NFC,hS03FLFC,hT07FKFC,hU0LFC,hV0KFC,hV01BFHFC,hW01FHFC,hX0BFFC,hY03FC,i03C,,::hR0IF8,hQ07FIF,hQ0JFHE8,hP01FKFC,hP03FLF80,hP07FMF8,hP0PF80,hO03FPF0,hO03FPFC,hO07FPFC,hO0RFC,hN01FQFC,::hN03FHFE8FLFC,hN01FHFC007FJFC,hN03FHF80H0KFC,hN03FHFJ01FIFC,hN0BFFE80H082FHFC,hN03FFC0K07FFC,:hN07FFC0K07FFC,hN0IFC0K07FFC,hN07FFC0K07FFC,::hN0BFFC0K07FFC,hN07FFC0K07FFC,hN03FFC0K07FFC,hN07FFC0K07FFC,hN03FFE0K07FFC,:hN03FHFL07FFC,hN01FHFL07FFC,hN03FHF80J07FFC,hN01FHF80J07FFC,hN01FHFC0J07FFC,:hO0IFE0J07FFC,hO0JFK07FFC,hO07FHF80I0IF8,:hO03FHFE0I0IF8,hO03FIFI01FHF8,hO01FIF8003FHF8,hO01FIFE00FIF0,hP0KFEBFIF0,hP07FNFE0,hP03FNFE0,hP01FNFC0,hQ0OFC0,hQ07FMFC0,hQ03FMF80,hQ01FMF,hR0MFE,hR03FKFC,hR01FJFB0,hP06007FIFE0,hP0E0H0JF80,hO01F0H03FFC,hO03F8,hO07FC,hO0HFE,:hN01FHF,hN01FHF80,hN09FFE80,hN01FHFC0,hN03FHF80,hN03FHF,hN03FFE,::hN07FFC,hN0IFC,hN07FFC,::::hN03FFC,hN07FFC,hN0BFFC0K040,hN03FFE0K040,hN03FFE0K060,hN01FFE0K070,hN03FHFL078,hN01FHFL078,hN03FHF80J07C,hN01FHFC0J07E,hN01FFEE0J07E80,hN01FHFE0J07F,hO0IFE0J07F80,hO0JF80I07FC0,hO07FHFC0I07FC0,hO03FHFE0I07FE0,hO03FIF80H07FE0,hO01FIFE0H07FF0,hP0KFH807FF8,hP0LFC07FFC,hP07FKF87FFC,hP03FOFC,hP01FOFC,:hQ0PFC,hQ03FNFC,hR0PF80,hR07FOF8,hR03FPF80,hS0QFE0,hS03FPF0,hT0QF8,hT02FOF8,hU01FNFC,hV03EFLFE,hW07FMF,hX07FLF80,hX01FLF80,hY03FKF80,i07FJFC0,iG03FIFE0,iH0KF0,hR03EE0L08FIF0,hQ01FHFC0M0IF8,hQ0BFIF80L03FFC,hP01FJFC0M01FC,hP03FKF80M03E,hP07FKFC0N03,hP0MFE,hO03FMF80,hO03FMFC0,hO07FMFE0,hO0PF0,hN01FOF8,:hN01FOFC,hN03FHFE03FIFE,hN01FHF800FJF,hN03FFE0H03FHFE80,hN03FFE0I0JF80,hN03FFE0I03FHFC0,hN07FFC0I03FHFC0,hN07FFC0I01FHFC0,hN07FFC0I01FHFE0,hN07FFC0J0IFE0,hN07FFC0J07FFE0,hN0IFC0J03FFE0,hN07FFC0J03FHF0,hN03FFC0J01FHF8,hN07FFC0J01FHF8,hN03FFC0K0IF8,hN03FFE0K0IF8,:hN03FFE0K07FFC,hN0BFHF80J0IFC,hN01FHFL07FFC,hN03FHF80J07FFC,hN01FHF80J07FFC,hN01FHFC0J07FFC,:hN01FHFE0J07FFC,hO0JFK07FFC,hO07FHF80I0IF8,hO07FHFC0I0IF8,hO03FHFE0I0IF8,hO03FIFI01FHF8,hO01FHFEC003FHF8,hO01FJFH07FHF0,hP0LFBFIF0,hP07FNFE0,hP0BFNFE0,hP01FNFC0,hQ0OFC0,hQ07FMFC0,hQ03FMF80,hQ01FMF,hR0MFE,hR01FKFC,hR08FKF8,hS07FIFE0,hJ0380M0JF80,hJ07E0M03FFC,hJ0HF80,hJ0HFC0,hJ0HFE020,hJ0HFE07E,hJ0HFE0FEE080,hI01FFE07FFC,hJ0HFE07FHF80,hJ0IF07FIF8,hJ0HFE0FJFE80,hJ07FE07FKF0,hJ03FE07FKFE,hJ01FE07FLFC0,hK0F80FNF8,hN07FNF,hN07FNFE0,hN07FOFE,hN03FPFE0,hO07FPFC,hP0QFC,hP01FOFC,hR0FEFLFC,hR01FMFC,hS03FLFC,hT03FKFC,hU03FJFC,hV07FIFC,hW0JFC,hX0IFC,hY0EFC,hY017C,i03C,,:hR01FC0,hR0IFC,hQ07FIF80,hP09FJFE0,hP03FKF0,hP0MFC,hO01FLFE,hO03FMF80,hO07FMFC0,hO07FMFE0,hO0OFE0,hO0PF8,hN01FOF8,hN01FOFC,hN01FIF07FIFE,hN03FHFC00FIFE,hN01FHF8007FIF,hN03FHFI03FIF80,hN07FFE0I0JF80,hN03FFE0I03FHFC0,hN07FFC0I03FHFC0,hN03FFC0I01FHFC0,hN07FFC0J0IFE0,hN0IFC0J0IFE0,hN07FFC0J07FHF0,hN0IFC0J03FHF0,hN07FFC0J03FHF0,hN0IFC0K0IF8,hN07FFC0J01FHF8,hN0IFC0K0IF8,hG010K07FFC0K0IF8,hG01E0J0IFC0K07FF8,hH0FE0I07FFC0K07FFC,hH0HFE0H0IFC0K07FFC,hH07FFC007FFC0K07FFC,hH03FHF80BFFC0K07FFC,hH01FIF07FFC0K07FFC,hI0NFC0K07FFC,::hI03FLFC0K0IF8,hI03FLFE0K0IF8,hI01FMFC0J0IF8,hJ0OF80081FHF8,hJ07FNF8003FHF8,hJ03FOFH0JF0,hJ03FUF0,hJ03FTFE0,hJ01FTFE0,hK03FSFE0,hL03FRFC0,hL08BFEFOFC0,hN07FPF80,hO0QF,hO07FOF,hP07FMFE,hQ07FLF8,hR0MF0,hS0KFC0,hR080FIF8080,hT01FF8,,::::::::::::::::::::::::::::::~DG001.GRF,01792,008,\n" +
                                ",:::::::::N063FHF8,:N0K20,,:Q01C0,P083E0,O01C770,O038638,O030618,O030E18,O030C18,:O031C30,O03B8B0,P0F0E0,P0A080,,:S08,P0IF8,O01FHF0,O03BAE0,O031830,O030C38,O030C18,::O038E38,O01C770,P0C3F0,Q01C0,,:Q02A0,P0C3F0,O01C7B0,O018618,O030E18,:O030C18,O031C18,O039C38,O01F870,P0F8E0,,::N0H2IA8,N063FHF8,N023BHB8,,::P02020,P07FF8,P0IF8,O01C0,O0380,O03,:::O0180,O02EAA8,O03FHF8,O03BHB8,,::P02020,O03FHF8,:Q0160,R020,R010,R018,:R038,:O02AAB8,O03FHF0,O03BBA0,,::P02A20,P0IF8,O01EHE8,O0380,O03,::O01,O0180,P0C0,P0IF8,O01FHF8,O0380,O03,::O0380,O0180,O02EAA8,O03FHF8,O02AHA8,,::::N07FIF8,:N0K20,,::::::::::P02A80,P07FC0,P0EAE0,O018030,O038038,O030018,::O038038,O018030,P0EAE0,P07FC0,N0202A80,N060,N063,:N07BAHA8,N03FIF8,O0BAHA8,O03,:,:P07FF8,P0IF8,O01C0,O0380,O03,::O0380,O0180,O02EAA8,O03FHF8,O02AHA8,,::::N03FBFB8,N07FIF8,O0K8,,::P028,O017F06,O03AB83,O0701C380O0E00E180N0180030C0N03A0038C0N0H3C01840N0H2FE9860N060FHF860N060A3F860N061006060N063002060N063001060N063001860N061801860N030E038C0N030F070C0N0383FE0C0N0181FC180O0E0H0380O070H07,O03A02E,P0F47C,P03FE8,,::::::::::::::::~DG002.GRF,01536,008,\n" +
                                ",::::::::::::::::::::N063FHF8,:N0I2020,,:Q01C0,P083E0,O01C770,O038638,O030618,O030E18,O030C18,:O031C30,O03B8B0,P0F0E0,P0A080,,:S08,P0IF8,O01FHF0,O03BAE0,O031830,O030C38,O030C18,::O038E38,O01C770,P0C3F0,Q01C0,,:Q02A0,P0C3F0,O01C7B0,O018618,O030E18,:O030C18,O031C18,O03BC38,O01F870,P0F8E0,,::N0H2IA8,N063FHF8,N023BHB8,,::P0H2,P07FF8,P0IF8,O01C0,O0380,O03,:::O0180,O02EAA8,O03FHF8,O03BHB8,,::O0I2,O03FHF8,:Q0160,R020,R010,R018,::R038,O02AAB8,O03FHF0,O03BBA0,,::P0I20,P0IF8,O01FEE8,O0380,O03,::O01,O0180,P0C4,P0IF8,O01FHF8,O03A020,O03,::O0380,O0180,O02EAA8,O03FHF8,O02AHA8,,::::N07FIF8,:N0K20,,::::::::::P02A80,P07FC0,P0EAE0,O018030,O038038,O030018,::O038038,O018030,P0EAE0,P07FC0,N0202A80,N060,N063,:N07BAHA8,N03FIF8,O0BAHA8,O03,:,:P07FF8,P0IF8,O01C0,O0380,O03,::O0380,O0180,O02EAA8,O03FHF8,O02AHA8,,::::N03BIB8,N07FIF8,O0K8,,:~DG003.GRF,15104,008,\n" +
                                ",::::::::::::::::::::::::::::N03BIB8,N07FIF8,N02AEAE8,O018030,O038038,O030018,::O038038,O018070,P0EAE0,P07FC0,P02A80,,::N063FHF8,:,:::S08,R018,:,:::P01F,P0HFE0,O01F1F0,O03A030,O030018,::::O018030,O01F1F0,P0BFA0,P01F,,:P02080,P061C0,O01E0E0,O018030,O030038,O030018,::O03A030,O01F5F0,P0HFE0,P015,,::R010,R018,:S08,,::Q08A8,O01FHF8,O03FHF8,O039860,O039830,O030C10,O030C18,::O010E18,O0186B8,P0C7F0,Q02A0,,::P0IF8,O01FHF8,O03A220,O03,:::O0180,P0EAA8,O01FHF8,O03FBB8,O0380,O03,::O01,P080,O01F750,O03FHF8,O015H50,,:O02,O03,:O01,O02A220,O03FHF8,O02EHE8,,::Q08A8,O01FHF8,O03FHF8,O039860,O039830,O030C10,O030C18,::O010E18,O0186B8,P0C7F0,N02002A0,N061,N063,:N07BAHA8,N03FIF8,N02BAHA8,O03,O02,,P02A80,P07FC0,P0EAE0,O018030,O038038,O030018,::O038038,O018030,P0EAE0,P07FC0,P02A80,,::N063FHF8,:,:Q02,P01F,P0HFE0,O01F1F0,O03A030,O030018,:::O010010,P08020,N075F170,N07FIF8,N0K50,,:::S08,R018,:,::O02,O03C0,O02FA,P03F,P023F8,R078,Q0BF8,P07F80,O03FA,O03C0,O02FA,P01F40,Q03F8,R078,Q0AE8,P03F80,O03FA,O03C0,O02,O01,O03A0,O01FC,P0AFA0,Q03F0,R0B8,Q05F8,P0BEA0,O03F0,O03E0,O01F8,P0AFA0,Q01F0,R038,Q05F0,P03FA0,O01FC,O03A0,,O0280,O03C0,O03FA,P01F80,Q0AF8,R078,P02BF8,P07FC0,O03E8,O03C0,O03FA,P01FC0,Q02F8,R078,Q02F8,P01FC0,O03FA,O03C0,O02,,::::::::Q06,:::::Q02,,::::::::::N03BIB8,N07FIF8,N02AEAE8,O018030,O038038,O030018,::O038038,O018070,P0EAE0,P07FC0,P02A80,,::N063FHF8,:,:::S08,R018,:,:::P01F,P0HFE0,O01F1F0,O03A030,O030018,::::O018030,O01F1F0,P0BFA0,P01F,,:P02080,P061C0,P0E0E0,O018030,O030038,O030018,::O038030,O01F5F0,P0HFE0,P015,,::R010,R018,:S08,,::R0A8,O01FHF8,O03FHF8,O039860,O038830,O030C10,O030C18,::O010E18,O0186B8,P0C7F0,Q02A0,,::P0IF8,O01FHF8,O03A220,O03,:::O0180,P0EAA8,O01FHF8,O03FBB8,O0380,O03,::O01,P080,O01F770,O03FHF8,O015H50,,:O02,O03,:O01,O02A220,O03FHF8,O02FEE8,,::R0A8,O01FHF8,O03FHF8,O039860,O038830,O030C10,O030C18,::O010E18,O0186B8,P0C7F0,N02002A0,N061,N063,:N07BAHA8,N03FIF8,N02BAHA8,O03,O02,,P02A80,P07FC0,P0EAE0,O018030,O038038,O030018,::O038038,O018030,P0EAE0,P07FC0,P02A80,,::N063FHF8,:,:Q02,P01F,P0HFE0,O01F1F0,O03A030,O030018,:::O010010,P08020,N075F170,N07FIF8,N0K50,,::S04,P0FE06,O03F707,O0E22A380O0C006180N0180030C0N030H010C0N0H3A018E0N073FF1860N060EFF860N061C1F060N063802060N063003060N063001860:N023801860N031C018C0N030E8B8C0N0307FF0C0N0182FA180O0C0H0380O0E0H02,O03801E,P0EAB8,P01FF0,Q0A80,,::N07FIF8,:N0K20,,::N063FHF8,:,::S08,P0IF8,O01FHF0,O03BAE0,O031830,O030C38,O030C18,::O038E38,O01C770,P0C3F0,Q01C0,,:P0I20,P0IF8,O01EHE8,O0380,O03,::O01,O0180,P0C4,P0IF8,O01FHF8,O03A020,O03,::O0380,O0180,O02EAA8,O03FHF8,O02AHA8,,::::::::::::R01B80R01840,:::P0I28,P0IF8,O01FFE8,O039860,O031830,O030C10,O030C18,::O018E18,O01C6B8,P0C3F0,Q02A0,,::N063FHF8,:,::Q01C0,P0C3E0,O01C730,O038638,O030E18,:O030C18,O030C38,O031C30,O01B8F0,O01F0E0,P020A0,,:P03C40,P0FC60,O01CC70,O018C38,O030C18,:::O030C38,O03AC30,P0EDF0,P0HFE0,P01F,,::P07FF8,P0IF8,O01C0,O0380,O03,::O0380,O0180,O02EAA8,O03FHF8,O02AHA8,,::P02E80,P07FE0,P0EAE0,O018070,O038038,O030018,::O038038,O018030,P0EAE0,P07FC0,P02A80,,::N07EFHF8,N07FIF8,N02AEAE8,O018030,O038038,O030018,::O038038,O018070,P0EAE0,P07FC0,P02A80,,::P0BFF8,O01FHF8,O03AA28,O03,:::O01,P080,O01F110,O03FHF8,:,:::O080808,N07FIF8,:,:::::::::::Q04,O0C3FE0,N03C3DF0,N0386030,N070C018,N060C018,:::N0206038,N0383070,N03EBAE0,O07FFC0,O02AA80,,::O0AFE80,N01FHFC0,N03A22E0,N030H030,N060H038,N060H018,::N030H038,N0380070,N03FABE0,O07FFC0,P0HA,,:Q05,O0A0FE0,N01F3DE0,N03BB830,N030E010,N060E018,N0606018,:N060H018,N020H038,N030H070,N03C01E0,O0C01C0,R080,,:::::N0K50,N07FIF8,N03F7H70,N01C,O0E,:O07,O03,,:::Q01,Q03,:N03FBFB8,N07FIF8,N0388B88,N01C03,O0A03,O0383,P0E3,P073,P03B,Q0F,:,::O0AFE80,N01FHFC0,N03A22E0,N030H030,N060H038,N060H018,::N030H038,N0380070,N03FABE0,O07FFC0,P0HA,,::O0F8018,N01FE018,N038F818,N0703818,N0601C18,N0600E18,N0600618,N0600718,N0200398,N03001F8,N03E00F8,O0E0078,O020028,,Q02,Q06,::::::Q02,,O020H08,N01FC018,N03FE018,N0307018,N0603818,N0601C18,N0600E18,N0600718,N0600398,N07001D8,N03801F8,N01E00F8,O0E0038,R018,,:O020H08,N01FC018,N03FE018,N0307018,N0603818,N0601C18,N0600E18,N0600718,N0600398,N07001D8,N03801F8,N01E00F8,O0E0038,R018,,:Q06,:::::Q02,,:R010,O0FA018,N01FE018,N038F818,N0703818,N0603A18,N0600E18,:N0600318,N0200398,N03001D8,N03E00F8,O0E0078,O020028,,:Q05,O0C3FA0,N01C3DE0,N0386030,N070C010,N060C018,:::N0206038,N0387070,O0EBAE0,O07FFC0,O02BA80,,:P010,P018,:::O023A20,O07FFE0,O06FEE0,P018,::::Q08,,::::::::::S08,R018,:,::S08,O030018,O038038,O01C0F0,P0E3A0,P03F,P03E,P03F,P0EBE0,O01C0F0,O038038,O020018,S08,,P0I28,P0IF8,O01FFE8,O039860,O031830,O030C10,O030C18,::O018E18,O01C6B8,P0C3F0,Q02A0,,:N040,N06020,N06030,::::::::N06ABAA8,N07FIF8,N02AIA8,,::::::::::Q0A,N0403FC0,N060EAE0,N060C030,N0608038,N0618018,:::N0708018,N07EE030,N01FE1E0,O02A1A0,,::Q0A,N0403FC0,N060EAE0,N060C030,N0608038,N0618018,:::N0708018,N07EE030,N01FE1E0,O02A1A0,,::N020,N070,N078,N07E,N06F80,N063C0,N060F8,N0603F,N0600BF8,N060H078,N060I08,N060,:N040,,:Q02,O041FC0,O0FBAE0,N03FB030,N030E038,N0606018,::N0602018,N070H010,N0380030,N01C01E0,O0C01A0,,::Q02,O041FC0,O0FBAE0,N03FB030,N030E038,N0606018,::N0602018,N070H010,N0380030,N01C01E0,O0C01A0,,::P0H2,O07FF,O0IFE0,N03C01F0,N0380038,N060H018,:::N070H030,N03800F0,N01F77E0,O0BFFA0,P054,,:O020H08,O0FC018,N03FE018,N0307018,N0203818,N0601C18,N0600E18,N0600718,N0600398,N07001D8,N03800F8,N01E00F8,O0E0038,R018,,:Q02,Q06,:::::,::O0F8018,N01FE018,N038F818,N0703818,N0601C18,N0600E18,N0600618,N0600718,N0200398,N03001F8,N03E00F8,O0E0078,O020028,,::O0F8018,N01FE018,N038F818,N0703818,N0601C18,N0600E18,N0600618,N0600718,N0200398,N03001F8,N03E00F8,O0E0078,O020028,,Q02,Q06,::::::Q02,,O020H08,N01FC018,N03FE018,N0307018,N0603818,N0601C18,N0600E18,N0600718,N0600398,N07001D8,N03801F8,N01E00F8,O0E0038,R018,,:Q0A80,N01C1FC0,N03C3AE0,N0306070,N060E038,N060C018,:::N0306030,N0382030,N01F1FE0,O0IF80,O01FF,P020,,Q08,P018,::::O07FFE0,:P018,::::,:::::::::::R010,R018,:S08,,::N02AIA8,N07FIF8,N03BIB8,,::P03820,P07C60,P0EC30,O018C30,O030C38,O030C18,::O038C38,O018C70,P0IE0,P07FC0,P02B80,,N020,N060,::::N062I20,N07FIF8,:N060,::::::,::::::::Q07,O0C3FA0,N01C3DE0,N0386030,N070C010,N060C018,:::N0206038,N0387070,O0EBAE0,O07FFC0,O02BA,,:Q03,O020FA0,N01F1DE0,N03BB830,N030E010,N060E018,N0606018,:N060H018,N020H038,N030H070,N03C01E0,O0C01C0,,::::::N0757570,N03FIF8,N03D5H50,O0C,O0E,O06,O07,O02,,:::::::N0757570,N03FIF8,N03D5H50,O0C,O0E,O06,O07,O02,,::::::::::S08,N040H018,N060H038,N070H070,N03800E0,N01E03C0,O038F80,O03DE,P0F8,P078,O03FA,O07CE,O0E8F80,N01E03C0,N03800E0,N070H070,N060H038,R018,,:P0HA,O03FF,O0FBBA0,N01E01E0,N03800E0,N030H030,:N060H018,::::N060H038,N030H030,N0380030,N01C0060,O0E8BE0,O07FF80,O03FE,,::Q02,O0507C0,O0F8FE0,N03DF870,N0307838,N0603018,::::::N0603038,N07FIF8,:N0K10,,::::::::::P070,O03FE,O07FFC0,O0E82E0,N0180070,N0380030,N030H030,N060H018,:::::N030H030,N030H070,N01800E0,N01F01C0,O0BFF80,O01FE,P0A8,,O02,O0780,N01FE0,N03DF0,N03830,N06018,::::::::N06ABAA8,N07FIF8,N02AIA8,,::::::::::::::N02AIA8,N07FIF8,N03EAHA8,N01C,O0E,O06,O03,:,:::Q0A,O0C1FC0,N03C3AE0,N0306030,N020E038,N060C018,::N060E038,N0306030,N03820B0,N01F3FE0,O0BFFA0,O01FC,,:::::N02AIA8,N07FIF8,N03EAHA8,N01C,O0E,O06,O03,:,:::P020,O03FF,O0IFE0,N03C01F0,N0380030,N060H018,::N060H038,N070H030,N03800F0,N01F77E0,O0BFFA0,P054,,:Q02,Q03,P08380,N07FIF8,N03FIF8,N03803,O0E03,O0703,O0383,P0C3,P0E3,P03B,Q0B,Q07,Q02,,::::::::Q06,::::::,::::::::::O03FHFA,O03FIF,O02EAE380O018030C0O038038E0O03001860::O038038E0O01C030C0P0EAE3C0P07FC380P02A82,,::P0BFF8,O01FHF8,O03AHA0,O03,:::O01,P080,O01F110,O03FHF8,:,:::O03FHF8,:O02AAE0,R030,R038,R018,::R038,O0I170,O03FFE0,O03FFC0,,:::N07FIF8,:N0H2EAE0,O018030,O038038,O030018,::O038038,O018070,P0EAE0,P07FC0,P02A80,,::P0BFF8,O01FHF8,O03AHA0,O03,:::O01,P080,O01F110,O03FHF8,:,::S08,P0IF8,O01FHF0,O03BAE0,O031830,O030C38,O030C18,::O038E38,O01C770,P0C3F0,Q01C0,,:Q02,O0507C0,O0F8FE0,N03FF870,N0307838,N0603018,:::::::N07FIF8,:N0K10,,:::::::::::O020BA0,N01F1FE0,N03FB8B0,N030F030,N0606038,N0606018,::N020E038,N03DF030,N03FBEE0,O071FC0,Q0280,,:R010,O0F8018,N01FE018,N038F818,N0703818,N0603C18,N0600E18,:N0600318,N0200398,N03001D8,N03E00F8,O0E0078,O020028,,::::::::::S08,R018,:,:::P01F,P0BFA0,O01E0F0,O018030,O030018,::::O03A030,P0F5E0,P0HFE0,P01F,,::N0K70,N07FIF8,N054H4F8,R0E0,Q03C0,Q0F80,P01E,P038,P070,O03E0,O0780,O0E,N03C,N03AIA8,N07FIF8,N02AIA8,,::::::::O02,O03,::O0380,O01C0,O03FHF8,:O0J20,,::O03BHB8,O03FHF8,O02AAE8,R030,:R018,::R038,R070,O03FFE0,O03FFC0,,::P03C40,P0FC60,O01CC70,O018C38,O030C18,:::O030C38,O01AC30,P0EDF0,P0HFE0,P01F,,:O030018,:N02BAAB8,N03FIF0,N02BAHA0,O03,O02,,Q02A0,P043F0,O01C730,O018618,O030E18,::O030C18,O039C38,O01F870,P0F8E0,P04040,,:P0I28,P0IF8,O01FFE8,O039860,O031830,O030C10,O030C18,::O018E18,O01C6B8,P0C3F0,Q02A0,,:O07,N03FE0,N03DF0,N03830,N06038,:N06018,::::::N06ABAA8,N07FIF8,N02AIA8,,:::::::::::R010,R018,:S08,,::N02AIA8,N07FIF8,N03BIB8,,:::N07FFD,N07FHFE0,N0757570,R038,R018,::R038,R070,Q01E0,Q01C0,,::::::::::~DG004.GRF,02816,008,\n" +
                                ",::::::::::::::::::::::::P0HA80,O03FHF0,N02FIFA,N07FJF,N0FA80AFC0N0F0I01C0N0A0,,:P03F,P0HF80,O01FFC0,O03EBE0,O0381E0,O0780F0,O070070,::O0780F0,O03C1E0,O03FFE0,O01FFC0,P0HF80,P01C,,O07,:::O0380,O03FAA0,O07FHF0,::,::P034,P0FC60,O01FC60,O03FCE0,O079C70,O071C70,:::O03BCF0,O03FFE0,:P0HF80,P03A,,:Q01,P08FA0,O038FE0,O039EE0,O079E70,O071E70,O071C70,O073C70,:O03B8E0,O03F9E0,:P06180,O02,O06,O07,:O03,O0380,O03FEA0,O07FHF0,::,::P01C,P0FC60,O01FC60,O03FCE0,O079C70,O071C70,:::O039CE0,O03FFE0,:P0HFC0,P02A,,:O0F,N03F80,N07FE0,N0HFE0,N0F9F0,N0F0F0,:::::N0FAFAA0,N0KF0,::N0I2020,,:T040N0E0I02C0N0FC001FC0N03FIFE80N01FIFC,O03FFE8,P014,,:::::::::P02020,P0IF0,O03FHF0,:O07BBE0,O0718E0,O071860,O071C70,:O079C70,O03CEF0,O03CFF0,O038FE0,Q01C0,,::P07FF0,O03FHF0,:O07BHB0,O07,::O03,O0180,O01EAA0,O01FHF0,O03FHF0,O07FHF0,O0780,O07,:O03,O0380,O01F570,O07FHF0,:O06EHE0,,O02,O06,O07,:O03,O0380,O03FEA0,O07FHF0,::,::R010,P0BAF0,O03FHF0,:O07FFC0,O07B8E0,O071C60,O071C70,:O078EF0,O03CFF0,O038FE0,O0187E0,P08280,,:N050,N0F0A0,N0F0F0,:::::::N0F7F770,N0KF0,:N0IAEA0,,::::::::::P03E,P0HF80,O03FFA0,O03F7E0,O0380E0,O070070,::::O03A2E0,O03FFE0,O01FFC0,P0HF80,P03A,,::N0A3BHB0,N0E7FHF0,:N0657570,P020,,:Q07,N03E1FA0,N07FBFE0,:N0F9F9F0,N0F0F0F0,::::::N0F5F5F0,N0KF0,:N0KE0,,:::::::::N050,N0F0,::::N0F7I70,N0KF0,:N0FAIA0,N0F0,:::::,::N02A80,N03FC0,N07FE0,:N0FBF0,N0F0F0,:::::N0F7F750,N0KF0,:N0HEFEE0,,:::::::::::::::::~DG005.GRF,00512,008,\n" +
                                ",::::::::::::::::::::::::::::::K0101010101,,N07FHFE0,M03FJF0,M01DFIF8,M07FJF8,M057FIFC,M03FJF8,K0105DDFDFD,M07FC7878,M057C7C7C,M03FC7878,M0H5C7C7C,M07FC7878,M057C7C7C,M03FC7878,K0105DD71FD,M07E0H0F8,M054001FC,M03C003F8,M05D01FFC,M07FJF8,M057FIFC,M03FJF8,K0101FDFHF9,M03FJF0,N0JFE0,,::::K0101010101,~DG006.GRF,00512,008,\n" +
                                ",::::::::::K0101010101,,::M03FJF0,::M038F7FF0,K01038F0FF1,M038F0FF0,M03870FF0,M0380,M03C0,M03E0,M03FF0FF0,:K0103FF0FF1,M03FJF0,::::::K0103FJF1,M03FJF0,,:::::K0101010101,,:::::::::::::::::::^XA\n" +
                                "^PW879\n" +
                                "^FT32,704^XG000.GRF,1,1^FS\n" +
                                "^FT768,320^XG001.GRF,1,1^FS\n" +
                                "^FT768,544^XG002.GRF,1,1^FS\n" +
                                "^FT768,2464^XG003.GRF,1,1^FS\n" +
                                "^FT768,2784^XG004.GRF,1,1^FS\n" +
                                "^FT768,352^XG005.GRF,1,1^FS\n" +
                                "^FT768,608^XG006.GRF,1,1^FS\n" +
                                "^FO20,81^GB829,2709,8^FS\n" +
                                "^FT155,2737^A0B,113,112^FH\\^FDKEPADA :^FS\n" +
                                "^FT762,2737^A0B,124,122^FH\\^FDUP. {0} ({1})^FS\n" +
                                "^FT640,2737^A0B,124,122^FH\\^FD{4}^FS\n" +
                                "^FT518,2462^A0B,124,122^FH\\^FD       {2}^FS\n" +
                                "^FT396,2737^A0B,124,122^FH\\^FD{5}^FS\n" +
                                "^FT518,2737^A0B,124,122^FH\\^FDD/A{3}^FS\n" +
                                "^FT276,2737^A0B,124,122^FH\\^FDGUDANG VAKSIN^FS\n" +
                                "^PQ1,0,1,Y^XZ\n" +
                                "^XA^ID000.GRF^FS^XZ\n" +
                                "^XA^ID001.GRF^FS^XZ\n" +
                                "^XA^ID002.GRF^FS^XZ\n" +
                                "^XA^ID003.GRF^FS^XZ\n" +
                                "^XA^ID004.GRF^FS^XZ\n" +
                                "^XA^ID005.GRF^FS^XZ\n" +
                                "^XA^ID006.GRF^FS^XZ";

            string zplstring = string.Format(kirimSent, masterbox.ContactName, masterbox.ContactPhone, address[0], masterbox.Postcode, address[1].Replace(" ", ""), masterbox.CustomerName);
            return zplstring;
        }

        string dataKirimPrinter(string batch, string expiry, string gtin, string serial, string weight, string qtytotal, string productname, string colie, string shippingOrderNumber, string createdtime, string batchnumber1, string batchnumber2, string batchnumber3, string qty1, string qty2, string qty3, string expdate1, string expdate2, string expdate3, string colienumber, string colieMax)
        {

            createdtime = db.getUnixString(double.Parse(createdtime));
            dapatGSoneMasterbox = "01" + gtin + "13" + createdtime.Replace("/", "") + "400" + shippingOrderNumber.Replace("-", "") + "21" + serial;

            string kirimBetul = "CT~~CD,~CC^~CT~\n" +
                              "~DG000.GRF,04608,016,\n" +
                              ",:::::::::::::::::::::::::::::::::L01E,L0HFE0,K01FHFC,K03FIFC0,K07FJFC,K07FKF,K0FE1FIF80,K0F801FHFC0,K0F8007FFC0,K0F8007FFE0,K0F8007C3F0,K0F8007C038,K0FC007C0,:K07E007C0,:K07F007C0,K03FC0FC0,K01FIF80,L0JF80,L07FHF,L03FFE,L01FFC,M03F0,,K0E,K0FC0,K0HFC,K0IFD0,K0JF8,K0KF80,L0JFC0,L01FHFC7,M01FFCF80,N01FCFC0,O01C780,Q07C0,L03F0H0380,L0HFC0,K03FHF0,K07FHF8,K07FHFC,K0HF7FE,K0FC0FF,K0FC07F,K0F803F80,K0F801F80,K0F800F80,K0F800FC0,:K0FC007C0,K07E007C0,K07F007C0,K07F80FC0,K03FC1FC0,K01FIF80,L0JF,L07FHF,L03FFE,H0780H0HFC,H03F8001E0,H01FF8,H01FHF,I0JF0,I07FIF,I03FIFC0,J07FIF0,K0JF8,J01FIFC,K0F8FFE,K0781FF,K07807F,K03803F,K01801F80,K01800FC0,L0800FC0,O07C0,:::O0FC0,N01F80,:O0F80,L01007,L03F06,L0HFC0,K03FHF0,K07FHF8,K07FHFC,K0HF7FE,K0FC0FE,J01FC07F,K0F803F80,K0F801F80,K0F800F80,J01F800FC0,K0F800FC0,K0F8007C0,:J01F8007C0,K0FC00FC0,K0HFC1FC0,K0KF80,:K0KF,K01FHFE,L01FF8,M01E0,,K0F,K0FE0,K0HFE,K0IFC0,K0JF0,K07FHF8,L07FFC,M0HFE,M01FF,N07F,N03F80,N01F80,N01FC0,O0FC0,N017C0,O07C0,::O0FC0,:N01FC0,K0F0H0F80,J01FF007,K0HFE06,K0IFC0,K0JF0,K07FHF8,L07FFC,M07FE,M01FE,N07F,N03F80,N01F80,O0F80,O0FC0,:O07C0,:K0C0H07C0,K0FC00FC0,K0HFC1FC0,K0KF80,K0KF,:K01FHFE0M040,L01FFE,L0H1HFN020,N0HFN020,N03F80L030,N01F80L030,N01F80L010,O0FC0L018,O0FC0L0180L08,O07C0L0180K010,O07C0L0140K030,K080H07C0L01C010I040,K0F800FC0L01E0220H080,K0HFH0FC0L01F0E2003,J01FJFC0L01FFC3007,K0KF80L01FFC300E,K0KFL0303FF8303C,K07FHFE0L0JF03CF8,L07FFC0K053FHF03FF0,M07F80K040FFE03FF0,V0207FC03FF0,V0300F803FE0,V030J03FE0,V0380I01FF0,L07F0M0380I01FF0,K01FFE0H078007C0J0HFC,K03FHFJ07F1FC0J01FFD0,K07FHF80H01FHFC,K07FHFC0I07FFE,K0FE3FE0J0HFE0M040,K0FC07F0J07FE0L01C0,K0F807F0J03FE0L03,K0F801F80I01FC0L0F,K0F801F80I01F80K01E,J01F800FC0I01F0L07E,K0F800FC0I01C0L0FE,K0F8007C0I0380K01FE,K0F8007C0I060L01FF,J01F8007C0I0C0L01FFC0,K0F8007C0Q01FFE0,K0FC00FC0Q01FHFC,K0HFC1F80H03FFC0K0JF80,J01FF7FF80I01FF80J0FC01F0,K0KF80J07FC0J0F0I0C,K0KFL03FE0J070,K01FHFE0K03FE0J030,L01FFC0K03FF0038030,M03E0L03FF00FF010,V03FF01FFC10,V07FF03FHF08,V07FF03FHFD0,V0F0707FF060,U01C030FFE0,U038030FFE0,U070011C3C0,U04001103C0,U08001201C0,T010H010H0C0,T020H010H0D0,gH060,:gH020,::gH010,::,::::::::::::::::::::::::::::::::~DG001.GRF,02304,008,\n" +
                              ",:0F,::0F0FLF,::::0F0517F515,0F0H03F8,0F0H01FC,0F0I0FE,0F0I07F,0F0I07F80,0F0H01FFC0,0F0H03FFE0,0F0017FHF0,0F003FF3F8,0F007FC1FC,0F03FF80FE,0F07FF007F,0F0FFC003F,0F0FF0H01F,0F0FE0I0F,0F0F40I07,0F0F0J03,0F0C0J01,0F08,0F,:0F005FC0,0F00FHF8,0F01FHFC,0F03FHFE,0F07FIF,0F07E03F,0F07C01F,0F0F800F80,0F0F0H0780,:::0F070H07,0F07800F,0F07D01F,0F03FBFE,0F03FHFE,0F01FHFC,0F017FF0,0F003FE0,0F0H05,0F,::0F005FD0,0F00FHF8,0F01FHFC,0F03FHFE,0F07FIF,0F07E03F80,0F07C01F,0F0F0H0780,::0F070H07,:0F07C00F,0F03C01E,0F05F77F77,0F0FLF,:::0F02L2,0F,::0F0H02,0F005FD0,0F00FHF8,0F01FHFC,0F03FHFE,0F07F77F,0F07C71F,0F070707,0F0F070780,::0F0F0707,0F0F070F,0F07C71F,0F07C7FF,:0F0387FE,0F0187FC,0F0087E0,0F0H01,0F,::::::::::::::0F07L7,0F0FLF,:::0F0223E22F,0F0H01E00F,:::::0F0H01F01F,0F0H01F00F,0F0H01F01F,0F0I0FCBE,0F0H01FIF,0F0I07FFE,0F0I07FFC,0F0I03FF8,0F0I017D0,0F,::0F0015,0F003FE0,0F01FHFC,0F03FHFC,0F07FIF,:0F07C71F,0F0F870F80,0F07070780,0F0F070780,::0F070717,0F07871F,0F07C7FF,0F0787FE,0F0387FC,0F0187F8,0F0H07D0,0F,::0F0J0780,:0F015H57D0,0F03FJF8,0F07FJFC,0F0FKFE,0F0FLF,0F0FBABFBA,0F0F0H0780,:0F070H0780,0F02,0F,:0F001I1,0F00FIF80,0F01FIF80,0F03FIF80,0F07FIF80,0F0FJF80,0F0FD404,0F0F80,0F0F,:0F07,:0F0780,0F03E0,0F05FFDD80,0F0FJF80,:::0F,::::0F405750,0FE0FHF8,0FC1FHFC,0FC3FHFE,0FC7FF7F,0FC7E03F,0FCFC01F,0F0F0H0F80,0F0F0H0780,:0F070H07,0F078007,0F07801F,0F83E03E,0FDDF57D,0FLF80,::0F7FJF80,0F,::::0F01F010,0F03FC1C,0F07FC1C,0F0FFE3F,0F07DF3F,0F0F8E0F,0F0F0707,0F0F070780,0F07070780,0F07030780,0F07870780,0F03C38F80,0F01F7FF,0F0FJF,:0F0FIFE,0F0FIF4,0F0E,0F,:::0F01C07C,0F03C1FE,0F07C1FF,0F07E3FF,0F07C7FF,0F0F87C780,0F0707C780,0F0F078780,0F0F07C780,0F0F0F8780,0F0F1F87,0F0F0F8F80,0F07DF1F,0F07FF0F,0F07FF1F,0F03FE0E,0F01FC0C,0F00F8,0F0010,0F,,:::::::::::::::::::::::~DG002.GRF,02688,012,\n" +
                              ",:L0F0,::L0F0FLF0,::::L0F0F157D1F0,L0F0F00780F0,::::::::L0F07007C1F0,L0F0F80FEBE0,L0F07F7FIF0,L0F07FFEFFE0,L0F07FFC7FC0,L0F03FFC3F80,L0F017F015,L0F003E0,L0F0,::L0F0H020,L0F005FD,L0F00FHF80,L0F01FHFC0,L0F03FHFE0,L0F07F77F0,L0F07C71F0,L0F0707070,L0F0F07078,::L0F0F07070,L0F0F070F0,L0F07C71F0,L0F07C7FF0,:L0F0387FE0,L0F0187FC0,L0F0087E,L0F0H010,L0F0,::L0F0551550,L0F0FJF8,:::L0F0FIFE0,L0F0I05F0,L0F0J0F0,:L0F0J0F8,::L0F0J070,L0F0,L0F004,L0F03F80,L0F07FC1C0,L0F07FE1E0,L0F07FE1F0,L0F0FFE3F0,L0F0F071F0,L0F0F07078,::L0F0707078,L0F0383078,L0F03C39F0,L0F03FIF8,L0F07FIF0,L0F0FJF0,L0F0FIFE0,L0F0FIFC0,L0F0D1H1,L0F0,::L0F0J078,:L0F015H57D,L0F03FJF80,L0F07FJFC0,L0F0FKFE0,:L0F0FAHAFAA0,L0F0F0H078,:L0F070H078,L0F0,::::::::::::::L0F0D5H5D550,L0F0FLF0,:::L0F0H07F,L0F0H03F,L0F0H01F80,L0F0I0FC0,L0F0I07F0,:L0F0H01FFC,L0F0H07FHF,L0F0H0JF,L0F003FF3FC0,L0F00FFE1FE0,L0F01FFC0FF0,L0F0FFE007F0,L0F0FFC003F0,L0F0FF0H01F0,L0F0FC0I070,L0F0F80I070,L0F0F0J010,L0F0C0J010,L0F0,:L0F0H010,L0F003FE,L0F007FF,L0F01FHFC0,L0F03FHFD0,L0F03FHFE0,L0F07D05F0,L0F07800F0,L0F070H070,L0F0F0H078,::::L0F07C01F0,:L0F07F57F0,L0F03FHFE0,L0F01FHFC0,L0F00FHF80,L0F005FF,L0F0H020,L0F0,:L0F0J078,:L0F015H57D,L0F03FJF80,L0F07FJFC0,L0F0FKFE0,L0F0FLF0,L0F0FAHAFAA0,L0F0F0H078,:L0F070H078,L0F0,::L0F005FC,L0F00FHF80,L0F01FHFC0,L0F03FHFE0,L0F07FIF0,L0F07E03F0,L0F07C01F0,L0F0F800F8,L0F0F0H078,:::L0F070H070,L0F07800F0,L0F07D01F0,L0F03FBFE0,L0F03FHFE0,L0F01FHFC0,L0F017FF,L0F003FE,L0F0H054,L0F0,::L0F05J50,L0F0FJF8,:::L0F0FIFE0,L0F01015F0,L0F0J0F0,:L0F0J0F8,::L0F0J070,,::::::~DG003.GRF,03456,012,\n" +
                              ",:::::P01FF4,P07FFE,O01FIFC0,O03FIFE0,O07FF7FF0,O07F00FF8,O07C005FC,O0F80H0FC,O070I07C,O0F0I03E,O0F0I01F,O0F0J0E,O0F0J0F,:O070J0F,O0780I0F,O07C0H01F,O03E0H01F,O01F4001F,O01FE003E,O01FF55FC,P07FIFC,:Q0JF0,Q07FFD0,R0BF80,M05,M0FE0,M0IF,M0JF8,M0KF50,M03FJFE,N017FIF80,O01FIF80,O01F7FF80,O03C07F80,O07001F,O0F0H0E,O0F0H07,::O0F800780,O07C00780,O07E00F80,O07FD7F,O03FIF,O01FIF,P07FFE,P017F4,Q02A0,,:P015,P03F80,O01FHF0,O03FHF8,O07FHFC,O07FHFE,O07C77F,O0F871F,O0F070F,O0F070780,::O07070780,O07870F80,O07C71F,O0387FF,O0187FF,O0187FE,Q07FC,Q03E0,,:O0D40,O0HF8,O0IF50,O0JFE,O0KF80,P0JF80,P017FF80,R0HF80,R07D,R01C,R01F,S0F,:S0F80,P0100780,O01F00380,O07FC01,O07FE,O07FF1C,O0HFE1E,O0F071F,:O07071F,O07030780,O07070780,O07830780,O03C30780,O0FE38780,O0IF8780,O0KF80,O0KF,:O045FHF,Q03FC,Q0H10,,::O015005,O07F80780,O07FFD780,O0KF80,O0KFC0,O0KFE0,O0F17FHF0,O0F003FF8,O070017FC,O010H07BC,S0780,S0380,P01501,P07F80,O01FHF0,O03FHF8,O07FHFC,O07FBFE,O07C07F,O0F801F,O0F001F,O0F0H0780,::O070H0780,O07800780,O07C007,O03F81F,O03FF7F,O01FHFE,P0IFC,P03FF8,P017F0,,::O0F,O0HF8,O0IF40,O0JFE,O07FIF80,P0JF80,P017FF80,R0HF80,R075,R03C,R01F,S0F,:S0F80,S0780,S0380,S01,,::::::::::P017F4,P07FFE,O01FIFC0,O03FIFE0,O07FJF0,O07F83FF8,O07C005FC,O0F80H0FC,O070I07C,O0F0I03E,O0F0I01F,O0F0J0E,O0F0J0F,:O0780I0F,:O07F0H01F,O03F8001E,O01F0H05E,O01F800FE,O017001FC,P03001F8,P01001F0,T0E0,S01,,P014,P07F80,O01FHF0,O03FHF8,O07FHFC,O07FHFE,O07D17F,O0F803F,O0F001F,O0F0H0F80,O0F0H0780,:O070H0780,O07800780,O07D007,O03F81F,O03FIF,O01FHFE,O017FFD,P03FF8,P017F0,,::P014,P0HF80,O01FHF0,O03FHF8,O07FHFC,O0JFE,O0FD17F,O0F803F,O0F0H0F,O0F0H0780,O070H0780,:O078007,O0BC00F80,O0HF01F,O0HFE3E,O0JFC,O0JFE,O07FJF0,P03FJF,Q05FIF,R03FHF,R0H17F,T03F,Q040H01,P03F80,O01FHF0,O03FHF8,O07FHFC,O07FHFE,O07D77F,O0F871F,O0F070F,O0F070780,::O07070780,O07870F80,O07C71F,O0387FF,O01C7FF,O0187FE,O0107FC,Q03A0,,:::::::::::~DG004.GRF,02048,008,\n" +
                              ",:I01E002,I01E00E,I01E03E,I01E07E,I01E0FE,I01E0FF,I01E0FA,I01E1F0,I01E1E0,:::I01E0E0,I01E0FC,I01E0FKFE0,I01E07FJFE0,I01E03FJFE0,I01E01FJFE0,I01E00AFHFBE0,I01E0,::::I01E002I20,I01E01FIF0,I01E03FIF0,I01E0FJF0,:I01E1FJF0,I01E1FA08,I01E1F0,I01E1E0,:I01E0E0,:I01E0F0,I01E07C,I01E0BFFBB0,I01E1FJF0,:::I01E0,::::I01E0AJA0,I01E1FJF0,::I01E1FIFE0,I01E17H7FC0,I01E0I03C0,I01E0I01E0,I01E0J0E0,I01E0J0F0,::I01E02AABF0,I01E1FJF0,I01E1FIFE0,:I01E1FIFA0,I01E1FIF80,I01E0I03C0,I01E0I01C0,I01E0J0E0,I01E0J0F0,::I01E02H23F0,I01E1FJF0,I01E1FIFE0,::I01E1FIF,I01E00808,I01E0,::::I01E0ALA0,I01E1FKFE0,:::I01E04L40,I01E0,::I01E004,I01E03E02,I01E07F8380,I01E0FF83E0,I01E0FFC7E0,I01E1FBC7E0,I01E1F1C1F0,I01E1E0E0E0,I01E1E0E0F0,I01E0E0E0F0,I01E0E060F0,:I01E07871F0,I01E03FHFE0,I01E1FIFE0,:I01E1FIFC0,I01E1FIF80,I01E1C0,I01E0,::::I01E1FKFE0,::::I01E0AHABAHA0,I01E0I01C0,I01E0J0E0,::I01E0J0F0,:I01E0I01F0,I01E1FBBFE0,I01E1FIFE0,:I01E1FIFC0,I01E1FIF80,I01E0,:::::::::::::::I01E0K01E0,::::::I01E08J89E0,I01E1FKFE0,::::I01E02J23E0,I01E0K01E0,::::::I01E0L020,I01E0,I01E00BF8,I01E01FHF,I01E03FHF80,I01E07FHFC0,I01E0FIFE0,I01E0FC07E0,I01E0F803E0,I01E1F001F0,I01E1E0H0F0,:::I01E0E0H0E0,I01E0F001E0,I01E0FA03E0,I01E07F7FC0,I01E07FHFC0,I01E03FHF80,I01E02FFE,I01E007FC,I01E0H0A0,I01E0,I01E0J020,I01E0J0F0,:I01E015H5F5,I01E0FKF80,::I01E1FKFC0,I01E1EAHAFAA0,I01E1E0H0F0,I01E0E0H0F0,I01E1E0H0F0,I01E0A0H0A0,I01E0,I01E008,I01E07F,I01E0FF8380,I01E0FFC3C0,I01E0FFC3E0,I01E1FFC7E0,I01E1E0E3E0,I01E1E0E0F0,::I01E0E0E0F0,I01E07060F0,I01E07873E0,I01E07FIF0,I01E0FIFE0,I01E1FIFE0,I01E1FIFC0,I01E1FIF80,I01E1A2H2,I01E0,::::I01E0ALA0,I01E1FKFE0,:::I01E0,::,:::::::::::~DG005.GRF,02304,008,\n" +
                              ",::::N020,L017FD,L03FHF80,L07FIF0,L0KF8,K01FD5FFC,K01F800FE,K01F0H07F,K03E0H01F,K01C0I0F,K03C0I0F80,K03C0I07C0,K03C0I0380,K03C05007C0,K03C07803C0,K01C07803C0,K03E07803C0,K01E07807C0,K01E07803C0,K01F47807C0,L0IF80F80,L0IF85F80,L0IF87F80,L07FF87F,M0HF83E,M04587C,P020,,:K03C0,K03FE,K03FFD0,K03FIF80,K01FIFE0,L03FHFE0,M05FFE0,N03FE0,N01D40,O0F,O07C0,O03C0,:O03E0,O01E0,P0E0,L05FD040,L07FFC,L0JF,:K01FDFFC0,K01E01FC0,K01C007C0,K03C003E0,K03C001C0,K03C001E0,:K03E001E0,K01F001C0,K01F803E0,K01FD07C0,L0JFC0,L07FHFC0,L03FHF80,L01FHF,M03F8,N040,,L010,L030,L070,L0F81C,K01F07F,K01F07F80,K01C0FFC0,K03C0FFC0,K03C1F1C0,K03C1F1E0,:K03C3E1E0,K01C3E1E0,K03E3E1E0,K01FFC1C0,L0HFC3E0,K01FFC7C0,L07F83C0,L05F07C0,M080380,O01,,L010,L070,:L0F83E,K01F87F,K01F8FF80,K01E0FFC0,K03C0FFC0,K03C1F5C0,K03C1F1E0,:K03C3E1E0,K01C7E1E0,K01E7C1E0,K01F7C1E0,K01FFC3E0,L0HFC7C0,L0HF87C0,L07F07C0,O0380,O05,,::::::::::::::P04040,O0AFFC0,K03FKFC0,:::K03FF5H50,K03FF,K017FC0,L03FF0,L017FC,M03FF,N07FC0,N03FF0,N017FC,O03FF,O017FC0,P03FC0,K015H57FFC0,K03FKFC0,::K03FF5I540,K03FE,K01FFC0,L07FE0,L01FF4,M07FE,M01FFC0,N07FE0,N01FFC,O03FF,O01FFC0,P07FC0,P01FC0,Q03C0,L01FD001C0,L03FFC,L07FHF,L0JF,K01FDFFC0,K01E1CFC0,K01C1C7C0,K03C1C3E0,K03C1C1C0,K03C1C1E0,:K03E1C1E0,K01E1C1C0,K01F1C3C0,K01F1FFC0,L0E1FF80,L061FF,M01FE,M0154,,:K03C0,K03F5,K03FFE0,K03FHFD40,K03FIFE0,K017FHFE340,M03FFE3C0,N05FE3C0,P0E3C0,Q03C0,J030L0C0,J0F00440,J0E03FE0,I01F07FFC,I01E0FHFE,I01F1FIF,I03C3FEFF80,I03C3F01FC0,I03C3E007C0,I03C1C005C0,I03C3C001E0,I01C1C001E0,I03E1E001E0,I01F5E001C0,I01FEF003C0,I01FHFC07C0,J0JFAF80,J07FJF,K0KFC0,K05FIFE0,M0IFE0,M015FE0,O0BE0,P040,,K03C0,K03FC,K03FFD0,K03FHFE,K01FIFD4,L0LF80,M05FIFC0,N03FHFC0,O0F7FC0,O078FC0,O038140,O0180,O01C0,K038001E0,K03F501E0,K03FFE3E0,K03FIFE0,:K017FHFC0,M03FFC0,N05F40,,:::L0C00180,K01FD01E0,K01FFE1E0,K01FHFDE0,K03FJF8,K03FJFC,K03C3FHFC,K03C057FC,K03C001FE,L04001F7,O01E0,O0160,,::::::::::::::::::::::::::::~DG006.GRF,02304,012,\n" +
                              ",:M03C0,::M03C3FKFC0,::::M03C04H45FFC0,M03C0I01FF,M03C0I07FC,M03C0I0HF8,M03C0H07FC0,M03C0H0HFC0,M03C001FF,M03C007FC,M03C05FF0,M03C07FE0,M03C1FFC0,M03C3FF,M03C3FFDIDC0,M03C3FKFC0,::M03C1DKDC0,M03C0,::::M03C017F0,M03C03FFE,M03C07FHF,M03C0FIF80,M03C1FIFC0,M03C1F80FC0,M03C1F007C0,M03C3E003E0,M03C3C001E0,:::M03C1C001C0,M03C1E003C0,M03C1F407C0,M03C0FEFF80,M03C0FIF80,M03C07FHF,M03C05FFC,M03C00FF8,M03C00140,M03C0,:::M03C3E0,::::M03C040,M03C0,:::::::::::::::M03C3FKFC0,::::M03C3D55F57C0,M03C3C01E03C0,::::::::M03C1C01F07C0,M03C3E03FCF80,M03C1F57FHF80,M03C1FHFBFF80,M03C1FHF1FF,M03C0FFE0FE,M03C07FC054,M03C00F80,M03C004,M03C0,::M03C07C04,M03C0FF07,M03C1FF07,M03C3FF8FC0,M03C1F7CFC0,M03C3E383C0,M03C3C1C1C0,M03C3C1C1E0,M03C1C1C1E0,M03C1C0C1E0,M03C1E1C1E0,M03C0F0E3E0,M03C07DFFC0,M03C3FIFC0,:M03C3FIF80,M03C3FHFD,M03C380,M03C0,:M03C0J040,M03C0I01E0,:M03C02AABEA,M03C1FKF,::M03C3FKF80,M03C3D5H5F540,M03C3C001E0,M03C1C001E0,M03C3C001E0,M03C1400140,M03C0,M03C001,M03C00FF8,M03C05FFC,M03C07FHF,M03C07FHF80,M03C0FIF80,M03C1F447C0,M03C1E003C0,M03C1C001C0,M03C3C001E0,:::M03C3E003E0,M03C1F40FC0,M03C1FC0FC0,M03C1FC07C0,M03C0F80780,M03C05C04,M03C008,M03C0,:::M03C35H575540,M03C3FKFC0,:::M03C0I0F80,M03C0I07C0,M03C0I01C0,:M03C0I01E0,::M03C0I07E0,M03C3FIFE0,M03C3FIFC0,:M03C3FIF80,M03C3FHFE,M03C04I4,M03C0,:,::::::~DG007.GRF,02304,008,\n" +
                              ",::::::O01C0,O01E0,:J0A0I01E0,I01FC0H01E0,I01FFA801E0,I01FHFD01E0,I01FIFEBE0,I01FKFE0,J02FJFE0,L07FHFE0,L02BFFE0,N07FE0,O03E0,O01E0,::::::K0280H0E0,K0HF0,J03FFE,J07FHF,J0JF80,J0JFC0,J0F82FE0,I01F007E0,I01E003E0,I01E001F0,I01E0H0F0,:J0E0H0F0,J0F0H0F0,J0FA00E0,J07F03E0,J07FHFE0,J03FHFC0,J02FHF80,K07FF,K02FE,,:::J050H0C0,J0FEA0F0,J0IF4F0,J0KF0,I01FJFC,I01EFIFE,I01E07FFE,I01E00BFF,I01E0H0HF,J020H0F280,N0F0,K0800B0,J01F,J0HF80,J0HFC0,J0HFC380,I01FFC1C0,I01E2E3E0,I01E0E3E0,J0E0E3E0,I01E061F0,J0E0E0E0,J0F060F0,J0F860F0,I01FC70F0,I01FHF0E0,I01FJF0,I01FIFE0,:J0ABFFE0,L07F80,M0A,,J0A0,I01F0,I01FEA0,I01FHFC,I01FIFE0,I017FIFC,J02FJFA0,L07FHFE0,M0AFFE0,N07FE0,O0AE0,,:::::::::::L040,K0HFE,J01FHFC0,J03FHFE8,J07FIF8,J0HFEFFE,I01FC07FF,I01F800FF80,I01E0H01F80,I03E0I0F80,I03C0I07C0,I03C0I03C0,I03C40H01C0,I03CE0H03E0,I03FE0H01E0,:I01FC0H01E0,I03F80H03E0,I03FC0H03E0,I0HFE0H03E0,I0IF800FC0,I0FBFEABF80,I0E1FJF80,I060FJF80,K03FHFE,K02FHFA,L01FE0,N080,,J020,J07F,J0HFE8,I01FIF,J0JFE0,I01FJF0,I01EAFHF0,I01E01FF0,J0E002F0,J0E0,:J070,J0B8,I01FE,I01FFA8,I01FIF,I01FIFE0,J07FIF0,K0BFHF0,L01FF0,M02F0,N010,,:J02E,J07F80,J0HF82,I01FFC1,J0FBE380,I01F1E3C0,I01E0E3E0,I01E0E1E0,J0E0E0E0,J0E060F0,J0E0E0F0,J07060F0,J0FE60F0,I01FHF1F0,I01FHFBE0,I01FIFE0,:K07FFC0,L0BF80,,::I01A8,I01FF,I01FFEA,I01FIFC0,I01FJF0,J01FIF0,K02FHF0,L01FF0,M0FA0,M0380,M01E0,N0E0,:I01C0H0F0,I01FE00F0,I01FFC1F0,I01FJF0,:J0AFHFE0,K05FFE0,L0AF80,,:::J010H0C0,J0FEA0F0,J0IF4F0,J0KF0,I01FJFC,I01EFIFE,I01E07FFE,I01E00BFF,I01E0H0HF,J020H0F280,N0F0,N0B0,I01C0,I01FE80,I01FHF0,I01FHFE80,I01FJF0,J0AFIF1A0,K01FHF1E0,L02FF1E0,M0171E0,N020E0,P060,,J010H0C0,J0FEA0F0,J0IF4F0,J0KF0,I01FJFC,I01EFIFE,I01E07FFE,I01E00BFF,I01E0H0HF,J020H0F280,N0F0,H0E0J0B0,01E0,01E0J020,01F0I07F0,01F80ABFF0,01FC7FIF0,H0MF0,H07FKF0,H03FHFHA,I0HF0,I03FE,I01FF,J0BFE0,J01FF0,K03FE,K01FF80,L0HFE0,L01FF0,M0HF0,M03F0,N0F0,N010,,:::::::::::::::::::::~DG008.GRF,00768,012,\n" +
                              ",::::::::::::::P028,P070H01C,P0F8003E,O01F8001F,O03F8003F,O03E0H01F,O0380I0F80,O07801C0780,O07801E0780,:O03801F0F80,O03C03F9F,O03EAFIF,O03FKF,O01FHFBFE,P0IF1FC,P03FE0A8,P01F80,Q08,,:::::::::::::::::::::::::::::~DG009.GRF,00512,008,\n" +
                              ",:::::::::::::H010,H01E0I0A0,H01F8001F0,H01FE0H0F8,H01FE001FC,H01FF801FC,H01FFC007C,H01FFE003E,H01F7E001E,H01F3F801E,H01F1F801E,H01F0FE03E,H01F07E03E,H01F03F8BE,H01F01FHFC,H01F00FHF8,H01F007FF0,H01F003FE0,H01F0H07C0,I020H02,,:::::::::::::::::::::::::::::~DG010.GRF,00512,008,\n" +
                              ",:::::::::::::::O03C0,O03E0,O01E0,P0E0,P0F0,P0F8,P07C,K02AIAFE,K07FKF80,:::K0M5,,::::::::::::::::::::::::::::::::::~DG011.GRF,01920,012,\n" +
                              ",:K0780,::K0787FKF80,::::K07808H8BFF80,K0780I03FE,K0780I0HF8,K0780H01FF0,K0780H0HF80,K078001FF80,K078003FE,K07800FF8,K0780BFE0,K0780FFC0,K0783FF80,K0787FE,K0787FFBIB80,K0787FKF80,::K0783BKB80,K0780,::::K07802FE0,K07807FFC,K0780FHFE,K0781FIF,K0783FIF80,K0783F01F80,K0783E00F80,K0787C007C0,K07878003C0,:::K0783800380,K0783C00780,K0783E80F80,K0781FDFF,K0781FIF,K0780FHFE,K0780BFF8,K07801FF0,K07800280,K0780,:::K0787C0,::::K078080,K0780,:::::::::::::::K0787FKF80,::::K0782ABFAHA80,K078001FC,K0780H0FE,K0780H07F,K0780H03F80,K0780H03FC0,K0780H0HFE0,K078001FHF0,K07800FIF8,K07801FFDFC,K0780BFE8FE,K0781FFC07F80,K0783FF803F80,K0787FE001F80,K0787F80H0F80,K0787F0I0780,K0787E0I0380,K07870J0180,K07860K080,K0780,::K07800BE8,K07807FF8,K0780FHFE,K0781FIF,K0783FBBF80,K0783F01F80,K0783E00F80,K0787C007C0,K0783800380,K07878003C0,::K0783800380,K0783C00780,K0783E02F80,K0781FD7F,K0781FIF80,K0780FHFE,K07803FFA,K07801FF0,K07800A80,K0780,::K078080H080,K0785D5D5D580,K0787FKF80,:::K0782AKA80,K0780,::::K0782AIA8280,K0787FIFC780,:::K0781J101,K0780,:,:::::::::~DG012.GRF,02304,012,\n" +
                              ",:::M0340,M03F8,M03FFD0,M03FHFE,M03FIFD0,M03FJFE,M03D7FIFC0,M03C0FIFC0,M03C03FHFC0,M03C03C3FC0,M03C03C07C0,M03C03C03C0,::::M01C03C03C0,M03E03E03C0,M01F47C07C0,M01FIF07C0,M01FHF7FFC0,N0IF7FF80,N07FE7FF,N03FC3FF,O0501FC,,::N07C,N0HF,M01FF04,M01FF86,M03F7C7,M03E3C380,M03C1C7C0,M03C1C3C0,M01C1C1C0,M01C0C1E0,:N0E0C1E0,M01F5C1E0,M03FFE3E0,M03FIFC0,::O0IF80,P05F,,:::N0C00180,M01FD01E0,M01FFE1E0,M01FHFDE0,M03FJF8,M03FJFC,M03C3FHFC,M03C057FC,M03C001FE,N04001F7,Q01E0,Q0160,,N01FD0,N07FF8,N0IFC,M01FIF,M01FDFF,M03E03F80,M03C007C0,M03C003C0,M03C001C0,M03C001E0,M01C001E0,M01E001E0,M01F001E0,N0F803E0,N0700FC0,N0780FC0,N01007C0,Q07,Q04,,:M0380,M03FD40,M03FFE0,M03FHFD,M03FJF0,N05FJF40,O03FIFC0,P05FHFC0,Q0IFC0,Q0757C0,Q0380C0,Q01C0,:M034001C0,M03FC01E0,M03FF57E0,M03FIFE0,M03FIFC0,N0JFC0,O05FFC0,P03F,Q04,,::::::::::::M0340,M03F8,M03FFD0,M03FHFE,M03FIFD0,N0KFE,N045FIFC0,Q0IFC0,Q017FC0,R07FC0,Q05FFC0,Q0IF80,P07FFC,O03FFE0,N01FHF40,N07FFC,M01FHF0,M03FF80,M03FC,M03FFC0,M03FHF5,M03FIFE0,N05FJF40,O02FIFC0,P05FHFC0,R0HFC0,R0H5C0,T0C0,O05,N01FE0,N07FFC,N0IFE,M01FIF,M01FIF80,M01F45FC0,M03E00FC0,M03C007C0,M03C003E0,M03C001E0,:M01C001E0,M01E001E0,M01F401C0,N0FE07C0,N0JFC0,N07FHF80,N05FHF40,O0HFE,O05F4,,::::::::::::::::::~DG013.GRF,04224,012,\n" +
                              ",::::::Q02,Q0E,P03E,P07E,P0FE,P0HF,P0FA,O01F0,O01E0,:::P0E0,P0FC,P0LFE0,P07FJFE0,P03FJFE0,P01FJFE0,Q0AFHFBE0,,::::Q0J20,P01FIF0,P03FIF0,P0KF0,:O01FJF0,O01FA08,O01F0,O01E0,:P0E0,:P0F0,P07C,P0BFFBB0,O01FJF0,:::,::::P0KA0,O01FJF0,::O01FIFE0,O017H7FC0,S03C0,S01E0,T0E0,T0F0,::P02AABF0,O01FJF0,O01FIFE0,:O01FIFA0,O01FIF80,S03C0,S01C0,T0E0,T0F0,::P0I23F0,O01FJF0,O01FIFE0,::O01FIF,Q0808,,::::P0MA0,O01FKFE0,:::P0M40,,::Q04,P03E02,P07F8380,P0HF83E0,P0HFC7E0,O01FBC7E0,O01F1C1F0,O01E0E0E0,O01E0E0F0,P0E0E0F0,P0E060F0,:P07871F0,P03FHFE0,O01FIFE0,:O01FIFC0,O01FIF80,O01C0,,::::O01FKFE0,::::P0IABAHA0,S01C0,T0E0,::T0F0,:S01F0,O01FBBFE0,O01FIFE0,:O01FIFC0,O01FIF80,,:::::::::::::O01,O01E8,O01FF,O01FFEA,P07FHF,Q0BFHF8,R07FHF40,S02FFE0,T07FE0,U0BE0,V020,:,::::::::::R0H2,R07FC0,Q03FHF8,Q0JFC,P03FIFE,P03FJF,P07FAAFF80,P07C001F80,P0F80H0FC0,P0F0I07C0,P0E0I03E0,O01E0I01C0,O01E0I01E0,O01E40H01E0,O01EE0H01E0,O01EF0H01E0,P0HEI03E0,P0FC0H03C0,P0FC0H03E0,P0FC0H07C0,P0FE002F80,P0HFC07F80,O01FKF80,O01FKF,O03EFIFE,O07C7FHF0,O0380FFA0,O0180,,:::P02FIF0,P07FIF0,P0KF0,:P0HFEFE0,O01F0,O01E0,:P0E0,::P070,P03A2020,O017FIF0,O01FJF0,:::Q0808,,::Q08,P07F,P0HF8380,P0HFC3C0,P0HFC3E0,O01FFC7E0,O01E0E3E0,O01E0E0F0,::P0E0E0F0,P07060F0,P07873E0,P07FIF0,P0JFE0,O01FIFE0,O01FIFC0,O01FIF80,O01A2H2,,::::P0KA0,O01FJF0,:::R01780,S03E0,T0E0,:T0F0,::S03F0,O01FJF0,O01FIFE0,:O01FIFC0,O01FIF,P0J2,,::T0F0,:P02AHAFA,P07FJF,P0LF80,O01FKFC0,:O01F5H5F540,O01E0H0F0,:P0E0H0F0,,:::O015J5140,O01FJF1E0,:::P0KA0A0,,::T0F0,:P02AHAFA,P07FJF,P0LF80,O01FKFC0,:O01F5H5F540,O01E0H0F0,:P0E0H0F0,,T020,T070,N0E0I0BF0,M01E0H01FF0,M01E0H0IF0,M01E007FHF0,M01F82FHF80,M01FC7FFC,N0JFE0,N07FHF,N03FF8,N01FHF,O03FFE0,P07FFC,P02FHFA0,Q01FHF0,R0BFF0,S07F0,S02F0,T030,T020,,:::::::::::::::~DG014.GRF,02816,008,\n" +
                              ",::::::K01,K07,J01F,J03F,J07F,J07F80,J07D,J0F8,J0F0,:::J070,J07E,J07FKF0,J03FKF0,J01FKF0,K0LF0,K057FFDF0,,::::K0J10,K0JF8,J01FIF8,J07FIF8,:J0KF8,J0FD04,J0F8,J0F0,:J070,:J078,J03E,J05FFDD8,J0KF8,:::,::::J0K50,J0KF8,::J0KF0,J0IBFE0,M01E0,N0F0,N070,N078,::J015H5F8,J0KF8,J0KF0,:J0JFD0,J0JFC0,M01E0,N0E0,N070,N078,::J0J1F8,J0KF8,J0KF0,::J0JF80,K0404,,::::J0M50,J0MF0,:::J0M20,,::K02,J01F01,J03FC1C0,J07FC1F0,J07FE3F0,J0FDE3F0,J0F8E0F8,J0F07070,J0F07078,J0707078,J0703078,:J03C38F8,J01FIF0,J0KF0,:J0JFE0,J0JFC0,J0E0,,::::J0MF0,::::J0J5D550,N0E0,N070,::N078,:N0F8,J0FDDFF0,J0KF0,:J0JFE0,J0JFC0,,:::::::::::::J080,J0F4,J0HF80,J0IF5,J03FHF80,K05FHFC,L03FHFA0,M017FF0,N03FF0,O05F0,P010,:,::::::::::L0H1,L03FE0,K01FHFC,K07FHFE,J01FJF,J01FJF80,J03FD57FC0,J03E0H0FC0,J07C0H07E0,J0780H03E0,J070I01F0,J0F0J0E0,J0F0J0F0,J0F20I0F0,J0F70I0F0,J0F780H0F0,J0H7I01F0,J07E0H01E0,J07E0H01F0,J07E0H03E0,J07F0017C0,J07FE03FC0,J0LFC0,J0LF80,I01F7FIF,I03E3FHF8,I01C07FD0,J0C0,,:::J017FHF8,J03FIF8,J07FIF8,:J07FF7F0,J0F8,J0F0,:J070,::J038,J01D1010,J0BFIF8,J0KF8,:::K0404,,::K04,J03F80,J07FC1C0,J07FE1E0,J07FE1F0,J0HFE3F0,J0F071F0,J0F07078,::J0707078,J0383078,J03C39F0,J03FIF8,J07FIF0,J0KF0,J0JFE0,J0JFC0,J0D1H1,,::::J0K50,J0KF8,:::M0BC0,M01F0,N070,:N078,::M01F8,J0KF8,J0KF0,:J0JFE0,J0JF80,J0J1,,::N078,:J015H57D,J03FJF80,J07FJFC0,J0LFE0,:J0FAHAFAA0,J0F0H078,:J070H078,,:::J0KA8A0,J0KF8F0,:::J0K5050,,::N078,:J015H57D,J03FJF80,J07FJFC0,J0LFE0,:J0FAHAFAA0,J0F0H078,:J070H078,,N010,N038,H070I05F8,H0F0I0HF8,H0F0H07FF8,H0F003FHF8,H0FC17FFC0,H0FE3FFE,H07FIF0,H03FHF80,H01FFC,I0IF80,I01FHF0,J03FFE,J017FFD0,L0IF8,L05FF8,M03F8,M0178,N018,N010,,:::::::::::::::~DG015.GRF,01280,008,\n" +
                              ",::::::M01,M03E0,M0D5C,L01802,L03101,L023E080,L0475CC0,L0HC0C40,L04C0440,L0C40660,:L0C70C60,L04DFC40,L0HC3E60,L0H404C0,L0H6H080,L03701,L039AE,L01854,,::M0I540,M0IFE0,M0101,,:M0IA,M0H7C,O0C,O04,O06,M0106,M0HFE,M0H54,,:O06,M0H5740,M0IFE0,M010760,O0260,P040,M01F8,M075C,M060E,M0C04,M0C06,:M060E,M075C,M03F8,M0H10,,:::::::M0IFE0,M0I540,,::M0H54,M0HFE,M0H14,O06,::M0H5C,M0HF8,O04,:O06,:M0H5C,M0HF8,M01,,M0H54,M07FE,M0510,M0C,M04,M02,M0H76,M0HBA,,:M0H54,M0HFE,O04,O06,::M0D5C,M0FE8,,:M0H5H40,M0HFE60,M01,,M0614,M0E3C,M0476,M0C66,M0C46,M0HCE,M07CC,M0380,,:M07,M078C,M0C44,M0HC6,M0C46,M0I6,M0744,M0HFC,M0H50,,:M0238,M0474,M0C66,:M0CE6,M04C4,M078C,M0540,,:M0BAA20,M0HFE40,,:::::::::::::~DG016.GRF,01280,008,\n" +
                              ",::::::::::::::::::L01AHA80,L01FDFC0,,::L01554,M0EF8,N018,O08,O0C,:L01FF8,M0HA8,,:O0C,M0HAE80,L01FDFC0,O0HC0,O04C0,P080,M03F0,M0EB8,M0C1C,L01808,L0180C,M080C,M0C1C,M0EB8,M07F0,M02,,::::::M0202,L01FHFC0,M0IA80,,::M0HA8,L01FFC,M0H28,O0C,::M0AB8,L01FF0,O08,:O0C,:M0AB8,L01FF0,N020,,M0HA8,M0HFC,M0A,L018,M08,M04,M0HEC,L01774,,:M0HA8,L01FFC,O08,O0C,::M0AB8,L01FF0,M02,,M0HAH80,L01FFCC0,M0H20,,M0C28,M0C78,M08EC,L018CC,L0188C,L0199C,M0F98,M07,,:M0E,M0F18,L018H8,L0198C,M0H8C,M0IC,M0E88,L01FF8,M0HA8,,:M0470,M08E8,L018CC,:L019CC,M0B88,M0F18,M0A,,:L0177440,L01FFCC0,,:::::::::::::::::::::::~DG017.GRF,10752,008,\n" +
                              ",:::::::::::::M01,M0380,M07,M0C,::M0I540,M03FFE0,M0H15,,:M0JA0,M0I760,,::M04,M08,,:::::::::M0IFE0,N03060,:::::N03860,N01DC0,O0F80,,:M07,M078C,M0C44,M0HC6,M0H46,M0I6,M0744,M0HFC,M0H50,,:M0238,M0474,M0C66,:M0CE6,M05C4,M078C,M05,,:O06,M05DFC0,M0EAE80,M0C06,,N040,M03F8,M0H7C,M0I6,M0C66,::M0466,M047C,M0278,,:M0H54,M07FE,M05,M0C,M04,M02,M0H76,M0HBA,,:M0514,M0HFE,M0H5C,O06,:O02,,::::::M0J20,M0IFE0,P0E0,O07,O0E,N054,N0E0,M0140,M07,M0FDDC0,M0JA0,,:M01D0,M07BC,M0514,M0C06,::M0504,M073C,M01F0,,::M04,M0C,,:::::::O01,M0E0180,M0F01C0,M0D8060,M0DC060,M0C6060,M0C7060,M0C38E0,M0C1FC0,M0803,,:M0141,M07E780,M041D40,M0C1860,::M041C40,M07E7C0,M0141,,::::::::M0IFE0,M0C1860,::::M041840,M0638E0,M0I7C0,M03C0,M01,,M0704,M0E8C,M0C44,M0C46,:M0C66,M0H74,M0HFC,M0D54,,:M0HBA,M0HFC,O0C,O04,O06,:M0HFC,M0H54,,N040,M01F8,M075C,M0606,M0C06,:M0404,M0208,M0FDDC0,M0JA0,,:M0H54,M07FE,M0510,M0C,M04,M02,M0H76,M0HBA,,:M0H54,M0HFE,O04,O06,::M0D5C,M0FE8,,:L041F0,L0E71C,L04504,L0HC06,:L0HC04,L0571C,L03FFE,L01554,,::::::N040,N0C0,::::N040,,::::::M01C0,M01B0,M019C,M0186,M0181,M0181C0,M0IFE0,M0180,M01,,:N0FE,M075740,M0600C0,M0C0040,M0C0060,M040060,M0600E0,M0755C0,N0FE,,::O02,O01,O0180,M0I5C0,M0IFE0,M0511,,::N010,M01FE,M0H75C0,M0618C0,M041C60,M0C0C60,:M0E08E0,M0H71C0,M03F1,,::O02,O01,O0180,M0H51C0,M0IFE0,M0I540,,::::::::::M0IFE0,N03060,::::::N01DC0,O0F80,,:N054,M01FF,M030180,M0600C0,M040040,M0C0060,::M040060,M0C00E0,M0701C0,M030180,M01D7,N078,N010,,::::::M0H5140,M0IFE0,M0D5D60,M0C1860,::::M041540,M072780,M01C1,,:N038,M01DF,M030180,M0701C0,M060060,M0C0040,M0C0060,::M040040,M0600C0,M070180,M01FF,N054,,:M0C0020,M070060,M0381C0,M0147,N03E,N03C,N07E,M01C7,M038180,M070040,M080020,,::::::::O02,O01,O0180,M0515C0,M0IFE0,M0I540,,::::O01,O03,O01,P080,M0IFE0,,::::M0101,M070180,M040040,M0C0060,M0C1860,:M051540,M0I380,M01C1,,N010,M01FE,M0H75C0,M0618C0,M041C60,M0C0C60,:M0E08E0,M0H71C0,M03F1,,::::::P020,P060,:::M0I560,M0IFE0,P060,:::P040,,M01D0,M07EC,M0564,M0C66,::M0464,M0H68,M0470,,:M0IBA0,M0IFE0,,::M0C,:,::::::::N030,::M0130,M03BB,M0175,N030,::N010,,M0H10,M01FF,M0H75C0,M0618C0,M041C60,M0C0C60,:M0E08E0,M0H71C0,M03E1,,:M0C01,M0F00C0,M0D0040,M0C8060,M0C4060,M0C2060,M0C1440,M0C0FC0,M0405,,N040,N0C0,::::N040,,M0C01,M0F00C0,M0D0040,M0C8060,M0C4060,M0C2060,M0C1140,M0C0F80,M0405,,O01,M0E0180,M0F01C0,M0D8060,M0DC060,M0C6060,M0C7060,M0C38E0,M0C1FC0,M0803,,:N0C0,::::,:M0E0180,M0F01C0,M0D8060,M0DC060,M0C4060,M0C7060,M0C38E0,M0C1DC0,M0C02,,:M0155,M03EF80,M070140,M0C0060,::M070140,M03AB80,M017D,,M01,M0301,M0701C0,M0E00C0,M0C1060,M0C1860,M0C1C40,M0E1CC0,M0I740,M03E0,,:M0101,M070180,M040040,M0C0060,M0C1860,:M0415C0,M07B780,M0141,,:P060,:M0A0060,M0FC060,N0F860,N01560,O0360,O01E0,P060,,:M0H14,M071F80,M041960,M0C0860,M0C0C60,:M051C60,M03B860,M01D040,,:M0310,M071D40,M0E08E0,M0C0C60,::M060860,M0H7060,M03F0,,:::::::M0I540,M0IFE0,M051D60,N01860,::::N01060,P060,,:M0704,M0E8C,M0C44,M0C46,:M0C66,M0H74,M0HFC,M0D54,,:M0C02,M071C,M01B8,M01F0,M03B8,M071C,M0C06,,::M0C,M04,,::::::::N010,N030,::M0175,M03FF,M0131,N030,::,:M01FD,M03BB80,M051940,M0C0860,M0C0C60,:M041D40,M03B9C0,M01F1,,O01,M0E0180,M0F01C0,M0D8060,M0DC060,M0C6060,M0C7060,M0C38E0,M0C1FC0,M0803,,:N0C0,::::,:M0C0180,M0F01C0,M0D8060,M0DC060,M0C4060,M0C7060,M0C38E0,M0C1DC0,M0C02,,:M0C01,M0F00C0,M0D0040,M0C8060,M0C4060,M0C2060,M0C1140,M0C0F80,M0405,,:N0C0,::::N040,,M0D01,M0F01C0,M0D0040,M0C8060,M0C4060,M0C2060,M0C1440,M0C0FC0,M0405,,:N0FE,M075740,M0600C0,M0C0040,M0C0060,:M0600C0,M0755C0,N0FE,N010,,M01C0,M01A0,M0194,M0186,M0181,M0180C0,M0IFE0,M0180,:,::O01,O03,O01,O0180,M0IFC0,,::::M0101,M070180,M040040,M0C0060,M0C1860,:M0415C0,M07B780,M0141,,:N0FE,M075740,M0600C0,M0C0040,M0C0060,M040060,M0600E0,M0755C0,N0FE,,:M01FD,M03BB80,M051940,M0C0860,M0C0C60,:M051D40,M03B9C0,M01D1,,::::::::M0I760,M0IBA0,,:M01,M0BAA,M0FDC,O0C,O04,O06,:M0HFC,M0H54,,N040,M01F8,M075C,M0606,M0C06,:M0404,M0208,M0FDDC0,M0JA0,,:M01D0,M07B8,M0504,M0C06,::M0404,M0738,M01F0,,M01,M0BAA,M0FDC,O0C,O04,O06,:M0HFC,M0H54,,N040,M01F8,M0H7C,M0I6,M0C66,::M0466,M067C,M0268,,:M0614,M063C,M0476,M0C66,M0C46,M0HCE,M07CC,M0380,,:M0H5H40,M0HFE60,,:M07,M078C,M0C44,M0HC6,M0C46,M0I6,M0744,M0HFC,M0H50,,::L054,L038,,::::::::M0IA,M0H7E,O08,O04,O06,M0106,M0BAE,M0FDC,O08,O04,O06,:M0ABE,M0H74,,:M0388,M07C4,M0HC6,M0C46,:M0H46,M0I6,M0HFC,M0EA8,,:M0H5H40,M0HFE60,M01,,M0I540,M0IFE0,N010,,M0H50,M0E3C,L01407,L0300380,L065D1C0,L0H638C0,L0H41440,L0HC0660,L0C40660,L0C60660,L0C71460,L0CFF860,L04C5C40,L06C00C0,L07601C0,L0H30380,L0H1D7,M0838,,:M0I540,M0IFE0,M0715,M0406,M0C06,:M0504,M072C,M01F4,,:M0HBA20,M0HFE60,,M01,M03F0,M075C,M0E0C,M0406,M0C06,:M0E0E,M075C,M03F8,,:O06,M0HBF80,M0H57C0,O0H60,O0460,P020,M0704,M0E8C,M0C44,M0C46,:M0C66,M0H74,M0HFC,M0D54,,::M0HFE,O08,O04,O06,:,M0H54,M0HFE,N01C,O04,O06,:M0H5C,M0HF8,N014,O06,::M0H5C,M0HF8,,:M07,M078C,M0C44,M0HC6,M0C46,M0I6,M0744,M0HFC,M0H50,,::M04,M08,,::M03F0,M075C,M0E0E,M0C06,:M0404,M071C,M0H10,,:M01F8,M075C,M060E,M0C04,M0C06,M0406,M060E,M075C,M03F8,M01,,:M0C,M04,,::M0H5H40,M0HFE60,,:M01F0,M073C,M0404,M0C06,:M0406,M051C,M0IFE0,M0I540,,:::::::N0C0,::::N040,,:::::O02,N05E,N0E8,M07,M0E,M05C0,N038,N01E,N03A,M05D0,M0E,M0740,N0F0,N05C,,O04,N02E,M0174,M0F80,M0D,M03A0,N05C,O0E,M0174,M03A0,M0F,M0F80,M0170,N01E,O04,O02,N016,N0F8,M0740,M0E,M0H50,N03A,N01E,N0BC,M0750,M0E,M0740,N0F8,N016,O02,,:M04,M08,,::M0J20,M0IFE0,M020C,M0404,M0C06,:M0E0E,M075C,M03F8,N040,,M0H5H40,M0HFE60,,:M0170,M07B8,M0404,M0C06,::M0404,M079C,M0170,,O04,O06,M0H5740,M0HBFE0,O0H60,:M010040,M0388,M07CC,M0HC6,M0C46,:M0H46,M0H6E,M07FC,M0HB8,,:M0514,M0HFE,M0H5C,O06,:O02,,M0IA,M0H7E,O08,O04,O06,:M0HBE,M0HFC,O08,O04,O06,:M0ABE,M0H74,,:M0388,M07CC,M0HC6,M0C46,:M0H46,M0I6,M07FC,M0EA8,,::M0C,M04,,::M0170,M07BC,M0404,M0C06,:M060E,M071C,M01,:,M01D0,M07BC,M0514,M0C06,::M0504,M073C,M01F0,,::M04,M0C,,::M0IA20,M0H7H60,,N040,M01F8,M075C,M0606,M0C06,:M0404,M0208,M0FDDC0,M0JA0,,:::::::::::::::::~DG018.GRF,02048,008,\n" +
                              ",::M0J20,M07FHF0,:M0H1D70,N01C70,:::O0EF0,O0FE0,O07C0,O01,,P050,P070,:::M07FHF0,:M03BBF0,P070,:::,::::::M0J20,M07FHF0,:M071D70,M071C70,:::M071E70,M075FF0,M03F3E0,M0171,,:M02AA20,M07FF30,:,:M0170,M03FE,M07DE,M0306,M0703,M0603,M0707,M038E,M01FC,N0F8,N040,,::::::M0J50,M07FHF0,:N01C70,:::::P070,,:M01C4,M07E6,M0767,M0623,M0673,M0233,M07F7,M07FE,M0H54,,:M0I2,M07FF,:M0H17,O03,::O01,M07FF,:M07FE,M0107,O03,O07,M07FF,:M03BE,O06,O03,M0103,M07FF,:M07E8,,:M01C4,M03E6,M0767,M0623,M0673,M0233,M07F7,M07FE,M0H54,,:::::::N0A8,L017FF,L03FBFE0,L0740570,L040H010,,:M0J50,M07FHF0,M0H7F70,N01C70,::::N01DF0,O0FE0,O0740,,N040,N0F8,M03FF,M03B6,M0733,M0633,M0733,M0H3E,M0H3C,N038,N010,,M0I5,M07FF,M0H7F,O06,O07,O03,O01,,M0H1C,M0H3E,M073F,M0633,M0673,:M0I7,M03E6,M01C4,,N010,M01F8,M01FC,M03BE,M0733,M0633,M0637,M0H3E,M0H3D,M0138,,:M0I5,M07FF,:O06,O03,:O01,,M0170,M03FE,M07DE,M0306,M0703,M0603,M0707,M038E,M01FC,N0F8,N040,,L040H010,L070H070,L05D55D0,M0IF80,M0175,,::::::::::::::::::::::::::::::::~DG019.GRF,02816,008,\n" +
                              ",::::::M01,M07,L01F,L03F,L07F,L07F80,L07D,L0F8,L0F0,:::L070,L07E,L07FKF0,L03FKF0,L01FKF0,M0LF0,M057FFDF0,,::::M0J10,M0JF8,L01FIF8,L07FIF8,:L0KF8,L0FD04,L0F8,L0F0,:L070,:L078,L03E,L05FFDD8,L0KF8,:::,::::L0K50,L0KF8,::L0KF0,L0IBFE0,O01E0,P0F0,P070,P078,::L015H5F8,L0KF8,L0KF0,:L0JFD0,L0JFC0,O01E0,P0E0,P070,P078,::L0J1F8,L0KF8,L0KF0,::L0JF80,M0404,,::::L0M50,L0MF0,:::L0M20,,::M02,L01F01,L03FC1C0,L07FC1F0,L07FE3F0,L0FDE3F0,L0F8E0F8,L0F07070,L0F07078,L0707078,L0703078,:L03C38F8,L01FIF0,L0KF0,:L0JFE0,L0JFC0,L0E0,,::::L0MF0,::::L0J5D550,P0E0,P070,::P078,:P0F8,L0FDDFF0,L0KF0,:L0JFE0,L0JFC0,,:::::::::::::L080,L0F4,L0HF80,L0IF5,L03FHF80,M05FHFC,N03FHFA0,O017FF0,P03FF0,Q05F0,R010,:,::::::::::N0H1,N03FE0,M01FHFC,M07FHFE,L01FJF,L01FJF80,L03FD57FC0,L03E0H0FC0,L07C0H07E0,L0780H03E0,L070I01F0,L0F0J0E0,L0F0J0F0,L0F20I0F0,L0F70I0F0,L0F780H0F0,L0H7I01F0,L07E0H01E0,L07E0H01F0,L07E0H03E0,L07F0017C0,L07FE03FC0,L0LFC0,L0LF80,K01F7FIF,K03E3FHF8,K01C07FD0,L0C0,,:::L017FHF8,L03FIF8,L07FIF8,:L07FF7F0,L0F8,L0F0,:L070,::L038,L01D1010,L0BFIF8,L0KF8,:::M0404,,::M04,L03F80,L07FC1C0,L07FE1E0,L07FE1F0,L0HFE3F0,L0F071F0,L0F07078,::L0707078,L0383078,L03C39F0,L03FIF8,L07FIF0,L0KF0,L0JFE0,L0JFC0,L0D1H1,,::::L0K50,L0KF8,:::O0BC0,O01F0,P070,:P078,::O01F8,L0KF8,L0KF0,:L0JFE0,L0JF80,L0J1,,::P078,:L015H57D,L03FJF80,L07FJFC0,L0LFE0,:L0FAHAFAA0,L0F0H078,:L070H078,,:::L0KA8A0,L0KF8F0,:::L0K5050,,::P078,:L015H57D,L03FJF80,L07FJFC0,L0LFE0,:L0FAHAFAA0,L0F0H078,:L070H078,,P010,P038,J070I05F8,J0F0I0HF8,J0F0H07FF8,J0F003FHF8,J0FC17FFC0,J0FE3FFE,J07FIF0,J03FHF80,J01FFC,K0IF80,K01FHF0,L03FFE,L017FFD0,N0IF8,N05FF8,O03F8,O0178,P018,P010,,:::::::::::::::~DG020.GRF,02048,008,\n" +
                              ",:I01E002,I01E00E,I01E03E,I01E07E,I01E0FE,I01E0FF,I01E0FA,I01E1F0,I01E1E0,:::I01E0E0,I01E0FC,I01E0FKFE0,I01E07FJFE0,I01E03FJFE0,I01E01FJFE0,I01E00AFHFBE0,I01E0,::::I01E00FFE,I01E01FHF,I01E03FHF80,I01E07FHFC0,I01E0FEEFE0,I01E0F0E3E0,I01E0E0E0E0,I01E1E0E0F0,:::I01E1E0E1F0,I01E0F8E3E0,I01E0F8FFE0,I01E0F8FFC0,I01E070FF80,I01E030FE80,I01E010FC,I01E0H0A0,I01E0,:::I01E1FJF0,::::I01E0AHAFA0,I01E0I01C0,I01E0J0E0,::I01E0J0F0,:I01E0I01F0,I01E0EHEFE0,I01E1FIFE0,:I01E1FIFC0,I01E1FHFE80,I01E0,:::::I01E15J5140,I01E1FJF1E0,:::I01E0AJA0A0,I01E0,::I01E02802,I01E0381F,I01E0383F80,I01E0787FC0,I01E0F87FE0,I01E0FC7FE0,I01E0F8F8E0,I01E1E0F8F0,:I01E1E1F0F0,::I01E0E3E0E0,I01E0F3E3E0,I01E0FFE3E0,I01E0FFC1E0,I01E07FE3E0,I01E03FC1,I01E00A80,I01E0,:::::::::::::::I01E1FKFE0,::::I01E1E2AFA3E0,I01E1E00F01E0,::::::::I01E0E00F83E0,I01E1F01FD7C0,I01E0FEFIFE0,I01E0FHFDFFC0,I01E0FHF8FF80,I01E07FF87F,I01E02FE02A,I01E007C0,I01E0,::I01E004,I01E03E02,I01E07F8380,I01E0FF83E0,I01E0FFC7E0,I01E1FBC7E0,I01E1F1C1F0,I01E1E0E0E0,I01E1E0E0F0,I01E0E0E0F0,I01E0E060F0,:I01E07871F0,I01E03FHFE0,I01E1FIFE0,:I01E1FIFC0,I01E1FIF80,I01E1C0,I01E0,:::I01E0AA2AA0,I01E1FJF0,:::I01E1FIFC0,I01E0I0BE0,I01E0I01E0,:I01E0I01F0,::I01E0J0E0,I01E0,I01E008,I01E07F,I01E0FF8380,I01E0FFC3C0,I01E0FFC3E0,I01E1FFC7E0,I01E1E0E3E0,I01E1E0E0F0,::I01E0E0E0F0,I01E07060F0,I01E07873E0,I01E07FIF0,I01E0FIFE0,I01E1FIFE0,I01E1FIFC0,I01E1FIF80,I01E1A2H2,I01E0,::::I01E0AJA0,I01E1FJF0,:::I01E0H01780,I01E0I03E0,I01E0J0E0,:I01E0J0F0,::I01E0I03F0,I01E1FJF0,I01E1FIFE0,:I01E1FIFC0,I01E1FIF,I01E02I2,I01E0,::I01E0H020,I01E407FC,I01F82FHF80,I01FC7FHFC0,I01F8FIFE0,:I01F8FA0BE0,I01F1F001F0,I01E1E0H0F0,:::I01E0E0H0E0,I01E07001C0,I01F8380B80,I01FKF80,I01FLF0,:::I01E2AJA0,I01E0,:,::::::~DG021.GRF,01536,008,\n" +
                              ",:::::L017F4,L07FFE,K01FIFC0,K03FIFE0,K07FJF0,K07F83FF8,K07C005FC,K0F80H0FC,K070I07C,K0F0I03E,K0F0I01F,K0F0J0E,K0F0J0F,:K0780I0F,:K07F0H01F,K03F8001E,K01F0H05E,K01F800FE,K017001FC,L03001F8,L01001F0,P0E0,O01,,L014,L07F80,K01FHF0,K03FHF8,K07FHFC,K07FHFE,K07D17F,K0F803F,K0F001F,K0F0H0F80,K0F0H0780,:K070H0780,K07800780,K07D007,K03F81F,K03FIF,K01FHFE,K017FFD,L03FF8,L017F0,,:K08,K0F40,K0HF8,K0IF50,K0JFE,K07FIFD4,L07FJF,L015FIF,N03FHF,O05FF,Q0F,K0F0J01,K0FE0,K0IF40,K0JF8,K07FIF,K03FIF8E,L057FF8F,N0HF8F,N0158F,Q0F,Q05,,L07F40,L0IF0,K01FHFC,K03FHFC,K07F7FF,K07873F,K07071F,K0F070F,K0F0707,K0F070780,:K0F870780,K078707,K07C70F,K07C7FF,K0387FE,K0187FC,M07F8,M0H50,,::::::::::::K05,K0FE0,K0HFD40,K0JF8,K0KF50,K03FJFA,L017FIF,N07FHF,N015FF,O01FF,O07FF,N03FFE,M01FHF0,M0IF80,L05FFD,L0IF8,K07FF40,K0HFE,K0HF0,K0HFE80,K0JF5,K0KFA0,K057FIFD,M0KF,M017FHF,O03FF,P05F,Q01,L0150H01,L07F80,K01FHF0,K03FHF8,K07FHFC,K07FBFE,K07C07F,K0F801F,K0F001F,K0F0H0780,::K070H0780,K07800780,K07C007,K03F81F,K03FF7F,K01FHFE,L0IFC,L03FF8,L017F0,,::::::::::::::::::::::::::::::::~DG022.GRF,07296,012,\n" +
                              ",:::::::::::::::::J01F80,:::J01F80EOE,J01F81FNFE,::::::J01F81FE020202FE,J01F81FC0K0FE,::::::::J01F80FC0K0FE,J01F81FC0J01FC,J01F80FE0J01FE,J01F80FE0J03FC,J01F80FF80I03FC,J01F807FC0I0HFC,J01F807FEA22BFF8,J01F807FMF0,J01F803FMF0,J01F801FLFE0,J01F800FLFE0,J01F8007FKF,J01F8003FJFE,J01F80H07FIF8,J01F80H02AFEA0,J01F80,:::J01F81C0,J01F81F8,J01F81FF,J01F81FFA0,J01F81FHF0,J01F81FHFE,J01F81FIFC0,J01F80FIFE8,J01F807FIFC,J01F800BFIF80,J01F8001FJF0,J01F80H0KFE,J01F80H0LF80,J01F80H0FEFIFE8,J01F80H0FE1FIFC,J01F80H0FE03FHFE,J01F80H0FE007FFE,J01F80H0FE002FFE,J01F80H0FE0H07FE,J01F80H0FE002FFE,J01F80H0FE00FHFE,J01F80H0FE0BFHFE,J01F80H0FE1FIFC,J01F80H0FEFIFE0,J01F80H0LF,J01F80H0KFE,J01F8001FJF0,J01F800FJFA0,J01F803FIFE,J01F80FJF8,J01F81FIFC0,J01F81FHFE,J01F81FHF0,J01F81FFE0,J01F81FF,J01F81FA,J01F81E0,J01F8180,J01F80,:::J01F81FNFE,::::::J01F81FHFBFHFBFE,J01F80J0FE00FE,:::::::::::::::J01F80J05400FE,J01F80N0FE,:J01F80N0HA,J01F80,:::J01F80N054,J01F80N0FE,:::::::::::J01F81FNFE,::::::J01F80N0FE,:::::::::::J01F80,:J01F81C0,J01F81F8,J01F81FF,J01F81FFA0,J01F81FHF0,J01F81FHFE,J01F81FIFC0,J01F80FIFE8,J01F807FIFE,J01F800FJF80,J01F8001FJF0,J01F80H0KFE,J01F80H0LF80,J01F80H0FEFIFE8,J01F80H0FE1FIFC,J01F80H0FE03FHFE,J01F80H0FE007FFE,J01F80H0FE002FFE,J01F80H0FE0H07FE,J01F80H0FE002FFE,J01F80H0FE00FHFE,J01F80H0FE0BFHFE,J01F80H0FE1FIFC,J01F80H0FEFIFE0,J01F80H0LF,J01F80H0KFE,J01F8001FJF0,J01F802FJFA0,J01F803FIFE,J01F80FJF8,J01F81FIFC0,J01F81FHFE,J01F81FHF0,J01F81FFE0,J01F81FF,J01F81FA,J01F81E0,J01F8180,J01F80,:::J01F81FNFE,:::::::J01F80J0FE00FE,:::::J01F80I01FE00FE,J01F80I03FE00FE,:J01F80I0HFE00FE,J01F80H01FFE00FE,J01F80H03FFE00FE,J01F80H0IFE00FE,J01F8003FHFE00FE,J01F8007FF7F01FE,J01F800FHF3F83FE,J01F803FFE3FIFC,J01F80FHFE3FIFC,J01F81FHFC3FIFC,J01F81FHF01FIF8,J01F81FFC00FIF0,J01F81FF800FHFE0,J01F81FE0H07FFC0,J01F81F80I0HF80,J01F81F0,J01F81E0,J01F81,J01F80,::::::::::::::::::::J01F81FNFE,:::::::J01F80,:::::J01F80H010,J01F8002F0I08,J01F8007F0H07F,J01F802FF002FFE0,J01F803FF007FHF0,J01F803FF80FIF8,J01F807FF80FIF8,J01F80FFE80FIFE,J01F80FFC01FIFE,J01F81FF803FEBFE,J01F81FE003FC0FE,J01F83FE003F80FE,J01F81FC007F807F,J01F83F8007F807F,:J01F83F800FF007F,:::J01F83F800FE00FE,J01F83FC01FE00FE,J01F83FC03FE00FE,J01F81FE03FC03FE,J01F81FE07FE0FFE,J01F81FJFC1FFC,J01F80FJF81FF8,:J01F807FIF01FE0,J01F803FHFE01FE0,J01F803FHFE01F80,J01F8007FF801C,J01F8002EE,J01F80,::::::J01F80EOE,J01F81FNFE,::::::J01F802O2,J01F80,::::::::::::::::::::::J01F80EOE,J01F81FNFE,::::::J01F81FE02FE02FE,J01F81FC00FE00FE,::::::::::J01F80FC00FE00FE,J01F81FC00FF01FE,J01F80FE01FF83FE,J01F81FE01FFC7FC,J01F80FF83FJFE,J01F80FNFC,J01F80FIFEFIF8,J01F807FHFE7FHF0,J01F803FHFE3FFE0,J01F803FHFC1FFC0,J01F801FHF80BF80,J01F8007FF,J01F8002FA,J01F80,:::J01F81C0,J01F81F8,J01F81FF,J01F81FFA0,J01F81FHF0,J01F81FHFE,J01F81FIFC0,J01F80FIFE8,J01F807FIFC,J01F800FJF80,J01F8001FJF0,J01F80H0KFE,J01F80H0LF80,J01F80H0FEFIFE8,J01F80H0FE1FIFC,J01F80H0FE03FHFE,J01F80H0FE007FFE,J01F80H0FE002FFE,J01F80H0FE0H07FE,J01F80H0FE002FFE,J01F80H0FE00FHFE,J01F80H0FE0BFHFE,J01F80H0FE1FIFC,J01F80H0FEFIFE0,J01F80H0LF,J01F80H0KFA,J01F8001FJF0,J01F800FJFA0,J01F803FIFE,J01F80FJF8,J01F81FIFC0,J01F81FHFE,J01F81FHF0,J01F81FFE0,J01F81FF,J01F81FA,J01F81E0,J01F8180,J01F80,:::J01F81FNFE,::::::J01F81FBFLFE,J01F80J0FE00FE,:::::J01F80I01FE00FE,J01F80I03FE00FE,:J01F80I0HFE00FE,J01F80H01FFE00FE,J01F80H03FFE00FE,J01F80H0IFE00FE,J01F8003FHFE00FE,J01F8007FF7F01FE,J01F800FHFBF83FE,J01F807FFE3FIFC,J01F80FHFE3FIFC,J01F81FHFC3FIFC,J01F81FHF81FIF8,J01F81FFC00FIF0,J01F81FF800FHFE0,J01F81FE0H07FFC0,J01F81F80I0HF80,J01F81F0,J01F81E0,J01F81,J01F80,J01F81,J01F81E0,J01F81F0,J01F81FE80,J01F81FFC0,J01F81FFE8,J01F81FHFE,J01F81FIF80,J01F81FJF0,J01F803FIFE,J01F8007FIFC0,J01F8002FIFE8,J01F80H0KFC,J01F80H0LFA0,J01F80H0FE7FIF0,J01F80H0FE3FIFA,J01F80H0FE07FHFE,J01F80H0FE00FHFE,J01F80H0FE001FFE,J01F80H0FE0H0HFE,J01F80H0FE001FFE,J01F80H0FE02FHFE,J01F80H0FE07FHFE,J01F80H0FE3FIFA,J01F80H0MF0,J01F80H0LFA0,J01F80H0KFC,J01F80H0JFE8,J01F8007FIFC0,J01F803FIFE,J01F81FJF0,J01F81FIFA0,J01F81FIF,J01F81FFE8,J01F81FFE0,J01F81FE80,J01F81FC,J01F81E0,J01F81,J01F80,:::J01F80AOA,J01F81FNFE,:::::J01F804K47FFE,J01F80L0IF8,J01F80K03FHF0,J01F80K0IFE0,J01F80J01FHF,J01F80J0BFFE,J01F80I01FHFC,J01F80I03FFE0,J01F80I07FFC0,J01F80H03FHF80,J01F80H07FFE,J01F8002FHF8,J01F8003FHF0,J01F800FHFE0,J01F801FHF,J01F80FHFE,J01F81FHF8,J01F81FHFMA,J01F81FNFE,:::::J01F817575757574,J01F80,:::::J01F80J020,J01F80I05FF,J01F80H03FHFE8,J01F80H0KFC,J01F8003FJFE,J01F8007FKF80,J01F800FLFE0,J01F801FLFE0,J01F803FHFEFIF0,J01F807FFC007FF8,J01F80FFE0H02FF8,J01F80FF80I07FC,J01F80FF0J03FE,J01F81FE0J01FE,J01F81FE0K0FE,J01F81FC0K0FE,J01F83F80K0FE,J01F83F80K07F,:::J01F83F807F0H07F,::J01F83F807F0H0FE,J01F81FC07F0H0HF,J01F83FC07F0H0FE,J01F81FC07F001FE,J01F81FE07F003FE,J01F81FE07F00FFC,J01F80FFAFF03FFC,J01F80FJF01FF8,J01F80FJF03FF8,J01F807FIF01FF0,J01F803FIF01FE0,J01F803FIF01FC0,J01F803FIFH0E,J01F800FIFH0C,J01F800AIA0,J01F80,::,:::::::::::::::::::~DG023.GRF,02560,008,\n" +
                              ",:::I0340,I03F8,I03FFD0,I03FHFE,I03FIFD0,J0KFE,J045FIFC0,M0IFC0,M017FC0,N07FC0,M05FFC0,M0IF80,L07FFC,K03FFE0,J05FHF40,J07FFC,I01FHF0,I03FF80,I03FC,I03FF80,I03FHF5,I03FIFE0,J05FJF40,K02FIFC0,L057FFC0,N0HFC0,N015C0,P0C0,J07C,J0HF,I01FF04,I01FF86,I03F7C7,I03E3C380,I03C1C7C0,I03C1C3C0,I01C1C1C0,I01C0C1E0,:J0E0C1E0,I01F5C1E0,I03FFE3E0,I03FIFC0,::K0IF80,L05F,,::I03D4,I03FE,I03FFD4,I03FIF80,I01FIFE0,J03FHFE0,K05FFE0,L01FE0,M0740,M0380,M01C0,I020H01C0,I03D401C0,I03FF81E0,I03FHF7E0,I03FIFE0,I01FIFC0,K0IFC0,K017FC0,M0F,M07,M0380,M01C0,I020H01C0,I03D401C0,I03FF81E0,I03FHF7E0,I03FIFE0,I015FHFC0,K0IFC0,K017FC0,M02,,:K01,K0FE0,J07FFC,J0IFE,I01FIF,I01FIF80,I01F5DFC0,I03E1C7C0,I03C1C3C0,I03C1C1E0,::I01C1C1E0,I01E1C3E0,I01F1C7C0,J0E1FFC0,J071FFC0,J061FF80,J041FF,L0E8,,:::::::::::::J05FD0,J07FFC,J07FHF,J0JF,I01FDFFC0,I01E01FC0,I01C007C0,I03C003E0,I03C001C0,I03C001E0,:I03E001E0,I01F001C0,I01F803E0,I01FD07C0,J0JFC0,J07FHFC0,J03FHF80,J01FHF,K03F8,L040,,:I02,I03D401C0,I03FF81E0,I03FHF5E0,I03FIFE0,I01FJFC,K0KF,K017FHF80,M07FF80,M01FFC0,M01EFC0,M01E3C0,N0E3C0,N043C0,O0380,O0180,,:::::::I0380,I03FC,I03FFC0,I03FHF5,I03FIFE0,I01FJFD40,K0KFC0,K017FHFC0,L07BFFC0,L0785FC0,L07803C0,:::::L03803C0,L07C03C0,L03E03C0,L03F17C0,L03FHF80,L01FHFC0,M0IF80,M07FF,M01FE,J040H054,I03C0,I03FD,I03FFE0,I03FHFD40,I03FIFE0,I015FHFE0,K03FFE0,L05FE0,M0E20,M07,M0780,M07C0,M03E0,::K054140,J01FE0,J07FFC,J0IFE,I01FIF,I01FEFF80,I01F01FC0,I03E007C0,I03C007C0,I03C001E0,::I01C001E0,I01E001E0,I01F001C0,J0FE07C0,J0HFDFC0,J07FHF80,J03FHF,K0HFE,K05FC,,::K0540,J03FE0,J07FF4,J0IFE,I01FIF,I03FEFF80,I03F01FC0,I03E007C0,I03C007C0,I03C001E0,I01C001E0,:I01C001C0,J0F003C0,I03F407C0,I03FFAF80,I03FIF,I03FIF80,I01FJF5,K0KF80,K057FHFC0,M0IFC0,M017FC0,O03C0,J040J040,J0FE,I01FFD0,I03FHFE,I01FIFC0,I03FIFE0,I03D5FFE0,I03C03FE0,I01C005E0,I01C0,:J0E0,I0170,I03FC,I03FF50,I03FHFE,I03FIFC0,J0JFE0,J017FFE0,L03FE0,M05E0,N020,,:K0540,J01FE0,J07FF4,J0IFE,I01FIF,I01FIF80,I01F05FC0,I03C00FC0,I03C007C0,I03C001E0,:I03E001E0,I01F001E0,I01F801E0,I01F007C0,J0F80FC0,J07007C0,J0300F80,M07,M06,M04,,:J0200180,I01FD41E0,I01FFE9E0,I01FIFE0,I03FJF8,I03DFIFC,I03C0FHFC,I03C017FE,I03C001FE,J04001E5,M01E0,M0160,,::~DG024.GRF,05760,012,\n" +
                              ",::::::::::::::::::::::::::::::::L028,L03F40,L03FFA80,L03FIF8,L03FIFEA0,L03FLF,L03FMFA80,L03FNFC0,L03FAFLFC0,L03F807FJFC0,L03F800ABFHFC0,L03F80H01FHFC0,L03F80J0BFC0,L03F80K01C0,L03F80,:::::::::L02F80,,::L020,L03F,L03FFA80,L03FIF0,L03FIFEA0,L03FKFC,L03FMFA80,L01FNFC0,N0AFLFC0,O01FKFC0,Q0BFIFC0,R01FHFC0,T0BFC0,U01C0,V080,N018,M02F8,M07FC,M0HF8,L01FFC0H05,L01FFC003FF8,L01FF400FHFC,L03F8001FHFE,L03F8001FIF80,L03F8003FIF80,L07F0H07FIFC0,L07F0H0HF83FC0,L07F0H0HF01FC0,L07F0H0FE00FE0,L07F001FE00FE0,L03F803FE00FE0,L07F003FC00FE0,L03F803F800FE0,L03F807F800FE0,L03FE0FF800FE0,L01FJFH01FC0,M0JFE003FC0,M0JFC007FC0,M0BFHF803FF80,M03FHF803FF,N0HFE003FE,N01F8003FC,O080H03F8,S01C0,,:::::T01A80,T01FC0,::L03A80J01FC0,L03FD0J01FC0,L03FFEA0H01FC0,L03FIFC001FC0,L03FJFA81FC0,L03FLF1FC0,L03FNFC0,M01FMFC0,N08BFKFC0,P01FJFC0,Q02FIFC0,S07FFC0,T0BFC0,T01FC0,::::::::U0BC0,V040,,:::::::::::O0HAH80,N01FHFC0,N0KFA,M01FKF,M03FKF80,M0NF0,M0NF8,L01FFC01FHFC,L03FE8003FFE,L03FC0I07FF,L03F80I03FF80,L07F80J0HF80,L07F80J03F80,L07F0K01FC0,L07F0K01FE0,L07F0K01FC0,L03F0L0FE0,L07F0L0FE0,L03F80K0FE0,L03FC0K0FE0,L03FE0K0FE0,L01FF0J01FC0,M0HF80I01FE0,M0HFC0I03FC0,M0BFF80H0BFC0,M03FHFH01FF80,M03FHFEAFHF80,N0NF,N0BFKFE,N01FKFC,O03FJF8,P0JFC0,P02BFA80,,::L03FA8,L03FFD,L03FHFEA,L03FJFC0,L03FKFA88,L03FMFC,L02AFMF80,N01FLFC0,P0BFJFC0,P01FJFC0,P01FEBFFC0,P01FC07FC0,P01FC01FC0,::::::::::::Q05C01FC0,T01FC0,:U0BC0,V040,,:::::::::::::O0BFBA8,N07FIF8,M02FJFE,M07FKFC0,M0MFE0,L01FMF8,L03FFEABFHF8,L03FF0H07FFE,L03FE0I0BFE,L03FC0I01FF,L03F80J0HF80,L07F0K07FC0,L07F0K03FC0,L07F0K01FC0,L07F0L0FE0,L07F0L0FC0,L03F80K0FE0,::L03FC0K0FE0,L01FE0K0FE0,L01FF80I01FC0,M0HFE0I03FC0,M07FE0I07FC0,M03FE0H03FF80,M01FC0H07FF80,N0FE0H03FE,N01C0H07FE,O080H03F8,S03F0,S02A0,,::O02AA,N01FHFC0,N0KF8,M01FKF,M03FKFA0,M0MFE0,M0NF8,L01FFC01FHFC,L03FE0H03FFE,L03FC0I07FF,L03F80I01FF80,L07F80J0HF80,L07F80J03F80,L07F0K03FC0,L07F0K01FC0,:L03F0L0FE0,L07F0L0FE0,L03F80K0FE0,:L03FE0K0FE0,L01FE0K0FE0,M0HF80I01FE0,M0HFC0I03FC0,M07FE80H0HFC0,M03FHFH01FF80,M01FIFBFHF80,N0NF,N03FKFE,N01FKFC,O0BFJF8,O01FIFC0,P02BFE80,,:L010,L03EA0,L03FFC,L03FHFA8,L03FJFC0,L03FKFA80,L03FMF4,M0ABFLF80,N017FKFC0,P0AFJFC0,Q01FIFC0,S0IFC0,R01FHFC0,R0JF80,Q07FIF,P03FIF8,O01FIFC0,O0BFHFE,N01FIF0,M08FIF80,M0JFC,L03FHFE8,L03FHF,L03FFE80,L03FIF8,L03FIFEA0,L03FLF,L03FMFH80,M01FMFC0,N02AFKFC0,P01FJFC0,R0BFHFC0,S01FFC0,T02BC0,V040,,:::T01A80,T01FC0,::L03880J01FC0,L03FD0J01FC0,L03FFEA0H01FC0,L03FIFC001FC0,L03FJFA81FC0,L03FLF9FC0,L03FNFC0,M01FMFC0,O0BFKFC0,P01FJFC0,Q02FIFC0,S07FFC0,T0BFC0,T01FC0,:::::::L010L01FC0,L03EA0K0BC0,L03FFC,L03FHFA8,L03FJFC0,L03FKFE80,L03FMF4,L03FBFLF80,L03F97FKFC0,L03F80BFJFC0,L03F800FJFC0,L03F800FEBFFC0,L03F800FE01FC0,:::::::::::::L01F800FE01FC0,M0A800BE01FC0,Q01601FC0,T01FC0,:U0BC0,L010,L03EA0,L03FFC,L03FHFA8,L03FJFC0,L03FKFA80,L03FMF4,M0ABFLF80,N017FKFC0,P0AFJFC0,Q01FIFC0,S0IFC0,R01FHFC0,R0JF80,Q07FIF,P03FIF8,O01FIFC0,O0BFHFE,N01FIF0,M08FIF80,M0JFC,L03FHFE8,L03FHF,L03FFE80,L03FIF8,L03FIFEA0,L03FLF,L03FMFH80,M01FMFC0,N02AFKFC0,P01FJFC0,R0BFHFC0,S01FFC0,T02BC0,V040,,:::T01A80,T01FC0,::L03880J01FC0,L03FD0J01FC0,L03FFEA0H01FC0,L03FIFC001FC0,L03FJFA81FC0,L03FLF9FC0,L03FNFC0,M01FMFC0,O0BFKFC0,P01FJFC0,Q02FIFC0,S07FFC0,T0BFC0,T01FC0,::::::::U0BC0,,:::::::::::::::::~DG025.GRF,00512,008,\n" +
                              ",::::::::::::::M020202,,::M0IFC0,L01FHFE0,L03FHFA0,L01FHFE0,L03F02A2,L01E00E0,L03C27A0,L01CE7C0,L03CE6A0,L01CE7E0,L03CE7A0,L01CE7C0,L03FFEA2,L01FHFE0,L01FHFA0,M0IFC0,,::::::::::::::::::::::::::::~DG026.GRF,00512,008,\n" +
                              ",::::::::::::::::::::K02020202,,:::L03FHFC0,::K023FHFC2,L03FHFC0,:L03F1FC0,::O01C0,:K023F1AC2,L03F18C0,L03FD8C0,L03FHFC0,:,::K02020202,,:::::::::::::::::^XA\n" +
                              "^PW878\n" +
                              "^FT704,352^XG000.GRF,1,1^FS\n" +
                              "^FT96,384^XG001.GRF,1,1^FS\n" +
                              "^FT160,320^XG002.GRF,1,1^FS\n" +
                              "^FT32,384^XG003.GRF,1,1^FS\n" +
                              "^FT256,352^XG004.GRF,1,1^FS\n" +
                              "^FT128,384^XG005.GRF,1,1^FS\n" +
                              "^FT448,288^XG006.GRF,1,1^FS\n" +
                              "^FT224,384^XG007.GRF,1,1^FS\n" +
                              "^FT288,480^XG008.GRF,1,1^FS\n" +
                              "^FT352,480^XG009.GRF,1,1^FS\n" +
                              "^FT384,480^XG010.GRF,1,1^FS\n" +
                              "^FT544,256^XG011.GRF,1,1^FS\n" +
                              "^FT416,288^XG012.GRF,1,1^FS\n" +
                              "^FT288,1632^XG013.GRF,1,1^FS\n" +
                              "^FT352,1632^XG014.GRF,1,1^FS\n" +
                              "^FT0,1984^XG015.GRF,1,1^FS\n" +
                              "^FT0,1824^XG016.GRF,1,1^FS\n" +
                              "^FT0,1664^XG017.GRF,1,1^FS\n" +
                              "^FT0,352^XG018.GRF,1,1^FS\n" +
                              "^FT384,1632^XG019.GRF,1,1^FS\n" +
                              "^FT640,352^XG020.GRF,1,1^FS\n" +
                              "^FT512,288^XG021.GRF,1,1^FS\n" +
                              "^FT736,1376^XG022.GRF,1,1^FS\n" +
                              "^FT608,416^XG023.GRF,1,1^FS\n" +
                              "^FT672,1312^XG024.GRF,1,1^FS\n" +
                              "^FT0,1856^XG025.GRF,1,1^FS\n" +
                              "^FT0,1696^XG026.GRF,1,1^FS\n" +
                              "^FO1,79^GB833,1913,8^FS\n" +
                              "^FT332,1875^A0R,39,38^FH\\^FDvial^FS\n" +
                              "^FT373,1875^A0R,39,38^FH\\^FDvial^FS\n" +
                              "^FT418,1875^A0R,34,40^FH\\^FDvial^FS\n" +
                              "^FT604,1640^A0R,28,28^FH\\^FDSN^FS\n" +
                              "^FT656,1640^A0R,28,28^FH\\^FDDO^FS\n" +
                              "^FT705,1638^A0R,28,28^FH\\^FDPKG DATE^FS\n" +
                              "^FT755,1636^A0R,28,28^FH\\^FDGTIN^FS\n" +
                              "^FT79,469^A0R,56,55^FH\\^FD{18}^FS\n" +
                              "^FT178,469^A0R,51,50^FH\\^FD{17}^FS\n" +
                              "^FT252,469^A0R,56,55^FH\\^FD{16}^FS\n" +
                              "^FT334,1687^A0R,39,38^FH\\^FD{15}^FS\n" +
                              "^FT372,1687^A0R,39,38^FH\\^FD{12}^FS\n" +
                              "^FT414,1687^A0R,39,38^FH\\^FD{9}^FS\n" +
                              "^FT328,469^A0R,39,38^FH\\^FD{13}^FS\n" +
                              "^FT365,469^A0R,39,38^FH\\^FD{10}^FS\n" +
                              "^FT319,842^A0R,39,38^FH\\^FDExp Date ({14})^FS\n" +
                              "^FT364,840^A0R,39,38^FH\\^FDExp Date ({11})^FS\n" +
                              "^FT406,842^A0R,39,38^FH\\^FDExp Date ({8})^FS\n" +
                              "^FT404,469^A0R,39,38^FH\\^FD{7}^FS\n" +
                              "^FT554,469^A0R,56,55^FH\\^FD{1} / {2}^FS\n" +
                              "^FT645,469^A0R,56,55^FH\\^FD{0}^FS\n" +
                              "^FT90,429^A0R,39,38^FH\\^FD:^FS\n" +
                              "^FT180,429^A0R,39,38^FH\\^FD:^FS\n" +
                              "^FT263,429^A0R,39,38^FH\\^FD:^FS\n" +
                              "^FT472,429^A0R,39,38^FH\\^FD:^FS\n" +
                              "^FT560,429^A0R,39,38^FH\\^FD:^FS\n" +
                              "^FT654,429^A0R,39,38^FH\\^FD:^FS\n" +
                              "^BY162,162^FT570,1384^BXR,9,200,0,0,1,~\n" +
                // "^FH\\^FD01{3}13" + createdtime.Replace("/", "") + "400{5}21{6}^FS\n" +
                                "^FH\\^FD" + dapatGSoneMasterbox + "^FS\n" +
                              "^FT610,1789^A0R,28,28^FH\\^FD{6}^FS\n" +
                              "^FT607,1773^A0R,28,28^FH\\^FD:^FS\n" +
                              "^FT657,1790^A0R,28,28^FH\\^FD{5}^FS\n" +
                              "^FT656,1774^A0R,28,28^FH\\^FD:^FS\n" +
                              "^FT706,1788^A0R,28,28^FH\\^FD{4}^FS\n" +
                              "^FT705,1772^A0R,28,28^FH\\^FD:^FS\n" +
                              "^FT755,1786^A0R,28,28^FH\\^FD{3}^FS\n" +
                              "^FT755,1770^A0R,28,28^FH\\^FD:^FS\n" +
                              "^PQ1,0,1,Y^XZ\n" +
                              "^XA^ID000.GRF^FS^XZ\n" +
                              "^XA^ID001.GRF^FS^XZ\n" +
                              "^XA^ID002.GRF^FS^XZ\n" +
                              "^XA^ID003.GRF^FS^XZ\n" +
                              "^XA^ID004.GRF^FS^XZ\n" +
                              "^XA^ID005.GRF^FS^XZ\n" +
                              "^XA^ID006.GRF^FS^XZ\n" +
                              "^XA^ID007.GRF^FS^XZ\n" +
                              "^XA^ID008.GRF^FS^XZ\n" +
                              "^XA^ID009.GRF^FS^XZ\n" +
                              "^XA^ID010.GRF^FS^XZ\n" +
                              "^XA^ID011.GRF^FS^XZ\n" +
                              "^XA^ID012.GRF^FS^XZ\n" +
                              "^XA^ID013.GRF^FS^XZ\n" +
                              "^XA^ID014.GRF^FS^XZ\n" +
                              "^XA^ID015.GRF^FS^XZ\n" +
                              "^XA^ID016.GRF^FS^XZ\n" +
                              "^XA^ID017.GRF^FS^XZ\n" +
                              "^XA^ID018.GRF^FS^XZ\n" +
                              "^XA^ID019.GRF^FS^XZ\n" +
                              "^XA^ID020.GRF^FS^XZ\n" +
                              "^XA^ID021.GRF^FS^XZ\n" +
                              "^XA^ID022.GRF^FS^XZ\n" +
                              "^XA^ID023.GRF^FS^XZ\n" +
                              "^XA^ID024.GRF^FS^XZ\n" +
                              "^XA^ID025.GRF^FS^XZ\n" +
                              "^XA^ID026.GRF^FS^XZ";
            String ZplStringtest = String.Format(kirimBetul, productname, colie, colieMax, gtin, createdtime, 
                shippingOrderNumber, serial, batchnumber1, expdate1, qty1, batchnumber2, expdate2, qty2,
                batchnumber3, expdate3, qty3, qtytotal, weight, adminid);

            // String ZplString = String.Format(kirimBaru, gtin, batch, expiry, serial, weight, qtytotal, productname, colie,shippingOrderNumber,createdtime);

            return ZplStringtest;
        }
        
        string dataKirimAlamatKiri(string costumername, string costumerAddress, string costumerAdress2, string zipcode, string contactName, string contactPhone, string printer)
        {
            string[] address = costumerAddress.Split('-');
            string kirimFix = "CT~~CD,~CC^~CT~\n" +
            "~DG000.GRF,25600,040,\n" +
            ",:::::::::::::::::::::::::::::::::::::::::::::::::::::::::W01FFC0,W0JF8,V03FJF,V0MF8,U01FLFE,U03FMFA0,U0PFC,U0PFE80,U0RF0,T01FQFE,T03FRFC0,T03FSF8,T07FTF80,T0VFC0,:T0JFH0PFE0,S01FHFC0H0PF0,S01FHF80801FNF8,S01FHFK01FMFC,S01FHF80J03FLFC,S01FHFL03FLFE,S03FFE0K03FLFE,S03FHFL03FMF,S03FFE0K03FMF80,S03FFE0K03FFE1FIFC0,S03FFE0K03FFE01FHFE0,S03FHFL03FHFH01FFE0,S03FFE0K03FHFI03FE0,S03FHFL03FHFJ03F0,S01FFE0K03FHFK0B8,S01FHFL03FHF0,S01FHF80J03FHF0,::T0IFC0J03FHF0,:T0IFE0J03FHF0,T07FHFK03FHF0,T07FHFK03FFE0,T03FHF80I03FFE0,T03FHFC0I03FFE0,T01FHFE0I07FFE0,T01FIF80H07FFE0,U0JF80H0IFE0,U0JFE001FHFC0,U0KF801FHFC0,U07FJF0FIFC0,U03FOF80,U01FOF80,U08FOFH8,V07FNF,V03FMFE,V01FMFC,W0NFH80,W07FLF8,W03FKFE0,X0LFC0,X03FIFB80,Y0JFD,Y03FHF8,g03FC0,,:S0380,S03F8,S03FB80,S03FHF8,S03FHFE,S03FIFE0,S03FJFA,S03FKFC0,S03FLFC,S03FMFC0,S03FNF8,S03FOF,S03FPF0,S03FPFE,T03FPFA0,U07FPF0,V0BFOF0,W0PF0,U0800FNF01F,X01FMF03FC0,Y0NF07FC0,g0MF07FE0,gG0LF0FHF0,gG01FJF0FHF8,gH02FIF0FHF8,gI03FHF07FF8,gJ03FF07FF8,gK07F07FF8,gL0603FF8,gN01FF8,gO0HF8,W03FFC0M07F0,V01FIF80L0380,V07FIFE,V0LF80,U03FKFC0,U07FLF8,U0NF8,U0NFE,T01FNF,T03FNF80,:T07FMFBE8,T07FNFE0,T0JFEFKF8,T0JFH0KF8,T0IFC00BFIF8,S01FHF80H0JFC,S01FHF80H07FHFC,S01FHFJ01FHFE,S03FFE0I08FHFA,S03FHFK07FHF,S03FFE0J03FHF80,:S03FFE0J01FHF80,S03FFE0J01FHFC0,S03FFE0K0IFC0,S03FFE0K07FFC0,:S01FHFL07FFE0,S01FHF80J07FFE0,S01FHFL07FFE0,S01FHF80J03FFE0,:T0IF80J03FFE0,T0IFC0J03FHF0,T0IF80J03FHF8,T07FFE0J03FHF0,T07FHFK03FHF0,:T03FHF80I03FFE0,T03FHFC0I03FFE0,T01FHFE0I07FFE0,T01FIF80H07FFE0,U0JF80H0IFC0,U0KFH01FHFC0,U07FIFE03FHFC0,U03FOF80,:U01FOF80,V0PF,V07FMFE,V03BFLFE,V01FMFC,W07FLF8,M0C0N03FLF0,M0780N0LFE0,M07FC0M07FJF80,M03FF80M0JFE,M01FHFN07FHFC,N0IFA80L0BFE8,N0KF0,N07FIFE,N03FJFE0,N01FKF8,N01FLFC0,O0MFE0,O0NFC,O07FMFA8,O03FNFC,O03FOF80,O01FPF0,P0QFE,P07FPF,Q0QFE0,Q01FPF0,R02BFNF8,S03FNFC,S03FNFE,S03FOF,S03FOF80,S03FOFC0,S03FFE0FKFE0,S01FFE01FKF0,S01FFE003FBFHF8,T0IFI07FIF8,T0HFE0I0JFC,T07FE0I07FHFC,T03FE0I03FHFE,T01FE0I01FIF,U0FE0J0JF,U0FE0J07FHF80,U0FE0J03FHF80,U07F0J01FHF80,U03E0K0IF80,U01E0K0IFC0,V0E0K0IFC0,V0E0K07FFC0,V060K07FFE0,V020K07FFE0,V020K03FFE0,gI03FFE0,:::::::::gI07FFE0,:gI0IFC0,gH01FHFC0,gH03FHFC0,gH01FHF80,gI0IF80,:gI07FF,gI03FE,gI03FC,gI01FC,W03FFC0H0F8,V01BFHF800F8,V07FIFE0070,V0LF8020,U03FKFE0,U07FLF8,U0NF8,U0NFE,T01FMFE,T03FMFB88,T03FNF80,T07FNFE0,:T0JFABFJF8,T0JFH07FIF8,T0IFC003FIF8,S01FHFC0H07FHFC,S01FHF80H03FHFC,S01FHFJ01FHFE,S01FFE80I0IFE,S03FFE0J07FHF,S03FFE0J03FHF80,:S03FFE0J01FHF80,S03FFE0J01FHFC0,S03FFE0K0IFC0,:S03FFE0K07FFC0,S03FFE0K07FFE0,:S03FFE0K03FFE0,:::S03FFE0K03FHF0,:::S03FFE0K03FFE0,:S03FFE0K07FFE0,S03FHF8080H07FFE8,S03FIF80I07FFE0,S03FJF80H0IFC0,S03FKFH01FHFC0,S03FKFE0FIF80,S03FQF80,:S03FQF,:S03FPFE,S03FPFC,T0QFC,U0PF8,U01FNF0,V03FLFE0,W01FKF80,X0LF80,X01FIFC,Y02FFE8,,::S03E0,S03FC,S03FFC008,S03FHF8,S03FHFE80,S03FJF0,S03FKF80,S03FKFE0,S03FLF8,S03FMF,S03FMFC0,S03FMFE0,S03FMFB8,S03FNFC,T0BFMFA,U0OF,V0NF80,V01FLFC0,W03FKFE0,X01FJFE0,Y03FJF8,g0KF8,g03FIFC,gG0JFC,gG03FHFE,gG01FIF,gH0JF08,gH07FHF,gH03FHF80,:gI0IFC0,::gI07FFC0,:gI07FFE0,:gI03FFE0,:gI03FHF0,:::::::gI07FFE0,::gI0IFC8,gH01FHFC0,S0380L01FHFC0,S03F0M0IF80,S03FE0L0IF80,S03FFE0K07FF80,S03FHFE0J07FF,S03FIFC0I03FE,S03FJF80H09F808,S03FKF8001F8,S03FIFEFE0H0F8,S03FLFC0060,S03FMF8020,S03FMFC0,S03FNF0,S03FNF8,:T07FMFE,U0OF80,V07FLF80,V03FLF80,W03FKFC0,X0BFJFE0,Y07FJF0,W0808FJF8,g03FIF8,gG0JFC,gG07FHFE,gG03FHFE,gG01FHFE,gH0JF,gH07FHF80,gH03FHF80,gH01FHF80,gI0IFC0,::gI07FFE0,::gI0BFFE8,gI03FFE0,:gI03FHF0,::S020N03FHF0,S03C0M03FFE0,S03F80L03FFE8,S03FF80K03FFE0,S03FHF80J07FFE0,S03FIFK07FFE0,S03FJF80H0IFE0,S03FKFH01FHFC0,S03FLFBFIFC0,S03FQF80,S03FLFBFIFH8,S03FQF80,S03FQF,:S03FPFE,S03FPFC,T0BFOF80X02,U07FNFg02,U08FMFE80X0B,V01FLFC0Y03,W03FKFE0Y03,X01FKFg0180,Y03FJF80X0180,g07FIF80X0180,g01FIFC0X0180,gG07FHFC0X01C0,gG03FHFE0Y0C0,gG01FIFg0E0,gH0JFg0E0,gH07FHFg0E0,gH03FHF80X0F0,gH03FHF80X070,gH01FHFC0X070,gI0IFC0X078,gI0IFC80W078,gI07FFE0X07C,gI07FFE0X07C0W020,gI07FFE0X07C0W040,gI03FFE0X03E0V0180,gI03FFE0X03E0V03,gI03FFE0X03E0V06,gI03FFE0X03F0K0H4O0C,gI03FFE80W03F80J0840M018,gI03FFE0X03F80I01040M030,gI03FFE0X03F80I03040M020,S03C0M03FFE0X03F80I07040M0E0,S03F80L03FFE0X03FC0I0E840L01E0,S03FF0L03FFE0X01FC0H01E060L03C0,S03FFE0K03FFE0X03FE0H03C060L0F80,S03FHFE0J07FFE0X03FF0H078060K01D,S03FHFB880H07FFE0X03FF800F80E0K01A,S03FJF80H0IFC0X03FFC01F0070K03E,S03FKFH03FHFC0X03FHF8FF0070K0FC,S03FQFC0X07FJFE00780I01F8,S03FQF80X07FJFE00780I03F8,S03FQF80X07FJFC00780I07E0,S03FQF80X0LF8007C0H01FE0,S03FQFU0C0I0LF8007C0H01FC0,S03FPFE080R0F8803FKF800FE0H07F80,S03FPFC0T01FC0FLFI07F001FF,T0BFOF80U0PFI07F80FHF,U0PF80S0403FMFE0H07FJFE,V0NFA80S0600FMFE0H07FJFC,V01FLFC0T06007FLFC0H07FJFC,W02FKFA0T030H0KFEF0I07FJF8,X05FIFE0U030H0MFJ07FJF0,W080BFBF80U038003BFIFE0I07FJF0,g03FC0V01C0H0KFE0I07FIFE0,h01E0H03FIFC0I03FIFE0,h01E0I0JF80I03FIFE0,h01E0I03FFE80I03FIFE0,h01F0J07FC0J03FIFE0,hG0F80J0A0K03FIFE0,hG0F80Q03FIFE0,:h01FC0Q01FIFE0,h01FE0Q01FJF0,W01FF80X01FE0R0KF0,W0JF80M080N01FE0R0KF8,V03FIFC0M0780M01FF0R07FIFE,V0LF80L02F80L03FF80Q03FJF80,U01FKFE0M01FC0K07FF80Q03FJFE0,U03FLF80M0IF80I0IF80R0BFJF8,U0NF80M01FHFC007FHFC0R01FKFE0,U0NFC0N03FNFC0T03FIFE0,T01FMFE0O07FMFE,T03FNF80O0NFE,T03FNF80O01FLFE,T03FNF80O03FLFE,T07FNFC0P07FLFgG08,T0KFBFIFE80O01FLFg038,T0JF81FJFR07FKFg060,T0IFE003FIF80Q0LFY01E0,S01FHFC001FIFC0Q0LFY07C0,S01FHF80H07FHFC0Q0BFJFX01F80,S01FHFJ03FHFE0Q01FIFE0W03F,S01FFE80H01FHFE0R0JFE0W0FE,S01FFE0J0IFE0R07FHFC0V01FC,S03FFE80I03FFB0R0BFHF80V07F8,S03FHFK03FHF80Q07FHF80V0HF8,S03FFE0J03FHF80Q07FHF80U03FF0,S03FFE0J01FHF80Q07FFE0V07FF0,S03FFE0K0IFC0Q07FFC0V0BFE0,S03FFE0K07FFC0Q07FF80U01FFE0,S03FFE0K07FFE0Q0IFW03FFE0,S03FFE0K07FFE0Q0HFC0V0IFE0,S03FFE0K0BFFE80P0HF80V0IFE0,S03FFE0K07FFE0P01FF0V01FHFE0,S03FFE0K03FFE0P03FA0V03FIF0,S03FFE0K03FFE0P07F0W03FIF0,S03FFE0K03FFE0P0FE0W07FIF8,S03FFE0K03FHFQ0FC0W07FIFC,S03FFE0K03FHFP01F0X07FIFE,S03FFE0K03FHFP03C0X07FJF80,S03FFE0K03FHF80N03080W0LFC0,S03FHFL03FFE0O0C0Y07FKF0,S03FFE0K03FFE0N0180Y07FKF8,S03FFE0K03FFE0gP07FLF,S03FHF80J07FFE0gP07FLFE0,S03FIFK07FFE0gP07FMF8,S03FIFE0I0IFE0gP03FMFE,S03FJFE001FHFC0N03FKF80S03FNFC0,S03FJFBE03FHFC80O0KFB80R01FHFE00BFHF8,S03FQF80P01FKFS01FHFJ01FFE,S03FQF80Q03FJFC0R0HFE0K02FC0,S03FQF80R0KFE0R0HFC0L01F8,S03FQFT0KFE0R0HF80M0BC,S03FPFE0S03FIFB0R07F80N03,S03FPFE0S01FJF80Q07F8,S01FPFC0T0KF80Q07F0,S08BFOF80T0KF80Q03F8,U07FNFV07FIFC0Q01F0,V0NFE0U07FIFC0Q01F8,W07FKFC0U07FIFC0R0F0,X0LF80U07FIFE0J03FE0J0F8,X01FIFC0V07FIFE0J07FF80I070,Y03FHF80V07FIFE0J0IFE0I078,hG07FIFE0I01FIFC0H038,gI080V0KFE0I03FIFE80038,hG0KFE0I07FJF80018,h01FJFE0I0LFE0018,h01FJFE0H01FLF801C,h03FJFE0H01FLFE00C,h07FJFE0H03FMFH06,h07FJFE0H03FMFC06,h0HFE03FE0H07FMFE02,h0HF800FE008FLF8BB80,gY01FF0H07E0H0LFC003E0,gY03F80H03E001FKF80H030,gY03F0I03F003FKFJ018,gY07E0I01F003FJFE,gY0FC0I01F007FJFE,gY0F80J0E00FKFE,gX01F0K0E00FE0FHFC,gX03E0K0E01F803FFC,gX07C0K0601F001FFC,gX0F80K0603E001FFC,gW01E0L060380H0HFC,gW03C0L020780H07FC,gW0380L02040I03FC,gW060M02060I01FC,gW0C0M02080I01FC,gV0180M02880J0FC,gV030N02180J0FC,gV030O010K0FC,gV060V07C,gV080V07E,gU010W03E,hT03E,:hM080K03A,hT01E,hU0E,hU0F,:hU07,::hU0380,::hU0180,:hU01C0,hV0C0,::hV040,,::::::::::::::::::~DG001.GRF,01792,008,\n" +
            ",:::::::::::K017FC0,K03E2F0,K07405C,K0E0H0E,J01C0H07,J0183F8180,J0307FC1C0,J030E0F0C0,J031C070C0,J061801860,J061800C60,J060800C60,J060400C60,J060600860,J061FC5060,J061FHF060,J06197F440,J021803CC0,J031C005C0,J030C00180,J0187007,J01C380E,K0C1D5C,K060FE8,M0140,,::K0K1,K01FIFE0,K01DFDFC0,,::::K015H54,K01FHFC,K015574,N018,N01C,O0C,::N01C,N038,K01FHF0,K01FFE0,,:O0C,:K015H5D,K01FIFC0,K015H5DE0,O0C60,:P060,L0154040,L03FE0,L07570,L0C018,K01C01C,K01800C,::K01C01C,L0C018,L07570,L03FE0,L01540,,::::::::::L0K40,K01FIFE0,:,::::K015H54,K01FHFC,K015574,N018,N01C,O0C,::N01C,K01FHF8,K01FHF0,N030,N018,O08,O0C,::N01C,K017H78,K01FHF0,L04540,,::L05DDC,L0IFC,K01D554,K01C0,:K0180,:L080,L040,L068,K01FHFC,:L04040,,::K01DHDC,K01FHFC,K015574,N018,O0C,:::N01C,N038,K01FHF0,K01FFE0,L04040,,::K01DHDC40,K01FHFC60,K015H5H40,,::L071F0,L0E1F8,K01C39C,K01838C,K01830C,K01870C,:K018618,L0DE38,L0FC30,L054,,:L038,L0FC30,L0HE38,K01C71C,K01830C,::K01C30C,L0C18C,L075DC,L0IF8,K01FHF0,K01,,:L01050,L070F0,L0D1DC,L0C38C,K01830C,:K01870C,K01860C,K01C61C,L0HE38,L07C10,L038,,:::::::::::::::::::~DG002.GRF,01792,008,\n" +
            ",::::::::::::::::::::::::::::K0K1,K01FIFE0,K01DIDC0,,::::K015H54,K01FHFC,K015574,N018,N01C,O0C,::N01C,N038,K01FHF0,K01FFE0,,:O0C,:K015H5D,K01FIFC0,K015H5DE0,O0C60,:P060,L0154040,L03FE0,L07570,L0C018,K01C01C,K01800C,::K01C01C,L0C018,L07570,L03FE0,L01540,,::::::::::L0K40,K01FIFE0,:,::::K015H54,K01FHFC,K015574,N018,N01C,O0C,::L0405C,K01FHF8,K01FHF0,M0230,N018,O08,O0C,::N01C,K0177F8,K01FHF0,L0I40,,::L05DDC,L0IFC,K01D554,K01C0,K0180,::L080,L040,L068,K01FHFC,:M0I4,,::K01DHDC,K01FHFC,K015574,N018,O0C,:::N01C,N038,K01FHF0,K01FFE0,M0H40,,::K01DHDC40,K01FHFC60,K015H5H40,,::L071F0,L0E1F8,K01C3DC,K01838C,K01830C,K01870C,:K018618,L0DE38,L0FC30,L054,,:L038,L0FC30,L0HE38,K01C71C,K01830C,::K01C30C,L0C18C,L075DC,L0IF8,K01FHF0,K01,,:L01050,L070F0,L0D1DC,L0C38C,K01830C,:K01870C,K01860C,K01C61C,L0HE38,L07C10,L038,,:L0404H40,K01FHFC60,:,:::::::::::::::::::::::::~DG003.GRF,15104,008,\n" +
            ",:::::L038,L078,L0E0,K01C0,K0180,::K01C0,L0EAEAE0,L07FHFE0,M0BFFE0,,:::K01DIDC0,K01FIFE0,K015I540,,::K01,K0180,:L080,,:::::::::::K015I540,K01FIFE0,K0155D560,M018060,::::::M01C060,:N0C1C0,N0FBC0,N07FC0,O0E,,:L054,L0FC30,K01D638,K018718,K01830C,::L0830C,L0C18C,L0619C,K017FF8,K01FHF0,K014H40,,:L02020,L071F0,L0E1F8,K01C39C,K01830C,K01870C,::K018618,L0CE38,L0FC20,L054,,O04,O0C,L0I5D40,L0JFC0,K01D55D40,K01800C,:,:M0F80,L07FF0,L0FB70,L0C358,K01C30C,K01830C,:::K01C318,L0E338,L063F0,L023C0,,::L03FFC,L07FFC,L0E0,K01C0,K0180,::L0C0,:K017554,K01FHFC,K01DHDC,,::L0J4,K01FHFC,:N038,N01C,O0C,::O04,,::::::::K015I540,K01FIFE0,K015I5C0,O03C0,O07,N01E,N07C,N0E0,M01C0,M0780,L01F,L03C,L070,K01F2H2A0,K01FIFE0,L0KE0,,::M0F80,L07FF0,L07AF0,L0C05C,K01800C,::::L0C018,L0F078,L05FD0,M0F80,,:::K0180,:K01,,::::::::::K014004,K01E007,K01F007C0,K01B800C0,K019C0040,K018C0060,K01870060,:K0183C060,K0181C0E0,K0181F1C0,K01807F80,K01801F,L080,,:L014,L03F8E,L0H7DFC0,L0C0FBC0,K01C07040,K01806060,::K01C06060,L0C0F0C0,L0D1DFC0,L07F8F80,L05D04,,:::::::::::L0K80,K01FIFE0,:K0180C060,:::::::K01C1E0C0,L0E1FFC0,L07F1F,L03E0A,M04,,:L038,L0FC30,L0HE38,K01C71C,K01830C,::K01C30C,L0C18C,L075DC,L0IF8,K01FHF0,K01,,::K01FHFC,:L0H8F8,N010,O08,O0C,:::L0I5C,K01FHF8,K01FFD0,,::L01540,L03FE0,L07570,L0E018,K01C01C,K01800C,::K01C01C,L0C018,L0757440,K01FIFE0,:,:::L03FFC,L07FFC,L0E8H8,K01C0,K0180,::K01C0,L0C0,L07554,K01FHFC,:,:::K01FHFC,:L0H8F8,N010,O08,O0C,:::L0I5C,K01FHF8,K01FFD0,,::K041540,J01C3FE0,J03C7570,J030C038,J071C01C,J061800C,::J071C01C,J030C018,J01C7574,K0JFC,K05FHFC,,::::::::::M06,::::::,::::::::M04,M0E,M0D,M0DC0,M0C70,M0C30,M0C1C,M0C0E,M0C07,M0C01C0,K01FIFC0,K01FIFE0,L01C10,M0C,M04,,:M02A0,L05FFD,L07EEF80,L0F001C0,L0C0H0E0,K01C0H060,K0180H060,::L0C001C0,L0F803C0,L07FHF,M0HFC,N040,,:::O0C,:O06,O07,O0380,K015H57C0,K01FIFE0,K015I540,,:::::M03F8,L05FFD,L07FCF80,L0D041C0,L0C060C0,K01C07060,K01803060,::K01C07040,L0C060C0,L075C3C0,L03F83,M05,,:::O0C,:O06,O07,O0380,K015H57C0,K01FIFE0,K015I540,,::::::::::::::K015I540,K01FIFE0,K0155D560,M018060,::::::::N0C1C0,N0FBC0,N07F80,N01E,O04,,M0150,M07F8,L01FFD,L0380F80,L0700180,L0E0H0C0,L0C0H0C0,K0180H060,:::::L0C0H0C0,L0C001C0,L0E00180,L07417,L03FFE,M07FC,N0E0,,::::::::::L0K80,K01FIFE0,:K01C0C060,K0180C060,::::::K01C1E0C0,L0E1FBC0,L07F1F,L03E0A,M04,,::M07FC,L01FFE,L07D17,L0600380,L0C001C0,L0C0H0C0,K01C0H060,K0180H060,::::L0C0H0C0,:L07001C0,L0780780,L05DDF,M0HFC,M0H50,,:K0180,K01C0H060,L0E0H0E0,L07001C0,L03C0780,L01F17,M073E,M05FC,M01E0,M01F0,M07BC,L01F1C,L03C0780,L07001C0,L0E0H0E0,K01C0H060,K0180H020,K01,,::::::::::O04,O0E,O06,O07,O03,L0IABC0,K01FIFC0,L0EAEAE0,,:::::::O04,O0E,O06,O07,O03,L0IABC0,K01FIFC0,L0EAEAE0,,::::::L03803,L07803C0,L0E0H0C0,K01C0H040,K0180H060,K01806060,:K01807060,L08070C0,L0C1DDC0,L07B8F80,L05F04,M0C,,:M05D4,L03FFE,L075D7,L0E0E1C0,K01C06040,K01803060,:::L08030E0,L0C061C0,L07BC380,L05FC3,M0E,,::::::::P060,::::::K01FIFE0,:L0J460,P060,::::P040,,L01D40,L03FE0,L0I70,L0E318,K01C31C,K01830C,::K01C30C,L0C318,L0C370,L063E0,L041C0,,::K01DIDC0,K01FIFE0,K015I540,,::K01,K0180,:L080,,:::::::::::M0180,::::L07FFE,:M0180,::::M01,,N040,M0HF8,L01FHF,L07F8F80,L0C041C0,L0C060C0,K01803060,:::K01C07060,L0E060C0,L075C3C0,L03F8380,L015,,:K0180,K01C007,K01F00780,K01F801C0,K01B800E0,K019C0060,K018E0060,K01870060,K01838060,K0181C060,K0180E0C0,K01807FC0,K01803F80,K010H04,,M04,M06,::::::M04,,K014004,K01E007,K01F007C0,K01F800C0,K019C0040,K018E0060,K01860060,K01870060,K01838060,K0181C0E0,K0181F1C0,K01807F80,K01801F,,::K014004,K01E007,K01F007C0,K01F800C0,K019C0040,K018E0060,K01860060,K01870060,K01838060,K0181C0E0,K0181F1C0,K01807F80,K01801F,,::M06,:::::M04,,:K0180,K01C007,K01F00780,K01F001C0,K01B800E0,K019C0060,K018E0060,K01870060,K01838060,K0181C040,K0180E0C0,K01807FC0,K01803F,K010H04,,:M02A0,L05FFD,L07EEF80,L0F001C0,L0C0H0E0,K0180H060,:::K01C001C0,L0F803C0,L07FHF,M0HFE,M0H40,,::L05803,L0780380,L0C001C0,L080H0E0,K01804060,K01806060,::K01C070C0,L0C0DFC0,L075DF,L03F82,M04,,::L05803,L0780380,L0C001C0,L080H0E0,K01804060,K01806060,::K01C070C0,L0C0DFC0,L075DF,L03F82,M04,,:P020,P060,:K010I060,K01E0H060,K01FD0060,M0FC060,M01F060,N03C60,N01F60,O07E0,O01E0,P0E0,P040,,::L05854,L0787F80,L0C077E0,K018010E0,K01801860,:::K01C01060,L0C03060,L0757060,L03FC020,M05,,::L05854,L0787F80,L0C077E0,K018010E0,K01801860,:::K01C01060,L0C03060,L0757060,L03FC020,M05,,::::::::::K015I540,K01FIFE0,K0155D560,N0C060,::::::::N04060,P020,,:L054,L0FC30,K01D638,K018718,K01830C,::L0830C,L0C18C,L0619C,K017FF8,K01FHF0,K014H40,,K01,K018004,K01C01C,L0F038,L07D70,M0FC0,M07C0,M0FC0,L05C70,L0F038,K01C01C,K01800C,K01,,::K0180,:K01,,::::::::::M01,M0180,::::L0H7F6,L07FFE,L045C4,M0180,:::N080,,:L015D4,L03FFE,L075D7,L0E0E1C0,K01C06040,K01803060,:::L08030E0,L0C061C0,L07BC380,L05FC3,M0A,,:K014004,K01E007,K01F007C0,K01B800C0,K019C0040,K018C0060,K01870060,:K0185C060,K0181C0E0,K0181F1C0,K01807F80,K01805F,L080,,:M04,M06,:::::,:K0180,K01C007,K01F00780,K01F801C0,K01B800E0,K019C0060,K018E0060,K01870060,K01838060,K0181C060,K0180E0C0,K01807FC0,K01803F80,K010H04,,:K0180,K01C007,K01F00780,K01F801C0,K01B800E0,K019C0060,K018E0060,K01870060,K01838060,K0181C060,K0180E0C0,K01807FC0,K01803F80,K010H04,,M04,M06,::::::M04,,K014004,K01E007,K01F007C0,K01F800C0,K019C0040,K018E0060,K01860060,K01870060,K01838060,K0181C0E0,K0181F1C0,K01807F80,K01801F,,::M0H50,L03FFE,L07D5FC0,L0E001C0,K01C0H0C0,K0180H060,::K01C0H060,L0C0H0C0,L07445C0,L03FHF80,L017F5,,::M0F,:M0DC0,M0CE0,M0C70,M0C1C,M0C05,M0C0380,K0H1D11C0,K01FIFE0,K01DFDFC0,M0C,:M08,,:::O0C,O0E,O07,:O0380,L0IEFC0,K01FIFE0,L0KA0,,:::::L010,L03803,L07803C0,L0E0H0C0,K01C0H040,K0180H060,K01806060,:K01807060,L08070C0,L0C1DDC0,L07BCF80,L07F05,M0A,,:M0H50,L03FFE,L07D5FC0,L0E001C0,K01C0H0C0,K0180H060,::K01C0H060,L0C0H0C0,L07445C0,L03FHF80,L017F5,,::L01554,L03FFE,L075D7C0,L0E0C1C0,K01C06040,K01803060,:::K018030E0,L0C061C0,L0FBC3C0,L07FC3,M02,,:::::::::::K01FIFE0,:K010101,,:::K01FHFC,:L0H8F8,N010,O08,O0C,:::K01455C,K01FHF8,K01FFD0,,::L01540,L03FE0,L07570,L0E018,K01C01C,K01800C,::K01C01C,L0C018,K01757540,K01FIFE0,K01FHF7E0,,::L01540,L03FE0,L07570,L0C018,K01C01C,K01800C,::K01C01C,L0E018,L07570,L07FE0,L01740,,::K015H54,K01FHFC,K015574,N018,N01C,O0C,::N01C,N038,K01FHF0,K01FFE0,,::M0F80,L07FF0,L0FB70,L0C35C,K01C30C,K01830C,:::K01C318,L0E338,L063F0,L023C0,,:L05040,L070F8,L0F1D8,L0C38C,K01C30C,K01830C,K01870C,:K01C61C,L0CE38,L07C30,L038,,::K01FHFC60,:,::L054,L0FC30,K01D638,K018718,K01830C,::L0830C,L0C18C,L0619C,K017FF8,K01FHF0,K014H40,,:::J02180,J01D80,,::::::::::::K015H54,K01FHFC,K015574,N018,N01C,O0C,::L0405C,K01FHF8,K01FHF0,M0230,N018,O08,O0C,::N01C,K017H78,K01FHF0,L0I40,,:L038,L0FC30,L0HE38,K01C71C,K01830C,::K01C30C,L0C18C,L075DC,L0IF8,K01FHF0,K01,,::K01FHFC60,:,::L0K40,K01FIFE0,:,::L015,L0HF80,K01D570,K07801C,K040H07,J01C0H03,J0185F4180,J030FFE0C0,J031D170C0,J0318038C0,J061801C40,J061800C60,:J060C00C60,J060401C60,J060F83860,J061FF7060,J0618FFCE0,J071805CC0,J03080H0C0,J030C00180,J0186003,J01C5447,K0E0EFC,K0607F0,K02,,::L0KA0,K01FIFE0,L0E8FAE0,L04010,L08008,K01800C,:::L0C05C,L0F8F8,L07FF0,M0F80,M04,,:K01FHFC60,:,::L01540,L03FE0,L07570,L0C018,K01C01C,K01800C,::K01C01C,L0C018,L07570,L03FE0,L01540,,O04,O0C,K015H5D40,K01FIFC0,K015H5DE0,O0C60,:O0860,L0540040,L0FE30,K01D618,K018708,K01830C,::L0830C,L0C11C,L0619C,K01FHFC,K01FHF8,K0150,,::K0177F4,K01FHFC,L0H454,O08,O0C,:O04,,:L0IA8,K01FHFC,L0HEF8,N010,O08,O0C,::N01C,K01DDFC,K01FHF8,K015570,N018,O0C,:::L0H45C,K01FHF8,K01FHF0,,::L054,L0FE30,K01D618,K018708,K01830C,::L0830C,L0C11C,L0619C,K01FHFC,K01FHF8,K0150,,::K01,K0180,:L080,,::M0A80,L07FF0,L0FAF8,L0C01C,K01800C,::K01C00C,L0C018,L07070,L03860,L01040,,:M0F80,L05FD0,L0F8F8,L0C018,K01800C,::::L0C05C,L0F8F8,L07FF0,M0F80,,:::K0180,:K01,,:::K01FHFC60,:,::L01540,L03FE0,L07570,L0E018,K01C01C,K01800C,::K01C01C,L0C018,K01757540,K01FIFE0,K01DIDC0,,::::::::::M04,M06,:::::,::::::::O04,N03C,M05FC,L03F80,K01F4,K01E0,K01F4,L03F80,M05FC,N03C,M017C,L03FE0,K01FD40,K01E0,K01F5,L01F80,M05FC,N03C,N014,,N05C,M03F8,L05FC0,L0FA,K01C0,L0F8,L05F50,M01F8,N07C,N0FC,L057D0,K01FA,K01D0,L0FC,L05F50,M03F8,N05C,O08,O04,N03C,M05FC,L01FC0,K0175,K01E0,K01FC,L02F80,M05F4,N03C,M05FC,L01FE0,K01FD,K01E0,K01FC40,M0FC0,M05F4,N03C,O04,,::K0180,:K01,,:::L0KA0,K01FIFE0,L0E8FAE0,L04010,L08008,K01800C,:::L0C05C,L0F8F8,L07FF0,M0F80,M04,,:K01FHFC60,:,::L01540,L03FE0,L07570,L0C018,K01C01C,K01800C,::K01C01C,L0C018,L07570,L03FE0,L01540,,O04,O0C,K015H5D40,K01FIFC0,K015H5DE0,O0C60,:O0860,L0540040,L0FE30,K01D618,K018708,K01830C,::L0830C,L0C19C,L0619C,K01FHFC,K01FHF8,K0151,,::K017H74,K01FHFC,L0H454,O08,O0C,:O04,,:L0IA8,K01FHFC,L0AEF8,N010,O08,O0C,::N01C,K01DDFC,K01FHF8,K015570,N018,O0C,:::L0H45C,K01FHF8,K01FHF0,,::L054,L0FE30,K01D618,K018708,K01830C,::L0830C,L0C19C,L0619C,K01FHFC,K01FHF8,K0151,,::K01,K0180,:L080,,::M0A80,L07FF0,L0FAF8,L0C05C,K01800C,::K01C00C,L0C018,L07078,L03860,L01040,,:M0F80,L05FD0,L0F8F8,L0C018,K01800C,::::L0C05C,L0F8F8,L07FF0,M0F80,,:::K0180,:K01,,:::K01FHFC60,:,::L01540,L03FE0,L07570,L0E018,K01C01C,K01800C,::K01C01C,L0C018,K01757540,K01FIFE0,K01DIDC0,,:::::::::::::::::::::::::::::::::~DG004.GRF,02816,008,\n" +
            ",::::::::::::L0H7F770,L0KF0,:L0AEFEF0,N0F0F0,:::::N0FDF0,N07FE0,:N03FC0,N01540,,::P0F0,:::::L0J5F0,L0KF0,:L0JEF0,P0F0,::::P0A0,,:::::::::L0K70,L0KF0,:L0FAFAF0,L0F0F0F0,::::::L0F9F9F0,L07FDFE0,:L05F87C0,M0E,,:N040,L0EAEA60,L0IFE70,:L0IDC50,,::M05C0,L01FF0,L03FF8,L07FFC,L0745C,L0E00E,::::L0701C,L07EFC,L05FFC,L01FF0,M07C0,,::::::::::L0575H50,L0KF0,:L0HEFEF0,N0F0F0,:::::::N050F0,P0A0,,:L01410,L07E18,L07F1C,L0HF3C,L0F71E,L0E38E,:L0638E,L071DE,L03FFE,L0IFC,:L0F5D0,L080,,::L0IFE,::L057FC,N01C,O0C,O0E,:O06,O04,,L0I76,L0IFE,:L0EAF8,N01C,O0C,O0E,:N01E,L0IFE,L0IFC,L0IF8,L0H578,N018,O0C,O0E,::L0IDE,L0IFC,:L0HFE0,,::L038,L07F1C,L0HF3C,L0F73C,L0E39E,L0E38E,:L0618E,L0718E,L07DDE,L0IFC,:L0IF0,L04040,,:::::::::M0280,K017FFC,K03FIF80,J017FIFC0,J03F8003F0,J0340I070,J020,,:L0404H40,L0KF0,::L0H5F5F0,N0F0F0,:::::N0F9F0,N07FF0,N07FE0,N01FC0,O0F,,:M0540,L03FF0,L07FFC,:L0739C,L0E38E,:::L0E39E,L073FC,L063F8,L063F0,M0380,,::L0IFE,::L057FC,N01C,O0C,O0E,:O06,O04,L01860,L079FC,:L071DC,L0E3CE,:L0E38E,L0E78E,L0E79E,L0H79C,L07F1C,L05F10,M08,,:M05C0,L01FF0,L07FFC,:L0F3DC,L0E38E,:::L0E39E,L073FC,L063F8,L063F0,M02C0,,::L0IFE,::L0H5FC,N01C,O0E,:::,M0380,L01FF0,L03FF8,L07FFC,L0783C,L0F01E,L0E00E,::L0F01E,L0781C,L07D7C,L03FF8,L01FF0,M0FC0,,:P050,J0380I0F0,J03F5015F0,K0KFE0,K05FIF40,L0IFC,L01550,,:::::::::::::::::::::::::::::~DG005.GRF,00512,008,\n" +
            ",::::::::::::::::::::::::::K0404040404,,::::L07FIF0,L0KFC,K05FHFDFC04,K01FJFE,K01FJF5,K01FKF,K01FFC05D,K01FC001E,K01F80015,K01F0H03F,K05F465DD04,K01F0E1FE,K01F1E1F5,K01E1E1FF,K01E1E1D5,K01E1E1FE,K01F1E1F5,K01F1E3FF,K05FDF5DD04,K01FJFE,K01FJF5,K01FJFE,K01FIFDC,L0KFC,L07FIF0,,K0404040404,,:::~DG006.GRF,00512,008,\n" +
            ",::::::::::::::K0808080808,,:::::L0KFC,K08FJFC08,L0KFC,::::::K08FF0FFC08,L0HF0FFC,:P07C,P03C,P01C,L0HF0E1C,L0HF0F1C,K08FF0F1C08,L0HFEF1C,L0KFC,::,::K0808080808,,:::::::::::::::^XA\n" +
            "^PW879\n" +
            "^FT544,2816^XG000.GRF,1,1^FS\n" +
            "^FT32,2816^XG001.GRF,1,1^FS\n" +
            "^FT32,2560^XG002.GRF,1,1^FS\n" +
            "^FT32,2336^XG003.GRF,1,1^FS\n" +
            "^FT32,480^XG004.GRF,1,1^FS\n" +
            "^FT32,2592^XG005.GRF,1,1^FS\n" +
            "^FT32,2368^XG006.GRF,1,1^FS\n" +
            "^FO28,117^GB829,2680,8^FS\n" +
            "^FT722,170^A0R,113,112^FH\\^FDKEPADA :^FS\n" +
            "^FT115,170^A0R,124,122^FH\\^FDUP. {0} ({1})^FS\n" +
            "^FT236,170^A0R,124,122^FH\\^FD{4}^FS\n" +
            "^FT358,445^A0R,124,122^FH\\^FD       {2}^FS\n" +
            "^FT480,170^A0R,124,122^FH\\^FD{5}^FS\n" +
            "^FT358,170^A0R,124,122^FH\\^FDD/A {3}^FS\n" +
            "^FT600,170^A0R,124,122^FH\\^FDGUDANG VAKSIN^FS\n" +
            "^PQ1,0,1,Y^XZ\n" +
            "^XA^ID000.GRF^FS^XZ\n" +
            "^XA^ID001.GRF^FS^XZ\n" +
            "^XA^ID002.GRF^FS^XZ\n" +
            "^XA^ID003.GRF^FS^XZ\n" +
            "^XA^ID004.GRF^FS^XZ\n" +
            "^XA^ID005.GRF^FS^XZ\n" +
            "^XA^ID006.GRF^FS^XZ";


            string zplstring = string.Format(kirimFix, contactName, contactPhone, address[0], zipcode, address[1].Replace(" ", ""), costumername);
            return zplstring;
        }

        string dataKirimAlamatKanan(string costumername, string costumerAddress, string costumerAdress2, string zipcode, string contactName, string contactPhone, string printer)
        {
            string[] address = costumerAddress.Split('-');
            string kirimSent = "CT~~CD,~CC^~CT~\n" +
                                "~DG000.GRF,21888,036,\n" +
                                ",:::::::::X020,:X030,:X010,X018,::X01C,Y0C,::Y0E,Y0E80,Y07,::Y0780,:Y0380,Y03C0,Y02C0L080,Y03E0,:Y03E0W08,Y03F0V030,Y01F0V060,Y03F80J040O0E0,Y01F80J0C0O0C0,Y01F80J0820M0280,Y01FC0I01820M03,Y01FC0I03020M0E,Y01FC0I07020M0C,Y01FE0I0E020L01C,Y01FF0H01E070L038,Y01FF8003C030L0F0,Y01FFC007C070K01F0,Y01FFE00E80380J02E0,Y01FHF83F80780J07C0,Y03FKFH0380J0F80,Y03FJFE00780I03F,Y03FJFE00780I0FE,T080I07FJFC007C0I0FE,T060I0LF8003E0H03FC,T03C001FKF8007E0H07FC,T08EE8FLF800BE800FF8,S0203FNFI07FC03FF0,S0201FMFE0H03FJFE0,S02007FLFE0H03FJFE0,S01803FLFC0H03FJFC0,S01800FLF80H03FJFC0,S018003FKF80H03FJF80,S01C001FKFJ03FJF,S01E0H07FIFE0I03FJFX080,S01E0H01FIFC0I03FJF,T0F0I03FHF80I03FIFE0V02FHF8,T0F0I01FHFK03FIFE0V07FIFC0,T0F80I03F80J03FIFE0V0KFE8,T0F80Q01FIFE0U03FLF,T0FC0Q01FIFE0U07FLFE0,T0FC0Q01FIFE0U0OFC,T0FE0R0JFE0T01FNFHE0,T0FE0R0KFU03FPF8,T0HFS0KF80S07FPFC,I060N01FF0R05FIFC0S07FPFC,I01E80L01FF80Q07FJFT0RFC,J0FC0L01FF80Q07FJF80Q01FQFC,J01FA0K03FF80Q03FJFE0Q01FQFC,K03FFC0I07FFC0R0LFC0P01FQFC,L0FEFE803FHFC0R03EFHFEF80O0BFFEE03FKFC,L01FNFE0S01FKFC0N01FHF8007FJFC,M03FMFE0gP03FHFJ0KFC,N0OFgQ03FFE0J0JFC,N03FMFgQ03FFE0K0IFC,O07FLFgQ03FFC0K07FFC,O01FLFgG080N03FFC0K07FFC,P07FKFg070O07FFC0K07FFC,P01FKF80W08E80N0IFC0K07FFC,Q0LFY01C0O07FFC0K07FFC,Q03FJFY0F80O07FFC0K07FFC,Q01FJFX01F0P07FFC0K07FFC,R0KFX0BF0P0HFEC0K07FFC,R0JFE0W0FE0P07FFC0K07FFC,R07FHFE0V03FC0P03FFC0K07FFC,R07FHFC0V07FC0P07FFC0K07FFC,R03FHF80V0HF80P03FFE0K07FFC,R03FHFW03FF0Q03FFE0K07FFC,R03FFE0V0IFR03FHFL07FFC,R07FFC0V0IFR01FHFL07FFC,R03FE80U02FFE0Q01FHF80J07FFC,R07FF0V03FFE0Q01FHF80J07FFC,R0HFE0V0IFE0Q01FHFC0J07FFC,R0HF80V0JFR01FHFE0J07FFC,R0FE0V02FHFE80Q0IFE0J07FFC,Q01FC0V03FIFS07FHFK07FFC,Q03F80V03FIF80Q07FHF80I0IF8,Q07E0W07FIFC0Q07FHFC0I0IF8,Q0F80W06FIFE0Q03FHFE0I0IF8,P01F0X07FJFR01FIF8003FHF8,P03C0X07FJF80P01FIFE007FHF0,P070Y07FJFE0Q0KFC1FIF0,P0E080W07FKF80P07FIFEFIFE0,gQ07FLFQ03FNFE0,gQ03FLFC0O03FNFE0,gQ07FLFC0O01FNFC0,gQ03FMF80O0OFC0,gQ03FNFP07FMF80,P03FIFE0T03FNFA0N03FMF80,P07FKFC0R01FIFH07FHF80M01FMF,P080FKF80R0IF80I0HFE080L0MFE80,R03FJFC0R0IFL07FC0M03FKF8,S0KFE0R0HFE0L03F80L01FKF0,S03FJFS07FC0M01E0M07FIFC0,S01FJF80Q03FC0X0JF80,T0KF80Q03F80X01FF8,T0KFC0Q03F80,T07FIFC0Q01F80,T03FIFE0M080H09F80,T03FIFE0R0F80,T03FIFE0K0280J0F80,T03FIFE0J03FE0J0780,T03FIFE0J0IFE0I0780,T07FIFE0J0JF80H0780,T07FIFE0I01FIFE0H0380,T07FJFJ03FJFI01C0V05FC0,T0KFE0I03FJFC001C0T082FHFE0080,T0LFJ07FKFI0C0U07FIFC0,S01FKFJ0MF800E0T03FKFA,S01FKFI01FMFH060T07FLFC0,S01FKFI03FMF8020T0NFE8,S03FKFI03FMFE030S01FOF80,S07FF81FF0H07FJFEFHF80T03FOFE0,S07FC007F0H07FKF01F80T07FPFC,S0HFI0BF0H0LFC0H0E0S087FPFC,R01FC0H03F0H0LF80H030T0RFC,R03F80H01F002FKF80W01FQFC,R03F0J0F003FKFY01FQFC,R0FE0J0F003FJFE0X01FQFC,R0FC0J0F007FJFE0X01FQFC,Q01F80J0700FF0FHFE0X03FHFC00FKFC,Q03E0K0700FC03FFE0X01FHFJ0KFC,Q03C0K0380F880FFE0X03FFE80H01EFHFC,Q05C0K0301F0H0HFC0X03FFE0J07FHFC,Q0F0L0303C0H07FE0X03FFE0K0IFC,P01E0L030380H03FC0X07FFC0L0HFC,P03C0L030B0I03FC0X07FFC0M0FC,P0780L01060I01FC0X07FFC0M01C,P060M01040J0FE0X0IFC,P0E0M01040J0FE0X07FFC,O0180M01080J0FE0X0IFC,O010N0H1L07E0X07FFC,O020V03E0X07FFC,O060V03E0X07FFC,O0C0V03E0X03FFE,N010W01E0X03FFE,N020W01E0X03FFE,gM01F0X03FHF,gN0F0X03FHF80,gN0F0X01FHF80,:gN070X01FHFC0,gN0780W01FHFE0,gN0380W01FHFE0,gN0380X0JF0,gN0380X0JF8,gN0380X07FHFC80,gN0180X03FIF,gO080X03FIF80,gO080X01FIFE0,gO080Y0KFA,gO080Y0LFC0,gO040Y03FKF8,gO040Y03FLF80,gO040Y0NFE0,gO040Y0PF,gO060X01FOFE0,gO060X03FPFC,hO07FPFC,:hO0RFC,hN01FQFC,hN09FFEFNFC,hN01FQFC,hN03FIFBFLFC,hN01FHFC00FKFC,hN03FHF80H0KFC,hN03FFE0I01FIFC,hN03FFE0J01FHFC,hN07FFC0K01FFC,hN0BFFC0L03FC,hN07FFC0M03C,hN07FFC,:hN0IFC,hN07FFC,hN03FFC,hN07FFC,hN03FFC80,hN03FFE,::hN03FHF,hN01FHF,hN03FHF80,hN01FHFC0,hN01FHFE0,:hO0IFE0,hO07FHF0,hO07FHF8,hO07FHFE,hO03FIF80,hO01FIFC0,hP0JFE880,hP0LF,hP07FJFE0,hP03FKFC,hP01FLFH8,hP01FMF0,hQ0NFE,hQ07FMFC0,hP082FNFC,hQ01FNFC,hR07FMFC,hR01FMFC,hP0200FMFC,hP06003FLFC,hP0F0H0HFBFIFC,hO01F8001FKFC,hN083F88002FJFC,hO07FC0I03FIFC,hO0HFE0J03FHFC,hN01FHFL07FFC,hN01FHFM0HFC,hN01FHF80L07C,hN03FHFC0M0C,hN01FHFC0,hN03FHF80,hN03FHF,hN03FFE,hN03FFC,hN07FFC,:::hN0IFC,hN07FFC,::hN03FFC,hN07FFC,hN03FFE,:hN03FFE80,hN01FHF,hN03FHF80,hN01FHF80,:hN01FHFC0,hN01FHFE0,hO0IFE0,hO0JF8,hO07FHF8,hO03FHFE,hO03FIF,hO03FIF80,hO01FJF0,hP0KFE,hP07FJFC0,hP03FKFH80,hP03FLF80,hP01FMF0,hQ07FMF80,hQ07FMFE8,hQ01FNFC,hR0OFC,hR07FMFC,hR01FMFC,hS0NFC,hS03FLFC,hT07FKFC,hU0LFC,hV0KFC,hV01BFHFC,hW01FHFC,hX0BFFC,hY03FC,i03C,,::hR0IF8,hQ07FIF,hQ0JFHE8,hP01FKFC,hP03FLF80,hP07FMF8,hP0PF80,hO03FPF0,hO03FPFC,hO07FPFC,hO0RFC,hN01FQFC,::hN03FHFE8FLFC,hN01FHFC007FJFC,hN03FHF80H0KFC,hN03FHFJ01FIFC,hN0BFFE80H082FHFC,hN03FFC0K07FFC,:hN07FFC0K07FFC,hN0IFC0K07FFC,hN07FFC0K07FFC,::hN0BFFC0K07FFC,hN07FFC0K07FFC,hN03FFC0K07FFC,hN07FFC0K07FFC,hN03FFE0K07FFC,:hN03FHFL07FFC,hN01FHFL07FFC,hN03FHF80J07FFC,hN01FHF80J07FFC,hN01FHFC0J07FFC,:hO0IFE0J07FFC,hO0JFK07FFC,hO07FHF80I0IF8,:hO03FHFE0I0IF8,hO03FIFI01FHF8,hO01FIF8003FHF8,hO01FIFE00FIF0,hP0KFEBFIF0,hP07FNFE0,hP03FNFE0,hP01FNFC0,hQ0OFC0,hQ07FMFC0,hQ03FMF80,hQ01FMF,hR0MFE,hR03FKFC,hR01FJFB0,hP06007FIFE0,hP0E0H0JF80,hO01F0H03FFC,hO03F8,hO07FC,hO0HFE,:hN01FHF,hN01FHF80,hN09FFE80,hN01FHFC0,hN03FHF80,hN03FHF,hN03FFE,::hN07FFC,hN0IFC,hN07FFC,::::hN03FFC,hN07FFC,hN0BFFC0K040,hN03FFE0K040,hN03FFE0K060,hN01FFE0K070,hN03FHFL078,hN01FHFL078,hN03FHF80J07C,hN01FHFC0J07E,hN01FFEE0J07E80,hN01FHFE0J07F,hO0IFE0J07F80,hO0JF80I07FC0,hO07FHFC0I07FC0,hO03FHFE0I07FE0,hO03FIF80H07FE0,hO01FIFE0H07FF0,hP0KFH807FF8,hP0LFC07FFC,hP07FKF87FFC,hP03FOFC,hP01FOFC,:hQ0PFC,hQ03FNFC,hR0PF80,hR07FOF8,hR03FPF80,hS0QFE0,hS03FPF0,hT0QF8,hT02FOF8,hU01FNFC,hV03EFLFE,hW07FMF,hX07FLF80,hX01FLF80,hY03FKF80,i07FJFC0,iG03FIFE0,iH0KF0,hR03EE0L08FIF0,hQ01FHFC0M0IF8,hQ0BFIF80L03FFC,hP01FJFC0M01FC,hP03FKF80M03E,hP07FKFC0N03,hP0MFE,hO03FMF80,hO03FMFC0,hO07FMFE0,hO0PF0,hN01FOF8,:hN01FOFC,hN03FHFE03FIFE,hN01FHF800FJF,hN03FFE0H03FHFE80,hN03FFE0I0JF80,hN03FFE0I03FHFC0,hN07FFC0I03FHFC0,hN07FFC0I01FHFC0,hN07FFC0I01FHFE0,hN07FFC0J0IFE0,hN07FFC0J07FFE0,hN0IFC0J03FFE0,hN07FFC0J03FHF0,hN03FFC0J01FHF8,hN07FFC0J01FHF8,hN03FFC0K0IF8,hN03FFE0K0IF8,:hN03FFE0K07FFC,hN0BFHF80J0IFC,hN01FHFL07FFC,hN03FHF80J07FFC,hN01FHF80J07FFC,hN01FHFC0J07FFC,:hN01FHFE0J07FFC,hO0JFK07FFC,hO07FHF80I0IF8,hO07FHFC0I0IF8,hO03FHFE0I0IF8,hO03FIFI01FHF8,hO01FHFEC003FHF8,hO01FJFH07FHF0,hP0LFBFIF0,hP07FNFE0,hP0BFNFE0,hP01FNFC0,hQ0OFC0,hQ07FMFC0,hQ03FMF80,hQ01FMF,hR0MFE,hR01FKFC,hR08FKF8,hS07FIFE0,hJ0380M0JF80,hJ07E0M03FFC,hJ0HF80,hJ0HFC0,hJ0HFE020,hJ0HFE07E,hJ0HFE0FEE080,hI01FFE07FFC,hJ0HFE07FHF80,hJ0IF07FIF8,hJ0HFE0FJFE80,hJ07FE07FKF0,hJ03FE07FKFE,hJ01FE07FLFC0,hK0F80FNF8,hN07FNF,hN07FNFE0,hN07FOFE,hN03FPFE0,hO07FPFC,hP0QFC,hP01FOFC,hR0FEFLFC,hR01FMFC,hS03FLFC,hT03FKFC,hU03FJFC,hV07FIFC,hW0JFC,hX0IFC,hY0EFC,hY017C,i03C,,:hR01FC0,hR0IFC,hQ07FIF80,hP09FJFE0,hP03FKF0,hP0MFC,hO01FLFE,hO03FMF80,hO07FMFC0,hO07FMFE0,hO0OFE0,hO0PF8,hN01FOF8,hN01FOFC,hN01FIF07FIFE,hN03FHFC00FIFE,hN01FHF8007FIF,hN03FHFI03FIF80,hN07FFE0I0JF80,hN03FFE0I03FHFC0,hN07FFC0I03FHFC0,hN03FFC0I01FHFC0,hN07FFC0J0IFE0,hN0IFC0J0IFE0,hN07FFC0J07FHF0,hN0IFC0J03FHF0,hN07FFC0J03FHF0,hN0IFC0K0IF8,hN07FFC0J01FHF8,hN0IFC0K0IF8,hG010K07FFC0K0IF8,hG01E0J0IFC0K07FF8,hH0FE0I07FFC0K07FFC,hH0HFE0H0IFC0K07FFC,hH07FFC007FFC0K07FFC,hH03FHF80BFFC0K07FFC,hH01FIF07FFC0K07FFC,hI0NFC0K07FFC,::hI03FLFC0K0IF8,hI03FLFE0K0IF8,hI01FMFC0J0IF8,hJ0OF80081FHF8,hJ07FNF8003FHF8,hJ03FOFH0JF0,hJ03FUF0,hJ03FTFE0,hJ01FTFE0,hK03FSFE0,hL03FRFC0,hL08BFEFOFC0,hN07FPF80,hO0QF,hO07FOF,hP07FMFE,hQ07FLF8,hR0MF0,hS0KFC0,hR080FIF8080,hT01FF8,,::::::::::::::::::::::::::::::~DG001.GRF,01792,008,\n" +
                                ",:::::::::N063FHF8,:N0K20,,:Q01C0,P083E0,O01C770,O038638,O030618,O030E18,O030C18,:O031C30,O03B8B0,P0F0E0,P0A080,,:S08,P0IF8,O01FHF0,O03BAE0,O031830,O030C38,O030C18,::O038E38,O01C770,P0C3F0,Q01C0,,:Q02A0,P0C3F0,O01C7B0,O018618,O030E18,:O030C18,O031C18,O039C38,O01F870,P0F8E0,,::N0H2IA8,N063FHF8,N023BHB8,,::P02020,P07FF8,P0IF8,O01C0,O0380,O03,:::O0180,O02EAA8,O03FHF8,O03BHB8,,::P02020,O03FHF8,:Q0160,R020,R010,R018,:R038,:O02AAB8,O03FHF0,O03BBA0,,::P02A20,P0IF8,O01EHE8,O0380,O03,::O01,O0180,P0C0,P0IF8,O01FHF8,O0380,O03,::O0380,O0180,O02EAA8,O03FHF8,O02AHA8,,::::N07FIF8,:N0K20,,::::::::::P02A80,P07FC0,P0EAE0,O018030,O038038,O030018,::O038038,O018030,P0EAE0,P07FC0,N0202A80,N060,N063,:N07BAHA8,N03FIF8,O0BAHA8,O03,:,:P07FF8,P0IF8,O01C0,O0380,O03,::O0380,O0180,O02EAA8,O03FHF8,O02AHA8,,::::N03FBFB8,N07FIF8,O0K8,,::P028,O017F06,O03AB83,O0701C380O0E00E180N0180030C0N03A0038C0N0H3C01840N0H2FE9860N060FHF860N060A3F860N061006060N063002060N063001060N063001860N061801860N030E038C0N030F070C0N0383FE0C0N0181FC180O0E0H0380O070H07,O03A02E,P0F47C,P03FE8,,::::::::::::::::~DG002.GRF,01536,008,\n" +
                                ",::::::::::::::::::::N063FHF8,:N0I2020,,:Q01C0,P083E0,O01C770,O038638,O030618,O030E18,O030C18,:O031C30,O03B8B0,P0F0E0,P0A080,,:S08,P0IF8,O01FHF0,O03BAE0,O031830,O030C38,O030C18,::O038E38,O01C770,P0C3F0,Q01C0,,:Q02A0,P0C3F0,O01C7B0,O018618,O030E18,:O030C18,O031C18,O03BC38,O01F870,P0F8E0,,::N0H2IA8,N063FHF8,N023BHB8,,::P0H2,P07FF8,P0IF8,O01C0,O0380,O03,:::O0180,O02EAA8,O03FHF8,O03BHB8,,::O0I2,O03FHF8,:Q0160,R020,R010,R018,::R038,O02AAB8,O03FHF0,O03BBA0,,::P0I20,P0IF8,O01FEE8,O0380,O03,::O01,O0180,P0C4,P0IF8,O01FHF8,O03A020,O03,::O0380,O0180,O02EAA8,O03FHF8,O02AHA8,,::::N07FIF8,:N0K20,,::::::::::P02A80,P07FC0,P0EAE0,O018030,O038038,O030018,::O038038,O018030,P0EAE0,P07FC0,N0202A80,N060,N063,:N07BAHA8,N03FIF8,O0BAHA8,O03,:,:P07FF8,P0IF8,O01C0,O0380,O03,::O0380,O0180,O02EAA8,O03FHF8,O02AHA8,,::::N03BIB8,N07FIF8,O0K8,,:~DG003.GRF,15104,008,\n" +
                                ",::::::::::::::::::::::::::::N03BIB8,N07FIF8,N02AEAE8,O018030,O038038,O030018,::O038038,O018070,P0EAE0,P07FC0,P02A80,,::N063FHF8,:,:::S08,R018,:,:::P01F,P0HFE0,O01F1F0,O03A030,O030018,::::O018030,O01F1F0,P0BFA0,P01F,,:P02080,P061C0,O01E0E0,O018030,O030038,O030018,::O03A030,O01F5F0,P0HFE0,P015,,::R010,R018,:S08,,::Q08A8,O01FHF8,O03FHF8,O039860,O039830,O030C10,O030C18,::O010E18,O0186B8,P0C7F0,Q02A0,,::P0IF8,O01FHF8,O03A220,O03,:::O0180,P0EAA8,O01FHF8,O03FBB8,O0380,O03,::O01,P080,O01F750,O03FHF8,O015H50,,:O02,O03,:O01,O02A220,O03FHF8,O02EHE8,,::Q08A8,O01FHF8,O03FHF8,O039860,O039830,O030C10,O030C18,::O010E18,O0186B8,P0C7F0,N02002A0,N061,N063,:N07BAHA8,N03FIF8,N02BAHA8,O03,O02,,P02A80,P07FC0,P0EAE0,O018030,O038038,O030018,::O038038,O018030,P0EAE0,P07FC0,P02A80,,::N063FHF8,:,:Q02,P01F,P0HFE0,O01F1F0,O03A030,O030018,:::O010010,P08020,N075F170,N07FIF8,N0K50,,:::S08,R018,:,::O02,O03C0,O02FA,P03F,P023F8,R078,Q0BF8,P07F80,O03FA,O03C0,O02FA,P01F40,Q03F8,R078,Q0AE8,P03F80,O03FA,O03C0,O02,O01,O03A0,O01FC,P0AFA0,Q03F0,R0B8,Q05F8,P0BEA0,O03F0,O03E0,O01F8,P0AFA0,Q01F0,R038,Q05F0,P03FA0,O01FC,O03A0,,O0280,O03C0,O03FA,P01F80,Q0AF8,R078,P02BF8,P07FC0,O03E8,O03C0,O03FA,P01FC0,Q02F8,R078,Q02F8,P01FC0,O03FA,O03C0,O02,,::::::::Q06,:::::Q02,,::::::::::N03BIB8,N07FIF8,N02AEAE8,O018030,O038038,O030018,::O038038,O018070,P0EAE0,P07FC0,P02A80,,::N063FHF8,:,:::S08,R018,:,:::P01F,P0HFE0,O01F1F0,O03A030,O030018,::::O018030,O01F1F0,P0BFA0,P01F,,:P02080,P061C0,P0E0E0,O018030,O030038,O030018,::O038030,O01F5F0,P0HFE0,P015,,::R010,R018,:S08,,::R0A8,O01FHF8,O03FHF8,O039860,O038830,O030C10,O030C18,::O010E18,O0186B8,P0C7F0,Q02A0,,::P0IF8,O01FHF8,O03A220,O03,:::O0180,P0EAA8,O01FHF8,O03FBB8,O0380,O03,::O01,P080,O01F770,O03FHF8,O015H50,,:O02,O03,:O01,O02A220,O03FHF8,O02FEE8,,::R0A8,O01FHF8,O03FHF8,O039860,O038830,O030C10,O030C18,::O010E18,O0186B8,P0C7F0,N02002A0,N061,N063,:N07BAHA8,N03FIF8,N02BAHA8,O03,O02,,P02A80,P07FC0,P0EAE0,O018030,O038038,O030018,::O038038,O018030,P0EAE0,P07FC0,P02A80,,::N063FHF8,:,:Q02,P01F,P0HFE0,O01F1F0,O03A030,O030018,:::O010010,P08020,N075F170,N07FIF8,N0K50,,::S04,P0FE06,O03F707,O0E22A380O0C006180N0180030C0N030H010C0N0H3A018E0N073FF1860N060EFF860N061C1F060N063802060N063003060N063001860:N023801860N031C018C0N030E8B8C0N0307FF0C0N0182FA180O0C0H0380O0E0H02,O03801E,P0EAB8,P01FF0,Q0A80,,::N07FIF8,:N0K20,,::N063FHF8,:,::S08,P0IF8,O01FHF0,O03BAE0,O031830,O030C38,O030C18,::O038E38,O01C770,P0C3F0,Q01C0,,:P0I20,P0IF8,O01EHE8,O0380,O03,::O01,O0180,P0C4,P0IF8,O01FHF8,O03A020,O03,::O0380,O0180,O02EAA8,O03FHF8,O02AHA8,,::::::::::::R01B80R01840,:::P0I28,P0IF8,O01FFE8,O039860,O031830,O030C10,O030C18,::O018E18,O01C6B8,P0C3F0,Q02A0,,::N063FHF8,:,::Q01C0,P0C3E0,O01C730,O038638,O030E18,:O030C18,O030C38,O031C30,O01B8F0,O01F0E0,P020A0,,:P03C40,P0FC60,O01CC70,O018C38,O030C18,:::O030C38,O03AC30,P0EDF0,P0HFE0,P01F,,::P07FF8,P0IF8,O01C0,O0380,O03,::O0380,O0180,O02EAA8,O03FHF8,O02AHA8,,::P02E80,P07FE0,P0EAE0,O018070,O038038,O030018,::O038038,O018030,P0EAE0,P07FC0,P02A80,,::N07EFHF8,N07FIF8,N02AEAE8,O018030,O038038,O030018,::O038038,O018070,P0EAE0,P07FC0,P02A80,,::P0BFF8,O01FHF8,O03AA28,O03,:::O01,P080,O01F110,O03FHF8,:,:::O080808,N07FIF8,:,:::::::::::Q04,O0C3FE0,N03C3DF0,N0386030,N070C018,N060C018,:::N0206038,N0383070,N03EBAE0,O07FFC0,O02AA80,,::O0AFE80,N01FHFC0,N03A22E0,N030H030,N060H038,N060H018,::N030H038,N0380070,N03FABE0,O07FFC0,P0HA,,:Q05,O0A0FE0,N01F3DE0,N03BB830,N030E010,N060E018,N0606018,:N060H018,N020H038,N030H070,N03C01E0,O0C01C0,R080,,:::::N0K50,N07FIF8,N03F7H70,N01C,O0E,:O07,O03,,:::Q01,Q03,:N03FBFB8,N07FIF8,N0388B88,N01C03,O0A03,O0383,P0E3,P073,P03B,Q0F,:,::O0AFE80,N01FHFC0,N03A22E0,N030H030,N060H038,N060H018,::N030H038,N0380070,N03FABE0,O07FFC0,P0HA,,::O0F8018,N01FE018,N038F818,N0703818,N0601C18,N0600E18,N0600618,N0600718,N0200398,N03001F8,N03E00F8,O0E0078,O020028,,Q02,Q06,::::::Q02,,O020H08,N01FC018,N03FE018,N0307018,N0603818,N0601C18,N0600E18,N0600718,N0600398,N07001D8,N03801F8,N01E00F8,O0E0038,R018,,:O020H08,N01FC018,N03FE018,N0307018,N0603818,N0601C18,N0600E18,N0600718,N0600398,N07001D8,N03801F8,N01E00F8,O0E0038,R018,,:Q06,:::::Q02,,:R010,O0FA018,N01FE018,N038F818,N0703818,N0603A18,N0600E18,:N0600318,N0200398,N03001D8,N03E00F8,O0E0078,O020028,,:Q05,O0C3FA0,N01C3DE0,N0386030,N070C010,N060C018,:::N0206038,N0387070,O0EBAE0,O07FFC0,O02BA80,,:P010,P018,:::O023A20,O07FFE0,O06FEE0,P018,::::Q08,,::::::::::S08,R018,:,::S08,O030018,O038038,O01C0F0,P0E3A0,P03F,P03E,P03F,P0EBE0,O01C0F0,O038038,O020018,S08,,P0I28,P0IF8,O01FFE8,O039860,O031830,O030C10,O030C18,::O018E18,O01C6B8,P0C3F0,Q02A0,,:N040,N06020,N06030,::::::::N06ABAA8,N07FIF8,N02AIA8,,::::::::::Q0A,N0403FC0,N060EAE0,N060C030,N0608038,N0618018,:::N0708018,N07EE030,N01FE1E0,O02A1A0,,::Q0A,N0403FC0,N060EAE0,N060C030,N0608038,N0618018,:::N0708018,N07EE030,N01FE1E0,O02A1A0,,::N020,N070,N078,N07E,N06F80,N063C0,N060F8,N0603F,N0600BF8,N060H078,N060I08,N060,:N040,,:Q02,O041FC0,O0FBAE0,N03FB030,N030E038,N0606018,::N0602018,N070H010,N0380030,N01C01E0,O0C01A0,,::Q02,O041FC0,O0FBAE0,N03FB030,N030E038,N0606018,::N0602018,N070H010,N0380030,N01C01E0,O0C01A0,,::P0H2,O07FF,O0IFE0,N03C01F0,N0380038,N060H018,:::N070H030,N03800F0,N01F77E0,O0BFFA0,P054,,:O020H08,O0FC018,N03FE018,N0307018,N0203818,N0601C18,N0600E18,N0600718,N0600398,N07001D8,N03800F8,N01E00F8,O0E0038,R018,,:Q02,Q06,:::::,::O0F8018,N01FE018,N038F818,N0703818,N0601C18,N0600E18,N0600618,N0600718,N0200398,N03001F8,N03E00F8,O0E0078,O020028,,::O0F8018,N01FE018,N038F818,N0703818,N0601C18,N0600E18,N0600618,N0600718,N0200398,N03001F8,N03E00F8,O0E0078,O020028,,Q02,Q06,::::::Q02,,O020H08,N01FC018,N03FE018,N0307018,N0603818,N0601C18,N0600E18,N0600718,N0600398,N07001D8,N03801F8,N01E00F8,O0E0038,R018,,:Q0A80,N01C1FC0,N03C3AE0,N0306070,N060E038,N060C018,:::N0306030,N0382030,N01F1FE0,O0IF80,O01FF,P020,,Q08,P018,::::O07FFE0,:P018,::::,:::::::::::R010,R018,:S08,,::N02AIA8,N07FIF8,N03BIB8,,::P03820,P07C60,P0EC30,O018C30,O030C38,O030C18,::O038C38,O018C70,P0IE0,P07FC0,P02B80,,N020,N060,::::N062I20,N07FIF8,:N060,::::::,::::::::Q07,O0C3FA0,N01C3DE0,N0386030,N070C010,N060C018,:::N0206038,N0387070,O0EBAE0,O07FFC0,O02BA,,:Q03,O020FA0,N01F1DE0,N03BB830,N030E010,N060E018,N0606018,:N060H018,N020H038,N030H070,N03C01E0,O0C01C0,,::::::N0757570,N03FIF8,N03D5H50,O0C,O0E,O06,O07,O02,,:::::::N0757570,N03FIF8,N03D5H50,O0C,O0E,O06,O07,O02,,::::::::::S08,N040H018,N060H038,N070H070,N03800E0,N01E03C0,O038F80,O03DE,P0F8,P078,O03FA,O07CE,O0E8F80,N01E03C0,N03800E0,N070H070,N060H038,R018,,:P0HA,O03FF,O0FBBA0,N01E01E0,N03800E0,N030H030,:N060H018,::::N060H038,N030H030,N0380030,N01C0060,O0E8BE0,O07FF80,O03FE,,::Q02,O0507C0,O0F8FE0,N03DF870,N0307838,N0603018,::::::N0603038,N07FIF8,:N0K10,,::::::::::P070,O03FE,O07FFC0,O0E82E0,N0180070,N0380030,N030H030,N060H018,:::::N030H030,N030H070,N01800E0,N01F01C0,O0BFF80,O01FE,P0A8,,O02,O0780,N01FE0,N03DF0,N03830,N06018,::::::::N06ABAA8,N07FIF8,N02AIA8,,::::::::::::::N02AIA8,N07FIF8,N03EAHA8,N01C,O0E,O06,O03,:,:::Q0A,O0C1FC0,N03C3AE0,N0306030,N020E038,N060C018,::N060E038,N0306030,N03820B0,N01F3FE0,O0BFFA0,O01FC,,:::::N02AIA8,N07FIF8,N03EAHA8,N01C,O0E,O06,O03,:,:::P020,O03FF,O0IFE0,N03C01F0,N0380030,N060H018,::N060H038,N070H030,N03800F0,N01F77E0,O0BFFA0,P054,,:Q02,Q03,P08380,N07FIF8,N03FIF8,N03803,O0E03,O0703,O0383,P0C3,P0E3,P03B,Q0B,Q07,Q02,,::::::::Q06,::::::,::::::::::O03FHFA,O03FIF,O02EAE380O018030C0O038038E0O03001860::O038038E0O01C030C0P0EAE3C0P07FC380P02A82,,::P0BFF8,O01FHF8,O03AHA0,O03,:::O01,P080,O01F110,O03FHF8,:,:::O03FHF8,:O02AAE0,R030,R038,R018,::R038,O0I170,O03FFE0,O03FFC0,,:::N07FIF8,:N0H2EAE0,O018030,O038038,O030018,::O038038,O018070,P0EAE0,P07FC0,P02A80,,::P0BFF8,O01FHF8,O03AHA0,O03,:::O01,P080,O01F110,O03FHF8,:,::S08,P0IF8,O01FHF0,O03BAE0,O031830,O030C38,O030C18,::O038E38,O01C770,P0C3F0,Q01C0,,:Q02,O0507C0,O0F8FE0,N03FF870,N0307838,N0603018,:::::::N07FIF8,:N0K10,,:::::::::::O020BA0,N01F1FE0,N03FB8B0,N030F030,N0606038,N0606018,::N020E038,N03DF030,N03FBEE0,O071FC0,Q0280,,:R010,O0F8018,N01FE018,N038F818,N0703818,N0603C18,N0600E18,:N0600318,N0200398,N03001D8,N03E00F8,O0E0078,O020028,,::::::::::S08,R018,:,:::P01F,P0BFA0,O01E0F0,O018030,O030018,::::O03A030,P0F5E0,P0HFE0,P01F,,::N0K70,N07FIF8,N054H4F8,R0E0,Q03C0,Q0F80,P01E,P038,P070,O03E0,O0780,O0E,N03C,N03AIA8,N07FIF8,N02AIA8,,::::::::O02,O03,::O0380,O01C0,O03FHF8,:O0J20,,::O03BHB8,O03FHF8,O02AAE8,R030,:R018,::R038,R070,O03FFE0,O03FFC0,,::P03C40,P0FC60,O01CC70,O018C38,O030C18,:::O030C38,O01AC30,P0EDF0,P0HFE0,P01F,,:O030018,:N02BAAB8,N03FIF0,N02BAHA0,O03,O02,,Q02A0,P043F0,O01C730,O018618,O030E18,::O030C18,O039C38,O01F870,P0F8E0,P04040,,:P0I28,P0IF8,O01FFE8,O039860,O031830,O030C10,O030C18,::O018E18,O01C6B8,P0C3F0,Q02A0,,:O07,N03FE0,N03DF0,N03830,N06038,:N06018,::::::N06ABAA8,N07FIF8,N02AIA8,,:::::::::::R010,R018,:S08,,::N02AIA8,N07FIF8,N03BIB8,,:::N07FFD,N07FHFE0,N0757570,R038,R018,::R038,R070,Q01E0,Q01C0,,::::::::::~DG004.GRF,02816,008,\n" +
                                ",::::::::::::::::::::::::P0HA80,O03FHF0,N02FIFA,N07FJF,N0FA80AFC0N0F0I01C0N0A0,,:P03F,P0HF80,O01FFC0,O03EBE0,O0381E0,O0780F0,O070070,::O0780F0,O03C1E0,O03FFE0,O01FFC0,P0HF80,P01C,,O07,:::O0380,O03FAA0,O07FHF0,::,::P034,P0FC60,O01FC60,O03FCE0,O079C70,O071C70,:::O03BCF0,O03FFE0,:P0HF80,P03A,,:Q01,P08FA0,O038FE0,O039EE0,O079E70,O071E70,O071C70,O073C70,:O03B8E0,O03F9E0,:P06180,O02,O06,O07,:O03,O0380,O03FEA0,O07FHF0,::,::P01C,P0FC60,O01FC60,O03FCE0,O079C70,O071C70,:::O039CE0,O03FFE0,:P0HFC0,P02A,,:O0F,N03F80,N07FE0,N0HFE0,N0F9F0,N0F0F0,:::::N0FAFAA0,N0KF0,::N0I2020,,:T040N0E0I02C0N0FC001FC0N03FIFE80N01FIFC,O03FFE8,P014,,:::::::::P02020,P0IF0,O03FHF0,:O07BBE0,O0718E0,O071860,O071C70,:O079C70,O03CEF0,O03CFF0,O038FE0,Q01C0,,::P07FF0,O03FHF0,:O07BHB0,O07,::O03,O0180,O01EAA0,O01FHF0,O03FHF0,O07FHF0,O0780,O07,:O03,O0380,O01F570,O07FHF0,:O06EHE0,,O02,O06,O07,:O03,O0380,O03FEA0,O07FHF0,::,::R010,P0BAF0,O03FHF0,:O07FFC0,O07B8E0,O071C60,O071C70,:O078EF0,O03CFF0,O038FE0,O0187E0,P08280,,:N050,N0F0A0,N0F0F0,:::::::N0F7F770,N0KF0,:N0IAEA0,,::::::::::P03E,P0HF80,O03FFA0,O03F7E0,O0380E0,O070070,::::O03A2E0,O03FFE0,O01FFC0,P0HF80,P03A,,::N0A3BHB0,N0E7FHF0,:N0657570,P020,,:Q07,N03E1FA0,N07FBFE0,:N0F9F9F0,N0F0F0F0,::::::N0F5F5F0,N0KF0,:N0KE0,,:::::::::N050,N0F0,::::N0F7I70,N0KF0,:N0FAIA0,N0F0,:::::,::N02A80,N03FC0,N07FE0,:N0FBF0,N0F0F0,:::::N0F7F750,N0KF0,:N0HEFEE0,,:::::::::::::::::~DG005.GRF,00512,008,\n" +
                                ",::::::::::::::::::::::::::::::K0101010101,,N07FHFE0,M03FJF0,M01DFIF8,M07FJF8,M057FIFC,M03FJF8,K0105DDFDFD,M07FC7878,M057C7C7C,M03FC7878,M0H5C7C7C,M07FC7878,M057C7C7C,M03FC7878,K0105DD71FD,M07E0H0F8,M054001FC,M03C003F8,M05D01FFC,M07FJF8,M057FIFC,M03FJF8,K0101FDFHF9,M03FJF0,N0JFE0,,::::K0101010101,~DG006.GRF,00512,008,\n" +
                                ",::::::::::K0101010101,,::M03FJF0,::M038F7FF0,K01038F0FF1,M038F0FF0,M03870FF0,M0380,M03C0,M03E0,M03FF0FF0,:K0103FF0FF1,M03FJF0,::::::K0103FJF1,M03FJF0,,:::::K0101010101,,:::::::::::::::::::^XA\n" +
                                "^PW879\n" +
                                "^FT32,704^XG000.GRF,1,1^FS\n" +
                                "^FT768,320^XG001.GRF,1,1^FS\n" +
                                "^FT768,544^XG002.GRF,1,1^FS\n" +
                                "^FT768,2464^XG003.GRF,1,1^FS\n" +
                                "^FT768,2784^XG004.GRF,1,1^FS\n" +
                                "^FT768,352^XG005.GRF,1,1^FS\n" +
                                "^FT768,608^XG006.GRF,1,1^FS\n" +
                                "^FO20,81^GB829,2709,8^FS\n" +
                                "^FT155,2737^A0B,113,112^FH\\^FDKEPADA :^FS\n" +
                                "^FT762,2737^A0B,124,122^FH\\^FDUP. {0} ({1})^FS\n" +
                                "^FT640,2737^A0B,124,122^FH\\^FD{4}^FS\n" +
                                "^FT518,2462^A0B,124,122^FH\\^FD       {2}^FS\n" +
                                "^FT396,2737^A0B,124,122^FH\\^FD{5}^FS\n" +
                                "^FT518,2737^A0B,124,122^FH\\^FDD/A{3}^FS\n" +
                                "^FT276,2737^A0B,124,122^FH\\^FDGUDANG VAKSIN^FS\n" +
                                "^PQ1,0,1,Y^XZ\n" +
                                "^XA^ID000.GRF^FS^XZ\n" +
                                "^XA^ID001.GRF^FS^XZ\n" +
                                "^XA^ID002.GRF^FS^XZ\n" +
                                "^XA^ID003.GRF^FS^XZ\n" +
                                "^XA^ID004.GRF^FS^XZ\n" +
                                "^XA^ID005.GRF^FS^XZ\n" +
                                "^XA^ID006.GRF^FS^XZ";

            string zplstring = string.Format(kirimSent, contactName, contactPhone, address[0], zipcode, address[1].Replace(" ", ""), costumername);
            return zplstring;
        }

        //BATASSSSS
        //public bool ValidasiBoxId(string masterbox) { 
        //    //cek data sudah beres belum 
        //    if (!cekMasterboxPickinglist(masterbox))
        //    {
        //        return false;
        //    }

        //    string Status = cekStatusMasterboxPickinglist(masterbox);
        //    //Status harus 0
        //    if (!Status.Equals("0")) {
        //        if (Status.Equals("1"))
        //        {
        //            new Confirm("Master Box GS1 ID not found", "Information", MessageBoxButtons.OK);
        //            log("Master Box GS1 ID not found");
        //        }
        //        else
        //        {
        //            new Confirm("Master Box GS1 ID not found", "Information", MessageBoxButtons.OK);
        //            log("Master Box GS1 ID not found");
        //        }
        //    }

        //    //cek ada masterbox nya 
        //    if (!getStatusMasterbox(masterbox))
        //    {
        //        return false;
        //    }            
        //    return true;
        //}

        public bool SendDataToPrint(string Admin, string masterbox, string weight)
        {
            log("Get Data Masterbox");
            GsOneScanned = "";            
            string dates = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            string[] dataMasterbox = getMasterbox(masterbox);
            string shipiingOrderNumber = getShippingOrderNumber(masterbox);
            string[] ShippingOrder = getShippingOrder(shipiingOrderNumber);
            List<string[]> batchnumberAndQty = getbatchnumber(masterbox);
            string qtytotal = getQtyTotal(masterbox);
            string packingSlipId = getPackingListId(shipiingOrderNumber);
            //string packingSlipId = shipiingOrderNumber;

            //log("Updae Weight data");
            Var_Masterbox temp_Masterbox = new Var_Masterbox();
            temp_Masterbox.MasterboxId = masterbox;
            temp_Masterbox.Timestamp = dates;
            temp_Masterbox.Weight = weight;
            temp_Masterbox.ShippingOrderNumber = shipiingOrderNumber;
            temp_Masterbox.CustomerId = ShippingOrder[0];
            temp_Masterbox.CustomerName = ShippingOrder[1];
            temp_Masterbox.CustomerAddress = ShippingOrder[2];
            temp_Masterbox.ContactName = ShippingOrder[3];
            temp_Masterbox.ContactPhone = ShippingOrder[4];
            temp_Masterbox.Postcode = ShippingOrder[5];
            temp_Masterbox.BatchAndQty = batchnumberAndQty;
            temp_Masterbox.Qtytotal = qtytotal;
            temp_Masterbox.CreatedTime = dataMasterbox[2];
            temp_Masterbox.Batchnumber = dataMasterbox[5];
            temp_Masterbox.ExpDate = dataMasterbox[6];
            //temp_Masterbox.SerialNumber = dataMasterbox[7];
            temp_Masterbox.Gtinmasterbox = dataMasterbox[8];
            temp_Masterbox.Qty = dataMasterbox[9];
            temp_Masterbox.Model_name = dataMasterbox[10];
            temp_Masterbox.ColieNumber = dataMasterbox[11];
            temp_Masterbox.ColieMax = ShippingOrder[7];
            temp_Masterbox.Status = "0";
            temp_Masterbox.PackingSlipId = packingSlipId.Replace("-", "");
            String CreatedTime = db.getUnixStringPkg(double.Parse(temp_Masterbox.CreatedTime));
            temp_Masterbox.CreatedTime = db.getUnixStringPkg(double.Parse(temp_Masterbox.CreatedTime));

            String ColieNumber = temp_Masterbox.ColieNumber.PadLeft(4, '0');
            int PanjangColi = 16 - temp_Masterbox.PackingSlipId.Length + ColieNumber.Length;
            temp_Masterbox.SerialNumber = ColieNumber.PadRight(PanjangColi, '0');
            temp_Masterbox.GsOneMasterBoxId = "01" + temp_Masterbox.Gtinmasterbox + "13" + CreatedTime.Replace("/", "") + "400" + temp_Masterbox.PackingSlipId + "21" + temp_Masterbox.SerialNumber;
            temp_Masterbox.GsOneMasterBoxIdPrint = "\\7E1" + "01" + temp_Masterbox.Gtinmasterbox + "13" + CreatedTime.Replace("/", "") + "400" + temp_Masterbox.PackingSlipId + "\\7E1" + "21" + temp_Masterbox.SerialNumber;            
            LIST_MASTERBOX.Add(temp_Masterbox);

            updateWeightDanSN(masterbox, weight, temp_Masterbox.SerialNumber);
            log("Updae Weight and serial number");

            GsOneScanned = temp_Masterbox.GsOneMasterBoxId;
            log(GsOneScanned + " Master Box GS1 ID generated");            
            log("Start Send To Prnter");
            Printer.Print(Admin, temp_Masterbox);
            log("Sent To Printer");
            return true;            
        }

        public void ScanGSOne(String GsOne) {

            if (GsOneMasterBoxIsExists(GsOne))
            {
                new Confirm("Master Box GS1 ID already exists", "Information", MessageBoxButtons.OK);
                log("Master Box GS1 ID already exists");
                wr.txtGs1MasterBox.Text = "";
                return;
            }

            if (!GsOne.Equals(GsOneScanned))
            {
                new Confirm("Master Box GS1 ID not found", "Information", MessageBoxButtons.OK);
                log("Master Box GS1 ID not found");
                wr.txtGs1MasterBox.Text = "";
                return;
            }

            updateGsoneMasterbox(GsOne, MasterBoxScanned);
            kirimkeDatabaseDistribusi(GsOne);
            db.Movement("", "Manual Packer", "Label manual", "1", GsOne);
            string shippingorder = updatePickinglist(MasterBoxScanned);
            UpdateShippingOrderServerDanPickingSlip(shippingorder, LIST_MASTERBOX[onGoing].ShippingOrderNumber, LIST_MASTERBOX[onGoing].ColieMax);

            DataRow Row = wr.datGs.NewRow();
            Row["GSONE"] = GsOneScanned;
            Row["MASTER_BOX"] = MasterBoxScanned;
            Row["WEIGHT"] = WightScanned;
            Row["COLI"] = GetColiNumber(GsOneScanned);
            wr.datGs.Rows.Add(Row);
            wr.dgvGsOne.DataSource = wr.datGs;
            wr.txtIdMasterBox.Text = "";
            wr.txtWeight.Text = "";
            wr.txtGs1MasterBox.Text = "";
            GsOneScanned = "";
            MasterBoxScanned = "";
            WightScanned = "";

            if (!(NoDoclose == ""))
            {
                new Confirm("DO " + NoDoclose + " close", "Information", MessageBoxButtons.OK);
                log("DO " + NoDoclose + "close");
                wr.txtGs1MasterBox.Text = "";             
            }
        }

        private void updateGsoneMasterbox(string gsone, string masterbox)
        {
            Dictionary<string, string> field = new Dictionary<string, string>();
            string where = "masterboxid='" + masterbox + "'";

            field.Add("masterBoxGsOneId", gsone);            
            db.update(field, "vaccine", where);

            field = new Dictionary<string, string>();
            field.Add("gsOneMasterBoxId", gsone);
            db.update(field, "masterbox", where);
        }

        public string cekStatusMasterboxPickinglist(string data)
        {
            List<string> field = new List<string>();
            field.Add("status");
            string where = "itemid ='" + data+"'";
            List<string[]> ds = db.selectList(field, "pickinglist", where);
            if (db.num_rows > 0)
            {
                return ds[0][0];
            }
            return "";
        }

        private bool GsOneMasterBoxIsExists(string gsone)
        {
            List<string> field = new List<string>();
            field.Add("gsOneMasterBoxId");
            string where = "gsOneMasterBoxId ='" + gsone + "'";
            List<string[]> ds = db.selectList(field, "masterbox", where);
            if (db.num_rows > 0)
            {
                return true;
            }
            return false;
        }

        private string GetColiNumber(string gsOneMasterBoxId)
        {
            List<string> field = new List<string>();
            field.Add("colieNumber");
            string where = "gsOneMasterBoxId ='" + gsOneMasterBoxId + "'";
            List<string[]> ds = db.selectList(field, "masterbox", where);
            if (db.num_rows > 0)
            {
                return ds[0][0];
            }
            return "";
        }
    }
}
