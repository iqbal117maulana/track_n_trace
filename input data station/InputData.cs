﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;

namespace Input_Data_Station
{
    public partial class InputData : Form
    {
        Control ms;
        int ordersItemIndex;
        int ordersItemIndex2;
        private int Interval = 10000; //ms
        int qu = 0;
        string cek;
        string cekProduct;
        sqlitecs sq;
        dbaccess db;
        dbaccessDist dbDist;
        List<string[]> datadetail;
        List<string[]> dataOrder = new List<string[]>();
        List<string[]> datacomboCold;
        List<string[]> datacomboWare;
        public List<string[]> datacombo;

        private Thread CameraAsyncTask;
        private Thread CameraDataAsyncTask;
        public InputData(string admin)
        {
            InitializeComponent();
            sq = new sqlitecs();
            db = new dbaccess();
            ms = new Control();
            ms.wr = this;
            ms.startstation(admin);
            lbladmin.Text = admin;
            settable("");
            Camera.Initialize();
            CameraAsyncTask = new Thread(new ThreadStart(CheckCameraConnection));
            CameraDataAsyncTask = new Thread(new ThreadStart(CheckScannedData));
            CameraAsyncTask.Start();
        }

        bool isStarted = false;

        private void CheckScannedData()
        {
            //if (isStarted)
          //  {
                string DataCapture = Camera.Read();
                string[] datapecah = DataCapture.Split(',');
                foreach (string da in datapecah)
                {
                    ms.getDataBarcode(da);
                }
           // }
        }

        private void CheckCameraConnection()
        {
            try
            {
                //if (isStarted)
                //{
                    if (!Camera.CheckConnection())
                    {
                        if (CameraDataAsyncTask.IsAlive)
                        {
                            CameraDataAsyncTask.Abort();
                            Camera.Close();
                        }
                    }
                    else
                    {
                        //if (!CameraDataAsyncTask.IsAlive)
                       //{
                            CameraDataAsyncTask.Start();
                       //}
                    }

                    Thread.Sleep(Interval);
                    CameraAsyncTask = new Thread(new ThreadStart(CheckCameraConnection));
                    CameraAsyncTask.Start();
                }
               // else
               // {
                  //  CameraAsyncTask.Interrupt();
                //}
            //}
            catch
            {

            }
        }

        private void Warehouse_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
        }

        private void button9_Click(object sender, EventArgs e)
        {
            config cfg = new config();
            cfg.Show();
        }

        private void dataGridView2_CellClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
           
        }

        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            if (dgvReceiving.SelectedRows.Count > 0)
                ordersItemIndex = dgvReceiving.SelectedRows[0].Index;
        }
       
        private void button8_Click(object sender, EventArgs e)
        {
            Confirm cf = new Confirm("Are you Sure? ", "Confirmation");
            bool hasilDialog = cf.conf();
            if (hasilDialog)
            {
                this.Dispose();
                Login lg = new Login();
                lg.Show();
            }
        }

        string cek2(string data)
        {
            if (data.Equals("0"))
                return "Ready";
            else if (data.Equals("1"))
                return "OnProgress";
            else
                return "Closed";
        }

        int cekbalik(string data)
        {
            if (data.Equals("Ready"))
                return 0;
            else if (data.Equals("OnProgress"))
                return 1;
            else
                return 2;
        }

        public void settable(string data)
        {
            string where = " shippingOrder.status >=0 ";
            if (data.Length > 0)
                where = where + " AND shippingOrder.shippingOrderNumber = '" + data + "'";
            where = where + " GROUP BY shippingOrder.shippingOrderNumber, shippingOrder.status";

            List<string> field = new List<string>();
            field.Add("shippingOrder.shippingOrderNumber 'Job Order', " +
                        "(SELECT SUM(shippingOrder_detail.qty) FROM shippingOrder_detail WHERE shippingOrder_detail.shippingOrderNumber = shippingOrder.shippingOrderNumber) as Quantity," +
                        "(SELECT COALESCE(SUM(pickingList.qty), 0) FROM pickingList WHERE pickingList.detailNo LIKE shippingOrder.shippingOrderNumber + '%' AND status NOT IN (0,3)) as 'Actual Qty' ,shippingOrder.status As Status");
            string from = "shippingOrder JOIN shippingOrder_detail ON shippingOrder.shippingOrderNumber = shippingOrder_detail.shippingOrderNumber ";
            List<string[]> ds = db.selectList(field, from, where);
            if (ds.Count > 0)
            {
                DataTable table = db.dataHeader;
                dgvReceiving.DataSource = table;
                foreach (string[] Row in ds)
                {
                    DataRow row = table.NewRow();
                    for (int k = 0; k < Row.Length; k++)
                    {
                        if (k == 3)
                            row[k] = cek2(Row[k]);
                        else
                            row[k] = Row[k];
                    }
                    table.Rows.Add(row);
                }
                dgvReceiving.DataSource = table;
            }
            else
            {
                DataTable table = db.dataHeader;
                dgvReceiving.DataSource = table;
            }
        }

        private void dgvReceiving_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            string joborder = dgvReceiving.Rows[ordersItemIndex].Cells[0].Value.ToString();
            int status = cekbalik(dgvReceiving.Rows[ordersItemIndex].Cells[3].Value.ToString());
            ms.SetDetail(joborder, status);
     
        }

        private void dgvReceiving_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void dgvReceiving_SelectionChanged(object sender, EventArgs e)
        {
            if (dgvReceiving.SelectedRows.Count > 0)
            {
                ordersItemIndex = dgvReceiving.SelectedRows[0].Index;
            }
        }

        private void cmbDetail_SelectedIndexChanged(object sender, EventArgs e)
        {
            ms.setProductModel(cmbDetail.SelectedIndex);
        }

        private void cmbDetail_SelectedValueChanged(object sender, EventArgs e)
        {
        }

        private void cmbDetail_SelectionChangeCommitted(object sender, EventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            ms.tampilJobOrder();
            settable("");
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                string barcode = txtBarcode.Text;
                ms.getDataBarcode(barcode);
                txtBarcode.Text= "";
            }
        }

        private void button2_Click(object sender, EventArgs e)
        { 
            string joborder = dgvReceiving.Rows[ordersItemIndex].Cells[0].Value.ToString();
            ms.executeShipping(joborder);
            ms.tampilJobOrder();
            settable("");
        }

        private void txtBarcode_KeyPress(object sender, KeyPressEventArgs e)
        {

        }
    }
}
