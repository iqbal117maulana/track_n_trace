﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Net;
using System.Threading;
using System.Data;

namespace Input_Data_Station
{
    class server
    {
        InputData ad;
        int port;
        Control ms;
        public server(InputData bl, string ports, Control mst)
        {
            ms = mst;
            port = int.Parse(ports);
            ad = bl;
            isRunning = true;
            if (!cekPort(ports))
            {
                mThread t = new mThread(StartListening);
                Thread masterThread = new Thread(() => t(ref isRunning));
                masterThread.IsBackground = true; //better to run it as a background thread
                masterThread.Start();
            }
        }
        public static bool isRunning;

        delegate void mThread(ref bool isRunning);
        delegate void AccptTcpClnt(ref TcpClient client, TcpListener listener);


        public static void AccptClnt(ref TcpClient client, TcpListener listener)
        {
            if (client == null)
                client = listener.AcceptTcpClient();
        }

        public void StartListening(ref bool isRunning)
        {
            TcpListener listener = new TcpListener(port);
            Byte[] bytes = new Byte[256];
            
                listener.Start();
                TcpClient handler = null;
                while (isRunning)
                {
                    AccptTcpClnt t = new AccptTcpClnt(AccptClnt);
                    Thread tt = new Thread(() => t(ref handler, listener));
                    tt.IsBackground = true;
                    tt.Start(); //the AcceptTcpClient() is a blocking method, so we are invoking it    in a seperate thread so that we can break the while loop wher isRunning becomes false

                    while (isRunning && tt.IsAlive && handler == null)
                        Thread.Sleep(500); //change the time as you prefer

                    if (handler != null)
                    {
                        string data = null;
                        NetworkStream stream = handler.GetStream();
                        int i;
                        try
                        {
                            Int32 da = stream.Read(bytes, 0, bytes.Length);
                            String Message = System.Text.Encoding.ASCII.GetString(bytes, 0, da);
                            ms.tambahData(Message);
                        }
                        catch (Exception ui)
                        {

                        }
                    }
                    else if (!isRunning && tt.IsAlive)
                    {
                        tt.Abort();
                    }
                    handler = null;
                }
                listener.Stop();
        }

        public bool cekPort(string port)
        {
            using (TcpClient tcpClient = new TcpClient())
            {
                try
                {
                    tcpClient.Connect("127.0.0.1", int.Parse(port));
                    return true;
                }
                catch (Exception)
                {
                    Console.WriteLine("Port closed");
                    return false;
                }
            }
            return false;
        }
    }
}
    
