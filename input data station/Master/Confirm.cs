﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Input_Data_Station
{
    public partial class Confirm : Form
    {
        public Confirm(string textConfirm, string labelatas)
        {
            InitializeComponent();
            lblMessage.Text = textConfirm;
            this.Text = labelatas;
        }

        public bool conf()
        {
            button1.DialogResult = DialogResult.OK;
            button2.DialogResult = DialogResult.No;
            this.ShowDialog();
            if (this.DialogResult == DialogResult.OK)
            {
                return true;
            }
            else
                return false;
        }
    }
}
