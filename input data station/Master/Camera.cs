﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Configuration;
using System.Windows.Forms;
using System.Net.NetworkInformation;
using System.IO;

namespace Input_Data_Station
{
    class Camera
    {
        private static bool IsInitialized = false;
        private static TcpClient Client;
        private static Byte[] Data = new Byte[256];
        private static NetworkStream Stream;
        public static bool IsConnected = false;
        static sqlitecs sqlite;

        /**
         * Initialize the camera client;
         */
        public static void Initialize()
        {
            if (!IsInitialized)
            {
                sqlite = new sqlitecs();
                IsInitialized = true;
            }
        }

        /**
         * Connect to a camera via ethernet.
         */
        public static void Connect()
        {
            try
            {
                Client = new TcpClient(sqlite.config["ipCamera"], Int32.Parse(sqlite.config["portCamera"]));
                if (Client.Connected)
                {
                    IsConnected = true;
                }
            }
            catch (SocketException ex)
            {
                if (!ex.NativeErrorCode.Equals(10056))
                    IsConnected = false;
            }
        }

        /**
         * Check the camera connection. Returns true if connected
         * otherwise false.
         */
        public static bool CheckConnection() 
        {
            Connect();
            return IsConnected;
        }

        /**
         * Read camera output then return it as a List of Strings.
         */
        public static String Read()
        {
            String Message = "0";
            try
            {
                Stream = Client.GetStream();
                Int32 Bytes = Stream.Read(Data, 0, Data.Length);
                Message = System.Text.Encoding.ASCII.GetString(Data, 0, Bytes);
            }
            catch (Exception ex)
            {
                
            }

            return Message;
        }

        /**
         * Close the connection with the camera.
         */
        public static void Close()
        {
            if (Stream != null)
                Stream.Close();

            Client.Close();
        }

        /**
         * Check connection state.
         */
        public static TcpState GetState()
        {
            var foo = IPGlobalProperties.GetIPGlobalProperties()
              .GetActiveTcpConnections()
              .SingleOrDefault(x => x.LocalEndPoint.Equals(Client.Client.LocalEndPoint));
            return foo != null? foo.State : TcpState.Unknown;
        }
    }
}
