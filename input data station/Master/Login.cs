﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Input_Data_Station
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }
        
        private void buttonLogin_Click(object sender, EventArgs e)
        {
            dbaccess db = new dbaccess();
            try
            {
                List<string[]> res = db.validasi(txtUsername.Text, txtPassword.Text);
                if (res != null)
                {
                    string adminid = res[0][0];
                    InputData bl = new InputData(adminid);
                    bl.Show();
                    this.Hide();
                }
                else
                {
                    MessageBox.Show("Username dan Password Salah !");
                    db.simpanLog("Error 0010 : Username dan Password Salah !");
                }
            }
            catch (ArgumentException ae)
            {
                db.simpanLog("Error " + ae.ToString());
            }

        }

        private void Login_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }
    }
}
