﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Input_Data_Station
{
    public partial class config : Form
    {
        public config()
        {
            InitializeComponent();
            getConfig();
        }

        void getConfig()
        {
            try
            {
                sqlitecs sql = new sqlitecs();
                txtIpCamera.Text = sql.config["ipCamera"];
                txtPortCamera.Text = sql.config["portCamera"];
                txtPortServer.Text = sql.config["portserver"];
                txtIpServer.Text = sql.config["ipServer"];
                txtStationName.Text = sql.config["Servername"];
            }
            catch (KeyNotFoundException k)
            {

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Are you sure? ", "Confirmation", MessageBoxButtons.YesNo);
            if (result == DialogResult.Yes)
            {
                sqlitecs sql = new sqlitecs();
                Dictionary<string, string> field = new Dictionary<string, string>();
                field.Add("ipCamera", txtIpCamera.Text);
                field.Add("portCamera", txtPortCamera.Text);
                field.Add("portserver", txtPortServer.Text);
                field.Add("ipServer", txtIpServer.Text);
                field.Add("Servername", txtStationName.Text);
                sql.update(field, "config", "");
                MessageBox.Show("Data Berhasil Disimpan");
                this.Dispose();
            }
        }
    }
}
