﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Windows.Forms;
using System.Threading;

namespace Input_Data_Station
{
    class Control
    {
        DataTable tableInbound = new DataTable();
        DataTable tableMovement;
        DataTable tableOutbound;
        DataTable tableDetail;
        List<string[]> dataOrder = new List<string[]>();
        sqlitecs sq;
        dbaccess db;
        dbaccessDist dbDist;
        int quantityDetail;

        public List<string[]> datacombo;
        public InputData wr;
        List<string[]> datacomboCold;
        List<string[]> datacomboWare;

        private Thread serverThread;
        public Control()
        {
            db = new dbaccess();
            dbDist = new dbaccessDist();
            sq = new sqlitecs();
            insialisasitable();
            serverThread = new Thread(new ThreadStart(MulaiServer));
            serverThread.Start();
            //datacomboWare = new List<string[]>();
            //string [] data =  new string[2];
            //data[0] = "Cold Room 1";
            //data[1] = "0";
            //datacomboWare.Add(data); data = new string[2];
            //data[0] = "Cold Room 2";
            //data[1] = "1";
            //datacomboWare.Add(data); data = new string[2];
            //data[0] = "Cold Room 3";
            //data[1] = "2";
            //datacomboWare.Add(data);
        }
        public void MulaiServer()
        {
            server srv = new server(wr, sq.config["portserver"], this);
        }
        public void insialisasitable()
        {
            tableInbound = new DataTable();
            tableInbound.Columns.Add("Basket ID");
            tableInbound.Columns.Add("Timestamp");
            tableMovement = new DataTable();
            tableMovement.Columns.Add("Basket ID");
            tableMovement.Columns.Add("Timestamp");
            tableOutbound = new DataTable();
            tableOutbound.Columns.Add("Basket ID");
            tableOutbound.Columns.Add("Timestamp");
        }

        public void startstation(string adminid)
        {
            log("Login successfully admin ID = " + adminid);
            log("Server Port : " + sq.config["portserver"]);
            //setcomboWare();
        }

       
        ////RECEIVING
        //public void tambahData(string dataBarcode)
        //{
        //    //cek data di db 
        //    List<string> field = new List<string>();
        //    field.Add("basketid");
        //    string where = "basketid ='" + dataBarcode + "'";
        //    List<string[]> ds = db.selectList(field, "basket", where);
        //    if (db.num_rows > 0)
        //    {
        //        if (cekBasketPickingList(dataBarcode))
        //        {
        //            if (!cekBasketsqlite(dataBarcode))
        //                reciveSqlite(dataBarcode);
        //        }
        //    }
        //}

        //public bool cekBasketPickingList(string databarcode)
        //{
        //    List<string> field = new List<string>();
        //    field.Add("itemid");
        //    string where = "itemid ='" + databarcode + "' and status = '1'";
        //    List<string[]> ds = db.selectList(field, "pickingList", where);
        //    if (db.num_rows > 0)
        //    {
        //        return true;
        //    }
        //    return false;
        //}

        //private void reciveSqlite(string basketid)
        //{
        //    //insert ke sqlite 
            
        //    string dates = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        //    Dictionary<string, string> field = new Dictionary<string, string>();
        //    field.Add("basketid", basketid);
        //    field.Add("timestamp", dates);
        //    sq.insert(field, "warehouse");

        //    // masukan ke table
        //    DataRow row = tableInbound.NewRow();
        //    row[0] = basketid;
        //    row[1] = dates;
        //    tableInbound.Rows.Add(row);
        //    wr.dgvInbound.DataSource = tableInbound;
        //    wr.lblQtyInbound.Text = "" + wr.dgvInbound.Rows.Count;
        //}

        //private bool cekBasketsqlite(string basket)
        //{
        //    List<string> field = new List<string>();
        //    field.Add("basketid");
        //    string where = "basketid ='" + basket + "'";
        //    List<string[]> ds = sq.select(field, "warehouse", where);
        //    if (sq.num_rows > 0)
        //    {
        //        return true;
        //    }
        //    return false;


        //}

        //public void hapusdata(string barcode)
        //{
        //    if (barcode.Length > 0)
        //        barcode = "basketid ='" + barcode + "'";
        //    sq.delete(barcode, "warehouse");
        //}
        
        //public void setcomboWare()
        //{
        //    List<string> da = new List<string>();
        //    List<string> field = new List<string>();
        //    field.Add("warehouse_location.locationName,warehouse_location.locationId");
        //    string from = "warehouse INNER JOIN dbo.warehouse_location ON warehouse_location.warehouseId = warehouse.warehouseId AND warehouse_location.warehouseId = warehouse.warehouseId";
        //    string where = "warehouse.owner = 'Distribution'";
        //    datacomboWare = db.selectList(field, from, where);
        //    if (db.num_rows > 0)
        //    {
        //        List<string> das = new List<string>();
        //        for (int i = 0; i < datacomboWare.Count; i++)
        //        {
        //            das.Add(datacomboWare[i][0]);
        //        }
        //        wr.cmbMoveLocation.DataSource = das;
        //        wr.cmbMoveNewLocation.DataSource = das;
        //        wr.cmbLocation.DataSource = das;
        //    }
        //}

        //public void ClearDgvInbound()
        //{
        //    tableInbound = new DataTable();
        //    tableInbound.Columns.Add("Basket ID");
        //    tableInbound.Columns.Add("Timestamp");
        //    wr.lblQtyInbound.Text = "0";
        //    wr.dgvInbound.DataSource = tableInbound;
        //    hapusdata("");
        //}
       
        //internal void submitReceiving()
        //{
        //    for (int i = 0; i < wr.dgvInbound.RowCount; i++)
        //    {
        //        Dictionary<string, string> field = new Dictionary<string, string>();
        //        field.Add("locationid", datacomboWare[wr.cmbLocation.SelectedIndex][1]);
        //        string where = "basketid ='" + wr.dgvInbound.Rows[i].Cells[0].Value.ToString() + "'";
        //        db.update(field, "basket", where);
        //    }
        //    ClearDgvInbound();
        //}

        string jobnumb;
        char delimit = ',' ;
        internal void SetDetail(string jobNumber, int status)
        {
            if (status == 1)
            {
                wr.btnExecute.Visible = false;
                wr.grpDetailDgv.Visible = true;
            }
            else
            {
                wr.btnExecute.Visible = true;
                wr.grpDetailDgv.Visible = false;
            }
            jobnumb = jobNumber;
            getDataDetail(jobNumber);
            wr.grpJobOrder.Visible = false;
            wr.grpDetail.Visible = true;
            wr.lblJobOrder.Text = jobNumber; 
            tampilTableDetail();
        }

        void tampilTableDetail()
        {
            List<string> field = new List<string>();
            field.Add("itemid 'Barcode',qty 'Quantity'");
            string where = "detailno ='" + jobnumb + "'";
            List<string[]> ds = db.selectList(field, "pickinglist", where);
            if (db.num_rows > 0)
            {
                DataTable table = db.dataHeader;
                wr.dgv.DataSource = table;
                int total=0;
                foreach (string[] Row in ds)
                {
                    DataRow row = table.NewRow();
                    for (int k = 0; k < Row.Length; k++)
                    {
                        row[k] = Row[k];
                        if (k == 1)
                        {
                            total = total + int.Parse(row[k].ToString());
                        }
                    }
                    table.Rows.Add(row);
                }
                wr.lblActual.Text = "" + total;
                wr.dgv.DataSource = table;
            }
            else
            {
                DataTable table = db.dataHeader;
                wr.dgv.DataSource = table;
            }
        }

        List<string> datacomboDetail;
        void getDataDetail(string jobNumber)
        {
            List<string> field = new List<string>();
            field.Add("shippingOrder_detail.productModelId,product_model.model_name,shippingOrder_detail.qty,shippingOrder_detail.status");
            string from = "shippingOrder INNER JOIN shippingOrder_detail ON shippingOrder_detail.shippingOrderNumber = shippingOrder.shippingOrderNumber INNER JOIN product_model ON shippingOrder_detail.productModelId =  product_model.product_model ";
            string where = "shippingOrder.shippingOrderNumber = '"+jobNumber+"' and shippingOrder_detail.status <> '2'";
            dataDetail = db.selectList(field, from, where);
            if (db.num_rows > 0)
            {
                datacomboDetail = new List<string>();
                foreach (string[] da in dataDetail)
                {
                    datacomboDetail.Add(da[1]);
                }
                wr.cmbDetail.DataSource = datacomboDetail;
            }
        }
        List<string[]> dataDetail;

        internal void setProductModel(int selectInd)
        {
            wr.lblQty.Text = dataDetail[selectInd][2];
            wr.lblStatus.Text = dataDetail[selectInd][3];
        }

        internal void tampilJobOrder()
        {
            wr.grpJobOrder.Visible = true;
            wr.grpDetail.Visible = false;
        }

        void simpanpickinglist(string itemid, string qty, string detailno)
        {
            Dictionary<string, string> field = new Dictionary<string, string>();
            field.Add("itemid", itemid);
            field.Add("qty", qty);
            field.Add("detailno", detailno);
            field.Add("status", "0");
            db.insert(field, "pickinglist");
        }

        void masukKeTable(string barcode, string type, string qty)
        {

        }

        bool cekItem(string barcode)
        {
            List<string> field = new List<string>();
            field.Add("itemid");
            string where = "itemid ='" + barcode + "'";
            List<string[]> ds = db.selectList(field, "pickingList", where);
            if (db.num_rows > 0)
                return false;
            else
                return true;
        }
        internal void getDataBarcode(string barcode)
        {
            
            if (cekItem(barcode))
            {
                string productmodel = dataDetail[wr.cmbDetail.SelectedIndex][0];
                log("Data : " + barcode);
                if (cekBasket(barcode, productmodel))
                {
                    log("Data : Basket");
                    int banyakVaccine = cekbanyakBasket(barcode);
                    quantityDetail = quantityDetail + banyakVaccine;
                    log("Data : " + banyakVaccine);
                    simpanpickinglist(barcode, "" + banyakVaccine, jobnumb);
                }
                else if (cekInfeed(barcode, productmodel))
                {
                    log("Data : Infeed");
                    int banyakVaccine = cekbanyakInfeed(barcode);
                    quantityDetail = quantityDetail + banyakVaccine;
                    log("Data : " + banyakVaccine);
                    simpanpickinglist(barcode, "" + banyakVaccine, jobnumb);
                }

                //else if (cekBlister(barcode))
                //{
                //int banyakVaccine = cekbanyakInfeed(barcode);
                //quantityDetail = quantityDetail + banyakVaccine;

                //}

                else if (cekvaccine(barcode, productmodel))
                {
                    log("Data : Vaccine");
                    quantityDetail = quantityDetail + 1;
                    log("Data : " + 1);
                    simpanpickinglist(barcode, "1", jobnumb);
                }
                else
                {
                    MessageBox.Show("Data Not Found");
                }
                tampilTableDetail();
                log("Quantity : " + quantityDetail);
            }
        }

        private bool cekBlister(string barcode, string productmodel)
        {
            throw new NotImplementedException();
        }

        private bool cekvaccine(string barcode, string productmodel)
        {
            List<string> field = new List<string>();
            field.Add("Vaccine.capId");
            string where = "(Vaccine.capId = '" + barcode + "' OR Vaccine.gsOneVialId = '" + barcode + "') AND Vaccine.productmodelid ='"+productmodel+"'";
            List<string[]> ds = db.selectList(field, "Vaccine", where);
            if (db.num_rows > 0)
            {
                return true;
            }
            return false;
        }

        public bool cekBasket(string barcode, string productmodel)
        {
            List<string> field = new List<string>();
            field.Add("DISTINCT basket.basketId");
            string from = "basket INNER JOIN innerBox ON innerBox.basketId = basket.basketId INNER JOIN blisterPack ON blisterPack.innerBoxId = innerBox.infeedInnerBoxId INNER JOIN Vaccine ON blisterPack.blisterpackId = Vaccine.blisterpackId";
            string where = "basket.basketId = '" + barcode + "'  AND Vaccine.productModelId = '" + productmodel + "'";
            List<string[]> ds = db.selectList(field,from,where);
            if (db.num_rows > 0)
            {
                return true;
            }
            return false;
            
        }
      
        public bool cekInfeed(string barcode, string productmodel)
        {
            List<string> field = new List<string>();
            field.Add("DISTINCT dbo.innerBox.infeedInnerBoxId");
            string from = "innerBox INNER JOIN blisterPack ON blisterPack.innerBoxId = innerBox.infeedInnerBoxId  INNER JOIN Vaccine ON blisterPack.blisterpackId = Vaccine.blisterpackId";
            string where = "(innerBox.infeedInnerBoxId = '" + barcode + "' OR innerBox.gsOneInnerBoxId = '" + barcode + "') AND Vaccine.productModelId = '" + productmodel + "'";
            List<string[]> ds = db.selectList(field, from, where);
            if (db.num_rows > 0)
            {
                return true;
            }
            return false;

        }
 
        public int cekbanyakBasket(string basketid)
        {
            List<string> field = new List<string>();
            field.Add("count(Vaccine.capId)");
            string tabl = "Vaccine LEFT JOIN dbo.blisterPack ON dbo.Vaccine.blisterpackId = dbo.blisterPack.blisterpackId LEFT JOIN dbo.innerBox ON dbo.blisterPack.innerBoxId = dbo.innerBox.infeedInnerBoxId left JOIN dbo.basket ON dbo.innerBox.basketId = dbo.basket.basketId";
            string where = "basket.basketId = '" + basketid + "'";
            List<string[]> ds = db.selectList(field, tabl, where);
            if (db.num_rows > 0)
                return int.Parse(ds[0][0]);
            else
                return 0;
        }

        private int cekbanyakInfeed(string barcode)
        {
            List<string> field = new List<string>();
            field.Add("count(Vaccine.capId)");
            string tabl = "Vaccine LEFT JOIN dbo.blisterPack ON dbo.Vaccine.blisterpackId = dbo.blisterPack.blisterpackId LEFT JOIN dbo.innerBox ON dbo.blisterPack.innerBoxId = dbo.innerBox.infeedInnerBoxId";
            string where = "innerBox.infeedInnerBoxId = '" + barcode + "'";
            List<string[]> ds = db.selectList(field, tabl, where);
            if (db.num_rows > 0)
                return int.Parse(ds[0][0]);
            else
                return 0;
        }

        public void log(string data)
        {
            try
            {
                string dates = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                string simpan = dates + "\t" + data + "\n";
                wr.txtLog.AppendText(simpan);
            }
            catch (ObjectDisposedException ob)
            {

            }
        }

        internal void executeShipping(string joborder)
        {
            Dictionary<string, string> field = new Dictionary<string, string>();
            field.Add("status","1");
            string where = "shippingOrderNumber ='"+joborder+"'";
            db.update(field,"shippingOrder",where);
        }

        internal void tambahData(string data)
        {
            if (wr.InvokeRequired)
            {
                wr.Invoke(new Action<string>(tambahData), new object[] { data });
                return;
            }
            log("Receive Data:" + data);
            data = data.Replace("\r", "");
            data = data.Replace("\n", "");
            if (!data.Contains("?") && data.Length > 6)
            {
                string[] hasilSplit = data.Split(delimit);
                foreach (string da in hasilSplit)
                {
                    getDataBarcode(da);
                }
            }
        }
    }
}
