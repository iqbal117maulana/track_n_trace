﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Windows.Forms;

namespace Receiving_Station
{
    class Control
    {
        public string adminid;
        public Receiving rv;
        public DetailVaccine rv2;
        public const string StatusStation = "0";

        DataTable tableReceiv;
        sqlitecs sqlite;
        public dbaccess db;
        public DataTable tablePicking;

        public string DeleteSpecialChar(string data)
        {
            data = data.Replace("\r", "");
            data = data.Replace("\n", "");
            data = data.Replace("]d2", "");
            data = data.Replace("$", "");
            data = data.Replace(" ", "");
            data = data.Replace("<GS>", "");
            char da = (char)29;
            data = data.Replace("" + da, "");

            return data;
        }

        public void AddTableReceiving(string data)
        {
            //data = data.Replace(" ", "");
            //data = data.Replace("/n", "");
            //data = data.Replace("/r", "");
            //cek data di db 
            data = DeleteSpecialChar(data);            
            log("Receive data : " + data);
            string sql;
            sql = "SELECT itemid,detailno,dbo.receivingOrder_detail.batchnumber,receivingOrder_detail.receivingOrderNumber," +
                    "receivingOrder_detail.receivingOrderDetailNo,product_model.model_name," +
                    "receivingOrder_detail.status as status_rod, pickinglist.status as ststus_pick " +
                    "FROM pickinglist INNER JOIN dbo.receivingOrder_detail ON dbo.pickingList.detailNo = dbo.receivingOrder_detail.receivingOrderDetailNo " +
                    "INNER JOIN dbo.packaging_order ON dbo.packaging_order.batchNumber = receivingOrder_detail.batchnumber " +
                    "INNER JOIN dbo.product_model ON dbo.product_model.product_model = dbo.packaging_order.productModelId " +
                    "WHERE itemid ='" + data + "'";
            DataTable dt = new DataTable();
            db.OpenQuery(out dt, sql);
            if (dt.Rows.Count > 0)
            {
                if ((dt.Rows[0]["status_rod"].ToString() == "0") && (dt.Rows[0]["ststus_pick"].ToString() == "0"))
                {
                    DataRow[] FoundRow;
                    FoundRow = dataVaccine.Select("itemid = '" + data + "'");
                    if (FoundRow.Length > 0)
                    {
                        //UPDATE STATUS 1 PICKINGLIST
                        Dictionary<string, string> field;
                        field = new Dictionary<string, string>();
                        field.Add("status", "1");
                        string where = "itemid = '" + data + "'";
                        db.update(field, "pickinglist", where);
                        db.eventname = "Receive data " + data;
                        db.systemlog();

                        //INSERT MOVEMENT HISTORY
                        string dates = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                        db.Movement("", "", "Receiving", "1", data);                        
                        log("Set at Picking list");

                        DataTable dtTmp;
                        sql = "SELECT itemid,detailno,dbo.receivingOrder_detail.batchnumber,receivingOrder_detail.receivingOrderNumber,receivingOrder_detail.receivingOrderDetailNo " +
                                "FROM pickinglist INNER JOIN dbo.receivingOrder_detail ON dbo.pickingList.detailNo = dbo.receivingOrder_detail.receivingOrderDetailNo " +
                                "WHERE itemid ='" + data + "' and receivingOrder_detail.status = '0' and pickinglist.status = '1'";
                        db.OpenQuery(out dtTmp, sql);
                        if (dtTmp.Rows.Count > 0)
                        {
                            cekDetailFull(dtTmp.Rows[0]["receivingOrderDetailNo"].ToString());
                            cekOrderFull(dtTmp.Rows[0]["receivingOrderNumber"].ToString());
                        }

                        //ADD & REMOVE EXISTING
                        tableReceiv.Rows.Add(FoundRow[0].ItemArray);
                        dataVaccine.Rows.Remove(FoundRow[0]);

                        //REFRESH DATA
                        rv.lblQty.Text = dataVaccine.Rows.Count.ToString() + " Basket";
                        rv.lblQtyHis.Text = tableReceiv.Rows.Count.ToString() + " Basket";
                        rv.dgvVaccine.Refresh();
                        rv.dgvReceiving.Refresh();

                        if (dataVaccine.Rows.Count == 0)
                        {
                            for (int i = 0; i < rv.dgvVaccine.Columns.Count; i++)
                            {
                                rv.dgvVaccine.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                            }
                        }

                        if (tableReceiv.Rows.Count > 0)
                        {
                            for (int i = 0; i < rv.dgvReceiving.Columns.Count; i++)
                            {
                                rv.dgvReceiving.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                            }
                        }
                    }
                    else
                    {
                        Confirm cf = new Confirm(data + " Not Found");
                        log(data + " Not Found");
                    }
                }
                else
                {
                    Confirm cf = new Confirm(data + " Already Scanned");
                    log(data + " Already Scanned");
                }
            }
            else
            {
                Confirm cf = new Confirm(data + " Not Found");
                log(data + " Not Found");
            }
        }

        public void SetComboLocation()
        {
            DataTable dtLocation;
            string sql;
            //sql = "SELECT warehouse_location.locationName,warehouse_location.locationId " +
            //        "FROM warehouse INNER JOIN dbo.warehouse_location ON warehouse_location.warehouseId = warehouse.warehouseId AND warehouse_location.warehouseId = warehouse.warehouseId " +
            //        "WHERE warehouse.owner = 'Distribution'";
            //db.OpenQuery(out dtLocation, sql);

            sql = "SELECT warehouseName, warehouseId " +
                    "FROM warehouse " +
                    "WHERE owner = 'Distribution'";
            db.OpenQuery(out dtLocation, sql);

            rv.cmbLocation.ValueMember = "warehouseId";
            rv.cmbLocation.DisplayMember = "warehouseName";
            rv.cmbLocation.DataSource = dtLocation.Copy();
            rv.cmbLocation.SelectedIndex = -1;

            rv.cmbMoveNewLocation.ValueMember = "warehouseId";
            rv.cmbMoveNewLocation.DisplayMember = "warehouseName";
            rv.cmbMoveNewLocation.DataSource = dtLocation.Copy();
            rv.cmbMoveNewLocation.SelectedIndex = -1;
        }

        internal void submitReceiving()
        {
            for (int i = 0; i <= tableReceiv.Rows.Count-1; i++)
            {
                rv.progressBar1.PerformStep();
                Dictionary<string, string> field = new Dictionary<string, string>();
                field.Add("basketid", tableReceiv.Rows[i]["itemid"].ToString());
                field.Add("warehouseid", rv.cmbLocation.SelectedValue.ToString());
                field.Add("locationid", rv.cmbLocation.SelectedValue.ToString());
                field.Add("timestamp", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                sqlite.insert(field, "warehouse");
                sqlite.Begin();
                updateVaccine(tableReceiv.Rows[i]["itemid"].ToString(), rv.cmbLocation.SelectedValue.ToString(), true);
            }
            //if (sqlite.dataInsert.Count > 0)
            //{
            //    sqlite.Begin();
            //}
            db.Movement("", "Receiving", "Cold Storage", "" + tableReceiv.Rows.Count, "");
            Confirm cf = new Confirm("Location has been saved to " + rv.cmbLocation.Text, "Information", MessageBoxButtons.OK);
            log("Location has been saved to " + rv.cmbLocation.Text);

            rv.lblOrder.Text = "-";
            rv.lblProduct.Text = "-";
            rv.lblBatch.Text = "-";
            rv.txtOrderNumber.Text = "";
            rv.cmbLocation.SelectedIndex = -1;
            InitialDgv();
        }

        void updateVaccine(string databarcode, string lastlocat, bool IsUpdateTime)
        {
            string cari = "";
            cekproduct(databarcode, "");
            if (uom.Equals("Basket"))
            {
                cari = "basketid ='" + databarcode + "'";
            }
            else if (uom.Equals("Vial"))
            {
                cari = "innerboxid ='" + databarcode + "' OR innerBoxGsOneId ='" + databarcode + "'";
            }
            else if (uom.Equals("Blister"))
            {
                cari = "blisterpackid='" + databarcode + "'";
            }
            else if (uom.Equals("Vaccine"))
            {
                cari = "capid ='" + databarcode + " OR gsonevialid ='" + databarcode + "'";
            }
            Dictionary<string, string> field = new Dictionary<string, string>();
            field.Add("lastLocationId", lastlocat);
            if (IsUpdateTime)
            {
                field.Add("inbound_time", "GETDATE()");
            }
            db.update(field, "vaccine", cari);
        }

        bool cekproduct(string databarcode, string locat)
        {
            uom = "";
            if (cekbasket(databarcode, locat))
            {
                //uomId = "basketid='" + databarcode + "'";
                uom = "Basket";
                return true;
            }
            else 
                if (cekInfeed(databarcode, locat))
            {
                //uomId = "innerBoxId='" + databarcode + "' or innerBoxGsOneId='" + databarcode + "'";
                uom = "Vial";
                return true;
            }
            else
                return false;
        }

        DataTable dtProduct;
        private bool cekbasket(string dataBarcode, string location)
        {
            String FieldLocation = "";
            String GroupLocation = "";
            String TableLocation = "";
            String WhereLocation = "";
            if (location.Length > 0)
            {
                FieldLocation = ", w.warehouseName ";
                GroupLocation = ", w.warehouseName ";
                TableLocation = ", dbo.warehouse w ";
                WhereLocation = " and v.lastLocationId IS NOT null AND w.warehouseId = v.lastLocationId";
            }
                        
            String from = " FROM dbo.vaccine v, dbo.product_model p ";
            string where = " WHERE v.basketid ='" + dataBarcode + "' and v.isReject = '0' AND v.productModelId = p.product_model " + WhereLocation;
            where = where + " GROUP BY v.basketId, p.model_name, v.expDate, v.batchNumber, p.isSAS, p.outbox_qty " + GroupLocation;
                        
            string sql;
            sql = "SELECT v.basketId, p.model_name, v.expDate, v.batchNumber, case when p.isSAS = 0 then COUNT(v.basketid) else (count(v.basketId) * p.outbox_qty)-sum(v.substraction) end AS Qty, 'vial' as uom " + FieldLocation + from + TableLocation + where;
            db.OpenQuery(out dtProduct, sql);

            if (dtProduct.Rows.Count > 0)
                return true;
            else
                return false;
        }

        private bool cekInfeed(string dataBarcode, string location)
        {
            String FieldLocation = "";
            String GroupLocation = "";
            String TableLocation = "";
            String WhereLocation = "";
            if (location.Length > 0)
            {
                FieldLocation = ", w.warehouseName ";
                GroupLocation = ", w.warehouseName ";
                TableLocation = ", dbo.warehouse w ";
                WhereLocation = " and v.lastLocationId IS NOT null AND w.warehouseId = v.lastLocationId";
            }

            String from = " FROM dbo.vaccine v, dbo.product_model p ";
            string where = " WHERE (v.innerBoxId ='" + dataBarcode + "' OR " + "v.innerBoxGsOneId ='" + dataBarcode + "') and isReject = '0' AND v.productModelId = p.product_model " + WhereLocation;
            where = where + " GROUP BY v.innerBoxGsOneId, p.model_name, v.expDate, v.batchNumber, p.isSAS, p.outbox_qty " + GroupLocation;
            
            DataTable dtTmp;
            string sql;
            sql = "select v.innerBoxGsOneId, p.model_name, v.expDate, v.batchNumber, case when p.isSAS = 0 then COUNT(v.basketid) else (count(v.basketId) * p.outbox_qty)-sum(v.substraction) end AS Qty, 'vial' as uom " + FieldLocation + from + TableLocation + where;
            db.OpenQuery(out dtTmp, sql);

            if (dtTmp.Rows.Count > 0)
                return true;
            else
                return false;
        }

        private void cekOrderFull(string dataOrder)
        {
             List<string> field = new List<string>();
            field.Add("count(dbo.receivingOrder_detail.qty)");
            string where = "dbo.receivingOrder_detail.receivingOrderNumber = '" + dataOrder + "' AND status = '0'";
            List<string[]> ds = db.selectList(field, "[dbo].[receivingOrder_detail]", where);
            if (db.num_rows > 0)
            {
                if (ds[0][0].Equals("0"))
                {
                    //update detail receiving order
                    Dictionary<string, string> fieldact = new Dictionary<string, string>();
                    fieldact.Add("status", "1");
                    where = "receivingOrderNumber ='" + dataOrder + "'";
                    db.update(fieldact, "receiving_order", where);
                    log("Order " + dataOrder + " Received");
                }
            }
        }

        private void cekDetailFull(string dataReceiving)
        {
            List<string> field = new List<string>();
            field.Add("dbo.receivingOrder_detail.qty");
            string where = "dbo.receivingOrder_detail.receivingOrderDetailNo = '" + dataReceiving + "'";
            List<string[]> ds = db.selectList(field, "dbo.receivingOrder_detail", where);
            if (db.num_rows > 0)
            {
                //banyak detail
                int qtyDetail = int.Parse(ds[0][0]);
                // cek banyak picking list
                field = new List<string>();
                field.Add("SUM(dbo.pickingList.qty)");
                where = "dbo.pickingList.detailNo = '" + dataReceiving + "' and status ='1'";
                ds = db.selectList(field, "dbo.pickingList", where);
                if (ds.Count > 0)
                {
                    if (ds[0][0].Length > 0)
                    {
                        int sumQty = int.Parse(ds[0][0]);
                        if (sumQty >= qtyDetail)
                        {
                            //update detail receiving detail order
                            Dictionary<string, string> fieldact = new Dictionary<string, string>();
                            fieldact.Add("status", "1");
                            where = "receivingOrderDetailNo ='" + dataReceiving + "'";
                            db.update(fieldact, "receivingOrder_detail", where);
                            log("Detail " + dataReceiving + " Received");
                        }
                    }
                }
            }
        }
        
        internal void getDataDetail(string p, int a)
        {
            rv.grpBoxDetail.Visible = true;
            rv.grpPending.Visible = false;

            uom = "Awal";
            cekUom(p, uom);

            getDetail(p);
            setButton(a);
        }
       
        internal void getDataDetail(string p, int a, string batch)
        {
            rv.grpBoxDetail.Visible = true;
            rv.grpPending.Visible = false;

            uom = "Awal";

            rv.lblOrder.Text = p;
            cekUom(p, uom, batch);

            getDetail(p);
            setButton(a);

            dataVaccineScan = new DataTable();
        }
        
        internal void getDataDetail(string p, int a, string batch,string detal)
        {
            rv.grpBoxDetail.Visible = true;
            rv.grpPending.Visible = false;

            uom = "Awal";
            cekUom("", uom, batch, detal); 

            getDetail(p);
            setButton(a);
        }

        internal void getDataDetail_Scan(string p, int a, string batch, string detal)
        {
            rv.grpBoxDetail.Visible = true;
            rv.grpPending.Visible = false;

            uom = "Awal";
            getDataBakset_Scan("", batch, detal);

            rv.lblOrder.Text = p;
            rv.lblUom.Text = "Basket";

            getDetail(p);
            setButton(a);
        }

        private void getDataBakset_Scan(string p, string batch, string d)
        {
            string sql;
            sql = "SELECT dbo.pickingList.itemId, pickinglist.qty,receivingOrder_detail.batchnumber,'Basket' 'uom', product_model.model_name " +
                    "FROM dbo.pickingList INNER JOIN dbo.receivingOrder_detail ON dbo.pickingList.detailNo = dbo.receivingOrder_detail.receivingOrderDetailNo " +
                    "INNER JOIN dbo.packaging_order ON dbo.packaging_order.batchNumber = receivingOrder_detail.batchnumber " +
                    "INNER JOIN dbo.product_model ON dbo.product_model.product_model = dbo.packaging_order.productModelId " +
                    "where ";
            bool ptr = false;
            if (p.Length > 0)
            {
                ptr = true;
                sql += " receivingOrder_detail.receivingOrderNumber ='" + p + "'";
            }
            if (d.Length > 0)
            {
                if (ptr)
                    sql += " AND ";
                sql += "dbo.pickingList.itemId='" + d + "'";
            }
            List<string[]> ds = db.selectList(sql);
            if (db.num_rows > 0)
            {
                if (dataVaccineScan.Rows.Count == 0)
                {
                    dataVaccineScan = db.dataHeader;
                }
                for (int i = 0; i < ds.Count; i++)
                {
                    DataRow row = dataVaccineScan.NewRow();
                    for (int j = 0; j < ds[i].Length; j++)
                    {
                        row[j] = ds[i][j];
                    }
                    dataVaccineScan.Rows.InsertAt(row, 0);
                    rv.dgvVaccine.AutoGenerateColumns = false;
                    rv.dgvVaccine.DataSource = dataVaccineScan;
                    rv.dgvVaccine.Rows[0].Selected = true;
                    rv.lblQty.Text = dataVaccineScan.Rows.Count.ToString() + " Basket";
                }
            }
            else
            {
                dataVaccineScan = db.dataHeader;
                rv.dgvVaccine.AutoGenerateColumns = false;
                rv.dgvVaccine.DataSource = dataVaccineScan;
            }
        }
                
        public void setButton(int a)
        {
            if (a == 0)
            {
                rv.btnCancel.Visible = true;
                rv.btnCancel.Text = "Cancel";
            }
            else
            {
                rv.btnCancel.Visible = true;
                rv.btnCancel.Text = "Back";
            }
        }

        public void getDetail(string p)
        {            
            List<string> field = new List<string>();
            field.Add("dbo.shippingOrder_detail.batchnumber,dbo.product_model.model_name,dbo.packaging_order.expired");
            string from = "dbo.shippingOrder_detail INNER JOIN dbo.product_model ON dbo.shippingOrder_detail.productModelId = dbo.product_model.product_model INNER JOIN dbo.packaging_order ON dbo.packaging_order.batchNumber = dbo.shippingOrder_detail.batchnumber";
            string where = "dbo.shippingOrder_detail.shippingOrderNumber = '" + p + "' "; //AND dbo.packaging_order.status = '2' AND dbo.shippingOrder_detail.status = '0'";
            List<string[]> ds = db.selectList(field, from, where);
            if (db.num_rows > 0)
            {
                rv.lblBatch.Text = ds[0][0];
                rv.lblExp.Text = ds[0][2];
                rv.lblProduct.Text = ds[0][1];
            }
        }

        public void getDetail2(string p, string rOrder)
        {
            dataVaccine = new DataTable();
            List<string> field = new List<string>();
            field.Add("dbo.product_model.model_name,dbo.packaging_order.expired,dbo.packaging_order.batchNumber");
            string where = "batchNumber ='" + p + "' ";
            List<string[]> ds = db.selectList(field, "dbo.product_model INNER JOIN dbo.packaging_order ON dbo.product_model.product_model = dbo.packaging_order.productModelId", where);
            if (db.num_rows > 0)
            {
                rv2.lblBatch.Text = p;
                rv2.lblExp.Text = ds[0][1];
                rv2.lblProduct.Text = ds[0][0];
            }
        }

        public void getDataInfeed2(string p, bool isSas)
        {
            dataVaccine = new DataTable();
            string sql;
            if (!isSas)
            {
                sql = "SELECT a.innerBoxGsOneId as itemid, 'vial' as uom, " +
                            "(SELECT count(b.CapId) FROM Vaccine b WHERE b.batchNumber = a.batchNumber AND b.isReject = '0' AND b.innerBoxGsOneId = a.innerBoxGsOneId) as qty " +
                        "FROM Vaccine a " +
                        "WHERE a.isReject = '0' And a.basketid ='" + p + "'" +
                        "GROUP BY a.innerBoxGsOneId, a.batchNumber";
            }
            else
            {
                sql = "SELECT a.innerBoxGsOneId as itemid, 'vial' as uom, " +
                            "((SELECT count(b.CapId) FROM Vaccine b WHERE b.batchNumber = a.batchNumber AND b.isReject = '0' AND b.innerBoxGsOneId = a.innerBoxGsOneId) * c.outbox_qty)-sum(a.substraction) as qty " +
                        "FROM Vaccine a, product_model c " +
                        "WHERE a.productModelId = c.product_model AND a.isReject = '0' And a.basketid ='" + p + "'" +
                        "GROUP BY a.innerBoxGsOneId, a.batchNumber, c.outbox_qty";
            }
            db.OpenQuery(out dataVaccine, sql);  
            int totVial = 0;
            for (int i = 0; i <= dataVaccine.Rows.Count-1; i++)
            {
                totVial += Convert.ToInt32(dataVaccine.Rows[i]["qty"]);
            }
            rv2.lblTotalDetail.Text = "Total : " + dataVaccine.Rows.Count + " Innerbox, " + totVial + " Vial";    
            rv2.dgvVaccine.AutoGenerateColumns = false;
            rv2.dgvVaccine.DataSource = dataVaccine; 
        }
     
        public void refreshQty()
        {
            rv.lblQtyReceiving.Text = "" + rv.dgvReceiving.Rows.Count;
            rv.lblQtyHis.Text = "" + rv.dgvReceiving.Rows.Count;
        }

        public void InitialDgv()
        {
            dataVaccineScan = new DataTable();
            tableReceiv = new DataTable();

            tableReceiv.Columns.Add("itemid");
            tableReceiv.Columns.Add("qty_innerbox");
            tableReceiv.Columns.Add("uom");
            tableReceiv.Columns.Add("qty");
            tableReceiv.Columns.Add("uom2");

            rv.dgvReceiving.AutoGenerateColumns = false;
            rv.dgvReceiving.DataSource = tableReceiv;
            for (int i = 0; i < rv.dgvReceiving.Columns.Count; i++)
            {
                rv.dgvReceiving.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }

            rv.dgvVaccine.AutoGenerateColumns = false;
            rv.dgvVaccine.DataSource = dataVaccineScan;
            for (int i = 0; i < rv.dgvVaccine.Columns.Count; i++)
            {
                rv.dgvVaccine.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }

            rv.lblQtyHis.Text = "" + tableReceiv.Rows.Count;

            tablePicking = new DataTable();
            tablePicking.Columns.Add("Picking");
            tablePicking.Columns.Add("model_name");

            tableMovement = new DataTable();
            tableMovement.Columns.Add("itemid");
            tableMovement.Columns.Add("model_name");
            tableMovement.Columns.Add("expiry_date");
            tableMovement.Columns.Add("batch_number");
            tableMovement.Columns.Add("qty");
            tableMovement.Columns.Add("uom");
            tableMovement.Columns.Add("last_location");
            rv.dgvMovement.AutoGenerateColumns = false;
            rv.dgvMovement.DataSource = tableMovement;
            for (int i = 0; i < rv.dgvMovement.Columns.Count; i++)
            {
                rv.dgvMovement.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }

            getPendingData();
            disableDGV();
        }

        private void disableDGV()
        {
            foreach (DataGridViewColumn column in rv.dgvPending.Columns)
            {
                column.SortMode = DataGridViewColumnSortMode.NotSortable;
            }
        }
        DataTable dataPending; 
        public List<string[]> dataPen;
        public string getOrderQty(string dataOrder)
        {
            List<string> field = new List<string>();
            field.Add("SUM (dbo.receivingOrder_detail.qty) AS [Order Qty]");
            string where = "dbo.receiving_order.status = '0' AND dbo.receiving_order.receivingType = '1' and receiving_order.receivingOrderNumber = '"+dataOrder+"' GROUP BY dbo.receivingOrder_detail.receivingOrderNumber";
            string from = "dbo.receivingOrder_detail INNER JOIN dbo.receiving_order ON dbo.receivingOrder_detail.receivingOrderNumber = dbo.receiving_order.receivingOrderNumber";
            List<string[]> ds = db.selectList(field, from, where);
            if (db.num_rows > 0)
            {
                return ds[0][0];
            }
            return "0";

        }
        public void getPendingData()
        {
            dataPending = new DataTable();
            List<string> field = new List<string>();
            field.Add("dbo.receivingOrder_detail.receivingOrderNumber AS [Transfer Order],	sum(dbo.receivingOrder_detail.qty) AS [Order Qty],	SUM (COALESCE(PL.qty, 0)) AS [Actual Qty]");
            string where = "dbo.receiving_order.status = '0' AND dbo.receiving_order.receivingType = '1' GROUP BY  dbo.receivingOrder_detail.receivingOrderNumber";
            string from = "dbo.receivingOrder_detail INNER JOIN dbo.packaging_order ON dbo.packaging_order.batchNumber = dbo.receivingOrder_detail.batchnumber INNER JOIN dbo.product_model ON dbo.receivingOrder_detail.productModelId = dbo.product_model.product_model INNER JOIN dbo.receiving_order ON dbo.receivingOrder_detail.receivingOrderNumber= dbo.receiving_order.receivingOrderNumber LEFT JOIN (SELECT * FROM dbo.pickingList WHERE dbo.pickingList.status = '1') PL ON PL.detailNo = dbo.receivingOrder_detail.receivingOrderDetailNo";
            List<string[]> ds = db.selectList(field, from, where);
            if (db.num_rows > 0)
            {
                dataPending = db.dataHeader;
                for (int i = 0; i < ds.Count; i++)
                {
                    DataRow row = dataPending.NewRow();
                    for (int j = 0; j < ds[i].Length; j++)
                    {
                        row[j] = ds[i][j];

                        if (j == 5)
                        {
                            if (row[j].ToString().Length == 0)
                                row[j] = "0";
                        }

                    }
                    row[1] = getOrderQty(ds[i][0]);
                    dataPending.Rows.Add(row);
                    rv.dgvPending.DataSource = dataPending;
                }
                rv.lblQtyReceiving.Text = ""+ds.Count;
            }
            else
            {
                dataPending = db.dataHeader;
                rv.dgvPending.DataSource = dataPending;
                rv.lblQtyReceiving.Text = "0";
            }
        }
      
        public void StartStation()
        {
            sqlite = new sqlitecs();
            db = new dbaccess();
            InitialDgv();
            db.adminid = adminid;
            db.from = "Receiving Station";
            db.eventname = "Start Station";
            db.systemlog();
            SetComboLocation();
        }

        public void StartStation2()
        {
            sqlite = new sqlitecs();
            db = new dbaccess();
            db.adminid = adminid;
            db.from = "Receiving Station";
            db.eventname = "Start Station";
            db.systemlog();
        }

        public void updatePickinglist(DataTable databarcode)
        {
            //Dictionary<string, string> field;
            //field = new Dictionary<string, string>();
            //field.Add("status", "1");
            //string where = "itemid in (";
            //foreach (DataRow row2 in databarcode.Rows)
            //{
            //    where = where + "'" + row2["Picking"].ToString() + "',";
            //}
            //where = where.Substring(0, where.Length - 1) + ")";
            //db.update(field, "pickinglist", where);

            //db.eventname = "Receive data " + databarcode;
            //db.systemlog();

            //string dates = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            //db.MovementMulti("", "", "Receiving", "1", databarcode);
            //foreach (DataRow row2 in databarcode.Rows)
            //{
            //    tableReceiv.Rows.Add(row2["Picking"].ToString(), dates, row2["model_name"].ToString());
            //}
            //rv.dgvReceiving.AutoGenerateColumns = false;
            //rv.dgvReceiving.DataSource = tableReceiv;
            //rv.lblQtyHis.Text = "" + tableReceiv.Rows.Count;
            //log("Set at Picking list");

            //List<string> field1 = new List<string>();
            //field1.Add("itemid,detailno,dbo.receivingOrder_detail.batchnumber,receivingOrder_detail.receivingOrderNumber,receivingOrder_detail.receivingOrderDetailNo");
            //string from = "pickinglist INNER JOIN dbo.receivingOrder_detail ON dbo.pickingList.detailNo = dbo.receivingOrder_detail.receivingOrderDetailNo";
            //where = "itemid ='" + databarcode + "' and receivingOrder_detail.status = '0' and pickinglist.status = '1'";
            //List<string[]> ds1 = db.selectList(field1, from, where);
            //if (db.num_rows > 0)
            //{
            //    cekDetailFull(ds1[0][4]);
            //    cekOrderFull(ds1[0][3]);
            //}
        }

        private bool cekBasketsqlite(string basket)
        {
            List<string> field = new List<string>();
            field.Add("basketid");
            string where = "basketid ='" + basket + "'";
            List<string[]> ds = sqlite.select(field, "warehouse", where);
            if (sqlite.num_rows > 0)
            {
                return true;
            }
            return false;
        }

        public void log(string data)
        {
            try
            {
                string dates = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                string simpan = dates + "\t" + data + "\n";
                rv.LogFile = rv.LogFile +  simpan + Environment.NewLine;
                db.simpanLog(simpan);
            }
            catch (ObjectDisposedException ob)
            {
            }
        }

        string uom ;
        string idBasket;
        internal void getDataDetail(string p, string rOrder)
        {
            rv.grpBoxDetail.Visible = true;
            rv.grpPending.Visible = false;
            idBasket = p;
            uom = "Awal";
            cekUom(p,uom);
            rv.lblPar.Visible = false;
            rv.lblParent.Visible = false;
        }       

        public DataTable dataVaccine;
        public DataTable dataVaccineScan;
        DataTable dataInnerbox;

        private void getDataInfeed(string p)
        {
            dataVaccine = new DataTable();
            List<string> field = new List<string>();
            field.Add("Vaccine.innerBoxGsOneId 'itemid', '1' 'qty', 'Innerbox' 'uom'");
            string where = "Vaccine.isReject = '0' And basketid ='" + p + "' GROUP BY innerBoxGsOneId";
            List<string[]> ds = db.selectList(field, "Vaccine", where);
            if (db.num_rows > 0)
            {
                dataVaccine = db.dataHeader;
                for (int i = 0; i < ds.Count; i++)
                {
                    DataRow row = dataVaccine.NewRow();
                    for (int j = 0; j < ds[i].Length; j++)
                    {
                        row[j] = ds[i][j];
                    }
                    dataVaccine.Rows.Add(row);
                    rv.dgvVaccine.AutoGenerateColumns = false;
                    rv.dgvVaccine.DataSource = dataVaccine;
                }
            }
            else
            {
                dataInnerbox = db.dataHeader;
            }
        }
       
        private void getDataVaccine(string p)
        {
            dataVaccine = new DataTable();
            List<string> field = new List<string>();
            field.Add("gsOneVialId 'itemid', '1' 'qty', 'Vial' 'uom'");
            string where = "Vaccine.isReject = '0' And innerBoxGsOneId ='" + p + "'";
            List<string[]> ds = db.selectList(field, "Vaccine", where); 
            if(db.num_rows>0)
            {
                dataVaccine = db.dataHeader;
                for (int i = 0; i < ds.Count; i++)
                {
                    DataRow row = dataVaccine.NewRow();
                    for (int j = 0; j < ds[i].Length; j++)
                    {
                        row[j] = ds[i][j];
                    }
                    dataVaccine.Rows.Add(row);
                    rv.dgvVaccine.AutoGenerateColumns = false;
                    rv.dgvVaccine.DataSource = dataVaccine;
                }
            }
            else
            {
                dataVaccine = db.dataHeader;
            }
        }

        public void getDataVaccine2(string p)
        {
            dataVaccine = new DataTable();
            List<string> field = new List<string>();
            field.Add("gsOneVialId 'itemid', '1' 'qty', 'Vial' 'uom'");
            string where = "Vaccine.isReject = '0' And innerBoxGsOneId ='" + p + "'";
            List<string[]> ds = db.selectList(field, "Vaccine", where);
            if (db.num_rows > 0)
            {
                dataVaccine = db.dataHeader;
                for (int i = 0; i < ds.Count; i++)
                {
                    DataRow row = dataVaccine.NewRow();
                    for (int j = 0; j < ds[i].Length; j++)
                    {
                        row[j] = ds[i][j];
                    }
                    dataVaccine.Rows.Add(row);
                    rv2.dgvVaccine.AutoGenerateColumns = false;
                    rv2.dgvVaccine.DataSource = dataVaccine;
                }
            }
            else
            {
                dataVaccine = db.dataHeader;
            }
        }

        private void getDataBakset(string p, string batch, string d)
        {
            string sql;
            //IsSas = 0
            sql = "SELECT dbo.pickingList.itemId, pickinglist.qty, receivingOrder_detail.batchnumber, 'innerbox' as uom, " +
                    "product_model.model_name, dbo.receivingOrder_detail.uom as uom2, count(dbo.innerBox.basketId) as qty_innerbox, dbo.product_model.isSAS " +
                    "FROM dbo.pickingList INNER JOIN dbo.receivingOrder_detail ON dbo.pickingList.detailNo = dbo.receivingOrder_detail.receivingOrderDetailNo " +
                    "INNER JOIN dbo.packaging_order ON dbo.packaging_order.batchNumber = receivingOrder_detail.batchnumber " +
                    "INNER JOIN dbo.product_model ON dbo.product_model.product_model = dbo.packaging_order.productModelId " +
                    "INNER JOIN dbo.innerBox ON dbo.innerBox.basketId = dbo.pickingList.itemId " +
                    "where dbo.innerBox.isReject = 0 and receivingOrder_detail.status = '0' and pickinglist.status = '0' " +
                    " AND dbo.innerBox.batchNumber = receivingOrder_detail.batchnumber ";
            if (p.Length > 0)
            {
                sql += " AND receivingOrder_detail.receivingOrderNumber ='" + p + "'";
            }
            if (d.Length > 0)
            {
                sql += " AND dbo.pickingList.itemId='" + d + "'";
            }
            sql += " GROUP BY dbo.pickingList.itemId, dbo.receivingOrder_detail.batchnumber, " +
                    "product_model.model_name,dbo.pickingList.uom, dbo.pickinglist.qty, " +
                    "dbo.receivingOrder_detail.uom, dbo.product_model.isSAS ";

            //sql += " UNION ";

            ////IsSas = 1
            //sql += "SELECT dbo.pickingList.itemId, dbo.pickinglist.qty, receivingOrder_detail.batchnumber, 'innerbox' as uom, " +
            //        "product_model.model_name, dbo.receivingOrder_detail.uom as uom2, count(dbo.innerBox.basketId) as qty_innerbox, dbo.product_model.isSAS " +
            //        "FROM dbo.pickingList INNER JOIN dbo.receivingOrder_detail ON dbo.pickingList.detailNo = dbo.receivingOrder_detail.receivingOrderDetailNo " +
            //        "INNER JOIN dbo.packaging_order ON dbo.packaging_order.batchNumber = receivingOrder_detail.batchnumber " +
            //        "INNER JOIN dbo.product_model ON dbo.product_model.product_model = dbo.packaging_order.productModelId " +
            //        "INNER JOIN dbo.innerBox ON dbo.innerBox.basketId = dbo.pickingList.itemId " +
            //        "where dbo.innerBox.isReject = 0 and receivingOrder_detail.status = '0' and pickinglist.status = '0' " +
            //        " AND dbo.innerBox.batchNumber = receivingOrder_detail.batchnumber AND dbo.product_model.isSAS = 1 ";
            //if (p.Length > 0)
            //{
            //    sql += " AND receivingOrder_detail.receivingOrderNumber ='" + p + "'";
            //}
            //if (d.Length > 0)
            //{
            //    sql += " AND dbo.pickingList.itemId='" + d + "'";
            //}
            //sql += " GROUP BY dbo.pickingList.itemId, dbo.receivingOrder_detail.batchnumber, " +
            //        "product_model.model_name, dbo.pickingList.uom, dbo.pickinglist.qty * dbo.product_model.outbox_qty, " +
            //        "dbo.receivingOrder_detail.uom, dbo.product_model.isSAS ";

            db.OpenQuery(out dataVaccine, sql);
            rv.dgvVaccine.AutoGenerateColumns = false;
            rv.dgvVaccine.DataSource = dataVaccine;
            rv.lblQty.Text = dataVaccine.Rows.Count.ToString() + " Basket";
            if (dataVaccine.Rows.Count > 0)
            {
                for (int i = 0; i < rv.dgvVaccine.Columns.Count; i++)
                {
                    rv.dgvVaccine.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                }
            }
            else {
                for (int i = 0; i < rv.dgvVaccine.Columns.Count; i++)
                {
                    rv.dgvVaccine.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                }
            }
            
        }

        private void getDataBaksetReceive(string p, string batch, string d)
        {
            string sql;  
            //isSas = 0
            sql = "SELECT dbo.pickingList.itemId, pickinglist.qty, receivingOrder_detail.batchnumber, 'innerbox' as uom, " +
                        "product_model.model_name, dbo.receivingOrder_detail.uom as uom2, " +
                        "(SELECT COUNT(*) FROM Innerbox a WHERE a.isReject = 0 AND a.basketId = dbo.pickingList.itemId) as qty_innerbox, " +
                        "product_model.isSas " +
                    "FROM dbo.pickingList INNER JOIN dbo.receivingOrder_detail ON dbo.pickingList.detailNo = dbo.receivingOrder_detail.receivingOrderDetailNo " +
                        "INNER JOIN dbo.packaging_order ON dbo.packaging_order.batchNumber = receivingOrder_detail.batchnumber " +
                        "INNER JOIN dbo.product_model ON dbo.product_model.product_model = dbo.packaging_order.productModelId " +
                    "WHERE receivingOrder_detail.status = '0' and pickinglist.status = '1' "+ //AND dbo.product_model.isSas = 0 " +
                        " AND exists(select basketid from vaccine where vaccine.basketId = pickinglist.itemId and lastLocationId is null)";            
            if (p.Length > 0)
            {
                sql += " AND receivingOrder_detail.receivingOrderNumber ='" + p + "'";
            }
            if (d.Length > 0)
            {
                sql += " AND dbo.pickingList.itemId='" + d + "'";
            }

            //isSas = 1
            //sql += " UNION ";
            //sql += "SELECT dbo.pickingList.itemId, pickinglist.qty * dbo.product_model.outbox_qty, receivingOrder_detail.batchnumber, 'innerbox' as uom, " +
            //            "product_model.model_name, dbo.receivingOrder_detail.uom as uom2, " +
            //            "(SELECT COUNT(*) FROM Innerbox a WHERE a.isReject = 0 AND a.basketId = dbo.pickingList.itemId) as qty_innerbox, " +
            //            "product_model.isSas " +
            //        "FROM dbo.pickingList INNER JOIN dbo.receivingOrder_detail ON dbo.pickingList.detailNo = dbo.receivingOrder_detail.receivingOrderDetailNo " +
            //            "INNER JOIN dbo.packaging_order ON dbo.packaging_order.batchNumber = receivingOrder_detail.batchnumber " +
            //            "INNER JOIN dbo.product_model ON dbo.product_model.product_model = dbo.packaging_order.productModelId " +
            //        "WHERE receivingOrder_detail.status = '0' and pickinglist.status = '1' AND dbo.product_model.isSas = 1 " +
            //            " AND exists(select basketid from vaccine where vaccine.basketId = pickinglist.itemId and lastLocationId is null)";
            //if (p.Length > 0)
            //{
            //    sql += " AND receivingOrder_detail.receivingOrderNumber ='" + p + "'";
            //}
            //if (d.Length > 0)
            //{
            //    sql += " AND dbo.pickingList.itemId='" + d + "'";
            //}

            db.OpenQuery(out tableReceiv, sql);
            rv.dgvReceiving.AutoGenerateColumns = false;
            rv.dgvReceiving.DataSource = tableReceiv;
            rv.lblQtyHis.Text = tableReceiv.Rows.Count.ToString() + " Basket";
            if (tableReceiv.Rows.Count > 0)
            {
                for (int i = 0; i < rv.dgvReceiving.Columns.Count; i++)
                {
                    rv.dgvReceiving.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                }
            }
            else {
                for (int i = 0; i < rv.dgvReceiving.Columns.Count; i++)
                {
                    rv.dgvReceiving.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                }
            }
        }

        public void getDataBakset2(string p, string batch, string d)
        {            
            string sql;
            sql = "SELECT dbo.pickingList.itemId,pickinglist.qty,receivingOrder_detail.batchnumber,'Basket' 'uom' " +
                    "FROM dbo.pickingList INNER JOIN dbo.receivingOrder_detail ON dbo.pickingList.detailNo = dbo.receivingOrder_detail.receivingOrderDetailNo where ";
            bool ptr = false;
            if (p.Length > 0)
            {
                ptr = true;
                sql += " receivingOrder_detail.receivingOrderNumber ='" + p + "'";
            }
            if (d.Length > 0)
            {
                if (ptr)
                    sql += " AND ";
                sql += "dbo.pickingList.itemId='" + d + "'";
            }
            List<string[]> ds = db.selectList(sql);
            if (db.num_rows > 0)
            {
                dataVaccine = db.dataHeader;
                for (int i = 0; i < ds.Count; i++)
                {
                    DataRow row = dataVaccine.NewRow();
                    for (int j = 0; j < ds[i].Length; j++)
                    {
                        row[j] = ds[i][j];
                    }
                    dataVaccine.Rows.Add(row);
                    rv2.dgvVaccine.AutoGenerateColumns = false;
                    rv2.dgvVaccine.DataSource = dataVaccine;
                    rv2.lblQty.Text = dataVaccine.Rows.Count.ToString() + " Basket";
                }
            }
            else
            {
                dataVaccine = db.dataHeader;
                rv2.dgvVaccine.AutoGenerateColumns = false;
                rv2.dgvVaccine.DataSource = dataVaccine;
            }
        }

        internal void refresh()
        {
            dataVaccine = new DataTable();
            dataVaccineScan = new DataTable();
            dataInnerbox = new DataTable();
            tablePicking = new DataTable();
            tablePicking.Columns.Add("Picking");
            tablePicking.Columns.Add("model_name");
            rv.dgvVaccine.AutoGenerateColumns = false;
            rv.dgvVaccine.DataSource = dataVaccine;
        }

        public void cekUom(string p, string q, string batch,string itemid)
        {
            uom = q;
            if (uom.Equals("Awal"))
            {
                getDataBakset(p, batch,itemid);
                rv.lblParent.Text = p;

                rv.lblParentUom.Text = p;
                rv.lblUom.Text = "Basket";
            }
            else if (uom.Equals("Basket"))
            {
                uom = "vial";
                getDataInfeed(p);
                rv.lblParent.Text = p;
                rv.lblUom.Text = "Innerbox";
                getDetail(batch, "");
                rv.lblParentUom.Text = "Basket";
            }
            else if (uom.Equals("Innerbox"))
            {
                getDataVaccine(p);
                rv.lblParent.Text = p;
                rv.lblUom.Text = "Vaccine";
                uom = "Akhir";
                getDetail(batch, "");
                rv.lblParentUom.Text = "Innerbox";
            }
            else
            {           
            }
        }

        public void cekUom(string p, string q, string batch)
        {
            uom = q;
            if(uom.Equals("Awal"))
            {
                getDataBakset(p, batch,"");
                getDataBaksetReceive(p, batch, "");

                rv.lblParent.Text = p;
                rv.lblUom.Text = "Basket";

                rv.lblParentUom.Text = p;

                rv.lblBatch.Text = "";
                rv.lblExp.Text = "";
                rv.lblProduct.Text = "";
            }
            else if (uom.Equals("Basket"))
            {
                uom = "vial";
                getDataInfeed(p);
                rv.lblParent.Text = p;
                rv.lblUom.Text = "Innerbox";
                getDetail(batch,"");

                rv.lblParentUom.Text = "Basket";
            }
            else if (uom.Equals("Innerbox"))
            {
                getDataVaccine(p);
                rv.lblParent.Text = p;
                rv.lblUom.Text = "Vaccine";
                uom = "Akhir";

                getDetail(batch, "");
                rv.lblParentUom.Text = "Innerbox";
            }
            else
            {
                getDataBakset(p, "","");
                rv.lblParent.Text = p;

                rv.lblUom.Text = "Basket";
                rv.lblParentUom.Text = p;
            }
        }

        public void cekUom(string p, string q)
        {
            uom = q;
            if (uom.Equals("Awal"))
            {
               // uom = "Basket";
                getDataBakset(p, "","");
                rv.lblParent.Visible = false;
                rv.lblPar.Visible = false;
                rv.lblParent.Text = p;
                rv.lblParentUom.Visible = false;
                rv.lblParUom.Visible = false;
                rv.lblParentUom.Text = p;
            }
            else if (uom.Equals("Basket"))
            {
                uom = "Innerbox";

                getDetail(q, "");
                getDataInfeed(p);
                rv.lblPar.Visible = true;
                rv.lblParent.Visible = true;
                rv.lblParent.Text = p;

                rv.lblUom.Text = "Innerbox";
                rv.lblParentUom.Visible = true;
                rv.lblParUom.Visible = true;
                rv.lblParentUom.Text = "Basket";
            }
            else if (uom.Equals("Innerbox"))
            {
                getDataVaccine(p);
                rv.lblParent.Visible = true;
                rv.lblPar.Visible = true;
                rv.lblParent.Text = p;
                uom = "Akhir";

                rv.lblUom.Text = "Vaccine";
                rv.lblParentUom.Visible = true;
                rv.lblParUom.Visible = true;
                rv.lblParentUom.Text = "Innerbox";
            }
            else
            {  // uom = "Basket";
                getDataBakset(p, "","");
                rv.lblParent.Visible = false;
                rv.lblPar.Visible = false;
                rv.lblParent.Text = p;

                rv.lblUom.Text = "Basket";
                rv.lblParentUom.Visible = false;
                rv.lblParUom.Visible = false;
                rv.lblParentUom.Text = p;
            }
        }

        private void getDetail(string p, string rOrder)
        {
            dataVaccine = new DataTable();
            List<string> field = new List<string>();
            field.Add("dbo.product_model.model_name,dbo.packaging_order.expired,dbo.packaging_order.batchNumber");
            string where = "batchNumber ='" + p + "' ";
            List<string[]> ds = db.selectList(field, "dbo.product_model INNER JOIN dbo.packaging_order ON dbo.product_model.product_model = dbo.packaging_order.productModelId", where);
            if(db.num_rows>0)
            {
                rv.lblBatch.Text = p;
                rv.lblExp.Text = ds[0][1];
                rv.lblProduct.Text = ds[0][0];
            }            
        }

        //MOVEMENT
        public void tambahDataMove(string dataBarcode)
        {
            //dataBarcode = dataBarcode.Replace("/n", "");
            //dataBarcode = dataBarcode.Replace("/r", "");
            dataBarcode = DeleteSpecialChar(dataBarcode);
            //cek data di db 
            string locationid = rv.cmbMoveNewLocation.SelectedValue.ToString();

            if (cekproduct(dataBarcode, "1"))
            {
                if (!cekBasketsqliteMove(dataBarcode))
                {
                    if (!cekdgvReceiving(dataBarcode, rv.dgvMovement))
                    {
                        if (!cekBasketLocationsqliteMove(dataBarcode, locationid))
                        {
                            reciveSqliteMove(dataBarcode, locationid);
                        }
                        else
                        {
                            log("Data " + dataBarcode + " already exist in location " + rv.cmbMoveNewLocation.Text);
                            new Confirm(" already exist in location " + rv.cmbMoveNewLocation.Text, "Information", MessageBoxButtons.OK);
                        }
                    }
                    else
                    {
                        log("Data " + dataBarcode + " already exist");
                        new Confirm(" already exist", "Information", MessageBoxButtons.OK);
                    }
                }
                else
                {
                    log("Data " + dataBarcode + " do not exist");
                    new Confirm(" do not exist", "Information", MessageBoxButtons.OK);
                }
            }
            else
            {
                log("Data not found");
                new Confirm("Data not found", "Information", MessageBoxButtons.OK);
            }
        }

        private bool cekBasketsqliteMove(string basket)
        {
            List<string> field = new List<string>();
            field.Add("basketid");
            string where = "basketid ='" + basket + "'";
            List<string[]> ds = sqlite.select(field, "warehouse", where);
            if (sqlite.num_rows > 0)            
                return false;
            else
                return true;
        }

        private bool cekBasketLocationsqliteMove(string basket, string location)
        {
            string sql;
            sql = "SELECT COUNT(CapId) FROM Vaccine WHERE basketId = '" + basket + "'" +
                    " AND LastLocationId = " + location;
            DataTable dtTmp = new DataTable();
            db.OpenQuery(out dtTmp, sql);
            if (Convert.ToInt32(dtTmp.Rows[0][0]) > 0)
                return true;
            else
                return false;
        }

        bool cekdgvReceiving(string data, DataGridView dgv)
        {
            for (int i = 0; i < dgv.Rows.Count; i++)
            {
                if (data.Equals(dgv.Rows[i].Cells[0].Value.ToString()))
                {
                    return true;
                }
            }
            return false;
        }

        public DataTable tableMovement;
        private void reciveSqliteMove(string basketid, string locationid)
        {
            //insert ke sqlite 
            string dates = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            Dictionary<string, string> field = new Dictionary<string, string>();
            field.Add("basketid", basketid);
            field.Add("timestamp", dates);

            cekproduct(basketid, "1");
            db.Movement("", "Movement", "", "1", basketid);
            // masukan ke table
            tableMovement.Rows.Add(
                basketid,
                dtProduct.Rows[0]["model_name"].ToString(),
                dtProduct.Rows[0]["expDate"],
                dtProduct.Rows[0]["batchNumber"].ToString(),
                dtProduct.Rows[0]["qty"],
                dtProduct.Rows[0]["uom"].ToString(),
                dtProduct.Rows[0]["warehouseName"].ToString());

            rv.dgvMovement.AutoGenerateColumns = false;
            rv.dgvMovement.DataSource = tableMovement;

            for (int i = 0; i < rv.dgvMovement.Columns.Count; i++)
            {
                rv.dgvMovement.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            }
            rv.lblQtyMovement.Text = rv.dgvMovement.Rows.Count + " Basket";
            rv.cmbMoveNewLocation.Enabled = false;
        }

        public void hapusdataMove(string barcode)
        {
            if (barcode.Length > 0)
                barcode = "basketid ='" + barcode + "'";
            sqlite.delete(barcode, "warehouseMove");
        }

        public void ClearDgvMovement()
        {
            tableMovement.Rows.Clear();
            rv.dgvMovement.Refresh();
            for (int i = 0; i < rv.dgvMovement.Columns.Count; i++)
            {
                rv.dgvMovement.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }

            rv.lblQtyMovement.Text = "0 Basket";
            rv.dgvMovement.DataSource = tableMovement;
            hapusdataMove("");
            rv.cmbMoveNewLocation.Enabled = true;
            rv.cmbMoveNewLocation.SelectedIndex = -1;
        }

        internal void submitReceivingMove()
        {
            for (int i = 0; i <= tableMovement.Rows.Count-1; i++)
            {
                //update basketdi location
                Dictionary<string, string> field = new Dictionary<string, string>();
                field.Add("locationid", rv.cmbMoveNewLocation.SelectedValue.ToString());
                field.Add("warehouseid", rv.cmbMoveNewLocation.SelectedValue.ToString());
                string where = "basketid ='" + tableMovement.Rows[i]["itemid"].ToString() + "'";
                db.update(field, "basket", where);

                //update sqlite
                string where1 = "basketid ='" + tableMovement.Rows[i]["itemid"].ToString() + "'";
                sqlite.update(field, "warehouse", where1);

                //update location id
                updateVaccine(tableMovement.Rows[i]["itemid"].ToString(), rv.cmbMoveNewLocation.SelectedValue.ToString(), false);
                db.Movement("", "Movement", "", "" + tableMovement.Rows.Count, "");
            }
            log("Data has been moved");
            InsertSystemLog("Data has been moved to " + rv.cmbMoveNewLocation.SelectedValue.ToString());
            new Confirm("Data has been moved", "Information", MessageBoxButtons.OK);
            ClearDgvMovement();
        }

        public void InsertSystemLog(string EventName) {
            db.eventname = EventName;
            db.systemlog();
        }

    }
}
