﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace Receiving_Station
{
    public partial class DetailVaccine : Form
    {
        Control ms;
        string uom;
        bool isSas;
  
        public DetailVaccine(string tmpP, string tmpQ, String tmpBatch, int tmpQty, string orderNumbeer, bool tmpisSas)
        {
            InitializeComponent();
            ms = new Control();
            ms.rv2 = this;
            ms.StartStation2();
            isSas = tmpisSas;
            cekUom(tmpP, tmpQ, tmpBatch);
            lblOrder.Text = orderNumbeer;
            lblQty.Text = System.Convert.ToString(tmpQty);            
        }

        public void cekUom(string p, string q, string batch)
        {
            uom = q;

            if (uom.Equals("Awal"))
            {
                ms.getDataBakset2(p, batch, "");

                lblParent.Text = p;
                lblUom.Text = "Basket";

                lblParentUom.Text = p;

                lblBatch.Text = "";
                lblExp.Text = "";
                lblProduct.Text = "";
            }
            else if (uom.Equals("Basket"))
            {
                uom = "vial";
                ms.getDataInfeed2(p, isSas);
                lblParent.Text = p;
                lblUom.Text = "Innerbox";
                ms.getDetail2(batch, "");

                lblParentUom.Text = "Innerbox";
            }
            else if (uom.Equals("Innerbox"))
            {
                ms.getDataVaccine2(p);
                lblParent.Text = p;
                lblUom.Text = "Vaccine";
                uom = "Akhir";

                ms.getDetail2(batch, "");
                lblParentUom.Text = "Vial";
            }
            else
            {
                ms.getDataBakset2(p, "", "");
                lblParent.Text = p;

                lblUom.Text = "Basket";
                lblParentUom.Text = p;
            }
        }

    }
}
