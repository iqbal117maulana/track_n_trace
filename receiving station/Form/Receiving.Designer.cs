﻿namespace Receiving_Station
{
    partial class Receiving
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Receiving));
            this.dgvReceiving = new System.Windows.Forms.DataGridView();
            this.itemid2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.qty_innerbox2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.uom22 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.qty2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.uom33 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label8 = new System.Windows.Forms.Label();
            this.txtBarcode = new System.Windows.Forms.TextBox();
            this.lblScan = new System.Windows.Forms.Label();
            this.grpPending = new System.Windows.Forms.GroupBox();
            this.dgvPending = new System.Windows.Forms.DataGridView();
            this.lblQtyReceiving = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.grpHistory = new System.Windows.Forms.GroupBox();
            this.btnConfirmLocation = new System.Windows.Forms.Button();
            this.cmbLocation = new System.Windows.Forms.ComboBox();
            this.label34 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.lblQtyHis = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.button4 = new System.Windows.Forms.Button();
            this.dgvVaccine = new System.Windows.Forms.DataGridView();
            this.itemid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.qty_innerbox = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.uom = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.qty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.uom2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.model_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.batchnumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.isSAS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label7 = new System.Windows.Forms.Label();
            this.lblQty = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblBatch = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.lblExp = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.lblProduct = new System.Windows.Forms.Label();
            this.grpBoxDetail = new System.Windows.Forms.GroupBox();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.lblUom = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.btnYes = new System.Windows.Forms.Button();
            this.lblParentUom = new System.Windows.Forms.Label();
            this.lblParUom = new System.Windows.Forms.Label();
            this.lblParent = new System.Windows.Forms.Label();
            this.lblPar = new System.Windows.Forms.Label();
            this.lblOrder = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.grpDetail = new System.Windows.Forms.GroupBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblDate = new System.Windows.Forms.Label();
            this.lblTime = new System.Windows.Forms.Label();
            this.lblRole = new System.Windows.Forms.Label();
            this.lblUserId = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.txtOrderNumber = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.button10 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.lblQtyMovement = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.dgvMovement = new System.Windows.Forms.DataGridView();
            this.itemid3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.model_name3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.expiry_date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.batch_number = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.qty3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.uom3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.last_location = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cmbMoveLocation = new System.Windows.Forms.ComboBox();
            this.label39 = new System.Windows.Forms.Label();
            this.cmbMoveNewLocation = new System.Windows.Forms.ComboBox();
            this.label40 = new System.Windows.Forms.Label();
            this.txtBasketMove = new System.Windows.Forms.TextBox();
            this.label41 = new System.Windows.Forms.Label();
            this.button12 = new System.Windows.Forms.Button();
            this.btnOpenLog = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.btnConfig = new System.Windows.Forms.Button();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            ((System.ComponentModel.ISupportInitialize)(this.dgvReceiving)).BeginInit();
            this.grpPending.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPending)).BeginInit();
            this.grpHistory.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVaccine)).BeginInit();
            this.grpBoxDetail.SuspendLayout();
            this.grpDetail.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMovement)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvReceiving
            // 
            this.dgvReceiving.AllowUserToAddRows = false;
            this.dgvReceiving.AllowUserToDeleteRows = false;
            this.dgvReceiving.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dgvReceiving.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvReceiving.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvReceiving.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvReceiving.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvReceiving.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.itemid2,
            this.qty_innerbox2,
            this.uom22,
            this.qty2,
            this.uom33});
            this.dgvReceiving.Location = new System.Drawing.Point(11, 49);
            this.dgvReceiving.Name = "dgvReceiving";
            this.dgvReceiving.ReadOnly = true;
            this.dgvReceiving.RowHeadersVisible = false;
            this.dgvReceiving.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvReceiving.Size = new System.Drawing.Size(419, 391);
            this.dgvReceiving.TabIndex = 7;
            // 
            // itemid2
            // 
            this.itemid2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.itemid2.DataPropertyName = "itemid";
            this.itemid2.HeaderText = "Basket Id";
            this.itemid2.Name = "itemid2";
            this.itemid2.ReadOnly = true;
            this.itemid2.Width = 102;
            // 
            // qty_innerbox2
            // 
            this.qty_innerbox2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.qty_innerbox2.DataPropertyName = "qty_innerbox";
            dataGridViewCellStyle2.Format = "n0";
            this.qty_innerbox2.DefaultCellStyle = dataGridViewCellStyle2;
            this.qty_innerbox2.HeaderText = "Qty";
            this.qty_innerbox2.Name = "qty_innerbox2";
            this.qty_innerbox2.ReadOnly = true;
            this.qty_innerbox2.Width = 58;
            // 
            // uom22
            // 
            this.uom22.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.uom22.DataPropertyName = "uom";
            this.uom22.HeaderText = "Uom";
            this.uom22.Name = "uom22";
            this.uom22.ReadOnly = true;
            this.uom22.Width = 68;
            // 
            // qty2
            // 
            this.qty2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.qty2.DataPropertyName = "qty";
            dataGridViewCellStyle3.Format = "n0";
            this.qty2.DefaultCellStyle = dataGridViewCellStyle3;
            this.qty2.HeaderText = "Qty";
            this.qty2.Name = "qty2";
            this.qty2.ReadOnly = true;
            this.qty2.Width = 58;
            // 
            // uom33
            // 
            this.uom33.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.uom33.DataPropertyName = "uom2";
            this.uom33.HeaderText = "Uom";
            this.uom33.Name = "uom33";
            this.uom33.ReadOnly = true;
            this.uom33.Width = 68;
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(2, 205);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(76, 20);
            this.label8.TabIndex = 9;
            this.label8.Text = "Quantity :";
            // 
            // txtBarcode
            // 
            this.txtBarcode.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBarcode.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.txtBarcode.Location = new System.Drawing.Point(130, 463);
            this.txtBarcode.Name = "txtBarcode";
            this.txtBarcode.Size = new System.Drawing.Size(326, 26);
            this.txtBarcode.TabIndex = 11;
            this.txtBarcode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtBarcode_KeyDown);
            this.txtBarcode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBarcode_KeyPress);
            // 
            // lblScan
            // 
            this.lblScan.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblScan.AutoSize = true;
            this.lblScan.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.lblScan.Location = new System.Drawing.Point(7, 466);
            this.lblScan.Name = "lblScan";
            this.lblScan.Size = new System.Drawing.Size(122, 20);
            this.lblScan.TabIndex = 10;
            this.lblScan.Text = "Scan Barcode : ";
            // 
            // grpPending
            // 
            this.grpPending.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.grpPending.Controls.Add(this.dgvPending);
            this.grpPending.Controls.Add(this.lblQtyReceiving);
            this.grpPending.Controls.Add(this.label8);
            this.grpPending.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpPending.Location = new System.Drawing.Point(21, 323);
            this.grpPending.Name = "grpPending";
            this.grpPending.Size = new System.Drawing.Size(512, 231);
            this.grpPending.TabIndex = 9;
            this.grpPending.TabStop = false;
            this.grpPending.Text = "Warehouse Transfer Order";
            // 
            // dgvPending
            // 
            this.dgvPending.AllowUserToAddRows = false;
            this.dgvPending.AllowUserToDeleteRows = false;
            this.dgvPending.AllowUserToResizeColumns = false;
            this.dgvPending.AllowUserToResizeRows = false;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dgvPending.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvPending.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvPending.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvPending.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPending.Location = new System.Drawing.Point(6, 25);
            this.dgvPending.MultiSelect = false;
            this.dgvPending.Name = "dgvPending";
            this.dgvPending.ReadOnly = true;
            this.dgvPending.RowHeadersVisible = false;
            this.dgvPending.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvPending.Size = new System.Drawing.Size(500, 180);
            this.dgvPending.TabIndex = 7;
            this.dgvPending.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvPending_CellDoubleClick);
            this.dgvPending.SelectionChanged += new System.EventHandler(this.dgvPending_SelectionChanged);
            // 
            // lblQtyReceiving
            // 
            this.lblQtyReceiving.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblQtyReceiving.AutoSize = true;
            this.lblQtyReceiving.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQtyReceiving.Location = new System.Drawing.Point(84, 205);
            this.lblQtyReceiving.Name = "lblQtyReceiving";
            this.lblQtyReceiving.Size = new System.Drawing.Size(18, 20);
            this.lblQtyReceiving.TabIndex = 10;
            this.lblQtyReceiving.Text = "0";
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.button1.Location = new System.Drawing.Point(941, 529);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(150, 40);
            this.button1.TabIndex = 135;
            this.button1.Text = "Clear Log";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // grpHistory
            // 
            this.grpHistory.Controls.Add(this.dgvReceiving);
            this.grpHistory.Controls.Add(this.btnConfirmLocation);
            this.grpHistory.Controls.Add(this.cmbLocation);
            this.grpHistory.Controls.Add(this.label34);
            this.grpHistory.Controls.Add(this.label33);
            this.grpHistory.Controls.Add(this.lblQtyHis);
            this.grpHistory.Controls.Add(this.label2);
            this.grpHistory.Controls.Add(this.label3);
            this.grpHistory.Controls.Add(this.groupBox1);
            this.grpHistory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpHistory.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.grpHistory.Location = new System.Drawing.Point(0, 0);
            this.grpHistory.Name = "grpHistory";
            this.grpHistory.Size = new System.Drawing.Size(436, 500);
            this.grpHistory.TabIndex = 136;
            this.grpHistory.TabStop = false;
            this.grpHistory.Text = "Receive";
            // 
            // btnConfirmLocation
            // 
            this.btnConfirmLocation.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnConfirmLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConfirmLocation.Image = global::Receiving_Station.Properties.Resources.submit;
            this.btnConfirmLocation.Location = new System.Drawing.Point(249, 450);
            this.btnConfirmLocation.Name = "btnConfirmLocation";
            this.btnConfirmLocation.Size = new System.Drawing.Size(150, 40);
            this.btnConfirmLocation.TabIndex = 169;
            this.btnConfirmLocation.Text = "Confirm";
            this.btnConfirmLocation.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnConfirmLocation.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnConfirmLocation.UseVisualStyleBackColor = true;
            this.btnConfirmLocation.Click += new System.EventHandler(this.btnConfirmLocation_Click);
            // 
            // cmbLocation
            // 
            this.cmbLocation.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cmbLocation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbLocation.FormattingEnabled = true;
            this.cmbLocation.Location = new System.Drawing.Point(89, 461);
            this.cmbLocation.Name = "cmbLocation";
            this.cmbLocation.Size = new System.Drawing.Size(150, 28);
            this.cmbLocation.TabIndex = 168;
            // 
            // label34
            // 
            this.label34.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(5, 465);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(82, 20);
            this.label34.TabIndex = 167;
            this.label34.Text = "Location : ";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label33.Location = new System.Drawing.Point(78, 22);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(13, 20);
            this.label33.TabIndex = 166;
            this.label33.Text = ":";
            // 
            // lblQtyHis
            // 
            this.lblQtyHis.AutoSize = true;
            this.lblQtyHis.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.lblQtyHis.Location = new System.Drawing.Point(92, 22);
            this.lblQtyHis.Name = "lblQtyHis";
            this.lblQtyHis.Size = new System.Drawing.Size(14, 20);
            this.lblQtyHis.TabIndex = 11;
            this.lblQtyHis.Text = "-";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(298, 476);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 20);
            this.label2.TabIndex = 10;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label3.Location = new System.Drawing.Point(6, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 20);
            this.label3.TabIndex = 9;
            this.label3.Text = "Total Qty";
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.groupBox1.Location = new System.Drawing.Point(192, 346);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(66, 17);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Product";
            this.groupBox1.Visible = false;
            // 
            // groupBox5
            // 
            this.groupBox5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox5.Controls.Add(this.button4);
            this.groupBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.groupBox5.Location = new System.Drawing.Point(277, 292);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(85, 57);
            this.groupBox5.TabIndex = 10;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Product";
            this.groupBox5.Visible = false;
            // 
            // button4
            // 
            this.button4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.Location = new System.Drawing.Point(15, -82);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(150, 50);
            this.button4.TabIndex = 138;
            this.button4.Text = "Back";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Visible = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // dgvVaccine
            // 
            this.dgvVaccine.AllowUserToAddRows = false;
            this.dgvVaccine.AllowUserToDeleteRows = false;
            this.dgvVaccine.AllowUserToResizeRows = false;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dgvVaccine.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle5;
            this.dgvVaccine.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvVaccine.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvVaccine.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvVaccine.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.itemid,
            this.qty_innerbox,
            this.uom,
            this.qty,
            this.uom2,
            this.model_name,
            this.batchnumber,
            this.isSAS});
            this.dgvVaccine.Location = new System.Drawing.Point(10, 132);
            this.dgvVaccine.MultiSelect = false;
            this.dgvVaccine.Name = "dgvVaccine";
            this.dgvVaccine.ReadOnly = true;
            this.dgvVaccine.RowHeadersVisible = false;
            this.dgvVaccine.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvVaccine.Size = new System.Drawing.Size(448, 308);
            this.dgvVaccine.TabIndex = 9;
            this.dgvVaccine.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvVaccine_CellDoubleClick);
            this.dgvVaccine.DataSourceChanged += new System.EventHandler(this.dgvVaccine_DataSourceChanged);
            this.dgvVaccine.SelectionChanged += new System.EventHandler(this.dgvVaccine_SelectionChanged);
            // 
            // itemid
            // 
            this.itemid.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.itemid.DataPropertyName = "itemid";
            this.itemid.HeaderText = "Basket Id";
            this.itemid.Name = "itemid";
            this.itemid.ReadOnly = true;
            this.itemid.Width = 102;
            // 
            // qty_innerbox
            // 
            this.qty_innerbox.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.qty_innerbox.DataPropertyName = "qty_innerbox";
            dataGridViewCellStyle6.Format = "n0";
            this.qty_innerbox.DefaultCellStyle = dataGridViewCellStyle6;
            this.qty_innerbox.HeaderText = "Qty";
            this.qty_innerbox.Name = "qty_innerbox";
            this.qty_innerbox.ReadOnly = true;
            this.qty_innerbox.Width = 58;
            // 
            // uom
            // 
            this.uom.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.uom.DataPropertyName = "uom";
            this.uom.HeaderText = "Uom";
            this.uom.Name = "uom";
            this.uom.ReadOnly = true;
            this.uom.Width = 68;
            // 
            // qty
            // 
            this.qty.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.qty.DataPropertyName = "qty";
            dataGridViewCellStyle7.Format = "n0";
            this.qty.DefaultCellStyle = dataGridViewCellStyle7;
            this.qty.HeaderText = "Qty";
            this.qty.Name = "qty";
            this.qty.ReadOnly = true;
            this.qty.Width = 58;
            // 
            // uom2
            // 
            this.uom2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.uom2.DataPropertyName = "uom2";
            this.uom2.HeaderText = "Uom";
            this.uom2.Name = "uom2";
            this.uom2.ReadOnly = true;
            this.uom2.Width = 68;
            // 
            // model_name
            // 
            this.model_name.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.model_name.DataPropertyName = "model_name";
            this.model_name.HeaderText = "Product Name";
            this.model_name.Name = "model_name";
            this.model_name.ReadOnly = true;
            this.model_name.Visible = false;
            // 
            // batchnumber
            // 
            this.batchnumber.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.batchnumber.DataPropertyName = "batchnumber";
            this.batchnumber.HeaderText = "batchnumber";
            this.batchnumber.Name = "batchnumber";
            this.batchnumber.ReadOnly = true;
            this.batchnumber.Visible = false;
            // 
            // isSAS
            // 
            this.isSAS.DataPropertyName = "isSAS";
            this.isSAS.HeaderText = "isSAS";
            this.isSAS.Name = "isSAS";
            this.isSAS.ReadOnly = true;
            this.isSAS.Visible = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label7.Location = new System.Drawing.Point(9, 103);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(72, 20);
            this.label7.TabIndex = 154;
            this.label7.Text = "Total Qty";
            // 
            // lblQty
            // 
            this.lblQty.AutoSize = true;
            this.lblQty.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.lblQty.Location = new System.Drawing.Point(132, 103);
            this.lblQty.Name = "lblQty";
            this.lblQty.Size = new System.Drawing.Size(14, 20);
            this.lblQty.TabIndex = 155;
            this.lblQty.Text = "-";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label1.Location = new System.Drawing.Point(9, 77);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(111, 20);
            this.label1.TabIndex = 141;
            this.label1.Text = "Batch Number";
            // 
            // lblBatch
            // 
            this.lblBatch.AutoSize = true;
            this.lblBatch.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.lblBatch.Location = new System.Drawing.Point(132, 77);
            this.lblBatch.Name = "lblBatch";
            this.lblBatch.Size = new System.Drawing.Size(14, 20);
            this.lblBatch.TabIndex = 142;
            this.lblBatch.Text = "-";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(588, 59);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(78, 19);
            this.label10.TabIndex = 143;
            this.label10.Text = "Expiry Date";
            this.label10.Visible = false;
            // 
            // lblExp
            // 
            this.lblExp.AutoSize = true;
            this.lblExp.Location = new System.Drawing.Point(709, 59);
            this.lblExp.Name = "lblExp";
            this.lblExp.Size = new System.Drawing.Size(14, 20);
            this.lblExp.TabIndex = 144;
            this.lblExp.Text = "-";
            this.lblExp.Visible = false;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label12.Location = new System.Drawing.Point(9, 48);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(110, 20);
            this.label12.TabIndex = 145;
            this.label12.Text = "Product Name";
            // 
            // lblProduct
            // 
            this.lblProduct.AutoSize = true;
            this.lblProduct.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.lblProduct.Location = new System.Drawing.Point(132, 48);
            this.lblProduct.Name = "lblProduct";
            this.lblProduct.Size = new System.Drawing.Size(14, 20);
            this.lblProduct.TabIndex = 146;
            this.lblProduct.Text = "-";
            // 
            // grpBoxDetail
            // 
            this.grpBoxDetail.Controls.Add(this.dgvVaccine);
            this.grpBoxDetail.Controls.Add(this.label29);
            this.grpBoxDetail.Controls.Add(this.label30);
            this.grpBoxDetail.Controls.Add(this.label31);
            this.grpBoxDetail.Controls.Add(this.label7);
            this.grpBoxDetail.Controls.Add(this.label24);
            this.grpBoxDetail.Controls.Add(this.lblUom);
            this.grpBoxDetail.Controls.Add(this.lblQty);
            this.grpBoxDetail.Controls.Add(this.label32);
            this.grpBoxDetail.Controls.Add(this.txtBarcode);
            this.grpBoxDetail.Controls.Add(this.label27);
            this.grpBoxDetail.Controls.Add(this.lblScan);
            this.grpBoxDetail.Controls.Add(this.label28);
            this.grpBoxDetail.Controls.Add(this.label26);
            this.grpBoxDetail.Controls.Add(this.label25);
            this.grpBoxDetail.Controls.Add(this.btnYes);
            this.grpBoxDetail.Controls.Add(this.lblParentUom);
            this.grpBoxDetail.Controls.Add(this.lblParUom);
            this.grpBoxDetail.Controls.Add(this.lblParent);
            this.grpBoxDetail.Controls.Add(this.lblPar);
            this.grpBoxDetail.Controls.Add(this.lblOrder);
            this.grpBoxDetail.Controls.Add(this.label9);
            this.grpBoxDetail.Controls.Add(this.lblProduct);
            this.grpBoxDetail.Controls.Add(this.label12);
            this.grpBoxDetail.Controls.Add(this.lblExp);
            this.grpBoxDetail.Controls.Add(this.label10);
            this.grpBoxDetail.Controls.Add(this.lblBatch);
            this.grpBoxDetail.Controls.Add(this.label1);
            this.grpBoxDetail.Controls.Add(this.groupBox5);
            this.grpBoxDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpBoxDetail.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.grpBoxDetail.Location = new System.Drawing.Point(0, 0);
            this.grpBoxDetail.Name = "grpBoxDetail";
            this.grpBoxDetail.Size = new System.Drawing.Size(469, 500);
            this.grpBoxDetail.TabIndex = 137;
            this.grpBoxDetail.TabStop = false;
            this.grpBoxDetail.Text = "Detail ";
            this.grpBoxDetail.Enter += new System.EventHandler(this.grpBoxDetail_Enter);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label29.Location = new System.Drawing.Point(119, 103);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(13, 20);
            this.label29.TabIndex = 165;
            this.label29.Text = ":";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(691, 78);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(12, 19);
            this.label30.TabIndex = 164;
            this.label30.Text = ":";
            this.label30.Visible = false;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(691, 40);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(12, 19);
            this.label31.TabIndex = 163;
            this.label31.Text = ":";
            this.label31.Visible = false;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(588, 78);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(39, 19);
            this.label24.TabIndex = 156;
            this.label24.Text = "Uom";
            this.label24.Visible = false;
            // 
            // lblUom
            // 
            this.lblUom.AutoSize = true;
            this.lblUom.Location = new System.Drawing.Point(710, 78);
            this.lblUom.Name = "lblUom";
            this.lblUom.Size = new System.Drawing.Size(14, 20);
            this.lblUom.TabIndex = 157;
            this.lblUom.Text = "-";
            this.lblUom.Visible = false;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(691, 21);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(12, 19);
            this.label32.TabIndex = 162;
            this.label32.Text = ":";
            this.label32.Visible = false;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label27.Location = new System.Drawing.Point(119, 48);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(13, 20);
            this.label27.TabIndex = 161;
            this.label27.Text = ":";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(691, 59);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(12, 19);
            this.label28.TabIndex = 160;
            this.label28.Text = ":";
            this.label28.Visible = false;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label26.Location = new System.Drawing.Point(119, 77);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(13, 20);
            this.label26.TabIndex = 159;
            this.label26.Text = ":";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label25.Location = new System.Drawing.Point(119, 22);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(13, 20);
            this.label25.TabIndex = 158;
            this.label25.Text = ":";
            // 
            // btnYes
            // 
            this.btnYes.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnYes.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnYes.Image = global::Receiving_Station.Properties.Resources.confirm;
            this.btnYes.Location = new System.Drawing.Point(306, 449);
            this.btnYes.Name = "btnYes";
            this.btnYes.Size = new System.Drawing.Size(150, 40);
            this.btnYes.TabIndex = 153;
            this.btnYes.Text = "Confirm";
            this.btnYes.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnYes.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnYes.UseVisualStyleBackColor = true;
            this.btnYes.Visible = false;
            this.btnYes.Click += new System.EventHandler(this.btnYes_Click_1);
            // 
            // lblParentUom
            // 
            this.lblParentUom.AutoSize = true;
            this.lblParentUom.Location = new System.Drawing.Point(709, 40);
            this.lblParentUom.Name = "lblParentUom";
            this.lblParentUom.Size = new System.Drawing.Size(14, 20);
            this.lblParentUom.TabIndex = 152;
            this.lblParentUom.Text = "-";
            this.lblParentUom.Visible = false;
            // 
            // lblParUom
            // 
            this.lblParUom.AutoSize = true;
            this.lblParUom.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblParUom.Location = new System.Drawing.Point(588, 40);
            this.lblParUom.Name = "lblParUom";
            this.lblParUom.Size = new System.Drawing.Size(83, 19);
            this.lblParUom.TabIndex = 151;
            this.lblParUom.Text = "Parent Uom";
            this.lblParUom.Visible = false;
            // 
            // lblParent
            // 
            this.lblParent.AutoSize = true;
            this.lblParent.Location = new System.Drawing.Point(709, 19);
            this.lblParent.Name = "lblParent";
            this.lblParent.Size = new System.Drawing.Size(14, 20);
            this.lblParent.TabIndex = 150;
            this.lblParent.Text = "-";
            this.lblParent.Visible = false;
            // 
            // lblPar
            // 
            this.lblPar.AutoSize = true;
            this.lblPar.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPar.Location = new System.Drawing.Point(588, 21);
            this.lblPar.Name = "lblPar";
            this.lblPar.Size = new System.Drawing.Size(67, 19);
            this.lblPar.TabIndex = 149;
            this.lblPar.Text = "Parent ID";
            this.lblPar.Visible = false;
            // 
            // lblOrder
            // 
            this.lblOrder.AutoSize = true;
            this.lblOrder.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.lblOrder.Location = new System.Drawing.Point(132, 22);
            this.lblOrder.Name = "lblOrder";
            this.lblOrder.Size = new System.Drawing.Size(14, 20);
            this.lblOrder.TabIndex = 148;
            this.lblOrder.Text = "-";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label9.Location = new System.Drawing.Point(9, 22);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(105, 20);
            this.label9.TabIndex = 147;
            this.label9.Text = "WTO Number";
            // 
            // grpDetail
            // 
            this.grpDetail.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.grpDetail.Controls.Add(this.btnCancel);
            this.grpDetail.Controls.Add(this.label5);
            this.grpDetail.Controls.Add(this.label6);
            this.grpDetail.Controls.Add(this.label11);
            this.grpDetail.Controls.Add(this.label13);
            this.grpDetail.Controls.Add(this.label15);
            this.grpDetail.Controls.Add(this.label16);
            this.grpDetail.Controls.Add(this.label17);
            this.grpDetail.Controls.Add(this.label18);
            this.grpDetail.Controls.Add(this.label19);
            this.grpDetail.Controls.Add(this.label20);
            this.grpDetail.Controls.Add(this.label22);
            this.grpDetail.Controls.Add(this.label23);
            this.grpDetail.Controls.Add(this.groupBox2);
            this.grpDetail.Controls.Add(this.groupBox6);
            this.grpDetail.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpDetail.Location = new System.Drawing.Point(21, 282);
            this.grpDetail.Name = "grpDetail";
            this.grpDetail.Size = new System.Drawing.Size(518, 332);
            this.grpDetail.TabIndex = 147;
            this.grpDetail.TabStop = false;
            this.grpDetail.Text = "Detail ";
            this.grpDetail.Visible = false;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.Location = new System.Drawing.Point(0, 275);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(150, 50);
            this.btnCancel.TabIndex = 149;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(127, 85);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(63, 20);
            this.label5.TabIndex = 146;
            this.label5.Text = "product";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(14, 85);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(118, 20);
            this.label6.TabIndex = 145;
            this.label6.Text = "Product Name :";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(127, 65);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(49, 20);
            this.label11.TabIndex = 144;
            this.label11.Text = "expiry";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(14, 65);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(95, 20);
            this.label13.TabIndex = 143;
            this.label13.Text = "Expiry date :";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(127, 45);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(49, 20);
            this.label15.TabIndex = 142;
            this.label15.Text = "batch";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(14, 45);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(115, 20);
            this.label16.TabIndex = 141;
            this.label16.Text = "BatchNumber :";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(127, 25);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(57, 20);
            this.label17.TabIndex = 140;
            this.label17.Text = "basket";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(14, 25);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(88, 20);
            this.label18.TabIndex = 139;
            this.label18.Text = "Basket ID :";
            // 
            // label19
            // 
            this.label19.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(485, 65);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(0, 20);
            this.label19.TabIndex = 14;
            // 
            // label20
            // 
            this.label20.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(447, 45);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(0, 20);
            this.label20.TabIndex = 12;
            // 
            // label22
            // 
            this.label22.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(370, 65);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(109, 20);
            this.label22.TabIndex = 13;
            this.label22.Text = "Qty InnerBox :";
            // 
            // label23
            // 
            this.label23.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(370, 45);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(71, 20);
            this.label23.TabIndex = 11;
            this.label23.Text = "Qty Vial :";
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.dataGridView1);
            this.groupBox2.Location = new System.Drawing.Point(18, 113);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(481, 22);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Vial ";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dataGridView1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle8;
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(6, 25);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(469, 0);
            this.dataGridView1.TabIndex = 9;
            // 
            // groupBox6
            // 
            this.groupBox6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox6.Controls.Add(this.dataGridView2);
            this.groupBox6.Location = new System.Drawing.Point(18, 116);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(481, 150);
            this.groupBox6.TabIndex = 0;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Inner Box";
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.AllowUserToDeleteRows = false;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dataGridView2.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle9;
            this.dataGridView2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Location = new System.Drawing.Point(6, 25);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.ReadOnly = true;
            this.dataGridView2.RowHeadersVisible = false;
            this.dataGridView2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView2.Size = new System.Drawing.Size(469, 119);
            this.dataGridView2.TabIndex = 9;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(148)))), ((int)(((byte)(172)))));
            this.panel1.Controls.Add(this.lblDate);
            this.panel1.Controls.Add(this.lblTime);
            this.panel1.Controls.Add(this.lblRole);
            this.panel1.Controls.Add(this.lblUserId);
            this.panel1.Controls.Add(this.pictureBox3);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.label21);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1101, 86);
            this.panel1.TabIndex = 148;
            // 
            // lblDate
            // 
            this.lblDate.AutoSize = true;
            this.lblDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lblDate.ForeColor = System.Drawing.Color.White;
            this.lblDate.Location = new System.Drawing.Point(216, 35);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(33, 15);
            this.lblDate.TabIndex = 152;
            this.lblDate.Text = "Date";
            // 
            // lblTime
            // 
            this.lblTime.AutoSize = true;
            this.lblTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            this.lblTime.ForeColor = System.Drawing.Color.White;
            this.lblTime.Location = new System.Drawing.Point(216, 17);
            this.lblTime.Name = "lblTime";
            this.lblTime.Size = new System.Drawing.Size(39, 15);
            this.lblTime.TabIndex = 151;
            this.lblTime.Text = "Time";
            // 
            // lblRole
            // 
            this.lblRole.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblRole.AutoSize = true;
            this.lblRole.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.lblRole.ForeColor = System.Drawing.Color.White;
            this.lblRole.Location = new System.Drawing.Point(1001, 41);
            this.lblRole.Name = "lblRole";
            this.lblRole.Size = new System.Drawing.Size(52, 20);
            this.lblRole.TabIndex = 150;
            this.lblRole.Text = "admin";
            // 
            // lblUserId
            // 
            this.lblUserId.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblUserId.AutoSize = true;
            this.lblUserId.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.lblUserId.ForeColor = System.Drawing.Color.White;
            this.lblUserId.Location = new System.Drawing.Point(1000, 21);
            this.lblUserId.Name = "lblUserId";
            this.lblUserId.Size = new System.Drawing.Size(57, 20);
            this.lblUserId.TabIndex = 149;
            this.lblUserId.Text = "admin";
            // 
            // pictureBox3
            // 
            this.pictureBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox3.Image = global::Receiving_Station.Properties.Resources.user;
            this.pictureBox3.Location = new System.Drawing.Point(949, 17);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(45, 45);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox3.TabIndex = 148;
            this.pictureBox3.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(47)))), ((int)(((byte)(54)))));
            this.panel2.Controls.Add(this.pictureBox1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(210, 86);
            this.panel2.TabIndex = 38;
            // 
            // pictureBox1
            // 
            this.pictureBox1.ImageLocation = "logo_biofarma.png";
            this.pictureBox1.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.InitialImage")));
            this.pictureBox1.Location = new System.Drawing.Point(30, 5);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(150, 75);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 129;
            this.pictureBox1.TabStop = false;
            // 
            // label14
            // 
            this.label14.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold);
            this.label14.ForeColor = System.Drawing.Color.White;
            this.label14.Location = new System.Drawing.Point(284, 41);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(649, 29);
            this.label14.TabIndex = 35;
            this.label14.Text = "DISTRIBUTION STATION";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label21
            // 
            this.label21.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold);
            this.label21.ForeColor = System.Drawing.Color.White;
            this.label21.Location = new System.Drawing.Point(279, 12);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(654, 29);
            this.label21.TabIndex = 36;
            this.label21.Text = "RECEIVING AND WAREHOUSING CONTROL";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // txtOrderNumber
            // 
            this.txtOrderNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtOrderNumber.Location = new System.Drawing.Point(63, 10);
            this.txtOrderNumber.Name = "txtOrderNumber";
            this.txtOrderNumber.ReadOnly = true;
            this.txtOrderNumber.Size = new System.Drawing.Size(290, 26);
            this.txtOrderNumber.TabIndex = 151;
            this.txtOrderNumber.Click += new System.EventHandler(this.txtBatchNumber_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label4.Location = new System.Drawing.Point(6, 13);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 20);
            this.label4.TabIndex = 149;
            this.label4.Text = "WTO :";
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl1.Location = new System.Drawing.Point(0, 91);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(933, 588);
            this.tabControl1.TabIndex = 153;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.progressBar1);
            this.tabPage3.Controls.Add(this.splitContainer1);
            this.tabPage3.Controls.Add(this.label4);
            this.tabPage3.Controls.Add(this.txtOrderNumber);
            this.tabPage3.Location = new System.Drawing.Point(4, 29);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(925, 555);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Receive & Inbound";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.Location = new System.Drawing.Point(9, 46);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.grpBoxDetail);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.grpHistory);
            this.splitContainer1.Size = new System.Drawing.Size(913, 500);
            this.splitContainer1.SplitterDistance = 469;
            this.splitContainer1.SplitterWidth = 8;
            this.splitContainer1.TabIndex = 152;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.button10);
            this.tabPage1.Controls.Add(this.button11);
            this.tabPage1.Controls.Add(this.groupBox4);
            this.tabPage1.Controls.Add(this.cmbMoveNewLocation);
            this.tabPage1.Controls.Add(this.label40);
            this.tabPage1.Controls.Add(this.txtBasketMove);
            this.tabPage1.Controls.Add(this.label41);
            this.tabPage1.Controls.Add(this.button12);
            this.tabPage1.Location = new System.Drawing.Point(4, 29);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(925, 555);
            this.tabPage1.TabIndex = 3;
            this.tabPage1.Text = "Movement";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // button10
            // 
            this.button10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button10.Image = global::Receiving_Station.Properties.Resources.clear;
            this.button10.Location = new System.Drawing.Point(763, 121);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(150, 40);
            this.button10.TabIndex = 27;
            this.button10.Text = "Clear All";
            this.button10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button10.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // button11
            // 
            this.button11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button11.Image = global::Receiving_Station.Properties.Resources.clear_selected;
            this.button11.Location = new System.Drawing.Point(763, 75);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(150, 40);
            this.button11.TabIndex = 26;
            this.button11.Text = "Clear Selected";
            this.button11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button11.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox4.Controls.Add(this.lblQtyMovement);
            this.groupBox4.Controls.Add(this.label38);
            this.groupBox4.Controls.Add(this.dgvMovement);
            this.groupBox4.Controls.Add(this.cmbMoveLocation);
            this.groupBox4.Controls.Add(this.label39);
            this.groupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.groupBox4.Location = new System.Drawing.Point(6, 3);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(747, 509);
            this.groupBox4.TabIndex = 25;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Scanned";
            // 
            // lblQtyMovement
            // 
            this.lblQtyMovement.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblQtyMovement.AutoSize = true;
            this.lblQtyMovement.Location = new System.Drawing.Point(114, 481);
            this.lblQtyMovement.Name = "lblQtyMovement";
            this.lblQtyMovement.Size = new System.Drawing.Size(18, 20);
            this.lblQtyMovement.TabIndex = 10;
            this.lblQtyMovement.Text = "0";
            // 
            // label38
            // 
            this.label38.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(6, 481);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(109, 20);
            this.label38.TabIndex = 9;
            this.label38.Text = "Qty Scanned :";
            // 
            // dgvMovement
            // 
            this.dgvMovement.AllowUserToAddRows = false;
            this.dgvMovement.AllowUserToDeleteRows = false;
            this.dgvMovement.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvMovement.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvMovement.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMovement.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.itemid3,
            this.model_name3,
            this.expiry_date,
            this.batch_number,
            this.qty3,
            this.uom3,
            this.last_location});
            this.dgvMovement.Location = new System.Drawing.Point(11, 28);
            this.dgvMovement.Name = "dgvMovement";
            this.dgvMovement.ReadOnly = true;
            this.dgvMovement.RowHeadersVisible = false;
            this.dgvMovement.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.dgvMovement.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvMovement.Size = new System.Drawing.Size(725, 444);
            this.dgvMovement.TabIndex = 7;
            // 
            // itemid3
            // 
            this.itemid3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.itemid3.DataPropertyName = "itemid";
            this.itemid3.HeaderText = "Basket Id";
            this.itemid3.Name = "itemid3";
            this.itemid3.ReadOnly = true;
            this.itemid3.Width = 102;
            // 
            // model_name3
            // 
            this.model_name3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.model_name3.DataPropertyName = "model_name";
            this.model_name3.HeaderText = "Product Name";
            this.model_name3.Name = "model_name3";
            this.model_name3.ReadOnly = true;
            this.model_name3.Width = 135;
            // 
            // expiry_date
            // 
            this.expiry_date.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.expiry_date.DataPropertyName = "expiry_date";
            this.expiry_date.HeaderText = "Expiry Date";
            this.expiry_date.Name = "expiry_date";
            this.expiry_date.ReadOnly = true;
            this.expiry_date.Width = 115;
            // 
            // batch_number
            // 
            this.batch_number.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.batch_number.DataPropertyName = "batch_number";
            this.batch_number.HeaderText = "Batch Number";
            this.batch_number.Name = "batch_number";
            this.batch_number.ReadOnly = true;
            this.batch_number.Width = 136;
            // 
            // qty3
            // 
            this.qty3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.qty3.DataPropertyName = "qty";
            this.qty3.HeaderText = "Qty";
            this.qty3.Name = "qty3";
            this.qty3.ReadOnly = true;
            this.qty3.Width = 58;
            // 
            // uom3
            // 
            this.uom3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.uom3.DataPropertyName = "uom";
            this.uom3.HeaderText = "Uom";
            this.uom3.Name = "uom3";
            this.uom3.ReadOnly = true;
            this.uom3.Width = 68;
            // 
            // last_location
            // 
            this.last_location.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.last_location.DataPropertyName = "last_location";
            this.last_location.HeaderText = "Last Location";
            this.last_location.Name = "last_location";
            this.last_location.ReadOnly = true;
            this.last_location.Width = 130;
            // 
            // cmbMoveLocation
            // 
            this.cmbMoveLocation.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbMoveLocation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbMoveLocation.FormattingEnabled = true;
            this.cmbMoveLocation.Location = new System.Drawing.Point(586, 377);
            this.cmbMoveLocation.Name = "cmbMoveLocation";
            this.cmbMoveLocation.Size = new System.Drawing.Size(150, 28);
            this.cmbMoveLocation.TabIndex = 21;
            this.cmbMoveLocation.Visible = false;
            // 
            // label39
            // 
            this.label39.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(582, 354);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(78, 20);
            this.label39.TabIndex = 22;
            this.label39.Text = "Location :";
            this.label39.Visible = false;
            // 
            // cmbMoveNewLocation
            // 
            this.cmbMoveNewLocation.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbMoveNewLocation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbMoveNewLocation.FormattingEnabled = true;
            this.cmbMoveNewLocation.Location = new System.Drawing.Point(763, 38);
            this.cmbMoveNewLocation.Name = "cmbMoveNewLocation";
            this.cmbMoveNewLocation.Size = new System.Drawing.Size(150, 28);
            this.cmbMoveNewLocation.TabIndex = 24;
            // 
            // label40
            // 
            this.label40.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(759, 13);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(113, 20);
            this.label40.TabIndex = 20;
            this.label40.Text = "New Location :";
            // 
            // txtBasketMove
            // 
            this.txtBasketMove.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBasketMove.Location = new System.Drawing.Point(127, 521);
            this.txtBasketMove.Name = "txtBasketMove";
            this.txtBasketMove.Size = new System.Drawing.Size(626, 26);
            this.txtBasketMove.TabIndex = 19;
            this.txtBasketMove.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtBasketMove_KeyDown);
            this.txtBasketMove.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBasketMove_KeyPress);
            // 
            // label41
            // 
            this.label41.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.label41.Location = new System.Drawing.Point(11, 522);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(110, 21);
            this.label41.TabIndex = 18;
            this.label41.Text = "Scan Barcode :";
            // 
            // button12
            // 
            this.button12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button12.Image = global::Receiving_Station.Properties.Resources.confirm;
            this.button12.Location = new System.Drawing.Point(763, 167);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(150, 40);
            this.button12.TabIndex = 23;
            this.button12.Text = "Confirm";
            this.button12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button12.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.button12_Click);
            // 
            // btnOpenLog
            // 
            this.btnOpenLog.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOpenLog.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.btnOpenLog.Image = global::Receiving_Station.Properties.Resources.log1;
            this.btnOpenLog.Location = new System.Drawing.Point(941, 622);
            this.btnOpenLog.Name = "btnOpenLog";
            this.btnOpenLog.Size = new System.Drawing.Size(150, 40);
            this.btnOpenLog.TabIndex = 152;
            this.btnOpenLog.Text = "Log File";
            this.btnOpenLog.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnOpenLog.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnOpenLog.UseVisualStyleBackColor = true;
            this.btnOpenLog.Click += new System.EventHandler(this.btnOpenLog_Click);
            // 
            // button3
            // 
            this.button3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.button3.Image = global::Receiving_Station.Properties.Resources.clear;
            this.button3.Location = new System.Drawing.Point(941, 575);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(150, 40);
            this.button3.TabIndex = 13;
            this.button3.Text = "Clear All";
            this.button3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Visible = false;
            this.button3.Click += new System.EventHandler(this.button3_Click_1);
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.button2.Image = global::Receiving_Station.Properties.Resources.decommission;
            this.button2.Location = new System.Drawing.Point(941, 483);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(150, 40);
            this.button2.TabIndex = 12;
            this.button2.Text = "Refresh";
            this.button2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Visible = false;
            this.button2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // button8
            // 
            this.button8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.button8.Image = global::Receiving_Station.Properties.Resources.logout;
            this.button8.Location = new System.Drawing.Point(941, 121);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(150, 40);
            this.button8.TabIndex = 125;
            this.button8.Text = "Logout";
            this.button8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button8.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // btnConfig
            // 
            this.btnConfig.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnConfig.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.btnConfig.Image = global::Receiving_Station.Properties.Resources.config;
            this.btnConfig.Location = new System.Drawing.Point(941, 167);
            this.btnConfig.Name = "btnConfig";
            this.btnConfig.Size = new System.Drawing.Size(150, 40);
            this.btnConfig.TabIndex = 123;
            this.btnConfig.Text = "Config";
            this.btnConfig.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnConfig.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnConfig.UseVisualStyleBackColor = true;
            this.btnConfig.Click += new System.EventHandler(this.btnConfig_Click);
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(380, 12);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(536, 23);
            this.progressBar1.TabIndex = 159;
            // 
            // Receiving
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1101, 677);
            this.Controls.Add(this.btnOpenLog);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.btnConfig);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.grpDetail);
            this.Controls.Add(this.grpPending);
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Receiving";
            this.Text = "Receiving and Warehousing Control";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Receiving_Load);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Receiving_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.dgvReceiving)).EndInit();
            this.grpPending.ResumeLayout(false);
            this.grpPending.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPending)).EndInit();
            this.grpHistory.ResumeLayout(false);
            this.grpHistory.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvVaccine)).EndInit();
            this.grpBoxDetail.ResumeLayout(false);
            this.grpBoxDetail.PerformLayout();
            this.grpDetail.ResumeLayout(false);
            this.grpDetail.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.groupBox6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMovement)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button btnConfig;
        private System.Windows.Forms.PictureBox pictureBox1;
        public System.Windows.Forms.DataGridView dgvReceiving;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label8;
        public System.Windows.Forms.Label lblQtyReceiving;
        private System.Windows.Forms.Button button1;
        public System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        public System.Windows.Forms.DataGridView dgvPending;
        public System.Windows.Forms.GroupBox grpPending;
        public System.Windows.Forms.GroupBox grpHistory;
        public System.Windows.Forms.TextBox txtBarcode;
        public System.Windows.Forms.Label lblScan;
        private System.Windows.Forms.GroupBox groupBox5;
        public System.Windows.Forms.DataGridView dgvVaccine;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.Label lblBatch;
        private System.Windows.Forms.Label label10;
        public System.Windows.Forms.Label lblExp;
        private System.Windows.Forms.Label label12;
        public System.Windows.Forms.Label lblProduct;
        public System.Windows.Forms.GroupBox grpBoxDetail;
        public System.Windows.Forms.GroupBox grpDetail;
        public System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        public System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label13;
        public System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        public System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        public System.Windows.Forms.Label label19;
        public System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.GroupBox groupBox2;
        public System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.GroupBox groupBox6;
        public System.Windows.Forms.DataGridView dataGridView2;
        public System.Windows.Forms.Label lblOrder;
        private System.Windows.Forms.Label label9;
        public System.Windows.Forms.Label lblParent;
        public System.Windows.Forms.Label lblPar;
        public System.Windows.Forms.Label lblParentUom;
        public System.Windows.Forms.Label lblParUom;
        public System.Windows.Forms.Button btnCancel;
        public System.Windows.Forms.Button btnYes;
        public System.Windows.Forms.Label lblQty;
        public System.Windows.Forms.Label label7;
        public System.Windows.Forms.Label lblUom;
        public System.Windows.Forms.Label label24;
        private System.Windows.Forms.Panel panel1;
        public System.Windows.Forms.Label lblRole;
        public System.Windows.Forms.Label lblUserId;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label lblDate;
        private System.Windows.Forms.Label lblTime;
        private System.Windows.Forms.Timer timer1;
        public System.Windows.Forms.TextBox txtOrderNumber;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Button btnOpenLog;
        public System.Windows.Forms.Label lblQtyHis;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label33;
        public System.Windows.Forms.ComboBox cmbLocation;
        private System.Windows.Forms.Label label34;
        public System.Windows.Forms.Button btnConfirmLocation;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button11;
        public System.Windows.Forms.GroupBox groupBox4;
        public System.Windows.Forms.Label lblQtyMovement;
        private System.Windows.Forms.Label label38;
        public System.Windows.Forms.DataGridView dgvMovement;
        public System.Windows.Forms.ComboBox cmbMoveNewLocation;
        private System.Windows.Forms.Label label39;
        public System.Windows.Forms.ComboBox cmbMoveLocation;
        private System.Windows.Forms.Label label40;
        public System.Windows.Forms.TextBox txtBasketMove;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.DataGridViewTextBoxColumn itemid2;
        private System.Windows.Forms.DataGridViewTextBoxColumn qty_innerbox2;
        private System.Windows.Forms.DataGridViewTextBoxColumn uom22;
        private System.Windows.Forms.DataGridViewTextBoxColumn qty2;
        private System.Windows.Forms.DataGridViewTextBoxColumn uom33;
        private System.Windows.Forms.DataGridViewTextBoxColumn itemid3;
        private System.Windows.Forms.DataGridViewTextBoxColumn model_name3;
        private System.Windows.Forms.DataGridViewTextBoxColumn expiry_date;
        private System.Windows.Forms.DataGridViewTextBoxColumn batch_number;
        private System.Windows.Forms.DataGridViewTextBoxColumn qty3;
        private System.Windows.Forms.DataGridViewTextBoxColumn uom3;
        private System.Windows.Forms.DataGridViewTextBoxColumn last_location;
        private System.Windows.Forms.DataGridViewTextBoxColumn itemid;
        private System.Windows.Forms.DataGridViewTextBoxColumn qty_innerbox;
        private System.Windows.Forms.DataGridViewTextBoxColumn uom;
        private System.Windows.Forms.DataGridViewTextBoxColumn qty;
        private System.Windows.Forms.DataGridViewTextBoxColumn uom2;
        private System.Windows.Forms.DataGridViewTextBoxColumn model_name;
        private System.Windows.Forms.DataGridViewTextBoxColumn batchnumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn isSAS;
        public System.Windows.Forms.ProgressBar progressBar1;
    }
}

