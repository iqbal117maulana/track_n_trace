﻿namespace Receiving_Station
{
    partial class DetailVaccine
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DetailVaccine));
            this.panel2 = new System.Windows.Forms.Panel();
            this.lblTotalDetail = new System.Windows.Forms.Label();
            this.dgvVaccine = new System.Windows.Forms.DataGridView();
            this.itemid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.qty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.uom2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.lblUom = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.lblQty = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lblParentUom = new System.Windows.Forms.Label();
            this.lblParUom = new System.Windows.Forms.Label();
            this.lblParent = new System.Windows.Forms.Label();
            this.lblPar = new System.Windows.Forms.Label();
            this.lblOrder = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.lblProduct = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.lblExp = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.lblBatch = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVaccine)).BeginInit();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.lblTotalDetail);
            this.panel2.Controls.Add(this.dgvVaccine);
            this.panel2.Controls.Add(this.label29);
            this.panel2.Controls.Add(this.label30);
            this.panel2.Controls.Add(this.label31);
            this.panel2.Controls.Add(this.label32);
            this.panel2.Controls.Add(this.label27);
            this.panel2.Controls.Add(this.label28);
            this.panel2.Controls.Add(this.label26);
            this.panel2.Controls.Add(this.label25);
            this.panel2.Controls.Add(this.lblUom);
            this.panel2.Controls.Add(this.label24);
            this.panel2.Controls.Add(this.lblQty);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.lblParentUom);
            this.panel2.Controls.Add(this.lblParUom);
            this.panel2.Controls.Add(this.lblParent);
            this.panel2.Controls.Add(this.lblPar);
            this.panel2.Controls.Add(this.lblOrder);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.lblProduct);
            this.panel2.Controls.Add(this.label12);
            this.panel2.Controls.Add(this.lblExp);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.lblBatch);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(979, 634);
            this.panel2.TabIndex = 1;
            // 
            // lblTotalDetail
            // 
            this.lblTotalDetail.AutoSize = true;
            this.lblTotalDetail.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.lblTotalDetail.Location = new System.Drawing.Point(9, 606);
            this.lblTotalDetail.Name = "lblTotalDetail";
            this.lblTotalDetail.Size = new System.Drawing.Size(14, 20);
            this.lblTotalDetail.TabIndex = 191;
            this.lblTotalDetail.Text = "-";
            // 
            // dgvVaccine
            // 
            this.dgvVaccine.AllowUserToAddRows = false;
            this.dgvVaccine.AllowUserToDeleteRows = false;
            this.dgvVaccine.AllowUserToResizeColumns = false;
            this.dgvVaccine.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dgvVaccine.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvVaccine.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvVaccine.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvVaccine.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvVaccine.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvVaccine.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.itemid,
            this.qty,
            this.uom2});
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvVaccine.DefaultCellStyle = dataGridViewCellStyle4;
            this.dgvVaccine.Location = new System.Drawing.Point(11, 158);
            this.dgvVaccine.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dgvVaccine.MultiSelect = false;
            this.dgvVaccine.Name = "dgvVaccine";
            this.dgvVaccine.ReadOnly = true;
            this.dgvVaccine.RowHeadersVisible = false;
            this.dgvVaccine.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvVaccine.Size = new System.Drawing.Size(958, 442);
            this.dgvVaccine.TabIndex = 190;
            // 
            // itemid
            // 
            this.itemid.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.itemid.DataPropertyName = "itemid";
            this.itemid.HeaderText = "Innerbox Id";
            this.itemid.Name = "itemid";
            this.itemid.ReadOnly = true;
            this.itemid.Width = 114;
            // 
            // qty
            // 
            this.qty.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.qty.DataPropertyName = "qty";
            dataGridViewCellStyle3.Format = "n0";
            this.qty.DefaultCellStyle = dataGridViewCellStyle3;
            this.qty.HeaderText = "Qty";
            this.qty.Name = "qty";
            this.qty.ReadOnly = true;
            this.qty.Width = 58;
            // 
            // uom2
            // 
            this.uom2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.uom2.DataPropertyName = "uom";
            this.uom2.HeaderText = "Uom";
            this.uom2.Name = "uom2";
            this.uom2.ReadOnly = true;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label29.Location = new System.Drawing.Point(373, 80);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(13, 20);
            this.label29.TabIndex = 189;
            this.label29.Text = ":";
            this.label29.Visible = false;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label30.Location = new System.Drawing.Point(373, 55);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(13, 20);
            this.label30.TabIndex = 188;
            this.label30.Text = ":";
            this.label30.Visible = false;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label31.Location = new System.Drawing.Point(373, 15);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(13, 20);
            this.label31.TabIndex = 187;
            this.label31.Text = ":";
            this.label31.Visible = false;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label32.Location = new System.Drawing.Point(123, 38);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(13, 20);
            this.label32.TabIndex = 186;
            this.label32.Text = ":";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label27.Location = new System.Drawing.Point(123, 123);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(13, 20);
            this.label27.TabIndex = 185;
            this.label27.Text = ":";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label28.Location = new System.Drawing.Point(123, 93);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(13, 20);
            this.label28.TabIndex = 184;
            this.label28.Text = ":";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label26.Location = new System.Drawing.Point(123, 65);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(13, 20);
            this.label26.TabIndex = 183;
            this.label26.Text = ":";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label25.Location = new System.Drawing.Point(123, 12);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(13, 20);
            this.label25.TabIndex = 182;
            this.label25.Text = ":";
            // 
            // lblUom
            // 
            this.lblUom.AutoSize = true;
            this.lblUom.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.lblUom.Location = new System.Drawing.Point(394, 58);
            this.lblUom.Name = "lblUom";
            this.lblUom.Size = new System.Drawing.Size(14, 20);
            this.lblUom.TabIndex = 181;
            this.lblUom.Text = "-";
            this.lblUom.Visible = false;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label24.Location = new System.Drawing.Point(272, 55);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(43, 20);
            this.label24.TabIndex = 180;
            this.label24.Text = "Uom";
            this.label24.Visible = false;
            // 
            // lblQty
            // 
            this.lblQty.AutoSize = true;
            this.lblQty.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.lblQty.Location = new System.Drawing.Point(394, 82);
            this.lblQty.Name = "lblQty";
            this.lblQty.Size = new System.Drawing.Size(14, 20);
            this.lblQty.TabIndex = 179;
            this.lblQty.Text = "-";
            this.lblQty.Visible = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label7.Location = new System.Drawing.Point(272, 80);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(72, 20);
            this.label7.TabIndex = 178;
            this.label7.Text = "Total Qty";
            this.label7.Visible = false;
            // 
            // lblParentUom
            // 
            this.lblParentUom.AutoSize = true;
            this.lblParentUom.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.lblParentUom.Location = new System.Drawing.Point(394, 17);
            this.lblParentUom.Name = "lblParentUom";
            this.lblParentUom.Size = new System.Drawing.Size(14, 20);
            this.lblParentUom.TabIndex = 177;
            this.lblParentUom.Text = "-";
            this.lblParentUom.Visible = false;
            // 
            // lblParUom
            // 
            this.lblParUom.AutoSize = true;
            this.lblParUom.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.lblParUom.Location = new System.Drawing.Point(272, 15);
            this.lblParUom.Name = "lblParUom";
            this.lblParUom.Size = new System.Drawing.Size(94, 20);
            this.lblParUom.TabIndex = 176;
            this.lblParUom.Text = "Parent Uom";
            this.lblParUom.Visible = false;
            // 
            // lblParent
            // 
            this.lblParent.AutoSize = true;
            this.lblParent.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.lblParent.Location = new System.Drawing.Point(139, 38);
            this.lblParent.Name = "lblParent";
            this.lblParent.Size = new System.Drawing.Size(14, 20);
            this.lblParent.TabIndex = 175;
            this.lblParent.Text = "-";
            // 
            // lblPar
            // 
            this.lblPar.AutoSize = true;
            this.lblPar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.lblPar.Location = new System.Drawing.Point(9, 38);
            this.lblPar.Name = "lblPar";
            this.lblPar.Size = new System.Drawing.Size(80, 20);
            this.lblPar.TabIndex = 174;
            this.lblPar.Text = "Basket ID";
            // 
            // lblOrder
            // 
            this.lblOrder.AutoSize = true;
            this.lblOrder.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.lblOrder.Location = new System.Drawing.Point(139, 12);
            this.lblOrder.Name = "lblOrder";
            this.lblOrder.Size = new System.Drawing.Size(14, 20);
            this.lblOrder.TabIndex = 173;
            this.lblOrder.Text = "-";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label9.Location = new System.Drawing.Point(9, 12);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(105, 20);
            this.label9.TabIndex = 172;
            this.label9.Text = "WTO Number";
            // 
            // lblProduct
            // 
            this.lblProduct.AutoSize = true;
            this.lblProduct.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.lblProduct.Location = new System.Drawing.Point(139, 123);
            this.lblProduct.Name = "lblProduct";
            this.lblProduct.Size = new System.Drawing.Size(14, 20);
            this.lblProduct.TabIndex = 171;
            this.lblProduct.Text = "-";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label12.Location = new System.Drawing.Point(9, 123);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(110, 20);
            this.label12.TabIndex = 170;
            this.label12.Text = "Product Name";
            // 
            // lblExp
            // 
            this.lblExp.AutoSize = true;
            this.lblExp.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.lblExp.Location = new System.Drawing.Point(139, 93);
            this.lblExp.Name = "lblExp";
            this.lblExp.Size = new System.Drawing.Size(14, 20);
            this.lblExp.TabIndex = 169;
            this.lblExp.Text = "-";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label10.Location = new System.Drawing.Point(9, 93);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(90, 20);
            this.label10.TabIndex = 168;
            this.label10.Text = "Expiry Date";
            // 
            // lblBatch
            // 
            this.lblBatch.AutoSize = true;
            this.lblBatch.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.lblBatch.Location = new System.Drawing.Point(139, 65);
            this.lblBatch.Name = "lblBatch";
            this.lblBatch.Size = new System.Drawing.Size(14, 20);
            this.lblBatch.TabIndex = 167;
            this.lblBatch.Text = "-";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label1.Location = new System.Drawing.Point(9, 65);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(111, 20);
            this.label1.TabIndex = 166;
            this.label1.Text = "Batch Number";
            // 
            // DetailVaccine
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(979, 634);
            this.Controls.Add(this.panel2);
            this.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DetailVaccine";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Detail";
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVaccine)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        public System.Windows.Forms.Label lblUom;
        public System.Windows.Forms.Label label24;
        public System.Windows.Forms.Label lblQty;
        public System.Windows.Forms.Label label7;
        public System.Windows.Forms.Label lblParentUom;
        public System.Windows.Forms.Label lblParUom;
        public System.Windows.Forms.Label lblParent;
        public System.Windows.Forms.Label lblPar;
        public System.Windows.Forms.Label lblOrder;
        private System.Windows.Forms.Label label9;
        public System.Windows.Forms.Label lblProduct;
        private System.Windows.Forms.Label label12;
        public System.Windows.Forms.Label lblExp;
        private System.Windows.Forms.Label label10;
        public System.Windows.Forms.Label lblBatch;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.DataGridView dgvVaccine;
        public System.Windows.Forms.Label lblTotalDetail;
        private System.Windows.Forms.DataGridViewTextBoxColumn itemid;
        private System.Windows.Forms.DataGridViewTextBoxColumn qty;
        private System.Windows.Forms.DataGridViewTextBoxColumn uom2;

    }
}