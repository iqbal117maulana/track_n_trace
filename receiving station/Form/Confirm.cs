﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Receiving_Station
{
    public partial class Confirm : Form
    {
        public Confirm(string textConfirm, string labelatas)
        {
            InitializeComponent();
            lblMessage.Text = textConfirm;
            this.Text = labelatas;
        }

        public bool conf()
        {
            btnYes.DialogResult = DialogResult.Yes;
            btnNo.DialogResult = DialogResult.No;
            this.ShowDialog();
            if (this.DialogResult == DialogResult.Yes)
            {
                return true;
            }
            else
                return false;
        }

        public Confirm(string textConfirm, string labelatas, MessageBoxButtons oke)
        {
            InitializeComponent();
            lblMessage.Text = textConfirm;
            this.Text = labelatas;
            btnOk.Visible = true;
            btnYes.Visible = false;
            btnNo.Visible = false;
            this.ShowDialog();
        }

        public Confirm(string textConfirm)
        {
            InitializeComponent();
            lblMessage.Text = textConfirm;
            this.Text = "Information";
            btnOk.Visible = true;
            btnYes.Visible = false;
            btnNo.Visible = false;
            this.ShowDialog();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }
    }
}
