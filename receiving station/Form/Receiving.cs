﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace Receiving_Station
{
    public partial class Receiving : Form
    {
        Control ms;
        int ordersItemIndex;
        string[] linenumberrs;
        string parameter;
        public string LogFile = "";
        public String role;
                
        //tambahan disable close button
        private const int CP_NOCLOSE_BUTTON = 0x200;
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams myCp = base.CreateParams;
                myCp.ClassStyle = myCp.ClassStyle | CP_NOCLOSE_BUTTON;
                return myCp;
            }
        }
        //end of disable close button

        public Receiving(string admin)
        {
            InitializeComponent();
            ms = new Control();
            ms.rv = this;
            ms.adminid = admin;
            ms.StartStation();
            setAdminName(admin);
            ms.adminid = admin;
            progressBar1.Visible = false;
        }

        public void setAdminName(string admin)
        {
            //List<string> field = new List<string>();
            //field.Add("first_name, company ");
            //string where = "id = '" + admin + "'";
            //List<string[]> ds = ms.db.selectList(field, "[USERS]", where);
            //if (ms.db.num_rows > 0)
            //{
            //    lblUserId.Text = ds[0][0];
            //    lblRole.Text = ds[0][1];
            //}                

            List<string> field = new List<string>();
            field.Add("a.first_name, c.name, c.id as role ");
            string where = "a.id ='" + admin + "'";
            string from = "users a inner join users_groups b on a.id = b.user_id inner join groups c on b.group_id = c.id";
            List<string[]> ds = ms.db.selectList(field, from, where);
            if (ms.db.num_rows > 0)
            {
                lblUserId.Text = ds[0][0];
                lblRole.Text = ds[0][1];
                role = ds[0][2];
                btnConfig.Visible = role == "1";
            }
        }
        
        private void button2_Click(object sender, EventArgs e)
        {
            if (dgvReceiving.Rows.Count > 0)
            {
                dgvReceiving.Rows.RemoveAt(ordersItemIndex);
                lblQtyReceiving.Text = "" + dgvReceiving.Rows.Count;
                lblQtyHis.Text = "" + dgvReceiving.Rows.Count;
                ms.log("Remove Data");
            }
        }

        private void dgvReceiving_SelectionChanged(object sender, EventArgs e)
        {

            if (dgvReceiving.SelectedRows.Count > 0)
            {
                ordersItemIndex = dgvReceiving.SelectedRows[0].Index;
            }
        }

        private void Receiving_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void txtBarcode_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                ms.AddTableReceiving(txtBarcode.Text);
                txtBarcode.Text = "";
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {

        }

        private void button8_Click(object sender, EventArgs e)
        {
            Confirm cf = new Confirm("Are you sure? ", "Confirmation");
            bool result = cf.conf();
            if (result)
            {
                ms.InsertSystemLog("Logout User : " + lblUserId.Text);
                this.Dispose();
                Login lg = new Login();                
                lg.Show();
            }
        }

        private void groupBox3_Enter(object sender, EventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            LogFile = "";            
        }

        private void button4_Click(object sender, EventArgs e)
        {
            grpBoxDetail.Visible = false;
            grpHistory.Visible = true;
            grpPending.Visible = true;
            lblScan.Visible = true;
            txtBarcode.Visible = true;
            ms.refresh();
        }
        int ordersItemIndex2;
        private void dgvPending_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {

            ms.getDataDetail(dgvPending.Rows[ordersItemIndex2].Cells[0].Value.ToString(), 1, dgvPending.Rows[ordersItemIndex2].Cells[1].Value.ToString());
      
        }

        private void dgvPending_SelectionChanged(object sender, EventArgs e)
        {
            if (dgvPending.SelectedRows.Count > 0)
            {
                ordersItemIndex2 = dgvPending.SelectedRows[0].Index;
            }
            else
            {
                ordersItemIndex2 = 0;
            }
        }

        private void grpBoxDetail_Enter(object sender, EventArgs e)
        {

        }
        int ordersItemIndex3;
        private void dgvVaccine_SelectionChanged(object sender, EventArgs e)
        {
            if (dgvVaccine.SelectedRows.Count > 0)
            {
                ordersItemIndex3 = dgvVaccine.SelectedRows[0].Index;
            }
            else
            {
                ordersItemIndex3 = 0;
            }
        }

        private void dgvVaccine_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            DetailVaccine frm;
            frm = new DetailVaccine(
                dgvVaccine.Rows[ordersItemIndex3].Cells["itemid"].Value.ToString(),
                lblUom.Text,
                dgvVaccine.Rows[ordersItemIndex3].Cells["batchnumber"].Value.ToString(),
                dgvVaccine.Rows.Count,
                lblOrder.Text,
                Convert.ToBoolean(dgvVaccine.Rows[ordersItemIndex3].Cells["isSas"].Value));
            frm.ShowDialog();
        }

        private void btnYes_Click_1(object sender, EventArgs e)
        {
            ms.updatePickinglist(ms.tablePicking);
            ms.getPendingData();            
            grpHistory.Visible = true;
            grpPending.Visible = true;
            lblScan.Visible = true;
            txtBarcode.Visible = true;

            txtOrderNumber.Text = "-";
            lblOrder.Text = "-";
            lblBatch.Text = "-";
            lblUom.Text = "-";
            lblQty.Text = "-";
            dgvVaccine.DataSource = null;

            ms.refresh();
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            ms.getPendingData();
        }

        private void dgvVaccine_DataSourceChanged(object sender, EventArgs e)
        {
            lblQty.Text = dgvVaccine.Rows.Count + " Basket";
        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            ms.InitialDgv();
        }

        private void Receiving_Load(object sender, EventArgs e)
        {
            timer1.Start();
            lblTime.Text = DateTime.Now.ToLongTimeString();
            lblDate.Text = DateTime.Now.ToShortDateString();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            lblTime.Text = DateTime.Now.ToLongTimeString();
            timer1.Start();
        }

        private void txtBatchNumber_Click(object sender, EventArgs e)
        {
            ListTransferOrder lto;
            lto = new ListTransferOrder();
            lto.ShowDialog();
            txtOrderNumber.Text = lto.orderNumber;
            if (txtOrderNumber.Text.Length > 0)
            {                                                
                ms.InsertSystemLog("WTO : "+txtOrderNumber.Text+" Selected ");

                ms.getDataDetail(lto.orderNumber, 1, lto.orderQty);
                ms.log("WTO : " + txtOrderNumber.Text+" Selected");
                //CLEAR DULU SEBELUM DIISI DATA2NA
                ms.dataVaccineScan.Rows.Clear();
                //dgvVaccine.AutoGenerateColumns = false;
                //dgvVaccine.DataSource = ms.dataVaccineScan;
                //--------------------------------------

                grpBoxDetail.Visible = true;
                grpPending.Visible = false;

                lblOrder.Text = lto.orderNumber;

                lblParent.Text = lto.orderNumber;
                lblUom.Text = "Basket";

                lblParentUom.Text = lto.orderNumber;

                lblBatch.Text = "";
                lblExp.Text = "";
                lblProduct.Text = "";

                ms.getDetail(lto.orderNumber);
                ms.setButton(1);

                ms.dataVaccineScan = new DataTable();

                txtBarcode.Focus();
            }
            else
            {
                ms.dataVaccineScan.Rows.Clear();
                dgvVaccine.AutoGenerateColumns = false;
                dgvVaccine.DataSource = ms.dataVaccineScan;
            }
        }

        private void btnOpenLog_Click(object sender, EventArgs e)
        {
            Log frm;
            frm = new Log(LogFile);
            frm.ShowDialog();
            if (frm.isClear == true)
                LogFile = "";
        }

        private void btnConfirmLocation_Click(object sender, EventArgs e)
        {
            if (cmbLocation.SelectedIndex == -1)
            {
                Confirm cf = new Confirm("Choose location first");
                ms.log("Choose location first");
                return;
            }
            if (dgvReceiving.Rows.Count == 0)
            {
                Confirm cf = new Confirm("Empty receiving item");
                ms.log("Empty receiving item");
                return;
            }
            Confirm cf2 = new Confirm(
                "Are you sure want to receive " + lblProduct.Text + " (" + lblQtyHis.Text + ") in warehouse " + cmbLocation.Text + " ?",
                "Confirmation");
            bool result = cf2.conf();
            if (result)
            {
                progressBar1.Visible = true;
                ms.submitReceiving();
                ms.InsertSystemLog("Received "+ lblProduct.Text + " (" + lblQtyHis.Text + ") in warehouse " + cmbLocation.Text);
                progressBar1.Visible = false;
            }
        }

        private void txtBasketMove_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (cmbMoveNewLocation.SelectedIndex == -1)
                {
                    new Confirm("Choose new location", "Information", MessageBoxButtons.OK);
                    ms.log("Choose new location");
                    return;
                }
                if (txtBasketMove.Text == "")
                {
                    new Confirm("Barcode empty", "Information", MessageBoxButtons.OK);
                    ms.log("Barcode empty");
                    return;
                }

                ms.tambahDataMove(txtBasketMove.Text);
                txtBasketMove.Text = "";
            }
        }

        private void button11_Click(object sender, EventArgs e)
        {
            if (dgvMovement.SelectedRows.Count > 0)
            {
                ms.hapusdataMove(dgvMovement.Rows[ordersItemIndex].Cells[0].Value.ToString());
                ms.tableMovement.Rows.RemoveAt(ordersItemIndex);
                dgvMovement.Refresh();
                lblQtyMovement.Text = "" + dgvMovement.Rows.Count;
                if (dgvMovement.Rows.Count == 0)
                {
                    cmbMoveLocation.Enabled = true;
                    cmbMoveNewLocation.Enabled = true;
                    cmbMoveNewLocation.SelectedIndex = -1;
                    for (int i = 0; i < dgvMovement.Columns.Count; i++)
                    {
                        dgvMovement.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                    }
                }
            }
            else
            {
                Confirm cf = new Confirm("No data selected to clear");
                ms.log("No data selected to clear");
            }
        }

        private void button10_Click(object sender, EventArgs e)
        {
            ms.ClearDgvMovement();
        }

        private void button12_Click(object sender, EventArgs e)
        {
            if (dgvMovement.Rows.Count > 0)
            {
                Confirm cf2 = new Confirm("Are you sure want to Confirm Movement ?", "Confirmation");
                bool result = cf2.conf();
                if (result)
                {
                    ms.submitReceivingMove();
                }
            }
            else
            {
                Confirm cf = new Confirm("Movement data is empty");
                ms.log("Movement data is empty");
            }
        }

        private void btnConfig_Click(object sender, EventArgs e)
        {
            config cfg = new config();
            cfg.ShowDialog();
        }

        private void txtBarcode_KeyPress(object sender, KeyPressEventArgs e)
        {
            var regex = new Regex(@"[^a-zA-Z0-9\b]");
            if (regex.IsMatch(e.KeyChar.ToString()))
            {
                e.Handled = true;
            }
        }

        private void txtBasketMove_KeyPress(object sender, KeyPressEventArgs e)
        {
            var regex = new Regex(@"[^a-zA-Z0-9\b]");
            if (regex.IsMatch(e.KeyChar.ToString()))
            {
                e.Handled = true;
            }
        }
    }
}
