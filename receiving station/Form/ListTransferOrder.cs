﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace Receiving_Station
{
    public partial class ListTransferOrder : Form
    {        
        dbaccess database = new dbaccess();
        string parameter;
        //Dashboard dsbb;
        string linenumber;
        string frm;
        Control ctrlCap = new Control();
        public string orderNumber = null;
        public string orderQty = null;

        DataTable dataPending;
        
        //public ListTransferOrder(Dashboard dsb, string line, string param, string from)
        public ListTransferOrder()
        {
            //parameter = param;
            //linenumber = line;
            InitializeComponent();
            refresh();
            SetDataGridListOrder();
            //dsbb = dsb;
            //frm = from;
        }

        public string getOrderQty(string dataOrder)
        {
            List<string> field = new List<string>();
            field.Add("SUM (dbo.receivingOrder_detail.qty) AS [Order Qty]");
            string where = "dbo.receiving_order.status = '0' AND dbo.receiving_order.receivingType = '1' and receiving_order.receivingOrderNumber = '" + dataOrder + "' GROUP BY dbo.receivingOrder_detail.receivingOrderNumber";
            string from = "dbo.receivingOrder_detail INNER JOIN dbo.receiving_order ON dbo.receivingOrder_detail.receivingOrderNumber = dbo.receiving_order.receivingOrderNumber";
            List<string[]> ds = database.selectList(field, from, where);
            if (database.num_rows > 0)
            {
                return ds[0][0];
            }
            return "0";

        }

        public void SetDataGridListOrder()
        {
            List<string> field = new List<string>();
            field.Add("dbo.receivingOrder_detail.receivingOrderNumber AS [Transfer Order],	sum(dbo.receivingOrder_detail.qty) AS [Order Qty],	SUM (COALESCE(PL.qty, 0)) AS [Actual Qty]");
            string where = "dbo.receiving_order.status = '0' AND dbo.receiving_order.receivingType = '1' GROUP BY  dbo.receivingOrder_detail.receivingOrderNumber";
            string from = "dbo.receivingOrder_detail INNER JOIN dbo.packaging_order ON dbo.packaging_order.batchNumber = dbo.receivingOrder_detail.batchnumber " +
                            "INNER JOIN dbo.product_model ON dbo.receivingOrder_detail.productModelId = dbo.product_model.product_model "+
                            "INNER JOIN dbo.receiving_order ON dbo.receivingOrder_detail.receivingOrderNumber= dbo.receiving_order.receivingOrderNumber " +
                            "LEFT JOIN (SELECT * FROM dbo.pickingList WHERE dbo.pickingList.status = '1') PL ON PL.detailNo = dbo.receivingOrder_detail.receivingOrderDetailNo";
            List<string[]> ds = database.selectList(field, from, where);
            if (database.num_rows > 0)
            {
                dataPending = database.dataHeader;
                for (int i = 0; i < ds.Count; i++)
                {
                    DataRow row = dataPending.NewRow();
                    for (int j = 0; j < ds[i].Length; j++)
                    {
                        row[j] = ds[i][j];

                        if (j == 5)
                        {
                            if (row[j].ToString().Length == 0)
                                row[j] = "0";
                        }

                    }
                    row[1] = getOrderQty(ds[i][0]);
                    dataPending.Rows.Add(row);
                    dgvListOrder.DataSource = dataPending;
                }
                //rv.lblQtyReceiving.Text = "" + ds.Count;
            }
            else
            {
                refresh();
            }
        }

        internal void refresh()
        {
            dataPending = new DataTable();
            dataPending.Columns.Add("No");
            dataPending.Columns.Add("Transfer Order");
            dataPending.Columns.Add("Order Qty");
            dataPending.Columns.Add("Actual Qty");            
            dgvListOrder.DataSource = dataPending;
            //this.dgvListOrder.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            //this.dgvListOrder.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            //this.dgvListOrder.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            //this.dgvListOrder.Columns[3].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                if (dgvListOrder.Rows[e.RowIndex].Cells[e.ColumnIndex].Value != null)
                {
                    orderNumber = this.dgvListOrder.Rows[e.RowIndex].Cells["Transfer Order"].FormattedValue.ToString();
                    orderQty = this.dgvListOrder.Rows[e.RowIndex].Cells["Order Qty"].FormattedValue.ToString();
                    //if (frm.Equals("dashboard pentabio"))
                    //{
                    //    bs = new ManualPacker(dsbb, dsbb.admin, dsbb.linenumber, parameter, ctrlCap);
                    //    bs.txtBatchNumber.Text = batch;
                    //    bs.lblUserId.Text = dsbb.lblUserId.Text;
                    //    bs.lblRole.Text = dsbb.lblRole.Text;
                    //    int[] cnt = ctrlCap.getCount(batch);
                    //    bs.lblQtyHistoryAll.Text = cnt[0].ToString();
                    //    bs.Show();
                    //    Close();
                    //    dsbb.Hide();
                    //}
                }
                Close();
            }
        }

        private void txtBatchNumber_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            var regex = new Regex(@"[^a-zA-Z0-9\b]");
            if (regex.IsMatch(e.KeyChar.ToString()))
            {
                e.Handled = true;
            }
        }

        private void txtBatchNumber_TextChanged_1(object sender, EventArgs e)
        {
            (dgvListOrder.DataSource as DataTable).DefaultView.RowFilter = string.Format("[Transfer Order] LIKE '%{0}%'", txtBatchNumber.Text);
        }

        private void ListTransferOrder_Load(object sender, EventArgs e)
        {

        }
    }
}
