﻿namespace Receiving_Station
{
    partial class config
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtNamaDb = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtIpDb = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtPortDb = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtNamaDb
            // 
            this.txtNamaDb.Location = new System.Drawing.Point(84, 67);
            this.txtNamaDb.Name = "txtNamaDb";
            this.txtNamaDb.Size = new System.Drawing.Size(100, 20);
            this.txtNamaDb.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 70);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "DB Name";
            // 
            // txtIpDb
            // 
            this.txtIpDb.Location = new System.Drawing.Point(84, 15);
            this.txtIpDb.Name = "txtIpDb";
            this.txtIpDb.Size = new System.Drawing.Size(100, 20);
            this.txtIpDb.TabIndex = 0;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 18);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(35, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "IP DB";
            // 
            // txtPortDb
            // 
            this.txtPortDb.Location = new System.Drawing.Point(84, 41);
            this.txtPortDb.Name = "txtPortDb";
            this.txtPortDb.Size = new System.Drawing.Size(100, 20);
            this.txtPortDb.TabIndex = 1;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 44);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(44, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "Port DB";
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.button1.Image = global::Receiving_Station.Properties.Resources.submit;
            this.button1.Location = new System.Drawing.Point(26, 98);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(150, 40);
            this.button1.TabIndex = 4;
            this.button1.Text = "Submit";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // config
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(194, 152);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.txtPortDb);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtIpDb);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtNamaDb);
            this.Controls.Add(this.label3);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "config";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Configuration";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtNamaDb;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtIpDb;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtPortDb;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button button1;
    }
}