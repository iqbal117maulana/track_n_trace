﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Receiving_Station
{
    public partial class config : Form
    {
        public config()
        {
            InitializeComponent();
            getConfig();
        }

        void getConfig()
        {
            try
            {
                sqlitecs sql = new sqlitecs();
                txtIpDb.Text = sql.config["ipDB"];
                txtPortDb.Text = sql.config["portDB"];
                txtNamaDb.Text = sql.config["namadb"];
            }
            catch (KeyNotFoundException k)
            {

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Confirm cf = new Confirm("Are you sure? ", "Confirmation");
            bool hasilDialog = cf.conf();
            if (hasilDialog)
            {
                if (validasi())
                {
                    sqlitecs sql = new sqlitecs();
                    Dictionary<string, string> field = new Dictionary<string, string>();
                    field.Add("ipDB", txtIpDb.Text);
                    field.Add("portDB", txtPortDb.Text);
                    field.Add("namadb", txtNamaDb.Text);
                    sql.update(field, "config", "");
                    Confirm cf1 = new Confirm("Configuration success saved", "Information", MessageBoxButtons.OK);
                    this.Dispose();
                }
                else
                {
                    Confirm cf1 = new Confirm("Invalid configuration data", "Information", MessageBoxButtons.OK);
                }
            }
        }

        private bool validasi()
        {
            if (txtIpDb.Text.Length > 0 && dbaccess.cekIP(txtIpDb.Text))
            {
                return true;
            }
            return false;
        }
    }
}
