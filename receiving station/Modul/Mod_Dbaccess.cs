﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Configuration;
using System.Windows.Forms;
using System.Data;
using System.IO;
using System.Diagnostics;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using System.Net;
using System.Net.Sockets;

namespace Receiving_Station
{
    class dbaccess
    {
        static string valid1;
        public static SqlConnection Connection;
        public static SqlCommand Command;
        public static SqlDataReader DataReader;
        public static SqlDataAdapter dataadapter;
        public List<string> dataInsert = new List<string>();
        public List<string> dataUpdate = new List<string>();
        public DataTable dataHeader;
        public int num_rows = 0;
        sqlitecs sqlite;
        public string valid;
        public bool sysLog = true;
        public string adminid;
        public int PackagingqtyOnprogress;
        public string eventname;
        public string eventType = "12";
        public string movementType = "6";
        public string from;
        public string uom;
        string eventtype;
        public bool isConnect;

        public int fcon = 0;
        public string errorMessage;

        public dbaccess()
        {
            sqlite = new sqlitecs();
            Connect();
        }

        public bool Connect()
        {
            //if (!isConnect)
            //{
            try
            {
                string connec = "Data Source=" + sqlite.config["ipDB"] + ";" +
                                "Initial Catalog=" + sqlite.config["namadb"] + ";" +
                                "User id=" + sqlite.config["usernameDB"] + ";" +
                                "Password=" + sqlite.config["passDB"] + ";" +
                                 "Connection Timeout=" + sqlite.config["timeout"] + ";";
                Connection = new SqlConnection(connec);
                Connection.Open();
                fcon = 0;
                errorMessage = "";
                return true;
            }
            catch (TimeoutException t)
            {
                Console.WriteLine("MASUK CATCH 1");
                fcon++;
                errorMessage = t.Message;
                if (fcon < 2)
                {
                    Console.WriteLine("MASUK CATCH 1 IF");
                    new Confirm("Can not open connection to Database Server.", "Information", MessageBoxButtons.OK);
                    config cfg = new config();
                    cfg.ShowDialog();
                }
            }
            catch (SqlException se)
            {
                Console.WriteLine("MASUK CATCH 2 : " + se.Message);
                fcon++;
                Console.WriteLine("FCON : " + fcon);
                errorMessage = se.Message;
                if (fcon < 2)
                {
                    Console.WriteLine("MASUK CATCH 2 IF : " + se.Message);
                    errorMessage = se.Message;
                    new Confirm("Can not open connection to Database Server.", "Information", MessageBoxButtons.OK);
                    config cfg = new config();
                    cfg.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("MASUK CATCH 3");
                fcon++;
                errorMessage = ex.Message;
                if (fcon < 2)
                {
                    Console.WriteLine("MASUK CATCH 3 IF");
                    new Confirm("Can not open connection to Database Server.", "Information", MessageBoxButtons.OK);
                    simpanLog("Error SQLSERVER : " + ex.ToString());
                    config cfg = new config();
                    cfg.ShowDialog();
                }
            }
            return false;
            //}
        }

        public void closeConnection()
        {
            Connection.Close();
        }

        //public void Connect()
        //{
        //    if (!isConnect)
        //    {
        //        try
        //        {
        //            string connec = "Data Source=" + sqlite.config["ipDB"] + ";" +
        //                            "Initial Catalog=" + sqlite.config["namadb"] + ";" +
        //                            "User id=" + sqlite.config["usernameDB"] + ";" +
        //                            "Password=" + sqlite.config["passDB"] + ";" +
        //                             "Connection Timeout=" + sqlite.config["timeout"] + ";";
        //            Connection = new SqlConnection(connec);
        //            try
        //            {
        //                isConnect = true;
        //                Connection.Open();
        //            }
        //            catch (Exception ex)
        //            {
        //                new Confirm("Can not open connection ! " + ex.ToString());
        //                simpanLog("Error SQLSERVER : " + ex.ToString());
        //                config cfg = new config();
        //                cfg.Show();
        //                //Application.Exit();
        //            }
        //        }
        //        catch (TimeoutException t)
        //        {
        //            new Confirm("Cannot Connect Database");
        //            config cfg = new config();
        //            cfg.Show();
        //        }

        //    }
        //}

        public static List<String[]> LoadProductionOrders()
        {
            List<String[]> Results = new List<String[]>();
            String Query = "SELECT production_order_number, created_date, status FROM transaction_production_order";

            Command = new SqlCommand(Query, Connection);
            DataReader = Command.ExecuteReader();

            while (DataReader.Read())
            {
                String[] Data = new String[] { DataReader.GetValue(0).ToString(), DataReader.GetValue(1).ToString(), DataReader.GetValue(2).ToString() };
                Results.Add(Data);
            }

            DataReader.Close();
            Command.Dispose();

            return Results;
        }

        public void insert(Dictionary<string, string> field, string table)
        {
            try
            {
                string sql = "INSERT INTO " + table + " (";
                int i = 0;
                foreach (string key in field.Keys)
                {
                    sql += "" + key + "";
                    if (i + 1 < field.Count)
                    {
                        sql += ",";
                        i++;
                    }
                }

                sql += ") values (";

                i = 0;
                foreach (string key in field.Values)
                {
                    sql += "'" + key + "'";
                    if (i + 1 < field.Count)
                    {
                        sql += ",";
                        i++;
                    }
                }
                sql += ")";
                simpanLog(sql);
                Connect();
                Command = new SqlCommand(sql, Connection);
                Command.ExecuteNonQuery();
            }
            catch (SqlException sq)
            {
                simpanLog("Error SQl : " + sq.ToString());
            }
        }

        public void insertMulti(Dictionary<string, string> field, string table, DataTable itemid)
        {
            try
            {
                string sql = "INSERT INTO " + table + " (";
                sql += "movementId";
                foreach (string key in field.Keys)
                {
                    sql += "," + key + "";
                }
                sql += ",itemid"; //HARDCORE SEMENTARA SESUAI KEBUTUHAN DULU
                sql += ") values ";

                int i = 0;
                string dates = DateTime.Now.ToString("ssMMddmmHHssyy");
                foreach (DataRow row2 in itemid.Rows)
                {
                    sql += "(";
                    dates = (i+1) + DateTime.Now.ToString("ssMMddmmHHssyy");
                    sql += "'" + dates + "'";
                    foreach (string key in field.Values)
                    {
                        sql += ",'" + key + "'";
                    }
                    sql += ",'" + row2["Picking"].ToString() + "'";
                    sql += "),";
                    i += 1;
                }
                sql = sql.Substring(0, sql.Length - 1);
                simpanLog(sql);
                Connect();
                Command = new SqlCommand(sql, Connection);
                Command.ExecuteNonQuery();
            }
            catch (SqlException sq)
            {
                simpanLog("Error SQl : " + sq.ToString());
            }
        }

        public void update(Dictionary<string, string> field, string table, string where)
        {
            try
            {
                string sql = "UPDATE " + table + " SET ";
                int i = 0;
                foreach (KeyValuePair<string, string> key in field)
                {
                    if (key.Value.Equals("GETDATE()"))
                    {
                        sql += key.Key + " = " + "" + key.Value + "";
                    }
                    else {
                        sql += key.Key + " = " + "'" + key.Value + "'";
                    }
                    
                    if (i + 1 < field.Count)
                    {
                        sql += ",";
                        i++;
                    }
                }

                if (where.Length > 0)
                    sql += " WHERE " + where;

                simpanLog(sql);
                Connect();
                Command = new SqlCommand(sql, Connection);
                Command.ExecuteNonQuery();
            }
            catch (SqlException sq)
            {
                simpanLog("Error SQl : " + sq.ToString());
            }
        }

        public void OpenQuery(out DataTable data, String sql)
        {
            data = new DataTable();
            try
            {

                simpanLog(sql);
                if (Connect())
                {

                    Command = new SqlCommand(sql, Connection);
                    DataReader = Command.ExecuteReader();
                    data.Load(DataReader);

                }
                closeConnection();

            }
            catch (SqlException sq)
            {
                var st = new StackTrace(sq, true);
                var frame = st.GetFrame(0);
                string line = frame.ToString();
                simpanLog("Error  00100: " + line);
            }
            catch (NullReferenceException nl)
            {

                var st = new StackTrace(nl, true);
                var frame = st.GetFrame(0);
                string line = frame.ToString();
                simpanLog("Error  00100: " + line);
            }
            finally
            {
                try
                {
                    DataReader.Close();
                    Command.Dispose();
                    Connection.Close();
                }
                catch (SqlException sq)
                {
                    simpanLog("Error SQLSERVER : " + sq.ToString());
                }
                catch (NullReferenceException nl)
                {
                    simpanLog("Error SQLSERVER : " + nl.ToString());
                }

            }
        }

        public DataSet select(List<string> field, string table, string where)
        {
            try
            {
                string sql = "SELECT ";
                for (int j = 0; j < field.Count; j++)
                {
                    sql += "'" + field[j] + "'";
                    if (j + 1 < field.Count)
                    {
                        sql += ",";
                        j++;
                    }
                }

                sql += " From " + table;

                if (where.Length > 0)
                    sql += " WHERE " + where;

                simpanLog(sql);Connect();
                dataadapter = new SqlDataAdapter(sql, Connection);
                DataSet Results = new DataSet();
                dataadapter.Fill(Results);
                return Results;
            }
            catch (SqlException sq)
            {
                simpanLog("Error SQLSERVER : " + sq.ToString());
            }
            finally
            {
                try
                {
                    DataReader.Close();
                    Command.Dispose();
                }
                catch (SqlException sq)
                {
                    simpanLog("Error SQLSERVER : " + sq.ToString());
                }
                catch (NullReferenceException nl)
                {
                    simpanLog("Error SQLSERVER : " + nl.ToString());
                }

            }
            return null;
        }

        public List<String[]> selectList(List<string> field, string table, string where)
        {
            try
            {
                string sql = "SELECT ";
                for (int j = 0; j < field.Count; j++)
                {
                    sql += field[j];
                    if (j + 1 < field.Count)
                    {
                        sql += ",";
                    }
                }
                sql += " FROM " + table;

                if (where.Length > 0)
                    sql += " WHERE " + where;

                simpanLog(sql);
                List<String[]> Results = new List<String[]>();
                int num = 0;
                if (Connect())
                {
                    Command = new SqlCommand(sql, Connection);
                    DataReader = Command.ExecuteReader();
                    while (DataReader.Read())
                    {
                        string[] data = new string[DataReader.FieldCount];
                        for (int u = 0; u < DataReader.FieldCount; u++)
                        {
                            data[u] = DataReader.GetValue(u).ToString();
                        }

                        Results.Add(data);
                        num++;
                    }

                    num_rows = num;
                    dataHeader = new DataTable();
                    for (int i = 0; i < DataReader.FieldCount; i++)
                    {
                        dataHeader.Columns.Add(DataReader.GetName(i));
                    }
                }
                closeConnection();
                return Results;

            }
            catch (SqlException sq)
            {
                var st = new StackTrace(sq, true);
                var frame = st.GetFrame(0);
                string line = frame.ToString();
                simpanLog("Error  00100: " + line);
            }
            catch (NullReferenceException nl)
            {

                var st = new StackTrace(nl, true);
                var frame = st.GetFrame(0);
                string line = frame.ToString();
                simpanLog("Error  00100: " + line);
            }
            finally
            {
                try
                {
                    DataReader.Close();
                    Command.Dispose();
                    Connection.Close();
                }
                catch (SqlException sq)
                {
                    simpanLog("Error SQLSERVER : " + sq.ToString());
                }
                catch (NullReferenceException nl)
                {
                    simpanLog("Error SQLSERVER : " + nl.ToString());
                }

            }
            return null;
        }

        public static List<String> GetPODetails(String PO)
        {
            List<String> Results = new List<String>();
            String Query = String.Format("SELECT production_order_number, product_code, product_name, product_quantity, status FROM transaction_production_order WHERE production_order_number = '{0}'", PO);

            Command = new SqlCommand(Query, Connection);
            DataReader = Command.ExecuteReader();

            while (DataReader.Read())
            {
                for (int i = 0; i < 5; i++)
                {
                    Results.Add(DataReader.GetValue(i).ToString());
                }
            }

            DataReader.Close();
            Command.Dispose();

            return Results;
        }

        public void simpanLog(String line)
        {
            DateTime now = DateTime.Now;
            string tahun = now.ToString("yyyy");
            string bulan = now.ToString("MM");
            string hari = now.ToString("dd");
            string dates = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            line = dates + "\t" + line;
            // cek file 
            bool cekfile = false;
            while (!cekfile)
            {
                if (Directory.Exists("eventlog"))
                {
                    if (Directory.Exists("eventlog" + @"\" + tahun))
                    {
                        if (Directory.Exists("eventlog" + @"\" + tahun + @"\" + bulan))
                        {

                            using (StreamWriter outputFile = new StreamWriter("eventlog" + @"\" + tahun + @"\" + bulan + @"\" + hari + ".txt", true))
                            {
                                outputFile.WriteLine(line);
                            }
                            cekfile = true;
                        }
                        else
                        {

                            Directory.CreateDirectory("eventlog" + @"\" + tahun + @"\" + bulan);
                        }
                    }
                    else
                    {
                        Directory.CreateDirectory("eventlog" + @"\" + tahun);
                    }
                }
                else
                {

                    Directory.CreateDirectory("eventlog");
                }
            }
        }

        internal bool cekPermision(string adminid, string modul_name, string permission)
        {
            List<string> field = new List<string>();
            field.Add("permissions_name");
            string where = "dbo.users.id = '" + adminid + "' AND dbo.permissions.module_name = '" + modul_name + "' AND dbo.permissions.permissions_name = '" + permission + "'";
            string from = "dbo.groups INNER JOIN dbo.permissions ON dbo.groups.id = dbo.permissions.role_id INNER JOIN dbo.users_groups ON dbo.users_groups.group_id = dbo.groups.id  INNER JOIN dbo.users ON dbo.users_groups.user_id = dbo.users.id";
            selectList(field, from, where);
            if (num_rows > 0)
                return true;
            return false;
        }

        public string md5hash(string source)
        {
            using (MD5 md5Hash = MD5.Create())
            {
                string hash = GetMd5Hash(md5Hash, source);
                return hash;
            }
            return null;
        }

        static string GetMd5Hash(MD5 md5Hash, string input)
        {

            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }

        static bool VerifyMd5Hash(MD5 md5Hash, string input, string hash)
        {
            // Hash the input.
            string hashOfInput = GetMd5Hash(md5Hash, input);

            // Create a StringComparer an compare the hashes.
            StringComparer comparer = StringComparer.OrdinalIgnoreCase;

            if (0 == comparer.Compare(hashOfInput, hash))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void Movement(Dictionary<string, string> field)
        {
            string from = "movement_history";
            DateTime now = DateTime.Now;
            string dates = DateTime.Now.ToString("yyMMddHHmmss");
            field.Add("movementId", dates);
            field.Add("eventTime", "" + (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1)).TotalSeconds));
            insert(field, from);
        }

        public string getUnixString()
        {
            string date = "" + (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1)).TotalSeconds);
            return date;
        }

        public string getUnixString(double data)
        {
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(data).ToLocalTime();
            return dtDateTime.ToShortDateString();
        }

        public void insertData(Dictionary<string, string> field, string table)
        {
            try
            {
                string sql = "INSERT INTO " + table + " (";
                int i = 0;
                foreach (string key in field.Keys)
                {
                    sql += "" + key + "";
                    if (i + 1 < field.Count)
                    {
                        sql += ",";
                        i++;
                    }
                }

                sql += ") values (";

                i = 0;
                foreach (string key in field.Values)
                {
                    sql += "'" + key + "'";
                    if (i + 1 < field.Count)
                    {
                        sql += ",";
                        i++;
                    }
                }
                sql += ")";
                simpanLog(sql);
                dataInsert.Add(sql);
            }
            catch (SqlException sq)
            {
                simpanLog("Error SQl : " + sq.ToString());
            }
        }

        public void updateData(Dictionary<string, string> field, string table, string where)
        {
            try
            {
                string sql = "UPDATE " + table + " SET ";
                int i = 0;
                foreach (KeyValuePair<string, string> key in field)
                {
                    sql += key.Key + " = " + "'" + key.Value + "'";
                    if (i + 1 < field.Count)
                    {
                        sql += ",";
                        i++;
                    }
                }

                if (where.Length > 0)
                    sql += " WHERE " + where;

                simpanLog(sql);
                dataInsert.Add(sql);
            }
            catch (SqlException sq)
            {
                simpanLog("Error SQl : " + sq.ToString());
            }
        }

        public void Begin()
        {
            try
            {
                string sql = "BEGIN TRANSACTION;\n ";

                for (int i = 0; i < dataInsert.Count; i++)
                {
                    sql += dataInsert[i] + "\n";
                }
                sql += "COMMIT;";
                simpanLog("BEGIN TRANSACTION");
                Connect();
                Command = new SqlCommand(sql, Connection);
                Command.ExecuteNonQuery();
                dataInsert = new List<string>();
            }
            catch (SqlException sq)
            {
                simpanLog("Error " + sq.ToString());

            }
        }
        public bool cekRole(string adminid, string module)
        {
            List<string> field = new List<string>();
            field.Add("permission.[module]");
            string from = "[user] INNER JOIN user_role ON [user].role = user_role.role_id INNER JOIN permission ON permission.roleId = user_role.role_id";
            string where = "permission.[read] = '0' AND [user].userId = '" + adminid + "' AND permission.[module] = '" + module + "'";
            List<string[]> ds = selectList(field, from, where);
            if (num_rows > 0)
                return true;
            else
                return false;
        }

        public void systemlog()
        {
            Dictionary<string, string> field = new Dictionary<string, string>();
            field.Add("eventtime", getUnixString());
            field.Add("eventname", eventname);
            field.Add("eventtype", eventType);
            field.Add("userid", adminid);
            field.Add("[from]", from);
            insert(field, "system_log");
        }

        public void Movement(string order, string from, string to, string qty, string itemid)
        {
            string dates = DateTime.Now.ToString("ssMMddmmHHssyy");
            Dictionary<string, string> field = new Dictionary<string, string>();
            field.Add("movementId", dates);
            field.Add("orderNumber", order);
            field.Add("[from]", from);
            field.Add("movementType", movementType);
            field.Add("[to]", to);
            field.Add("qty", qty);
            field.Add("uom", uom);
            field.Add("userid", adminid);
            field.Add("itemid", itemid);
            field.Add("eventTime", getUnixString());
            insert(field, "movement_history");
        }

        public void MovementMulti(string order, string from, string to, string qty, DataTable itemid)
        {
            //string dates = DateTime.Now.ToString("ssMMddmmHHssyy");
            Dictionary<string, string> field = new Dictionary<string, string>();
            //field.Add("movementId", dates);
            field.Add("orderNumber", order);
            field.Add("[from]", from);
            field.Add("movementType", movementType);
            field.Add("[to]", to);
            field.Add("qty", qty);
            field.Add("uom", uom);
            field.Add("userid", adminid);
            //field.Add("itemid", itemid);
            field.Add("eventTime", getUnixString());
            insertMulti(field, "movement_history", itemid);
        }

        public List<String[]> selectList(string sql)
        {
            try
            {
               
                simpanLog(sql);
            Connect();
                List<String[]> Results = new List<String[]>();
                Command = new SqlCommand(sql, Connection);
                DataReader = Command.ExecuteReader();
                int num = 0;
                while (DataReader.Read())
                {
                    string[] data = new string[DataReader.FieldCount];
                    for (int u = 0; u < DataReader.FieldCount; u++)
                    {
                        data[u] = DataReader.GetValue(u).ToString();
                    }

                    Results.Add(data);
                    num++;
                }
                num_rows = num;
                dataHeader = new DataTable();
                for (int i = 0; i < DataReader.FieldCount; i++)
                {
                    dataHeader.Columns.Add(DataReader.GetName(i));
                }

                return Results;
            }
            catch (SqlException sq)
            {
                var st = new StackTrace(sq, true);
                var frame = st.GetFrame(0);
                string line = frame.ToString();
                simpanLog("Error  00100: " + line);
            }
            catch (NullReferenceException nl)
            {

                var st = new StackTrace(nl, true);
                var frame = st.GetFrame(0);
                string line = frame.ToString();
                simpanLog("Error  00100: " + line);
            }
            finally
            {
                try
                {
                    DataReader.Close();
                    Command.Dispose();
                }
                catch (SqlException sq)
                {
                    simpanLog("Error SQLSERVER : " + sq.ToString());
                }
                catch (NullReferenceException nl)
                {
                    simpanLog("Error SQLSERVER : " + nl.ToString());
                }

            }
            return null;
        }

       internal static bool cekIP(string data)
       {
           if (data.Length < 3 & data.Length > 0)
               return false;
           int errorCounter = Regex.Matches(data, @"[a-zA-Z]").Count;
           if (errorCounter == 0)
           {
               string[] dapat = data.Split('.');
               if (dapat.Length != 4)
               {
                   valid1 = "Error : IP must 4 point";
                   return false;
               }
               if (dapat[3].Equals("0"))
               {
                   valid1 = "Error : the fourth digit of ip cannot be 0";
                   return false;
               }

               bool kosong = false;
               if (dapat[0].Equals("000"))
                   kosong = true;
               for (int i = 0; i < dapat.Length; i++)
               {
                   //if (dapat[i] == "" || dapat[i].Length <= 0)
                   //{
                   //    new Confirm("Error : IP must 4 point");
                   //    return false;
                   //}
                   if (dapat[i][0].Equals("0"))
                       return false;
                   if (dapat[i].Equals("00"))
                   {
                       valid1 = "Error : IP cannot fill 00";
                       return false;
                   }

                   bool rege = Regex.Match(dapat[i], @"^[0-9]+$").Success;
                   if (!rege)
                   {
                       valid1 = "Error : IP cannot contain alphabeth and symbols " + data;
                       return false;
                   }
                   if (dapat[i].Length == 0)
                   {
                       valid1 = "Error : Cannot empty";
                       return false;
                   }
                   int temp = int.Parse(dapat[i]);
                   if (temp >= 255)
                   {
                       valid1 = "Error : IP must be less then 255";
                       return false;
                   }
                   if (temp == 0 && kosong)
                   {
                       valid1 = "Error : First digit of ip cannot be 0";
                       return false;
                   }
                   if (dapat[i].Equals("000") && !kosong)
                   {
                       valid1 = "Error : IP cannot fill 000";
                       return false;
                   }
                   if (dapat[i][0].Equals('0') && temp != 0 && dapat[i].Length > 1)
                   {
                       valid1 = "Error : Ip cannot fill '0'";
                       return false;
                   }
               }
               return true;
           }
           else
           {
               valid1 = "Error : IP cannot contain alphabeth " + errorCounter;
               return false;
           }
           return true;
       }

       public bool validasi(string username)
       {
           try
           {
               if (username.Length > 0)
               {
                   List<string> field = new List<string>();
                   field.Add("id");
                   string where = "username = '" + username + "'";
                   List<string[]> result = selectList(field, "[users]", where);
                   if (result.Count > 0)
                   {
                       return true;
                   }
                   else
                       return false;
               }
               else
               {
                   return false;
               }
           }
           catch (NullReferenceException nl)
           {
               simpanLog("Error  00100: " + nl.Message);
               return false;
           }

       }

       public List<string[]> validasi(string username, string password, string status)
       {
           try
           {
               if (username.Length > 0 && password.Length > 0)
               {
                   List<string> field = new List<string>();
                   field.Add("id");
                   field.Add("first_name");
                   string where = "username = '" + username + "' AND password = '" + md5hash(password) + "' AND Active = " + status;
                   List<string[]> result = selectList(field, "[users]", where);
                   if (result.Count > 0)
                   {
                       from = "Receiving Station";
                       adminid = result[0][0];
                       eventType = "1";
                       eventname = "Login User : "+username;
                       systemlog();
                       eventtype = "12";
                       return result;
                   }
                   else
                       return null;
               }
               else
               {
                   string code = "0010";
                   simpanLog(code + " : USERNAME DAN PASSWORD SALAH");
                   return null;
               }
           }
           catch (NullReferenceException nl)
           {
               var st = new StackTrace(nl, true);
               var frame = st.GetFrame(0);
               string line = frame.ToString();
               simpanLog("Error  00100: " + line);
               return null;
           }

       }

       public bool validasi(string username, string password)
       {
           try
           {
               if (username.Length > 0 && password.Length > 0)
               {
                   List<string> field = new List<string>();
                   field.Add("id");
                   string where = "username = '" + username + "' AND password = '" + md5hash(password) + "'";
                   List<string[]> result = selectList(field, "[users]", where);
                   if (result.Count > 0)
                   {
                       return true;
                   }
                   else
                       return false;
               }
               else
               {
                   string code = "0010";
                   simpanLog(code + " : USERNAME DAN PASSWORD SALAH");
                   return false;
               }
           }
           catch (NullReferenceException nl)
           {
               var st = new StackTrace(nl, true);
               var frame = st.GetFrame(0);
               string line = frame.ToString();
               simpanLog("Error  00100: " + line);
               return false;
           }

       }
    
    }
}

