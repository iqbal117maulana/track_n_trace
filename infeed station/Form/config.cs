﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace Infeed_Station
{
    public partial class config : Form
    {
        Infeed blis;
        public config(Infeed bl, int stat, string admin)
        {
            blis = bl;
            InitializeComponent();
            getConfig();
        }

        public config()
        {
            InitializeComponent();
            getConfig();
        }



        void getConfig()
        {
            Mod_Sqlite sql = new Mod_Sqlite();
            try
            {
                txtIpCamera.Text = sql.config["ipCamera"];
                txtPortCamera.Text = sql.config["portCamera"];
                txtIpPrinter.Text = sql.config["ipprinter"];
                txtPortPrinter.Text = sql.config["portprinter"];
                txtPortServer.Text = sql.config["portserver"];
                txtIpServer.Text = sql.config["ipServer"];
                txtIpDB.Text = sql.config["ipDB"];
                txtPortDb.Text = sql.config["portDB"];
                txtTimeoutDb.Text = sql.config["timeout"];
                txtIpCamera2.Text = sql.config["ipCamera2"];
                txtPortCamera2.Text = sql.config["portCamera2"];
                txtNamaDB.Text = sql.config["namaDB"];
                txtAudit.Text = sql.config["audit"];
                txtGenerate.Text = sql.config["qtygenerate"];
                txtSend.Text = sql.config["qtysend"];
                txtDelay.Text = sql.config["delay"];
                txtIpCamera3.Text = sql.config["ipCamera3"];
                txtIPPLC.Text = sql.config["IPPLC"];
                txtPortPLC.Text = sql.config["PortPLC"]; 
                if (sql.config["buffer"].Equals("1"))
                {
                    chkBuffer.Checked = true;
                }
                else
                {
                    chkBuffer.Checked = false;
                }
            }
            catch (KeyNotFoundException k)
            {
                sql.simpanLog("Data Tidak Ditemukan : " + k.ToString());
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Confirm cf = new Confirm("Are you sure? ", "Confirmation");
            bool hasilDialog = cf.conf();
            if (hasilDialog)
            {
                Mod_Sqlite sql = new Mod_Sqlite();
                Dictionary<string, string> field = new Dictionary<string, string>();
                if (cekKosong())
                {
                    if (validasi())
                    {
                        field.Add("ipCamera", txtIpCamera.Text);
                        field.Add("portCamera", "" + int.Parse(txtPortCamera.Text));
                        field.Add("ipprinter", txtIpPrinter.Text);
                        field.Add("portPrinter", "" + int.Parse(txtPortPrinter.Text));
                        field.Add("portserver", "" + int.Parse(txtPortServer.Text));
                        field.Add("ipServer", txtIpServer.Text);
                        field.Add("ipDB", txtIpDB.Text);
                        field.Add("portDB", txtPortDb.Text);
                        field.Add("timeout", txtTimeoutDb.Text);
                        field.Add("ipCamera2", txtIpCamera2.Text);
                        field.Add("namaDB", txtNamaDB.Text);
                        field.Add("portCamera2", "" + int.Parse(txtPortCamera2.Text));
                        field.Add("audit", txtAudit.Text);
                        field.Add("qtysend", txtSend.Text);
                        field.Add("qtygenerate", txtGenerate.Text);
                        field.Add("delay", txtDelay.Text);
                        field.Add("ipCamera3", txtIpCamera3.Text);
                        field.Add("IPPLC", txtIPPLC.Text);
                        field.Add("PortPLC", txtPortPLC.Text);
                        if (chkBuffer.Checked)
                        {
                            field.Add("buffer", "1"); 
                        }
                        else
                        {
                            field.Add("buffer", "0"); 
                        }
                        sql.update(field, "config", "");
                        new Confirm("Configuration is successfully saved", "Information",MessageBoxButtons.OK);
                        this.Dispose();
                        if (blis != null)
                            blis.refresh();
                    }
                    else
                    {
                        cf = new Confirm("Configuration wrong format", "Information", MessageBoxButtons.OK);
                    }
                }
                else
                {
                    cf = new Confirm("Configuration can not be empty", "Information", MessageBoxButtons.OK);
                }
            }
        }

        private bool cekKosong()
        {
            if (!cek(txtIpCamera.Text))
                return false;
            if (!cek(txtIpCamera2.Text))
                return false;
            if (!cek(txtIpDB.Text))
                return false;
            if (!cek(txtIpPrinter.Text))
                return false;
            if (!cek(txtIpServer.Text))
                return false;
            if (!cek(txtPortCamera.Text))
                return false;
            if (!cek(txtPortCamera2.Text))
                return false;
            if (!cek(txtPortPrinter.Text))
                return false;
            if (!cek(txtPortServer.Text))
                return false;
            if (!cek(txtDelay.Text))
                return false;
            return true;
        }

        private bool cek(string p)
        {
            if (p.Length > 0) return true; else return false;
        }

        private bool validasi()
        {
            if(!cekIP(txtIpCamera.Text))
                return false; 
            if (!cekIP(txtIpCamera2.Text))
                return false;
            if (!cekIP(txtIpDB.Text))
                return false;
            if (!cekIP(txtIpPrinter.Text))
                return false;
            if (!cekAngka(txtPortCamera.Text))
                return false;
            if (!cekAngka(txtPortCamera2.Text))
                return false;
            if (!cekAngka(txtPortPrinter.Text))
                return false;
            if (!cekAngka(txtPortServer.Text))
                return false;
            if (!cekAngka(txtDelay.Text))
                return false;
            return true;
            
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {

        }

        bool cekIP(string data)
        {
            if (data.Length == 0)
                return true;

            int errorCounter = Regex.Matches(data, @"[a-zA-Z]").Count;
            if (errorCounter == 0)
            {
                string[] dapat = data.Split('.');
                if (dapat[3].Equals("0"))
                {
                    Confirm cf = new Confirm("Error : the fourth digit of ip cannot be 0 " + data, "Information", MessageBoxButtons.OK);
                    return false;
                }
                if (dapat.Length != 4)
                {
                    Confirm cf = new Confirm("Error : IP must 4 point " + data, "Information", MessageBoxButtons.OK);
                    return false;
                }
                bool kosong = false;
                if (dapat[0].Equals("000"))
                    kosong = true;
                for (int i = 0; i < dapat.Length; i++)
                {
                    //if (dapat[i] == "" || dapat[i].Length <= 0)
                    //{
                    //    Confirm cf = new Confirm("Error : IP must 4 point");
                    //    return false;
                    //}

                    if (dapat[i].Equals("00"))
                    {
                        Confirm cf = new Confirm("Error : IP cannot fill 00 " + data, "Information", MessageBoxButtons.OK);
                        return false;
                    }

                    bool rege = Regex.Match(dapat[i], @"^[0-9]+$").Success;
                    if (!rege)
                    {
                        Confirm cf = new Confirm("Error : IP cannot contain alphabeth and symbols " + data, "Information", MessageBoxButtons.OK);
                        return false;
                    }
                    if (dapat[i].Length == 0)
                    {
                        new Confirm("Error : Cannot empty "+data);
                        return false;
                    }
                    int temp = int.Parse(dapat[i]);
                    if (temp >= 255)
                    {
                        new Confirm("Error : IP must be less then 255 "+data);
                        return false;
                    }
                    if (temp == 0 && kosong)
                    {
                        new Confirm("Error : First digit of ip cannot be 0 "+data);
                        return false;
                    }
                    if (dapat[i].Equals("000") && !kosong)
                    {
                        new Confirm("Error : IP cannot fill 000 "+data);
                        return false;
                    }
                    if (dapat[i][0].Equals('0') && temp != 0 && dapat[i].Length > 1)
                    {
                        new Confirm("Error : Ip cannot fill '0' "+data);
                        return false;
                    }
                }
                return true;
            }
            else
            {
                new Confirm("Error : IP cannot contain alphabeth " + data);
                return false;
            }
            return true;
        }

        bool cekAngka(string data)
        {
            if (data.Length == 0)
                return true;
            bool rege = Regex.Match(data, @"^[0-9]+$").Success;
            if (!rege)
            {
                new Confirm("Error : IP cannot contain alphabeth and symbols " + data);
                return false;
            }
            return true;
        }

        private void config_Load(object sender, EventArgs e)
        {

        }
        
    }
}
