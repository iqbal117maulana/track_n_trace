﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Net.Sockets;
using System.Net;

namespace Infeed_Station
{
    public partial class Infeed : Form
    {
        List<string> dataBlister = new List<string>();
        Mod_Sqlite sqlite;
        generateCode Code;
        Mod_Dbaccess db;
        Control ms;
        bool audit = false;
        bool start = false;
        bool startprinting = false;

        public Dashboard dbss;
        string parameter;
        public ListBatchNumber lbn;
        string[] linenumber;

        //tambahan disable close button
        private const int CP_NOCLOSE_BUTTON = 0x200;
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams myCp = base.CreateParams;
                myCp.ClassStyle = myCp.ClassStyle | CP_NOCLOSE_BUTTON;
                return myCp;
            }
        }
        //end of disable close button

        public Infeed(Dashboard dbs, string adminid, string[] line, string param, Control ctrl)
        {
            InitializeComponent();
            this.Text = this.Text + " V " + System.Windows.Forms.Application.ProductVersion;
            sqlite = new Mod_Sqlite();
            Code = new generateCode();
            db = new Mod_Dbaccess();
            ms = ctrl;
            ms.sqlite = sqlite;
            ms.bl = this;
            db.adminid = adminid;
            ms.db = db;
            ms.Code = Code;
            ms.adminid = adminid;
            ms.linename = line[0];
            ms.linenumber = line[1];
            labelLine.Text = line[0];
            ms.startStation();
            ms.log("Server Port : " + sqlite.config["portserver"]);

            dbss = dbs;
            parameter = param;
            linenumber = line;
            labelBatch.Text = "";
            labelProduct.Text = "";
            cekCamera();
            cekDevice();
            cekDB();
        }

        public void close(string ti)
        {
            try
            {
                if (InvokeRequired)
                {
                    this.Invoke(new Action<string>(close), new object[] { ti });
                    return;
                }
                if (ms.srv.isRunning)
                {
                    ms.srv.closeServer();
                    ms.listenPrinter = false;
                }
                Application.Exit();
                Environment.Exit(Environment.ExitCode);
            }
            catch (Exception ex)
            {
            }
        }

        public void close2(string ti)
        {
            try
            {
                if (InvokeRequired)
                {
                    this.Invoke(new Action<string>(close), new object[] { ti });
                    return;
                }
                if (ms.srv.isRunning)
                {
                    ms.srv.closeServer();
                }
            }
            catch (Exception ex_a)
            {
            }
        }

        private void Infeed_FormClosed(object sender, FormClosedEventArgs e)
        {
            close("");
        }
       
        private void cmbBatch_SelectedIndexChanged(object sender, EventArgs e)
        {
            audit = false;
            btnAudit.Text = "Pre-check";
            ms.clearDataSend();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Confirm cf = new Confirm("Are you sure? ", "Confirmation");
            bool result = cf.conf();
            if (result)
            {
                //close2("");
                //db.eventname = "Logout infeed station ";
                //db.systemlog();
                //close("");
                //close2("");
                //db.eventname = "Logout Infeed Station "+ms.adminid;
                //db.systemlog();
                //this.Dispose();
                //Login lg = new Login();
                //lg.Show();
                Environment.Exit(Environment.ExitCode);
                Application.Exit();
            }

        }

        private void button3_Click(object sender, EventArgs e)
        {
            config cfg = new config(this, 1, lblAdmin.Text);
            cfg.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Confirm cf = new Confirm("Are you sure? ", "Confirmation");
            bool hasilDialog = cf.conf();
            if (hasilDialog)
            {
                if (!start)
                {
                    if (cekCamera())
                    {
                        if (cekDevice())
                        {
                            if (cekPLCConnect())
                            {
                                //if (cmbBatch.SelectedIndex > -1)
                                //{
                                    lblQuantityReceive.Text = "0";
                                    ms.batch = txtBatchNumber.Text;
                                    //ms.batch = ms.datacombo[cmbBatch.SelectedIndex][0];
                                    ms.clearBufferGs();
                                    ms.clearDataReceive();
                                    ms.sendStartPrinter();
                                    ms.disable();
                                    btnAudit.Enabled = false;
                                    pictureBox1.Enabled = false;
                                    start = true;
                                    //cmbBatch.Enabled = false;
                                    txtBatchNumber.Enabled = false;
                                    btnStart.Text = "Stop";
                                    btnStart.Image = global::Infeed_Station.Properties.Resources.stop;
                                    ms.SetDataGridSend("0", "");
                                //}
                                //else
                                //    new Confirm("Batchnumber not recognition", "Information", MessageBoxButtons.OK);
                            }
                            else
                            {
                                new Confirm("PLC not ready!", "Information", MessageBoxButtons.OK);
                            }
                        }
                        else
                            new Confirm("Printer not ready!", "Information", MessageBoxButtons.OK);
                    }
                }
                else
                {
                    btnStart.Text = "Start GS1";
                    btnStart.Image = global::Infeed_Station.Properties.Resources.start;
                    txtBatchNumber.Enabled = true;
                    pictureBox1.Enabled = true;
                    ms.Stop();
                    start = false;
                    ms.enable();
                }
            }
        }


        private bool Is_FirstConnect = false;
        public bool cekPLCConnect()
        {
            if (!Is_FirstConnect)
            {
                Is_FirstConnect = ms.checkPLCConnected();
                return Is_FirstConnect;
            }
            else
                return true;
        }

        public bool cekCamera()
        {
            if (ms.PingHost(sqlite.config["ipCamera"]))
            {
                lblCamera.Text = "Online";
                onCamera1.Visible = true;
                offCamera1.Visible = false;

                if (ms.PingHost(sqlite.config["ipCamera2"]))
                {
                    lblCamera2.Text = "Online";
                    onCamera2.Visible = true;
                    offCamera2.Visible = false;

                    if (ms.PingHost(sqlite.config["ipCamera3"]))
                    {
                        lblCamera3.Text = "Online";
                        onCamera3.Visible = true;
                        offCamera3.Visible = false;
                        return true;
                    }
                    else
                    {
                        lblCamera3.Text = "Offline";
                        onCamera3.Visible = false;
                        offCamera3.Visible = true;
                        new Confirm("Camera 3 not connected");
                    }
                }
                else
                {
                    lblCamera2.Text = "Offline";
                    onCamera2.Visible = false;
                    offCamera2.Visible = true;
                    new Confirm("Camera 2 not connected");
                }
            }
            else
            {
                lblCamera.Text = "Offline";
                onCamera1.Visible = false;
                offCamera1.Visible = true;
                new Confirm("Camera 1 not connected");
            }
            return false;
        }

        public bool cekDevice()
        {
            print pr = new print();
            if (pr.cekKoneksi())
            {
                lblPrinter.Text = "Online";
                onPrinter.Visible = true;
                offPrinter.Visible = false;
                return true;
            }
            else
            {
                lblPrinter.Text = "Offline";
                onPrinter.Visible = false;
                offPrinter.Visible = true;
            }
            return false;
        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {
            Confirm cf = new Confirm("Are you sure? ", "Confirmation");
            bool hasilDialog = cf.conf();

            if (hasilDialog)
            {
                if (cekCamera())
                {
                    if (cekDevice())
                    {
                        if (cekPLCConnect())
                        {
                            //if (cmbBatch.SelectedIndex > -1)
                            //{
                            //if (ms.cekPrinterready())
                            //{
                            //ms.batch = ms.datacombo[cmbBatch.SelectedIndex][0];
                            ms.batch = txtBatchNumber.Text;
                            ms.startAudit();
                            audit = true;
                            ms.SendToPrinter(ms.getDataSendToPrinter(1));
                            ms.SetDataGridSend("0", "");
                            //}
                            //   else
                            //{
                            //   Confirm cf2 = new Confirm("Printer not ready !", "Information",MessageBoxButtons.OK);
                            //}
                            //}
                        }
                        else
                            new Confirm("PLC not ready!", "Information", MessageBoxButtons.OK);
                    }
                    else
                        new Confirm("Printer not ready!", "Information", MessageBoxButtons.OK);
                }
            }
        }

        private void btnDeco_Click(object sender, EventArgs e)
        {
            Decomission dc = new Decomission(lblAdmin.Text,ms.linenumber);
            dc.ShowDialog();
        }

        private void stopPrinting()
        {
            ms.start = false;
            //ms.refresh();
            ms.enable();
            ms.delayTimer.Stop();
        }

        private void label14_Click(object sender, EventArgs e)
        {

        }

        private void dgvSend_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            lblQtySend.Text = ""+dgvSend.Rows.Count;
        }

        private void dgvReceive_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {

            //lblQuantityReceive.Text = "" + dgvReceive.Rows.Count;
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            Sample smp = new Sample(lblAdmin.Text);
            smp.ShowDialog();
        }

        private void label10_Click(object sender, EventArgs e)
        {
            ms.test();
        }

        public void refresh()
        {
            sqlite = new Mod_Sqlite();
            db = new Mod_Dbaccess();
            ms.sqlite = new Mod_Sqlite();
            ms.db = new Mod_Dbaccess();
        }

        private void label7_Click(object sender, EventArgs e)
        {
            ms.test();
        }

        private void button1_Click_2(object sender, EventArgs e)
        {
            //txtLog.Text = "";
            //ms.SetDataComboBoxBatchNumber();
        }

        private void pictureBox1_DoubleClick(object sender, EventArgs e)
        {
            close2("");
            dbss.Show();
            this.Hide();
        }

        private void txtBatchNumber_Click(object sender, EventArgs e)
        {
            lbn = new ListBatchNumber(null, linenumber[1], parameter, "");
            lbn.ShowDialog();
            txtBatchNumber.Text = lbn.batch;
            int[] cnt = ms.getCount(lbn.batch);
            //lblQuantityGood.Text = cnt[0].ToString();
            //lblQuantityReject.Text = cnt[1].ToString();
            lblQuantityReceiveAll.Text = cnt[0].ToString();
            btnAudit.Focus();
            if (txtBatchNumber.Text.Length > 0)
            {
                btnAudit.Enabled = true;
                btnStart.Enabled = true;
            }
            else
            {
                btnAudit.Enabled = false;
                btnStart.Enabled = false;
            }
        }

        public bool cekDB()
        {
            if (db.Connect())
            {
                lblDb.Text = "Online";
                onDb.Visible = true;
                offDb.Visible = false;
                ms.StopServer();
                ms.StartServer2();
                db.closeConnection();
                return true;
            }
            else
            {
                lblDb.Text = "Offline";
                onDb.Visible = false;
                offDb.Visible = true;
                ms.StopServer();
                return false;
            }
        }

        private void pictureBox1_MouseEnter(object sender, EventArgs e)
        {
            this.pictureBox1.BackColor = ColorTranslator.FromHtml("#00505E");
        }

        private void pictureBox1_MouseLeave(object sender, EventArgs e)
        {
            this.pictureBox1.BackColor = ColorTranslator.FromHtml("#002F36");
        }

        private void btnIndicator_Click(object sender, EventArgs e)
        {
            cekCamera();
            cekDevice();
            cekDB();
        }

        private void btnLog_Click(object sender, EventArgs e)
        {
            Log lg = ms.lg;
            lg.Show();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            lblTime.Text = DateTime.Now.ToLongTimeString();
            timer1.Start();
        }

        private void Infeed_Load(object sender, EventArgs e)
        {
            timer1.Start();
            lblTime.Text = DateTime.Now.ToLongTimeString();
            lblDate.Text = DateTime.Now.ToShortDateString();
        }

    }
}
