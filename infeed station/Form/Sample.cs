﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Infeed_Station
{
    public partial class Sample : Form
    {
        Mod_Dbaccess db;
        public Sample(string adminid)
        {
            InitializeComponent();
            db = new Mod_Dbaccess();
            db.adminid = adminid;
            db.from = "Infeed Station";

        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Enter)
            {
                sample();
            }
        }

        private bool cekInfeed(string databarcode, int status)
        {
            List<string> field = new List<string>();
            field.Add("*");
            string where = "infeedInnerBoxId like '%" + databarcode + "%' and isreject='" + status + "'";
            db.selectList(field, "innerbox", where);
            if (db.num_rows > 0)
                return true;
            return false;
        }


        private void sample()
        {
            if (db.validasi(txtBlisterid.Text, 1))
            {
                if (cekInfeed(txtBlisterid.Text, 0))
                {
                    if (db.sampleData(txtBlisterid.Text))
                    {
                        txtLog.AppendText(txtBlisterid.Text + " set sample success" + Environment.NewLine);
                    }
                    else
                    {
                        txtLog.AppendText(txtBlisterid.Text + " set sample failed" + Environment.NewLine);
                    }
                }
                else
                {
                    Confirm cf = new Confirm("Data not found!", "Information", MessageBoxButtons.OK);
                }
            }
            else
            {
                Confirm cf = new Confirm("Infeed ID is empty", "Information", MessageBoxButtons.OK);
            }

            txtBlisterid.Text = "";
            txtBlisterid.Focus();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            sample();
        }

        private void Sample_Load(object sender, EventArgs e)
        {
            txtBlisterid.Text = "";
            txtBlisterid.Focus();
        }
    }
}
