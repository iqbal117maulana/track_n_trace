﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Infeed_Station
{
    public partial class Decomission : Form
    {
        Mod_Dbaccess db;
        Model.Vaccine vaccine;
        Model.Blister blister;
        Model.innerBox innerbox;
        string linenumber;

        public Decomission(string adminid, string line)
        {
            InitializeComponent();
            db = new Mod_Dbaccess();
            db.adminid = adminid;
            db.from = "Infeed Station";
            linenumber = line;
            SetBatchNumber();
            vaccine = new Model.Vaccine();
            blister = new Model.Blister();
            innerbox = new Model.innerBox();

        }

        private void SetBatchNumber()
        {
            List<string> field = new List<string>();
            field.Add("batchNumber,productModelId");
            string where = "status = '1' and linepackagingid = '" + linenumber + "'";
            List<string[]> datacombo = db.selectList(field, "[packaging_order]", where);
            List<string> da = new List<string>();
            for (int i = 0; i < datacombo.Count; i++)
            {
                da.Add(datacombo[i][0]);
            }
            cmbBatch.DataSource = da;
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Enter)
            {
                decomision();
            }
        }
        
        private void decomision()
        {
            vaccine = new Model.Vaccine();
            blister = new Model.Blister();
            innerbox = new Model.innerBox();
            txtProduct.Text = Util.Common.REMOVE_UNUSED_CHAR(txtProduct.Text);
            if (!Util.Common.CHECK_EMPTY(txtProduct))
            {
                // cek blister
                blister.BLISTERPACKID = txtProduct.Text;
                blister.BATCHNUMBER = cmbBatch.Text;

                innerbox.INERBOXID = txtProduct.Text;
                innerbox.GS1INNERBOXID = txtProduct.Text;
                innerbox.BATCHNUMBER = cmbBatch.Text;

                vaccine.BATCHNUMBER = cmbBatch.Text;
                vaccine.CAPID = txtProduct.Text;
                vaccine.GS1VIALID = txtProduct.Text;
                if (innerbox.CHECK_INNERBOX())
                {
                    decommission_INNERBOX();
                }
                else if (blister.CHECK_BLISTER())
                {

                    decommission_BLISTER();
                }
                else
                {
                    log("Product " + txtProduct.Text + " Not Found" + Environment.NewLine);
                }
            }

            txtProduct.Text = "";
            txtProduct.Focus();
        }

        private void decommission_BLISTER()
        {
            vaccine = new Model.Vaccine();
            blister = new Model.Blister();
            innerbox = new Model.innerBox();
            blister.BLISTERPACKID = txtProduct.Text;
            if (blister.CHECK_BLISTER())
            {
                blister.BATCHNUMBER = cmbBatch.Text;
                if (blister.CHECK_BLISTER())
                {
                    blister.ISREJECT = "0";
                    if (blister.CHECK_BLISTER())
                    {
                        blister.INNERBOXID = "kosong";
                        if (blister.CHECK_BLISTER())
                        {
                            if (blister.DECOMMISSION())
                            {
                                vaccine.BLISTERID = txtProduct.Text;
                                vaccine.BATCHNUMBER = cmbBatch.Text;
                                if (vaccine.UPDATE_EMPTY_BLISTER())
                                {
                                    db.eventname = "Decomission =" + txtProduct.Text + " success";
                                    db.to = txtProduct.Text;
                                    db.systemlog();
                                    log(txtProduct.Text + " decommission success" + Environment.NewLine);
                                }
                                else
                                {
                                    db.eventname = "Decommission failed," + txtProduct.Text + " Something wrong with Database[Vaccine]";
                                    db.to = txtProduct.Text;
                                    db.systemlog();
                                    log("Decommission failed," + txtProduct.Text + " Something wrong with Database" + Environment.NewLine);

                                }

                            }
                            else
                            {
                                db.eventname = "Decommission failed," + txtProduct.Text + " Something wrong with Database[Blister]";
                                db.to = txtProduct.Text;
                                db.systemlog();
                                log("Decommission failed," + txtProduct.Text + " Something wrong with Database" + Environment.NewLine);
                            }
                        }
                        else
                        {
                            db.eventname = "Decommission failed," + txtProduct.Text + " is register with innerbox";
                            db.to = txtProduct.Text;
                            db.systemlog();
                            log("Decommission failed," + txtProduct.Text + " is register with innerbox" + Environment.NewLine);                          
                        }
                    }
                    else
                    {

                        db.eventname = "Decommission failed," + txtProduct.Text + " is not a good product";
                        db.to = txtProduct.Text;
                        db.systemlog();
                        log("Decommission failed," + txtProduct.Text + " is not a good product " + Environment.NewLine);
                    }
                }
                else
                {
                    db.eventname = "Decommission failed," + txtProduct.Text + " Wrong BatchNumber";
                    db.to = txtProduct.Text;
                    db.systemlog();
                    log("Decommission failed," + txtProduct.Text + " Wrong BatchNumber" + Environment.NewLine);

                }
            }
            else
            {
                db.eventname = "Decomission =" + txtProduct.Text + " Not Found";
                db.to = txtProduct.Text;
                db.systemlog();
                log("Decommission failed," + txtProduct.Text + " Not Found" + Environment.NewLine);
            }
        }

        private void decommission_INNERBOX()
        {
            vaccine = new Model.Vaccine();
            blister = new Model.Blister();
            innerbox = new Model.innerBox();

            innerbox.INERBOXID = txtProduct.Text;
            innerbox.GS1INNERBOXID = txtProduct.Text;
            if (innerbox.CHECK_INNERBOX())
            {
                innerbox.BATCHNUMBER = cmbBatch.Text;
                if (innerbox.CHECK_INNERBOX())
                {
                    innerbox.ISREJECT = "0";
                    if (innerbox.CHECK_INNERBOX())
                    {
                        innerbox.BASKETID = "kosong";
                        if (innerbox.CHECK_INNERBOX())
                        {
                            blister.INNERBOXID = txtProduct.Text;
                            blister.BATCHNUMBER = cmbBatch.Text;
                            if (blister.UPDATE_EMPTY_INNERBOX())
                            {
                                if (innerbox.DECOMMISSION())
                                {
                                    vaccine.INNERBOXID = txtProduct.Text;
                                    vaccine.INNERBOXGSONEID = txtProduct.Text;
                                    vaccine.BATCHNUMBER = cmbBatch.Text;
                                    if (vaccine.UPDATE_EMPTY_INNERBOX())
                                    {
                                        db.eventname = "Decomission =" + txtProduct.Text + " success";
                                        db.to = txtProduct.Text;
                                        db.systemlog();
                                        log(txtProduct.Text + " decommission success" + Environment.NewLine);
                                    }
                                    else
                                    {
                                        db.eventname = "Decommission failed," + txtProduct.Text + " Something wrong with Database[Vaccine]";
                                        db.to = txtProduct.Text;
                                        db.systemlog();
                                        log("Decommission failed," + txtProduct.Text + " Something wrong with Database" + Environment.NewLine);

                                    }
                                }

                                else
                                {
                                    db.eventname = "Decommission failed," + txtProduct.Text + " Something wrong with Database[Blister]";
                                    db.to = txtProduct.Text;
                                    db.systemlog();
                                    log("Decommission failed," + txtProduct.Text + " Something wrong with Database" + Environment.NewLine);
                                }

                            }
                            else
                            {
                                db.eventname = "Decommission failed," + txtProduct.Text + " Something wrong with Database[InnerBox]";
                                db.to = txtProduct.Text;
                                db.systemlog();
                                log("Decommission failed," + txtProduct.Text + " Something wrong with Database" + Environment.NewLine);

                            }
                        }
                        else
                        {
                            db.eventname = "Decommission failed," + txtProduct.Text + " is register with basket";
                            db.to = txtProduct.Text;
                            db.systemlog();
                            log("Decommission failed," + txtProduct.Text + " is register with basket" + Environment.NewLine);
                        }
                    }
                    else
                    {

                        db.eventname = "Decommission failed," + txtProduct.Text + " is not a good product";
                        db.to = txtProduct.Text;
                        db.systemlog();
                        log("Decommission failed," + txtProduct.Text + " is not a good product " + Environment.NewLine);
                    }
                }
                else
                {
                    db.eventname = "Decommission failed," + txtProduct.Text + " Wrong BatchNumber";
                    db.to = txtProduct.Text;
                    db.systemlog();
                    log("Decommission failed," + txtProduct.Text + " Wrong BatchNumber" + Environment.NewLine);

                }
            }
            else
            {
                db.eventname = "Decomission =" + txtProduct.Text + " Not Found";
                db.to = txtProduct.Text;
                db.systemlog();
                log("Decommission failed," + txtProduct.Text + " Not Found" + Environment.NewLine);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            decomision();
        }

        private void Decomission_Load(object sender, EventArgs e)
        {
            txtProduct.Text = "";
            txtProduct.Focus();
        }

        private void log(string line)
        {
            line = Util.Common.SetWithDate(line);
            txtLog.AppendText(line+"\n");
        }
    }
}
