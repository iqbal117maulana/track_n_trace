﻿namespace Infeed_Station
{
    partial class Infeed
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Infeed));
            this.lblPrinter = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.lblCamera2 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lblCamera = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dgvSend = new System.Windows.Forms.DataGridView();
            this.lblQtySend = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lblQuantityReceiveAll = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblQuantityReceive = new System.Windows.Forms.Label();
            this.dgvReceive = new System.Windows.Forms.DataGridView();
            this.label18 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.lblAdmin = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.lblCamera3 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.labelProduct = new System.Windows.Forms.Label();
            this.labelBatch = new System.Windows.Forms.Label();
            this.labelLine = new System.Windows.Forms.Label();
            this.lblRole = new System.Windows.Forms.Label();
            this.lblUserId = new System.Windows.Forms.Label();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.lblDate = new System.Windows.Forms.Label();
            this.lblTime = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.txtBatchNumber = new System.Windows.Forms.TextBox();
            this.lblDb = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.offDb = new System.Windows.Forms.PictureBox();
            this.onDb = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.btnIndicator = new System.Windows.Forms.Button();
            this.btnLog = new System.Windows.Forms.Button();
            this.offPrinter = new System.Windows.Forms.PictureBox();
            this.offCamera3 = new System.Windows.Forms.PictureBox();
            this.offCamera2 = new System.Windows.Forms.PictureBox();
            this.offCamera1 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.btnSample = new System.Windows.Forms.Button();
            this.btnAudit = new System.Windows.Forms.Button();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBoxCamera = new System.Windows.Forms.PictureBox();
            this.btnDeco = new System.Windows.Forms.Button();
            this.btnConfig = new System.Windows.Forms.Button();
            this.btnLogout = new System.Windows.Forms.Button();
            this.btnStart = new System.Windows.Forms.Button();
            this.onCamera1 = new System.Windows.Forms.PictureBox();
            this.onCamera2 = new System.Windows.Forms.PictureBox();
            this.onCamera3 = new System.Windows.Forms.PictureBox();
            this.onPrinter = new System.Windows.Forms.PictureBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSend)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvReceive)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.offDb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.onDb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.offPrinter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.offCamera3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.offCamera2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.offCamera1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCamera)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.onCamera1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.onCamera2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.onCamera3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.onPrinter)).BeginInit();
            this.SuspendLayout();
            // 
            // lblPrinter
            // 
            this.lblPrinter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblPrinter.AutoSize = true;
            this.lblPrinter.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrinter.Location = new System.Drawing.Point(945, 611);
            this.lblPrinter.Name = "lblPrinter";
            this.lblPrinter.Size = new System.Drawing.Size(55, 20);
            this.lblPrinter.TabIndex = 81;
            this.lblPrinter.Text = "Offline";
            // 
            // label16
            // 
            this.label16.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(888, 635);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(59, 20);
            this.label16.TabIndex = 80;
            this.label16.Text = "Printer ";
            // 
            // lblCamera2
            // 
            this.lblCamera2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCamera2.AutoSize = true;
            this.lblCamera2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCamera2.Location = new System.Drawing.Point(946, 443);
            this.lblCamera2.Name = "lblCamera2";
            this.lblCamera2.Size = new System.Drawing.Size(55, 20);
            this.lblCamera2.TabIndex = 73;
            this.lblCamera2.Text = "Offline";
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(885, 469);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(82, 20);
            this.label8.TabIndex = 72;
            this.label8.Text = "Camera 2 ";
            // 
            // lblCamera
            // 
            this.lblCamera.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCamera.AutoSize = true;
            this.lblCamera.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCamera.Location = new System.Drawing.Point(946, 363);
            this.lblCamera.Name = "lblCamera";
            this.lblCamera.Size = new System.Drawing.Size(55, 20);
            this.lblCamera.TabIndex = 71;
            this.lblCamera.Text = "Offline";
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(885, 388);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(82, 20);
            this.label5.TabIndex = 70;
            this.label5.Text = "Camera 1 ";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(14, 111);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(119, 20);
            this.label7.TabIndex = 67;
            this.label7.Text = "Batch Number :";
            this.label7.Click += new System.EventHandler(this.label7_Click);
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(594, 59);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(110, 20);
            this.label3.TabIndex = 59;
            this.label3.Text = "Product Name";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(594, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(117, 20);
            this.label2.TabIndex = 58;
            this.label2.Text = "Line Packaging";
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox1.Controls.Add(this.dgvSend);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(7, 151);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(434, 645);
            this.groupBox1.TabIndex = 63;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Ready";
            // 
            // dgvSend
            // 
            this.dgvSend.AllowUserToAddRows = false;
            this.dgvSend.AllowUserToDeleteRows = false;
            this.dgvSend.AllowUserToResizeColumns = false;
            this.dgvSend.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dgvSend.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvSend.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvSend.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvSend.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSend.Location = new System.Drawing.Point(6, 19);
            this.dgvSend.Name = "dgvSend";
            this.dgvSend.RowHeadersVisible = false;
            this.dgvSend.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvSend.Size = new System.Drawing.Size(422, 615);
            this.dgvSend.TabIndex = 6;
            this.dgvSend.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dgvSend_RowsAdded);
            // 
            // lblQtySend
            // 
            this.lblQtySend.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblQtySend.AutoSize = true;
            this.lblQtySend.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQtySend.Location = new System.Drawing.Point(398, 137);
            this.lblQtySend.Name = "lblQtySend";
            this.lblQtySend.Size = new System.Drawing.Size(18, 20);
            this.lblQtySend.TabIndex = 87;
            this.lblQtySend.Text = "0";
            this.lblQtySend.Visible = false;
            // 
            // label12
            // 
            this.label12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(303, 137);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(80, 20);
            this.label12.TabIndex = 86;
            this.label12.Text = "Quantity : ";
            this.label12.Visible = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.lblQuantityReceiveAll);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.lblQuantityReceive);
            this.groupBox2.Controls.Add(this.dgvReceive);
            this.groupBox2.Controls.Add(this.label18);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(447, 151);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(411, 645);
            this.groupBox2.TabIndex = 64;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Pass";
            // 
            // lblQuantityReceiveAll
            // 
            this.lblQuantityReceiveAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblQuantityReceiveAll.AutoSize = true;
            this.lblQuantityReceiveAll.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQuantityReceiveAll.Location = new System.Drawing.Point(118, 614);
            this.lblQuantityReceiveAll.Name = "lblQuantityReceiveAll";
            this.lblQuantityReceiveAll.Size = new System.Drawing.Size(18, 20);
            this.lblQuantityReceiveAll.TabIndex = 91;
            this.lblQuantityReceiveAll.Text = "0";
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(0, 614);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(101, 20);
            this.label6.TabIndex = 90;
            this.label6.Text = "All Quantity : ";
            // 
            // lblQuantityReceive
            // 
            this.lblQuantityReceive.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblQuantityReceive.AutoSize = true;
            this.lblQuantityReceive.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQuantityReceive.Location = new System.Drawing.Point(372, 614);
            this.lblQuantityReceive.Name = "lblQuantityReceive";
            this.lblQuantityReceive.Size = new System.Drawing.Size(18, 20);
            this.lblQuantityReceive.TabIndex = 89;
            this.lblQuantityReceive.Text = "0";
            // 
            // dgvReceive
            // 
            this.dgvReceive.AllowUserToAddRows = false;
            this.dgvReceive.AllowUserToDeleteRows = false;
            this.dgvReceive.AllowUserToResizeColumns = false;
            this.dgvReceive.AllowUserToResizeRows = false;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dgvReceive.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvReceive.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvReceive.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvReceive.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvReceive.Location = new System.Drawing.Point(6, 19);
            this.dgvReceive.Name = "dgvReceive";
            this.dgvReceive.ReadOnly = true;
            this.dgvReceive.RowHeadersVisible = false;
            this.dgvReceive.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvReceive.Size = new System.Drawing.Size(399, 584);
            this.dgvReceive.TabIndex = 7;
            this.dgvReceive.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dgvReceive_RowsAdded);
            // 
            // label18
            // 
            this.label18.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(237, 614);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(137, 20);
            this.label18.TabIndex = 88;
            this.label18.Text = "Current Quantity : ";
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(594, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(111, 20);
            this.label1.TabIndex = 57;
            this.label1.Text = "Batch Number";
            // 
            // label9
            // 
            this.label9.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(320, 9);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(231, 33);
            this.label9.TabIndex = 84;
            this.label9.Text = "Infeed Cartoner";
            this.label9.Click += new System.EventHandler(this.label9_Click);
            // 
            // label10
            // 
            this.label10.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(373, 42);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(112, 33);
            this.label10.TabIndex = 85;
            this.label10.Text = "Station";
            this.label10.Click += new System.EventHandler(this.label10_Click);
            // 
            // lblAdmin
            // 
            this.lblAdmin.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblAdmin.AutoSize = true;
            this.lblAdmin.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAdmin.Location = new System.Drawing.Point(799, 9);
            this.lblAdmin.Name = "lblAdmin";
            this.lblAdmin.Size = new System.Drawing.Size(59, 20);
            this.lblAdmin.TabIndex = 87;
            this.lblAdmin.Text = "Admin";
            this.lblAdmin.Visible = false;
            // 
            // label19
            // 
            this.label19.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.White;
            this.label19.Location = new System.Drawing.Point(711, 34);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(13, 20);
            this.label19.TabIndex = 119;
            this.label19.Text = ":";
            // 
            // label20
            // 
            this.label20.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.White;
            this.label20.Location = new System.Drawing.Point(711, 9);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(13, 20);
            this.label20.TabIndex = 120;
            this.label20.Text = ":";
            // 
            // label21
            // 
            this.label21.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.White;
            this.label21.Location = new System.Drawing.Point(711, 59);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(13, 20);
            this.label21.TabIndex = 121;
            this.label21.Text = ":";
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(657, 94);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(150, 40);
            this.button1.TabIndex = 128;
            this.button1.Text = "Refresh";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click_2);
            // 
            // lblCamera3
            // 
            this.lblCamera3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCamera3.AutoSize = true;
            this.lblCamera3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCamera3.Location = new System.Drawing.Point(946, 525);
            this.lblCamera3.Name = "lblCamera3";
            this.lblCamera3.Size = new System.Drawing.Size(55, 20);
            this.lblCamera3.TabIndex = 130;
            this.lblCamera3.Text = "Offline";
            // 
            // label14
            // 
            this.label14.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(885, 551);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(78, 20);
            this.label14.TabIndex = 129;
            this.label14.Text = "Camera 3";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(148)))), ((int)(((byte)(172)))));
            this.panel1.Controls.Add(this.labelProduct);
            this.panel1.Controls.Add(this.labelBatch);
            this.panel1.Controls.Add(this.labelLine);
            this.panel1.Controls.Add(this.lblRole);
            this.panel1.Controls.Add(this.lblUserId);
            this.panel1.Controls.Add(this.pictureBox5);
            this.panel1.Controls.Add(this.lblDate);
            this.panel1.Controls.Add(this.lblTime);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.label21);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.label20);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.label19);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.lblAdmin);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1024, 86);
            this.panel1.TabIndex = 132;
            // 
            // labelProduct
            // 
            this.labelProduct.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelProduct.AutoSize = true;
            this.labelProduct.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelProduct.ForeColor = System.Drawing.Color.White;
            this.labelProduct.Location = new System.Drawing.Point(720, 61);
            this.labelProduct.Name = "labelProduct";
            this.labelProduct.Size = new System.Drawing.Size(110, 20);
            this.labelProduct.TabIndex = 150;
            this.labelProduct.Text = "Product Name";
            // 
            // labelBatch
            // 
            this.labelBatch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelBatch.AutoSize = true;
            this.labelBatch.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelBatch.ForeColor = System.Drawing.Color.White;
            this.labelBatch.Location = new System.Drawing.Point(720, 36);
            this.labelBatch.Name = "labelBatch";
            this.labelBatch.Size = new System.Drawing.Size(111, 20);
            this.labelBatch.TabIndex = 148;
            this.labelBatch.Text = "Batch Number";
            // 
            // labelLine
            // 
            this.labelLine.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelLine.AutoSize = true;
            this.labelLine.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelLine.ForeColor = System.Drawing.Color.White;
            this.labelLine.Location = new System.Drawing.Point(720, 10);
            this.labelLine.Name = "labelLine";
            this.labelLine.Size = new System.Drawing.Size(39, 20);
            this.labelLine.TabIndex = 149;
            this.labelLine.Text = "Line";
            // 
            // lblRole
            // 
            this.lblRole.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblRole.AutoSize = true;
            this.lblRole.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRole.ForeColor = System.Drawing.Color.White;
            this.lblRole.Location = new System.Drawing.Point(925, 44);
            this.lblRole.Name = "lblRole";
            this.lblRole.Size = new System.Drawing.Size(52, 20);
            this.lblRole.TabIndex = 147;
            this.lblRole.Text = "admin";
            // 
            // lblUserId
            // 
            this.lblUserId.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblUserId.AutoSize = true;
            this.lblUserId.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUserId.ForeColor = System.Drawing.Color.White;
            this.lblUserId.Location = new System.Drawing.Point(924, 24);
            this.lblUserId.Name = "lblUserId";
            this.lblUserId.Size = new System.Drawing.Size(57, 20);
            this.lblUserId.TabIndex = 146;
            this.lblUserId.Text = "admin";
            // 
            // pictureBox5
            // 
            this.pictureBox5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox5.Image = global::Infeed_Station.Properties.Resources.user;
            this.pictureBox5.Location = new System.Drawing.Point(873, 20);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(45, 45);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox5.TabIndex = 145;
            this.pictureBox5.TabStop = false;
            // 
            // lblDate
            // 
            this.lblDate.AutoSize = true;
            this.lblDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDate.ForeColor = System.Drawing.Color.White;
            this.lblDate.Location = new System.Drawing.Point(216, 38);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(33, 15);
            this.lblDate.TabIndex = 119;
            this.lblDate.Text = "Date";
            // 
            // lblTime
            // 
            this.lblTime.AutoSize = true;
            this.lblTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTime.ForeColor = System.Drawing.Color.White;
            this.lblTime.Location = new System.Drawing.Point(216, 20);
            this.lblTime.Name = "lblTime";
            this.lblTime.Size = new System.Drawing.Size(39, 15);
            this.lblTime.TabIndex = 118;
            this.lblTime.Text = "Time";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(47)))), ((int)(((byte)(54)))));
            this.panel2.Controls.Add(this.pictureBox1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(210, 86);
            this.panel2.TabIndex = 86;
            // 
            // pictureBox1
            // 
            this.pictureBox1.ImageLocation = "logo_biofarma.png";
            this.pictureBox1.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.InitialImage")));
            this.pictureBox1.Location = new System.Drawing.Point(29, 4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(150, 75);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 65;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.DoubleClick += new System.EventHandler(this.pictureBox1_DoubleClick);
            this.pictureBox1.MouseLeave += new System.EventHandler(this.pictureBox1_MouseLeave);
            this.pictureBox1.MouseEnter += new System.EventHandler(this.pictureBox1_MouseEnter);
            // 
            // txtBatchNumber
            // 
            this.txtBatchNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBatchNumber.Location = new System.Drawing.Point(131, 108);
            this.txtBatchNumber.Name = "txtBatchNumber";
            this.txtBatchNumber.ReadOnly = true;
            this.txtBatchNumber.Size = new System.Drawing.Size(192, 26);
            this.txtBatchNumber.TabIndex = 151;
            this.txtBatchNumber.Click += new System.EventHandler(this.txtBatchNumber_Click);
            // 
            // lblDb
            // 
            this.lblDb.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblDb.AutoSize = true;
            this.lblDb.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDb.Location = new System.Drawing.Point(953, 696);
            this.lblDb.Name = "lblDb";
            this.lblDb.Size = new System.Drawing.Size(50, 18);
            this.lblDb.TabIndex = 154;
            this.lblDb.Text = "Offline";
            // 
            // label15
            // 
            this.label15.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(892, 719);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(79, 20);
            this.label15.TabIndex = 153;
            this.label15.Text = "Database";
            // 
            // offDb
            // 
            this.offDb.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.offDb.Image = global::Infeed_Station.Properties.Resources.off1;
            this.offDb.Location = new System.Drawing.Point(956, 679);
            this.offDb.Name = "offDb";
            this.offDb.Size = new System.Drawing.Size(14, 14);
            this.offDb.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.offDb.TabIndex = 156;
            this.offDb.TabStop = false;
            // 
            // onDb
            // 
            this.onDb.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.onDb.Image = global::Infeed_Station.Properties.Resources.on1;
            this.onDb.Location = new System.Drawing.Point(956, 679);
            this.onDb.Name = "onDb";
            this.onDb.Size = new System.Drawing.Size(14, 14);
            this.onDb.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.onDb.TabIndex = 155;
            this.onDb.TabStop = false;
            this.onDb.Visible = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox6.Image = global::Infeed_Station.Properties.Resources.dtbs;
            this.pictureBox6.Location = new System.Drawing.Point(892, 666);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(55, 50);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox6.TabIndex = 152;
            this.pictureBox6.TabStop = false;
            // 
            // btnIndicator
            // 
            this.btnIndicator.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnIndicator.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnIndicator.Image = global::Infeed_Station.Properties.Resources.indicator;
            this.btnIndicator.Location = new System.Drawing.Point(864, 756);
            this.btnIndicator.Name = "btnIndicator";
            this.btnIndicator.Size = new System.Drawing.Size(150, 40);
            this.btnIndicator.TabIndex = 151;
            this.btnIndicator.Text = "Indicator";
            this.btnIndicator.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnIndicator.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnIndicator.UseVisualStyleBackColor = true;
            this.btnIndicator.Click += new System.EventHandler(this.btnIndicator_Click);
            // 
            // btnLog
            // 
            this.btnLog.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLog.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLog.Image = global::Infeed_Station.Properties.Resources.log;
            this.btnLog.Location = new System.Drawing.Point(864, 231);
            this.btnLog.Name = "btnLog";
            this.btnLog.Size = new System.Drawing.Size(150, 40);
            this.btnLog.TabIndex = 146;
            this.btnLog.Text = "Log";
            this.btnLog.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnLog.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnLog.UseVisualStyleBackColor = true;
            this.btnLog.Click += new System.EventHandler(this.btnLog_Click);
            // 
            // offPrinter
            // 
            this.offPrinter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.offPrinter.Image = global::Infeed_Station.Properties.Resources.off1;
            this.offPrinter.Location = new System.Drawing.Point(953, 592);
            this.offPrinter.Name = "offPrinter";
            this.offPrinter.Size = new System.Drawing.Size(14, 14);
            this.offPrinter.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.offPrinter.TabIndex = 145;
            this.offPrinter.TabStop = false;
            // 
            // offCamera3
            // 
            this.offCamera3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.offCamera3.Image = global::Infeed_Station.Properties.Resources.off1;
            this.offCamera3.Location = new System.Drawing.Point(953, 503);
            this.offCamera3.Name = "offCamera3";
            this.offCamera3.Size = new System.Drawing.Size(14, 14);
            this.offCamera3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.offCamera3.TabIndex = 144;
            this.offCamera3.TabStop = false;
            // 
            // offCamera2
            // 
            this.offCamera2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.offCamera2.Image = global::Infeed_Station.Properties.Resources.off1;
            this.offCamera2.Location = new System.Drawing.Point(953, 423);
            this.offCamera2.Name = "offCamera2";
            this.offCamera2.Size = new System.Drawing.Size(14, 14);
            this.offCamera2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.offCamera2.TabIndex = 143;
            this.offCamera2.TabStop = false;
            // 
            // offCamera1
            // 
            this.offCamera1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.offCamera1.Image = global::Infeed_Station.Properties.Resources.off1;
            this.offCamera1.Location = new System.Drawing.Point(953, 344);
            this.offCamera1.Name = "offCamera1";
            this.offCamera1.Size = new System.Drawing.Size(14, 14);
            this.offCamera1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.offCamera1.TabIndex = 142;
            this.offCamera1.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox3.ImageLocation = "genie-nano-camera.png";
            this.pictureBox3.Location = new System.Drawing.Point(889, 498);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(55, 50);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 131;
            this.pictureBox3.TabStop = false;
            // 
            // btnSample
            // 
            this.btnSample.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSample.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSample.Image = global::Infeed_Station.Properties.Resources.sample;
            this.btnSample.Location = new System.Drawing.Point(864, 94);
            this.btnSample.Name = "btnSample";
            this.btnSample.Size = new System.Drawing.Size(150, 40);
            this.btnSample.TabIndex = 126;
            this.btnSample.Text = "Sample";
            this.btnSample.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSample.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSample.UseVisualStyleBackColor = true;
            this.btnSample.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // btnAudit
            // 
            this.btnAudit.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAudit.Image = global::Infeed_Station.Properties.Resources.pre_check;
            this.btnAudit.Location = new System.Drawing.Point(343, 94);
            this.btnAudit.Name = "btnAudit";
            this.btnAudit.Size = new System.Drawing.Size(150, 40);
            this.btnAudit.TabIndex = 122;
            this.btnAudit.Text = "Pre-Check";
            this.btnAudit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAudit.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnAudit.UseVisualStyleBackColor = true;
            this.btnAudit.Click += new System.EventHandler(this.button5_Click);
            // 
            // pictureBox4
            // 
            this.pictureBox4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox4.ImageLocation = "libringer.jpg";
            this.pictureBox4.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox4.InitialImage")));
            this.pictureBox4.Location = new System.Drawing.Point(889, 582);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(55, 50);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox4.TabIndex = 118;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox2.ImageLocation = "genie-nano-camera.png";
            this.pictureBox2.Location = new System.Drawing.Point(889, 335);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(55, 50);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 116;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBoxCamera
            // 
            this.pictureBoxCamera.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBoxCamera.ImageLocation = "genie-nano-camera.png";
            this.pictureBoxCamera.Location = new System.Drawing.Point(889, 416);
            this.pictureBoxCamera.Name = "pictureBoxCamera";
            this.pictureBoxCamera.Size = new System.Drawing.Size(55, 50);
            this.pictureBoxCamera.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxCamera.TabIndex = 115;
            this.pictureBoxCamera.TabStop = false;
            // 
            // btnDeco
            // 
            this.btnDeco.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDeco.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeco.Image = global::Infeed_Station.Properties.Resources.deco;
            this.btnDeco.Location = new System.Drawing.Point(864, 139);
            this.btnDeco.Name = "btnDeco";
            this.btnDeco.Size = new System.Drawing.Size(150, 40);
            this.btnDeco.TabIndex = 86;
            this.btnDeco.Text = "Decommission";
            this.btnDeco.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDeco.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnDeco.UseVisualStyleBackColor = true;
            this.btnDeco.Click += new System.EventHandler(this.btnDeco_Click);
            // 
            // btnConfig
            // 
            this.btnConfig.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnConfig.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConfig.Image = global::Infeed_Station.Properties.Resources.config;
            this.btnConfig.Location = new System.Drawing.Point(864, 185);
            this.btnConfig.Name = "btnConfig";
            this.btnConfig.Size = new System.Drawing.Size(150, 40);
            this.btnConfig.TabIndex = 83;
            this.btnConfig.Text = "Config";
            this.btnConfig.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnConfig.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnConfig.UseVisualStyleBackColor = true;
            this.btnConfig.Click += new System.EventHandler(this.button3_Click);
            // 
            // btnLogout
            // 
            this.btnLogout.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLogout.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLogout.Image = global::Infeed_Station.Properties.Resources.logout;
            this.btnLogout.Location = new System.Drawing.Point(864, 277);
            this.btnLogout.Name = "btnLogout";
            this.btnLogout.Size = new System.Drawing.Size(150, 40);
            this.btnLogout.TabIndex = 82;
            this.btnLogout.Text = "Logout";
            this.btnLogout.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnLogout.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnLogout.UseVisualStyleBackColor = true;
            this.btnLogout.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnStart
            // 
            this.btnStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStart.Image = global::Infeed_Station.Properties.Resources.start;
            this.btnStart.Location = new System.Drawing.Point(501, 94);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(150, 40);
            this.btnStart.TabIndex = 68;
            this.btnStart.Text = "Start";
            this.btnStart.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnStart.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.button2_Click);
            // 
            // onCamera1
            // 
            this.onCamera1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.onCamera1.Image = global::Infeed_Station.Properties.Resources.on1;
            this.onCamera1.Location = new System.Drawing.Point(952, 344);
            this.onCamera1.Name = "onCamera1";
            this.onCamera1.Size = new System.Drawing.Size(14, 14);
            this.onCamera1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.onCamera1.TabIndex = 147;
            this.onCamera1.TabStop = false;
            // 
            // onCamera2
            // 
            this.onCamera2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.onCamera2.Image = global::Infeed_Station.Properties.Resources.on1;
            this.onCamera2.Location = new System.Drawing.Point(952, 423);
            this.onCamera2.Name = "onCamera2";
            this.onCamera2.Size = new System.Drawing.Size(14, 14);
            this.onCamera2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.onCamera2.TabIndex = 148;
            this.onCamera2.TabStop = false;
            // 
            // onCamera3
            // 
            this.onCamera3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.onCamera3.Image = global::Infeed_Station.Properties.Resources.on1;
            this.onCamera3.Location = new System.Drawing.Point(952, 503);
            this.onCamera3.Name = "onCamera3";
            this.onCamera3.Size = new System.Drawing.Size(14, 14);
            this.onCamera3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.onCamera3.TabIndex = 149;
            this.onCamera3.TabStop = false;
            // 
            // onPrinter
            // 
            this.onPrinter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.onPrinter.Image = global::Infeed_Station.Properties.Resources.on1;
            this.onPrinter.Location = new System.Drawing.Point(952, 592);
            this.onPrinter.Name = "onPrinter";
            this.onPrinter.Size = new System.Drawing.Size(14, 14);
            this.onPrinter.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.onPrinter.TabIndex = 150;
            this.onPrinter.TabStop = false;
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // Infeed
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1024, 801);
            this.Controls.Add(this.lblQtySend);
            this.Controls.Add(this.offDb);
            this.Controls.Add(this.onDb);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.lblDb);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.pictureBox6);
            this.Controls.Add(this.txtBatchNumber);
            this.Controls.Add(this.btnIndicator);
            this.Controls.Add(this.btnLog);
            this.Controls.Add(this.offPrinter);
            this.Controls.Add(this.offCamera3);
            this.Controls.Add(this.offCamera2);
            this.Controls.Add(this.offCamera1);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.lblCamera3);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnSample);
            this.Controls.Add(this.btnAudit);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBoxCamera);
            this.Controls.Add(this.btnDeco);
            this.Controls.Add(this.btnConfig);
            this.Controls.Add(this.btnLogout);
            this.Controls.Add(this.lblPrinter);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.lblCamera2);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.lblCamera);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.onCamera1);
            this.Controls.Add(this.onCamera2);
            this.Controls.Add(this.onCamera3);
            this.Controls.Add(this.onPrinter);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Infeed";
            this.Text = "Infeed Station";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Infeed_Load);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Infeed_FormClosed);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvSend)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvReceive)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.offDb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.onDb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.offPrinter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.offCamera3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.offCamera2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.offCamera1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCamera)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.onCamera1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.onCamera2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.onCamera3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.onPrinter)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBoxCamera;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        public System.Windows.Forms.Button btnStart;
        public System.Windows.Forms.Label label7;
        public System.Windows.Forms.DataGridView dgvSend;
        public System.Windows.Forms.DataGridView dgvReceive;
        public System.Windows.Forms.Button btnLogout;
        public System.Windows.Forms.Button btnConfig;
        public System.Windows.Forms.Button btnDeco;
        public System.Windows.Forms.Button btnAudit;
        public System.Windows.Forms.Label lblQtySend;
        public System.Windows.Forms.Label lblAdmin;
        public System.Windows.Forms.Label lblPrinter;
        public System.Windows.Forms.Label lblCamera2;
        public System.Windows.Forms.Label lblCamera;
        public System.Windows.Forms.Button btnSample;
        public System.Windows.Forms.Button button1;
        private System.Windows.Forms.PictureBox pictureBox3;
        public System.Windows.Forms.Label lblCamera3;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label18;
        public System.Windows.Forms.Label lblQuantityReceive;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label lblDate;
        private System.Windows.Forms.Label lblTime;
        public System.Windows.Forms.Label lblRole;
        public System.Windows.Forms.Label lblUserId;
        private System.Windows.Forms.PictureBox pictureBox5;
        public System.Windows.Forms.PictureBox offPrinter;
        public System.Windows.Forms.PictureBox offCamera3;
        public System.Windows.Forms.PictureBox offCamera2;
        public System.Windows.Forms.PictureBox offCamera1;
        public System.Windows.Forms.Button btnLog;
        public System.Windows.Forms.PictureBox onPrinter;
        public System.Windows.Forms.PictureBox onCamera3;
        public System.Windows.Forms.PictureBox onCamera2;
        public System.Windows.Forms.PictureBox onCamera1;
        public System.Windows.Forms.Button btnIndicator;
        public System.Windows.Forms.Label labelProduct;
        public System.Windows.Forms.Label labelLine;
        public System.Windows.Forms.Label labelBatch;
        public System.Windows.Forms.TextBox txtBatchNumber;
        public System.Windows.Forms.Label lblQuantityReceiveAll;
        private System.Windows.Forms.Label label6;
        public System.Windows.Forms.PictureBox offDb;
        public System.Windows.Forms.PictureBox onDb;
        public System.Windows.Forms.Label lblDb;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.Timer timer1;

    }
}

