﻿namespace Infeed_Station
{
    partial class config
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtPortServer = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtIpPrinter = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtPortPrinter = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtIpCamera = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.txtPortDb = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtIpDB = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtTimeoutDb = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtIpCamera2 = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtNamaDB = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtAudit = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtGenerate = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtSend = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtDelay = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label18 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.txtIpCamera3 = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label41 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.chkBuffer = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtIpServer = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtPortCamera = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtPortCamera2 = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txtBagi = new System.Windows.Forms.TextBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.label34 = new System.Windows.Forms.Label();
            this.txtPortPLC = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.txtIPPLC = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtPortServer
            // 
            this.txtPortServer.Location = new System.Drawing.Point(109, 22);
            this.txtPortServer.Name = "txtPortServer";
            this.txtPortServer.Size = new System.Drawing.Size(100, 20);
            this.txtPortServer.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Port Server";
            // 
            // txtIpPrinter
            // 
            this.txtIpPrinter.Location = new System.Drawing.Point(96, 26);
            this.txtIpPrinter.Name = "txtIpPrinter";
            this.txtIpPrinter.Size = new System.Drawing.Size(100, 20);
            this.txtIpPrinter.TabIndex = 10;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 29);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(50, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "IP Printer";
            // 
            // txtPortPrinter
            // 
            this.txtPortPrinter.Location = new System.Drawing.Point(96, 52);
            this.txtPortPrinter.Name = "txtPortPrinter";
            this.txtPortPrinter.Size = new System.Drawing.Size(100, 20);
            this.txtPortPrinter.TabIndex = 11;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(10, 55);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(59, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Port Printer";
            // 
            // txtIpCamera
            // 
            this.txtIpCamera.Location = new System.Drawing.Point(109, 19);
            this.txtIpCamera.Name = "txtIpCamera";
            this.txtIpCamera.Size = new System.Drawing.Size(100, 20);
            this.txtIpCamera.TabIndex = 7;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 22);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "IP Camera 1";
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.button1.Image = global::Infeed_Station.Properties.Resources.submit;
            this.button1.Location = new System.Drawing.Point(295, 325);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(150, 50);
            this.button1.TabIndex = 16;
            this.button1.Text = "Submit";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtPortDb
            // 
            this.txtPortDb.Enabled = false;
            this.txtPortDb.Location = new System.Drawing.Point(96, 45);
            this.txtPortDb.Name = "txtPortDb";
            this.txtPortDb.Size = new System.Drawing.Size(100, 20);
            this.txtPortDb.TabIndex = 13;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(10, 48);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(44, 13);
            this.label8.TabIndex = 27;
            this.label8.Text = "Port DB";
            // 
            // txtIpDB
            // 
            this.txtIpDB.Location = new System.Drawing.Point(96, 19);
            this.txtIpDB.Name = "txtIpDB";
            this.txtIpDB.Size = new System.Drawing.Size(100, 20);
            this.txtIpDB.TabIndex = 12;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(10, 22);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(35, 13);
            this.label9.TabIndex = 25;
            this.label9.Text = "IP DB";
            // 
            // txtTimeoutDb
            // 
            this.txtTimeoutDb.Enabled = false;
            this.txtTimeoutDb.Location = new System.Drawing.Point(96, 68);
            this.txtTimeoutDb.Name = "txtTimeoutDb";
            this.txtTimeoutDb.Size = new System.Drawing.Size(100, 20);
            this.txtTimeoutDb.TabIndex = 14;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(10, 71);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(63, 13);
            this.label10.TabIndex = 23;
            this.label10.Text = "Timeout DB";
            // 
            // txtIpCamera2
            // 
            this.txtIpCamera2.Location = new System.Drawing.Point(109, 44);
            this.txtIpCamera2.Name = "txtIpCamera2";
            this.txtIpCamera2.Size = new System.Drawing.Size(100, 20);
            this.txtIpCamera2.TabIndex = 8;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 47);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(65, 13);
            this.label13.TabIndex = 31;
            this.label13.Text = "IP Camera 2";
            // 
            // txtNamaDB
            // 
            this.txtNamaDB.Location = new System.Drawing.Point(96, 94);
            this.txtNamaDB.Name = "txtNamaDB";
            this.txtNamaDB.Size = new System.Drawing.Size(100, 20);
            this.txtNamaDB.TabIndex = 15;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(10, 97);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(53, 13);
            this.label11.TabIndex = 35;
            this.label11.Text = "DB Name";
            // 
            // txtAudit
            // 
            this.txtAudit.Location = new System.Drawing.Point(108, 22);
            this.txtAudit.Name = "txtAudit";
            this.txtAudit.Size = new System.Drawing.Size(100, 20);
            this.txtAudit.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(11, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(75, 13);
            this.label3.TabIndex = 41;
            this.label3.Text = "Qty Pre-check";
            // 
            // txtGenerate
            // 
            this.txtGenerate.Location = new System.Drawing.Point(108, 74);
            this.txtGenerate.Name = "txtGenerate";
            this.txtGenerate.Size = new System.Drawing.Size(100, 20);
            this.txtGenerate.TabIndex = 4;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(11, 78);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(70, 13);
            this.label14.TabIndex = 39;
            this.label14.Text = "Qty Generate";
            // 
            // txtSend
            // 
            this.txtSend.Location = new System.Drawing.Point(108, 48);
            this.txtSend.Name = "txtSend";
            this.txtSend.Size = new System.Drawing.Size(100, 20);
            this.txtSend.TabIndex = 3;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(11, 52);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(51, 13);
            this.label15.TabIndex = 37;
            this.label15.Text = "Qty Send";
            // 
            // txtDelay
            // 
            this.txtDelay.Location = new System.Drawing.Point(108, 100);
            this.txtDelay.Name = "txtDelay";
            this.txtDelay.Size = new System.Drawing.Size(100, 20);
            this.txtDelay.TabIndex = 5;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(11, 103);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(75, 13);
            this.label16.TabIndex = 53;
            this.label16.Text = "Delay (Minute)";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.txtPortServer);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(226, 51);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Port";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(93, 22);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(10, 13);
            this.label18.TabIndex = 75;
            this.label18.Text = ":";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label23);
            this.groupBox2.Controls.Add(this.label22);
            this.groupBox2.Controls.Add(this.txtPortPrinter);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.txtIpPrinter);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Location = new System.Drawing.Point(249, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(226, 82);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Printer";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(80, 59);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(10, 13);
            this.label23.TabIndex = 76;
            this.label23.Text = ":";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(80, 29);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(10, 13);
            this.label22.TabIndex = 75;
            this.label22.Text = ":";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label26);
            this.groupBox3.Controls.Add(this.label29);
            this.groupBox3.Controls.Add(this.label24);
            this.groupBox3.Controls.Add(this.label25);
            this.groupBox3.Controls.Add(this.txtIpDB);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.txtTimeoutDb);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.txtPortDb);
            this.groupBox3.Controls.Add(this.txtNamaDB);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Location = new System.Drawing.Point(249, 100);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(226, 130);
            this.groupBox3.TabIndex = 5;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Database";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(80, 97);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(10, 13);
            this.label26.TabIndex = 80;
            this.label26.Text = ":";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(80, 75);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(10, 13);
            this.label29.TabIndex = 79;
            this.label29.Text = ":";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(80, 47);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(10, 13);
            this.label24.TabIndex = 78;
            this.label24.Text = ":";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(80, 22);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(10, 13);
            this.label25.TabIndex = 77;
            this.label25.Text = ":";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label30);
            this.groupBox4.Controls.Add(this.label31);
            this.groupBox4.Controls.Add(this.txtIpCamera3);
            this.groupBox4.Controls.Add(this.label20);
            this.groupBox4.Controls.Add(this.label19);
            this.groupBox4.Controls.Add(this.txtIpCamera);
            this.groupBox4.Controls.Add(this.label6);
            this.groupBox4.Controls.Add(this.label13);
            this.groupBox4.Controls.Add(this.txtIpCamera2);
            this.groupBox4.Location = new System.Drawing.Point(12, 238);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(226, 98);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Camera";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(93, 73);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(10, 13);
            this.label30.TabIndex = 80;
            this.label30.Text = ":";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(6, 73);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(65, 13);
            this.label31.TabIndex = 79;
            this.label31.Text = "IP Camera 3";
            // 
            // txtIpCamera3
            // 
            this.txtIpCamera3.Location = new System.Drawing.Point(109, 70);
            this.txtIpCamera3.Name = "txtIpCamera3";
            this.txtIpCamera3.Size = new System.Drawing.Size(100, 20);
            this.txtIpCamera3.TabIndex = 9;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(93, 47);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(10, 13);
            this.label20.TabIndex = 77;
            this.label20.Text = ":";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(93, 22);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(10, 13);
            this.label19.TabIndex = 76;
            this.label19.Text = ":";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label41);
            this.groupBox5.Controls.Add(this.label27);
            this.groupBox5.Controls.Add(this.label39);
            this.groupBox5.Controls.Add(this.label28);
            this.groupBox5.Controls.Add(this.label36);
            this.groupBox5.Controls.Add(this.label17);
            this.groupBox5.Controls.Add(this.chkBuffer);
            this.groupBox5.Controls.Add(this.txtAudit);
            this.groupBox5.Controls.Add(this.label15);
            this.groupBox5.Controls.Add(this.txtSend);
            this.groupBox5.Controls.Add(this.label14);
            this.groupBox5.Controls.Add(this.txtGenerate);
            this.groupBox5.Controls.Add(this.txtDelay);
            this.groupBox5.Controls.Add(this.label3);
            this.groupBox5.Controls.Add(this.label16);
            this.groupBox5.Location = new System.Drawing.Point(13, 69);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(226, 161);
            this.groupBox5.TabIndex = 2;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "System";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(92, 129);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(10, 13);
            this.label41.TabIndex = 78;
            this.label41.Text = ":";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(92, 25);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(10, 13);
            this.label27.TabIndex = 74;
            this.label27.Text = ":";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(92, 102);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(10, 13);
            this.label39.TabIndex = 77;
            this.label39.Text = ":";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(92, 50);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(10, 13);
            this.label28.TabIndex = 75;
            this.label28.Text = ":";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(92, 76);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(10, 13);
            this.label36.TabIndex = 76;
            this.label36.Text = ":";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(11, 129);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(65, 13);
            this.label17.TabIndex = 60;
            this.label17.Text = "Buffer Mode";
            // 
            // chkBuffer
            // 
            this.chkBuffer.AutoSize = true;
            this.chkBuffer.Location = new System.Drawing.Point(108, 129);
            this.chkBuffer.Name = "chkBuffer";
            this.chkBuffer.Size = new System.Drawing.Size(40, 17);
            this.chkBuffer.TabIndex = 6;
            this.chkBuffer.Text = "On";
            this.chkBuffer.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(614, 103);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "IP Server";
            // 
            // txtIpServer
            // 
            this.txtIpServer.Location = new System.Drawing.Point(711, 100);
            this.txtIpServer.Name = "txtIpServer";
            this.txtIpServer.Size = new System.Drawing.Size(100, 20);
            this.txtIpServer.TabIndex = 56;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(614, 132);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(74, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "Port Camera 1";
            // 
            // txtPortCamera
            // 
            this.txtPortCamera.Location = new System.Drawing.Point(711, 129);
            this.txtPortCamera.Name = "txtPortCamera";
            this.txtPortCamera.Size = new System.Drawing.Size(100, 20);
            this.txtPortCamera.TabIndex = 56;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(614, 158);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(71, 13);
            this.label12.TabIndex = 33;
            this.label12.Text = "Port Camera2";
            // 
            // txtPortCamera2
            // 
            this.txtPortCamera2.Location = new System.Drawing.Point(711, 155);
            this.txtPortCamera2.Name = "txtPortCamera2";
            this.txtPortCamera2.Size = new System.Drawing.Size(100, 20);
            this.txtPortCamera2.TabIndex = 135;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(625, 184);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(73, 13);
            this.label21.TabIndex = 51;
            this.label21.Text = "Qty Threshold";
            this.label21.Visible = false;
            // 
            // txtBagi
            // 
            this.txtBagi.Location = new System.Drawing.Point(711, 181);
            this.txtBagi.Name = "txtBagi";
            this.txtBagi.Size = new System.Drawing.Size(100, 20);
            this.txtBagi.TabIndex = 95;
            this.txtBagi.Visible = false;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.label34);
            this.groupBox6.Controls.Add(this.txtPortPLC);
            this.groupBox6.Controls.Add(this.label35);
            this.groupBox6.Controls.Add(this.label32);
            this.groupBox6.Controls.Add(this.txtIPPLC);
            this.groupBox6.Controls.Add(this.label33);
            this.groupBox6.Location = new System.Drawing.Point(249, 236);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(226, 83);
            this.groupBox6.TabIndex = 136;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Port";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(80, 46);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(10, 13);
            this.label34.TabIndex = 78;
            this.label34.Text = ":";
            // 
            // txtPortPLC
            // 
            this.txtPortPLC.Location = new System.Drawing.Point(96, 42);
            this.txtPortPLC.Name = "txtPortPLC";
            this.txtPortPLC.Size = new System.Drawing.Size(100, 20);
            this.txtPortPLC.TabIndex = 76;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(10, 46);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(49, 13);
            this.label35.TabIndex = 77;
            this.label35.Text = "Port PLC";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(80, 21);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(10, 13);
            this.label32.TabIndex = 75;
            this.label32.Text = ":";
            // 
            // txtIPPLC
            // 
            this.txtIPPLC.Location = new System.Drawing.Point(96, 22);
            this.txtIPPLC.Name = "txtIPPLC";
            this.txtIPPLC.Size = new System.Drawing.Size(100, 20);
            this.txtIPPLC.TabIndex = 1;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(10, 22);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(40, 13);
            this.label33.TabIndex = 2;
            this.label33.Text = "IP PLC";
            // 
            // config
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(495, 387);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.txtBagi);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.txtPortCamera2);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.txtPortCamera);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtIpServer);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "config";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Configuration";
            this.Load += new System.EventHandler(this.config_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtPortServer;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtIpPrinter;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtPortPrinter;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtIpCamera;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtPortDb;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtIpDB;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtTimeoutDb;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtIpCamera2;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtNamaDB;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtAudit;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtGenerate;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtSend;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtDelay;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.CheckBox chkBuffer;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox txtIpCamera3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtIpServer;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtPortCamera;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtPortCamera2;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtBagi;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox txtPortPLC;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TextBox txtIPPLC;
        private System.Windows.Forms.Label label33;
    }
}