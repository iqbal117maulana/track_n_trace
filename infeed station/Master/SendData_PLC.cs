﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace Infeed_Station
{
    class SendData_PLC
    {
        string IPAddress_;
        string Port_;
        string timeout_;
        string textData_;

      
        public string ErrMsg;
        tcp_testing3 tcp;
        bool is_connect;


        public bool Is_connect
        {
            get { return is_connect; }
            set { is_connect = value; }
        }
        public string TextData
        {
            get { return textData_; }
            set { textData_ = value; }
        }

        public string IPAddress
        {
            get { return IPAddress_; }
            set { IPAddress_ = value; }
        }

        public string Port
        {
            get { return Port_; }
            set { Port_ = value; }
        }

        public string Timeout
        {
            get { return timeout_; }
            set { timeout_ = value; }
        }

        public void Connect()
        {
            if (IPAddress.Length > 0 && Port.Length > 0 && Timeout.Length > 0)
            {
                tcp = new tcp_testing3();
                tcp.IPAddress = this.IPAddress;
                tcp.Port = this.Port;
                tcp.Timeout = this.Timeout;
                is_connect= tcp.Connect();
            }
            else
                ErrMsg = "Parameter Not Complete";
        }


        void checkConnection()
        {
            
        }
        public bool SendText()
        {
            try
            {
                
             //   if (tcp.Connect())
                //{
                    Is_connect = true;
                    if (TextData.Length > 0)
                    {
                        tcp.send(TextData);
                        return true;
                    }
                    else
                    {
                        ErrMsg = "Text data not initialization";
                        return false;
                    }
                //}
                //else
                //{
                //    is_connect = false;
                //    int cekNotConnect = 0;
                //    while (!is_connect)
                //    {
                //        if (cekNotConnect < 2)
                //        {
                //            if (tcp.Connect())
                //            {
                //                is_connect = true;
                //                SendText();
                //                return true;
                //            }
                //            cekNotConnect++;
                //        }
                //        else
                //        {
                //            cekNotConnect = 0;
                //            is_connect = true;
                //            MessageBox.Show("PLC Not Connected", "Information", MessageBoxButtons.OK);
                //            return false;
                //        }
                //        Thread.Sleep(500);
                //    }
                //    return false;
                //}
            }
            catch (Exception)
            {

                is_connect = false;
                int cekNotConnect = 0;
                while (!is_connect)
                {
                    if (cekNotConnect < 5)
                    {
                        //ctrl.log("Try to reconect [Buffer]");
                        if (tcp.Connect())
                        {
                            is_connect = true;
                            // ctrl.log("Connected to " + IPAddress + ":" + Port);
                            SendText();
                            return true;
                        }
                        else
                        {
                            //  ctrl.log("Not connected " + IPAddress + ":" + Port);
                        }
                        cekNotConnect++;
                    }
                    else
                    {
                        cekNotConnect = 0;
                        MessageBox.Show("PLC Not Connected", "Information", MessageBoxButtons.OK);
                    }
                    Thread.Sleep(5000);
                }
                return false;
            }

        }

        public bool CheckConnected()
        {
            tcp = new tcp_testing3();
            tcp.IPAddress = this.IPAddress;
            tcp.Port = this.Port;
            tcp.Timeout = this.Timeout;
            is_connect = tcp.Connect();
            return Is_connect;
        }
        public void Disconect()
        {
            tcp.disconnect();
        }
    }
}
