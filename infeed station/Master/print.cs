﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Threading;

namespace Infeed_Station
{
    public partial class print : Form
    {
        // printer domino inkjet
        tcp_testing4 tcpgs;
        bool isConnectGS;
        int hitung = 0;
        public string statusLeibinger;
        public bool isConnect;
        Mod_Sqlite sql;
        public int CountBuffer = -1; 

        public print()
        {
            InitializeComponent();
        }

        public bool cekKoneksi()
        {
            tcpgs = new tcp_testing4();
            sql = new Mod_Sqlite();
            tcpgs.IPAddress = sql.config["ipprinter"];
            tcpgs.Port = sql.config["portprinter"];
            tcpgs.Timeout = sql.config["portprinter"];
            bool res = tcpgs.Connect();
            isConnect = res;
            return res;
        }

        public void disconect()
        {
            isConnect = false;
            tcpgs.dc();
        }

        public void simpanLog(String line)
        {
            DateTime now = DateTime.Now;
            string tahun = now.ToString("yyyy");
            string bulan = now.ToString("MM");
            string hari = now.ToString("dd");
            string dates = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            line = dates + "\t" + line;
            // cek file 
            bool cekfile = false;
            try
            {
                while (!cekfile)
                {
                    if (Directory.Exists("eventlog\\Printer"))
                    {
                        if (Directory.Exists("eventlog\\Printer" + @"\" + tahun))
                        {
                            if (Directory.Exists("eventlog\\Printer" + @"\" + tahun + @"\" + bulan))
                            {

                                using (StreamWriter outputFile = new StreamWriter("eventlog\\Printer" + @"\" + tahun + @"\" + bulan + @"\" + hari + ".txt", true))
                                {
                                    outputFile.WriteLine(line);
                                }
                                cekfile = true;
                            }
                            else
                            {

                                Directory.CreateDirectory("eventlog\\Printer" + @"\" + tahun + @"\" + bulan);
                            }
                        }
                        else
                        {
                            Directory.CreateDirectory("eventlog\\Printer" + @"\" + tahun);
                        }
                    }
                    else
                    {

                        Directory.CreateDirectory("eventlog\\Printer");
                    }
                }
            }
            catch (Exception ex)
            {
                // simpanLog(ex.Message);
            }
        }

        internal void clearBuffergs()
        {

            string Data = Convert.ToChar(27) + "OE" + "00000" + Convert.ToChar(4);
            simpanLog(Data);
            tcpgs.send(Data);
            tcpgs.dc();
        }

        internal bool cekKoneksigs()
        { 
            tcpgs = new tcp_testing4();
            sql = new Mod_Sqlite();
            tcpgs.IPAddress = sql.config["ipprinter"];
            tcpgs.Port = sql.config["portprinter"];
            tcpgs.Timeout = sql.config["timeout"];
            bool res = tcpgs.Connect();
            isConnectGS = res;
            return res;
        }
//##kirim lewat laser
        //public int kirimData(List<string> data, int count)
        //{
        //    string disimpan = "";
        //    CountBuffer = count;
        //    for (int i = 0; i < data.Count; i++)
        //    {
        //        string simpan;
        //        simpan = "BUFFERDATA " + CountBuffer + " \"" + data[i] + "\"";
        //        tcpgs.send(simpan);
        //        disimpan = disimpan + " \n| " + simpan;
        //        hitung++;
        //        CountBuffer++;
        //        if (CountBuffer >= 10000)
        //            CountBuffer = -1;
        //    }
        //    simpanLog(disimpan);
        //    return CountBuffer;
        //}

        public void kirimData(List<string> data)
        {
            for (int i = 0; i < data.Count; i++)
            {
                string Send = Convert.ToChar(27) + "OE" + "0006" + data[i] + Convert.ToChar(4);
                tcpgs.send(Send);
            }
          //  tcpgs.dc();
        }



        public int kirimData(List<string> data, int count)
        {
            string disimpan = "";
            CountBuffer = count;
            for (int i = 0; i < data.Count; i++)
            {
                string simpan;
                simpan = Convert.ToChar(27) + "OE" + "0006" + data[i] + Convert.ToChar(4);
                tcpgs.send(simpan);
                disimpan = disimpan + " \n| " + simpan;
                hitung++;
                CountBuffer++;
                if (CountBuffer >= 10000)
                    CountBuffer = -1;
            }
            simpanLog(disimpan);
            return CountBuffer;
        }


        internal void start()
        {
            tcpgs.conneect = true;
        }

        Control ctrl;
        private Thread serverThread2;
        internal void startListener(Control control)
        {
           // ctrl = control;
           // tcpgs.ctrl = this.ctrl;
            //serverThread2 = new Thread(new ThreadStart(tcpgs.MulaiServer));
            //serverThread2.Start();

        }
    }
}
