﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Windows.Forms;
using System.IO;
using System.Threading;

namespace Infeed_Station
{
    class tcp_testing4
    {
        public const string CHECK_BUFFER = "STX";
        private static TcpClient Client;
        private static Byte[] Data = new Byte[256];
        private static NetworkStream ClientStream;
        public static bool IsConnected = false;
        public print pr;
        public Control ctrl;
        private Thread serverThread2;
        public string IPAddress;
        public string Port;
        public string Timeout;
        public int timer;
        bool isChecking = false;

        //## Connect
        //public bool Connect(string ip, string port, string timeout)
        //{
        //    try
        //    {
        //        Client = new TcpClient();
        //        IAsyncResult result = Client.BeginConnect(ip, Int32.Parse(port), null, null);
        //        bool success = result.AsyncWaitHandle.WaitOne(Int32.Parse(timeout), true);
        //        if (success)
        //        {
        //            IsConnected = true;
        //            return true;
        //        }
        //        else
        //        {
        //            IsConnected = false;
        //            return false;
        //        }
        //    }
        //    catch (SocketException ex)
        //    {
        //       if (!ex.NativeErrorCode.Equals(10056))
        //            IsConnected = false;
        //    }
        //    catch (InvalidOperationException te)
        //    {
        //        new Confirm("Printer not connected !");
        //    }
        //    return false;
        //}

        public void send(string dataTerima)
        {
            try
            {
                if (IsConnected)
                {
                    ClientStream = Client.GetStream();
                    StreamWriter sw = new StreamWriter(ClientStream);
                    sw.WriteLine(dataTerima);
                    sw.Flush();
                }
                else
                {
                    IsConnected = false;
                    int cekNotConnect = 0;
                    while (!IsConnected && conneect)
                    {
                        if (cekNotConnect < 10)
                        {
                            ctrl.log("Try Reconect [Send] + try:"+cekNotConnect);
                            if (Connect())
                            {
                                IsConnected = true;
                                send(dataTerima);
                            }
                            cekNotConnect++;
                        }
                        else
                        {
                            cekNotConnect = 0;
                            MessageBox.Show("Printer Not Connected", "Information", MessageBoxButtons.OK);
                        }
                        Thread.Sleep(1000);
                    }
                }
            }
            catch (Exception ex)
            {
                IsConnected = false;
                int cekNotConnect = 0;
                while (!IsConnected && conneect)
                {
                    if (cekNotConnect < 10)
                    {
                        ctrl.log("Try Reconect [Send] + try:" + cekNotConnect);
                        if (Connect())
                        {
                            IsConnected = true;
                            send(dataTerima);
                        }
                        cekNotConnect++;
                    }
                    else
                    {
                        cekNotConnect = 0;
                        MessageBox.Show("Printer Not Connected", "Information", MessageBoxButtons.OK);
                    }
                    Thread.Sleep(1000);
                }
            }
        }

        public void dc()
        {
            IsConnected = false;
            Client.Close();
        }

        public void updatestatus(string data)
        {
            string dates = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            //pr.txtStatus.AppendText(dates+"\t"+data+"\n");
        }

        public string sendBack(string dataTerima)
        {
            try
            {
                byte[] data = new Byte[256];
                if (IsConnected)
                {
                    ClientStream = Client.GetStream();
                    StreamWriter sw = new StreamWriter(ClientStream);
                    sw.WriteLine(dataTerima);
                    sw.Flush();
                    Int32 bytes = ClientStream.Read(data, 0, data.Length);
                    string responseData = System.Text.Encoding.ASCII.GetString(data, 0, bytes);
                    return responseData;
                }
                return "";
            }
            catch (InvalidOperationException ex)
            {

            }
            return "";
        }

        public bool conneect = true;

  
        private int GetBufferData(string data)
        {
            string[] dataSplit = data.Split(' ');
            return int.Parse(dataSplit[1]);
        }

        private string getBufferPrinter()
        {
            string data = "";
            for (int i = 0; i < 5; i++)
            {
                if (conneect)
                {
                    send(CHECK_BUFFER);
                    data = listener();
                    // if (data.Contains("SM"))
                    {
                        return data;
                    }
                }
                else
                    break;
            }
            return data;

        }

        private bool GetStatusPrinter()
        {
            try
            {
                string data = "";
                if (!isChecking && conneect)
                {
                    isChecking = true;
                    for (int i = 0; i < 5; i++)
                    {
                        send("^0?RS");
                        data = listener();
                        if (data.Contains("RS"))
                        {
                            break;
                        }
                    }
                    string nozzle = data.Substring(5, 1);
                    string[] split = data.Split('\t');
                    if (nozzle.Equals("2") && split[1].Equals("6"))
                    {
                        isChecking = false;
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                isChecking = false;
                return false;
            }
            isChecking = false;
            return false;

        }

        internal string listener()
        {
            try
            {
                byte[] data = new Byte[256];
                if (IsConnected)
                {
                    if (ClientStream == null)
                    {
                        send("cek");
                    }
                    ClientStream.ReadTimeout = 10000;
                    Int32 bytes = ClientStream.Read(data, 0, data.Length);
                    string responseData = System.Text.Encoding.ASCII.GetString(data, 0, bytes);
                    simpanLog(responseData);
                    return responseData;
                }
                else
                {
                    Connect();
                }
            }
            catch (IOException te)
            {
                IsConnected = false;
                if (Connect())
                {
                    IsConnected = true;
                    ClientStream = Client.GetStream();
                    ctrl.log("Timeout reply from printer");
                }
            }
            catch (Exception ex)
            {
                IsConnected = false;
                int cekNotConnect = 0;
                while (!IsConnected)
                {
                    if (cekNotConnect < 5)
                    {
                        if (Connect())
                        {
                            IsConnected = true;
                            ctrl.log("Connected to " + IPAddress + ":" + Port);
                        }
                        else
                        {
                            ctrl.log("Not connected " + IPAddress + ":" + Port);
                        }
                        cekNotConnect++;
                    }
                    else
                    {
                        cekNotConnect = 0;
                        new Confirm("Inkjet not connected", "Information", MessageBoxButtons.OK);
                    }
                    Thread.Sleep(timer * 1000);
                }
                return "Reconected";
            }
            return "";
        }

        public void simpanLog(String line)
        {
            DateTime now = DateTime.Now;
            string tahun = now.ToString("yyyy");
            string bulan = now.ToString("MM");
            string hari = now.ToString("dd");
            string dates = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            line = dates + "\t" + line;
            // cek file 
            bool cekfile = false;
            string namaFolder = "eventlog\\Printer";
            try
            {
                while (!cekfile)
                {
                    if (Directory.Exists(namaFolder))
                    {
                        if (Directory.Exists(namaFolder + @"\" + tahun))
                        {
                            if (Directory.Exists(namaFolder + @"\" + tahun + @"\" + bulan))
                            {

                                using (StreamWriter outputFile = new StreamWriter(namaFolder + @"\" + tahun + @"\" + bulan + @"\" + hari + ".txt", true))
                                {
                                    outputFile.WriteLine(line);
                                }
                                cekfile = true;
                            }
                            else
                            {

                                Directory.CreateDirectory(namaFolder + @"\" + tahun + @"\" + bulan);
                            }
                        }
                        else
                        {
                            Directory.CreateDirectory(namaFolder + @"\" + tahun);
                        }
                    }
                    else
                    {

                        Directory.CreateDirectory(namaFolder);
                    }
                }
            }
            catch (Exception ex)
            {
                simpanLog(ex.Message);
            }
        }

        internal bool Connect()
        {
            try
            {
                Client = new TcpClient();
                IAsyncResult result = Client.BeginConnect(IPAddress, Int32.Parse(Port), null, null);
                bool success = result.AsyncWaitHandle.WaitOne(Int32.Parse("2"), true);
                if (success)
                {
                    IsConnected = true;
                    return true;
                }
                else
                {
                    IsConnected = false;
                    return false;
                }
            }
            catch (SocketException ex)
            {
                new Confirm("error +" + ex.ToString());
                if (!ex.NativeErrorCode.Equals(10056))
                    IsConnected = false;
            }
            catch (InvalidOperationException te)
            {
                new Confirm("Printer not connected !");
                IsConnected = false;
            }
            return false;
        }


    }
}
