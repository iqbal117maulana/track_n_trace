﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Data;
using System.Net.NetworkInformation;
using System.ComponentModel;
using System.IO;

namespace Infeed_Station
{
    public class Control
    {
        public const string NUMBER_CODE = "03";
        public const string NAMA_FIELD = "infeedInnerBoxId";
        public const string NAMA_TABLE_TEMP = "tmp_infeedInnerbox";
        public const string NAMA_TABLE_UTAMA = "innerBox";
        public const string NAMA_STATION = "INFEED";
        public const string NAMA_SEQ = "infeedseq";
        public char delimit = ',';

        Mod_TCP printer;
        public Mod_Sqlite sqlite;
        public result rs;
        public Mod_Dbaccess db;
        public Infeed bl;
        public generateCode Code;
        private Thread serverThread;
        private Thread printerThread;
        private Thread printerThread2;
        private Thread DatagridThread;
        public string batch;
        public string adminid;
        public System.Timers.Timer delayTimer;
        public System.Timers.Timer delaydevice;
        public string linenumber;
        public string linename;
        public List<string[]> datacombo;
        public bool start;
        public bool startDelay;
        public int templastcountkirim;
        public List<string> dataSendToPrinter;
        public string[] dataPackagingOrder;
        public int countVaccine = 0;
        public bool listenPrinter = true;
        bool auditStarted;
        bool isConnectedPrinter = false;
        int tempTerima = 0;

        DataTable tableProductReceive = new DataTable();
        DataTable tableProductSend = new DataTable();
        int tempTerimaKirimUlang = 1;
        private BackgroundWorker worker = new BackgroundWorker();
        private AutoResetEvent resetEvent = new AutoResetEvent(false);
        public Mod_Server srv;
        bool isbuffer = false;
        public string movementType = "6";
        DataTable tableProductFail = new DataTable();

        // untuk mengirim ke printer dengan parameter
        int countRepeatSend = 0;
        int countRepeatDelete = 0;
        DataTable tableTerimaCamera = new DataTable();
        DataRow rowCamera;
        string blister = "";
        string stat = "";
        bool camera1 = false;
        bool camera2 = false;
        List<string[]> dataKawin = new List<string[]>();
        int dapatCamera1 = 0;
        int dapatCamera2 = 0;
        int dapatCamera3 = 0;
        print prin2;

        int quantityReceive = 0;
        int allQty = 0;
        public Log lg = new Log();

        public void workThread_Server()
        {
            srv = new Mod_Server();
            srv.Start(this, sqlite.config["portserver"]);
        }

        public void StopServer()
        {
            srv.listener.Stop();
        }

        public void StartServer2()
        {
            srv.listener.Start();
        }


        public void workThread_datasend()
        {
            while (connect)
            {
                SetDataGridSend("0", "");
                Thread.Sleep(1000);
            }
        }


        bool connect = false;
  
        public void disable()
        {
            bl.btnStart.Text = "Stop";
            bl.btnAudit.Enabled = false;
            bl.btnDeco.Enabled = false;
            bl.btnLogout.Enabled = false;
            bl.btnConfig.Enabled = false;
            bl.btnSample.Enabled = false;
            bl.btnIndicator.Enabled = false;
        }

        public void enable()
        {
            bl.btnStart.Text = "Start";
            bl.btnAudit.Enabled = true;
            bl.btnDeco.Enabled = true;
            bl.btnLogout.Enabled = true;
            bl.btnConfig.Enabled = true;
            bl.btnSample.Enabled = true;
            auditStarted = false;
            bl.btnIndicator.Enabled = true;
        }

        //public void SetDataComboBoxBatchNumber()
        //{
        //    List<string> field = new List<string>();
        //    field.Add("batchNumber,productModelId");
        //    string where = "status = '1' and linepackagingid = '" + linenumber + "'";
        //    datacombo = db.selectList(field, "[packaging_order]", where);
        //    List<string> da = new List<string>();
        //    for (int i = 0; i < datacombo.Count; i++)
        //    {
        //        da.Add(datacombo[i][0]);
        //    }
        //    bl.cmbBatch.DataSource = da;
        //}
        

        //public void log(string data)
        //{
        //    try
        //    {
        //        if (bl.InvokeRequired)
        //        {
        //            bl.Invoke(new Action<string>(log), new object[] { data });
        //            return;
        //        }
        //        string dates = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        //        data = data.Replace("\t", "");
        //        data = data.Replace("\n", "");
        //        data = data.Replace("\r", "");
        //        string simpan = dates + "\t" + data + "\n";
        //        bl.txtLog.AppendText(simpan);
        //    }
        //    catch (Exception ob)
        //    {
        //        db.simpanLog("Error Logger " + ob.Message);
        //    }
        //}

        public void log(string data)
        {
            try
            {
                if (lg.txtLog.InvokeRequired)
                {
                    lg.txtLog.Invoke(new Action<string>(log), new object[] { data });
                    return;
                }
                string dates = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                string simpan = dates + "\t" + data + "\n";
                lg.txtLog.AppendText(simpan + Environment.NewLine);
            }
            catch (ObjectDisposedException ob)
            {

            }
        }

        public void move(string To)
        {
            Dictionary<string, string> field = new Dictionary<string, string>();
            field.Add("userid", adminid);
            field.Add("[from]", NAMA_TABLE_UTAMA);
            field.Add("[to]", To);
            field.Add("qty", "" + Code.PackagingOrder);
            field.Add("movementType", movementType);
            field.Add("orderNumber", batch);
            db.Movement(field);
        }


        internal string getAdminName(string admin)
        {
            List<string> field = new List<string>();
            field.Add("first_name   ");
            string where = "id ='" + admin + "'";
            List<string[]> ds = db.selectList(field, "[USERS]", where);
            if (db.num_rows > 0)
                return ds[0][0];
            else
                return "";
        }

        internal bool checkPLCConnected()
        {
            return sendData_plc.CheckConnected();
        }
        SendData_PLC sendData_plc;
        public void SendData_PLC(string data)
        {
            try
            {
                sendData_plc.TextData = data;
                sendData_plc.SendText();
                //  sendData_plc.Disconect();
            }
            catch (Exception)
            {
                new Confirm("Something wrong witg PLC", "Information", MessageBoxButtons.OK);
            }
        }


        public void startStation()
        {
            db.adminid = adminid;
            db.from = "Infeed Station";
            db.eventname = "Start Station Infeed " + adminid;
            db.systemlog();
            rs = new result();
            bl.lblAdmin.Text = getAdminName(adminid);

            //Set OR Clear Data Receive
            clearDataReceive();
            clearDataSend();
            //SetDataComboBoxBatchNumber();
            //IF @Configuration Buffer send to Server True
            CheckConfiguration_Buffer();
            //Get Data From PLC
            serverThread = new Thread(new ThreadStart(workThread_Server));
            serverThread.Start();
            //Send & Get Data From Printer


            sendData_plc = new SendData_PLC();
            sendData_plc.IPAddress = sqlite.config["IPPLC"];
            sendData_plc.Port = sqlite.config["PortPLC"];
            sendData_plc.Timeout = "5";


            //Check Device
            //RepeatCheckDevice();
        }

        private void CheckConfiguration_Buffer()
        {
            if (sqlite.config["buffer"].Equals("0"))
                isbuffer = false;
            else
                isbuffer = true;
        }

        public void sudahditerimaProduct(string[] hasilsplit)
        {
            // tambah di table result
            Dictionary<string, string> field;
            List<string> fieldList = new List<string>();
            fieldList.Add("blisterpackid");
            for (int i = 2; i < hasilsplit.Length; i++)
            {
                field = new Dictionary<string, string>();
                field.Add(NAMA_FIELD, hasilsplit[1]);
                field.Add("flag", hasilsplit[0]);
                field.Add("createdtime", db.getUnixString());
                field.Add("blisterpackid", hasilsplit[i]);
                rs.insert(field, "result");
            }
            //hapus yang ada di tmp
            string where = NAMA_FIELD + " ='" + hasilsplit[1] + "'";
            sqlite.delete(where, NAMA_TABLE_TEMP);
        }

        //cek yang sudah dikirim ke printer
        public bool CekData(string blister)
        {
            List<string> field = new List<string>();
            field.Add(NAMA_FIELD);
            string where = NAMA_FIELD + " = '" + blister + "' and isused ='1'";
            sqlite.select(field, NAMA_TABLE_TEMP, where);
            if (sqlite.num_rows > 0)
                return true;
            else
                return false;
        }

        //cek status
        public string cek(string data)
        {
            if (data.Equals("0"))
            {
                return "Pass";
            }
            else if (data.Equals("2"))
            {
                SendData_PLC("1");
                return "Not Found Blister";
            }
            else if (data.Equals("3"))
            {
                SendData_PLC("1");
                return "Blister Already Exist";
            }
            else if (data.Equals("4"))
            {
                SendData_PLC("1");
                return "Infeed Already Exist";
            }
            else if (data.Equals("5"))
            {
                SendData_PLC("1");
                return "DB Not Connected";
            }
            else
                return "Fail";
        }

        private bool cekDataSample(string blisterid)
        {
            List<string> field = new List<string>();
            field.Add("infeedInnerBoxId");
            string where = "flag = 1 And infeedInnerBoxId ='" + blisterid + "'";
            List<string[]> ds = sqlite.select(field, "tmp_infeedInnerbox", where);
            if (sqlite.num_rows > 0 && ds==null)
            {
                return true;
            }
            return false;
        }

        public string[] AddNewVaccine(string[] hasilsplit)
        {
            List<string> fieldList2 = new List<string>();
            fieldList2.Add("infeedInnerBoxId");
            string where5 = "infeedInnerBoxId ='" + hasilsplit[1] + "' and batchnumber='" + batch + "'";
            List<string[]> dataInnerbox = db.selectList(fieldList2, "innerbox", where5);
            if (db.num_rows == 0)
            {

                Dictionary<string, string> fieldInfeed = new Dictionary<string, string>();
                hasilsplit[1] = hasilsplit[1].Replace(" ", "");
                fieldInfeed.Add(NAMA_FIELD, hasilsplit[1]);
                fieldInfeed.Add("isreject", hasilsplit[0]);
                fieldInfeed.Add("createdtime", db.getUnixString());
                fieldInfeed.Add("batchnumber", batch);
                if (hasilsplit[0].Equals("1"))
                {
                    fieldInfeed.Add("rejecttime", db.getUnixString());
                }
                fieldInfeed.Add("flag", "0");
                db.insert(fieldInfeed, "innerbox");
                Dictionary<string, string> field;
                Dictionary<string, string> fieldBlister;
                List<string> fieldList = new List<string>();
                fieldList.Add("blisterpackid");
                for (int i = 2; i < hasilsplit.Length; i++)
                {
                    string where1 = "blisterpackid ='" + hasilsplit[i] + "' and batchnumber='" + batch + "'";
                    List<string[]> dataCapid = db.selectList(fieldList, "blisterpack", where1);
                    if (db.num_rows > 0)
                    {
                        string where3 = "blisterpackid ='" + hasilsplit[i] + "' and batchnumber='" + batch + "' and innerboxid is null";
                        List<string[]> dataCa = db.selectList(fieldList, "blisterpack", where3);
                        if (db.num_rows > 0)
                        {
                            // Update Blister
                            fieldBlister = new Dictionary<string, string>();
                            fieldBlister.Add("innerBoxId", hasilsplit[1]);
                            fieldBlister.Add("isreject", "0");
                            db.update(fieldBlister, "blisterpack", "blisterpackId ='" + hasilsplit[i] + "' and batchnumber='" + batch + "'");

                            // Update vaccine
                            field = new Dictionary<string, string>();
                            field.Add("innerBoxId", hasilsplit[1]);
                            field.Add("isreject", "0");
                            db.update(field, "vaccine", "blisterpackid like'%" + hasilsplit[i] + "%'  and batchnumber='" + batch + "'");
                        }
                        else
                        {

                            hasilsplit[0] = "3";
                            log("Data Blister: " + hasilsplit[i] + " Already Registered");
                        }
                    }
                    else
                    {
                        hasilsplit[0] = "2";
                        log("Data Blister: " + hasilsplit[i] + " Not Found");
                    }
                }

                if (hasilsplit[0] != "0")
                {
                    // data blister not found
                    field = new Dictionary<string, string>();
                    field.Add("isreject", "1");
                    db.update(field, "innerBox", "blisterpackid like'%" + hasilsplit[1] + "%'  and batchnumber='" + batch + "'");

                    field = new Dictionary<string, string>();
                    field.Add("innerboxid", "null");
                    db.update(field, "blisterpack", "blisterpackid like'%" + hasilsplit[1] + "%'  and batchnumber='" + batch + "'");


                    db.update(field, "vaccine", "blisterpackid like'%" + hasilsplit[1] + "%'  and batchnumber='" + batch + "'");

                }
            }
            else
            {
                hasilsplit[0] = "4";
                log("Data Innerbox: " + hasilsplit[1] + " Already Exist");
            }
            return hasilsplit;
        }

        // Terima data dari PLC
        public void tambahData(string data)
        {
            if (bl.InvokeRequired)
            {
                bl.Invoke(new Action<string>(tambahData), new object[] { data });
                return;
            }
            //log("Receive Data:" + data);
            data = data.Replace("\r", "");
            data = data.Replace("\n", "");
            data = data.Replace("]d2", "");
            data = data.Replace("$", "");
            data = data.Replace("?", "");
            data = data.Replace(" ", "");
            List<string> dataMas = dataMassInfeed(data);
            if (bl.cekPLCConnect())
            {

            }
            for (int i = 0; i < dataMas.Count; i++)
            {
                quantityReceive++;
                log("Proses Data:" + dataMas[i]);
                string[] hasilSplit = dataMas[i].Split(delimit);
                if (bl.cekDB())
                {
                    if (data.Length > 0)
                    {
                        //string[] hasilSplit = dataMas[i].Split(delimit);
                        if (hasilSplit[0].Equals("0"))
                        {
                            hasilSplit = AddNewVaccine(hasilSplit);
                        }
                        //updatetableReceive(hasilSplit);
                        //if (auditStarted)
                        //    cekKirimUlang();
                    }
                    else
                    {
                        if (data.Length > 0)
                            log("Data " + data + " invalid");
                    }
                }
                else
                {
                    StopServer();
                    Console.WriteLine("TIDAK KONEKKKKK");
                    hasilSplit[0] = "5";
                }
                updatetableReceive(hasilSplit);
                bl.lblQuantityReceive.Text = "" + quantityReceive;
                if (hasilSplit[0].Equals("0"))
                {
                    allQty++;
                }
                bl.lblQuantityReceiveAll.Text = "" + allQty;
                if (auditStarted)
                    cekKirimUlang();
            }
        }

        private List<string> dataMassInfeed(string data)
        {
            char[] delimiters = new char[] { '|' };
            List<string> hasilSplit = new List<string>(data.Split(delimiters, StringSplitOptions.RemoveEmptyEntries));
            return hasilSplit;
        }

        // masukan ke tabel recive()
        public void updatetableReceive(string[] hasilSplit)
        {
            string dates = DateTime.Now.ToString("HH:mm:ss");
            string infeedID = hasilSplit[1];
            string hasil = cek(hasilSplit[0]);
            for (int i = 2; i < hasilSplit.Length; i++)
            {
                DataRow row = tableProductReceive.NewRow();
                row[0] = infeedID;
                row[1] = hasilSplit[i];
                row[2] = dates;
                row[3] = hasil;
                tableProductReceive.Rows.InsertAt(row, 0);
            }
            bl.dgvReceive.DataSource = tableProductReceive;
            setlog(hasilSplit);
            bl.dgvReceive.ClearSelection();

            if (bl.dgvReceive.Rows.Count > 100)
            {
                bl.dgvReceive.Rows.RemoveAt(100);
                bl.dgvReceive.Rows.RemoveAt(100);
                //quantityReceive = bl.dgvReceive.Rows.Count;
            }
            bl.dgvReceive.FirstDisplayedScrollingRowIndex = 1;
        }

        // cek banyak yang belum dikirim
        public int cekDataNotsend()
        {
            List<string> field = new List<string>();
            field.Add("count(*)");
            string where = "isused = '0'";
            List<string[]> ds = sqlite.select(field, NAMA_TABLE_TEMP, where);
            if (sqlite.num_rows > 0)
            {
                return int.Parse(ds[0][0]);
            }
            else
            {
                return 0;
            }
        }

        //Check Untuk generate ulang
        public void cekgenerateUlang()
        {
            int setengahgenerate = int.Parse(sqlite.config["qtygenerate"]) / 2;
            if (cekDataNotsend() < setengahgenerate)
            {
                generateAgain();
                tempTerima = 0;
                SetDataGridSend("0", "");
            }
        }
      
        public void cekKirimUlang()
        {

            int setengahgenerate = int.Parse(sqlite.config["qtysend"]);
            if (tempTerimaKirimUlang >= setengahgenerate)
            {
                SendToPrinter(getDataSendToPrinter(int.Parse(sqlite.config["qtysend"])));
                tempTerimaKirimUlang = 1;
            }
            else
            {
                tempTerimaKirimUlang++;
            }
        }

        // ambil data yang mau dikirim
        public List<string> getDataSendToPrinter(int count)
        {
            List<string> field = new List<string>();
            field.Add(NAMA_FIELD);
            string where = "isused = '0' order by " + NAMA_FIELD + " asc limit " + count;
            List<string[]> ds = sqlite.select(field, NAMA_TABLE_TEMP, where);
            List<string> temp = new List<string>();
            // ambil id nya saja 
            int i = 0;
            foreach (string[] data in ds)
            {
                temp.Add(ds[i][0]);
                i++;
            }
            return temp;
        }
        public void SendToPrinter(List<string> data)
        {
            Code.kirimPrinter(data);
            sqlite.BeginDelete(data);
            log("Send " + data.Count + " Data");
            cekgenerateUlang();
        }
        public void SendToPrinterTemp(List<string> data)
        {
            Code.kirimPrinter(data);
            db.eventname = "Send To Printer " + data.Count;
            db.systemlog();
            log("Send To Printer : " + data.Count);
            Dictionary<string, string> field = new Dictionary<string, string>();
            field.Add("isused", "1");
            field.Add("count", "" + countRepeatSend);
            foreach (string da in data)
            {
                log("Send To Printer : " + da);
                sqlite.update(field, NAMA_TABLE_TEMP, NAMA_FIELD + " = '" + da + "'");
            }
            getdatasend();
            templastcountkirim = templastcountkirim + data.Count;
            db.updatesequence(batch, "" + templastcountkirim);
            countRepeatSend++;
            cekgenerateUlang();
        }
     
        public bool CheckConection()
        {
            printer = new Mod_TCP();
            printer.IPAddress = sqlite.config["ipprinter"];
            printer.port = sqlite.config["portprinter"];
            printer.timeout = "1";
            printer.Log("Intizialize");
            bool res = printer.OpenConnection();
            return res;
        }

        public void kirimData(List<string> data)
        {
            for (int i = 0; i < data.Count; i++)
            {
                string Send = "^MD^" + sqlite.config["ipServer"] + " " + data[i];
                printer.dataPrint = Send;
                printer.send(Send);
            }
        }

        int count = 0;
   

        internal void KirimUlang()
        {
            SendToPrinter(getDataSendToPrinter(1));
            tempTerimaKirimUlang = 1;
            SetDataGridSend("0", "");
        }

        // ambil data yang sudah di kirim 
        private void getdatasend()
        {
            dataSendToPrinter = new List<string>();
            List<string> field = new List<string>();
            field.Add(NAMA_FIELD);
            string where = "isused = 1";
            List<string[]> ds = sqlite.select(field, NAMA_TABLE_TEMP, where);
            foreach (string[] da in ds)
            {
                dataSendToPrinter.Add(da[0]);
            }
        }

        //untuk generate code
        public void generateCodeSend(int qty)
        {
            //cek lastcount

            //Code.getLastcount();
            //set batch
            Code.batch = batch;
            //buat code
            Code.generateProduct(qty);
            db.eventname = "GenerateCode :" + qty + " Infeed Station";
            db.systemlog();
            qty = qty * 3;
            log("Generate Code : " + qty);
        }



        public void generateCodeSend_AG(int qty)
        {
            //buat code
            Code = new generateCode();
            Code.generateProduct(qty);
        }

        //stop proses
        public void Stop()
        {
            try
            {
                log("Stop Production");
                listenPrinter = false;
                Code.disconect();
                printer.CloseConnection();
                srv.clearCache();
                auditStarted = false;
                start = false;
                //bl.cmbBatch.Enabled = true;
                enable();
                //SetDataComboBoxBatchNumber();
                //  sqlite.delete("", NAMA_TABLE_TEMP);
                //  sqlite.delete("", "result");
                delayTimer.Stop();
                delaydevice.Stop();
                move("infeed");
                db.eventname = "Finish Print";
                db.systemlog();
                //sqlite.updatestatusconfig("");
            }
            catch (NullReferenceException nf)
            {

            }
        }

        // hapus data datagridview receive
        public void clearDataReceive()
        {
            tableProductReceive = new DataTable();
            tableProductReceive.Columns.Add("Infeed ID");
            tableProductReceive.Columns.Add("Blister ID");
            tableProductReceive.Columns.Add("Timestamp");
            tableProductReceive.Columns.Add("Status");
            bl.dgvReceive.DataSource = tableProductReceive;
            quantityReceive = 0;
        }

        // hapus data datagridview Send
        public void clearDataSend()
        {
            tableProductSend = new DataTable();
            tableProductSend.Columns.Add("Infeed ID");
            bl.dgvSend.DataSource = tableProductSend;
        }

        // mengecek batchnumber masih aktif atau tidak
        public bool cekBatchNumber(string batch)
        {
            List<string> field = new List<string>();
            field.Add("batchnumber");
            string where = "batchnumber ='" + batch + "' and status ='1'";
            List<string[]> ds = db.selectList(field, "packaging_order", where);
            if (db.num_rows > 0)
                return true;
            else
                return false;
        }

        // memulai audit
        public void startAudit()
        {
            setIdAudit();
        }

        //memulai printing dengan sebelumnya memulai audit
        public void startPrinting()
        {
            db.eventname = "Start Audit";
            db.systemlog();
            dataPackagingOrder = db.dataPO(batch);
            //bl.lblPo.Text = batch;
            //bl.lblProductName.Text = dataPackagingOrder[3];
            //getVaccine();qtygenerate
            generateCodeSend(int.Parse(sqlite.config["qtygenerate"]));
            SetDataGridSend("0", "" + int.Parse(sqlite.config["qtysend"]));
        }

        public void setIdAudit()
        {
            db.eventname = "Pre-check " + batch;
            db.systemlog();
            auditStarted = true;
            string[] datapack = db.dataPO(batch);
            //bl.lblPo.Text = batch;
            //bl.lblProductName.Text = datapack[3];
            sqlite.delete("", NAMA_TABLE_TEMP);
            Code.lastCount = db.getcountProduct(batch) + 1;
            templastcountkirim = Code.lastCount - 1;
            Code.audit = true;
            generateCodeSend(int.Parse(sqlite.config["qtygenerate"]));
            Code.audit = false;
            SendToPrinter(getDataSendToPrinter(1));
        }

        // setelah kirim printer apabila kurang maka generate ulang
        public void generateAgain()
        {
            // Code.lastCount = db.getcountProduct(batch) + 1;
            generateCodeSend(int.Parse(sqlite.config["qtygenerate"]));
        }


        // untuk tampil data di grid send
        public void SetDataGridSend(string status, string banyak)
        {
            if (bl.InvokeRequired)
            {
                bl.Invoke(new Action<string, string>(SetDataGridSend), new object[] { status, banyak });
                return;
            }
            List<string> field = new List<string>();
            field.Add(NAMA_FIELD + " 'Infeed ID'");
            string limit = "";
            if (banyak.Length > 0)
                limit = "limit " + banyak;
            List<string[]> ds = sqlite.select(field, NAMA_TABLE_TEMP, "isused = '" + status + "' " + limit);
            if (sqlite.num_rows > 0)
            {
                tableProductSend = sqlite.dataHeader;
                bl.dgvSend.DataSource = tableProductSend;
                foreach (string[] Row in ds)
                {
                    DataRow row = tableProductSend.NewRow();
                    row[0] = Row[0];
                    tableProductSend.Rows.Add(row);
                }

                bl.dgvSend.DataSource = tableProductSend;
            }
        }

       

        //update data sample
        public void sampleData(string blisterid)
        {
            Dictionary<string, string> field = new Dictionary<string, string>();
            //status sample
            field.Add("isused", "4");
            string where = NAMA_FIELD + "='" + blisterid + "'";
            sqlite.update(field, NAMA_TABLE_TEMP, where);
        }

        // untuk mengirim ke database server
        public void delay()
        {
            startDelay = true;
            delayTimer = new System.Timers.Timer();
            int delaySql = int.Parse(sqlite.config["delay"]);
            delaySql = delaySql * 60000;
            delayTimer.Interval = delaySql;
            delayTimer.Elapsed += new System.Timers.ElapsedEventHandler(delayTimer_Elapsed);
            delayTimer.Start();
        }

        // yang dilakukan pada saat delay
        void delayTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            if (!db.SendToDB)
            {
                db.SendToDB = true;
                db.addVaccine(batch, linenumber, "", rs);
            }
        }

        public void CekDeviceDelay()
        {
            delaydevice = new System.Timers.Timer();
            delaydevice.Interval = 10000;
            delaydevice.Elapsed += new System.Timers.ElapsedEventHandler(delaydevice_Elapsed);
            delaydevice.Start();
        }

        void delaydevice_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            CekDevice();
        }

        public bool cekDeviceGS()
        {
            print pr = new print();
            if (pr.cekKoneksigs())
            {
                pr.disconect();
                return true;
            }
            else
            {
                bl.lblPrinter.Text = "Offline";
                return false;
            }
        }
 
        public void CekDevice()
        {
            if (PingHost(sqlite.config["ipCamera"]))
                bl.lblCamera.Text = "Online";
            else
                bl.lblCamera.Text = "Offline";


            if (PingHost(sqlite.config["ipCamera2"]))
                bl.lblCamera2.Text = "Online";
            else
                bl.lblCamera2.Text = "Offline";
            if (PingHost(sqlite.config["ipprinter"]))
                bl.lblPrinter.Text = "Online";
            else
                bl.lblPrinter.Text = "Offline";
        }

        //untuk ngeping ke ip parameter
        public bool PingHost(string nameOrAddress)
        {
            if (nameOrAddress.Length == 0)
                return false;
            bool pingable = false;
            Ping pinger = new Ping();
            try
            {
                PingReply reply = pinger.Send(nameOrAddress);
                pingable = reply.Status == IPStatus.Success;
            }
            catch (PingException)
            {
                // Discard PingExceptions and return false;
            }
            return pingable;
        }

        //void RepeatCheckDevice()
        //{
        //    worker.DoWork += new DoWorkEventHandler(worker_DoWork);
        //    worker.WorkerReportsProgress = true;
        //    worker.RunWorkerAsync();
        //    resetEvent.WaitOne();
        //}

        //void worker_DoWork(object sender, DoWorkEventArgs e)
        //{
        //    //CekDevice();
        //    bl.cekDB();
        //    bl.cekCamera();
        //    bl.cekDevice();
        //    resetEvent.Set();
        //}

        public void test()
        {
            db.addVaccine(batch, sqlite.config["lineNumber"], "", rs);

        }

        public void sendStartPrinter()
        {
            db.eventname = "Start printing " + batch + " Infeed Station";
            db.systemlog();
            tempTerima = 1;
            auditStarted = true;
            string[] datapack = db.dataPO(batch);
            bl.labelBatch.Text = batch;
            bl.labelProduct.Text = datapack[3];
            sqlite.delete("", NAMA_TABLE_TEMP);
            Code.lastCount = db.getcountProduct(batch) + 1;
            templastcountkirim = Code.lastCount - 1;
            generateCodeSend(int.Parse(sqlite.config["qtygenerate"]));
            SendToPrinter(getDataSendToPrinter(int.Parse(sqlite.config["qtysend"])));
            SendToPrinter(getDataSendToPrinter(int.Parse(sqlite.config["qtysend"])));
        //  SetStartPrinterThread();
        }

        private void SetStartPrinterThread()
        {
            prin2 = new print();

            if (prin2.cekKoneksi())
            {
                prin2.start();
                // prin2.clearBuffer();
                prin2.startListener(this);
                // SetDataGridSend("0", "");
            }
            else
            {
                log("Printer not connected");

                

            }
        }

        void setlog(string[] hasilsplit)
        {
            log("----------------------------------------");
            log("Infeed ID  : " + hasilsplit[1]);
            log("Blister ID : " + hasilsplit[2]);
            log("Status     : " + cek(hasilsplit[0]));
            log("----------------------------------------");
        }

        public void tampil(string data)
        {
            if (bl.InvokeRequired)
            {
                bl.Invoke(new Action<string>(tampil), new object[] { data });
                return;
            }

            log("Receive Data : " + data);
        }

        internal int cekKamera(string Message)
        {
            Message = Message.Replace("\r", "");
            Message = Message.Replace("\n", "");
            tampil("Receive Data : " + Message);
            List<string> field = new List<string>();
            field.Add("*");
            string where = "infeedInnerBoxId = '" + Message + "'";
            sqlite.select(field, "tmp_infeedInnerbox", where);
            if (sqlite.num_rows > 0)
            {
                return 0;
            }
            else
            {
                return 1;
            }
        }

        internal void terimaData(string Message, int p)
        {
            if (bl.InvokeRequired)
            {
                bl.Invoke(new Action<string, int>(terimaData), new object[] { Message, p });
                return;
            }
            if (!Message.Equals("0"))
            {
                Message = Message.Replace("\r", "");
                Message = Message.Replace("\n", "");
                Message = Message.Replace("]d2", "");
                string[] data = Message.Split(',');
                //Camera,pattern,data
                if (data.Length == 3)
                {

                    Message = data[2];
                    string hasil = "";
                    if (!data[1].Equals("3"))
                    {
                        log(Message);
                        simpanLogKawin(Message);
                    }
                    if (p == 0)
                    {
                        log("Camera 1:" + Message + " Status:" + data[1]);
                        simpanLogKawin("Camera 1:" + Message + " Status:" + data[1]);
                        if (data[1].Equals("0"))
                        {
                            string[] dataTemp = new string[4];
                            dataTemp[0] = Message;
                            dataKawin.Add(dataTemp);
                            dapatCamera1++;
                            //rowCamera = tableTerimaCamera.NewRow();
                            //rowCamera[0] = Message;
                            //blister = Message + ",";
                            camera1 = true;
                        }
                        else if (data[1].Equals("1"))
                        {
                            string[] dataTemp = new string[4];
                            dataTemp[0] = "Fail";
                            dataKawin.Add(dataTemp);
                            dapatCamera1++;
                            camera1 = false;
                        }
                    }
                    else if (p == 1)
                    {
                        log("Camera 2:" + Message + " Status:" + data[1]);
                        simpanLogKawin("Camera 2:" + Message + " Status:" + data[1]);
                        if (data[1].Equals("0"))
                        {
                            dataKawin[dapatCamera2][1] = Message;
                            //rowCamera[1] = Message;
                            //blister = blister + Message;
                            camera2 = true;
                        }
                        else if (data[1].Equals("1"))
                        {
                            dataKawin[dapatCamera2][1] = "Fail";
                            //rowCamera[1] = Message;
                            //blister = blister + Message;
                            camera2 = false;
                        }
                        dapatCamera2++;
                    }
                    else if (p == 2)
                    {
                        log("Camera 3:" + Message + " Status:" + data[1]);
                        simpanLogKawin("Camera 3:" + Message + " Status:" + data[1]);
                        if (data[1].Equals("0"))
                        {
                            dataKawin[dapatCamera3][2] = Message;
                            //rowCamera[2] = Message;
                            if (!dataKawin[dapatCamera3][0].Equals("Fail") && !dataKawin[dapatCamera3][1].Equals("Fail"))
                            {
                                dataKawin[dapatCamera3][3] = "Pass";
                                hasil = "0";
                            }
                            else
                            {
                                dataKawin[dapatCamera3][3] = "Fail";
                                hasil = "1";
                            }
                            //         stat = hasil + "," + dataKawin[dapatCamera3][2] + "," + dataKawin[dapatCamera3][0] + "," + dataKawin[dapatCamera3][1];
                        }
                        else if (data[1].Equals("1"))
                        {
                            dataKawin[dapatCamera3][2] = "Fail";
                            dataKawin[dapatCamera3][3] = "Fail";
                            hasil = "1";
                        }

                        stat = hasil + "," + dataKawin[dapatCamera3][2] + "," + dataKawin[dapatCamera3][0] + "," + dataKawin[dapatCamera3][1];
                        tambahData(stat);
                        dapatCamera3++;
                        blister = "";
                        stat = "";
                    }
                    //tableTerimaCamera.Rows.Add(rowCamera);
                }
            }
        }

        DataTable ConvertListToDataTable(List<string[]> list)
        {
            // Add rows. 
            tableTerimaCamera = new DataTable();
            tableTerimaCamera.Columns.Add("Camera 1");
            tableTerimaCamera.Columns.Add("Camera 2");
            tableTerimaCamera.Columns.Add("Camera 3");
            tableTerimaCamera.Columns.Add("Status");
            foreach (var array in list)
            {
                tableTerimaCamera.Rows.Add(array);
            }

            return tableTerimaCamera;
        }

        public void simpanLogKawin(String line)
        {
            DateTime now = DateTime.Now;
            string tahun = now.ToString("yyyy");
            string bulan = now.ToString("MM");
            string hari = now.ToString("dd");
            string dates = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            line = dates + "\t" + line;
            // cek file 
            bool cekfile = false;
            string namaFolder = "eventlog\\Camera";
            try
            {
                while (!cekfile)
                {
                    if (Directory.Exists(namaFolder))
                    {
                        if (Directory.Exists(namaFolder + @"\" + tahun))
                        {
                            if (Directory.Exists(namaFolder + @"\" + tahun + @"\" + bulan))
                            {

                                using (StreamWriter outputFile = new StreamWriter(namaFolder + @"\" + tahun + @"\" + bulan + @"\" + hari + ".txt", true))
                                {
                                    outputFile.WriteLine(line);
                                }
                                cekfile = true;
                            }
                            else
                            {

                                Directory.CreateDirectory(namaFolder + @"\" + tahun + @"\" + bulan);
                            }
                        }
                        else
                        {
                            Directory.CreateDirectory(namaFolder + @"\" + tahun);
                        }
                    }
                    else
                    {

                        Directory.CreateDirectory(namaFolder);
                    }
                }
            }
            catch (Exception ex)
            {
                //simpanLog(ex.Message);
            }
        }

        internal void clearBufferGs()
        {
            print pr2 = new print();
            pr2.cekKoneksigs();
            pr2.clearBuffergs();
            pr2.disconect();
            pr2 = null;
        }

        //tambahan
        public int[] getCount(string btch)
        {
            int[] cnt = new int[1];
            allQty = db.selectCountPass(btch);
            //qtyFail = db.selectCountReject(btch);
            //quantityReceive = qtyGood + qtyFail;
            //cnt[0] = qtyGood;
            //cnt[1] = qtyFail;
            cnt[0] = allQty;
            return cnt;
        }
    }
}

