﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;

namespace Infeed_Station
{
    
    public class generateCode
    {

        public const int QTY_KIRIM = 10;
        public const int QTY_MAKS_KIRIM_PRODUCT = 20;

        public int pembuatan;
        public int PackagingOrder;
        public int postCodeProduct;
        public int tempBanyak;
        public string batch;
        Mod_Sqlite sql;
        public int tambahProduct = 1;
        public List<string> dataProduct;
        public bool masih;
        public int penomoran;
        public int lastCount;
        public bool audit;

        Mod_Dbaccess db = new Mod_Dbaccess();
        public generateCode()
        {
            sql = new Mod_Sqlite();
            dataProduct = new List<string>();
        }

        public string[] generateProduct()
        {
            cekTemp();
            string[] temp = new string[tempBanyak];
            for (int i = 0; i < tempBanyak; i++)
            {
               // temp[i] = batch + Control.NUMBER_CODE + penomoran.ToString().PadLeft(5, '0');
                temp[i] = Control.NUMBER_CODE + penomoran.ToString().PadLeft(5, '0');
                Dictionary<string, string> field = new Dictionary<string, string>();
                field.Add(Control.NAMA_FIELD, temp[i]);
                field.Add("isused", "0");
                sql.insertData(field, Control.NAMA_TABLE_TEMP);
                dataProduct.Add(temp[i]);
                tambahProduct++;
                penomoran++;
            }
            sql.Begin();
            return temp;
        }

        int banyakBuffer = -1;
        print PRgs;

        //public void kirimPrinter(List<string> data)
        //{
        //    print pr = new print();
        //    pr.cekKoneksi();
        //    pr.kirimData(data);
        //}

        public void disconect()
        {
            PRgs.disconect();
        }
        public void kirimPrinter(List<string> data)
        {
            print pr = new print();
            pr.cekKoneksi();
            pr.kirimData(data);
            //PRgs = new print();
            //int i = 0;
            //bool konek = false;
            //while(i<10 || !PRgs.cekKoneksigs())
            ////if (PRgs.cekKoneksigs())
            //{
            //    konek = true;
            //    Thread.Sleep(800);
            //}
            //if (konek)
            //{
            //    banyakBuffer = PRgs.kirimData(data, banyakBuffer);
            //    PRgs.disconect();
            //}
            //else
            //{
            // //   new Confirm("Printer not connected", "Information", MessageBoxButtons.OK);
            //}
        }

        public void cekTemp()
        {
            int qtygenerate = 0;
            if (tambahProduct == 0)
                qtygenerate = QTY_KIRIM * 2;
            else
            {
                qtygenerate = QTY_KIRIM;
            }
            int temp = PackagingOrder - tambahProduct;
            if (temp > qtygenerate)
            {
                tempBanyak = qtygenerate;
                postCodeProduct = postCodeProduct + tempBanyak;
            }
            else
            {
                if (temp <= qtygenerate)
                {
                    tempBanyak = temp;
                    masih = true;
                }
                else
                {
                    tempBanyak = qtygenerate;
                    postCodeProduct = postCodeProduct + tempBanyak;
                }
            }
        }

        public string[] genarateProductAudit(int banyak, string batch)
        {

            string[] temp = new string[banyak];
            for (int i = 0; i < banyak; i++)
            {
                int j = i + 1;
                string product = batch + Control.NUMBER_CODE + j.ToString().PadLeft(5, '0');
                Dictionary<string, string> field = new Dictionary<string, string>();
                field.Add(Control.NAMA_FIELD, product);
                field.Add("isused", "0");
                sql.insertData(field, Control.NAMA_TABLE_TEMP);
                temp[i] = product;
                tambahProduct++;
            }
            sql.Begin();
            return temp;
        }

        public void genarateProductAudit(int banyak)
        {
            //getdata
            if (tambahProduct == 1)
            {
                for (int i = 0; i < banyak; i++)
                {
                    int j = i + 1;
                    string capid = batch + Control.NUMBER_CODE + j.ToString().PadLeft(5, '0');
                    Dictionary<string, string> field = new Dictionary<string, string>();
                    field.Add(Control.NAMA_FIELD, capid);
                    field.Add("isused", "0");
                    sql.insertData(field, Control.NAMA_TABLE_TEMP);
                    tambahProduct++;
                }
                sql.Begin();
            }
        }

        public void genarateProductAuditContinue(int banyak)
        {
            //getdata
            for (int i = 0; i < banyak; i++)
            {
                int j = i + 1;
                string capid = batch + Control.NUMBER_CODE + tambahProduct.ToString().PadLeft(5, '0');
                Dictionary<string, string> field = new Dictionary<string, string>();
                field.Add(Control.NAMA_FIELD, capid);
                field.Add("isused", "0");
                sql.insertData(field, Control.NAMA_TABLE_TEMP);
                tambahProduct++;
            }
            sql.Begin();
        }
        Random random = new Random((int)DateTime.Now.Ticks);
        private string RandomString(int length)
        {
            const string pool = "npqrstuvwxyz";
            var builder = new StringBuilder();

            for (var i = 0; i < length; i++)
            {
                var c = pool[random.Next(0, pool.Length)];
                builder.Append(c);
            }

            return builder.ToString();
        }
        public string RandomString()
        {
            RandomStringGenerator RSG = new RandomStringGenerator(false, true, true, false);
            //  RSG.UniqueStrings = true;
            // Or we can use the constructor
            RSG.LowerCaseCharacters = "npqrstuvwxyz".ToCharArray();
            return RSG.Generate(1);

        }

        public void generateProductTemp(int banyak)
        {
            //getdata
            for (int i = 0; i < banyak; i++)
            {
                int j = i + 1;
                string guid = RandomString(1)+Guid.NewGuid().ToString().Substring(0, 5);
                //string capid = batch + Control.NUMBER_CODE + lastCount.ToString().PadLeft(6, '0');
               // string capid =  lastCount.ToString().PadLeft(6, '0');
                //string capid = batch + Control.NUMBER_CODE +guid;
                Dictionary<string, string> field = new Dictionary<string, string>();
                field.Add(Control.NAMA_FIELD, guid);
                field.Add("isused", "0");
                if (audit)
                {
                    field.Add("flag", "1");
                }
                else
                {
                    field.Add("flag", "0");
                }

                sql.insertData(field, Control.NAMA_TABLE_TEMP);
                lastCount++;
            }
            sql.Begin();
        }


        public void generateProduct(int banyak)
        {
            //getdata
            banyak = banyak * 3;
            List<string> data = create_Guid(banyak);
            data = checkNotExistDb(data);
            saveSqlLite(data);
        }

        private void saveSqlLite(List<string> data)
        {
            for (int i = 0; i < data.Count; i++)
            {
                Dictionary<string, string> field = new Dictionary<string, string>();
                field.Add(Control.NAMA_FIELD, data[i]);
                field.Add("isused", "0");
                if (audit)
                    field.Add("flag", "1");
                else
                    field.Add("flag", "0");
                sql.insertData(field, Control.NAMA_TABLE_TEMP);
            } sql.Begin();
        }

        private List<string> checkNotExistDb(List<string> data)
        {
            List<string> field = new List<string>();
            field.Add(Control.NAMA_FIELD);
            string where = Control.NAMA_FIELD + " in( ";
            for (int i = 0; i < data.Count; i++)
            {
                where += "'" + data[i] + "'";
                if (i + 1 != data.Count)
                    where += ",";
            }
            where += ") and batchnumber ='" + batch + "'";
            List<string[]> res = db.selectList(field, Control.NAMA_TABLE_UTAMA, where);
            foreach (string[] dataTemp in res)
            {
                string dataRemove = dataTemp[0];
                data.Remove(dataRemove);
            }
            return data;
        }

        private List<string> create_Guid(int banyak)
        {
            List<string> datatemp = new List<string>();
            for (int i = 0; i < banyak; i++)
            {
                string guid = Guid.NewGuid().ToString().Substring(0, 5);
                string randdat = RandomString();
                datatemp.Add(randdat + guid);
            }
            return datatemp;
        }
    }
}
