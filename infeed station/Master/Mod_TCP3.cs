﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Windows.Forms;
using System.IO;
using System.Threading;

namespace Infeed_Station
{
    public class tcp_testing3
    {
        private static TcpClient Client;
        private static Byte[] Data = new Byte[256];
        private static Stream ClientStream;
        public static bool IsConnected = false;
        public print pr;
        public bool startTerimaDomino;
        public string IPAddress;
        public string Port;
        public string Timeout;
        public int timer;
        //public Control ctrl;
        private Thread serverThread;
        private Thread serverThread2;
        private Thread serverThreadGs;
        public bool conneect = true;

        internal bool Connect()
        {
            try
            {
               // if (!checkConnected())
                //{
                    Client = new TcpClient();
                    IAsyncResult result = Client.BeginConnect(IPAddress, Int32.Parse(Port), null, null);
                    bool success = result.AsyncWaitHandle.WaitOne(Int32.Parse(Timeout) * 100, true);
                    if (success)
                    {
                        IsConnected = true;
                        return true;
                    }
                    else
                    {
                        IsConnected = false;
                        return false;
                    }

                //}
                //else
               // {
                 //   IsConnected = true;
                   // return true;
                //}
            }
            catch (SocketException ex)
            {
                new Confirm("error +" + ex.ToString());
                if (!ex.NativeErrorCode.Equals(10056))
                    IsConnected = false;
            }
            catch (InvalidOperationException te)
            {
                new Confirm("Printer not connected !");
                IsConnected = false;
            }
            return false;
        
        }

        public bool Connect(string ip , string port, string timeout )
        {
            try
            {
                Client = new TcpClient();
                IAsyncResult result = Client.BeginConnect(ip, Int32.Parse(port), null, null);
                bool success = result.AsyncWaitHandle.WaitOne(Int32.Parse(timeout), true);
                if (success)
                {
                    IsConnected = true;
                    return true;
                }
                else
                {
                    IsConnected = false;
                    return false;
                }
            }
            catch (SocketException ex)
            {
                new Confirm("error +" + ex.ToString());
                if (!ex.NativeErrorCode.Equals(10056))
                    IsConnected = false;
            }
            catch (InvalidOperationException te)
            {
                new Confirm("Printer not connected !");
            }
            return false;
        }

        public bool checkConnected()
        {
            try
            {
                return Connect();
            }
            catch (Exception)
            {
                return false;
            }
        }

        public void sendText(string dataTerima)
        {
            try
            {
                if (IsConnected)
                {
                    ClientStream = Client.GetStream();
                    StreamWriter sw = new StreamWriter(ClientStream);
                    sw.WriteLine(dataTerima);
                    sw.Flush();
                }
            }
            catch (Exception ex)
            {
                IsConnected = false;
                int cekNotConnect = 0;
                while (!IsConnected && conneect)
                {
                    if (cekNotConnect < 5)
                    {
                        if (Connect())
                        {
                            IsConnected = true;
                            sendText(dataTerima);
                        }
                        cekNotConnect++;
                    }
                    else
                    {
                        cekNotConnect = 0;
                        MessageBox.Show("PLC not connected", "Information", MessageBoxButtons.OK);
                    }
                    Thread.Sleep(1000);
                }
            }
        }

        public void send(string dataTerima)
        {
            try
            {
                if (IsConnected)
                {
                    ClientStream = Client.GetStream();
                    StreamWriter sw = new StreamWriter(ClientStream);
                    sw.WriteLine(dataTerima);
                    sw.Flush();
                }
            }
            catch (Exception ex)
            {
                IsConnected = false;
                int cekNotConnect = 0;
                while (!IsConnected && conneect)
                {
                    if (cekNotConnect < 5)
                    {
                       // ctrl.log("Try to reconect [SEND]");
                        if (Connect())
                        {
                            IsConnected = true;
                       //     ctrl.log("Connected to " + IPAddress + ":" + Port);
                            send(dataTerima);
                        }
                        else
                        {
                         //   ctrl.log("Not connected " + IPAddress + ":" + Port);
                        }
                        cekNotConnect++;
                    }
                    else
                    {
                        cekNotConnect = 0;
                        MessageBox.Show("Inkjet not connected", "Information", MessageBoxButtons.OK);
                    }
                    Thread.Sleep(5000);
                }
            }
        }

        public string sendBack()
        {
            try
            {
                byte[] data = new Byte[256];
                if (IsConnected)
                {
                    ClientStream = Client.GetStream();
                    Int32 bytes = ClientStream.Read(data, 0, data.Length);
                    string responseData = System.Text.Encoding.ASCII.GetString(data, 0, bytes);
                    startTerimaDomino = true;
                    startTerimaDomino = true;
                    return responseData;
                }
                return "";
            }
            catch (InvalidOperationException ex)
            {

            }
            return "";
        }

        public string sendBack(string dataTerima)
        {
            try
            {
                byte [] data = new Byte[256];
                if (IsConnected)
                {
                    ClientStream = Client.GetStream();
                    StreamWriter sw = new StreamWriter(ClientStream);
                    sw.WriteLine(dataTerima);
                    sw.Flush();
                    Int32 bytes = ClientStream.Read(data, 0, data.Length);
                    string responseData = System.Text.Encoding.ASCII.GetString(data, 0, bytes);
                    return responseData;
                }
                return "";
            }
            catch (InvalidOperationException ex)
            {

            }
            return "";
        }

        public void disconnect()
        {
            try
            {
                Client.GetStream().Close();
                Client.Close();
            }
            catch (Exception)
            {

            }
        }

        public void dc()
        {
            try
            {
                Client.Close();
            }
            catch (Exception)
            {

            }
        }

        public void updatestatus(string data)
        {
            string dates = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            //pr.txtStatus.AppendText(dates+"\t"+data+"\n");
        }

        internal void closeStream()
        {
            Client.GetStream().Close();
            Client.Close();
        }
   
 
    }
}
