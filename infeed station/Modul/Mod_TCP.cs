﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.IO;
using System.Windows.Forms;
using System.Threading;

namespace Infeed_Station
{
    class Mod_TCP
    {
        private static TcpClient Client;
        private static Byte[] Data = new Byte[256];
        private static Stream ClientStream;
        public const string CHECK_BUFFER = "STX";

        public string IPAddress="";
        public string port="";
        public string timeout="";
        public string statusFail;
        public string dataPrint;
        public Control ctrl;
        internal static bool IsConnected = false;

        internal bool OpenConnection()
        {
            if (checkParameter())
            {
                try
                {
                    Log("Try Open Connection IP " + IPAddress + " Port " + port);
                    Client = new TcpClient();
                    IAsyncResult result = Client.BeginConnect(IPAddress, Int32.Parse(port), null, null);
                    bool success = result.AsyncWaitHandle.WaitOne(Int32.Parse(timeout), true);

                    if (success)
                    {
                        Log("Open Connection Success Port " + port);
                        IsConnected = true;
                        return true;
                    }
                    else
                    {
                        
                        IsConnected = false;
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    Log("Open Connection Failed "+ex.Message);
                    statusFail = ex.Message;
                }
            }
            else
            {
                Log("Configuration Parameter Not Good");
                statusFail = "Configuration Parameter Not Good";
                return false;
            }
            return false;
        }
        
        private bool checkParameter()
        {
            if (IPAddress.Length == 0 || port.Length == 0 || timeout.Length == 0)
            {
                return false;
            }
            return true;
        }

        internal void CloseConnection()
        {
            try
            {
                IsConnected = false;
                ClientStream.Close();
                Client.Close();
            }
            catch (Exception ex)
            {
                Log("Err " + ex.Message);
            }
            
        }

        public void send(string dataTerima)
        {
            try
            {
                if (IsConnected)
                {
                    ClientStream = Client.GetStream();
                    StreamWriter sw = new StreamWriter(ClientStream);
                    sw.WriteLine(dataTerima);
                    sw.Flush();
                }
                else
                {
                    IsConnected = false;
                    int cekNotConnect = 0;
                    while (!IsConnected && conneect)
                    {
                        if (cekNotConnect < 5)
                        {
                            if (OpenConnection())
                            {
                                IsConnected = true;
                                send(dataTerima);
                            }
                            cekNotConnect++;
                        }
                        else
                        {
                            cekNotConnect = 0;
                            MessageBox.Show("Printer Not Connected", "Information", MessageBoxButtons.OK);
                        }
                        Thread.Sleep(1000);
                    }
                }
            }
            catch (Exception ex)
            {
                IsConnected = false;
                int cekNotConnect = 0;
                while (!IsConnected && conneect)
                {
                    if (cekNotConnect < 5)
                    {
                        if (OpenConnection())
                        {
                            IsConnected = true;
                            send(dataTerima);
                        }
                        cekNotConnect++;
                    }
                    else
                    {
                        cekNotConnect = 0;
                        MessageBox.Show("Printer Not Connected", "Information", MessageBoxButtons.OK);
                    }
                    Thread.Sleep(1000);
                }
            }
        }

        internal void Log(String line)
        {
            DateTime now = DateTime.Now;
            string tahun = now.ToString("yyyy");
            string bulan = now.ToString("MM");
            string hari = now.ToString("dd");
            string dates = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            line = dates + "\t" + line;
            // cek file 
            bool cekfile = false;
            string namaFolder = "eventlog\\TCP";
            try
            {
                while (!cekfile)
                {
                    if (Directory.Exists(namaFolder))
                    {
                        if (Directory.Exists(namaFolder + @"\" + tahun))
                        {
                            if (Directory.Exists(namaFolder + @"\" + tahun + @"\" + bulan))
                            {

                                using (StreamWriter outputFile = new StreamWriter(namaFolder + @"\" + tahun + @"\" + bulan + @"\" + hari + ".txt", true))
                                {
                                    outputFile.WriteLine(line);
                                }
                                cekfile = true;
                            }
                            else
                            {

                                Directory.CreateDirectory(namaFolder + @"\" + tahun + @"\" + bulan);
                            }
                        }
                        else
                        {
                            Directory.CreateDirectory(namaFolder + @"\" + tahun);
                        }
                    }
                    else
                    {

                        Directory.CreateDirectory(namaFolder);
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }

        internal string listener()
        {
            try
            {
                byte[] data = new Byte[256];
                if (IsConnected)
                {
                    if (ClientStream == null)
                    {
                        send("cek");
                    }
                    Int32 bytes = ClientStream.Read(data, 0, data.Length);
                    string responseData = System.Text.Encoding.ASCII.GetString(data, 0, bytes);
                    return responseData;
                }
                else
                {
                    OpenConnection();
                }
            }
            catch (Exception ex)
            {
                CloseConnection();
                IsConnected = false;
                Log("Reconnect");
                OpenConnection();
                return "err " + ex.Message;
            }
            return "";
        }

        public bool conneect = true;
        public int timer;
        public void MulaiServer()
        {
            timer = Int32.Parse(ctrl.sqlite.config["delayTime"]);
            while (conneect)
            {
                try
                {
                    // if (GetStatusPrinter())
                    {
                        int QtySend = int.Parse(ctrl.sqlite.config["qtysend"]);
                        string data = getBufferPrinter();
                        //if (data.Length > 0 && data.Contains("SM"))
                        if (data.Length > 0)
                        {
                            int dataIndat = GetBufferData(data);
                            if (dataIndat < QtySend * 2)
                            {
                                ctrl.log("Send to printer");
                                List<string> dataKirim = ctrl.getDataSendToPrinter(QtySend);
                                string temp_port = port;
                                port = "16000";
                                OpenConnection();
                                for (int i = 0; i < QtySend; i++)
                                {
                                    ctrl.log("Send to printer :" + dataKirim[i]);
                                    ctrl.sqlite.delete(Control.NAMA_FIELD + " = '" + dataKirim[i] + "'", Control.NAMA_TABLE_TEMP);
                                    send("^0=MR" + i + " " + dataKirim[i]);
                                }
                                port = temp_port;
                                OpenConnection();

                                ctrl.SetDataGridSend("0", "");
                                ctrl.cekKirimUlang();
                            }
                        }
                    }
                    Thread.Sleep(timer * 1000);
                }
                catch (Exception ex)
                {
                    ctrl.log("Error data from printer");
                }

            }
        }

        private int GetBufferData(string data)
        {
            string[] dataSplit = data.Split(' ');
            return int.Parse(dataSplit[1]);
        }

        private string getBufferPrinter()
        {
            string data = "";
            for (int i = 0; i < 5; i++)
            {
                if (conneect)
                {
                    send(CHECK_BUFFER);
                    data = listener();
                    // if (data.Contains("SM"))
                    {
                        return data;
                    }
                }
                else
                    break;
            }
            return data;

        }

    }



}
