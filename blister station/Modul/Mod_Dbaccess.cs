﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Configuration;
using System.Windows.Forms;
using System.Data;
using System.IO;
using System.Diagnostics;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using System.Net;
using System.Net.Sockets;

namespace Blister_Station
{
    public class dbaccess
    {
        public static SqlConnection Connection;
        public static SqlCommand Command;
        public static SqlDataReader DataReader;
        public static SqlDataAdapter dataadapter;
        public List<string> dataInsert = new List<string>();
        public List<string> dataUpdate = new List<string>();
        public DataTable dataHeader;
        public int num_rows = 0;
        sqlitecs sqlite;
        public string valid;
        string eventtype;
        public bool sysLog = true;
        public string adminid;
        public string adminname;
        public string ipAddress;
        public string eventname;
        public string eventType = "12";
        public string movementType = "6";
        public string from;
        public string uom= "Blisterpack";
        public string to="";
        public string batch;
        public int PackagingqtyOnprogress;
        //public bool isConnect;
        //public bool isOpened;

        public int fcon = 0;
        public string errorMessage;

        public dbaccess()
        {
            sqlite = new sqlitecs();
            Connect();
        }

        public bool Connect()
        {
            //if (!isConnect)
            //{
                try
                {
                    string connec = "Data Source=" + sqlite.config["ipDB"] + ";" +
                                    "Initial Catalog=" + sqlite.config["namaDB"] + ";" +
                                    "User id=" + sqlite.config["usernameDB"] + ";" +
                                    "Password=" + sqlite.config["passDB"] + ";" +
                                    //"Password=docotronics;" +
                                     "Connection Timeout=" + sqlite.config["timeout"] + ";";
                    Connection = new SqlConnection(connec);
                    Connection.Open();
                    fcon = 0;
                    errorMessage = "";
                    return true;
                }
                catch (TimeoutException t)
                {
                    Console.WriteLine("MASUK CATCH 1");
                    fcon++;
                    errorMessage = t.Message;
                    if (fcon < 2)
                    {
                        Console.WriteLine("MASUK CATCH 1 IF");
                        new Confirm("Can not open connection to Database Server.", "Information", MessageBoxButtons.OK);
                        config cfg = new config(null, 0, "");
                        cfg.ShowDialog();
                    }
                }
                catch (SqlException se)
                {
                    Console.WriteLine("MASUK CATCH 2 : " + se.Message);
                    fcon++;
                    Console.WriteLine("FCON : " + fcon);
                    errorMessage = se.Message;
                    if (fcon < 2)
                    {
                        Console.WriteLine("MASUK CATCH 2 IF : " + se.Message);
                        errorMessage = se.Message;
                        new Confirm("Can not open connection to Database Server.", "Information", MessageBoxButtons.OK);
                        config cfg = new config(null, 0, "");
                        cfg.ShowDialog();
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("MASUK CATCH 3");
                    fcon++;
                    errorMessage = ex.Message;
                    if (fcon < 2)
                    {
                        Console.WriteLine("MASUK CATCH 3 IF");
                        new Confirm("Can not open connection to Database Server.", "Information", MessageBoxButtons.OK);
                        simpanLog("Error SQLSERVER : " + ex.ToString());
                        config cfg = new config(null, 0, "");
                        cfg.ShowDialog();
                    }
                }
                return false;
            //}
        }

        public void closeConnection()
        {
            Connection.Close();
        }

        public static List<String[]> LoadProductionOrders()
        {
            List<String[]> Results = new List<String[]>();
            String Query = "SELECT production_order_number, created_date, status FROM transaction_production_order";

            Connection.Open();
            Command = new SqlCommand(Query, Connection);
            DataReader = Command.ExecuteReader();

            while (DataReader.Read())
            {
                String[] Data = new String[] { DataReader.GetValue(0).ToString(), DataReader.GetValue(1).ToString(), DataReader.GetValue(2).ToString() };
                Results.Add(Data);
            }

            DataReader.Close();
            Command.Dispose();
            Connection.Close();
            return Results;
        }

        public bool insert(Dictionary<string, string> field, string table)
        {
            try
            {
                string sql = "INSERT INTO " + table + " (";
                int i = 0;
                foreach (string key in field.Keys)
                {
                    sql += "" + key + "";
                    if (i + 1 < field.Count)
                    {
                        sql += ",";
                        i++;
                    }
                }

                sql += ") values (";

                i = 0;
                foreach (string key in field.Values)
                {
                    sql += "'" + key + "'";
                    if (i + 1 < field.Count)
                    {
                        sql += ",";
                        i++;
                    }
                }
                sql += ")";
                simpanLog(sql);
                eventtype = "0"; 
                if (Connect())
                {
                    Command = new SqlCommand(sql, Connection);
                    Command.ExecuteNonQuery();
                    Connection.Close();
                    return true;
                }
                
            }
            catch (SqlException see)
            {
                Console.WriteLine("MASUK CATCH INSERT 2 : " + see.Message);
            }
            catch (Exception sq)
            {
                Connection.Close();
                simpanLog("Error SQl : " + sq.ToString());
            }
            return false;
        }

        public void insertMovement(List<string> tbl, List<Model.Movement> val, string table)
        {
            try
            {
                string sql = "INSERT INTO " + table + " (";
                for (int i = 0; i < tbl.Count; i++)
                {
                    sql += "" + tbl[i] + "";
                    if (i + 1 < tbl.Count)
                    {
                        sql += ",";
                    }
                }

                sql += ") values ";

                for (int j = 0; j < val.Count; j++)
                {
                    sql += "(";
                    sql += val[j].movementId + ", ";
                    sql += val[j].from + ", ";
                    sql += val[j].movementType + ", ";
                    sql += val[j].to + ", ";
                    sql += val[j].qty + ", ";
                    sql += val[j].uom + ", ";
                    sql += val[j].userId + ", ";
                    sql += val[j].username + ", ";
                    sql += val[j].itemid + ", ";
                    sql += val[j].eventTime + ", ";
                    sql += "(";
                    if (j + 1 < tbl.Count)
                    {
                        sql += ",";
                    }
                }
                simpanLog(sql);
                eventtype = "0";
                Connect();
                Command = new SqlCommand(sql, Connection);
                Command.ExecuteNonQuery();
                Connection.Close();
            }
            catch (Exception sq)
            {
                Connection.Close();
                simpanLog("Error SQl : " + sq.ToString());
            }
        }

        public bool update(Dictionary<string, string> field, string table, string where)
        {
            try
            {
                string sql = "UPDATE " + table + " SET ";
                int i = 0;
                foreach (KeyValuePair<string, string> key in field)
                {
                    if (key.Value.Equals("null"))
                    {
                        sql += key.Key + " = " + key.Value;
                    }
                    else
                    {
                        sql += key.Key + " = " + "'" + key.Value + "'";
                    }
                    if (i + 1 < field.Count)
                    {
                        sql += ",";
                        i++;
                    }
                }

                if (where.Length > 0)
                    sql += " WHERE " + where;

                simpanLog(sql);
                eventtype = "1"; 
                if (Connect())
                {
                    Command = new SqlCommand(sql, Connection);
                    Command.ExecuteNonQuery();
                    Connection.Close();
                    return true;
                }
                
            }
            catch (SqlException see)
            {
                Console.WriteLine("MASUK CATCH INSERT 2 : " + see.Message);
            }
            catch (Exception sq)
            {
                Connection.Close();
                simpanLog("Error SQl : " + sq.ToString());
            }
            return false;
        }

        public DataSet select(List<string> field, string table, string where)
        {
            try
            {
                string sql = "SELECT ";
                for (int j = 0; j < field.Count; j++)
                {
                    //sql += "'" + field[j] + "'";
                    sql += field[j];
                    if (j + 1 < field.Count)
                    {
                        sql += ",";
                        j++;
                    }
                }

                sql += " From " + table;

                if (where.Length > 0)
                    sql += " WHERE " + where;

                simpanLog(sql); 
                Connect();
                dataadapter = new SqlDataAdapter(sql, Connection);
                DataSet Results = new DataSet();
                dataadapter.Fill(Results);
                return Results;
            }
            catch (SqlException sq)
            {
                simpanLog("Error SQLSERVER : " + sq.ToString());
            }
            finally
            {
                try
                {
                    DataReader.Close();
                    Command.Dispose();
                    Connection.Close();
                }
                catch (SqlException sq)
                {
                    simpanLog("Error SQLSERVER : " + sq.ToString());
                }
                catch (NullReferenceException nl)
                {
                    simpanLog("Error SQLSERVER : " + nl.ToString());
                }

            }
            return null;
        }

        public List<String[]> selectList(List<string> field, string table, string where)
        {
            try
            {
                string sql = "SELECT ";
                for (int j = 0; j < field.Count; j++)
                {
                    sql += field[j];
                    if (j + 1 < field.Count)
                    {
                        sql += ",";
                    }
                }
                sql += " From " + table;

                if (where.Length > 0)
                    sql += " WHERE " + where;

                simpanLog(sql);
                eventtype = "1";
                if (Connect())
                {
                    List<String[]> Results = new List<String[]>();
                    Command = new SqlCommand(sql, Connection);
                    DataReader = Command.ExecuteReader();
                    int num = 0;
                    while (DataReader.Read())
                    {
                        string[] data = new string[DataReader.FieldCount];
                        for (int u = 0; u < DataReader.FieldCount; u++)
                        {
                            data[u] = DataReader.GetValue(u).ToString();
                        }

                        Results.Add(data);
                        num++;
                    }
                    num_rows = num;
                    dataHeader = new DataTable();
                    for (int i = 0; i < DataReader.FieldCount; i++)
                    {
                        dataHeader.Columns.Add(DataReader.GetName(i));
                    }

                    return Results;
                }
                else
                {
                    Console.WriteLine("MASUK ELSE NULL");
                    return null;
                }
            }
            catch (SqlException sq)
            {
                var st = new StackTrace(sq, true);
                var frame = st.GetFrame(0);
                string line = frame.ToString();
                simpanLog("Error  00100: " + line);
            }
            catch (NullReferenceException nl)
            {

                var st = new StackTrace(nl, true);
                var frame = st.GetFrame(0);
                string line = frame.ToString();
                simpanLog("Error  00100: " + line);
            }
            catch (InvalidOperationException ide)
            {
                Application.Exit();
            }
            finally
            {
                try
                {
                    DataReader.Close();
                    Command.Dispose();
                }
                catch (SqlException sq)
                {
                    simpanLog("Error SQLSERVER : " + sq.ToString());
                }
                catch (NullReferenceException nl)
                {
                    simpanLog("Error SQLSERVER : " + nl.ToString());
                }

            }
            return null;
        }

        public static List<String> GetPODetails(String PO)
        {
            List<String> Results = new List<String>();
            String Query = String.Format("SELECT production_order_number, product_code, product_name, product_quantity, status FROM transaction_production_order WHERE production_order_number = '{0}'", PO);

            Connection.Open();
            Command = new SqlCommand(Query, Connection);
            DataReader = Command.ExecuteReader();

            while (DataReader.Read())
            {
                for (int i = 0; i < 5; i++)
                {
                    Results.Add(DataReader.GetValue(i).ToString());
                }
            }

            DataReader.Close();
            Command.Dispose();
            return Results;
        }

        public void simpanLog(String line)
        {
            DateTime now = DateTime.Now;
            string tahun = now.ToString("yyyy");
            string bulan = now.ToString("MM");
            string hari = now.ToString("dd");
            string dates = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            line = dates + "\t" + line;
            // cek file 
            bool cekfile = false;
            string namaFolder = "eventlog\\Database";
            try
            {
                while (!cekfile)
                {
                    if (Directory.Exists(namaFolder))
                    {
                        if (Directory.Exists(namaFolder + @"\" + tahun))
                        {
                            if (Directory.Exists(namaFolder + @"\" + tahun + @"\" + bulan))
                            {

                                using (StreamWriter outputFile = new StreamWriter(namaFolder + @"\" + tahun + @"\" + bulan + @"\" + hari + ".txt", true))
                                {
                                    outputFile.WriteLine(line);
                                }
                                cekfile = true;
                            }
                            else
                            {

                                Directory.CreateDirectory(namaFolder + @"\" + tahun + @"\" + bulan);
                            }
                        }
                        else
                        {
                            Directory.CreateDirectory(namaFolder + @"\" + tahun);
                        }
                    }
                    else
                    {

                        Directory.CreateDirectory(namaFolder);
                    }
                }
            }
            catch (Exception ex)
            {
                simpanLog(ex.Message);
            }
        }
    
        public void ErrorLog(String line)
        {
            DateTime now = DateTime.Now;
            string tahun = now.ToString("yyyy");
            string bulan = now.ToString("MM");
            string hari = now.ToString("dd");
            string dates = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            line = dates + "\t" + line;
            // cek file 
            bool cekfile = false;
            string namaFolder = "eventlog\\miss";
            try
            {
                while (!cekfile)
                {
                    if (Directory.Exists(namaFolder))
                    {
                        if (Directory.Exists(namaFolder + @"\" + tahun))
                        {
                            if (Directory.Exists(namaFolder + @"\" + tahun + @"\" + bulan))
                            {

                                using (StreamWriter outputFile = new StreamWriter(namaFolder + @"\" + tahun + @"\" + bulan + @"\" + hari + ".txt", true))
                                {
                                    outputFile.WriteLine(line);
                                }
                                cekfile = true;
                            }
                            else
                            {

                                Directory.CreateDirectory(namaFolder + @"\" + tahun + @"\" + bulan);
                            }
                        }
                        else
                        {
                            Directory.CreateDirectory(namaFolder + @"\" + tahun);
                        }
                    }
                    else
                    {

                        Directory.CreateDirectory(namaFolder);
                    }
                }
            }
            catch (Exception ex)
            {
                simpanLog(ex.Message);
            }
        }

       
        public string md5hash(string source)
        {
            using (MD5 md5Hash = MD5.Create())
            {
                string hash = GetMd5Hash(md5Hash, source);
                return hash;
            }
            return null;
        }

        static string GetMd5Hash(MD5 md5Hash, string input)
        {

            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }

        static bool VerifyMd5Hash(MD5 md5Hash, string input, string hash)
        {
            // Hash the input.
            string hashOfInput = GetMd5Hash(md5Hash, input);

            // Create a StringComparer an compare the hashes.
            StringComparer comparer = StringComparer.OrdinalIgnoreCase;

            if (0 == comparer.Compare(hashOfInput, hash))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void Movement(Dictionary<string, string> field)
        {
            string from = "movement_history";
            DateTime now = DateTime.Now;
            string dates = DateTime.Now.ToString("ssMMddmmHHssyy");
            field.Add("movementId", dates);
            field.Add("eventTime", "" + (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1)).TotalSeconds));
            insert(field, from);
        }


        string iden = "BLI_";
        public void Movement2(string order, List<string> hasilsplit, string to, string qty, string itemid)
        {
            string dates = DateTime.Now.ToString("ssMMddmmHHssyy");
            List<string> tbl = new List<string>();
            tbl.Add("movementId");
            tbl.Add("orderNumber");
            tbl.Add("[from]");
            tbl.Add("movementType");
            tbl.Add("[to]");
            tbl.Add("qty");
            tbl.Add("uom");
            tbl.Add("userid");
            tbl.Add("username");
            tbl.Add("itemid");
            tbl.Add("eventTime");

            List<Model.Movement> val = new List<Model.Movement>();
            for (int i = 0; i < hasilsplit.Count; i++ )
            {
                string guid = Guid.NewGuid().ToString().Substring(0, 10);
                val.Add(new Model.Movement
                {
                    movementId = iden + dates + "_" + guid,
                    orderNumber = order,
                    from = hasilsplit[i],
                    movementType = movementType,
                    to = to,
                    qty = qty,
                    uom = uom,
                    userId = adminid,
                    username = adminname,
                    itemid = "Vaccine -> Blisterpack",
                    eventTime = getUnixString()
                });
            }
            insertMovement(tbl, val, "movement_history");
        }

        public void Movement(string order, string from, string to, string qty, string itemid)
        {
            string dates = DateTime.Now.ToString("ssMMddmmHHssyy");
            Dictionary<string, string> field = new Dictionary<string, string>();
            string guid = Guid.NewGuid().ToString().Substring(0, 10);
            field.Add("movementId", iden + dates + "_" + guid);
            field.Add("orderNumber", order);
            field.Add("[from]", from);
            field.Add("movementType", movementType);
            field.Add("[to]", to);
            field.Add("qty", qty);
            field.Add("uom", uom);
            field.Add("userid", adminid);
            field.Add("username", adminname);
            field.Add("itemid", "Vaccine -> Blisterpack");
            field.Add("eventTime", getUnixString());
            insert(field, "movement_history");
        }

        public string getUnixString()
        {
            string date = "" + (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1)).TotalSeconds);
            return date;
        }

        public string getUnixString(double data)
        {
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(data).ToLocalTime();
            return dtDateTime.ToShortDateString();
        }

        public void insertData(Dictionary<string, string> field, string table)
        {
            try
            {
                string sql = "INSERT INTO " + table + " (";
                int i = 0;
                foreach (string key in field.Keys)
                {
                    sql += "" + key + "";
                    if (i + 1 < field.Count)
                    {
                        sql += ",";
                        i++;
                    }
                }

                sql += ") values (";

                i = 0;
                foreach (string key in field.Values)
                {
                    sql += "'" + key + "'";
                    if (i + 1 < field.Count)
                    {
                        sql += ",";
                        i++;
                    }
                }
                sql += ")";
                simpanLog(sql);
                dataInsert.Add(sql);
            }
            catch (Exception sq)
            {
                simpanLog("Error SQl : " + sq.ToString());
            }
        }

        public void Begin()
        {
            try
            {
                string sql = "BEGIN TRANSACTION;\n ";

                for (int i = 0; i < dataInsert.Count; i++)
                {
                    sql += dataInsert[i] + "\n";
                }
                sql += "COMMIT;";
                simpanLog(sql);
                Connect();
                Command = new SqlCommand(sql, Connection);
                Command.ExecuteNonQuery();
                Connection.Close();
                dataInsert = new List<string>();
            }
            catch (Exception sq)
            {
                ErrorLog(sq.Message);
                dataInsert = new List<string>();
                Connection.Close();

            }
            finally
            {
                dataInsert = new List<string>();
            }
        }
        public bool delayStat;
      
        public void addVaccine(string batch, string line, string productmodel, result sq)
        {
            Dictionary<string, string> field;
            Dictionary<string, string> fieldUpdate;
            List<string> fieldlist = new List<string>();
            fieldlist.Add("DISTINCT " + Control.NAMA_FIELD + ",createdtime,flag,sample");
            List<string[]> res = sq.select(fieldlist, "result", "");
            if (sq.num_rows > 0)
            {
                foreach (string[] data in res)
                {
                    field = new Dictionary<string, string>();
                    field.Add(Control.NAMA_FIELD, data[0]);
                    field.Add("createdtime", data[1]);
                    if (data[2].Equals("0"))
                    {
                        // vaccine sukses
                        field.Add("isreject", "0");
                    }
                    else
                    {
                        field.Add("isreject", "1");
                        field.Add("rejectTime", data[1]);
                    }

                    field.Add("rejectTime", data[1]);
                    field.Add("flag", data[2]);
                    insertData(field, Control.NAMA_TABLE_UTAMA);
                }
                Begin();
                //update inside product
                foreach (string[] data in res)
                {
                    dataVaccineResult(data[0], sq, batch);
                    updatestatus(data[0], sq);
                }
            }
            else
            {

            }
            delayStat = false;
        }

        public void dataVaccineResult(string blisterpackid, result sql , string batch)
        {
            List<string> field = new List<string>();
            field.Add("capid");
            string where = Control.NAMA_FIELD + " ='" + blisterpackid + "'";
            List<string[]> ds = sql.select(field, "result", where);
            Dictionary<string, string> fieldb;
            if (sql.num_rows > 0)
            {
                for (int i = 0; i < ds.Count; i++)
                {
                    fieldb = new Dictionary<string, string>();
                    fieldb.Add(Control.NAMA_FIELD, blisterpackid);
                    fieldb.Add("isreject", "'0'");
                    string wheredb = "capid ='" + ds[i][0] + "' AND " + Control.NAMA_FIELD + " IS NULL";
                    update(fieldb, "vaccine", wheredb);
                    Movement(batch, ds[i][0], blisterpackid, "1", "");
                }
            }
        }

        void updatestatus(string capid, result sq)
        {
            string where = "blisterpackid ='" + capid + "'";
            sq.delete(where, "result");
        }


        public bool validasi(string data, int val)
        {
            //text ip val = 0
            if (val == 0)
                return cekIP(data);
            else if (val == 1)
            {
                //text tidak kosong val =1
                if (data.Length == 0)
                {
                    valid = "Data kosong";
                    return false;
                }
            }
            else if (val == 2)
            {
                //text tidak ada angka
                int errorCounter = Regex.Matches(data, @"[a-zA-Z]").Count;
                if (errorCounter > 0)
                {
                    valid = "data mengandung angka";
                    return false;
                }
            }
            else if (val == 3)
            {
                //text tidak ada angka

                if (data.Length > 6)
                    return false;
                bool rege = Regex.Match(data, @"^[0-9]+$").Success;
                if (!rege)
                {
                    valid = "data mengandung angka";
                    return false;
                }
            }
            return true;

        }

        bool cekIP(string data)
        {
            if (data.Length < 3 & data.Length > 0)
                return false;
            int errorCounter = Regex.Matches(data, @"[a-zA-Z]").Count;
            if (errorCounter == 0)
            {
                string[] dapat = data.Split('.');
                if (dapat.Length != 4)
                {
                    valid = "Error : IP must 4 point";
                    return false;
                }
                if (dapat[3].Equals("0"))
                {
                    valid = "Error : the fourth digit of ip cannot be 0";
                    return false;
                }

                bool kosong = false;
                if (dapat[0].Equals("000"))
                    kosong = true;
                for (int i = 0; i < dapat.Length; i++)
                {
                    //if (dapat[i] == "" || dapat[i].Length <= 0)
                    //{
                    //    new Confirm("Error : IP must 4 point");
                    //    return false;
                    //}
                    if (dapat[i][0].Equals("0"))
                        return false;
                    if (dapat[i].Equals("00"))
                    {
                        valid = "Error : IP cannot fill 00";
                        return false;
                    }

                    bool rege = Regex.Match(dapat[i], @"^[0-9]+$").Success;
                    if (!rege)
                    {
                        valid = "Error : IP cannot contain alphabeth and symbols " + data;
                        return false;
                    }
                    if (dapat[i].Length == 0)
                    {
                        valid = "Error : Cannot empty";
                        return false;
                    }
                    int temp = int.Parse(dapat[i]);
                    if (temp >= 255)
                    {
                        valid = "Error : IP must be less then 255";
                        return false;
                    }
                    if (temp == 0 && kosong)
                    {
                        valid = "Error : First digit of ip cannot be 0";
                        return false;
                    }
                    if (dapat[i].Equals("000") && !kosong)
                    {
                        valid = "Error : IP cannot fill 000";
                        return false;
                    }
                    if (dapat[i][0].Equals('0') && temp != 0 && dapat[i].Length > 1)
                    {
                        valid = "Error : Ip cannot fill '0'";
                        return false;
                    }
                }
                return true;
            }
            else
            {
                valid = "Error : IP cannot contain alphabeth " + errorCounter;
                return false;
            }
            return true;
        }

        public void systemlog()
        {
            Dictionary<string, string> field = new Dictionary<string, string>();
            field.Add("eventtime", getUnixString());
            field.Add("eventname", eventname);
            field.Add("eventtype", eventType);
            field.Add("userid", adminid);
            field.Add("[to]", to);
            field.Add("[from]", from);
            insert(field, "system_log");
        }

        public string[] dataPO(string batch)
        {
            List<string> field = new List<string>();
            field.Add("gtin,expired,packagingqty,model_name,product_model");
            string from = "packaging_order INNER JOIN product_model ON packaging_order.productModelId = product_model.product_model ";
            string where = "batchnumber = '" + batch + "'";
            List<string[]> ds = selectList(field, from, where);
            return ds[0];
        }

        public void updateStatus(string batch)
        {
            Dictionary<string, string> field = new Dictionary<string, string>();
            field.Add("isused", "1");
            string where = "batchnumber='" + batch + "'";
            update(field, Control.NAMA_TABLE_TEMP, where);
        }

        public static string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            throw new Exception("Local IP Address Not Found!");
        }

        public string[] cekLine(string where)
        {
            ipAddress = GetLocalIPAddress();
            List<string> field = new List<string>();
            string[] temp = new string[0];
            field.Add("lineName,linePackaging.linePackagingId");
            string from = "linePackagingDetail INNER JOIN linePackaging ON linePackagingDetail.linePackagingId = linePackaging.linePackagingId";
            //string where1 = where + "= '" + GetLocalIPAddress() + "'";
            string where1 = where + "= '192.168.12.101'";
            List<string[]> ds = selectList(field, from, where1);
            if (num_rows > 0)
                return ds[0];
            else
                return temp;
        }

        public bool decomission(string blister)
        {
            List<string> fieldlist = new List<string>();
            fieldlist.Add("capid");
            selectList(fieldlist, "Vaccine", "(Capid like '%" + blister + "%' or gsOneVialId ='" + blister + "') and isreject = '0'");
            if (num_rows > 0)
            {
                //update vaccine
                simpanLog("Data Vaccine " + blister);
                Dictionary<string, string> field = new Dictionary<string, string>();
                field.Add("blisterpackid", "null");
                field.Add("isreject", "2");
                string where = "(Capid like '%" + blister + "%' or gsOneVialId ='" + blister + "') and isreject = '0'";
                update(field, "Vaccine", where);
                return true;
            }
            else
            {
                fieldlist = new List<string>();
                fieldlist.Add("blisterpackId");
                selectList(fieldlist, "blisterPack", "blisterpackId like '%" + blister + "%' and isreject = '0'");
                if (num_rows > 0)
                {
                    //update blister nya 
                    simpanLog("Data blister " + blister);
                    Dictionary<string, string> field = new Dictionary<string, string>();
                    field.Add("isreject", "2");
                    string where = "blisterpackId = '" + blister + "'";
                    update(field, "blisterPack", where);

                    //update vaccine
                    field = new Dictionary<string, string>();
                    field.Add("blisterpackid", "null");
                    where = "blisterpackId like '%" + blister + "%'";
                    update(field, "Vaccine", where);
                    return true;
                }
                else
                {
                    Confirm cf = new Confirm("Data not found.", "Information", MessageBoxButtons.OK);
                    return false;
                }
            }
        }

        public bool comission(string blister,string blisterid)
        {
            List<string> fieldlist = new List<string>();
            fieldlist.Add("capid");
            selectList(fieldlist, "Vaccine", "(Capid = '" + blister + "' OR gsonevialid = '" + blister + "') and isreject ='0'");
            if (num_rows > 0)
            {
                Dictionary<string, string> field = new Dictionary<string, string>();
                field.Add("blisterpackid", blisterid);
                string where = "(Capid = '" + blister + "' OR gsonevialid = '" + blister + "') and isreject ='0'";
                update(field, "Vaccine", where);
                return true;
            }
            else
            {
                Confirm cf = new Confirm("Data not found.", "Information", MessageBoxButtons.OK);
                return false;
            }
        }

        public bool onprogress(string batch)
        {
            List<string> field = new List<string>();
            field.Add("status,packagingqty");
            string where = "batchnumber = '" + batch + "'";
            List<string[]> ds = selectList(field, "packaging_order", where);
            if (num_rows > 0)
            {
                string status = ds[0][0];
                if (status.Equals("99"))
                    return false;
                else
                    return true;
                PackagingqtyOnprogress = int.Parse(ds[0][1]);
            }
            else
            {
                return false;
            }
        }

        internal int getcountProduct(string batch)
        {
            List<string> field = new List<string>();
            field.Add(Control.NAMA_SEQ);
            string where = "batchnumber = '"+batch+"'";
            List<string[]> ds = selectList(field, "packaging_order", where);
            if (num_rows > 0)
                return int.Parse(ds[0][0]);
            else
                return 0;
        }

        public void updatesequence(string batch, string status)
        {
            Dictionary<string, string> field = new Dictionary<string, string>();
            field.Add(Control.NAMA_SEQ, status);
            string where = "batchnumber = '"+batch+"'";
            update(field, "packaging_order", where);
        }

        public bool sampleData(string blisterid)
        {
            Dictionary<string, string> field = new Dictionary<string, string>();
            //status sample
            field.Add("isReject", "3");
            string where = Control.NAMA_FIELD + " like '%" + blisterid + "%'";
            update(field, Control.NAMA_TABLE_UTAMA, where);
            
            field = new Dictionary<string, string>();
            field.Add("isreject", "3");
            where = "(blisterpackid like '%" + blisterid + "%') and isreject = '0'";
            update(field, "Vaccine", where);
            return true;
        }

        public bool cekRole(string adminid, string module)
        {
            List<string> field = new List<string>();
            field.Add("permission.[module]");
            string from = "[user] INNER JOIN user_role ON [user].role = user_role.role_id INNER JOIN permission ON permission.roleId = user_role.role_id";
            string where = "permission.[read] = '0' AND [user].userId = '"+adminid+"' AND permission.[module] = '"+module+"'";
            List<string[]> ds = selectList(field, from, where);
            if (num_rows > 0)
                return true;
            else
                return false;
        }


        internal string[] dataPO(string batch, string AGGREGATION)
        {
            throw new NotImplementedException();
        }

        internal bool cekPermision(string adminid, string modul_name, string permission)
        {
            List<string> field = new List<string>();
            field.Add("permissions_name");
            string where = "dbo.users.id = '" + adminid + "' AND dbo.permissions.module_name = '" + modul_name + "' AND dbo.permissions.permissions_name = '" + permission + "'";
            string from = "dbo.groups INNER JOIN dbo.permissions ON dbo.groups.id = dbo.permissions.role_id INNER JOIN dbo.users_groups ON dbo.users_groups.group_id = dbo.groups.id  INNER JOIN dbo.users ON dbo.users_groups.user_id = dbo.users.id";
            selectList(field, from, where);
            if (num_rows > 0)
                return true;
            return false;
        }

        public bool validasi(string username)
        {
            try
            {
                if (username.Length > 0)
                {
                    List<string> field = new List<string>();
                    field.Add("id");
                    string where = "username = '" + username + "'";
                    List<string[]> result = selectList(field, "[users]", where);
                    if (result.Count > 0)
                    {
                        return true;
                    }
                    else
                        return false;
                }
                else
                {
                    return false;
                }
            }
            catch (NullReferenceException nl)
            {
                simpanLog("Error  00100: " + nl.Message);
                return false;
            }

        }
      
        public List<string[]> validasi(string username, string password, string status)
        {
            try
            {
                if (username.Length > 0 && password.Length > 0)
                {
                    List<string> field = new List<string>();
                    field.Add("id");
                    string where = "username = '" + username + "' AND password = '" + md5hash(password) + "' AND Active = " + status;
                    List<string[]> result = selectList(field, "[users]", where);
                    if (result.Count > 0)
                    {
                        from = "Bottle Station";
                        adminid = result[0][0];
                        eventtype = "1";
                        eventname = "Login";
                        systemlog();
                        return result;
                    }
                    else
                        return null;
                }
                else
                {
                    string code = "0010";
                    simpanLog(code + " : USERNAME DAN PASSWORD SALAH");
                    return null;
                }
            }
            catch (NullReferenceException nl)
            {
                var st = new StackTrace(nl, true);
                var frame = st.GetFrame(0);
                string line = frame.ToString();
                simpanLog("Error  00100: " + line);
                return null;
            }

        }

        public bool validasi(string username, string password)
        {
            try
            {
                if (username.Length > 0 && password.Length > 0)
                {
                    List<string> field = new List<string>();
                    field.Add("id");
                    string where = "username = '" + username + "' AND password = '" + md5hash(password) + "'";
                    List<string[]> result = selectList(field, "[users]", where);
                    if (result.Count > 0)
                    {
                        return true;
                    }
                    else
                        return false;
                }
                else
                {
                    string code = "0010";
                    simpanLog(code + " : USERNAME DAN PASSWORD SALAH");
                    return false;
                }
            }
            catch (NullReferenceException nl)
            {
                var st = new StackTrace(nl, true);
                var frame = st.GetFrame(0);
                string line = frame.ToString();
                simpanLog("Error  00100: " + line);
                return false;
            }

        }

        //tambahan
        internal List<string[]> getRole(string adminid)
        {
            List<string> field = new List<string>();
            field.Add("first_name, company");
            string where = "id = '" + adminid + "';";
            string from = "dbo.[users]";
            List<string[]> result = selectList(field, from, where);
            Connection.Close();
            if (result.Count > 0)
            {
                return result;
            }
            else
                return null;
        }

        public void insertDecommission(string blister)
        {
            try
            {
                Console.WriteLine("MASUK INSERT DECOMM");
                string sql = "INSERT INTO [Vaccine_Reject] (capId, gsOneVialId, blisterpackId, createdTime, lineNumber, batchNumber, isReject, productModelId, expDate, flag) " +
                                "SELECT capId, gsOneVialId, blisterpackId, createdTime, lineNumber, batchNumber, isReject = 5, productModelId, expDate, flag from [Vaccine] where blisterpackId = '"+blister+"'";
                
                simpanLog(sql);
                eventtype = "0";
                Connect();
                Command = new SqlCommand(sql, Connection);
                Command.ExecuteNonQuery();
                Connection.Close();
            }
            catch (Exception sq)
            {
                Connection.Close();
                simpanLog("Error SQl : " + sq.ToString());
            }
        }

        public int selectCountPass(string batchNumber)
        {
            try
            {
                string sql = "SELECT Count(*) from (select a.blisterpackId from [Vaccine] a where a.batchNumber = '" + batchNumber + "' and a.isReject = 0 and a.blisterpackId is not null group by a.blisterpackId) as m";

                simpanLog(sql);
                Connect();
                Command = new SqlCommand(sql, Connection);
                DataReader = Command.ExecuteReader();
                int Results = 0;
                while (DataReader.Read())
                {
                    string[] data = new string[DataReader.FieldCount];
                    for (int u = 0; u < DataReader.FieldCount; u++)
                    {
                        data[u] = DataReader.GetValue(u).ToString();
                    }

                    Results = Int32.Parse(data[0]);
                }

                return Results;
            }
            catch (SqlException sq)
            {
                simpanLog("Error SQLSERVER : " + sq.ToString());
            }
            finally
            {
                try
                {
                    DataReader.Close();
                    Command.Dispose();
                }
                catch (SqlException sq)
                {
                    simpanLog("Error SQLSERVER : " + sq.ToString());
                }
                catch (NullReferenceException nl)
                {
                    simpanLog("Error SQLSERVER : " + nl.ToString());
                }

            }
            return 0;
        }

        public int selectCountReject(string batchNumber)
        {
            try
            {
                string sql = "SELECT Count(*) from (select a.blisterpackId, a.isReject from [Vaccine_Reject] a where a.batchNumber = '" + batchNumber + "' and a.blisterpackId is not null group by a.blisterpackId, a.isReject) as m";

                simpanLog(sql);
                Connect();
                Command = new SqlCommand(sql, Connection);
                DataReader = Command.ExecuteReader();
                int Results = 0;
                while (DataReader.Read())
                {
                    string[] data = new string[DataReader.FieldCount];
                    for (int u = 0; u < DataReader.FieldCount; u++)
                    {
                        data[u] = DataReader.GetValue(u).ToString();
                    }

                    Results = Int32.Parse(data[0]);
                }

                return Results;
            }
            catch (SqlException sq)
            {
                simpanLog("Error SQLSERVER : " + sq.ToString());
            }
            finally
            {
                try
                {
                    DataReader.Close();
                    Command.Dispose();
                }
                catch (SqlException sq)
                {
                    simpanLog("Error SQLSERVER : " + sq.ToString());
                }
                catch (NullReferenceException nl)
                {
                    simpanLog("Error SQLSERVER : " + nl.ToString());
                }

            }
            return 0;
        }

        public int selectCountVaccine(string batchNumber, string blister)
        {
            try
            {
                string sql = "SELECT Count(*) from [Vaccine] where batchNumber = '" + batchNumber + "' and isReject = 0 and blisterpackId = '"+blister+"'";

                simpanLog(sql);
                if (Connect())
                {
                    Command = new SqlCommand(sql, Connection);
                    DataReader = Command.ExecuteReader();
                    int Results = 0;
                    while (DataReader.Read())
                    {
                        string[] data = new string[DataReader.FieldCount];
                        for (int u = 0; u < DataReader.FieldCount; u++)
                        {
                            data[u] = DataReader.GetValue(u).ToString();
                        }

                        Results = Int32.Parse(data[0]);
                    }
                    Connection.Close();
                    return Results;
                }
            }
            catch (SqlException sq)
            {
                simpanLog("Error SQLSERVER : " + sq.ToString());
            }
            finally
            {
                try
                {
                    DataReader.Close();
                    Command.Dispose();
                }
                catch (SqlException sq)
                {
                    simpanLog("Error SQLSERVER : " + sq.ToString());
                }
                catch (NullReferenceException nl)
                {
                    simpanLog("Error SQLSERVER : " + nl.ToString());
                }

            }
            return 0;
        }

        //end of tambahan

    }
}

