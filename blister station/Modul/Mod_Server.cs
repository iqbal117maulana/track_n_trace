﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Net.Sockets;
using System.Net;

namespace Blister_Station
{
    public class Mod_Server
    {
      //  static List<Listener> listeners = new List<Listener>();
        Control ctrl;
        public bool isRunning = true;

        TcpClient client;
        public TcpListener listener;
        public void Start(Control ctrl1, string port)
        {
            try
            {
                ctrl = ctrl1;
                listener = new TcpListener(IPAddress.Any, int.Parse(port));
                if (ctrl1.db.Connect())
                {
                    listener.Start();

                    while (isRunning) // Add your exit flag here
                    {
                        if (!listener.Pending())
                        {
                            Thread.Sleep(500); // choose a number (in milliseconds) that makes sense
                            continue; // skip to next iteration of loop
                        }
                        client = listener.AcceptTcpClient();
                        ThreadPool.QueueUserWorkItem(ThreadProc, client);
                    }
                }
                else
                {
                    isRunning = false;
                    client.Close();
                    listener.Stop();
                    Console.WriteLine("MASUK LISTENER STOP");
                }

            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("Only one usage"))
                {
                    new Confirm("Port already used:" + port + ", Restart Application", "Information", System.Windows.Forms.MessageBoxButtons.OK);
                    Environment.Exit(Environment.ExitCode);
                }
                else if (ex.Message.Contains("Not listening"))
                {
                    Console.WriteLine("Port not listening, please check Database Connection");
                    //new Confirm("Port not listening, please check Database Connection", "Information", System.Windows.Forms.MessageBoxButtons.OK);
                }
            }
        }

        public void closeServer(string data)
        {
            try
            {
                //isRunning = false;
                //if (client != null)
                //{
                //    client.GetStream().Close();
                //    client.Close();
                //}
                listener.Stop();
            }
            catch
            {
                Console.WriteLine("Cannot Stop Server");
            }
            
        }

        List<string> dataTampung = new List<string>();

        string tampung = "";
        public void clearCache()
        {
            tampung = "";
        }
        private void ThreadProc(object obj)
        {
            string bufferincmessage;
            TcpClient client = (TcpClient)obj;
            NetworkStream clientStream = client.GetStream();
            ctrl.log("Connected ");
            byte[] message = new byte[4096];
            int bytesRead;
            int Counting = 0;
            int countSucces = 0;
            while (isRunning)
            {
                bytesRead = 0;
                try
                {
                    //blocks until a client sends a message

                    ctrl.log("Waiting data");
                    bytesRead = clientStream.Read(message, 0, 4096);
                }
                catch
                {
                    //a socket error has occured
                    break;
                }
                if (bytesRead == 0)
                {
                    //the client has disconnected from the server
                    break;
                }

                //message has successfully been received
                ASCIIEncoding encoder = new ASCIIEncoding();
                bufferincmessage = encoder.GetString(message, 0, bytesRead);

                ctrl.log("Receive : " + bufferincmessage);
                //ctrl.log("Close Connection");
                tampung = tampung + bufferincmessage;
                if (bufferincmessage.Contains('|'))
                {
                    string dataKirim = tampung;
                    if (tampung.Contains('$'))
                    //if (tampung.Contains('?'))
                    {
                        //string[] temp = tampung.Split(new char[] { '?' }, StringSplitOptions.RemoveEmptyEntries);
                        string[] temp = tampung.Split(new char[] { '$' }, StringSplitOptions.RemoveEmptyEntries);
                        if (temp.Length > 1)
                        {
                            dataKirim = temp[0];
                            bool hapusTampung = false;
                            for (int i = 0; i < temp.Length; i++)
                            {
                                if (temp[i].Contains('|'))
                                {
                                    ctrl.ReceiveData(temp[i]);
                                }
                                else
                                {
                                    tampung = "$" + temp[i];
                                    hapusTampung = true;
                                    ctrl.log(tampung);
                                }
                            }

                            if (!hapusTampung)
                            {
                                tampung = "";
                            }
                        }
                        else
                        {
                            ctrl.ReceiveData(tampung);
                            tampung = "";
                        }
                    }
                    else
                    {
                    }
                    Counting = 0;
                }
                else
                {
                    //ctrl.log("Tampung : " + tampung);
                    Counting++;
                }
            }
        

        }
    }
 }