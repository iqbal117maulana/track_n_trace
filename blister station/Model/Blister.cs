﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Blister_Station.Model
{
    class Blister
    {
        public string BLISTERPACKID { get; set; }
        public string INNERBOXID { get; set; }

        public string CREATEDTIME { get; set; }
        public string ISREJECT { get; set; }
        public string REJECTTIME { get; set; }

        public string FLAG { get; set; }
        public string BATCHNUMBER { get; set; }

        public string CAPID { get; set; }
        public string GS1VIALID { get; set; }

        Modul.DBACCESS db = new Modul.DBACCESS();

        public bool CHECK_BLISTER()
        {
            try
            {
                db.FIELD.Add("blisterpackId");
                db.WHERE = "(blisterpackId = '" + BLISTERPACKID + "')";

                if (BATCHNUMBER != null)
                    db.WHERE += " AND BATCHNUMBER = '" + BATCHNUMBER + "'";

                if (ISREJECT != null)
                    db.WHERE += " AND isreject = '" + ISREJECT + "'";

                if (INNERBOXID != null)
                {
                    if (INNERBOXID == "kosong")
                        db.WHERE += " AND innerBoxId is null";
                }
                db.TABLE = "BLISTERPACK";
                db.SELECT();
                if (db.num_rows > 0)
                {
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                Util.Common.simpanLog("Error Blister : " + ex.Message);
                return false;
            }
        }

        public bool CHECK_VACCINE()
        {
            try
            {
                db.FIELD.Add("blisterpackId");
                db.WHERE = "(Capid ='" + CAPID + "' OR gsOneVialId='" + GS1VIALID + "')";

                if (BATCHNUMBER != null)
                    db.WHERE += "AND BATCHNUMBER = '" + BATCHNUMBER + "'";

                if (ISREJECT != null)
                    db.WHERE += "AND isreject = '" + ISREJECT + "'";
                //if (BLISTERPACKID != null)
                //{
                //    if (BLISTERPACKID == "kosong")
                //        db.WHERE += " AND blisterpackId is null";
                //}
                if (INNERBOXID != null)
                {
                    if (INNERBOXID == "kosong")
                        db.WHERE += " AND innerBoxId is null";
                }
                db.TABLE = "Vaccine";
                List<string[]> blstr = db.SELECT();
                if (db.num_rows > 0)
                {
                    for (int i = 0; i < blstr.Count; i++)
                    {
                        BLISTERPACKID = blstr[i][0];
                    }
                    Console.WriteLine("Blister : " + BLISTERPACKID);
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                Util.Common.simpanLog("Error Vaccine : " + ex.Message);
                return false;
            }
        }

        internal bool DECOMMISSION()
        {
            db.PARAMETER.Add("isreject", "2");
            db.WHERE = "(blisterpackId = '" + BLISTERPACKID + "')";
            db.TABLE = "BLISTERPACK";
            if (BATCHNUMBER.Length > 0)
                db.WHERE += "AND BATCHNUMBER = '" + BATCHNUMBER + "'";
            return db.UPDATE();
        }


    }
}
