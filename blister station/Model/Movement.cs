﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Blister_Station.Model
{
    public class Movement
    {
        public string movementId { get; set; }
        public string orderNumber { get; set; }
        public string from { get; set; }
        public string movementType { get; set; }
        public string to { get; set; }
        public string qty { get; set; }
        public string uom { get; set; }
        public string userId { get; set; }
        public string username { get; set; }
        public string itemid { get; set; }
        public string eventTime { get; set; }
    }
}
