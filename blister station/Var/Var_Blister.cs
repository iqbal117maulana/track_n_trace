﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Blister_Station.Var
{
    class Var_Blister
    {
        public static string TABLE = "blisterPack";
        public static List<string> FIELD = new List<string>(new string[] { "blisterpackId", "innerBoxId", "createdTime", "isReject", "rejectTime", "flag", "batchnumber" });
        public string blisterpackId="";
        public string innerBoxId = "";
        public string createdTime = "";
        public string isReject = "";
        public string rejectTime = "";
        public string flag = "";
        public string batchnumber = "";
        public string ErrMSG;
        public int NUM_ROWS;
        public List<Var_Blister> ROWS;

        Dictionary<string, string> FIELD_BLISTER;
        //dbaccess db;
        dbaccess db = new dbaccess();

        public Var_Blister()
        {
            //db = new dbaccess();
            INIT();
        }
        
        void INIT()
        {
            this.FIELD_BLISTER = new Dictionary<string, string>();

        }

        public Var_Blister GetBlisterPack()
        {
            string where="";
            if (this.blisterpackId.Length > 0)
            {
                where = "blisterpackId = '" + blisterpackId + "'";
            }

            if (this.innerBoxId.Length > 0)
            {
                where = "innerBoxId = '" + innerBoxId + "'";
            }
            
            return convertDB(db.selectList(FIELD, TABLE, where));
        }

        public Var_Blister convertDB(List<string[]> data)
        {

            Var_Blister temp = new Var_Blister();
            temp.NUM_ROWS = 0;
            if (data.Count > 0)
            {
                temp.NUM_ROWS = data.Count;
                this.NUM_ROWS = data.Count;
                temp.ROWS = new List<Var_Blister>();
                foreach (string [] row in data)
                {
                    Var_Blister res = new Var_Blister();
                    res.blisterpackId = row[0];
                    res.innerBoxId = row[1];
                    res.createdTime = row[2];
                    res.isReject = row[3];
                    res.rejectTime = row[4];
                    res.flag = row[5];
                    res.batchnumber = row[6];
                    temp.ROWS.Add(res);
                }
                return temp;
            }
            else
            {
                return temp;
            }
        }

        public bool GetExistBlisterPack()
        {
            if (this.blisterpackId.Length > 0)
            {
                this.GetBlisterPack();
                if (this.NUM_ROWS > 0)
                {
                    return true;
                }
                else
                    return false;
            }
            else
            {
                ErrMSG = "BLISTERPACKID NULL";
                return false;
            }
        }

        public bool AddBlisterPack()
        {
            if (!GetExistBlisterPack())
            {
                INIT();
                FIELD_BLISTER.Add("blisterpackid", this.blisterpackId);
                FIELD_BLISTER.Add("createdtime", db.getUnixString());

                if (this.isReject.Length > 0)
                    FIELD_BLISTER.Add("isreject", this.isReject);
                if (this.rejectTime.Length > 0)
                    FIELD_BLISTER.Add("rejecttime", this.rejectTime);
                if (this.batchnumber.Length > 0)
                    FIELD_BLISTER.Add("batchnumber", this.batchnumber);
                if (this.flag.Length > 0)
                    FIELD_BLISTER.Add("flag", this.flag);
                db.insert(FIELD_BLISTER, TABLE);
                return true;
            }
            else
            {
                this.ErrMSG = "BlisterPack Already Exist";
                return false;
            }
        }

        public bool UpdateBlisterPack()
        {
            if (this.blisterpackId.Length > 0)
            {
                if (GetExistBlisterPack())
                {
                    INIT();
                    string where = "blisterpackid ='" + this.blisterpackId + "' and batchnumber='" + this.batchnumber + "'";
                    if (this.innerBoxId.Length > 0)
                    {
                        this.FIELD_BLISTER.Add("innerBoxId", this.blisterpackId);
                    }
                    if (this.isReject.Length > 0)
                    {
                        this.FIELD_BLISTER.Add("isreject", this.isReject);
                        this.FIELD_BLISTER.Add("rejecttime", this.rejectTime);
                    }
                    db.update(this.FIELD_BLISTER, TABLE, where);
                    return true;
                }
                else
                {
                    this.ErrMSG = "BlisterPack is not Exist";
                    return false;
                }
            }
            else
            {
                this.ErrMSG = "BlisterPackID Null";
                return false;
            }
        }

    }
}
