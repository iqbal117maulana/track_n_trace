﻿namespace Blister_Station
{
    partial class config
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtIpServer = new System.Windows.Forms.TextBox();
            this.txtPortServer = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtIpPrinter = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtPortPrinter = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtIpCamera = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtPortCamera = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtPortDb = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtIpDB = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtTimeoutDb = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtAudit = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtPortCamera2 = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtIpCamera2 = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtPortCamera3 = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtIpCamera3 = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtPortCamera4 = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtIpCamera4 = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtNamaDb = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtSend = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtGenerate = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtBagi = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtDelay = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label49 = new System.Windows.Forms.Label();
            this.txtDelayTimer = new System.Windows.Forms.TextBox();
            this.label50 = new System.Windows.Forms.Label();
            this.chkDebug = new System.Windows.Forms.CheckBox();
            this.label47 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.chkBuffer = new System.Windows.Forms.CheckBox();
            this.label45 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.label51 = new System.Windows.Forms.Label();
            this.txtIp_PLC = new System.Windows.Forms.TextBox();
            this.label52 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.txtPort_PLC = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(745, 161);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Ip Server";
            this.label1.Visible = false;
            // 
            // txtIpServer
            // 
            this.txtIpServer.Location = new System.Drawing.Point(836, 158);
            this.txtIpServer.Name = "txtIpServer";
            this.txtIpServer.Size = new System.Drawing.Size(100, 20);
            this.txtIpServer.TabIndex = 1;
            this.txtIpServer.Visible = false;
            // 
            // txtPortServer
            // 
            this.txtPortServer.Location = new System.Drawing.Point(109, 18);
            this.txtPortServer.Name = "txtPortServer";
            this.txtPortServer.Size = new System.Drawing.Size(100, 20);
            this.txtPortServer.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Port Server";
            // 
            // txtIpPrinter
            // 
            this.txtIpPrinter.Location = new System.Drawing.Point(96, 19);
            this.txtIpPrinter.Name = "txtIpPrinter";
            this.txtIpPrinter.Size = new System.Drawing.Size(100, 20);
            this.txtIpPrinter.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(5, 22);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Ip Printer";
            // 
            // txtPortPrinter
            // 
            this.txtPortPrinter.Location = new System.Drawing.Point(96, 45);
            this.txtPortPrinter.Name = "txtPortPrinter";
            this.txtPortPrinter.Size = new System.Drawing.Size(100, 20);
            this.txtPortPrinter.TabIndex = 4;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 48);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(59, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Port Printer";
            // 
            // txtIpCamera
            // 
            this.txtIpCamera.Location = new System.Drawing.Point(103, 18);
            this.txtIpCamera.Name = "txtIpCamera";
            this.txtIpCamera.Size = new System.Drawing.Size(100, 20);
            this.txtIpCamera.TabIndex = 12;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 21);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(64, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "Ip Camera 1";
            // 
            // txtPortCamera
            // 
            this.txtPortCamera.Location = new System.Drawing.Point(836, 6);
            this.txtPortCamera.Name = "txtPortCamera";
            this.txtPortCamera.Size = new System.Drawing.Size(100, 20);
            this.txtPortCamera.TabIndex = 13;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(739, 9);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(74, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "Port Camera 1";
            // 
            // txtPortDb
            // 
            this.txtPortDb.Enabled = false;
            this.txtPortDb.Location = new System.Drawing.Point(99, 47);
            this.txtPortDb.Name = "txtPortDb";
            this.txtPortDb.Size = new System.Drawing.Size(100, 20);
            this.txtPortDb.TabIndex = 7;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(9, 50);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(44, 13);
            this.label8.TabIndex = 27;
            this.label8.Text = "Port DB";
            // 
            // txtIpDB
            // 
            this.txtIpDB.Location = new System.Drawing.Point(99, 21);
            this.txtIpDB.Name = "txtIpDB";
            this.txtIpDB.Size = new System.Drawing.Size(100, 20);
            this.txtIpDB.TabIndex = 6;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(9, 24);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(34, 13);
            this.label9.TabIndex = 25;
            this.label9.Text = "Ip DB";
            // 
            // txtTimeoutDb
            // 
            this.txtTimeoutDb.Enabled = false;
            this.txtTimeoutDb.Location = new System.Drawing.Point(99, 73);
            this.txtTimeoutDb.Name = "txtTimeoutDb";
            this.txtTimeoutDb.Size = new System.Drawing.Size(100, 20);
            this.txtTimeoutDb.TabIndex = 8;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(9, 76);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(63, 13);
            this.label10.TabIndex = 23;
            this.label10.Text = "Timeout DB";
            // 
            // txtAudit
            // 
            this.txtAudit.Location = new System.Drawing.Point(108, 21);
            this.txtAudit.Name = "txtAudit";
            this.txtAudit.Size = new System.Drawing.Size(100, 20);
            this.txtAudit.TabIndex = 19;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 24);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(75, 13);
            this.label11.TabIndex = 29;
            this.label11.Text = "Qty Pre-check";
            // 
            // txtPortCamera2
            // 
            this.txtPortCamera2.Location = new System.Drawing.Point(836, 32);
            this.txtPortCamera2.Name = "txtPortCamera2";
            this.txtPortCamera2.Size = new System.Drawing.Size(100, 20);
            this.txtPortCamera2.TabIndex = 15;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(739, 35);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(71, 13);
            this.label12.TabIndex = 33;
            this.label12.Text = "Port Camera2";
            // 
            // txtIpCamera2
            // 
            this.txtIpCamera2.Location = new System.Drawing.Point(103, 44);
            this.txtIpCamera2.Name = "txtIpCamera2";
            this.txtIpCamera2.Size = new System.Drawing.Size(100, 20);
            this.txtIpCamera2.TabIndex = 14;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(5, 47);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(64, 13);
            this.label13.TabIndex = 31;
            this.label13.Text = "Ip Camera 2";
            // 
            // txtPortCamera3
            // 
            this.txtPortCamera3.Location = new System.Drawing.Point(836, 55);
            this.txtPortCamera3.Name = "txtPortCamera3";
            this.txtPortCamera3.Size = new System.Drawing.Size(100, 20);
            this.txtPortCamera3.TabIndex = 16;
            this.txtPortCamera3.TextChanged += new System.EventHandler(this.textBox3_TextChanged);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(739, 58);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(74, 13);
            this.label14.TabIndex = 37;
            this.label14.Text = "Port Camera 3";
            // 
            // txtIpCamera3
            // 
            this.txtIpCamera3.Location = new System.Drawing.Point(103, 70);
            this.txtIpCamera3.Name = "txtIpCamera3";
            this.txtIpCamera3.Size = new System.Drawing.Size(100, 20);
            this.txtIpCamera3.TabIndex = 15;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(6, 73);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(64, 13);
            this.label15.TabIndex = 35;
            this.label15.Text = "Ip Camera 3";
            // 
            // txtPortCamera4
            // 
            this.txtPortCamera4.Location = new System.Drawing.Point(836, 81);
            this.txtPortCamera4.Name = "txtPortCamera4";
            this.txtPortCamera4.Size = new System.Drawing.Size(100, 20);
            this.txtPortCamera4.TabIndex = 18;
            this.txtPortCamera4.TextChanged += new System.EventHandler(this.textBox5_TextChanged);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(739, 84);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(74, 13);
            this.label16.TabIndex = 41;
            this.label16.Text = "Port Camera 4";
            // 
            // txtIpCamera4
            // 
            this.txtIpCamera4.Location = new System.Drawing.Point(695, 221);
            this.txtIpCamera4.Name = "txtIpCamera4";
            this.txtIpCamera4.Size = new System.Drawing.Size(100, 20);
            this.txtIpCamera4.TabIndex = 17;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(598, 224);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(64, 13);
            this.label17.TabIndex = 39;
            this.label17.Text = "Ip Camera 4";
            // 
            // txtNamaDb
            // 
            this.txtNamaDb.Location = new System.Drawing.Point(99, 99);
            this.txtNamaDb.Name = "txtNamaDb";
            this.txtNamaDb.Size = new System.Drawing.Size(100, 20);
            this.txtNamaDb.TabIndex = 42;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(8, 102);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(53, 13);
            this.label18.TabIndex = 43;
            this.label18.Text = "DB Name";
            // 
            // txtSend
            // 
            this.txtSend.Location = new System.Drawing.Point(108, 48);
            this.txtSend.Name = "txtSend";
            this.txtSend.Size = new System.Drawing.Size(100, 20);
            this.txtSend.TabIndex = 9;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(6, 51);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(51, 13);
            this.label19.TabIndex = 45;
            this.label19.Text = "Qty Send";
            // 
            // txtGenerate
            // 
            this.txtGenerate.Location = new System.Drawing.Point(108, 74);
            this.txtGenerate.Name = "txtGenerate";
            this.txtGenerate.Size = new System.Drawing.Size(100, 20);
            this.txtGenerate.TabIndex = 10;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(6, 77);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(73, 13);
            this.label20.TabIndex = 47;
            this.label20.Text = "Qty Generate ";
            // 
            // txtBagi
            // 
            this.txtBagi.Location = new System.Drawing.Point(836, 107);
            this.txtBagi.Name = "txtBagi";
            this.txtBagi.Size = new System.Drawing.Size(100, 20);
            this.txtBagi.TabIndex = 20;
            this.txtBagi.Visible = false;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(739, 110);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(78, 13);
            this.label21.TabIndex = 49;
            this.label21.Text = "Print Threshold";
            this.label21.Visible = false;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(836, 132);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 11;
            this.textBox1.Visible = false;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(739, 136);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(80, 13);
            this.label22.TabIndex = 51;
            this.label22.Text = "Gen. Threshold";
            this.label22.Visible = false;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(822, 161);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(10, 13);
            this.label23.TabIndex = 52;
            this.label23.Text = ":";
            this.label23.Visible = false;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(93, 22);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(10, 13);
            this.label24.TabIndex = 53;
            this.label24.Text = ":";
            this.label24.Visible = false;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(82, 22);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(10, 13);
            this.label25.TabIndex = 54;
            this.label25.Text = ":";
            this.label25.Visible = false;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(82, 48);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(10, 13);
            this.label26.TabIndex = 55;
            this.label26.Text = ":";
            this.label26.Visible = false;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(86, 76);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(10, 13);
            this.label27.TabIndex = 59;
            this.label27.Text = ":";
            this.label27.Visible = false;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(86, 50);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(10, 13);
            this.label28.TabIndex = 58;
            this.label28.Text = ":";
            this.label28.Visible = false;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(86, 24);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(10, 13);
            this.label29.TabIndex = 57;
            this.label29.Text = ":";
            this.label29.Visible = false;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(816, 136);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(10, 13);
            this.label31.TabIndex = 63;
            this.label31.Text = ":";
            this.label31.Visible = false;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(93, 74);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(10, 13);
            this.label32.TabIndex = 62;
            this.label32.Text = ":";
            this.label32.Visible = false;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(93, 48);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(10, 13);
            this.label33.TabIndex = 61;
            this.label33.Text = ":";
            this.label33.Visible = false;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(85, 102);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(10, 13);
            this.label34.TabIndex = 60;
            this.label34.Text = ":";
            this.label34.Visible = false;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(822, 35);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(10, 13);
            this.label35.TabIndex = 67;
            this.label35.Text = ":";
            this.label35.Visible = false;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(88, 47);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(10, 13);
            this.label36.TabIndex = 66;
            this.label36.Text = ":";
            this.label36.Visible = false;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(822, 9);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(10, 13);
            this.label37.TabIndex = 65;
            this.label37.Text = ":";
            this.label37.Visible = false;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(89, 21);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(10, 13);
            this.label38.TabIndex = 64;
            this.label38.Text = ":";
            this.label38.Visible = false;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(822, 84);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(10, 13);
            this.label39.TabIndex = 71;
            this.label39.Text = ":";
            this.label39.Visible = false;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(681, 224);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(10, 13);
            this.label40.TabIndex = 70;
            this.label40.Text = ":";
            this.label40.Visible = false;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(822, 58);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(10, 13);
            this.label41.TabIndex = 69;
            this.label41.Text = ":";
            this.label41.Visible = false;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(89, 73);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(10, 13);
            this.label42.TabIndex = 68;
            this.label42.Text = ":";
            this.label42.Visible = false;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(822, 110);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(10, 13);
            this.label43.TabIndex = 73;
            this.label43.Text = ":";
            this.label43.Visible = false;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(93, 24);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(10, 13);
            this.label44.TabIndex = 72;
            this.label44.Text = ":";
            this.label44.Visible = false;
            this.label44.Click += new System.EventHandler(this.label44_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(607, 115);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(10, 13);
            this.label3.TabIndex = 76;
            this.label3.Text = ":";
            // 
            // txtDelay
            // 
            this.txtDelay.Location = new System.Drawing.Point(622, 112);
            this.txtDelay.Name = "txtDelay";
            this.txtDelay.Size = new System.Drawing.Size(100, 20);
            this.txtDelay.TabIndex = 74;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(520, 115);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(75, 13);
            this.label30.TabIndex = 75;
            this.label30.Text = "Delay (Minute)";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtPortServer);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label24);
            this.groupBox1.Location = new System.Drawing.Point(238, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(225, 53);
            this.groupBox1.TabIndex = 77;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Port";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label49);
            this.groupBox2.Controls.Add(this.txtDelayTimer);
            this.groupBox2.Controls.Add(this.label50);
            this.groupBox2.Controls.Add(this.chkDebug);
            this.groupBox2.Controls.Add(this.label47);
            this.groupBox2.Controls.Add(this.label48);
            this.groupBox2.Controls.Add(this.txtAudit);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.label19);
            this.groupBox2.Controls.Add(this.txtSend);
            this.groupBox2.Controls.Add(this.label20);
            this.groupBox2.Controls.Add(this.txtGenerate);
            this.groupBox2.Controls.Add(this.label44);
            this.groupBox2.Controls.Add(this.label33);
            this.groupBox2.Controls.Add(this.label32);
            this.groupBox2.Location = new System.Drawing.Point(7, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(225, 162);
            this.groupBox2.TabIndex = 78;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "System";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(96, 124);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(10, 13);
            this.label49.TabIndex = 89;
            this.label49.Text = ":";
            // 
            // txtDelayTimer
            // 
            this.txtDelayTimer.Location = new System.Drawing.Point(111, 124);
            this.txtDelayTimer.Name = "txtDelayTimer";
            this.txtDelayTimer.Size = new System.Drawing.Size(100, 20);
            this.txtDelayTimer.TabIndex = 87;
            this.txtDelayTimer.TextChanged += new System.EventHandler(this.txtDelayTimer_TextChanged);
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(6, 124);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(80, 13);
            this.label50.TabIndex = 88;
            this.label50.Text = "Delay (Second)";
            // 
            // chkDebug
            // 
            this.chkDebug.AutoSize = true;
            this.chkDebug.Location = new System.Drawing.Point(108, 102);
            this.chkDebug.Name = "chkDebug";
            this.chkDebug.Size = new System.Drawing.Size(40, 17);
            this.chkDebug.TabIndex = 86;
            this.chkDebug.Text = "On";
            this.chkDebug.UseVisualStyleBackColor = true;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(96, 103);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(10, 13);
            this.label47.TabIndex = 85;
            this.label47.Text = ":";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(6, 102);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(39, 13);
            this.label48.TabIndex = 84;
            this.label48.Text = "Debug";
            // 
            // chkBuffer
            // 
            this.chkBuffer.AutoSize = true;
            this.chkBuffer.Location = new System.Drawing.Point(622, 89);
            this.chkBuffer.Name = "chkBuffer";
            this.chkBuffer.Size = new System.Drawing.Size(40, 17);
            this.chkBuffer.TabIndex = 83;
            this.chkBuffer.Text = "On";
            this.chkBuffer.UseVisualStyleBackColor = true;
            this.chkBuffer.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(607, 90);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(10, 13);
            this.label45.TabIndex = 82;
            this.label45.Text = ":";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(520, 90);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(35, 13);
            this.label46.TabIndex = 81;
            this.label46.Text = "Buffer";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.txtIpCamera);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.txtIpCamera2);
            this.groupBox3.Controls.Add(this.label15);
            this.groupBox3.Controls.Add(this.txtIpCamera3);
            this.groupBox3.Controls.Add(this.label38);
            this.groupBox3.Controls.Add(this.label36);
            this.groupBox3.Controls.Add(this.label42);
            this.groupBox3.Location = new System.Drawing.Point(7, 180);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(225, 100);
            this.groupBox3.TabIndex = 79;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Camera";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label10);
            this.groupBox4.Controls.Add(this.txtTimeoutDb);
            this.groupBox4.Controls.Add(this.label9);
            this.groupBox4.Controls.Add(this.txtIpDB);
            this.groupBox4.Controls.Add(this.label8);
            this.groupBox4.Controls.Add(this.txtPortDb);
            this.groupBox4.Controls.Add(this.label34);
            this.groupBox4.Controls.Add(this.label29);
            this.groupBox4.Controls.Add(this.label28);
            this.groupBox4.Controls.Add(this.label27);
            this.groupBox4.Controls.Add(this.txtNamaDb);
            this.groupBox4.Controls.Add(this.label18);
            this.groupBox4.Location = new System.Drawing.Point(238, 71);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(225, 137);
            this.groupBox4.TabIndex = 80;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Database";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label4);
            this.groupBox5.Controls.Add(this.txtIpPrinter);
            this.groupBox5.Controls.Add(this.label26);
            this.groupBox5.Controls.Add(this.label5);
            this.groupBox5.Controls.Add(this.label25);
            this.groupBox5.Controls.Add(this.txtPortPrinter);
            this.groupBox5.Location = new System.Drawing.Point(238, 214);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(225, 80);
            this.groupBox5.TabIndex = 84;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Printer";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.label51);
            this.groupBox6.Controls.Add(this.txtIp_PLC);
            this.groupBox6.Controls.Add(this.label52);
            this.groupBox6.Controls.Add(this.label53);
            this.groupBox6.Controls.Add(this.label54);
            this.groupBox6.Controls.Add(this.txtPort_PLC);
            this.groupBox6.Location = new System.Drawing.Point(7, 286);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(225, 80);
            this.groupBox6.TabIndex = 85;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "PLC";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(5, 22);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(40, 13);
            this.label51.TabIndex = 6;
            this.label51.Text = "IP PLC";
            // 
            // txtIp_PLC
            // 
            this.txtIp_PLC.Location = new System.Drawing.Point(96, 19);
            this.txtIp_PLC.Name = "txtIp_PLC";
            this.txtIp_PLC.Size = new System.Drawing.Size(100, 20);
            this.txtIp_PLC.TabIndex = 3;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(82, 48);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(10, 13);
            this.label52.TabIndex = 55;
            this.label52.Text = ":";
            this.label52.Visible = false;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(6, 48);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(60, 13);
            this.label53.TabIndex = 8;
            this.label53.Text = "PORT PLC";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(82, 22);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(10, 13);
            this.label54.TabIndex = 54;
            this.label54.Text = ":";
            this.label54.Visible = false;
            // 
            // txtPort_PLC
            // 
            this.txtPort_PLC.Location = new System.Drawing.Point(96, 45);
            this.txtPort_PLC.Name = "txtPort_PLC";
            this.txtPort_PLC.Size = new System.Drawing.Size(100, 20);
            this.txtPort_PLC.TabIndex = 4;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Image = global::Blister_Station.Properties.Resources.submit;
            this.button1.Location = new System.Drawing.Point(284, 309);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(150, 50);
            this.button1.TabIndex = 21;
            this.button1.Text = "Submit";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // config
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(485, 371);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label43);
            this.Controls.Add(this.label39);
            this.Controls.Add(this.chkBuffer);
            this.Controls.Add(this.label45);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label46);
            this.Controls.Add(this.label41);
            this.Controls.Add(this.txtIpCamera4);
            this.Controls.Add(this.label40);
            this.Controls.Add(this.label35);
            this.Controls.Add(this.label37);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label31);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.txtDelay);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label30);
            this.Controls.Add(this.txtBagi);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.txtPortCamera4);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.txtPortCamera3);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.txtPortCamera2);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.txtPortCamera);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtIpServer);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "config";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Configuration";
            this.Load += new System.EventHandler(this.config_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtIpServer;
        private System.Windows.Forms.TextBox txtPortServer;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtIpPrinter;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtPortPrinter;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtIpCamera;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtPortCamera;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtPortDb;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtIpDB;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtTimeoutDb;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtAudit;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtPortCamera2;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtIpCamera2;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtPortCamera3;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtIpCamera3;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtPortCamera4;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtIpCamera4;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtNamaDb;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtSend;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtGenerate;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtBagi;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtDelay;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.CheckBox chkBuffer;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.CheckBox chkDebug;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.TextBox txtDelayTimer;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.TextBox txtIp_PLC;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.TextBox txtPort_PLC;
    }
}