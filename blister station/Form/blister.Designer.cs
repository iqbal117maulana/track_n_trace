﻿namespace Blister_Station
{
    partial class blister
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(blister));
            this.dgvSend = new System.Windows.Forms.DataGridView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblQtySend = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lblCamera = new System.Windows.Forms.Label();
            this.lblCamera2 = new System.Windows.Forms.Label();
            this.lblCamera3 = new System.Windows.Forms.Label();
            this.lblPrinter = new System.Windows.Forms.Label();
            this.lblAdmin = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lblQuantityReject = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.lblQuantityGood = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.lblQtyNow = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.dgvReceive = new System.Windows.Forms.DataGridView();
            this.lblQuantityReceive = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.lblQtyReceive = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtBatchNumber = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lblTime = new System.Windows.Forms.Label();
            this.lblDate = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblBufferData = new System.Windows.Forms.Label();
            this.lblRole = new System.Windows.Forms.Label();
            this.lblUserId = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.lblProductName = new System.Windows.Forms.Label();
            this.lblLine = new System.Windows.Forms.Label();
            this.lblBatch = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.label15 = new System.Windows.Forms.Label();
            this.lblDb = new System.Windows.Forms.Label();
            this.offDb = new System.Windows.Forms.PictureBox();
            this.onDb = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.offPrinter = new System.Windows.Forms.PictureBox();
            this.offCamera3 = new System.Windows.Forms.PictureBox();
            this.offCamera2 = new System.Windows.Forms.PictureBox();
            this.offCamera1 = new System.Windows.Forms.PictureBox();
            this.onPrinter = new System.Windows.Forms.PictureBox();
            this.onCamera3 = new System.Windows.Forms.PictureBox();
            this.onCamera2 = new System.Windows.Forms.PictureBox();
            this.onCamera1 = new System.Windows.Forms.PictureBox();
            this.btnIndicator = new System.Windows.Forms.Button();
            this.btnLog = new System.Windows.Forms.Button();
            this.btnCommision = new System.Windows.Forms.Button();
            this.btnSample = new System.Windows.Forms.Button();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBoxCamera = new System.Windows.Forms.PictureBox();
            this.btnDeo = new System.Windows.Forms.Button();
            this.btnAudit = new System.Windows.Forms.Button();
            this.btnConfig = new System.Windows.Forms.Button();
            this.btnLogout = new System.Windows.Forms.Button();
            this.btnStart = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSend)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvReceive)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.offDb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.onDb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.offPrinter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.offCamera3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.offCamera2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.offCamera1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.onPrinter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.onCamera3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.onCamera2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.onCamera1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCamera)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvSend
            // 
            this.dgvSend.AllowUserToAddRows = false;
            this.dgvSend.AllowUserToDeleteRows = false;
            this.dgvSend.AllowUserToResizeColumns = false;
            this.dgvSend.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dgvSend.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvSend.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvSend.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvSend.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSend.Location = new System.Drawing.Point(6, 19);
            this.dgvSend.Name = "dgvSend";
            this.dgvSend.ReadOnly = true;
            this.dgvSend.RowHeadersVisible = false;
            this.dgvSend.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvSend.Size = new System.Drawing.Size(257, 624);
            this.dgvSend.TabIndex = 6;
            this.dgvSend.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dgvSend_RowsAdded);
            this.dgvSend.ColumnAdded += new System.Windows.Forms.DataGridViewColumnEventHandler(this.dgvSend_ColumnAdded);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox1.Controls.Add(this.dgvSend);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.groupBox1.Location = new System.Drawing.Point(16, 139);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(269, 652);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Ready";
            // 
            // lblQtySend
            // 
            this.lblQtySend.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblQtySend.AutoSize = true;
            this.lblQtySend.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQtySend.Location = new System.Drawing.Point(291, 59);
            this.lblQtySend.Name = "lblQtySend";
            this.lblQtySend.Size = new System.Drawing.Size(18, 20);
            this.lblQtySend.TabIndex = 91;
            this.lblQtySend.Text = "0";
            this.lblQtySend.Visible = false;
            // 
            // label20
            // 
            this.label20.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(216, 59);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(80, 20);
            this.label20.TabIndex = 90;
            this.label20.Text = "Quantity : ";
            this.label20.Visible = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(15, 107);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(119, 20);
            this.label7.TabIndex = 42;
            this.label7.Text = "Batch Number :";
            this.label7.Click += new System.EventHandler(this.label7_Click);
            // 
            // lblCamera
            // 
            this.lblCamera.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCamera.AutoSize = true;
            this.lblCamera.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCamera.Location = new System.Drawing.Point(954, 355);
            this.lblCamera.Name = "lblCamera";
            this.lblCamera.Size = new System.Drawing.Size(50, 18);
            this.lblCamera.TabIndex = 46;
            this.lblCamera.Text = "Offline";
            // 
            // lblCamera2
            // 
            this.lblCamera2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCamera2.AutoSize = true;
            this.lblCamera2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCamera2.Location = new System.Drawing.Point(954, 440);
            this.lblCamera2.Name = "lblCamera2";
            this.lblCamera2.Size = new System.Drawing.Size(50, 18);
            this.lblCamera2.TabIndex = 48;
            this.lblCamera2.Text = "Offline";
            // 
            // lblCamera3
            // 
            this.lblCamera3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCamera3.AutoSize = true;
            this.lblCamera3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCamera3.Location = new System.Drawing.Point(954, 525);
            this.lblCamera3.Name = "lblCamera3";
            this.lblCamera3.Size = new System.Drawing.Size(50, 18);
            this.lblCamera3.TabIndex = 50;
            this.lblCamera3.Text = "Offline";
            // 
            // lblPrinter
            // 
            this.lblPrinter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblPrinter.AutoSize = true;
            this.lblPrinter.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrinter.Location = new System.Drawing.Point(954, 612);
            this.lblPrinter.Name = "lblPrinter";
            this.lblPrinter.Size = new System.Drawing.Size(50, 18);
            this.lblPrinter.TabIndex = 56;
            this.lblPrinter.Text = "Offline";
            // 
            // lblAdmin
            // 
            this.lblAdmin.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblAdmin.AutoSize = true;
            this.lblAdmin.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAdmin.Location = new System.Drawing.Point(398, 62);
            this.lblAdmin.Name = "lblAdmin";
            this.lblAdmin.Size = new System.Drawing.Size(57, 20);
            this.lblAdmin.TabIndex = 90;
            this.lblAdmin.Text = "admin";
            this.lblAdmin.Visible = false;
            // 
            // label16
            // 
            this.label16.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(884, 633);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(55, 20);
            this.label16.TabIndex = 55;
            this.label16.Text = "Printer";
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(879, 546);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(78, 20);
            this.label10.TabIndex = 49;
            this.label10.Text = "Camera 3";
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(879, 461);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(78, 20);
            this.label8.TabIndex = 47;
            this.label8.Text = "Camera 2";
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(879, 376);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(78, 20);
            this.label5.TabIndex = 45;
            this.label5.Text = "Camera 1";
            // 
            // lblQuantityReject
            // 
            this.lblQuantityReject.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblQuantityReject.AutoSize = true;
            this.lblQuantityReject.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQuantityReject.Location = new System.Drawing.Point(185, 623);
            this.lblQuantityReject.Name = "lblQuantityReject";
            this.lblQuantityReject.Size = new System.Drawing.Size(18, 20);
            this.lblQuantityReject.TabIndex = 95;
            this.lblQuantityReject.Text = "0";
            // 
            // label13
            // 
            this.label13.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(144, 623);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(46, 20);
            this.label13.TabIndex = 94;
            this.label13.Text = "Fail : ";
            // 
            // lblQuantityGood
            // 
            this.lblQuantityGood.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblQuantityGood.AutoSize = true;
            this.lblQuantityGood.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQuantityGood.Location = new System.Drawing.Point(56, 623);
            this.lblQuantityGood.Name = "lblQuantityGood";
            this.lblQuantityGood.Size = new System.Drawing.Size(18, 20);
            this.lblQuantityGood.TabIndex = 93;
            this.lblQuantityGood.Text = "0";
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(6, 623);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(56, 20);
            this.label6.TabIndex = 92;
            this.label6.Text = "Pass : ";
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(379, 5);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(13, 20);
            this.label4.TabIndex = 126;
            this.label4.Text = ":";
            this.label4.Visible = false;
            // 
            // label12
            // 
            this.label12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(270, 8);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(103, 20);
            this.label12.TabIndex = 124;
            this.label12.Text = "Buffer Printer";
            this.label12.Visible = false;
            // 
            // groupBox5
            // 
            this.groupBox5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox5.Controls.Add(this.lblQtyNow);
            this.groupBox5.Controls.Add(this.label18);
            this.groupBox5.Controls.Add(this.lblQuantityGood);
            this.groupBox5.Controls.Add(this.lblQuantityReject);
            this.groupBox5.Controls.Add(this.label6);
            this.groupBox5.Controls.Add(this.dgvReceive);
            this.groupBox5.Controls.Add(this.label13);
            this.groupBox5.Controls.Add(this.lblQuantityReceive);
            this.groupBox5.Controls.Add(this.label22);
            this.groupBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox5.Location = new System.Drawing.Point(291, 139);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(565, 652);
            this.groupBox5.TabIndex = 94;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Pass";
            // 
            // lblQtyNow
            // 
            this.lblQtyNow.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblQtyNow.AutoSize = true;
            this.lblQtyNow.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQtyNow.Location = new System.Drawing.Point(496, 623);
            this.lblQtyNow.Name = "lblQtyNow";
            this.lblQtyNow.Size = new System.Drawing.Size(18, 20);
            this.lblQtyNow.TabIndex = 97;
            this.lblQtyNow.Text = "0";
            // 
            // label18
            // 
            this.label18.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(422, 623);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(80, 20);
            this.label18.TabIndex = 96;
            this.label18.Text = "Qty Now : ";
            // 
            // dgvReceive
            // 
            this.dgvReceive.AllowUserToAddRows = false;
            this.dgvReceive.AllowUserToDeleteRows = false;
            this.dgvReceive.AllowUserToResizeColumns = false;
            this.dgvReceive.AllowUserToResizeRows = false;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dgvReceive.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvReceive.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvReceive.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvReceive.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvReceive.Location = new System.Drawing.Point(6, 19);
            this.dgvReceive.Name = "dgvReceive";
            this.dgvReceive.ReadOnly = true;
            this.dgvReceive.RowHeadersVisible = false;
            this.dgvReceive.Size = new System.Drawing.Size(553, 600);
            this.dgvReceive.TabIndex = 7;
            this.dgvReceive.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dgvReceive_RowsAdded);
            // 
            // lblQuantityReceive
            // 
            this.lblQuantityReceive.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblQuantityReceive.AutoSize = true;
            this.lblQuantityReceive.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQuantityReceive.Location = new System.Drawing.Point(342, 623);
            this.lblQuantityReceive.Name = "lblQuantityReceive";
            this.lblQuantityReceive.Size = new System.Drawing.Size(18, 20);
            this.lblQuantityReceive.TabIndex = 93;
            this.lblQuantityReceive.Text = "0";
            // 
            // label22
            // 
            this.label22.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(279, 623);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(66, 20);
            this.label22.TabIndex = 92;
            this.label22.Text = "All Qty : ";
            // 
            // lblQtyReceive
            // 
            this.lblQtyReceive.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblQtyReceive.AutoSize = true;
            this.lblQtyReceive.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQtyReceive.Location = new System.Drawing.Point(755, 143);
            this.lblQtyReceive.Name = "lblQtyReceive";
            this.lblQtyReceive.Size = new System.Drawing.Size(18, 20);
            this.lblQtyReceive.TabIndex = 93;
            this.lblQtyReceive.Text = "0";
            this.lblQtyReceive.Visible = false;
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(651, 143);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(80, 20);
            this.label9.TabIndex = 92;
            this.label9.Text = "Quantity : ";
            this.label9.Visible = false;
            // 
            // txtBatchNumber
            // 
            this.txtBatchNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBatchNumber.Location = new System.Drawing.Point(140, 104);
            this.txtBatchNumber.Name = "txtBatchNumber";
            this.txtBatchNumber.ReadOnly = true;
            this.txtBatchNumber.Size = new System.Drawing.Size(192, 26);
            this.txtBatchNumber.TabIndex = 130;
            this.txtBatchNumber.Click += new System.EventHandler(this.txtBatchNumber_Click);
            // 
            // label17
            // 
            this.label17.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.White;
            this.label17.Location = new System.Drawing.Point(328, 25);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(229, 37);
            this.label17.TabIndex = 88;
            this.label17.Text = "Blister Station";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(47)))), ((int)(((byte)(54)))));
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(210, 86);
            this.panel2.TabIndex = 0;
            // 
            // lblTime
            // 
            this.lblTime.AutoSize = true;
            this.lblTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTime.ForeColor = System.Drawing.Color.White;
            this.lblTime.Location = new System.Drawing.Point(216, 20);
            this.lblTime.Name = "lblTime";
            this.lblTime.Size = new System.Drawing.Size(39, 15);
            this.lblTime.TabIndex = 116;
            this.lblTime.Text = "Time";
            // 
            // lblDate
            // 
            this.lblDate.AutoSize = true;
            this.lblDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDate.ForeColor = System.Drawing.Color.White;
            this.lblDate.Location = new System.Drawing.Point(216, 38);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(33, 15);
            this.lblDate.TabIndex = 117;
            this.lblDate.Text = "Date";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(148)))), ((int)(((byte)(172)))));
            this.panel1.Controls.Add(this.lblQtySend);
            this.panel1.Controls.Add(this.lblBufferData);
            this.panel1.Controls.Add(this.label20);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.lblAdmin);
            this.panel1.Controls.Add(this.lblRole);
            this.panel1.Controls.Add(this.lblUserId);
            this.panel1.Controls.Add(this.pictureBox3);
            this.panel1.Controls.Add(this.lblProductName);
            this.panel1.Controls.Add(this.lblLine);
            this.panel1.Controls.Add(this.lblBatch);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label19);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.lblDate);
            this.panel1.Controls.Add(this.lblTime);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.label17);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1024, 86);
            this.panel1.TabIndex = 127;
            // 
            // lblBufferData
            // 
            this.lblBufferData.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblBufferData.AutoSize = true;
            this.lblBufferData.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBufferData.Location = new System.Drawing.Point(398, 5);
            this.lblBufferData.Name = "lblBufferData";
            this.lblBufferData.Size = new System.Drawing.Size(18, 20);
            this.lblBufferData.TabIndex = 133;
            this.lblBufferData.Text = "0";
            this.lblBufferData.Visible = false;
            // 
            // lblRole
            // 
            this.lblRole.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblRole.AutoSize = true;
            this.lblRole.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRole.ForeColor = System.Drawing.Color.White;
            this.lblRole.Location = new System.Drawing.Point(931, 44);
            this.lblRole.Name = "lblRole";
            this.lblRole.Size = new System.Drawing.Size(52, 20);
            this.lblRole.TabIndex = 144;
            this.lblRole.Text = "admin";
            // 
            // lblUserId
            // 
            this.lblUserId.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblUserId.AutoSize = true;
            this.lblUserId.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUserId.ForeColor = System.Drawing.Color.White;
            this.lblUserId.Location = new System.Drawing.Point(930, 24);
            this.lblUserId.Name = "lblUserId";
            this.lblUserId.Size = new System.Drawing.Size(57, 20);
            this.lblUserId.TabIndex = 143;
            this.lblUserId.Text = "admin";
            // 
            // pictureBox3
            // 
            this.pictureBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox3.Image = global::Blister_Station.Properties.Resources.user;
            this.pictureBox3.Location = new System.Drawing.Point(879, 20);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(45, 45);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox3.TabIndex = 142;
            this.pictureBox3.TabStop = false;
            // 
            // lblProductName
            // 
            this.lblProductName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblProductName.AutoSize = true;
            this.lblProductName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProductName.ForeColor = System.Drawing.Color.White;
            this.lblProductName.Location = new System.Drawing.Point(738, 53);
            this.lblProductName.Name = "lblProductName";
            this.lblProductName.Size = new System.Drawing.Size(110, 20);
            this.lblProductName.TabIndex = 141;
            this.lblProductName.Text = "Product Name";
            // 
            // lblLine
            // 
            this.lblLine.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblLine.AutoSize = true;
            this.lblLine.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLine.ForeColor = System.Drawing.Color.White;
            this.lblLine.Location = new System.Drawing.Point(738, 11);
            this.lblLine.Name = "lblLine";
            this.lblLine.Size = new System.Drawing.Size(39, 20);
            this.lblLine.TabIndex = 140;
            this.lblLine.Text = "Line";
            // 
            // lblBatch
            // 
            this.lblBatch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblBatch.AutoSize = true;
            this.lblBatch.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBatch.ForeColor = System.Drawing.Color.White;
            this.lblBatch.Location = new System.Drawing.Point(738, 33);
            this.lblBatch.Name = "lblBatch";
            this.lblBatch.Size = new System.Drawing.Size(111, 20);
            this.lblBatch.TabIndex = 139;
            this.lblBatch.Text = "Batch Number";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(602, 33);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(111, 20);
            this.label2.TabIndex = 133;
            this.label2.Text = "Batch Number";
            // 
            // label19
            // 
            this.label19.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.White;
            this.label19.Location = new System.Drawing.Point(729, 53);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(13, 20);
            this.label19.TabIndex = 138;
            this.label19.Text = ":";
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(602, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(117, 20);
            this.label1.TabIndex = 134;
            this.label1.Text = "Line Packaging";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(729, 11);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(13, 20);
            this.label3.TabIndex = 137;
            this.label3.Text = ":";
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.White;
            this.label11.Location = new System.Drawing.Point(602, 53);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(110, 20);
            this.label11.TabIndex = 135;
            this.label11.Text = "Product Name";
            // 
            // label14
            // 
            this.label14.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.White;
            this.label14.Location = new System.Drawing.Point(729, 33);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(13, 20);
            this.label14.TabIndex = 136;
            this.label14.Text = ":";
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // label15
            // 
            this.label15.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(888, 721);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(79, 20);
            this.label15.TabIndex = 143;
            this.label15.Text = "Database";
            // 
            // lblDb
            // 
            this.lblDb.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblDb.AutoSize = true;
            this.lblDb.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDb.Location = new System.Drawing.Point(954, 700);
            this.lblDb.Name = "lblDb";
            this.lblDb.Size = new System.Drawing.Size(50, 18);
            this.lblDb.TabIndex = 144;
            this.lblDb.Text = "Offline";
            // 
            // offDb
            // 
            this.offDb.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.offDb.Image = global::Blister_Station.Properties.Resources.off1;
            this.offDb.Location = new System.Drawing.Point(957, 683);
            this.offDb.Name = "offDb";
            this.offDb.Size = new System.Drawing.Size(14, 14);
            this.offDb.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.offDb.TabIndex = 146;
            this.offDb.TabStop = false;
            // 
            // onDb
            // 
            this.onDb.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.onDb.Image = global::Blister_Station.Properties.Resources.on1;
            this.onDb.Location = new System.Drawing.Point(957, 683);
            this.onDb.Name = "onDb";
            this.onDb.Size = new System.Drawing.Size(14, 14);
            this.onDb.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.onDb.TabIndex = 145;
            this.onDb.TabStop = false;
            this.onDb.Visible = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox5.Image = global::Blister_Station.Properties.Resources.dtbs;
            this.pictureBox5.Location = new System.Drawing.Point(888, 668);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(55, 50);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox5.TabIndex = 142;
            this.pictureBox5.TabStop = false;
            // 
            // offPrinter
            // 
            this.offPrinter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.offPrinter.Image = global::Blister_Station.Properties.Resources.off1;
            this.offPrinter.Location = new System.Drawing.Point(957, 595);
            this.offPrinter.Name = "offPrinter";
            this.offPrinter.Size = new System.Drawing.Size(14, 14);
            this.offPrinter.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.offPrinter.TabIndex = 141;
            this.offPrinter.TabStop = false;
            // 
            // offCamera3
            // 
            this.offCamera3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.offCamera3.Image = global::Blister_Station.Properties.Resources.off1;
            this.offCamera3.Location = new System.Drawing.Point(957, 508);
            this.offCamera3.Name = "offCamera3";
            this.offCamera3.Size = new System.Drawing.Size(14, 14);
            this.offCamera3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.offCamera3.TabIndex = 140;
            this.offCamera3.TabStop = false;
            // 
            // offCamera2
            // 
            this.offCamera2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.offCamera2.Image = global::Blister_Station.Properties.Resources.off1;
            this.offCamera2.Location = new System.Drawing.Point(957, 423);
            this.offCamera2.Name = "offCamera2";
            this.offCamera2.Size = new System.Drawing.Size(14, 14);
            this.offCamera2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.offCamera2.TabIndex = 139;
            this.offCamera2.TabStop = false;
            // 
            // offCamera1
            // 
            this.offCamera1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.offCamera1.Image = global::Blister_Station.Properties.Resources.off1;
            this.offCamera1.Location = new System.Drawing.Point(957, 338);
            this.offCamera1.Name = "offCamera1";
            this.offCamera1.Size = new System.Drawing.Size(14, 14);
            this.offCamera1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.offCamera1.TabIndex = 138;
            this.offCamera1.TabStop = false;
            // 
            // onPrinter
            // 
            this.onPrinter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.onPrinter.Image = global::Blister_Station.Properties.Resources.on1;
            this.onPrinter.Location = new System.Drawing.Point(957, 595);
            this.onPrinter.Name = "onPrinter";
            this.onPrinter.Size = new System.Drawing.Size(14, 14);
            this.onPrinter.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.onPrinter.TabIndex = 137;
            this.onPrinter.TabStop = false;
            this.onPrinter.Visible = false;
            // 
            // onCamera3
            // 
            this.onCamera3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.onCamera3.Image = global::Blister_Station.Properties.Resources.on1;
            this.onCamera3.Location = new System.Drawing.Point(957, 508);
            this.onCamera3.Name = "onCamera3";
            this.onCamera3.Size = new System.Drawing.Size(14, 14);
            this.onCamera3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.onCamera3.TabIndex = 136;
            this.onCamera3.TabStop = false;
            this.onCamera3.Visible = false;
            // 
            // onCamera2
            // 
            this.onCamera2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.onCamera2.Image = global::Blister_Station.Properties.Resources.on1;
            this.onCamera2.Location = new System.Drawing.Point(957, 423);
            this.onCamera2.Name = "onCamera2";
            this.onCamera2.Size = new System.Drawing.Size(14, 14);
            this.onCamera2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.onCamera2.TabIndex = 135;
            this.onCamera2.TabStop = false;
            this.onCamera2.Visible = false;
            // 
            // onCamera1
            // 
            this.onCamera1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.onCamera1.Image = global::Blister_Station.Properties.Resources.on1;
            this.onCamera1.Location = new System.Drawing.Point(957, 338);
            this.onCamera1.Name = "onCamera1";
            this.onCamera1.Size = new System.Drawing.Size(14, 14);
            this.onCamera1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.onCamera1.TabIndex = 134;
            this.onCamera1.TabStop = false;
            this.onCamera1.Visible = false;
            // 
            // btnIndicator
            // 
            this.btnIndicator.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnIndicator.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnIndicator.Image = global::Blister_Station.Properties.Resources.indicator;
            this.btnIndicator.Location = new System.Drawing.Point(866, 751);
            this.btnIndicator.Name = "btnIndicator";
            this.btnIndicator.Size = new System.Drawing.Size(150, 40);
            this.btnIndicator.TabIndex = 132;
            this.btnIndicator.Text = "Indicator";
            this.btnIndicator.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnIndicator.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnIndicator.UseVisualStyleBackColor = true;
            this.btnIndicator.Click += new System.EventHandler(this.btnIndicator_Click);
            // 
            // btnLog
            // 
            this.btnLog.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLog.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLog.Image = global::Blister_Station.Properties.Resources.log;
            this.btnLog.Location = new System.Drawing.Point(864, 231);
            this.btnLog.Name = "btnLog";
            this.btnLog.Size = new System.Drawing.Size(150, 40);
            this.btnLog.TabIndex = 131;
            this.btnLog.Text = "Log";
            this.btnLog.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnLog.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnLog.UseVisualStyleBackColor = true;
            this.btnLog.Click += new System.EventHandler(this.btnLog_Click);
            // 
            // btnCommision
            // 
            this.btnCommision.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCommision.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCommision.Image = global::Blister_Station.Properties.Resources.comi;
            this.btnCommision.Location = new System.Drawing.Point(862, 93);
            this.btnCommision.Name = "btnCommision";
            this.btnCommision.Size = new System.Drawing.Size(150, 40);
            this.btnCommision.TabIndex = 122;
            this.btnCommision.Text = "Commission";
            this.btnCommision.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCommision.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCommision.UseVisualStyleBackColor = true;
            this.btnCommision.Click += new System.EventHandler(this.button1_Click_3);
            // 
            // btnSample
            // 
            this.btnSample.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSample.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSample.Image = global::Blister_Station.Properties.Resources.sample;
            this.btnSample.Location = new System.Drawing.Point(706, 93);
            this.btnSample.Name = "btnSample";
            this.btnSample.Size = new System.Drawing.Size(150, 40);
            this.btnSample.TabIndex = 121;
            this.btnSample.Text = "Sample";
            this.btnSample.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSample.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSample.UseVisualStyleBackColor = true;
            this.btnSample.Click += new System.EventHandler(this.button1_Click_2);
            // 
            // pictureBox6
            // 
            this.pictureBox6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox6.ImageLocation = "genie-nano-camera.png";
            this.pictureBox6.Location = new System.Drawing.Point(888, 493);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(55, 50);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox6.TabIndex = 116;
            this.pictureBox6.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox4.ImageLocation = "libringer.jpg";
            this.pictureBox4.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox4.InitialImage")));
            this.pictureBox4.Location = new System.Drawing.Point(888, 580);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(55, 50);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox4.TabIndex = 114;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox2.ImageLocation = "genie-nano-camera.png";
            this.pictureBox2.Location = new System.Drawing.Point(888, 408);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(55, 50);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 112;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBoxCamera
            // 
            this.pictureBoxCamera.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBoxCamera.ImageLocation = "genie-nano-camera.png";
            this.pictureBoxCamera.Location = new System.Drawing.Point(888, 323);
            this.pictureBoxCamera.Name = "pictureBoxCamera";
            this.pictureBoxCamera.Size = new System.Drawing.Size(55, 50);
            this.pictureBoxCamera.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxCamera.TabIndex = 111;
            this.pictureBoxCamera.TabStop = false;
            // 
            // btnDeo
            // 
            this.btnDeo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDeo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeo.Image = global::Blister_Station.Properties.Resources.deco;
            this.btnDeo.Location = new System.Drawing.Point(862, 138);
            this.btnDeo.Name = "btnDeo";
            this.btnDeo.Size = new System.Drawing.Size(150, 40);
            this.btnDeo.TabIndex = 91;
            this.btnDeo.Text = "Decommission";
            this.btnDeo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDeo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnDeo.UseVisualStyleBackColor = true;
            this.btnDeo.Click += new System.EventHandler(this.btnDeo_Click);
            // 
            // btnAudit
            // 
            this.btnAudit.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAudit.Image = global::Blister_Station.Properties.Resources.pre_check;
            this.btnAudit.Location = new System.Drawing.Point(338, 92);
            this.btnAudit.Name = "btnAudit";
            this.btnAudit.Size = new System.Drawing.Size(150, 40);
            this.btnAudit.TabIndex = 89;
            this.btnAudit.Text = "Pre-Check";
            this.btnAudit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAudit.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnAudit.UseVisualStyleBackColor = true;
            this.btnAudit.Click += new System.EventHandler(this.button4_Click);
            // 
            // btnConfig
            // 
            this.btnConfig.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnConfig.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConfig.Image = global::Blister_Station.Properties.Resources.config;
            this.btnConfig.Location = new System.Drawing.Point(863, 185);
            this.btnConfig.Name = "btnConfig";
            this.btnConfig.Size = new System.Drawing.Size(150, 40);
            this.btnConfig.TabIndex = 85;
            this.btnConfig.Text = "Config";
            this.btnConfig.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnConfig.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnConfig.UseVisualStyleBackColor = true;
            this.btnConfig.Click += new System.EventHandler(this.button3_Click);
            // 
            // btnLogout
            // 
            this.btnLogout.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLogout.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLogout.Image = global::Blister_Station.Properties.Resources.logout;
            this.btnLogout.Location = new System.Drawing.Point(864, 277);
            this.btnLogout.Name = "btnLogout";
            this.btnLogout.Size = new System.Drawing.Size(150, 40);
            this.btnLogout.TabIndex = 84;
            this.btnLogout.Text = "Logout";
            this.btnLogout.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnLogout.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnLogout.UseVisualStyleBackColor = true;
            this.btnLogout.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnStart
            // 
            this.btnStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStart.Image = global::Blister_Station.Properties.Resources.start;
            this.btnStart.Location = new System.Drawing.Point(494, 92);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(150, 40);
            this.btnStart.TabIndex = 43;
            this.btnStart.Text = "Start";
            this.btnStart.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnStart.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.button2_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(47)))), ((int)(((byte)(54)))));
            this.pictureBox1.ImageLocation = "logo_biofarma.png";
            this.pictureBox1.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.InitialImage")));
            this.pictureBox1.Location = new System.Drawing.Point(29, 4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(150, 75);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 14;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.DoubleClick += new System.EventHandler(this.pictureBox1_DoubleClick);
            this.pictureBox1.MouseLeave += new System.EventHandler(this.pictureBox1_MouseLeave);
            this.pictureBox1.MouseEnter += new System.EventHandler(this.pictureBox1_MouseEnter);
            // 
            // blister
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1024, 801);
            this.Controls.Add(this.offDb);
            this.Controls.Add(this.onDb);
            this.Controls.Add(this.lblDb);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.offPrinter);
            this.Controls.Add(this.offCamera3);
            this.Controls.Add(this.offCamera2);
            this.Controls.Add(this.offCamera1);
            this.Controls.Add(this.onPrinter);
            this.Controls.Add(this.onCamera3);
            this.Controls.Add(this.onCamera2);
            this.Controls.Add(this.onCamera1);
            this.Controls.Add(this.btnIndicator);
            this.Controls.Add(this.btnLog);
            this.Controls.Add(this.txtBatchNumber);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.lblQtyReceive);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.btnCommision);
            this.Controls.Add(this.btnSample);
            this.Controls.Add(this.pictureBox6);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBoxCamera);
            this.Controls.Add(this.btnDeo);
            this.Controls.Add(this.btnAudit);
            this.Controls.Add(this.btnConfig);
            this.Controls.Add(this.btnLogout);
            this.Controls.Add(this.lblPrinter);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.lblCamera3);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.lblCamera2);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.lblCamera);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "blister";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Blister Station";
            this.TransparencyKey = System.Drawing.Color.AntiqueWhite;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.blister_Load);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.blister_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.dgvSend)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvReceive)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.offDb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.onDb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.offPrinter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.offCamera3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.offCamera2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.offCamera1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.onPrinter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.onCamera3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.onCamera2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.onCamera1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCamera)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBoxCamera;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label5;
        public System.Windows.Forms.DataGridView dgvSend;
        public System.Windows.Forms.Button btnStart;
        public System.Windows.Forms.Button btnConfig;
        public System.Windows.Forms.Button btnLogout;
        public System.Windows.Forms.Button btnAudit;
        public System.Windows.Forms.Button btnDeo;
        public System.Windows.Forms.Label lblQtySend;
        public System.Windows.Forms.Label lblAdmin;
        public System.Windows.Forms.Label lblCamera;
        public System.Windows.Forms.Label lblCamera2;
        public System.Windows.Forms.Label lblCamera3;
        public System.Windows.Forms.Label lblPrinter;
        public System.Windows.Forms.Button btnSample;
        public System.Windows.Forms.Button btnCommision;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label12;
        public System.Windows.Forms.Label lblQuantityReject;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.GroupBox groupBox5;
        public System.Windows.Forms.DataGridView dgvReceive;
        public System.Windows.Forms.Label lblQuantityReceive;
        public System.Windows.Forms.Label lblQtyReceive;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label9;
        public System.Windows.Forms.Label lblQuantityGood;
        public System.Windows.Forms.TextBox txtBatchNumber;
        public System.Windows.Forms.Button btnLog;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label lblTime;
        private System.Windows.Forms.Label lblDate;
        private System.Windows.Forms.Panel panel1;
        public System.Windows.Forms.Button btnIndicator;
        public System.Windows.Forms.Label lblProductName;
        public System.Windows.Forms.Label lblLine;
        public System.Windows.Forms.Label lblBatch;
        public System.Windows.Forms.Label label2;
        public System.Windows.Forms.Label label19;
        public System.Windows.Forms.Label label1;
        public System.Windows.Forms.Label label3;
        public System.Windows.Forms.Label label11;
        public System.Windows.Forms.Label label14;
        private System.Windows.Forms.PictureBox pictureBox3;
        public System.Windows.Forms.Label lblRole;
        public System.Windows.Forms.Label lblUserId;
        private System.Windows.Forms.Label lblBufferData;
        public System.Windows.Forms.PictureBox onCamera1;
        public System.Windows.Forms.PictureBox onCamera2;
        public System.Windows.Forms.PictureBox onCamera3;
        public System.Windows.Forms.PictureBox onPrinter;
        public System.Windows.Forms.PictureBox offCamera1;
        public System.Windows.Forms.PictureBox offCamera2;
        public System.Windows.Forms.PictureBox offCamera3;
        public System.Windows.Forms.PictureBox offPrinter;
        private System.Windows.Forms.Timer timer1;
        public System.Windows.Forms.Label lblQtyNow;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.Label label15;
        public System.Windows.Forms.PictureBox offDb;
        public System.Windows.Forms.PictureBox onDb;
        public System.Windows.Forms.Label lblDb;
    }
}

