﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Blister_Station
{
    public partial class Sample : Form
    {
        dbaccess db;
        public Sample(string admin)
        {
            InitializeComponent();
            db = new dbaccess();
            db.adminid = admin;
            db.from = "Blister Station";

        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Enter)
            {
                sample();
            }
        }

        private void sample()
        {
            if (db.validasi(txtBlisterid.Text, 1))
            {
                if (cekBlister(txtBlisterid.Text, 0))
                {
                    if (db.sampleData(txtBlisterid.Text))
                    {
                        db.eventname = "Sample =" + txtBlisterid.Text + " success";
                        db.to = txtBlisterid.Text;
                        db.systemlog();
                        txtLog.AppendText(txtBlisterid.Text + " set sample success\n");
                    }
                    else
                    {
                        db.eventname = "Sample =" + txtBlisterid.Text + " Failed";
                        db.to = txtBlisterid.Text;
                        db.systemlog();
                        txtLog.AppendText(txtBlisterid.Text + " set sample failed\n");
                    }
                }
                else
                {
                    txtLog.AppendText(txtBlisterid.Text + " set sample failed\n");
                    db.eventname = "Sample =" + txtBlisterid.Text + " Failed";
                    db.to = txtBlisterid.Text;
                    db.systemlog();
                }
            }
            else
                new Confirm("Blister ID is empty", "Information", MessageBoxButtons.OK);

            txtBlisterid.Text = "";
            txtBlisterid.Focus();
        }

        private bool cekBlister(string databarcode, int status)
        {
            List<string> field = new List<string>();
            field.Add("*");
            string where = "blisterpackid = '" + databarcode + "' and isreject='" + status + "'";
            db.selectList(field, "blisterpack", where);
            if (db.num_rows > 0)
                return true;
            return false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            sample();
        }

        private void Sample_Load(object sender, EventArgs e)
        {
            txtBlisterid.Text = "";
            txtBlisterid.Focus();
        }
    }
}
