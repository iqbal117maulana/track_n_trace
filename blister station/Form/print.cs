﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Threading;

namespace Blister_Station
{
    public partial class print : Form
    {
        tcp_testing tcp;
        List<string> dataCap = new List<string>();
        sqlitecs sql;
        public string statusLeibinger;
        private Thread serverThread2;
        Control ctrl;
        public print()
        {
            InitializeComponent();
            tcp = new tcp_testing();
            // tcp.pr = this;
        }

        internal void clearBuffer()
        {
            string Data = Convert.ToChar(27) + "OE" + "00000" + Convert.ToChar(4);
            simpanLog(Data);
            tcp.send(Data);
            tcp.dc();
        }

        public void clearBufferGs()
        {

        }

        public bool cekKoneksi()
        {
            tcp = new tcp_testing();
            sql = new sqlitecs();
            tcp.IPAddress = sql.config["ipprinter"];
            tcp.Port = sql.config["portprinter"]; ;
            tcp.Timeout = sql.config["timeout"];
            bool res = tcp.Connect();
            return res;
        }

        public print(List<string> data)
        {
            dataCap = data;
            kirimData();
        }

        // yang di pake buat blister 
        public void kirimData(List<string> data)
        {
            string disimpan = "";
            for (int i = 0; i < data.Count; i++)
            {
                string Send; 
                if (sql.config["debug"].Equals("0"))
                {

                    Send = Convert.ToChar(27) + "OE" + "0006" + data[i] + Convert.ToChar(4);
                   // Send = "^0=MR" + hitung + " " + data[i];
                }
                else
                {
                    Send = data[i] + "|";
                }
                tcp.send(Send);
                disimpan = disimpan + " \n| " + Send;
                hitung++;
            }
            //int akhir = hitung - 1;
            //disimpan = disimpan + " \n| " + "^0=CM " + akhir; 
            //if (sql.config["debug"].Equals("0"))
            //    tcp.send("^0=CM " + akhir);
            simpanLog(disimpan);
            tcp.dc();
        }

        public void simpanLog(String line)
        {
            DateTime now = DateTime.Now;
            string tahun = now.ToString("yyyy");
            string bulan = now.ToString("MM");
            string hari = now.ToString("dd");
            string dates = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            line = dates + "\t" + line;
            // cek file 
            bool cekfile = false;
            try
            {
                while (!cekfile)
                {
                    if (Directory.Exists("eventlog\\Printer"))
                    {
                        if (Directory.Exists("eventlog\\Printer" + @"\" + tahun))
                        {
                            if (Directory.Exists("eventlog\\Printer" + @"\" + tahun + @"\" + bulan))
                            {

                                using (StreamWriter outputFile = new StreamWriter("eventlog\\Printer" + @"\" + tahun + @"\" + bulan + @"\" + hari + ".txt", true))
                                {
                                    outputFile.WriteLine(line);
                                }
                                cekfile = true;
                            }
                            else
                            {

                                Directory.CreateDirectory("eventlog\\Printer" + @"\" + tahun + @"\" + bulan);
                            }
                        }
                        else
                        {
                            Directory.CreateDirectory("eventlog\\Printer" + @"\" + tahun);
                        }
                    }
                    else
                    {

                        Directory.CreateDirectory("eventlog\\Printer");
                    }
                }
            }
            catch (Exception ex)
            {
               // simpanLog(ex.Message);
            }
        }

        private void kirimData()
        {
            sql = new sqlitecs();
            for (int i = 0; i < dataCap.Count; i++)
            {
                tcp = new tcp_testing();
               // tcp.Connect(sql.config["ipLibringer"], sql.config["portLibringer"], sql.config["timeout"]);
                Console.WriteLine("^0=MR" + hitung + " " + dataCap[i]);
                 tcp.send("^0=MR" + hitung + " " + dataCap[i]);
                //tcp.send("Data Blister " + hitung +" : "+ dataCap[i]);
                hitung++;
            }
           // tcp.send("^0=MR" + hitung + " " + "End Of");

        }

        private void button7_Click(object sender, EventArgs e)
        {
         //   tcp.Connect(txtIp.Text, txtPort.Text,"10");
            //tcp.updatestatus("Connected");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            tcp.send("^0!PO");
            tcp.updatestatus("Power ON");
        }

        private void button4_Click(object sender, EventArgs e)
        {
            tcp.send("^0!NO");
            tcp.updatestatus("Noozle ON");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            tcp.send("^0!PF");
            tcp.updatestatus("Power OFF");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            tcp.send("^0!NC");
            tcp.updatestatus("Nozzle Close");
        }

        private void button6_Click(object sender, EventArgs e)
        {
            tcp.send("^0=ET " + txtExternal.Text);
            tcp.updatestatus("EXTERNAL TEXT : "+txtExternal.Text);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            tcp.send("^0!GO");
            hitung = 0;
            tcp.updatestatus("PRINT...");
        }
        int hitung=0;
    
        private void button8_Click(object sender, EventArgs e)
        {
            tcp.send("^0=MR" + hitung + " " + txtMailing.Text);
            tcp.updatestatus("MAILING RECORD " + hitung + " " + txtMailing.Text);
            hitung++;
        }

        private void label3_Click(object sender, EventArgs e)
        {
            tcp.send("^0?SM");
            tcp.updatestatus("Mail Status");
        }

        public bool cekStatusPrinter()
        {
            string data = tcp.sendBack("^0?RS");
            bool balikan = false;
            bool balikan1 = false;
            string teksbalikan = "";
            if (data.Length > 0)
            {
                char nozzle = data[5];
                switch (nozzle)
                {
                    case '1':
                        statusLeibinger = "Nozzle Opens";
                        break;
                    case '2':
                        statusLeibinger = "Nozzle Is Open";
                        balikan = true;
                        break;
                    case '3':
                        statusLeibinger = "Nozzle Closes";
                        break;
                    case '4':
                        statusLeibinger = "Nozzle Is Closed";
                        break;
                    case '5':
                        statusLeibinger = "Nozzle In Between";
                        break;
                    default:
                        statusLeibinger = "Nozzle Is Invalid";
                        break;
                }

                char state = data[7];
                switch (state)
                {
                    case '1':
                        statusLeibinger = statusLeibinger + ", State : Standby";
                        break;
                    case '2':
                        statusLeibinger = statusLeibinger + ", State : Initialization";
                        break;
                    case '3':
                        statusLeibinger = statusLeibinger + ", State : Interval";
                        break;
                    case '4':
                        statusLeibinger = statusLeibinger + ", State : Ready For Action";
                        break;
                    case '5':
                        statusLeibinger = statusLeibinger + ", State : Ready For PrintStart";

                        break;
                    default:
                        statusLeibinger = statusLeibinger + ", State : Printing";
                        balikan1 = true;
                        break;
                }
                if (balikan && balikan1)
                    return true;
                else
                    return false;
            }
            else
            {
                return false;
            }
        }

        public void disconect()
        {
            tcp.dc();
        }

        static tcp_testing tcp1 = new tcp_testing();
       //static sqlitecs sql = new sqlitecs();

        private static String ZplTemplate = "^XA~TA000~JSN^LT0^MNW^MTT^PON^PMN^LH0,0^JMA^PR3,3~SD23^JUS^LRN^CI0^XZ\r\n"
            + "^XA\r\n"
            + "^MMT\r\n"
            + "^PW709\r\n"
            + "^LL0709\r\n"
            + "^LS0\r\n"
            + "^FO0,0^GFA,08448,08448,00044,:Z64:eJzt2E1u2zgUB3Aq9ITFwKhmOYug7BG67CIIfRQfIcsuipJBDtArMehByu5mye60EPT6f6SVSJ7Yei4aoDMwIbhm/LNM8ePxsUqdy7mcy7n8x8vqheyr38D+fYL964Xsu9/Abn+53Zx83yRSGLELqV0r9YdqZPbyBKsx0UMTRbYJahW1zKqoVlsjtEmtbqX2g1q/b4X2Wq1vrNC+UWtLSWZbdcU2SKyJVy1lsxHZf64N9R9FbdB9b4g6URsa6jXRN6ltiO5EbVDUKyItt73Q+l75ZMTWhlZsefRktsOLlVka7XIQbijj1WEA46I11QY1LDfBE9/PJ5cXaUtUv0Fh0b4a7y5owuv6D9FyE5Qe7XITxhApaYJSt9X2EntdbSexN+XVCx4NMyHIbY1lLolsuaOV2fJURtaG0ltaZusoiPrMVisKaK7aNxK7mwmSAKF3FnvXYmnHGbZZtm60bxcpwulQv7LcaaYvY2Ekc/0T9gDFrU6LFLGmKSFKsNw4hLF1tNwELiVEiZbmaTY1WW7Ra0LbREAvtAGRUmrv2cr6TGPnDkJ7YYmiF4VJdXHjKX0UZkcRKcwHnSTUYJLl20YUH1a8a90qkeX53m2R0slsvt6qK6FNV9txP1q08XIriiXF/rlROshs0BtR3Cm22UiPAoaTa0Hc2dmN9Gj4uljZkbN27eUJ9kJk1/Pq0TP4+mh1Xvae6v0xu9dbR2fnnj0a4OcH6bKaDj7f3JZd/GAqOrclR/DhgJ0fpO1R+3ZuE14OhqG5LdmMcPso9tMJVnY8Ui7KHBf/UhZpaDMgB8F7i72aU3SDiuv4qvUnO/B+3vLfPH8n83Yd67FpPArsCmH7gHX4G5KL0DJA5McZT3Nqb6fZL2zUfEzrcA+KJW9Bho4zwGdOvt0004BNmo9/3A5KOCCgHdRjB/qKr/KBYWaz6QxbfP4dVb5/74hLamhim2pbzoPw+RcqbabOV6tpcgpg2xXLuyndc/Nx/1wo5bkdlIW1eGok8dZQhB30A2X0jM2lPrHIrTK6HTZdIov10XXNHUX0eNuV+mj1zmZFDX6Nc4uIZB6/0uLqbD+ZXJpHrc0+KbrjrsfIJlTMwIkybDe1PS6buSfv0eu4v0uomJ5/AjZPjgGm2ISfrzaxDY+W63HfYlDwlj9wXEEkwCEANk4s/mL27AOpCvetge3ctwGDgrfcQdXmegH6qcW3H9jyyQNBxaPCEaZYrk+tLVYPo/1Sbbm4HqY2szUDRza/6w9Eo2ety/4O/d4Xi8Vz31fLF9r7NHfw8JjggbAuOQq6nS39/C87aKRgASlbx1GQ54V5tC7h5Wmu8wSNVFaHrdP9aUzw6qbrokzmMs2TxfK0bGs/c1fztJ+vIVUtL/slO8Dit4LD4YDXXftofWxpvuZ7XkfEPvASfhwTvszMesqwHG44edVsc9kUeJoGPbOIXSX0dTWBnlm+pharpRlKOPT15FHGhEbunj/NH9y4nimy/PknrJyeYpuXssLzFBd9ihX9d8DOCo8GxSa5NeEEK6fCJPdc/sflB96IGnQ=:B49C^BY208,208^FT420,422^BXN,8,200,0,0,1,~\r\n"
            + "^FH\\^FD\\7E101{0}10{2}\\7E117{3}21{1}^FS"
            + "^FT400,163^A0N,42,52^FH\\^FDPENTA BIO^FS\r\n"
            + "^FT438,490^A0N,29,28^FH\\^FD(01){0}^FS\r\n"
            + "^FT438,526^A0N,29,28^FH\\^FD(21){1}^FS\r\n"
            + "^FT438,562^A0N,29,28^FH\\^FD(10){2}^FS\r\n"
            + "^FT438,598^A0N,29,28^FH\\^FD(17){3}^FS\r\n"
            + "^FT438,634^A0N,29,28^FH\\^FD    {4}^FS\r\n"
            + "^FT42,320^A0N,37,36^FH\\^FDsimpan antara ^FS\r\n"
            + "^FT42,366^A0N,37,36^FH\\^FD+2 \\F8C dan +8 \\F8C^FS\r\n"
            + "^FT21,209^A0N,29,28^FH\\^FDBANDUNG - INDONESIA^FS\r\n"
            + "^FT332,490^A0N,29,28^FH\\^FDGTIN^FS\r\n"
            + "^FT332,526^A0N,29,28^FH\\^FDSN^FS\r\n"
            + "^FT332,562^A0N,29,28^FH\\^FDLOT NO^FS\r\n"
            + "^FT332,598^A0N,29,28^FH\\^FDEXP^FS\r\n"
            + "^FT332,634^A0N,29,28^FH\\^FDQTY^FS\r\n"
            + "^FT25,610^A0N,29,24^FH\\^FDPT. Bio Farma (Persero)^FS\r\n"
            + "^FT25,646^A0N,29,24^FH\\^FDJl. Pasteur No. 28, Bandung^FS\r\n"
            + "^FT25,682^A0N,29,24^FH\\^FDIndonesia ^FS\r\n"
            + "^PQ1,0,1,Y^XZ";

        private static string kirim = "CT~~CD,~CC^~CT~/n" +
           "^XA~TA000~JSN^LT0^MNW^MTT^PON^PMN^LH0,0^JMA^PR3,3~SD23^JUS^LRN^CI0^XZ/n" +
           "^XA/n" +
           "^MMT/n" +
           "^PW1181/n" +
           "^LL1290/n" +
           "^LS0/n" +
           "^FO0,32^GFA,16384,16384,00064,:Z64:eJztm0^1u3CAUgJGo9K7QRSSu1l2u0EUkkLLoNZGy6BWQikR5P9gej9uOH8ygqIMm8QTzAYb3Aw/HmGd6pocn28nDZP7LJ+dfnnxX+v7ku1L63LyNU3gbOttv2Cy+iQ0o+Tf5NYt3wrtJPCD3quctyv1rMD7M4U2uPx9Rz3vkkymzeFfBWOw8PiFf1Dzkqru+lLjakofytlQZcqUkGglFKhFi5TMoh6CX99lHqBNQfk3i69hb5H+ocOMa7+bwgLJT8dTD10EIb3N4i7wr2czkof7kDt5m/Mzj8arkQWwfEH9eiHp53/hCfyiaZ8bVajwPxQN5xLPwy9cH8nX0pM/Eh7P8Ol9Y0/lFfC8PccuH0/wq9V7TfDe/Lny8bg3Ry/u48CoD4BrlVY/fzUPTeKfjbVNZOK/7Y/i0q+ixvFmmXTt+bQK8Sn7G8aDiYVEbe9p3jODdqraqPVgvv7GaEFXNr2ZHMQG9/IXT8qrmy/r10Tyt25mCMoFHm78sX87jgCZDDLgvmrVTH09BF1j4s7h0gs2eYu00jMfgnW71MIKPZHk7eDLB8/hAJlDPm8I7YO38dfIOaOGrsT5D+Bffx7/xvkMdAuvlPdvAol3/+IBWtK7jvU6Aevk3cmLvqRmSR/Om8doAAIfQYq1mGh8BeX0MO9gU9WcQA3iTgsTyp/BViiqvjWDhxFHwWBlB6+YzegGjCR4tPAXPqZI5PKc4h1/k5lsnr0xH/Nfb8cOzzxN9ugd/5jz2Hjyc4cN13hIWmsSLM7/JIhyonZzn3bYlP3j1qJd3LSZ93bXPxLub+INXn3p53zalN+BHrw75tim+hT8wNaN4rUft5gUMSl67mRnG83YEH0PiqhQraMEKGh664VtGuOIjd0M2NnQBDjIAbRYx5yJjMJ+IB97YQJFjzky9bTck4yr40sm3uBCe6vOT4CVZGVa6YM7HJiMc89IT3GVl4HJ8KS0FLh6H83VjxV3mWSjZcXG/n" +
           "6fCx85Aq3PPCNyvM3rjD70kaxlJ8b3kknx/NReLqksuFh4RNnXPIYUEDeUUvYwkd5r6U8D6nLTobAccbl6c0IHvDhI+ZX5cLzcV8HIQBNSd3v8I02R7Wpkbwjoah88DS8dHmBWoaLV8PGN0jGOH605w3xLC/Bsw4lzs0k8oFO/VuGvwPvkCcvTFrmyKSk1ZK5VmHa8WTVkH/HqrGavK01oobkTYVx9/wjeIsl6XRJqmmzQtXIjfaYd+b5kGvJ3fL2TzwJn2UfseONnJZtKxzL8+AKXyQ2hbkWc83GeUmF9kL/G58RtGgs4rZXl3y8A08B2cA8oOE+5tk9yzQM5TGoVqXWknVcePQdkUYa2pgTH3ennx5bKWwa6ifxOtixLdnoAvOofzCUJ4eQm/sjK9z81p53aBPZL+x52/hwySeziP6deHZXtrkmWV9wufetLhAv3no0j+6L1w3ybgWXs3veXPO80LHimpMcLh3yLKqDeVN4hRbXdVswEqpebcGig/6Kd7ysIeUty7sZ2BnRhbLlabjMSN6K32o+grXTHvHsDAfzuzR9O9LLp3+X+UuyT76Tj1N55atBGz7M5TvFv5d3fdOv/ueMUXzv/zY+0zM903+SfgM7nEA4:C813^BY312,312^FT747,678^BXN,13,200,0,0,1,~/n" +
           "^FH\\^FD\\7E101{0}10{2}\\7E117{3}21{1}^FS/n" +
           "^FT713,236^A0N,62,81^FH\\^FDPENTA BIO^FS/n" +
           "^FT570,840^A0N,50,43^FH\\^FDGTIN^FS/n" +
           "^FT570,903^A0N,50,43^FH\\^FDLOT NO^FS/n" +
           "^FT570,966^A0N,50,43^FH\\^FDEXP^FS/n" +
           "^FT570,1029^A0N,50,43^FH\\^FDSN^FS/n" +
           "^FT74,392^A0N,33,33^FH\\^FDBANDUNG - INDONESIA^FS/n" +
           "^FT715,839^A0N,54,50^FH\\^FD(01){0}^FS/n" +
           "^FT715,907^A0N,54,50^FH\\^FD(21){1}^FS/n" +
           "^FT715,975^A0N,54,50^FH\\^FD(17){2}^FS/n" +
           "^FT715,1043^A0N,54,50^FH\\^FD(10){3}^FS/n" +
           "^FT715,1111^A0N,54,50^FH\\^FD       {4}^FS/n" +
           "^FT50,602^A0N,46,45^FH\\^FDsimpan antara ^FS/n" +
           "^FT50,660^A0N,46,45^FH\\^FD+2 \\F8C dan +8 \\F8C^FS/n" +
           "^FT20,1000^A0N,50,40^FH\\^FDPT. Bio Farma (Persero)^FS/n" +
           "^FT20,1063^A0N,50,40^FH\\^FDJl. Pasteur No. 28, Bandung^FS/n" +
           "^FT20,1126^A0N,50,40^FH\\^FDIndonesia ^FS/n" +
           "^PQ1,0,1,Y^XZ/n";
        
        private static String ZplTemplate2 = "^XA~TA000~JSN^LT0^MNW^MTT^PON^PMN^LH0,0^JMA^PR3,3~SD23^JUS^LRN^CI0^XZ\r\n"
           + "^XA\r\n"
           + "^MMT\r\n"
           + "^PW709\r\n"
           + "^LL0709\r\n"
           + "^LS0\r\n"
           + "^FO0,0^GFA,08448,08448,00044,:Z64:eJzt2E1u2zgUB3Aq9ITFwKhmOYug7BG67CIIfRQfIcsuipJBDtArMehByu5mye60EPT6f6SVSJ7Yei4aoDMwIbhm/LNM8ePxsUqdy7mcy7n8x8vqheyr38D+fYL964Xsu9/Abn+53Zx83yRSGLELqV0r9YdqZPbyBKsx0UMTRbYJahW1zKqoVlsjtEmtbqX2g1q/b4X2Wq1vrNC+UWtLSWZbdcU2SKyJVy1lsxHZf64N9R9FbdB9b4g6URsa6jXRN6ltiO5EbVDUKyItt73Q+l75ZMTWhlZsefRktsOLlVka7XIQbijj1WEA46I11QY1LDfBE9/PJ5cXaUtUv0Fh0b4a7y5owuv6D9FyE5Qe7XITxhApaYJSt9X2EntdbSexN+XVCx4NMyHIbY1lLolsuaOV2fJURtaG0ltaZusoiPrMVisKaK7aNxK7mwmSAKF3FnvXYmnHGbZZtm60bxcpwulQv7LcaaYvY2Ekc/0T9gDFrU6LFLGmKSFKsNw4hLF1tNwELiVEiZbmaTY1WW7Ra0LbREAvtAGRUmrv2cr6TGPnDkJ7YYmiF4VJdXHjKX0UZkcRKcwHnSTUYJLl20YUH1a8a90qkeX53m2R0slsvt6qK6FNV9txP1q08XIriiXF/rlROshs0BtR3Cm22UiPAoaTa0Hc2dmN9Gj4uljZkbN27eUJ9kJk1/Pq0TP4+mh1Xvae6v0xu9dbR2fnnj0a4OcH6bKaDj7f3JZd/GAqOrclR/DhgJ0fpO1R+3ZuE14OhqG5LdmMcPso9tMJVnY8Ui7KHBf/UhZpaDMgB8F7i72aU3SDiuv4qvUnO/B+3vLfPH8n83Yd67FpPArsCmH7gHX4G5KL0DJA5McZT3Nqb6fZL2zUfEzrcA+KJW9Bho4zwGdOvt0004BNmo9/3A5KOCCgHdRjB/qKr/KBYWaz6QxbfP4dVb5/74hLamhim2pbzoPw+RcqbabOV6tpcgpg2xXLuyndc/Nx/1wo5bkdlIW1eGok8dZQhB30A2X0jM2lPrHIrTK6HTZdIov10XXNHUX0eNuV+mj1zmZFDX6Nc4uIZB6/0uLqbD+ZXJpHrc0+KbrjrsfIJlTMwIkybDe1PS6buSfv0eu4v0uomJ5/AjZPjgGm2ISfrzaxDY+W63HfYlDwlj9wXEEkwCEANk4s/mL27AOpCvetge3ctwGDgrfcQdXmegH6qcW3H9jyyQNBxaPCEaZYrk+tLVYPo/1Sbbm4HqY2szUDRza/6w9Eo2ety/4O/d4Xi8Vz31fLF9r7NHfw8JjggbAuOQq6nS39/C87aKRgASlbx1GQ54V5tC7h5Wmu8wSNVFaHrdP9aUzw6qbrokzmMs2TxfK0bGs/c1fztJ+vIVUtL/slO8Dit4LD4YDXXftofWxpvuZ7XkfEPvASfhwTvszMesqwHG44edVsc9kUeJoGPbOIXSX0dTWBnlm+pharpRlKOPT15FHGhEbunj/NH9y4nimy/PknrJyeYpuXssLzFBd9ihX9d8DOCo8GxSa5NeEEK6fCJPdc/sflB96IGnQ=:B49C^BY208,208^FT420,422^BXN,8,200,0,0,1,~\r\n"
           + "^FH\\^FD{0}^FS"
           + "^FT400,163^A0N,42,52^FH\\^FDPENTA BIO^FS\r\n"
           + "^FT438,490^A0N,29,28^FH\\^FD{0}^FS\r\n"
           + "^FT42,320^A0N,37,36^FH\\^FDsimpan antara ^FS\r\n"
           + "^FT42,366^A0N,37,36^FH\\^FD+2 \\F8C dan +8 \\F8C^FS\r\n"
           + "^FT21,209^A0N,29,28^FH\\^FDBANDUNG - INDONESIA^FS\r\n"
           + "^FT332,490^A0N,29,28^FH\\^FDSN^FS\r\n"
       //    + "^FT332,526^A0N,29,28^FH\\^FDQTY^FS\r\n"
           + "^FT25,610^A0N,29,24^FH\\^FDPT. Bio Farma (Persero)^FS\r\n"
           + "^FT25,646^A0N,29,24^FH\\^FDJl. Pasteur No. 28, Bandung^FS\r\n"
           + "^FT25,682^A0N,29,24^FH\\^FDIndonesia ^FS\r\n"
           + "^PQ1,0,1,Y^XZ";

        string kirim2 = "CT~~CD,~CC^~CT~\n" +
              "^XA~TA000~JSN^LT0^MNW^MTT^PON^PMN^LH0,0^JMA^PR5,3~SD25^JUS^LRN^CI0^XZ\n" +
              "^XA\n" +
              "^MMT\n" +
              "^PW1063\n" +
              "^LL2362\n" +
              "^LS0\n" +
              "^FO32,1888^GFA,15360,15360,00032,:Z64:\n" +
              "eJztmr2O3DYQx0koNl1cTu7swjhdkCbllSmM6Mo8RIqULlOmMCIaAeIURuqUeYQ8Artr/Qh6BAUIkA0gLzP8kihKM7OGvIEN3BS3u/dbUfz888/RChHjWpw3Hu/kT3fyRzv5xZn5w51cnZtrmld7uYlvPkP42/gGGWiZrkcmkuzjGF/j3BdtED4ELjXOvxJ4Ox1/IYhxPAjxLbxc4lz+Ci/PMD6KyvGvMX4U1Rt4GXCuLkI3bIcVV59nw7Tm+huou8K5eQm81giW1o7AW+xy4IdaiJ+26Y3jQ+16YTO+E5XjWh62+ZdGWdvXWiHd88Whcdw0CH9sIUzdt/02f9oC1zV8B+ENzZ89AS6A621+Ac0/EvxSdPZvx7exqEVjR+BHhCuh7CHjTdGPSkvbw3cSLysC06IzM3eDUXL3N/HaFgVs8EUBql9w19upqX5Rh3Gt0j89N7EuJufhrl3GRedu6Dl0ch+bl3HHYn+kWi2465luTOXGcjIOtbmzgTehgILD1EwNs74G0g4qnyozF16/KvhSzpuMV76dbixmrjL+0HMN95i5nHvT60/JU7tc/LjFm4l7fVElnyeWH+gVrybu9WNVvpx4y3ArtnnUDXkM3Cy5sHG+PxkRHmdjHXlflh8/NGP4uOI6vOkwnu4T+VD0T6p+bAdUd8HVoeBj0f99LCf2U3f0bZy5STxUpLNL3oiJ+4a0Vqt8AT+fub+qAS3N+TBz/7bO1q8LM/Nxg097bhX/rZZ82nOrsH78ayZ1j3LuuspN+GwnmdaP+/8792bJ3fw3ifsbdNbmClmljwCbo5gW+hQTN24j9Q3sc96E24oOWDVkShUjLSDHxZXwI5iHSgPna2mSoOUV9K/Y9pt4Q3EYBpLD5lnjXLvJT3IoA3OhkZvESxsC7dfOIaSZUm6EjrvJFd2dLDdit7PCd95Fl6mWw+/H0+3e1pj09WUBife36eNyq22HVrt/Hr4PH21RQGcax814HT6uOAhj6xzCTWxewcEb6A5uKd9ucrBFCvgY3bEsOUw5pX2bH21y6O7q1bFLfSLBqi04qHb16tAkDvq47GEYdvlzXx8mbqALMn7luFEz10v9AX2Rr3WVc9Fm/Afgvwi54LmTdYvht5kH7bqdu8d99c1sLlXhbvys+n3mVcndnz+FeIlwr09/4NyfHXXOzYKnc8tzhF/H62feL/htfL2Kr3LJpyPPs4kv1EOmN/P12zZ7OtuwfNvGT2fUbtunznzb5068LTpozTd9fpXeNNtGetqHSvmOMXXkSj8KvtKPgjd2u4DEV/oRI/1rpR8FX+lHvP8tzdNEWunDqTzdx47dJr8OLyv9WHGDDXHkhf/b4Av92OL4SXilH9scj3L9f3hudvKe5JLlaBbiJI7px8yRNEAKRD9mjp2TY7RMByP6MUXDVBBPBIRA9GOKilxeYQGSXUivzyAgFG8ZjghcigumAVeC5q1TSIJ3umI4qR9g0+UDkvfOsqHYz288Uxb3z1v6eiL2r29NcrlTfzj9+9T1kdc/jnP6yOnf2fVRk5xcPMLpH11BxF9NUZf5wyKUpQtIeQAsZHk+K4PTR07/GoarEwSc4mLbP83B7TCC4zW7Bmg+5ZTQAhhOBpanT8Fs8Kv8dBGKmR41M79W5/8iMP+eAvPvp3LWf+/lAzm/yvz0mhvoIppTGnyKv6f4Kv/8nnzv/Xle5p8/LF/ln9+bD/T6L/PTKz7SvMxPr7ileZmfLqPMT6/5SB4Byvx1GYrhFaMPp+xP5BbfMf6/ZQQQSXCkuCjz00VcSnp/blcPGBchO6pwQXpvH7v92z5/xp2feW4Yrs/KPwb/Lklu0N9pnMK9vhmG9xQf/O89cH4guddHago5faTWN+gjza2WtP8wklqgjR3IMyDIA6nfwP+iuGL8S8Vwyfnz9JwOi44R0Ia5gW8AMYK+gvQI0QrP7SCC464FmuDcCUPSGZbsxxJYUEmydqRV1LetRXnI7lRoB8fzI/JjIt85rmz0t1ots/12TOd3TOefMr17htP6Q2YoYf9R1OwF/ayo2ev0leaaTDE43hId6PaXZievCe7uT+1hp3BqD+PPLzQ/tz+XrD//l/HnI+PPWf9O+3PYnxj/PpD+nfPn/4d/J/35Xv/e8P7doPDGV1BjWLrH0B1ePe8ciKdj0tK/ZZb2NX6x53dk3wBncmd3TOff0erc3dG9376m5b1RzOMXbnopRv89pwpg8jdh/hOczX8yz3f4/OUJ7oLkbAqazU8Sz399EDM8xC3DqSCWlwtqebpQjPeqefmgKiA/ef8D2wPpb3rO30jW/zD+5tz+h/I3ivEvp/AHKA/X4v7nELlBeMfw9kDnXxvL8X9IHn58jnPF8MpzvP7h2TbOgzkg+t+vDsLf+MMrkZ/0TxeIBcpxvzsQ81syPJyeifUjmdOzL4DyN9zzPXZ7qZgHkJxA38d93Md9fPTxH6GCeHw=:89A7\n" +
              "^FO1152,448^GFA,00512,00512,00008,:Z64:\n" +
              "eJxjYBhYUP///38QDaT+N+Chf9jBaH78NB9/Pci8H1Dz/6DR/6A0WDGI/gfRh5Mm4C5q0/QGAGgAeCY=:780B\n" +
              "^FO1120,256^GFA,00512,00512,00008,:Z64:\n" +
              "eJxjYKA5YP////8PIM0PpP+QRe9/D6b/yTdDaH5KaWYwfYABRrOD6QYGfihfHkrbk+de4mhYuAwoAAApzncN:FB7C\n" +
              "^BY220,220^FT266,973^BXB,10,200,0,0,1,~\n" +
              "^FH\\^FD01{0}10{9}17{8}21{3}^FS\n" +
              "^FT208,1720^A0B,62,91^FH\\^FDLIST OF CONTENT^FS\n" +
              "^FT116,1777^A0B,62,91^FH\\^FDDAFTAR ISI BARANG^FS\n" +
              "^FT125,541^A0B,42,45^FH\\^FD{0}^FS\n" +
              "^FT177,541^A0B,42,45^FH\\^FD{9}^FS\n" +
              "^FT229,541^A0B,42,45^FH\\^FD{8}^FS\n" +
              "^FT281,541^A0B,42,45^FH\\^FD{3}^FS\n" +
              "^FT1182,2086^A0B,25,24^FH\\^FDJl. Pasteur No. 28 Bandung - 40161 PO BOX 1136 Tel. +62-22-2033755 Fax. +62-22-2041306 Indonesia, mail@biofarma.co.id - www.biofarma.co.id ^FS\n" +
              "^FT1174,260^A0B,25,24^FH\\^FD@infoimunisasi^FS\n" +
              "^FT1177,454^A0B,25,24^FH\\^FDInfo Imunisasi^FS\n" +
              "^FT1180,2331^A0B,25,24^FH\\^FDPT. Bio Farma (Persero)^FS\n" +
              "^FT1118,2335^A0B,37,36^FH\\^FDOperator Code^FS\n" +
              "^FT1065,2333^A0B,37,36^FH\\^FDKode Petugas^FS\n" +
              "^FT951,2333^A0B,37,36^FH\\^FDBerat Kotor^FS\n" +
              "^FT1002,2332^A0B,37,36^FH\\^FDGross Weight^FS\n" +
              "^FT892,2333^A0B,37,36^FH\\^FDTotal Quantity^FS\n" +
              "^FT839,2333^A0B,37,36^FH\\^FDJumlah Total^FS\n" +
              "^FT777,2335^A0B,37,36^FH\\^FD3^FS\n" +
              "^FT1088,2014^A0B,37,36^FH\\^FD:^FS\n" +
              "^FT972,2013^A0B,37,36^FH\\^FD:^FS\n" +
              "^FT861,2011^A0B,37,36^FH\\^FD:^FS\n" +
              "^FT568,2015^A0B,37,36^FH\\^FD:^FS\n" +
              "^FT464,2016^A0B,37,36^FH\\^FD:^FS\n" +
              "^FT1089,1959^A0B,37,36^FH\\^FD adminid^FS\n" +
              "^FT973,1960^A0B,37,36^FH\\^FD {4}^FS\n" +
              "^FT862,1959^A0B,37,36^FH\\^FD {5}^FS\n" +
            //   "^FT783,2274^A0B,37,36^FH\\^FDINI BATCH NUMBER 3^FS\n"+
            //    "^FT725,2276^A0B,37,36^FH\\^FDINI BATCH NUMBER 2^FS\n"+
              "^FT771,586^A0B,37,36^FH\\^FDvial / ampl / pouch^FS\n" +
            //  "^FT774,983^A0B,37,36^FH\\^FDini Quantity 3^FS\n"+
              "^FT772,1403^A0B,37,36^FH\\^FDJumlah / Quantity^FS\n" +
              "^FT722,583^A0B,37,36^FH\\^FDvial / ampl / pouch^FS\n" +
            //   "^FT725,981^A0B,37,36^FH\\^FDini Quantity 2^FS\n"+
              "^FT727,1402^A0B,37,36^FH\\^FDJumlah / Quantity^FS\n" +
              "^FT670,586^A0B,37,36^FH\\^FDvial / ampl / pouch^FS\n" +
              "^FT676,984^A0B,37,36^FH\\^FD{5}^FS\n" +
              "^FT672,1403^A0B,37,36^FH\\^FDJumlah / Quantity^FS\n" +
              "^FT670,2277^A0B,37,36^FH\\^FD{1}^FS\n" +
              "^FT465,1960^A0B,37,36^FH\\^FD{7}^FS\n" +
              "^FT354,1957^A0B,37,36^FH\\^FD{6}^FS\n" +
              "^FT354,2016^A0B,37,36^FH\\^FD:^FS\n" +
              "^FT723,2335^A0B,37,36^FH\\^FD2^FS\n" +
              "^FT669,2333^A0B,37,36^FH\\^FD1^FS\n" +
              "^FT615,2333^A0B,37,36^FH\\^FDBatch No^FS\n" +
              "^FT559,2333^A0B,37,36^FH\\^FDNo. Batch^FS\n" +
              "^FT500,2332^A0B,37,36^FH\\^FDColie No^FS\n" +
              "^FT447,2333^A0B,37,36^FH\\^FDNo. Koli^FS\n" +
              "^FT386,2335^A0B,37,36^FH\\^FDName of Product^FS\n" +
              "^FT333,2334^A0B,37,36^FH\\^FDJenis Barang^FS\n" +
              "^FT125,698^A0B,42,48^FH\\^FDGTIN^FS\n" +
              "^FT177,698^A0B,42,48^FH\\^FDPKG DT^FS\n" +
              "^FT229,698^A0B,42,48^FH\\^FDDO^FS\n" +
              "^FT281,698^A0B,42,48^FH\\^FDSN^FS\n" +
              "^FO961,2175^GB0,158,1^FS\n" +
              "^FO1078,2139^GB0,193,1^FS\n" +
              "^FO851,2148^GB0,185,1^FS\n" +
              "^FO572,2194^GB0,139,1^FS\n" +
              "^FO458,2220^GB0,112,1^FS\n" +
              "^FO343,2146^GB0,187,1^FS\n" +
              "^PQ1,0,1,Y^XZ\n";

        string kirim3 = "CT~~CD,~CC^~CT~\\n" +
"^XA~TA000~JSN^LT0^MNW^MTT^PON^PMN^LH0,0^JMA^PR2,2~SD4^JUS^LRN^CI0^XZ\\n" +
"^XA\\n" +
"^MMT\\n" +
"^PW1181\\n" +
"^LL2955\\n" +
"^LS0\\n" +
"^FO0,2464^GFA,15360,15360,00032,:Z64\\n" +
"eJztmktu3EYQhptqx3SQB2UkCLQKBWSR7KzsBMNwX0VHkHdCEIRMECDLXME3MX2CXIHZZM0lF4PpVPWDbDZZVePQMmJABUicmW+myX79/XeRSj3EB4lH91z+2U6ud/JiL2/vl6tuJ+/vmQ8Cv93J7+KLdpv/FI5UR1+HIzWQXoQ+pgb6i1D0BcFfBn5F80/wSNUT+Bd47AheK/UpHAqG/wgH3VK8Va/gUBJY1Z16Bb/9luF/t64ZCN7rt6+TblrxoXzbKTWSfKx+7VRBDiNjLfCyZ3jRq6rbhI+RH4FDK2zFZ61qgA/KbBf+6E5ZewB+JLjRyEdN8LPaWjsWh/KwzXUJfCjgOwTXAv8NeA+caL6ihebpGN5V9tgC7wnewwUg77Y5qIO1aubaZhxaFqo+8Tq/kFFVY8KhM3KOEXlh85qs+bKAw4Jja8eqer04htP2M49NfZdyf9Yq5RX8pvDc2EOoXsKRhfaow1UtOLZMZdtQbu/LSXlpR+u5DtVacoXvulgwXoE5qpTXNuk41K/mAJ/NXKf8Bjn2RTLUUo4vGmgGk3AzDxynPzmvZl6pDa5n7vTFZjwZeOMWh44J3OtHXj5cYOClwOue4G144b6H7bPgdRjvZ/57a36MFfH8kJcf3mjPzYqH0V5RvI/X4fkxb594+Z7j5S54nK4Tb5ftH2UrtHMFhybl48R7z/sF1/3EXUVKOKQTuGwn7n6lQUtTPi1KQReKZP5iXMzcVSDntwl3H2e8S/ghHBOpm70ffo5HnIDt9Ok8f/Dz18rPuJnj+B8nfvAfLBSyCW/rAxTWzRM9Rh35iAupq+BiJdH+tKrCRvtFzUoVaxAu0HHd+R5MIzRH5f5fT4KWXKAjZa+2I3DN8luew+JZ0HzAwc/yGsqg7IPn48TzdRTXE3AIcaTU+UKIHAdXcHcmW4jdyop9fx2/nna/6x9cve14Eb++LCDyw5P4drnUlsdycB9+7t/arIBq1MjH+nf/dsX7YijRIfwTqpfxuiuGCk/5fJuDLULeBndscg5DbqiRF9scXt+av6Y2MXZpZVC1b42eOOjjooWx269+LqYpA9pSphxH9NWdmvmgFvqD+nIFHZjyMuFfwt9VN5s/5KmTRf15mnA3uZ7MHHv9aTuby9zd4Kj6Ss28yfgN/H0DvURxqJ76muZu73ie8uXojPuWkuCuU89TvrSpsabYzC7Mksctz7T3MEv1uFz9PreHPqa9TS3ydvMLfTjm6rbmPctLm+tDzrd9/k04art9grgO5fIdY9o75vqR8ZV+ZNxJxUYB5+G40o8QcR1d6UfGV/oQ4geBx4G00ocTuZr4n5s87t1z/Vjxkexiz3P/t8FLgdM74bV+bHImcn1475yp20mc2ObGMBInttGnckI/Et6ynNKPmfcsL4UOpPQjht6e/lPQiYAQhH5M0QgFmMyf5lHz89MJCMdLgVMCF3HLV0B3NctLTGcwvBriToTkrH6ATb98w/GDcvkOKtz4ZvVPsfohzY/d81vQD7NTfyT9+9j1UdS/vfoo6p/EW5bL+ijpH99Amlcf0l/FKPL0Xx5WKKAhDFoMY+lUKIakj5L+aYEriRuBVwIXV5ha4IXAWX/lChD4dEPnvwSVp48hLfCr/HQWwvAoLD++Vvv/LCj/HoPy76dyyr/HkP25wI/8+Mry02s+QhPxnNXgU/w9x1f553fke88v8jz//J75Kv/8rjzPT2exyk+veMvyPD+95j3L8/x0Hnl+es3tHxzP89OrkHgjCMwp61PL8Dw/nUcpLLBUgiPitmEdQtEZtvro37nTPxP8G+e9Meq9/m2nP5P2zyLf6d/38v+Df7/k+Eg/p3EKx/7n7Ady8kEJ5cfHS45bnjt9ZDnoY0tz1EeeD+FO53agfzc9zTWoA7cHxAcNOP1G/876Q8FfOH3kuBF4LRi0ikiwxtDSBSLnRMAIJ6gEhyiuILXA3QMnDBd3GN9JOwjDb7HYJFmJbcOoqLt/V9LcZ3fiXfFVhP0j8TCRbxw8UrwUOqcSGr8SGl/aXhph+CLnlihj+QwlrD/s6AX9bLjRC5xNMTQDn2JoMP/W0xwaT+/kBcdxfeE66BTOrWHi/kXg9+3PjejPLwV/3gr+XPLvvD/H9Yn350fWv0v+/IP495bhe/17/nxJHuz2/LG7QLr5nrXTg0qb4ZwDc3fM9B13beAshIVl5NOXwPnc2Sg0/sirczXyrV8OvLzrXjAH4vCygv5boQAp/1NI9kZaoIT7O7K74O/fqPUTOnlIKWgxP8nd/3XBjHAXzP0bMbjphcFNTwwhvV3I8sFewPcfu//B/CTDwf9I/saI/qdj+P37H9bfWMG/nMLfUPzcc9L/1IFTHVz1PAfbz+ZfoWUEfmR5ePic1geJN5a9/nBznubeHNDt72YH42/c3pHJT7q7C8wElXjheUdxv7lluNs9c/PHCAukFua3eH9PXF4a4QaRJNAP8RAPsT/+BZmzS/0=:E102\\n" +
"^FO1120,1024^GFA,00512,00512,00008,:Z64:\\n" +
"eJxjYBhegP////8PgLQ8kP6Aj2a/D6UP4qcZD74Hmwc1X74BjT4ApT9A6f9QfThpAu6iMk0IAABlUnBE:D6C5\\n" +
"^FO1088,832^GFA,00768,00768,00012,:Z64:\\n" +
"eJxjYBgFMPAfCKBMRhC7gZbswz/q4eyDH2jMfgBn/2FggLN/AL2Mjf0BqAUhzvwfoVeetmFCGhs5vsgGALe8kI0=:FE99\\n" +
"^BY220,220^FT237,1560^BXB,10,200,0,0,1,~\\n" +
"^FH\\^FD\\7E\\7E10112345678901234101234567\\7E\\7E117120212211700001123^FS\\n" +
"^FT179,2307^A0B,62,91^FH\\^FDLIST OF CONTENT^FS\\n" +
"^FT87,2364^A0B,62,91^FH\\^FDDAFTAR ISI BARANG^FS\\n" +
"^FT96,1128^A0B,42,45^FH\\^FDINI GTIN^FS\\n" +
"^FT148,1128^A0B,42,45^FH\\^FDINI PACKAGING DATE^FS\\n" +
"^FT200,1128^A0B,42,45^FH\\^FDINI DELIVERY ORDER^FS\\n" +
"^FT252,1128^A0B,42,45^FH\\^FDINI SERIAL NUMBER^FS\\n" +
"^FT1153,2672^A0B,25,24^FH\\^FDJl. Pasteur No. 28 Bandung - 40161 PO BOX 1136 Tel. +62-22-2033755 Fax. +62-22-2041306 Indonesia, mail@biofarma.co.id - www.biofarma.co.id ^FS\\n" +
"^FT1145,847^A0B,25,24^FH\\^FD@infoimunisasi^FS\\n" +
"^FT1148,1041^A0B,25,24^FH\\^FDInfo Imunisasi^FS\\n" +
"^FT1151,2917^A0B,25,24^FH\\^FDPT. Bio Farma (Persero)^FS\\n" +
"^FT1089,2921^A0B,37,36^FH\\^FDOperator Code^FS\\n" +
"^FT1036,2920^A0B,37,36^FH\\^FDKode Petugas^FS\\n" +
"^FT922,2920^A0B,37,36^FH\\^FDBerat Kotor^FS\\n" +
"^FT973,2919^A0B,37,36^FH\\^FDGross Weight^FS\\n" +
"^FT863,2920^A0B,37,36^FH\\^FDTotal Quantity^FS\\n" +
"^FT810,2919^A0B,37,36^FH\\^FDJumlah Total^FS\\n" +
"^FT748,2921^A0B,37,36^FH\\^FD3^FS\\n" +
"^FT1059,2601^A0B,37,36^FH\\^FD:^FS\\n" +
"^FT943,2600^A0B,37,36^FH\\^FD:^FS\\n" +
"^FT832,2598^A0B,37,36^FH\\^FD:^FS\\n" +
"^FT539,2602^A0B,37,36^FH\\^FD:^FS\\n" +
"^FT435,2602^A0B,37,36^FH\\^FD:^FS\\n" +
"^FT1060,2546^A0B,37,36^FH\\^FDINI id petugas Manual packer^FS\\n" +
"^FT944,2547^A0B,37,36^FH\\^FDINI BERAT^FS\\n" +
"^FT833,2546^A0B,37,36^FH\\^FDINI QUANTITY TOTAL^FS\\n" +
"^FT754,2861^A0B,37,36^FH\\^FDINI BATCH NUMBER 3^FS\\n" +
"^FT696,2862^A0B,37,36^FH\\^FDINI BATCH NUMBER 2^FS\\n" +
"^FT742,1173^A0B,37,36^FH\\^FDvial / ampl / pouch^FS\\n" +
"^FT745,1570^A0B,37,36^FH\\^FDini Quantity 3^FS\\n" +
"^FT743,1990^A0B,37,36^FH\\^FDJumlah / Quantity^FS\\n" +
"^FT693,1170^A0B,37,36^FH\\^FDvial / ampl / pouch^FS\\n" +
"^FT696,1568^A0B,37,36^FH\\^FDini Quantity 2^FS\\n" +
"^FT697,1988^A0B,37,36^FH\\^FDJumlah / Quantity^FS\\n" +
"^FT641,1173^A0B,37,36^FH\\^FDvial / ampl / pouch^FS\\n" +
"^FT647,1570^A0B,37,36^FH\\^FDini Quantity 1^FS\\n" +
"^FT643,1990^A0B,37,36^FH\\^FDJumlah / Quantity^FS\\n" +
"^FT641,2864^A0B,37,36^FH\\^FDINI BATCH NUMBER 1^FS\\n" +
"^FT436,2547^A0B,37,36^FH\\^FDINI KOLI^FS\\n" +
"^FT325,2544^A0B,37,36^FH\\^FDINI PRODUCT MODEL NAME^FS\\n" +
"^FT325,2603^A0B,37,36^FH\\^FD:^FS\\n" +
"^FT694,2922^A0B,37,36^FH\\^FD2^FS\\n" +
"^FT640,2919^A0B,37,36^FH\\^FD1^FS\\n" +
"^FT586,2920^A0B,37,36^FH\\^FDBatch No^FS\\n" +
"^FT530,2919^A0B,37,36^FH\\^FDNo. Batch^FS\\n" +
"^FT470,2919^A0B,37,36^FH\\^FDColie No^FS\\n" +
"^FT418,2920^A0B,37,36^FH\\^FDNo. Koli^FS\\n" +
"^FT357,2921^A0B,37,36^FH\\^FDName of Product^FS\\n" +
"^FT304,2921^A0B,37,36^FH\\^FDJenis Barang^FS\\n" +
"^FT96,1284^A0B,42,48^FH\\^FDGTIN^FS\\n" +
"^FT148,1284^A0B,42,48^FH\\^FDPKG DT^FS\\n" +
"^FT200,1284^A0B,42,48^FH\\^FDDO^FS\\n" +
"^FT252,1284^A0B,42,48^FH\\^FDSN^FS\\n" +
"^FO932,2762^GB0,158,1^FS\\n" +
"^FO1049,2726^GB0,193,1^FS\\n" +
"^FO822,2734^GB0,186,1^FS\\n" +
"^FO543,2781^GB0,138,1^FS\\n" +
"^FO429,2807^GB0,112,1^FS\\n" +
"^FO314,2733^GB0,186,1^FS\\n" +
"^PQ1,0,1,Y^XZ\\n";


        public void Print2(String SerialNumber)
        {
            //String ZplString = String.Format(kirim2, SerialNumber);
            tcp1 = new tcp_testing();
            tcp1.IPAddress = txtIp.Text;
            tcp1.Port = txtPort.Text;
            tcp1.Timeout = "20";
            tcp1.Connect();
           // txtStatus.AppendText(tcp1.Connect(ip, port, "2").ToString());
            tcp1.send(SerialNumber);
            tcp1.dc();
        }
        public string ip;
        public string port;

        public void Print(String Gtin, String SerialNumber, String LotNo, String ExpiryDate, String Quantity)
        {
            String ZplString = String.Format(ZplTemplate, Gtin, SerialNumber, LotNo, ExpiryDate, Quantity);
            tcp1 = new tcp_testing();

            //txtStatus.AppendText(tcp1.Connect(ip, port, "2").ToString());
            tcp1.send(ZplString);
            tcp1.dc();
        }

        private void button9_Click(object sender, EventArgs e)
        {
            ip = txtIp.Text;
            port = txtPort.Text;
            Print2(textBox6.Text);
        }
        
        public void kirimData(string[] data)
        {
            string disimpan = "";
            for (int i = 0; i < data.Length; i++)
            {
                string Send;
                if (sql.config["debug"].Equals("0"))
                {
                    Send = "^0=MR" + hitung + " " + data[i];
                }
                else
                {
                    Send = data[i]+"|";
                }
                tcp.send(Send);
                disimpan = Send + "\n";
                hitung++;
            }
            simpanLog(disimpan);
            tcp.dc();
        }

        internal void startListener(Control control)
        {
            ctrl = control;
            tcp.ctrl = this.ctrl;
            serverThread2 = new Thread(new ThreadStart(tcp.MulaiServerCapid));
            serverThread2.Start();
        
        }

        internal void stop()
        {
            tcp.conneect = false;
        }

        internal void start()
        {
            tcp.conneect = true;
        }
    }
}
