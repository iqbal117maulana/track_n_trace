﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace Blister_Station
{
    public partial class commission : Form
    {
        dbaccess db;
        public string blisterid;
        public commission(string admin, string param, string line)
        {
            InitializeComponent();
            db = new dbaccess();
            db.adminid = admin;
            db.from = "Blister Station";
            SetBatchNumber(param, line);
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                decomision();
            }
        }

        bool cekblister(string blister,int status)
        {
            List<string> field = new List<string>();
            field.Add("*");
            string where = "blisterpackid = '" + blister + "' and isreject='" + status + "'";
            db.selectList(field, "blisterpack", where);
            if (db.num_rows > 0)
                return true;
            return false;
        }

        private void decomision()
        {
            if (txtBlister.Text.Length > 0)
            {
                if (db.validasi(txtCapid.Text, 1))
                {
                    if (cekblister(txtBlister.Text, 0))
                    {
                        if (db.selectCountVaccine(cmbBatch.Text, txtBlister.Text) < 5)
                        {
                            if (db.comission(txtCapid.Text, txtBlister.Text))
                            {
                                db.eventname = "Comission =" + txtCapid.Text + " To " + txtCapid.Text + " success";
                                db.to = txtCapid.Text;
                                db.systemlog();
                                txtLog.AppendText(txtCapid.Text + " commission success" + Environment.NewLine);
                            }
                            else
                            {
                                db.eventname = "Comission =" + txtCapid.Text + " To " + txtCapid.Text + " Failed";
                                db.to = txtCapid.Text;
                                db.systemlog();
                                txtLog.AppendText(txtCapid.Text + " commission failed" + Environment.NewLine);
                            }
                            txtBlister.Focus();
                        }
                        else
                        {
                            new Confirm("The quantiry of vaccines are 5", "Information", MessageBoxButtons.OK);
                            txtCapid.Focus();
                        }
                    }
                    else
                    {
                        new Confirm("Vaccine ID is empty", "Information", MessageBoxButtons.OK);
                        txtCapid.Focus();
                    }
                }
                else
                {
                    new Confirm("Blister ID is not recognized", "Information", MessageBoxButtons.OK);
                    txtBlister.Focus();
                }
            }
            else
            {
                new Confirm("Blister ID is empty", "Information", MessageBoxButtons.OK);
                txtBlister.Focus();
            }

            txtCapid.Text = "";
            txtBlister.Text = "";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            decomision();
        }

        private void commission_Load(object sender, EventArgs e)
        {
            txtCapid.Text = "";
            txtBlister.Text = "";
            txtBlister.Focus();
        }

        private void txtBlister_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                txtCapid.Focus();
        }

        private void SetBatchNumber(string parameter, string linenumber)
        {
            List<string> field = new List<string>();
            field.Add("batchNumber,productModelId");
            string where = "status = '1' and linepackagingid = '" + linenumber + "' and productModelId IN " + parameter + "";
            List<string[]> datacombo = db.selectList(field, "[packaging_order]", where);
            List<string> da = new List<string>();
            //for (int i = 0; i < datacombo.Count; i++)
            for (int i = datacombo.Count - 1; i > -1; i--)
            {
                da.Add(datacombo[i][0]);
            }
            cmbBatch.DataSource = da;
        }

        //private List<string[]> loadData()
        //{
        //    List<string> field = new List<string>();
        //    field.Add("gsOneVialId, SUBSTRING(gsOneVialId, CHARINDEX('BIOF', gsOneVialId), 14)");
        //    string where = "batchNumber = '" + cmbBatch.Text + "' and isReject = 0 and blisterpackId = '" + txtBlister.Text + "' ";
        //    List<string[]> data = db.selectList(field, "[Vaccine]", where);
        //    AutoCompleteStringCollection namesCollection = new AutoCompleteStringCollection();
        //    for (int i = 0; i < data.Count; i++)
        //    {
        //        namesCollection.Add(data[i][0]);
        //    }
        //    txtCapid.AutoCompleteMode = AutoCompleteMode.None;
        //    txtCapid.AutoCompleteSource = AutoCompleteSource.CustomSource;
        //    txtCapid.AutoCompleteCustomSource = namesCollection;

        //    return data;
        //}

        private void txtCapid_TextChanged(object sender, EventArgs e)
        {
            //listBox2.Items.Clear();
            //if (txtCapid.Text.Length == 0)
            //{
            //    hideResults();
            //    return;
            //}

            //foreach (String ss in txtCapid.AutoCompleteCustomSource)
            //{
            //    if (ss.Contains(txtCapid.Text))
            //    {
            //        listBox2.Items.Add(ss);
            //        listBox2.Visible = true;
            //    }
            //}
        }

        void hideResults()
        {
            listBox2.Visible = false;
        }

        private void listBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtCapid.Text = listBox2.Items[listBox2.SelectedIndex].ToString();
            hideResults();
        }

        void listBox2_LostFocus(object sender, System.EventArgs e)
        {
            hideResults();
        }

        private void txtCapid_KeyPress(object sender, KeyPressEventArgs e)
        {
            var regex = new Regex(@"[^a-zA-Z0-9\b]");
            if (regex.IsMatch(e.KeyChar.ToString()))
            {
                e.Handled = true;
            }
        }

        //private void txtBlister_KeyPress(object sender, KeyPressEventArgs e)
        //{
        //    var regex = new Regex(@"[^a-zA-Z0-9\b]");
        //    if (regex.IsMatch(e.KeyChar.ToString()))
        //    {
        //        e.Handled = true;
        //        if (txtBlister.Text.Length == 6)
        //        {
        //            if (loadData().Count > 0)
        //            {
        //                txtCapid.Enabled = true;
        //                loadData();
        //            }
        //            else
        //            {
        //                MessageBox.Show("Can not find vaccine!");
        //            }
        //        }
        //    }
        //}
    }
}
