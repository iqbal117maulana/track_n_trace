﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace Blister_Station
{
    public partial class Decomission : Form
    {
        dbaccess db;
        Model.Vaccine vaccine;
        Model.Blister blister;
        string linenumber;

        public Decomission(string admin, string line, string param)
        {
            InitializeComponent();
            db = new dbaccess();
            db.adminid = admin;
            db.from = "Blister Station";
            linenumber = line;
            SetBatchNumber(param);
            blister = new Model.Blister();
            vaccine = new Model.Vaccine();
        }

        private void SetBatchNumber(string parameter)
        {
            List<string> field = new List<string>();
            field.Add("batchNumber,productModelId");
            string where = "status = '1' and linepackagingid = '" + linenumber + "' and productModelId IN " + parameter + "";
            List<string[]> datacombo = db.selectList(field, "[packaging_order]", where);
            List<string> da = new List<string>();
            //for (int i = 0; i < datacombo.Count; i++)
            for (int i = datacombo.Count - 1; i > -1; i--)
            {
                da.Add(datacombo[i][0]);
            }
            cmbBatch.DataSource = da;
        }
        
        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Enter)
            {
                decomision();
            }
        }

        private void decomision()
        {
            blister = new Model.Blister();
            vaccine = new Model.Vaccine();
            txtProduct.Text = Util.Common.REMOVE_UNUSED_CHAR(txtProduct.Text);
            if (!Util.Common.CHECK_EMPTY(txtProduct))
            {
                // cek blister
                blister.BLISTERPACKID = txtProduct.Text;
                blister.BATCHNUMBER = cmbBatch.Text;
                blister.CAPID = txtProduct.Text;
                blister.GS1VIALID = txtProduct.Text;

                //vaccine.BATCHNUMBER = cmbBatch.Text;
                //vaccine.CAPID = txtProduct.Text;
                //vaccine.GS1VIALID = txtProduct.Text;
                if (blister.CHECK_BLISTER())
                {
                    decommission_Blister(txtProduct.Text);
                }
                else if (blister.CHECK_VACCINE())
                {
                    decommission_Blister(blister.BLISTERPACKID);
                    //decommission_Vaccine();
                }
                else
                {
                    log("Product " + txtProduct.Text + " Not Found");
                }
            }

            txtProduct.Text = "";
            txtProduct.Focus();
        }

        private void decommission_Vaccine()
        {
            vaccine.CAPID = txtProduct.Text;
            vaccine.GS1VIALID = txtProduct.Text;
            if (vaccine.CHECK_VACCINE())
            {
                vaccine.BATCHNUMBER = cmbBatch.Text;
                if (vaccine.CHECK_VACCINE())
                {
                    vaccine.ISREJECT = "0";
                    if (vaccine.CHECK_VACCINE())
                    {
                        vaccine.BLISTERID = "kosong";
                        if (vaccine.CHECK_VACCINE())
                        {
                            if (vaccine.DECOMMISSION())
                            {
                                db.eventname = "Decomission =" + txtProduct.Text + " success";
                                db.to = txtProduct.Text;
                                db.systemlog();
                                log(txtProduct.Text + " decommission success" + Environment.NewLine);
                            }
                            else
                            {
                                db.eventname = "Decommission failed," + txtProduct.Text + " Something wrong with Database";
                                db.to = txtProduct.Text;
                                db.systemlog();
                                log("Decommission failed," + txtProduct.Text + " Something wrong with Database" + Environment.NewLine);
                            }
                        }
                        else
                        {
                            db.eventname = "Decommission failed," + txtProduct.Text + " is register with blister";
                            db.to = txtProduct.Text;
                            db.systemlog();
                            log("Decommission failed," + txtProduct.Text + " is register with blister" + Environment.NewLine);

                        }
                    }
                    else
                    {

                        db.eventname = "Decommission failed," + txtProduct.Text + " is not a good product";
                        db.to = txtProduct.Text;
                        db.systemlog();
                        log("Decommission failed," + txtProduct.Text + " is not a good product " + Environment.NewLine);
                    }
                }
                else
                {
                    db.eventname = "Decommission failed," + txtProduct.Text + " Wrong BatchNumber";
                    db.to = txtProduct.Text;
                    db.systemlog();
                    log("Decommission failed," + txtProduct.Text + " Wrong BatchNumber" + Environment.NewLine);

                }
            }
            else
            {
                db.eventname = "Decomission =" + txtProduct.Text + " Not Found";
                db.to = txtProduct.Text;
                db.systemlog();
                log("Decommission failed," + txtProduct.Text + " Not Found" + Environment.NewLine);
            }
        }

        private void decommission_Blister(string data)
        {
            //blister.BLISTERPACKID = txtProduct.Text;
            if (blister.CHECK_BLISTER())
            {
                blister.BATCHNUMBER = cmbBatch.Text;
                if (blister.CHECK_BLISTER())
                {
                    blister.ISREJECT = "0";
                    if (blister.CHECK_BLISTER())
                    {
                        blister.INNERBOXID = "kosong";
                        if (blister.CHECK_BLISTER())
                        {
                            if (blister.DECOMMISSION())
                            {
                                db.insertDecommission(txtProduct.Text);
                                //vaccine.BLISTERID = txtProduct.Text;
                                //vaccine.BATCHNUMBER = cmbBatch.Text;
                                vaccine.BLISTERID = blister.BLISTERPACKID;
                                vaccine.BATCHNUMBER = blister.BATCHNUMBER;
                                if (vaccine.UPDATE_EMPTY_BLISTER())
                                {
                                    db.eventname = "Decomission =" + txtProduct.Text + " success";
                                    db.to = txtProduct.Text;
                                    db.systemlog();
                                    log(txtProduct.Text + " decommission success" + Environment.NewLine);
                                }
                                else
                                {
                                    db.eventname = "Decommission failed," + txtProduct.Text + " Something wrong with Database[Vaccine]";
                                    db.to = txtProduct.Text;
                                    db.systemlog();
                                    log("Decommission failed," + txtProduct.Text + " Something wrong with Database" + Environment.NewLine);

                                }
                            }
                            else
                            {
                                db.eventname = "Decommission failed," + txtProduct.Text + " Something wrong with Database[Blister]";
                                db.to = txtProduct.Text;
                                db.systemlog();
                                log("Decommission failed," + txtProduct.Text + " Something wrong with Database" + Environment.NewLine);
                            }
                        }
                        else
                        {
                            db.eventname = "Decommission failed," + txtProduct.Text + " is register with innerbox";
                            db.to = txtProduct.Text;
                            db.systemlog();
                            log("Decommission failed," + txtProduct.Text + " is register with innerbox" + Environment.NewLine);
                        }
                    }
                    else
                    {
                        db.eventname = "Decommission failed," + txtProduct.Text + " is not a good product";
                        db.to = txtProduct.Text;
                        db.systemlog();
                        log("Decommission failed," + txtProduct.Text + " is not a good product " + Environment.NewLine);
                    }
                }
                else
                {
                    db.eventname = "Decommission failed," + txtProduct.Text + " Wrong BatchNumber";
                    db.to = txtProduct.Text;
                    db.systemlog();
                    log("Decommission failed," + txtProduct.Text + " Wrong BatchNumber" + Environment.NewLine);

                }
            }
            else
            {
                db.eventname = "Decomission =" + txtProduct.Text + " Not Found";
                db.to = txtProduct.Text;
                db.systemlog();
                log("Decommission failed," + txtProduct.Text + " Not Found" + Environment.NewLine);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            decomision();
        }

        private void Decomission_Load(object sender, EventArgs e)
        {
            txtProduct.Text = "";
            txtProduct.Focus();
        }

        private void log(string line)
        {
            line = Util.Common.SetWithDate(line);
            txtLog.AppendText(line+"\n");
        }

        private void txtProduct_KeyPress(object sender, KeyPressEventArgs e)
        {
            var regex = new Regex(@"[^a-zA-Z0-9\b]");
            if (regex.IsMatch(e.KeyChar.ToString()))
            {
                e.Handled = true;
            }
        }

        //private void loadData()
        //{
        //    List<string> field = new List<string>();
        //    field.Add("gsOneVialId, SUBSTRING(gsOneVialId, CHARINDEX('BIOF', gsOneVialId), 14)");
        //    string where = "batchNumber = '" + cmbBatch.Text + "' and isReject = 0 ";
        //    List<string[]> data = db.selectList(field, "[Vaccine]", where);
        //    AutoCompleteStringCollection namesCollection = new AutoCompleteStringCollection();
        //    for (int i = 0; i < data.Count; i++)
        //    {
        //        namesCollection.Add(data[i][0]);
        //    }
        //    txtProduct.AutoCompleteMode = AutoCompleteMode.None;
        //    txtProduct.AutoCompleteSource = AutoCompleteSource.CustomSource;
        //    txtProduct.AutoCompleteCustomSource = namesCollection;
        //}

        private void cmbBatch_SelectedIndexChanged(object sender, EventArgs e)
        {
            //loadData();
        }

        private void txtProduct_TextChanged(object sender, EventArgs e)
        {
            //listBox1.Items.Clear();
            //if (txtProduct.Text.Length == 0)
            //{
            //    hideResults();
            //    return;
            //}

            //foreach (String ss in txtProduct.AutoCompleteCustomSource)
            //{
            //    if (ss.Contains(txtProduct.Text))
            //    {
            //        listBox1.Items.Add(ss);
            //        listBox1.Visible = true;
            //    }
            //}
        }

        //void hideResults()
        //{
        //    listBox1.Visible = false;
        //}

        //private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    txtProduct.Text = listBox1.Items[listBox1.SelectedIndex].ToString();
        //    hideResults();
        //}

        //void listBox1_LostFocus(object sender, System.EventArgs e)
        //{
        //    hideResults();
        //}
    }
}
