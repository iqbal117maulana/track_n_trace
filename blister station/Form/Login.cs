﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace Blister_Station
{
    public partial class Login : Form
    {
        blister bl;

        public Login()
        {
            InitializeComponent();
            this.Text = this.Text + " V " + System.Windows.Forms.Application.ProductVersion;
        }

        private void buttonLogin_Click(object sender, EventArgs e)
        {
            do_login();
        }

        private void do_login()
        {
            dbaccess db = new dbaccess();
            try
            { //cek useraname
                if (db.validasi(txtUsername.Text))
                {
                    //cek Password
                    if (db.validasi(txtUsername.Text, txtPassword.Text))
                    {
                        List<string[]> res = db.validasi(txtUsername.Text, txtPassword.Text, "0");
                        if (res != null)
                        {
                            string adminid = res[0][0];
                            if (db.cekPermision(adminid, "blister_station", "read"))
                            {
                                string[] lineBottle = db.cekLine("blisterPackStationIp");
                                if (lineBottle.Length > 0)
                                {
                                    List<string[]> resu = db.getRole(adminid);
                                    Dashboard ds = new Dashboard(adminid, lineBottle);
                                    ds.lblUserId.Text = resu[0][0].ToString();
                                    ds.lblRole.Text = resu[0][1].ToString();
                                    ds.Show();
                                    this.Hide();
                                }
                                else
                                {
                                    string ip = db.ipAddress;
                                    Confirm cf = new Confirm("IP Address " + ip + " Is Not Recognize,\n Contact Your Administrator", "Information", MessageBoxButtons.OK);
                                }
                            }
                            else
                            {
                                new Confirm("Dont have permission", "Information", MessageBoxButtons.OK);
                            }
                        }
                        else
                        {
                            new Confirm("User is inactive", "Information", 0);
                            db.simpanLog("Error 0010 : Username dan Password Salah !");
                        }
                    }
                    else
                    {
                        new Confirm("Invalid Password", "Information", MessageBoxButtons.OK);
                        db.simpanLog("Error 0010 : Username dan Password Salah !");
                        txtPassword.Focus();
                        txtPassword.Text = "";
                    }
                }
                else
                {
                    new Confirm("Invalid Username", "Information", MessageBoxButtons.OK);
                    db.simpanLog("Error 0010 : Username Salah !");
                    txtUsername.Focus();
                    clearText();
                }
            }
            catch (ArgumentException ae)
            {
                db.simpanLog("Error " + ae.ToString());
            }
        }

        private void clearText()
        {
            txtUsername.Text = "";
            txtPassword.Text = "";
            txtUsername.Focus();
        }

        private void Login_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                bl.close("");
            }
            catch (Exception ex)
            {
                Application.Exit();
            }
        }

        private void txtUsername_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (txtUsername.Text.Length > 0)
                {
                    this.ActiveControl = txtPassword;
                }
                else
                {
                    new Confirm("Username cannot empty", "Information", MessageBoxButtons.OK);
                    txtUsername.Focus();
                }
            }
        }

        private void txtPassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (txtUsername.Text.Length > 0)
                {
                    if (txtPassword.Text.Length > 0)
                    {
                        do_login();
                    }
                    else
                    {
                        new Confirm("Password cannot empty", "Information", MessageBoxButtons.OK);
                        txtPassword.Focus();
                    }
                }
                else
                {
                    new Confirm("Username cannot empty", "Information", MessageBoxButtons.OK);
                    txtUsername.Focus();
                }
            }
        }

        private void txtUsername_KeyPress(object sender, KeyPressEventArgs e)
        {
            var regex = new Regex(@"[^a-zA-Z0-9\s\b\@\-_\.]");
            if (regex.IsMatch(e.KeyChar.ToString()))
            {
                e.Handled = true;
            }
        }
    }
}
