﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Net;
using System.Net.Sockets;

namespace Blister_Station
{
    public partial class blister : Form
    {
        List<string> dataBlister = new List<string>();
        sqlitecs sqlite;
        generateCode Code;
        dbaccess db;
        Control ms;
        bool audit = false;
        bool start = false;
        string admin;

        public Dashboard dbss;
        string parameter;
        public ListBatchNumber lbn;
        string[] linenumber;

        //tambahan disable close button
        private const int CP_NOCLOSE_BUTTON = 0x200;
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams myCp = base.CreateParams;
                myCp.ClassStyle = myCp.ClassStyle | CP_NOCLOSE_BUTTON;
                return myCp;
            }
        }
        //end of disable close button

        public blister(Dashboard dbs, string adminid, string[] line, string param, Control ctrl)
        {
            InitializeComponent();
            this.Text = this.Text + " V " + System.Windows.Forms.Application.ProductVersion;
            sqlite = new sqlitecs();
            Code = new generateCode();
            db = new dbaccess();
            ms = ctrl;
            ms.sqlite = sqlite;
            ms.bl = this;
            db.adminid = adminid;
            ms.db = db;
            ms.Code = Code;
            ms.adminid = adminid;
            this.admin = adminid;
            ms.linename = line[0];
            ms.linenumber = line[1];
            ms.startStation();
            lblLine.Text = line[0];
           // ms.delay();

            cekDevice();
            cekCamera();
            cekDB();
            dbss = dbs;
            parameter = param;
            linenumber = line;
            lblBatch.Text = "";
            lblProductName.Text = "";
        }

        public void close(string ti)
        {
            try
            {
                if (InvokeRequired)
                {
                    this.Invoke(new Action<string>(close), new object[] { ti });
                    return;
                }
                if(ms.srv.isRunning)
                {
                    Console.WriteLine("Masuk Is Running");
                    ms.srv.closeServer("");
                }
                Application.ExitThread();
                Application.Exit();
                Environment.Exit(Environment.ExitCode);
            }
            catch (Exception ex)
            {
            }
        }

        public void setLabelBufferPrinter(string ti)
        {
            try
            {
                if (InvokeRequired)
                {
                    this.Invoke(new Action<string>(setLabelBufferPrinter), new object[] { ti });
                    return;
                }
                lblBufferData.Text = ti;

            }
            catch (Exception ex)
            {
            }
        }

        public void close2(string ti)
        {
            try
            {
                if (InvokeRequired)
                {
                    this.Invoke(new Action<string>(close), new object[] { ti });
                    return;
                }
                if (ms.srv.isRunning)
                {
                    ms.srv.closeServer("");
                }
            }
            catch (Exception ex_a)
            {
            }
        }
        private void blister_FormClosed(object sender, FormClosedEventArgs e)
        {
            close("");
        }

        private void cmbBatch_SelectedIndexChanged(object sender, EventArgs e)
        {
            audit = false;
            btnAudit.Text = "Pre-Check";
            ms.SetEmptyDataGridReady();
            // ms.getData();
        }

        private bool cekDevice()
        {
            print pr = new print();
            if (pr.cekKoneksi())
            {
                lblPrinter.Text = "Online";
                onPrinter.Visible = true;
                offPrinter.Visible = false;
                return true;
            }
            else
            {
                lblPrinter.Text = "Offline";
                onPrinter.Visible = false;
                offPrinter.Visible = true;
            }
            return false;
        }


     

        private void button2_Click(object sender, EventArgs e)
        {
            Confirm cf = new Confirm("Are you sure? ", "Confirmation");
            bool hasilDialog = cf.conf();
            if (hasilDialog)
            {
                if (!start)
                {
                    if (cekCamera())
                    {
                        if (cekDevice())
                        {
                            if (cekPLCConnect())
                            {
                                if (cekDB())
                                {
                                    //if (cmbBatch.SelectedIndex > -1)
                                    //{

                                    //ms.batch = ms.datacombo[cmbBatch.SelectedIndex][0];
                                    //  ms.SendClearBufferPrinter();
                                    ms.batch = txtBatchNumber.Text;
                                    ms.SetParameterStartProduction();
                                    ms.disable();
                                    btnAudit.Enabled = false;
                                    start = true;
                                    //cmbBatch.Enabled = false;
                                    txtBatchNumber.Enabled = false;
                                    btnStart.Text = "Stop";
                                    btnStart.Image = global::Blister_Station.Properties.Resources.stop;
                                    ms.SetDataGridReady("0", "");
                                    //}
                                    //else
                                    //    new Confirm("Batchnumber not recognition", "Information", MessageBoxButtons.OK);
                                }
                            }
                            else
                            {
                                new Confirm("PLC Not Connected!", "Information", MessageBoxButtons.OK);
                            }
                        }
                        else
                        {
                            new Confirm("Printer not ready!", "Information", MessageBoxButtons.OK);
                        }

                    }
                }
                else
                {
                    ms.SetStopStation();
                    btnStart.Text = "Start GS1";
                    btnStart.Image = global::Blister_Station.Properties.Resources.start;
                    btnAudit.Enabled = true;
                    start = false;
                    ms.enable();
                }
            }
    
    

//# Di CR kan
//            Confirm cf = new Confirm("Are you sure? ", "Confirmation");
//            bool hasilDialog = cf.conf() ;
//            if (hasilDialog)
//            {
//                if (cekCamera())
//                {
//                    if (cekDevice())
//                    {
//                        if (cmbBatch.SelectedIndex > -1)
//                        {
//                            if (!start)
//                            {
//                                ms.batch = ms.datacombo[cmbBatch.SelectedIndex][0];
//                                ms.clearDataReceive();
//                                ms.sendStartPrinter();
//                                ms.disable();
//                                btnAudit.Enabled = false;
//                                start = true;
//                                cmbBatch.Enabled = false;
//                                btnStart.Text = "Stop";
//                                ms.SetDataGridSend("0", "");
//                            }
//                            else
//                            {
//                                ms.Stop();
//                                btnAudit.Enabled = true;
//                                start = false;
//                                ms.enable();
//                            }
//                        }
//                        else
//                        {
//                            new Confirm("Batchnumber not recognition", "Information", MessageBoxButtons.OK);
//                        }
//                    }
//                    else
//                        new Confirm("Printer not ready!", "Information", MessageBoxButtons.OK);
//                }
//            }
        }
        private bool Is_FirstConnect = false;
        private bool cekPLCConnect()
        {
            if (!Is_FirstConnect)
            {
                Is_FirstConnect = ms.checkPLCConnected();
                return Is_FirstConnect;
            }
            else
                return true;
        }


        private bool cekCamera()
        {
            if (ms.PingHost(sqlite.config["ipCamera"]))
            {
                lblCamera.Text = "Online";
                onCamera1.Visible = true;
                offCamera1.Visible = false;

                if (ms.PingHost(sqlite.config["ipCamera2"]))
                {
                    lblCamera2.Text = "Online";
                    onCamera2.Visible = true;
                    offCamera2.Visible = false;

                    if (ms.PingHost(sqlite.config["ipCamera3"]))
                    {
                        lblCamera3.Text = "Online";
                        onCamera3.Visible = true;
                        offCamera3.Visible = false;
                        return true;
                    }
                    else
                    {
                        lblCamera3.Text = "Offline";
                        onCamera3.Visible = false;
                        offCamera3.Visible = true;
                        new Confirm("Camera 3 not connected");
                    }
                }
                else
                {
                    lblCamera2.Text = "Offline";
                    onCamera2.Visible = false;
                    offCamera2.Visible = true;
                    new Confirm("Camera 2 not connected");
                }
            }
            else
            {
                lblCamera.Text = "Offline";
                onCamera1.Visible = false;
                offCamera1.Visible = true;
                new Confirm("Camera 1 not connected");
            }
            return false;
        }

        public bool cekDB()
        {
            if (db.Connect())
            {
                lblDb.Text = "Online";
                onDb.Visible = true;
                offDb.Visible = false;
                ms.StopServer();
                ms.StartServer2();
                db.closeConnection();
                return true;
            }
            else
            {
                lblDb.Text = "Offline";
                onDb.Visible = false;
                offDb.Visible = true;
                ms.StopServer();
                return false;
            }
        }

        private void stopPrinting()
        {
            ms.start = false;
            ms.enable();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Confirm cf = new Confirm("Are you sure? ", "Confirmation");
            bool result = cf.conf();
            if (result)
            {
                //close2("");
                //if (db.Connect())
                //{
                //    db.eventname = "Logout Blister station " + admin;
                //    db.systemlog();
                //    db.closeConnection();
                //}
                //close("");
                //Environment.Exit(Environment.ExitCode);
                Application.Exit();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            config cfg = new config(this,1,lblAdmin.Text);
            cfg.ShowDialog();
        }

        internal void clearBuffer()
        {
            print pr2 = new print();
            pr2.cekKoneksi();
            pr2.clearBuffer();
            pr2.disconect();
            pr2 = null;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Confirm cf = new Confirm("Are you sure? ", "Confirmation");
            bool hasilDialog = cf.conf() ;
            if (hasilDialog)
            {
                print pr = new print();

                //if (cmbBatch.SelectedIndex > -1)
                //{
                    if (pr.cekKoneksi())
                    {
                        pr.disconect();
                        //ms.batch = ms.datacombo[cmbBatch.SelectedIndex][0];
                        ms.batch = txtBatchNumber.Text;
                        if (sqlite.config["debug"].Equals("0"))
                            ms.SendClearBufferPrinter();
                        ms.SetEmptyDataGridReceive();
                        ms.SetPreCheck();
                        audit = true;
                        ms.SendToPrinter(ms.getDataSendToPrinter(int.Parse(sqlite.config["audit"])));
                        ms.SetDataGridReady("0", "");
                        //Confirm ConfirmDialog = new Confirm("Pre-Check Success? ", "Confirmation");
                        //bool hasilDialog1 = ConfirmDialog.conf();
                        //if (hasilDialog1)
                        //{
                        //    btnStart.Enabled = true;
                        //}
                        //else
                        //{
                        //    btnStart.Enabled = false;
                        //}
                    }
                    else
                    {
                        cf = new Confirm("Printer not connected!", "Information", MessageBoxButtons.OK);
                    }
                //}
                //else
                //{
                //    cf = new Confirm("Batchnumber Closed from administrator", "Information", MessageBoxButtons.OK);
                //}

                //if (!audit)
                //{
                //    ms.batch = ms.datacombo[cmbBatch.SelectedIndex][0];
                //    ms.startAudit();
                //    btnAudit.Text = "Send Audit";
                //    lblQtySend.Text = "" + dgvSend.Rows.Count;
                //    audit = true;
                //}
                //else
                //{
                //    ms.SendToPrinter(ms.getDataSendToPrinter(int.Parse(sqlite.config["audit"])));
                //    btnStart.Enabled = true;
                //    audit = false;
                //    btnAudit.Text = "Pre-check";
                //    ms.SetDataGridSend("0", "");
                //}
            }

        }

        private void btnDeo_Click(object sender, EventArgs e)
        {
            Decomission dc = new Decomission(lblAdmin.Text, ms.linenumber, parameter);
            dc.ShowDialog();
        }

        private void label7_Click(object sender, EventArgs e)
        {
           // db.dataVaccineResult("7832589872400200631", sqlite);
        }

        private void dgvSend_ColumnAdded(object sender, DataGridViewColumnEventArgs e)
        {
            lblQtySend.Text = "" + dgvSend.Rows.Count;
        }

        private void dgvSend_SelectionChanged(object sender, EventArgs e)
        {
            lblQtySend.Text = "" + dgvSend.Rows.Count;
        }

        private void dgvSend_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            lblQtySend.Text = "" + dgvSend.Rows.Count;
        }

        private void dgvReceive_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            //lblQuantityReceive.Text = "" + dgvReceive.Rows.Count;
        }

        private void button1_Click_2(object sender, EventArgs e)
        {
            Sample smp = new Sample(lblAdmin.Text);
            smp.ShowDialog();
        }

        public void refresh()
        {
            sqlite = new sqlitecs();
            db = new dbaccess();
            ms.SetParameterConfig();
            ms.refresh();
        }

        private void button1_Click_3(object sender, EventArgs e)
        {
            commission dc = new commission(lblAdmin.Text, parameter, ms.linenumber);
            dc.ShowDialog();
        }

        //private void button1_Click_4(object sender, EventArgs e)
        //{
        //    txtLog1.Text = "";
        //    ms.refresh();
        //}

        private void dgvStatus_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
         //   lblQtyReceive.Text = ""+dgvStatus.RowCount;
        }

        private void btnLog_Click(object sender, EventArgs e)
        {
            Log lg = ms.lg;
            lg.Show();
        }

        private void btnIndicator_Click(object sender, EventArgs e)
        {
            cekCamera();
            cekDevice();
            cekDB();
        }

        private void pictureBox1_DoubleClick(object sender, EventArgs e)
        {
            close2("");
            dbss.Show();
            this.Hide();
        }

        private void pictureBox1_MouseEnter(object sender, EventArgs e)
        {
            this.pictureBox1.BackColor = ColorTranslator.FromHtml("#00505E");
        }

        private void pictureBox1_MouseLeave(object sender, EventArgs e)
        {
            this.pictureBox1.BackColor = ColorTranslator.FromHtml("#002F36");
        }

        private void txtBatchNumber_Click(object sender, EventArgs e)
        {
            lbn = new ListBatchNumber(null, linenumber[1], parameter, "");
            lbn.ShowDialog();
            txtBatchNumber.Text = lbn.batch;
            int[] cnt = ms.getCount(lbn.batch);
            lblQuantityGood.Text = cnt[0].ToString();
            lblQuantityReject.Text = cnt[1].ToString();
            lblQuantityReceive.Text = cnt[2].ToString();
            btnAudit.Focus();
            if (txtBatchNumber.Text.Length > 0)
            {
                btnAudit.Enabled = true;
                btnStart.Enabled = true;
            }
            else
            {
                btnAudit.Enabled = false;
                btnStart.Enabled = false;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            lblTime.Text = DateTime.Now.ToLongTimeString();
            timer1.Start();
        }

        private void blister_Load(object sender, EventArgs e)
        {
            timer1.Start();
            lblTime.Text = DateTime.Now.ToLongTimeString();
            lblDate.Text = DateTime.Now.ToShortDateString();
        }
    }
}
