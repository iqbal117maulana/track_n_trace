﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Data;
using System.Net.NetworkInformation;
using System.ComponentModel;
using System.Text.RegularExpressions;

namespace Blister_Station
{
    public class Control
    {
        public const string NUMBER_CODE = "02";
        public const string NAMA_FIELD = "blisterPackId";
        public const string NAMA_TABLE_TEMP = "tmp_blisterPack_id";
        public const string NAMA_TABLE_UTAMA = "blisterpack";
        public const string NAMA_STATION = "BLISTER";
        public const string NAMA_SEQ = "BLISTERSEQ";
        public char delimit = ',';
        public int qtyGenerateCode;
        public int qtySendToPrinter;


        SendData_PLC sendData_plc;
        public result rs;
        public sqlitecs sqlite;
        public dbaccess db;
        public blister bl;
        public generateCode Code;
        private Thread serverThread;
        private Thread serverThread2;
        private Thread serverThread3;
        private Thread serverThreadCamera;
        public string batch;
        public string adminid;
        public System.Timers.Timer delayTimer;
        public System.Timers.Timer delaydevice;
        public string linenumber;
        public string linename;
        public List<string[]> datacombo;
        public bool start;
        public bool startDelay;
        public int templastcountkirim;
        public List<string> dataSendToPrinter;
        public string productmodelid;
        public string[] dataPackagingOrder;
        public int countVaccine = 0;
        bool audit = false;
        print prin2;
        int quantityReceive = 0;
        int qtyNow = 0;
        DataTable tableProductReceive = new DataTable();
        DataTable tableProductSend = new DataTable();
        List<string> dataVaccine;
        int tempTerima;
        int tempTerimaKirimUlang = 1;
        int qtySend = 0;
        int qtyReceive = 0;
        string exp;
        bool buffer=false;

        // untuk mengirim ke printer dengan parameter
        int countRepeatSend = 0;
        int countRepeatDelete = 0;

        private BackgroundWorker worker = new BackgroundWorker();
        private AutoResetEvent resetEvent = new AutoResetEvent(false);

        public Mod_Server srv;
        public server srv2;
        public server srv3;
        string adminName;
        DataTable tableTerimaCamera = new DataTable();

        public Log lg = new Log();

        public void MulaiServer()
        {
            srv = new Mod_Server();
            srv.Start(this, sqlite.config["portserver"]);
        }

        public void StopServer()
        {
            srv.listener.Stop();
        }

        public void StartServer2()
        {
            srv.listener.Start();
        }


        public void disable()
        {
            bl.btnStart.Text = "Stop";
            bl.btnAudit.Enabled = false;
            bl.btnDeo.Enabled = false;
            bl.btnLogout.Enabled = false;
            bl.btnConfig.Enabled = false;
            bl.btnSample.Enabled = false;
            bl.btnCommision.Enabled = false;
            bl.btnIndicator.Enabled = false;
        }

        public void enable()
        {
            bl.btnStart.Text = "Start";
            bl.btnAudit.Enabled = true;
            bl.btnDeo.Enabled = true;
            bl.btnLogout.Enabled = true;
            bl.btnConfig.Enabled = true;
            //bl.btnStart.Enabled = false;
            bl.btnSample.Enabled = true;
            bl.btnCommision.Enabled = true;
            bl.btnIndicator.Enabled = true;
        }

        //public void SetComboBoxBatchNumber()
        //{
        //    List<string> field = new List<string>();
        //    field.Add("batchNumber,productModelId");
        //    string where = "status = '1' and linepackagingid = '" + linenumber + "'";
        //    datacombo = db.selectList(field, "[packaging_order]", where);
        //    List<string> da = new List<string>();
        //    for (int i = 0; i < datacombo.Count; i++)
        //    {
        //        da.Add(datacombo[i][0]);
        //    }
        //    bl.cmbBatch.DataSource = da;
        //}

        public void log(string data)
        {
            try
            {
                if (lg.txtLog.InvokeRequired)
                {
                    lg.txtLog.Invoke(new Action<string>(log), new object[] { data });
                    return;
                }
                string dates = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                string simpan = dates + "\t" + data + "\n";
                lg.txtLog.AppendText(simpan + Environment.NewLine);
            }
            catch (ObjectDisposedException ob)
            {

            }
        }

        public bool GetStatusPrinterReady()
        {
            print pr = new print();
            if (pr.cekKoneksi())
            {
                if (pr.cekStatusPrinter())
                {
                    bl.lblPrinter.Text = pr.statusLeibinger;
                    return true;
                }
            }
            Confirm cf = new Confirm("Printer Not Connected" + pr.statusLeibinger, "Information", MessageBoxButtons.OK);
            bl.lblPrinter.Text = pr.statusLeibinger;
            return false;
        }

        public void startStation()
        {
            db.adminid = adminid;
            adminName = GetAdminName(adminid);
            db.adminname = adminName;
            bl.lblAdmin.Text = adminName;
            db.from = "Blister Station";
            db.eventname = "Start Blister Station" + adminid;
            db.systemlog();
            //CekDevice();
            log("Server Port : " + sqlite.config["portserver"]);
            rs = new result();
            //SetComboBoxBatchNumber();
            setTableReceive();
            setTableSend();
            SetParameterConfig();

            sendData_plc = new SendData_PLC();
            sendData_plc.IPAddress = sqlite.config["IpPLC"];
            sendData_plc.Port = sqlite.config["PortPLC"];
            sendData_plc.Timeout = "5";
            //delay();
            GetStatusConfigBuffer();
            serverThread = new Thread(new ThreadStart(MulaiServer));
            serverThread.Start();
            //setParameterBackgroudWorker();
        }

        private void GetStatusConfigBuffer()
        {
            if (sqlite.config["buffer"].Equals("0"))
            {
                buffer = false;
            }
            else
            {
                buffer = true;
            }
        }

        public void SetParameterConfig()
        {
            sqlite = new sqlitecs();
            qtyGenerateCode = int.Parse(sqlite.config["qtygenerate"]);
            qtySendToPrinter = int.Parse(sqlite.config["qtysend"]);
            qtySend = int.Parse(sqlite.config["qtysend"]);
            //CekDevice();
        }

        public void setTableReceive()
        {
            tableProductReceive = new DataTable();
            tableProductReceive.Columns.Add("Blister ID");
            tableProductReceive.Columns.Add("Cap ID");
            tableProductReceive.Columns.Add("Timestamp");
            tableProductReceive.Columns.Add("Status");
            bl.dgvReceive.DataSource = tableProductReceive;
        }

        public void setTableSend()
        {
            tableProductSend = new DataTable();
            tableProductSend.Columns.Add("Blister ID");
            bl.dgvSend.DataSource = tableProductSend;
        }

        private bool GetStatusDataSample(string blisterid)
        {
            List<string> field = new List<string>();
            field.Add("blisterpackid");
            string where = "flag = 1 And blisterPackId ='" + blisterid + "'";
            List<string[]> ds = sqlite.select(field, "tmp_blisterPack_id", where);
            if (sqlite.num_rows > 0)
            {
                return true;
            }
            return false;
        }

        bool GetExistBlister(string data)
        {
            List<string> fieldCekData = new List<string>();
            fieldCekData.Add(NAMA_FIELD);
            string where1 = NAMA_FIELD+" like'%" + data + "%'";
            List<string[]> dataCapid = db.selectList(fieldCekData, "blisterpack", where1);
            if (db.num_rows > 0)
            {
                return false;
            }
            return true;
        }

        //public string[] InsertBlister(string[] hasilsplit)
        //{
        //    Var.Var_Blister var_blister = new Var.Var_Blister();
        //    var_blister.blisterpackId = hasilsplit[1];

        //    if (!var_blister.GetExistBlisterPack())
        //    {
        //        var_blister.isReject = hasilsplit[0];
        //        var_blister.batchnumber = batch;

        //        if (hasilsplit[0].Equals("1"))
        //        {
        //            var_blister.isReject = db.getUnixString();
        //        }

        //        if (GetStatusDataSample(hasilsplit[1]))
        //        {
        //            var_blister.flag = "1";
        //        }
        //        else
        //        {
        //            var_blister.flag = "0";
        //        }
        //        var_blister.AddBlisterPack();

        //        // update table Vaccine
        //        Dictionary<string, string> field;
        //        List<string> fieldList = new List<string>();
        //        fieldList.Add("capid");
        //        for (int i = 2; i < hasilsplit.Length; i++)
        //        {
        //            if (!hasilsplit[i].Equals("XXXXXXX"))
        //            {
        //                if (!hasilsplit[i].Equals("xxxxxxx"))
        //                {
        //                    // cek data yang ada di database
        //                    string where1 = "capid like'%" + hasilsplit[i] + "%' and isreject = 0 and batchnumber ='" + batch + "'";
        //                    List<string[]> dataCapid = db.selectList(fieldList, "Vaccine", where1);
        //                    if (db.num_rows > 0)
        //                    {
        //                        string where2 = "capid like'%" + hasilsplit[i] + "%' and isreject = 0 and batchnumber ='" + batch + "' and blisterpackId is null";
        //                        List<string[]> dataCapid1 = db.selectList(fieldList, "Vaccine", where2);
        //                        if (db.num_rows > 0)
        //                        {
        //                            //masukan datanya
        //                            field = new Dictionary<string, string>();
        //                            field.Add(NAMA_FIELD, hasilsplit[1]);
        //                            field.Add("isreject", "0");
        //                            db.update(field, "vaccine", "capid like'%" + hasilsplit[i] + "%' and batchnumber ='" + batch + "'");
        //                            db.Movement(batch, hasilsplit[i], hasilsplit[1], "1", "");
        //                        }
        //                        else
        //                        {
        //                            log("data capid: " + hasilsplit[i] + " Already Registered");
        //                            hasilsplit[0] = "4";
        //                            InsertToDatabaseReject(hasilsplit[i], hasilsplit[1], hasilsplit[0]);
        //                            var_blister.isReject = "1";
        //                            var_blister.rejectTime = db.getUnixString();
        //                            var_blister.batchnumber = batch;
        //                            var_blister.UpdateBlisterPack();

        //                            Dictionary<string, string> fieldBlister = new Dictionary<string, string>();
        //                            fieldBlister.Add("blisterpackid", "null");
        //                            db.update(fieldBlister, "vaccine", NAMA_FIELD + " ='" + hasilsplit[1] + "' and batchnumber='" + batch + "'");
        //                            break;
        //                        }
        //                    }
        //                    else
        //                    {
        //                        log("data capid: " + hasilsplit[i] + " Not Found");
        //                        hasilsplit[0] = "2";
        //                        InsertToDatabaseReject(hasilsplit[i], hasilsplit[1], "6");
        //                        var_blister.isReject = "1";
        //                        var_blister.rejectTime = db.getUnixString();
        //                        var_blister.batchnumber = batch;
        //                        var_blister.UpdateBlisterPack();

        //                        Dictionary<string, string> fieldBlister = new Dictionary<string, string>();
        //                        fieldBlister.Add("blisterpackid", "null");
        //                        db.update(fieldBlister, "vaccine", NAMA_FIELD + " ='" + hasilsplit[1] + "' and batchnumber='" + batch + "'");
        //                        break;
        //                    }
        //                }
        //                else
        //                {
        //                    log("data capid: " + hasilsplit[i] + " Not Found");
        //                    hasilsplit[0] = "2";
        //                    InsertToDatabaseReject(hasilsplit[i], hasilsplit[1], "6");
        //                    var_blister.isReject = "1";
        //                    var_blister.rejectTime = db.getUnixString();
        //                    var_blister.batchnumber = batch;
        //                    var_blister.UpdateBlisterPack();

        //                    Dictionary<string, string> fieldBlister = new Dictionary<string, string>();
        //                    fieldBlister.Add("blisterpackid", "null");
        //                    db.update(fieldBlister, "vaccine", NAMA_FIELD + " ='" + hasilsplit[1] + "'  and batchnumber='" + batch + "'");
        //                    break;
        //                }
        //            }
        //            else
        //            {
        //                log("data capid: " + hasilsplit[i] + " Not Found");
        //                hasilsplit[0] = "2";
        //                InsertToDatabaseReject(hasilsplit[i], hasilsplit[1], "6");
        //                var_blister.isReject = "1";
        //                var_blister.rejectTime = db.getUnixString();
        //                var_blister.batchnumber = batch;
        //                var_blister.UpdateBlisterPack();

        //                Dictionary<string, string> fieldBlister = new Dictionary<string, string>();
        //                fieldBlister.Add("blisterpackid", "null");
        //                db.update(fieldBlister, "vaccine", NAMA_FIELD + " ='" + hasilsplit[1] + "' and batchnumber='" + batch + "'");
        //                break;
        //            }
        //        }
        //    }
        //    else
        //    {
        //        hasilsplit[0] = "3";
        //        for (int i = 2; i < hasilsplit.Length; i++)
        //        {
        //            InsertToDatabaseReject(hasilsplit[i], hasilsplit[1], hasilsplit[0]);
        //        }
        //        log("Data Blister: " + hasilsplit[1] + " is Already Exist and Not Progress");
        //    }
        //    return hasilsplit;
        //}

        public string[] 
            InsertBlister(string[] hasilsplit)
        {
            Var.Var_Blister var_blister = new Var.Var_Blister();
            //string bls = RemoveCharacter(hasilsplit[1]);
            //var regex = new Regex(@"[^a-zA-Z0-9]");
            
            //if (bls.Equals("") || bls.Length < 2)
            //{
                //if (regex.IsMatch(bls))
                //{
                    var_blister.blisterpackId = hasilsplit[1];
                    if (!var_blister.GetExistBlisterPack())
                    {
                        var_blister.isReject = hasilsplit[0];
                        var_blister.batchnumber = batch;

                        if (hasilsplit[0].Equals("1"))
                        {
                            var_blister.isReject = db.getUnixString();
                        }

                        if (GetStatusDataSample(hasilsplit[1]))
                        {
                            var_blister.flag = "1";
                        }
                        else
                        {
                            var_blister.flag = "0";
                        }
                        var_blister.AddBlisterPack();

                        // update table Vaccine
                        Dictionary<string, string> field;
                        List<string> fieldList = new List<string>();
                        fieldList.Add("capid");
                        string where1 = "capid in( ";
                        List<string> hasilsplitMv = new List<string>();
                        for (int i = 2; i < hasilsplit.Length; i++)
                        {
                            where1 += "'" + hasilsplit[i] + "'";
                            if (i + 1 != hasilsplit.Length)
                            {
                                where1 += ",";
                            }
                            hasilsplitMv.Add(hasilsplit[i].ToString());
                        }

                        where1 += ") and isreject = 0 and batchnumber ='" + batch + "' and blisterpackId is null";
                        List<string[]> dataCapid = db.selectList(fieldList, "Vaccine", where1);
                        if (db.num_rows == 5)
                        {
                            field = new Dictionary<string, string>();
                            field.Add(NAMA_FIELD, hasilsplit[1]);
                            field.Add("isreject", "0");
                            if (db.update(field, "vaccine", where1))
                            {
                                db.Movement2(batch, hasilsplitMv, hasilsplit[1], "1", "");
                            }
                            else
                            {
                                sendToPLC();
                            }
                        }
                        else if (db.num_rows < 5)
                        {
                            log("Data Capid Not Found!");
                            hasilsplit[0] = "2";
                            for (int i = 0; i < hasilsplitMv.Count; i++)
                            {
                                InsertToDatabaseReject(hasilsplitMv[i], hasilsplit[1], hasilsplit[0]);
                            }
                            var_blister.isReject = "1";
                            var_blister.rejectTime = db.getUnixString();
                            var_blister.batchnumber = batch;
                            var_blister.UpdateBlisterPack();
                        }
                        else
                        {
                            log("Data Capid Already Registered!");
                            hasilsplit[0] = "4";
                            for (int i = 0; i < hasilsplitMv.Count; i++)
                            {
                                InsertToDatabaseReject(hasilsplitMv[i], hasilsplit[1], hasilsplit[0]);
                            }
                            var_blister.isReject = "1";
                            var_blister.rejectTime = db.getUnixString();
                            var_blister.batchnumber = batch;
                            var_blister.UpdateBlisterPack();
                        }
                    }
                    else
                    {
                        //error ga masuk kesini
                        hasilsplit[0] = "3";
                        for (int i = 2; i < hasilsplit.Length; i++)
                        {
                            InsertToDatabaseReject(hasilsplit[i], hasilsplit[1], hasilsplit[0]);
                        }
                        log("Data Blister: " + hasilsplit[1] + " is Already Exist and Not Progress");
                    }
                //}
                //else
                //{
                //    hasilsplit[0] = "6";
                //    for (int i = 2; i < hasilsplit.Length; i++)
                //    {
                //        InsertToDatabaseReject(hasilsplit[i], hasilsplit[1], hasilsplit[0]);
                //    }
                //    log("Data Blister is Not Recognize");
                //}
            //}
            //else
            //{
            //    hasilsplit[0] = "6";
            //    for (int i = 2; i < hasilsplit.Length; i++)
            //    {
            //        InsertToDatabaseReject(hasilsplit[i], hasilsplit[1], hasilsplit[0]);
            //    }
            //    log("Data Blister is Not Recognize");
            //}
            return hasilsplit;
        }

        public void InsertToBufferData(string[] hasilsplit)
        {
            // tambah di table result
            Dictionary<string, string> field;
            List<string> fieldList = new List<string>();
            fieldList.Add("capid");
            for (int i = 2; i < hasilsplit.Length; i++)
            {
                // cek data yang ada di database
                string where1 = "capid ='" + hasilsplit[i] + "'";
                List<string[]> dataCapid = db.selectList(fieldList, "Vaccine", where1);
                if (db.num_rows > 0)
                {
                    //masukan datanya
                    field = new Dictionary<string, string>();
                    field.Add(NAMA_FIELD, hasilsplit[1]);
                    field.Add("flag", hasilsplit[0]);
                    field.Add("isreject", "1");
                    field.Add("createdtime", db.getUnixString());
                    field.Add("capid", hasilsplit[i]); 
                    if (GetStatusDataSample(hasilsplit[1]))
                    {
                        field.Add("sample", "1");
                    }
                    else
                    {
                        field.Add("sample", "0");
                    }
                    rs.insert(field, "result");
                }
                else
                {
                    log("data capid: " + hasilsplit[i] + " Not Found");
                }
            }
            //hapus yang ada di tmp
            string where = NAMA_FIELD + " ='" + hasilsplit[1] + "'";
            sqlite.delete(where, NAMA_TABLE_TEMP);
        }

        //public void InsertToBufferData(string[] hasilsplit)
        //{
        //    // tambah di table result
        //    Dictionary<string, string> field;
        //    List<string> fieldList = new List<string>();
        //    fieldList.Add("capid");
        //    for (int i = 2; i < hasilsplit.Length; i++)
        //    {
        //        // cek data yang ada di database
        //        string where1 = "capid ='" + hasilsplit[i] + "'";
        //        List<string[]> dataCapid = db.selectList(fieldList, "Vaccine", where1);
        //        if (db.num_rows > 0)
        //        {
        //            //masukan datanya
        //            field = new Dictionary<string, string>();
        //            field.Add(NAMA_FIELD, hasilsplit[1]);
        //            field.Add("flag", hasilsplit[0]);
        //            field.Add("isreject", "1");
        //            field.Add("createdtime", db.getUnixString());
        //            field.Add("capid", hasilsplit[i]);
        //            if (GetStatusDataSample(hasilsplit[1]))
        //            {
        //                field.Add("sample", "1");
        //            }
        //            else
        //            {
        //                field.Add("sample", "0");
        //            }
        //            rs.insert(field, "result");
        //        }
        //        else
        //        {
        //            log("data capid: " + hasilsplit[i] + " Not Found");
        //        }
        //    }
        //    //hapus yang ada di tmp
        //    string where = NAMA_FIELD + " ='" + hasilsplit[1] + "'";
        //    sqlite.delete(where, NAMA_TABLE_TEMP);
        //}

        int qtyGood = 0;
        int qtyFail = 0;

        //cek status
        public string GetStatusReceiveData(string data)
        {

            if (data.Equals("0"))
            {
                qtyGood++;
                return "Pass";
            }
            else if (data.Equals("1"))
            {
                qtyFail++;
                return "Fail";
            }
            else if (data.Equals("2"))
            {
                //qtyGood
                qtyFail++;
                sendToPLC();
                return "Not Found";
            }
            else if (data.Equals("3"))
            {
                //qtyGood++;
                qtyFail++;
                sendToPLC();
                return "Blister Already Exist";
            }
            else if (data.Equals("4"))
            {
                //qtyGood++;
                qtyFail++;
                sendToPLC();
                return "Capid Already Registered";
            }
            else if (data.Equals("5"))
            {
                //qtyGood++;
                //qtyFail++;
                sendToPLC();
                return "DB Not Connected";
            }
            else
            {
                qtyFail++;
                sendToPLC();
                return "Not Recognize";
            }
        }

        private void sendToPLC()
        {
            sendData_plc.TextData = "1";
            sendData_plc.SendText();
        }

        string RemoveCharacter(string data)
        {
            data = data.Replace(" ", "");
            data = data.Replace("\0", "");
            data = data.Replace("\n", "");
            data = data.Replace("\t", "");
            return data;
        }

        // Terima data dari PLC
        public void ReceiveData(string data)
        {
            try
            {
                if (bl.InvokeRequired)
                {
                    bl.Invoke(new Action<string>(ReceiveData), new object[] { data });
                    return;
                }
                if (data.Length > 0)
                {
                    log("Receive Data:" + data);
                    db.simpanLog("Proses Data: " + data);
                    data = data.Replace("\r", "");
                    data = data.Replace("\n", "");
                    data = data.Replace("]d2", "");
                    data = data.Replace("$", "");
                    data = data.Replace("?", "");
                    data = data.Replace(" ", "");
                    List<string> dataMas = GetSplitReceiveData(data);
                    for (int i = 0; i < dataMas.Count; i++)
                    {
                        quantityReceive++;
                        qtyNow++;
                        bl.lblQuantityReceive.Text = "" + quantityReceive;
                        bl.lblQtyNow.Text = "" + qtyNow;
                        log("Proses Data: " + dataMas[i]);
                        db.simpanLog("Proses Data: " + dataMas[i]);
                        if (data.Length > 0)
                        {
                            string[] hasilSplit = dataMas[i].Split(delimit);
                            if (bl.cekDB())
                            {
                                //Console.WriteLine("KONEKKKKK");
                                if (hasilSplit[0].Equals("0"))
                                {
                                    if (buffer)
                                    {
                                        //Console.WriteLine("MASUK BUFFER");
                                        InsertToBufferData(hasilSplit);
                                    }
                                    else
                                    {
                                        //Console.WriteLine("MASUK OKE TIDAK BUFFER");
                                        hasilSplit = InsertBlister(hasilSplit);
                                    }
                                }
                                if (!hasilSplit[0].Equals("0"))
                                {
                                    hasilSplit = InsertBlisterFail(hasilSplit);
                                    log("Blister ID " + hasilSplit[1] + " fail");
                                    db.simpanLog("Blister ID " + hasilSplit[1] + " fail");
                                }
                            }
                            else
                            {
                                StopServer();
                                //Console.WriteLine("TIDAK KONEKKKKK");
                                hasilSplit[0] = "5";
                            }
                            SetDataGridReceive(hasilSplit);
                        }
                        else
                        {
                            log("Data invalid");
                        }
                        // cek Kirim ulang
                        if (auditStarted)
                            CheckSendDataPrinter();
                    }
                }
                else
                {
                    log("Data receive empty");
                    db.simpanLog("Data receive empty");
                }
            }
            catch (Exception te)
            {
                log("Data " + data + " invalid");
            }
        }


        public void CheckSendDataPrinter()
        {
            if (tempTerimaKirimUlang >= int.Parse(sqlite.config["qtysend"]))
            {
                SendToPrinter(getDataSendToPrinter(int.Parse(sqlite.config["qtysend"])));
                tempTerimaKirimUlang = 1;
                SetDataGridReady("0", "");
            }
            else
            {
                tempTerimaKirimUlang++;
            }
        }

        private List<string> GetSplitReceiveData(string data)
        {
            char[] delimiters = new char[] { '|' };
            List<string> hasilSplit = new List<string>(data.Split(delimiters, StringSplitOptions.RemoveEmptyEntries));
            return hasilSplit;
        }
       
        DataTable tableProductFail = new DataTable();
        int quantityTotalReceive;

        public void SetDataGridReceive(string[] hasilSplit)
        {
            string dates = DateTime.Now.ToString("HH:mm:ss");
            string blisterID = hasilSplit[1];
            string hasil = GetStatusReceiveData(hasilSplit[0]); ;
            for (int i = 2; i < hasilSplit.Length; i++)
            {
                DataRow row = tableProductReceive.NewRow();
                row[0] = blisterID;
                row[1] = hasilSplit[i];
                row[2] = dates;
                row[3] = hasil;
                tableProductReceive.Rows.InsertAt(row, 0);

            }

            bl.dgvReceive.DataSource = tableProductReceive;
            quantityTotalReceive++;
            bl.lblQuantityGood.Text = "" + qtyGood;
            bl.lblQuantityReject.Text = "" + qtyFail;
            bl.lblQtyReceive.Text = "" + quantityTotalReceive;
            SetLogStatusReceiveData(hasilSplit, hasil);
            bl.dgvReceive.ClearSelection();

            if (bl.dgvReceive.Rows.Count > 100)
            {
                bl.dgvReceive.Rows.RemoveAt(100);
                bl.dgvReceive.Rows.RemoveAt(100);
                bl.dgvReceive.Rows.RemoveAt(100);
                bl.dgvReceive.Rows.RemoveAt(100);
                bl.dgvReceive.Rows.RemoveAt(100);
                //quantityReceive = bl.dgvReceive.Rows.Count;
            }
            bl.dgvReceive.FirstDisplayedScrollingRowIndex = 1;
        }

        // cek banyak yang belum dikirim
        public int GetCountData()
        {
            List<string> field = new List<string>();
            field.Add("count(*)");
            string where = "isused = '0'";
            List<string[]> ds = sqlite.select(field, NAMA_TABLE_TEMP, where);
            if (sqlite.num_rows > 0)
            {
                return int.Parse(ds[0][0]);
            }
            else
            {
                return 0;
            }
        }

        //Check Untuk generate ulang
        public void GenerateCode()
        {
            int setengahgenerate = qtyGenerateCode / 2;
            if (GetCountData() < setengahgenerate)
            {
                ReGenerateCode();
                tempTerima = 0;
                SetDataGridReady("0", "");
            }
        }

        // ambil data yang mau dikirim
        public List<string> getDataSendToPrinter(int count)
        {
            log("Send To Printer : " + count);
            List<string> field = new List<string>();
            field.Add(NAMA_FIELD);
            string where = "isused = '0' order by " + NAMA_FIELD + " asc limit " + count;
            List<string[]> ds = sqlite.select(field, NAMA_TABLE_TEMP, where);
            List<string> temp = new List<string>();
            // ambil id nya saja 
            int i = 0;
            foreach (string[] data in ds)
            {
                temp.Add(ds[i][0]);
                i++;
            }
            return temp;
        }

        public void SendToPrinterTemp(List<string> data)
        {
            Code.kirimPrinter(data);
            Dictionary<string, string> field = new Dictionary<string, string>();
            field.Add("isused", "1");
            field.Add("count", "" + countRepeatSend);
            foreach (string da in data)
            {
                log("Send To Printer : " + da);
                sqlite.update(field, NAMA_TABLE_TEMP, NAMA_FIELD + " = '" + da + "'");
            }
            db.eventname = "Send To Printer +" + data.Count();
            db.systemlog();
            templastcountkirim = templastcountkirim + data.Count;
            db.updatesequence(batch, "" + templastcountkirim);
            countRepeatSend++;
            GenerateCode();
        }

        public void SendToPrinter(List<string> data)
        {
            Code.kirimPrinter(data);
            sqlite.BeginDelete(data);
            log("Send " + data.Count + " Data");
            GenerateCode();
        }

        //untuk generate code
        private void SetParameterGenerateCode(int qty)
        {
            //cek lastcount
            db.eventname = "Generate : " + qty + " Blister ID";
            db.systemlog();
            Code.batch = batch;
            //buat code
            Code.generateProduct(qty);
        }

        //stop proses
        public void SetStopStation()
        {
            try
            {
                start = false;
                auditStarted= false;
                //bl.cmbBatch.Enabled = true;
                bl.txtBatchNumber.Enabled = true;
                enable();
                //SetComboBoxBatchNumber();
                //delayTimer.Stop();
                //delaydevice.Stop();
                prin2.stop();
                db.eventname = "Finish Print Blister Station";
                db.systemlog();
                sqlite.updatestatusconfig("");
                db.addVaccine(batch, linenumber, productmodelid, rs);
            }
            catch (NullReferenceException nf)
            {

            }
        }

        public void SendClearBufferPrinter()
        {
            print pr = new print();
            pr.cekKoneksi();
            pr.clearBuffer();
            pr.disconect();

        }

        // hapus data datagridview receive
        public void SetEmptyDataGridReceive()
        {
            tableProductReceive = new DataTable();
            tableProductReceive.Columns.Add("Blister ID");
            tableProductReceive.Columns.Add("Cap ID");
            tableProductReceive.Columns.Add("Timestamp");
            tableProductReceive.Columns.Add("Status");
            bl.dgvReceive.DataSource = tableProductReceive;
            //bl.lblQuantityReceive.Text = "0";
            //qtyFail = 0;
            //qtyGood = 0;
            //bl.lblQuantityReject.Text = "0";
            //bl.lblQuantityGood.Text = "0";
            bl.lblQtyNow.Text = "0";
            qtyNow = 0;
        }

        // hapus data datagridview Send
        public void SetEmptyDataGridReady()
        {
            tableProductSend = new DataTable();
            tableProductSend.Columns.Add("Blister ID");
            bl.dgvSend.DataSource = tableProductSend;
        }

        // mengecek batchnumber masih aktif atau tidak
        public bool GetStatusBatchnumber(string batch)
        {
            List<string> field = new List<string>();
            field.Add("batchnumber");
            string where = "batchnumber ='" + batch + "' and status ='1'";
            List<string[]> ds = db.selectList(field, "packaging_order", where);
            if (db.num_rows > 0)
                return true;
            else
                return false;
        }

        // memulai audit
        public void SetPreCheck()
        {
            audit = true;
            SetParameterPrecheck();
            db.eventname = "Start Pre-check " + batch + " Blister Station";
            db.systemlog();
        }
    
        public void SetParameterStartProduction()
        {
            //quantityReceive = 0;
            SetEmptyDataGridReceive();
            auditStarted = true;
            db.eventname = "Start printing " + batch + " Blister Station";
            db.systemlog();
            start = true;
            audit = false;
            tempTerima = 1;
            string[] datapack = db.dataPO(batch);
            productmodelid = datapack[4];
            exp = datapack[1];
            bl.lblBatch.Text = batch;
            bl.lblProductName.Text = datapack[3];
            sqlite.delete("", NAMA_TABLE_TEMP);
            Code.lastCount = db.getcountProduct(batch) + 1;
            templastcountkirim = Code.lastCount - 1;
            SetParameterGenerateCode(qtyGenerateCode);
           // SetStartPrinterThread();
            //clearDataSendFail();
            tempTerimaKirimUlang = 1;
            quantityTotalReceive = 0;
            bl.lblQuantityReceive.Text = "" + quantityReceive;
            SendToPrinter(getDataSendToPrinter(qtySendToPrinter));
            SendToPrinter(getDataSendToPrinter(qtySendToPrinter));
        }
       
        private void SetStartPrinterThread()
        {
            prin2 = new print();

            if (prin2.cekKoneksi())
            {
                prin2.start();
               // prin2.clearBuffer();
                prin2.startListener(this);
               // SetDataGridSend("0", "");
            }
            else
            {
                new Confirm("Printer not connected", "Information", MessageBoxButtons.OK);

            }
        }


        bool auditStarted;

        // untuk mem-generate data
        public void SetParameterPrecheck()
        {
            db.eventname = "Pre-check " + batch;
            db.systemlog();
            auditStarted = true;
            string[] datapack = db.dataPO(batch);
            productmodelid = datapack[4];
            exp = datapack[1];
            bl.lblBatch.Text = batch;
            bl.lblProductName.Text = datapack[3];
            sqlite.delete("", NAMA_TABLE_TEMP);
            Code.lastCount = db.getcountProduct(batch) + 1;
            templastcountkirim = Code.lastCount - 1;
            Code.audit = true;
            SetParameterGenerateCode(qtyGenerateCode);
            Code.audit = false;

            //string[] datapack = db.dataPO(batch);
            //bl.lblPo.Text = batch;
            //bl.lblProductName.Text = datapack[3];
            //sqlite.delete("", NAMA_TABLE_TEMP);
            //Code.lastCount = db.getcountProduct(batch) + 1;
            //templastcountkirim = Code.lastCount - 1;
            //generateCodeSend(qtyGenerateCode);
        }

        // setelah kirim printer apabila kurang maka generate ulang
        public void ReGenerateCode()
        {
            //Code.lastCount = db.getcountProduct(batch) + 1;
            log("Generate Code : " + qtyGenerateCode);
            SetParameterGenerateCode(qtyGenerateCode);
        }

        // untuk tampil data di grid send
        public void SetDataGridReady(string status, string banyak)
        {
            if (bl.InvokeRequired)
            {
                bl.Invoke(new Action<string, string>(SetDataGridReady), new object[] { status, banyak });
                return;
            }
            List<string> field = new List<string>();
            field.Add(NAMA_FIELD + " 'Blister ID'");
            string limit = "";
            if (banyak.Length > 0)
                limit = "limit " + banyak;
            List<string[]> ds = sqlite.select(field, NAMA_TABLE_TEMP, "isused = '" + status + "' " + limit);
            if (sqlite.num_rows > 0)
            {
                tableProductSend = sqlite.dataHeader;
                bl.dgvSend.DataSource = tableProductSend;
                foreach (string[] Row in ds)
                {
                    DataRow row = tableProductSend.NewRow();
                    row[0] = Row[0];
                    tableProductSend.Rows.Add(row);
                }

                bl.dgvSend.DataSource = tableProductSend;
            }
        }

//##ini Untuk mengirim dataper waktu
        //// untuk mengirim ke database server
        //public void delay()
        //{
        //    startDelay = true;
        //    delayTimer = new System.Timers.Timer();
        //    int delaySql = int.Parse(sqlite.config["delay"]);
        //    delaySql = delaySql * 60000;
        //    delayTimer.Interval = delaySql;
        //    delayTimer.Elapsed += new System.Timers.ElapsedEventHandler(delayTimer_Elapsed);
        //    delayTimer.Start();
        //}

        //// yang dilakukan pada saat delay
        //void delayTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        //{
        //    if (!db.delayStat)
        //    {
        //        //   log("Start insert to server");
        //        db.delayStat = true;
        //        db.addVaccine(batch, linenumber, productmodelid, rs);
        //        // log("Done insert to server");
        //    }
        //}

        //public void CekDevice()
        //{
        //    if (PingHost(sqlite.config["ipCamera"]))
        //    {
        //        bl.lblCamera.Text = "Online";
        //        bl.onCamera1.Visible = true;
        //        bl.offCamera1.Visible = false;
        //    }
        //    else
        //    {
        //        bl.lblCamera.Text = "Offline";
        //        bl.onCamera1.Visible = false;
        //        bl.offCamera1.Visible = true;
        //    }
        //    if (PingHost(sqlite.config["ipCamera2"]))
        //    {
        //        bl.lblCamera2.Text = "Online";
        //        bl.onCamera2.Visible = true;
        //        bl.offCamera2.Visible = false;
        //    }
        //    else
        //    {
        //        bl.lblCamera2.Text = "Offline";
        //        bl.onCamera2.Visible = false;
        //        bl.offCamera2.Visible = true;
        //    }

        //    if (PingHost(sqlite.config["ipCamera3"]))
        //    {
        //        bl.lblCamera3.Text = "Online";
        //        bl.onCamera3.Visible = true;
        //        bl.offCamera3.Visible = false;
        //    }
        //    else
        //    {
        //        bl.lblCamera3.Text = "Offline";
        //        bl.onCamera3.Visible = false;
        //        bl.offCamera3.Visible = true;
        //    }

        //    print pr = new print();
        //    if ((pr.cekKoneksi()))
        //    {
        //        bl.lblPrinter.Text = "Online";
        //        bl.onPrinter.Visible = true;
        //        bl.offPrinter.Visible = false;
        //        pr.disconect();
        //    }
        //    else
        //    {
        //        bl.lblPrinter.Text = "Offline";
        //        bl.onPrinter.Visible = false;
        //        bl.offPrinter.Visible = true;
        //    }
        //}


        public bool checkPLCConnected()
        {
            return sendData_plc.CheckConnected();
        }

        //untuk ngeping ke ip parameter
        public bool PingHost(string nameOrAddress)
        {
            if (nameOrAddress.Length == 0)
                return false;
            bool pingable = false;
            Ping pinger = new Ping();
            try
            {
                PingReply reply = pinger.Send(nameOrAddress);
                pingable = reply.Status == IPStatus.Success;
            }
            catch (PingException)
            {
                // Discard PingExceptions and return false;
            }
            return pingable;
        }

        //void setParameterBackgroudWorker()
        //{
        //    worker.DoWork += new DoWorkEventHandler(worker_DoWork);
        //    worker.WorkerReportsProgress = true;
        //    worker.RunWorkerAsync();
        //    resetEvent.WaitOne();
        //}

        //void worker_DoWork(object sender, DoWorkEventArgs e)
        //{
        //    CekDevice();
        //    resetEvent.Set();
        //}

        void SetLogStatusReceiveData(string[] hasilsplit, string hasil)
        {
            log("----------------------------------------");
            log("Blister ID : " + hasilsplit[1]);
            log("Cap ID     : " + hasilsplit[2]);
            log("Status     : " + hasil);
            log("----------------------------------------");
        }

        public void SetFormlog(string data)
        {
            if (bl.InvokeRequired)
            {
                bl.Invoke(new Action<string>(SetFormlog), new object[] { data });
                return;
            }

            log("Receive Data : " + data);
        }

        internal void refresh()
        {
            SetEmptyDataGridReceive();
        }

        internal string GetAdminName(string admin)
        {
            List<string> field = new List<string>();
            field.Add("first_name");
            string where = "id ='" + admin + "'";
            List<string[]> ds = db.selectList(field, "[USERS]", where);
            if (db.num_rows > 0)
                return ds[0][0];
            else
                return "";
        }

        //tambahan
        public string[] InsertBlisterFail(string[] hasilsplit)
        {
            for (int i = 2; i < hasilsplit.Length; i++)
            {
                InsertToDatabaseReject(hasilsplit[i], hasilsplit[1], hasilsplit[0]);
            }
            return hasilsplit;
        }

        private void InsertToDatabaseReject(string dataCapid, string datablisterpackId, string status)
        {
            if (batch != null && productmodelid != null)
            {
                if (batch.Length > 0 && productmodelid.Length > 0)
                {
                    List<string> fieldz = new List<string>();
                    fieldz.Add("[gsOneVialId]");
                    DataSet getGs1 = db.select(fieldz, "[Vaccine]", "capId = '" + dataCapid + "' and batchNumber = '" + batch + "'");
                    string gsone = null;
                    if (getGs1.Tables[0].Rows.Count != 0)
                    {
                        gsone = getGs1.Tables[0].Rows[0]["gsOneVialId"].ToString();
                    }
                    
                    Dictionary<string, string> field = new Dictionary<string, string>();
                    field.Add("capid", dataCapid);
                    field.Add("blisterpackid", datablisterpackId);
                    field.Add("gsonevialid", gsone);
                    if (status != "1")
                    {
                        field.Add("flag", "0");
                    }
                    else
                    {
                        field.Add("flag", "1");
                    }
                    field.Add("batchnumber", batch);
                    field.Add("productModelId", productmodelid);
                    field.Add("expdate", exp);
                    field.Add("createdtime", db.getUnixString());
                    field.Add("isreject", status);
                    field.Add("linenumber", linenumber);
                    if (!db.insert(field, "vaccine_reject"))
                    {
                        sendToPLC();
                    }
                }
                else
                {
                    status = "1";
                    log("Data reject not saved.");
                }
            }
            else
            {
                status = "1";
                log("Data reject not saved.");
            }
        }

        public int[] getCount(string btch)
        {
            int[] cnt = new int[3];
            qtyGood = db.selectCountPass(btch);
            qtyFail = db.selectCountReject(btch);
            quantityReceive = qtyGood + qtyFail;
            cnt[0] = qtyGood;
            cnt[1] = qtyFail;
            cnt[2] = quantityReceive;
            return cnt;
        }

        //end tambahan
    }
    
}
