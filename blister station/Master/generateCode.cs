﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Blister_Station
{
    
    public class generateCode
    {

        public const int QTY_KIRIM = 50;
        public const int QTY_MAKS_KIRIM_PRODUCT = 20;
        public const int QTY_GENERATE = 200;

        public int pembuatan;
        public int PackagingOrder;
        public int postCodeProduct;
        public int tempBanyak;
        public string batch;
        sqlitecs sql;
        public int tambahProduct = 1;
        public List<string> dataProduct;
        public bool masih;
        public int penomoran;
        dbaccess db = new dbaccess();

        public bool audit;

        public int lastCount;

        public generateCode()
        {
            sql = new sqlitecs();
            dataProduct = new List<string>();
        }

        public string[] generateProduct()
        {
            cekTemp();
            string[] temp = new string[tempBanyak];
            for (int i = 0; i < tempBanyak; i++)
            {
                temp[i] = penomoran.ToString().PadLeft(5, '0');
                //temp[i] = batch + Control.NUMBER_CODE + penomoran.ToString().PadLeft(5, '0');
                Dictionary<string, string> field = new Dictionary<string, string>();
                field.Add(Control.NAMA_FIELD, temp[i]);
                field.Add("isused", "0");
                sql.insertData(field, Control.NAMA_TABLE_TEMP);
                dataProduct.Add(temp[i]);
                tambahProduct++;
                penomoran++;
            }
            sql.Begin();
            return temp;
        }

        public void kirimPrinter(string[] data)
        {
            print pr = new print();
            pr.cekKoneksi();
            pr.kirimData(data);
        }

        public void kirimPrinter(List<string> data)
        {
            print pr = new print();
            pr.cekKoneksi();
            pr.kirimData(data);
        }

        public void cekTemp()
        {
            int qtygenerate = 0;
            if (tambahProduct == 0)
                qtygenerate = QTY_KIRIM * 2;
            else
            {
                qtygenerate = QTY_KIRIM;
            }
            int temp = PackagingOrder - tambahProduct;
            if (temp > qtygenerate)
            {
                tempBanyak = qtygenerate;
                postCodeProduct = postCodeProduct + tempBanyak;
            }
            else
            {
                if (temp <= qtygenerate)
                {
                    tempBanyak = temp;
                    masih = true;
                }
                else
                {
                    tempBanyak = qtygenerate;
                    postCodeProduct = postCodeProduct + tempBanyak;
                }
            }
        }

        public string[] genarateProductAudit(int banyak, string batch)
        {

            string[] temp = new string[banyak];
            for (int i = 0; i < banyak; i++)
            {
                int j = i + 1;
                string product = batch + Control.NUMBER_CODE + j.ToString().PadLeft(5, '0');
                Dictionary<string, string> field = new Dictionary<string, string>();
                field.Add(Control.NAMA_FIELD, product);
                field.Add("isused", "0");
                sql.insertData(field, Control.NAMA_TABLE_TEMP);
                temp[i] = product;
                tambahProduct++;
            }
            sql.Begin();
            return temp;
        }

        public void genarateProductAudit(int banyak)
        {
            //getdata
            if (tambahProduct == 1)
            {
                for (int i = 0; i < banyak; i++)
                {
                    int j = i + 1;
                    string capid = batch + Control.NUMBER_CODE + j.ToString().PadLeft(5, '0');
                    Dictionary<string, string> field = new Dictionary<string, string>();
                    field.Add(Control.NAMA_FIELD, capid);
                    field.Add("isused", "0");
                    sql.insertData(field, Control.NAMA_TABLE_TEMP);
                    tambahProduct++;
                }
                sql.Begin();
            }
        }

        public void genarateProductAuditContinue(int banyak)
        {
            //getdata
            for (int i = 0; i < banyak; i++)
            {
                int j = i + 1;
                string capid = batch + Control.NUMBER_CODE + tambahProduct.ToString().PadLeft(5, '0');
                Dictionary<string, string> field = new Dictionary<string, string>();
                field.Add(Control.NAMA_FIELD, capid);
                field.Add("isused", "0");
                sql.insertData(field, Control.NAMA_TABLE_TEMP);
                tambahProduct++;
            }
            sql.Begin();
        }

        public void updatelastCountConfig()
        {
            Dictionary<string, string> field = new Dictionary<string, string>();
            int templast = lastCount - 1;
            field.Add("lastpost", "" + templast);
            sql.update(field, "config", "");
        }

        public string RandomString()
        {
            RandomStringGenerator RSG = new RandomStringGenerator(false,true,true,false);
            //  RSG.UniqueStrings = true;
            // Or we can use the constructor
             RSG.LowerCaseCharacters = "abcdefghijklmn".ToCharArray();
            return RSG.Generate(1);

        }

        public void generateProducttemp(int banyak)
        {
            //getdata
            for (int i = 0; i < banyak; i++)
            {
                int j = i + 1;
                string guid = Guid.NewGuid().ToString().Substring(0, 5);
                string blisterId = lastCount.ToString().PadLeft(6, '0');
                //string blisterId = batch + Control.NUMBER_CODE + lastCount.ToString().PadLeft(6, '0');
                //string blisterId = batch + Control.NUMBER_CODE + guid;
                Dictionary<string, string> field = new Dictionary<string, string>();
                string rand = RandomString();
                field.Add(Control.NAMA_FIELD, rand + guid);
                field.Add("flag", "0");
                field.Add("isused", "0");
                if (audit)
                {
                    field.Add("flag", "1");
                }
                else
                {
                    field.Add("flag", "0");
                }

                sql.insertData(field, Control.NAMA_TABLE_TEMP);
                lastCount++;
            }
            sql.Begin();
            updatelastCountConfig();
        }

        public void generateProduct(int banyak)
        {
            //getdata
            banyak = banyak * 3;
            List<string> data = create_Guid(banyak);
            data = checkNotExistDb(data);
            saveSqlLite(data);
        }

        private void saveSqlLite(List<string> data)
        {
            for (int i = 0; i < data.Count; i++)
            {
                Dictionary<string, string> field = new Dictionary<string, string>();
                field.Add(Control.NAMA_FIELD, data[i]);
                field.Add("isused", "0");
                if (audit)
                    field.Add("flag", "1");
                else
                    field.Add("flag", "0");
                sql.insertData(field, Control.NAMA_TABLE_TEMP);
            } sql.Begin();
        }

        private List<string> checkNotExistDb(List<string> data)
        {
            List<string> field = new List<string>();
            field.Add("blisterpackId");
            string where = "blisterpackId in( ";
            for (int i = 0; i < data.Count; i++)
            {
                where += "'" + data[i] + "'";
                if (i + 1 != data.Count)
                    where += ",";
            }
            where += ") and batchnumber ='" + batch + "'";
            List<string[]> res = db.selectList(field, "blisterPack", where);
            foreach (string[] dataTemp in res)
            {
                string dataRemove = data[0];
                data.Remove(dataRemove);
            }
            return data;
        }

        private List<string> create_Guid(int banyak)
        {
            List<string> datatemp = new List<string>();
            for (int i = 0; i < banyak; i++)
            {
                string guid = Guid.NewGuid().ToString().Substring(0, 5);
                string randdat = RandomString();
                datatemp.Add(randdat + guid);
            }
            return datatemp;
        }

        public int getLastcount()
        {
            List<string> field = new List<string>();
            field.Add("lastPost");
            List<string[]> ds = sql.select(field, "config", "");
            int dapat = int.Parse(ds[0][0]);
            lastCount = dapat;
            return dapat;
        }


    }
}
