﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Net;
using System.Threading;
using System.Data;
using System.Windows.Forms;

namespace Blister_Station
{
    public class server
    {
        blister ad;
        int port;
        Control ms;
        int fun;
        bool alredyOpened;
        public server(blister bl, string ports, Control mst, int func)
        {
            ms = mst;
            port = int.Parse(ports);
            ad = bl;
            fun = func;
            isRunning = true; 
            mThread t;
            if (!cekPort(ports))
            {
                alredyOpened = false;
                t = new mThread(StartListening);
                Thread masterThread = new Thread(() => t(ref isRunning));
                masterThread.IsBackground = true; //better to run it as a background thread
                masterThread.Start();
            }
            else
            {
                alredyOpened = true;
                t = new mThread(StartListening);
                Thread masterThread = new Thread(() => t(ref isRunning));
                masterThread.IsBackground = true; //better to run it as a background thread
                masterThread.Start();
             //   new Confirm("Port already opened", "Information", MessageBoxButtons.OK);
                mst.db.simpanLog("Port :" + ports + " Used");
            }
        }
        public static bool isRunning;

        delegate void mThread(ref bool isRunning);
        delegate void AccptTcpClnt(ref TcpClient client, TcpListener listener);


        public static void AccptClnt(ref TcpClient client, TcpListener listener)
        {
            try
            {
                if (client == null)
                    client = listener.AcceptTcpClient();
            }
            catch (Exception ex)
            {

            }
        }

        public void StreamClose()
        {
            try
            {
                stream.Close();
                listener.Stop();
                tt.Abort();
                handler.Close();
                handler = null;
            }
            catch (Exception ex)
            {
                //ms.db.simpanLog(ex.Message);
            }
        }

        TcpClient handler = null;

        TcpListener listener;
        Thread tt;
        NetworkStream stream;
        public void StartListening(ref bool isRunning)
        {
            listener = new TcpListener(port);
            Byte[] bytes = new Byte[256];
            //handler = null;
            if (!alredyOpened)
                listener.Start();
            while (isRunning)
            {

                AccptTcpClnt t = new AccptTcpClnt(AccptClnt);
                tt = new Thread(() => t(ref handler, listener));
                tt.IsBackground = true;
                tt.Start();

                while (isRunning && tt.IsAlive && handler == null)
                    Thread.Sleep(1); //change the time as you prefer

                if (handler != null)
                {
                    if (fun == 0)
                    {
                        stream = handler.GetStream();
                        ms.log("Connected " + handler.Client.RemoteEndPoint);
                        String Message = "";
                        bool konek = true;
                        while (konek)
                        {
                            try
                            {
                                string data = null;
                                int i;
                                ms.log("Waiting Data");
                                Int32 da = stream.Read(bytes, 0, bytes.Length);

                                Message = "";
                                while (true)
                                {
                                    if (da == 0)
                                        break;
                                    Message = Message + System.Text.Encoding.ASCII.GetString(bytes, 0, da);
                                    if (da < 256)
                                        break;
                                    da = stream.Read(bytes, 0, bytes.Length);
                                }
                                if (Message.Length > 0)
                                {
                                    ms.ReceiveData(Message);
                                }
                                else
                                {
                                    konek = false;
                                    stream.Close();
                                    ms.log("Close Connection");
                                }
                            }
                            catch (Exception ex)
                            {
                                konek = false;
                                stream.Close();
                                ms.db.simpanLog(ex.Message);
                                ms.log("Close Connection");
                            }
                        }
                    }
                    else if(fun ==1)
                    {
                        string data = null;
                        stream = handler.GetStream();
                        int i;
                        Int32 da = stream.Read(bytes, 0, bytes.Length);
                        String Message = System.Text.Encoding.ASCII.GetString(bytes, 0, da);
                       // string databalik = "" + ms.cekKamera(Message) + "\n"; ;
                       // byte[] msg = System.Text.Encoding.ASCII.GetBytes(databalik);
                      //  stream.Write(msg, 0, msg.Length);
                        stream.Flush();
                        stream.Close();
                    }
                    else if (fun == 2)
                    {
                        //terima data camera 1

                        bool konek = true;
                        try
                        {
                            stream = handler.GetStream();
                            string data = null;
                            int i;
                            ms.log("Waiting Data");
                            Int32 da = stream.Read(bytes, 0, bytes.Length);
                            String Message = "";
                            while (true)
                            {
                                if (da == 0)
                                    break;
                                Message = Message + System.Text.Encoding.ASCII.GetString(bytes, 0, da);
                                if (da < 256)
                                    break;
                            }

                         //   ms.terimaData(Message, 0);
                            stream.Close();
                        }
                        catch (Exception ex)
                        {
                            stream.Close();
                        }
                    }
                    else if (fun == 3)
                    {
                        //terima data camera 2
                        bool konek = true;
                        try
                        {
                            stream = handler.GetStream();
                            string data = null;
                            int i;
                            ms.log("Waiting Data");
                            Int32 da = stream.Read(bytes, 0, bytes.Length);
                            String Message = "";
                            while (true)
                            {
                                if (da == 0)
                                    break;
                                Message = Message + System.Text.Encoding.ASCII.GetString(bytes, 0, da);
                                if (da < 256)
                                    break;
                            }

                           // ms.terimaData(Message, 1);
                        }
                        catch (Exception ex)
                        {
                            stream.Close();
                        }
                    }
                    else if (fun == 4)
                    {
                        //terima data camera 3
                        try
                        {
                            stream = handler.GetStream();
                            string data = null;
                            int i;
                            ms.log("Waiting Data");
                            Int32 da = stream.Read(bytes, 0, bytes.Length);
                            String Message = "";
                            while (true)
                            {
                                if (da == 0)
                                    break;
                                Message = Message + System.Text.Encoding.ASCII.GetString(bytes, 0, da);
                                if (da < 256)
                                    break;
                            }

                            //ms.terimaData(Message, 2);
                            stream.Close();
                        }
                        catch (Exception ex)
                        {
                            stream.Close();
                        }
                    }
                }
                else if (!isRunning && tt.IsAlive)
                {
                    tt.Abort();
                }
                handler = null;
            }
            listener.Stop();
        }

        public bool cekPort(string port)
        {
            using (TcpClient tcpClient = new TcpClient())
            {
                try
                {
                    tcpClient.Connect("127.0.0.1", int.Parse(port));
                    return true;
                }
                catch (Exception)
                {
                    Console.WriteLine("Port closed");
                    return false;
                }
            }
            return false;
        }
    }
}
    
