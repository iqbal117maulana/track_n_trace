﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Data;
using System.Net.NetworkInformation;
using System.ComponentModel;

namespace Blister_Station
{
    class Control
    {
        public const string NUMBER_CODE = "02";
        public const string NAMA_FIELD = "blisterPackId";
        public const string NAMA_TABLE_TEMP = "tmp_blisterPack_id";
        public const string NAMA_TABLE_UTAMA = "blisterpack";
        public const string NAMA_STATION = "BLISTER";
        public int qtySendToPrinter = 5;
        public int qtyGenerateCode = 20;
        public const string NAMA_SEQ = "BLISTERSEQ";
        public char delimit = ',';

        public result rs;
        public sqlitecs sqlite;
        public dbaccess db;
        public blister bl;
        public generateCode Code;
        private Thread serverThread;
        private Thread serverThread2;
        private Thread serverThread3;
        private Thread serverThreadCamera;
        public string batch;
        public string adminid;
        public System.Timers.Timer delayTimer;
        public System.Timers.Timer delaydevice;
        public string linenumber;
        public string linename;
        public List<string[]> datacombo;
        public bool start;
        public bool startDelay;
        public int templastcountkirim;
        public List<string> dataSendToPrinter;
        public string productmodelid;
        public string[] dataPackagingOrder;
        public int countVaccine = 0;
        bool audit = false;

        DataTable tableProductReceive = new DataTable();
        DataTable tableProductSend = new DataTable();
        List<string> dataVaccine;
        int tempTerima;
        int tempTerimaKirimUlang = 1;
        int qtySend = 0;
        int qtyReceive = 0;

        private BackgroundWorker worker = new BackgroundWorker();
        private AutoResetEvent resetEvent = new AutoResetEvent(false);

        public server srv;
        public server srv2;
        public server srv3;

        public void MulaiServer()
        {
            srv = new server(bl, sqlite.config["portserver"], this, 0);
        }

        public void MulaiServer2()
        {
            server srv2 = new server(bl, sqlite.config["portCamera"], this, 1);
        }

        public void MulaiServer3()
        {
            srv2 = new server(bl, sqlite.config["portserver2"], this, 3);
        }

        public void MulaiServer4()
        {
            srv3 = new server(bl, sqlite.config["portserver3"], this, 4);
        }

        public void disable()
        {
            bl.btnStart.Text = "Stop";
            bl.btnAudit.Enabled = false;
            bl.btnDeo.Enabled = false;
            bl.btnLogout.Enabled = false;
            bl.btnConfig.Enabled = false;
            bl.btnSample.Enabled = false;
            bl.btnCommision.Enabled = false;
        }

        public void enable()
        {
            bl.btnStart.Text = "Start";
            bl.btnAudit.Enabled = true;
            bl.btnDeo.Enabled = true;
            bl.btnLogout.Enabled = true;
            bl.btnConfig.Enabled = true;
           // bl.btnStart.Enabled = false;
            bl.btnSample.Enabled = true;
            bl.btnCommision.Enabled = true;
        }

        public void cmb()
        {
            List<string> field = new List<string>();
            field.Add("batchNumber,productModelId");
            string where = "status = '1' and linepackagingid = '" + linenumber + "'";
            datacombo = db.selectList(field, "[packaging_order]", where);
            List<string> da = new List<string>();
            for (int i = 0; i < datacombo.Count; i++)
            {
                da.Add(datacombo[i][0]);
            }
            bl.cmbBatch.DataSource = da;
        }

        public void log(string data)
        {
            try
            {
                if (bl.InvokeRequired)
                {
                    bl.Invoke(new Action<string>(log), new object[] { data });
                    return;
                }
                string dates = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                string simpan = dates + "\t" + data + "\n";
                bl.txtLog1.AppendText(simpan);
            }
            catch (Exception ob)
            {

            }
        }



        public bool cekPrinterready()
        {
            print pr = new print();
            if (pr.cekKoneksi())
            {
                if (pr.cekStatusPrinter())
                {
                    bl.lblPrinter.Text = pr.statusLeibinger;
                    return true;
                }
            }
            Confirm cf = new Confirm("Printer Not Connected" + pr.statusLeibinger, "Information", MessageBoxButtons.OK);
            bl.lblPrinter.Text = pr.statusLeibinger;
            return false;
        }
        DataTable tableTerimaCamera = new DataTable();
        public void startStation()
        {

            db.adminid = adminid;
            bl.lblAdmin.Text = adminid;
            db.from = "Blister Station";
            db.eventname = "Start Station Blister Station " + adminid;
            db.systemlog();
            CekDevice();
            log("Login successfully admin ID = " + adminid);
            log("Server Port : " + sqlite.config["portserver"]);
            rs = new result();
            cmb();
            setTableReceive();
            setQty();
            delay();
            tableTerimaCamera.Columns.Add("Camera1");
            tableTerimaCamera.Columns.Add("Camera2");
            tableTerimaCamera.Columns.Add("Camera3");
            tableTerimaCamera.Columns.Add("Status");

            serverThread = new Thread(new ThreadStart(MulaiServer));
            serverThread.Start();
            serverThreadCamera = new Thread(new ThreadStart(MulaiServer2));
            serverThreadCamera.Start();
            backgroundwo();
        }

        public void setQty()
        {
            qtyGenerateCode = int.Parse(sqlite.config["qtygenerate"]);
            qtySend = int.Parse(sqlite.config["qtysend"]);
            CekDevice();
        }

        public void setTableReceive()
        {
            tableProductReceive.Columns.Add("Blister ID");
            tableProductReceive.Columns.Add("Cap ID");
            tableProductReceive.Columns.Add("Timestamp");
            tableProductReceive.Columns.Add("Status");
            bl.dgvReceive.DataSource = tableProductReceive;
        }

        private bool cekDataSample(string blisterid)
        {
            List<string> field = new List<string>();
            field.Add("blisterpackid");
            string where = "flag = 1 And blisterPackId ='" + blisterid + "'";
            List<string[]> ds = sqlite.select(field, "tmp_blisterPack_id", where);
            if (sqlite.num_rows > 0)
            {
                return true;
            }
            return false;
        }

        public void AddNewVaccine(string[] hasilsplit)
        {
            //tambah blister dulu 
            Dictionary<string, string> fieldBlister = new Dictionary<string,string>();
            fieldBlister.Add(NAMA_FIELD, hasilsplit[1]);
            fieldBlister.Add("isreject", hasilsplit[0]);
            fieldBlister.Add("createdtime", db.getUnixString());
            fieldBlister.Add("batchnumber", batch);
            if (hasilsplit[0].Equals("1"))
            {
                fieldBlister.Add("rejecttime", db.getUnixString());
            }

            if (cekDataSample(hasilsplit[1]))
            {
                fieldBlister.Add("flag", "1");
            }
            else
            {
                fieldBlister.Add("flag", "0");
            }
            db.insert(fieldBlister, "blisterPack");
            
            // update table Vaccine
            Dictionary<string, string> field;
            List<string> fieldList = new List<string>();
            fieldList.Add("capid");
            for (int i = 2; i < hasilsplit.Length; i++)
            {
                // cek data yang ada di database
                string where1 = "capid ='" + hasilsplit[i] + "'";
                List<string[]> dataCapid = db.selectList(fieldList, "Vaccine", where1);
                if (db.num_rows > 0)
                {
                    //masukan datanya
                    field = new Dictionary<string, string>();
                    field.Add(NAMA_FIELD, hasilsplit[1]);
                    field.Add("isreject", "0");
                    db.update(field, "vaccine", "capid ='" + hasilsplit[i] + "'");
                }
                else
                {
                    log("data capid: " + hasilsplit[i] + " Not Found");
                }
            }
            //hapus yang ada di tmp
            string where = NAMA_FIELD + " ='" + hasilsplit[1] + "'";
            sqlite.delete(where, NAMA_TABLE_TEMP);
        }

        public void sudahditerimaProduct(string[] hasilsplit)
        {
            // tambah di table result
            Dictionary<string, string> field;
            List<string> fieldList = new List<string>();
            fieldList.Add("capid");
            for (int i = 2; i < hasilsplit.Length; i++)
            {
                // cek data yang ada di database
                string where1 = "capid ='" + hasilsplit[i] + "'";
                List<string[]> dataCapid = db.selectList(fieldList, "Vaccine", where1);
                if (db.num_rows > 0)
                {
                    //masukan datanya
                    field = new Dictionary<string, string>();
                    field.Add(NAMA_FIELD, hasilsplit[1]);
                    field.Add("flag", hasilsplit[0]);
                    field.Add("createdtime", db.getUnixString());
                    field.Add("capid", hasilsplit[i]);
                    rs.insert(field, "result");
                }
                else
                {
                    log("data capid: " + hasilsplit[i] + " Not Found");
                }
            }
            //hapus yang ada di tmp
            string where = NAMA_FIELD + " ='" + hasilsplit[1] + "'";
            sqlite.delete(where, NAMA_TABLE_TEMP);
        }

        //cek yang sudah dikirim ke printer
        public bool CekData(string blister)
        {
            List<string> field = new List<string>();
            field.Add(NAMA_FIELD);
            string where = NAMA_FIELD + " = '" + blister + "' and isused ='1'";
            sqlite.select(field, NAMA_TABLE_TEMP, where);
            if (sqlite.num_rows > 0)
                return true;
            else
                return false;
        }

        //cek status
        public string cek(string data)
        {
            if (data.Equals("0"))
                return "Pass";
            else
                return "Fail";
        }

        // Terima data dari PLC
        public void tambahData(string data)
        {
            if (bl.InvokeRequired)
            {
                bl.Invoke(new Action<string>(tambahData), new object[] { data });
                return;
            }

            if (data.Length > 0)
                log("Receive Data:" + data);
            data = data.Replace("\r", "");
            data = data.Replace("\n", "");
            data = data.Replace("]d2", "");
            List<string> dataMas = dataMassBlis(data);
            for (int i = 0; i < dataMas.Count; i++)
            {
                log("Proses Data:" + dataMas[i]);
                if (data.Length > 0)
                {
                    string[] hasilSplit = data.Split(delimit);
                    string status = hasilSplit[0];
                    string product = hasilSplit[1];
                    AddNewVaccine(hasilSplit);
                    updatetableReveive(hasilSplit);
                    bl.dgvReceive.DataSource = tableProductReceive;
                    cekKirimUlang();

                }
                else
                {
                    if (data.Length > 0)
                        log("Data " + data + " invalid");
                }
            }
        }

        private List<string> dataMassBlis(string data)
        {
            char[] delimiters = new char[] { '|' };
            List<string> hasilSplit = new List<string>(data.Split(delimiters, StringSplitOptions.RemoveEmptyEntries));
            return hasilSplit;
        }
        DataTable tableProductFail = new DataTable();

        private void tampilDataFail()
        {
            try
            {
                //get data capid
                List<string> field = new List<string>();
                field.Add("blisterPackId");
                string where = "isused ='1'";
                List<string[]> ds = sqlite.select(field, "tmp_blisterPack_id", where);

                //tampil ke dgvFail
                tableProductFail = new DataTable();
                tableProductFail.Columns.Add("Blister Pack ID");
                for (int i = 0; i < ds.Count; i++)
                {
                    DataRow row = tableProductFail.NewRow();
                    row[0] = ds[i][0];
                    tableProductFail.Rows.Add(row);

                    Dictionary<string, string> field1;
                    field1 = new Dictionary<string, string>();
                    field1.Add("blisterpackid", ds[i][0]);
                    field1.Add("flag", "1");
                    field1.Add("createdtime", db.getUnixString());
                    rs.insert(field1, "result");
                }
                bl.lblQtyFail.Text = ds.Count.ToString();
                bl.dgvFail.DataSource = tableProductFail;
            }
            catch (Exception ex)
            {
                db.simpanLog(ex.Message);
            }
        }

        // masukan ke tabel recive()
        public void updatetableReveive(string[] hasilSplit)
        {
            string dates = DateTime.Now.ToString("HH:mm:ss");
            string blisterID = hasilSplit[1];
            string hasil = cek(hasilSplit[0]); ;
            for (int i = 2; i < hasilSplit.Length; i++)
            {
                DataRow row = tableProductReceive.NewRow();
                row[0] = blisterID;
                row[1] = hasilSplit[i];
                row[2] = dates;
                row[3] = hasil;
                tableProductReceive.Rows.InsertAt(row, 0);
            }
            setlog(hasilSplit);
        }

        // cek banyak yang belum dikirim
        public int cekDataNotsend()
        {
            List<string> field = new List<string>();
            field.Add("count(*)");
            string where = "isused = '0'";
            List<string[]> ds = sqlite.select(field, NAMA_TABLE_TEMP, where);
            if (sqlite.num_rows > 0)
            {
                return int.Parse(ds[0][0]);
            }
            else
            {
                return 0;
            }
        }

        //Check Untuk generate ulang
        public void cekgenerateUlang()
        {
            int setengahgenerate = qtyGenerateCode / 2;
            if (cekDataNotsend() < setengahgenerate)
            {
                generateAgain();
                tempTerima = 0;
                SetDataGridSend("0", "");
            }
        }

        public void cekKirimUlang()
        {
            if (!audit)
            {
                if (tempTerimaKirimUlang >= qtySendToPrinter)
                {
                    tampilDataFail();
                    SendToPrinter(getDataSendToPrinter(qtySendToPrinter));
                    tempTerimaKirimUlang = 1;
                    SetDataGridSend("0", "");
                }
                else
                {
                    tempTerimaKirimUlang++;
                }
            }
        }

        // ambil data yang mau dikirim
        public List<string> getDataSendToPrinter(int count)
        {
            log("Send To Printer : " + count);
            List<string> field = new List<string>();
            field.Add(NAMA_FIELD);
            string where = "isused = '0' order by " + NAMA_FIELD + " asc limit " + count;
            List<string[]> ds = sqlite.select(field, NAMA_TABLE_TEMP, where);
            List<string> temp = new List<string>();
            // ambil id nya saja 
            int i = 0;
            foreach (string[] data in ds)
            {
                temp.Add(ds[i][0]);
                i++;
            }
            return temp;
        }

        // untuk mengirim ke printer dengan parameter
        public void SendToPrinter(List<string> data)
        {
            Dictionary<string, string> field = new Dictionary<string, string>();
            field.Add("isused", "1");
            foreach (string da in data)
            {
                log("Send To Printer : " + da);
                sqlite.update(field, NAMA_TABLE_TEMP, NAMA_FIELD + " = '" + da + "'");
            }
            db.eventname = "Send To Printer +" + data.Count();
            db.systemlog();

            getdatasend();
            Code.kirimPrinter(data);
            templastcountkirim = templastcountkirim + data.Count;
            db.updatesequence(batch, "" + templastcountkirim);
            cekgenerateUlang();
        }

        // ambil data yang sudah di kirim 
        private void getdatasend()
        {
            dataSendToPrinter = new List<string>();
            List<string> field = new List<string>();
            field.Add(NAMA_FIELD);
            string where = "isused = 1";
            List<string[]> ds = sqlite.select(field, NAMA_TABLE_TEMP, where);

            foreach (string[] da in ds)
            {
                dataSendToPrinter.Add(da[0]);
            }
        }

        //untuk generate code
        private void generateCodeSend(int qty)
        {
            //cek lastcount
            db.eventname = "Generate : " + qty + " Blister ID";
            db.uom = "Blister";
            db.systemlog();
            db.uom = "";
            Code.batch = batch;
            //buat code
            Code.generateProduct(qty);
        }

        //stop proses
        public void Stop()
        {
            try
            {
                start = false;
                bl.cmbBatch.Enabled = true;
                enable();
                cmb();
                //sqlite.delete("", NAMA_TABLE_TEMP);
               // sqlite.delete("", "result");
                delayTimer.Stop();
                delaydevice.Stop();
                //move("infeed");
                db.eventname = "Finish Print Blister Station";
                db.systemlog();
                sqlite.updatestatusconfig("");
                db.addVaccine(batch, linenumber, productmodelid, rs);
            }
            catch (NullReferenceException nf)
            {

            }
        }

        public void clearBufffer()
        {
            print pr = new print();
            pr.cekKoneksi();
            pr.clearBuffer();

        }

        // hapus data datagridview receive
        public void clearDataReceive()
        {
            tableProductReceive = new DataTable();
            tableProductReceive.Columns.Add("Blister ID");
            tableProductReceive.Columns.Add("Cap ID");
            tableProductReceive.Columns.Add("Timestamp");
            tableProductReceive.Columns.Add("Status");
            bl.dgvReceive.DataSource = tableProductReceive;
            bl.lblQuantityReceive.Text = "0";
        }

        // hapus data datagridview Send
        public void clearDataSend()
        {
            tableProductSend = new DataTable();
            tableProductSend.Columns.Add("Blister ID");
            bl.dgvSend.DataSource = tableProductSend;
        }

        // mengecek batchnumber masih aktif atau tidak
        public bool cekBatchNumber(string batch)
        {
            List<string> field = new List<string>();
            field.Add("batchnumber");
            string where = "batchnumber ='" + batch + "' and status ='1'";
            List<string[]> ds = db.selectList(field, "packaging_order", where);
            if (db.num_rows > 0)
                return true;
            else
                return false;
        }

        // memulai audit
        public void startAudit()
        {
            audit = true;
            setIdAudit();
            db.eventname = "Start Pre-check " + batch + " Blister Station";
            db.systemlog();
        }

        //memulai printing dengan sebelumnya memulai audit
        public void startPrinting()
        {
            db.eventname = "Start Printing Blister Station";
            db.systemlog();
            dataPackagingOrder = db.dataPO(batch);
            //getVaccine();
            generateCodeSend(qtyGenerateCode);
            SetDataGridSend("0", "" + qtySendToPrinter);
        }

        public void sendStartPrinter()
        {
            db.eventname = "Start printing " + batch + " Blister Station";
            db.systemlog();

            audit = false;
            tempTerima = 1;
            string[] datapack = db.dataPO(batch);
            bl.lblPo.Text = batch;
            bl.lblProductName.Text = datapack[3];
            //sqlite.delete("", NAMA_TABLE_TEMP);
            Code.lastCount = db.getcountProduct(batch) + 1;
            templastcountkirim = Code.lastCount - 1;
            generateCodeSend(qtyGenerateCode);

            SendToPrinter(getDataSendToPrinter(qtySendToPrinter * 2));
        }
        bool auditStarted;
        // untuk mem-generate data
        public void setIdAudit()
        {
            db.eventname = "Pre-check " + batch;
            db.systemlog();
            auditStarted = true;
            string[] datapack = db.dataPO(batch);
            bl.lblPo.Text = batch;
            bl.lblProductName.Text = datapack[3];
            sqlite.delete("", NAMA_TABLE_TEMP);
            Code.lastCount = db.getcountProduct(batch) + 1;
            templastcountkirim = Code.lastCount - 1;
            Code.audit = true;
            generateCodeSend(qtyGenerateCode);
            Code.audit = false;

            //string[] datapack = db.dataPO(batch);
            //bl.lblPo.Text = batch;
            //bl.lblProductName.Text = datapack[3];
            //sqlite.delete("", NAMA_TABLE_TEMP);
            //Code.lastCount = db.getcountProduct(batch) + 1;
            //templastcountkirim = Code.lastCount - 1;
            //generateCodeSend(qtyGenerateCode);
        }

        // setelah kirim printer apabila kurang maka generate ulang
        public void generateAgain()
        {
            //Code.lastCount = db.getcountProduct(batch) + 1;
            log("Generate Code : " + qtyGenerateCode);
            generateCodeSend(qtyGenerateCode);
        }

        // unused
        public void getVaccine()
        {
            List<string> field = new List<string>();
            field.Add("capid");
            string where = "batchnumber ='" + batch + "'";
            List<string[]> ds = db.selectList(field, "vaccine", where);
            dataVaccine = new List<string>();
            if (db.num_rows > 0)
            {
                foreach (string[] da in ds)
                {
                    dataVaccine.Add(da[0]);
                }
            }
            else
            {

            }

        }

        //unused
        public void setIdstartprint()
        {

        }

        // untuk tampil data di grid send
        public void SetDataGridSend(string status, string banyak)
        {
            List<string> field = new List<string>();
            field.Add(NAMA_FIELD + " 'Blister ID'");
            string limit = "";
            if (banyak.Length > 0)
                limit = "limit " + banyak;
            List<string[]> ds = sqlite.select(field, NAMA_TABLE_TEMP, "isused = '" + status + "' " + limit);
            if (sqlite.num_rows > 0)
            {
                tableProductSend = sqlite.dataHeader;
                bl.dgvSend.DataSource = tableProductSend;
                foreach (string[] Row in ds)
                {
                    DataRow row = tableProductSend.NewRow();
                    row[0] = Row[0];
                    tableProductSend.Rows.Add(row);
                }

                bl.dgvSend.DataSource = tableProductSend;
            }
        }

        //update data sample
        public void sampleData(string blisterid)
        {
            Dictionary<string, string> field = new Dictionary<string, string>();
            //status sample
            field.Add("isused", "4");
            string where = NAMA_FIELD + "='" + blisterid + "'";
            sqlite.update(field, NAMA_TABLE_TEMP, where);
        }

        // untuk mengirim ke database server
        public void delay()
        {
            startDelay = true;
            delayTimer = new System.Timers.Timer();
            int delaySql = int.Parse(sqlite.config["delay"]);
            delaySql = delaySql * 60000;
            delayTimer.Interval = delaySql;
            delayTimer.Elapsed += new System.Timers.ElapsedEventHandler(delayTimer_Elapsed);
            delayTimer.Start();
        }

        // yang dilakukan pada saat delay
        void delayTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            if (!db.delayStat)
            {
                //   log("Start insert to server");
                db.delayStat = true;
                db.addVaccine(batch, linenumber, productmodelid, rs);
                // log("Done insert to server");
            }
        }

        public void CekDeviceDelay()
        {
            delaydevice = new System.Timers.Timer();
            delaydevice.Interval = 10000;
            delaydevice.Elapsed += new System.Timers.ElapsedEventHandler(delaydevice_Elapsed);
            delaydevice.Start();
        }

        void delaydevice_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            CekDevice();
        }

        public void CekDevice()
        {
            if (PingHost(sqlite.config["ipCamera"]))
                bl.lblCamera.Text = "Online";
            else
                bl.lblCamera.Text = "Offline";


            if (PingHost(sqlite.config["ipCamera2"]))
                bl.lblCamera2.Text = "Online";
            else
                bl.lblCamera2.Text = "Offline";


            if (PingHost(sqlite.config["ipCamera3"]))
                bl.lblCamera3.Text = "Online";
            else
                bl.lblCamera3.Text = "Offline";


            print pr = new print();
            if ((pr.cekKoneksi()))
            {
                bl.lblPrinter.Text = "Online";
                pr.disconect();
            }
            else
                bl.lblPrinter.Text = "Offline";
        }

        //untuk ngeping ke ip parameter
        public bool PingHost(string nameOrAddress)
        {
            if (nameOrAddress.Length == 0)
                return false;
            bool pingable = false;
            Ping pinger = new Ping();
            try
            {
                PingReply reply = pinger.Send(nameOrAddress);
                pingable = reply.Status == IPStatus.Success;
            }
            catch (PingException)
            {
                // Discard PingExceptions and return false;
            }
            return pingable;
        }

        void backgroundwo()
        {
            worker.DoWork += new DoWorkEventHandler(worker_DoWork);
            worker.WorkerReportsProgress = true;
            worker.RunWorkerAsync();
            resetEvent.WaitOne();
        }

        void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            CekDevice();
            resetEvent.Set();
        }

        void setlog(string[] hasilsplit)
        {
            log("----------------------------------------");
            log("Blister ID : " + hasilsplit[1]);
            log("Cap ID     : " + hasilsplit[2]);
            log("Status     : " + cek(hasilsplit[0]));
            log("----------------------------------------");
        }

        public void tampil(string data)
        {
            if (bl.InvokeRequired)
            {
                bl.Invoke(new Action<string>(tampil), new object[] { data });
                return;
            }

            log("Receive Data : " + data);
        }

        internal int cekKamera(string Message)
        {
            Message = Message.Replace("\r", "");
            Message = Message.Replace("\n", "");
            tampil("Receive Data : " + Message);
            List<string> field = new List<string>();
            field.Add("*");
            string where = "blisterpackid = '" + Message + "'";
            sqlite.select(field, "tmp_blisterPack_id", where);
            if (sqlite.num_rows > 0)
            {
                return 0;
            }
            else
            {
                return 1;
            }
        }
        int terimaCamera1 = 0;
        int terimaCamera2 = 0;
        string dataTerimaCamera1;
        string dataTerimaCamera2;
        string dataTerimaCamera12;
        string dataTerimaCamera22;

        DataRow rowCamera;
        DataRow rowCamera2;
        internal void terimaData(string Message, int p)
        {
            if (bl.InvokeRequired)
            {
                bl.Invoke(new Action<string, int>(terimaData), new object[] { Message, p });
                return;
            }

            Message = Message.Replace("\r", "");
            Message = Message.Replace("\n", "");
            Message = Message.Replace("]d2", "");
            string[] data = Message.Split(',');

            DataTable tableTerimaCamera = new DataTable();
            if (data[1].Equals("0"))
            {
                Message = data[2];
                if (p == 0)
                {
                    if (terimaCamera1 == 0)
                    {
                        dataTerimaCamera1 = Message;
                        terimaCamera1++;
                        rowCamera = tableTerimaCamera.NewRow();
                        rowCamera[0] = Message;
                    }
                    else
                    {
                        rowCamera2 = tableTerimaCamera.NewRow();
                        rowCamera2[0] = Message;
                        dataTerimaCamera2 = Message;
                        terimaCamera1 = 0;
                    }
                }
                else if (p == 1)
                {
                    if (terimaCamera2 == 0)
                    {
                        rowCamera = tableTerimaCamera.NewRow();
                        rowCamera[1] = Message;
                        rowCamera[2] = "Pass";
                        dataTerimaCamera12 = Message;
                        terimaCamera2++;
                    }
                    else
                    {
                        rowCamera2 = tableTerimaCamera.NewRow();
                        rowCamera2[1] = Message;
                        rowCamera2[2] = "Pass";

                        dataTerimaCamera22 = Message;
                        terimaCamera2 = 0;
                        string gabung = "0," + dataTerimaCamera1 + "," + dataTerimaCamera12;
                        tambahData(gabung);
                        gabung = "0," + dataTerimaCamera2 + "," + dataTerimaCamera22;
                        tambahData(gabung);

                    }
                }

                tableTerimaCamera.Rows.Add(rowCamera);
                bl.dgvStatus.DataSource = tableTerimaCamera;
            }
        }

        internal void refresh()
        {
            clearDataReceive();
            clearDataSendFail();
        }

        private void clearDataSendFail()
        {
            tableProductFail = new DataTable();
            tableProductFail.Columns.Add("Blister Pack ID");
            bl.dgvFail.DataSource = tableProductFail;
            bl.lblQtyFail.Text = "0";
        }
    }
    
}
