﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Windows.Forms;
using System.IO;

namespace Generate_GS1
{
    class tcp
    {
        private static TcpClient Client;
        private static Byte[] Data = new Byte[256];
        private static Stream ClientStream;
        public static bool IsConnected = false;

        public bool Connect(string ip, string port, string timeout)
        {
            try
            {
                Client = new TcpClient();
                IAsyncResult result = Client.BeginConnect(ip, Int32.Parse(port), null, null);
                bool success = result.AsyncWaitHandle.WaitOne(Int32.Parse(timeout), true);
                if (success)
                {
                    IsConnected = true;
                    return true;
                }
                else
                {
                    IsConnected = false;
                    return false;
                }
            }
            catch (SocketException ex)
            {
                MessageBox.Show("error +" + ex.ToString());
                if (!ex.NativeErrorCode.Equals(10056))
                    IsConnected = false;
            }
            catch (InvalidOperationException te)
            {
                MessageBox.Show("Printer not connected !");
            }
            return false;
        }

        public void send(string dataTerima)
        {
            try
            {
                if (IsConnected)
                {
                    ClientStream = Client.GetStream();
                    StreamWriter sw = new StreamWriter(ClientStream);
                    sw.WriteLine(dataTerima);
                    sw.Flush();
                }
            }
            catch (InvalidOperationException ex)
            {

            }
        }

        public void dc()
        {
            Client.Close();
        }

        public void updatestatus(string data)
        {
            string dates = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            //pr.txtStatus.AppendText(dates+"\t"+data+"\n");
        }

        public string sendBack(string dataTerima)
        {
            try
            {
                byte[] data = new Byte[256];
                if (IsConnected)
                {
                    ClientStream = Client.GetStream();
                    StreamWriter sw = new StreamWriter(ClientStream);
                    sw.WriteLine(dataTerima);
                    sw.Flush();
                    Int32 bytes = ClientStream.Read(data, 0, data.Length);
                    string responseData = System.Text.Encoding.ASCII.GetString(data, 0, bytes);
                    return responseData;
                }
                return "";
            }
            catch (InvalidOperationException ex)
            {

            }
            return "";
        }
    }
}
