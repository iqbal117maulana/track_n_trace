﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Net.NetworkInformation;
using System.Windows.Forms;
using System.Data;

namespace Generate_GS1
{
    class Control
    {
        public const char delimit = ',';
        private Thread serverThread;
        public sqlitecs sq;
        public dbaccess db;

        public String UOM_BASKET = "Basket";
        public String UOM_INNER_BOX = "Innerbox";
        public String UOM_VIAL = "Vial";
        public String UOM_MASTER_BOX = "Masterbox";

        public String UOM_BASKET_TEXT = "Basket";
        public String UOM_INNER_BOX_TEXT = "Innerbox";
        public String UOM_VIAL_TEXT = "Vial";

        public ServerMultiClient srv;

        public void MulaiServer()
        {
            srv = new ServerMultiClient();
            srv.Start(this, sq.config["portserver"]);
        }

        public Control()
        {
            sq = new sqlitecs();
            db = new dbaccess();
            serverThread = new Thread(new ThreadStart(MulaiServer));
            serverThread.Start();
        }

        public bool GetBatch(string batchNumber)
        {
            string sql;
            sql = "SELECT DISTINCT batchNumber FROM Vaccine WHERE batchNumber = '" + batchNumber + "'";
            DataTable dtTmp = new DataTable();
            db.OpenQuery(out dtTmp, sql);
            if (dtTmp.Rows.Count > 0)
            {
                return true;
            }
            else
                return false;
        }

        public string GetGTIN(out String GS1, out String ExpDate, string uom, string batchNumber)
        {
            string sql;
            string hasil = "";

            if (uom == UOM_VIAL)
            {
                sql = "SELECT a.gsOneVialId, b.gtin, a.expDate FROM Vaccine a INNER JOIN product_model b ON a.productModelId = b.product_model " +
                        " WHERE a.batchNumber = '" + batchNumber  + "'";
            }
            else if (uom == UOM_INNER_BOX)
            {
                sql = "SELECT a.innerBoxGsOneId, b.gtinoutbox, a.expDate FROM Vaccine a INNER JOIN product_model b ON a.productModelId = b.product_model " +
                        " WHERE a.batchNumber = '" + batchNumber + "'";
            }
            else
            {
                sql = "SELECT a.masterBoxGsOneId, b.gtinmasterbox, a.expDate FROM Vaccine a INNER JOIN product_model b ON a.productModelId = b.product_model " +
                        " WHERE a.batchNumber = '" + batchNumber + "'";
            }
            DataTable dtTmp = new DataTable();
            db.OpenQuery(out dtTmp, sql);
            if (dtTmp.Rows.Count > 0)
            {
                GS1 = dtTmp.Rows[0][0].ToString();
                ExpDate = dtTmp.Rows[0][2].ToString();
                hasil = dtTmp.Rows[0][1].ToString();
            }
            else
            {
                GS1 = "";
                ExpDate = "";
                hasil = "";
            }
            return hasil;
        }

        public DataTable GetGTIN(int count, string uom, string batchNumber)
        {
            string sql;
            if (uom == UOM_VIAL)
            {
                sql = "SELECT TOP " + count + " a.gsOneVialId, b.gtin, a.expDate FROM Vaccine a INNER JOIN product_model b ON a.productModelId = b.product_model " +
                        " WHERE a.batchNumber = '" + batchNumber + "' AND a.gsOneVialId IS NOT NULL ";
            }
            else if (uom == UOM_INNER_BOX)
            {
                sql = "SELECT TOP " + count + " a.innerBoxGsOneId, b.gtinoutbox, a.expDate FROM Vaccine a INNER JOIN product_model b ON a.productModelId = b.product_model " +
                        " WHERE a.batchNumber = '" + batchNumber + "' AND a.innerBoxGsOneId IS NOT NULL";
            }
            else
            {
                sql = "SELECT TOP " + count + " a.masterBoxGsOneId, b.gtinmasterbox, a.expDate FROM Vaccine a INNER JOIN product_model b ON a.productModelId = b.product_model " +
                        " WHERE a.batchNumber = '" + batchNumber + "' AND a.masterBoxGsOneId IS NOT NULL";
            }
            DataTable dtTmp = new DataTable();
            db.OpenQuery(out dtTmp, sql);
            return dtTmp;
        }

        public bool GetDO(out String ShippingOrderNUmber, out String customerName, 
            out String customerAddress, out String salesOrder, string DONumber)
        {
            string sql;
            sql = "select a.PackingSlipId, b.shippingOrderNumber, b.customerName, b.customerAddress, b.salesOrder " +
                    "FROM packing_slip a INNER JOIN shippingOrder b ON a.ShipmentOrderNumber = b.shippingOrderNumber " + 
                    "WHERE a.PackingSlipId = '" + DONumber + "'";
            DataTable dtTmp = new DataTable();
            db.OpenQuery(out dtTmp, sql);
            if (dtTmp.Rows.Count > 0)
            {
                ShippingOrderNUmber = dtTmp.Rows[0][1].ToString();
                customerName = dtTmp.Rows[0][2].ToString();
                customerAddress = dtTmp.Rows[0][3].ToString();
                salesOrder = dtTmp.Rows[0][4].ToString();
                return true;
            }
            else
            {
                ShippingOrderNUmber = "";
                customerName = "";
                customerAddress = "";
                salesOrder = "";
                return false;
            }                
        }
    }
}
