﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace Generate_GS1
{
    public partial class Form1 : Form
    {
        Control ms;
        static tcp tcp = new tcp();
        static sqlitecs sql = new sqlitecs();

        public Form1()
        {
            InitializeComponent();
            ms = new Control();
        }

        void CekBatch()
        {
            if (txtBatch.Text != "")
            {
                bool hasil;
                hasil = ms.GetBatch(txtBatch.Text);
                if (!hasil)
                {
                    MessageBox.Show("Batch not found");
                }
                else
                    cmbTipe.Focus();
            }
            else
                MessageBox.Show("Batch empty");
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void txtBatch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                CekBatch();
            }
        }

        private void txtBatch_Leave(object sender, EventArgs e)
        {
            CekBatch();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (txtBatch.Text == "")
            {
                MessageBox.Show("Batch empty");
                return;
            }
            if (cmbTipe.SelectedIndex == -1)
            {
                MessageBox.Show("Choose type GS1");
                return;
            }
            if ((txtBatch.Text == "") || (txtBatch.Text == "0"))
            {
                MessageBox.Show("Batch empty");
                return;
            }

            //PROSES PRINT
            if (cmbTipe.SelectedIndex == 0)
            {
                DataTable dtTmp = new DataTable();
                dtTmp = ms.GetGTIN(Convert.ToInt32(txtCount.Text), ms.UOM_VIAL, txtBatch.Text);

                List<object> val = new List<object>();

                tcp = new tcp();
                tcp.Connect(sql.config["ipprinter"], sql.config["portprinter"], sql.config["timeout"]);
                for (int i = 0; i <= dtTmp.Rows.Count - 1; i++)
                {
                    val = new List<object>();
                    val.Add(dtTmp.Rows[i][0].ToString());
                    val.Add("GS1 VIAL");
                    val.Add(": " + dtTmp.Rows[i][1].ToString());
                    val.Add(": " + dtTmp.Rows[i][2].ToString());

                    String ZplString = String.Format(ZplTemplateGS1, val[0], val[1], val[2], val[3]);
                    tcp.send(ZplString);
                }
                tcp.dc();
            }
            else if (cmbTipe.SelectedIndex == 1)
            {
                DataTable dtTmp = new DataTable();
                dtTmp = ms.GetGTIN(Convert.ToInt32(txtCount.Text), ms.UOM_INNER_BOX, txtBatch.Text);

                List<object> val = new List<object>();

                tcp = new tcp();
                tcp.Connect(sql.config["ipprinter"], sql.config["portprinter"], sql.config["timeout"]);
                for (int i = 0; i <= dtTmp.Rows.Count - 1; i++)
                {
                    val = new List<object>();
                    val.Add(dtTmp.Rows[i][0].ToString());
                    val.Add("GS1 INNERBOX");
                    val.Add(": " + dtTmp.Rows[i][1].ToString());
                    val.Add(": " + dtTmp.Rows[i][2].ToString());

                    String ZplString = String.Format(ZplTemplateGS1, val[0], val[1], val[2], val[3]);
                    tcp.send(ZplString);
                }
                tcp.dc();
            }
            else if (cmbTipe.SelectedIndex == 2)
            {
                DataTable dtTmp = new DataTable();
                dtTmp = ms.GetGTIN(Convert.ToInt32(txtCount.Text), ms.UOM_MASTER_BOX, txtBatch.Text);

                List<object> val = new List<object>();

                tcp = new tcp();
                tcp.Connect(sql.config["ipprinter"], sql.config["portprinter"], sql.config["timeout"]);
                for (int i = 0; i <= dtTmp.Rows.Count - 1; i++)
                {
                    val = new List<object>();
                    val.Add(dtTmp.Rows[i][0].ToString());
                    val.Add("GS1 MASTERBOX");
                    val.Add(": " + dtTmp.Rows[i][1].ToString());
                    val.Add(": " + dtTmp.Rows[i][2].ToString());

                    String ZplString = String.Format(ZplTemplateGS1, val[0], val[1], val[2], val[3]);
                    tcp.send(ZplString);
                }
                tcp.dc();
            }
        }

        private String ZplTemplateGS1 = "^XA~TA000~JSN^LT0^MNW^MTT^PON^PMN^LH0,0^JMA^PR2,3~SD25^JUS^LRN^CI0^XZ\r\n" +
            "^XA~SD25^MD25^MU203"
            + "^FX "
            + "^FO10,10^GB540,250,3^FS"
            + "^FO20,50"
            //+ "^BXN,9,200,,,,,"
            + "^BXN,7,200,26,26"
            + "^FD{0}^FS"
            + "^FT225,40^A0N20,30^FD{1}^FS"
            + "^FO225,50^A0N,20,20^FH^FDGS1 :^FS"
            + "^FO280,50^FB250,4,3,L,0^A0N,20,20^FH^FD{0}^FS"
            + "^FO225,120^A0N,20,20^FH^FDGTIN^FS"
            + "^FO310,120^A0N,20,20^FH^FD{2}^FS"
            + "^FO225,150^A0N,20,20^FH^FDExp Date^FS"
            + "^FO310,150^FB260,2,3,L,12^A0N,20,20^FH^FD{3}^FS"            
            + "^XZ";

        private static String ZplTemplateDO = "^XA~TA000~JSN^LT0^MNW^MTT^PON^PMN^LH0,0^JMA^PR2,3~SD25^JUS^LRN^CI0^XZ\r\n" +
            "^XA~SD25^MD25^MU203"
            + "^FX "
            + "^FO10,10^GB540,250,3^FS"
            + "^FO20,50"
            + "^BXN,10,200,,,,,"
            + "^FD{0}^FS"
            + "^FT225,40^A0N20,30^FDDO STICKER^FS"
            + "^FO225,50^A0N,20,20^FH^FDDO^FS"
            + "^FO310,50^A0N,20,20^FH^FD{1}^FS"
            + "^FO225,80^A0N,20,20^FH^FDSO^FS"
            + "^FO310,80^A0N,20,20^FH^FD{2}^FS"
            + "^FO225,110^A0N,20,20^FH^FDCustomer^FS"
            + "^FO310,110^FB260,2,3,L,12^A0N,20,20^FH^FD{3}^FS"
            + "^FO225,160^A0N,20,20^FH^FDAddress^FS"
            + "^FO310,160^FB260,4,3,L,12^A0N,20,20^FH^FD{4}^FS"
            + "^XZ";

        private void button2_Click(object sender, EventArgs e)
        {
            if (txtDO.Text == "")
            {
                MessageBox.Show("DO is empty");
                return;
            }
            else
            {
                string ShippingOrderNumber;
                string CustomerName;
                string CustomerAddress;
                string SalesOrder;
                if (ms.GetDO(out ShippingOrderNumber, out CustomerName, out CustomerAddress, out SalesOrder, txtDO.Text))
                {
                    List<object> val = new List<object>();
                    val.Add(txtDO.Text);
                    val.Add(": " + txtDO.Text);
                    val.Add(": " + SalesOrder);
                    val.Add(": " + CustomerName);
                    val.Add(": " + CustomerAddress);
                    String ZplString = String.Format(ZplTemplateDO, val[0], val[1], val[2], val[3], val[4]);

                    //HARUS PAKE YG INI
                    tcp = new tcp();
                    tcp.Connect(sql.config["ipprinter"], sql.config["portprinter"], sql.config["timeout"]);
                    tcp.send(ZplString);
                    tcp.dc();
                }
                else
                    MessageBox.Show("DO not found");
            }
        }
    }
}
